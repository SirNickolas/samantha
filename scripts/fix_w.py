#!/usr/bin/env python
# coding: utf-8

"""
This script moves `{w}` out of `{en}` and `{ru}`, because they just don't work together.
"""

import os.path
import re
import sys

try:
    from cStringIO import StringIO as BytesIO
except ImportError:
    from io import BytesIO

try:
    range = xrange
except NameError:
    pass


def is_suspicious(fragment):
    return b"{en" in fragment or b"{/en" in fragment or b"{ru" in fragment or b"{/ru" in fragment


def process_translation(en, ru, get_fragment, result):
    if b"{w" not in en and b"{w" not in ru:
        # Fast path.
        result.write(get_fragment())
        return True

    en_parts = re.split(br"(\{w\b.*?\})", en)
    ru_parts = re.split(br"(\{w\b.*?\})", ru)
    en_parts.append(b"")
    ru_parts.append(b"")
    n = len(en_parts)
    if n != len(ru_parts):
        result.write(get_fragment())
        return False

    # Compare `{w}`s.
    if en_parts[1::2] != ru_parts[1::2]:
        result.write(get_fragment())
        return False

    for i in range(0, n, 2):
        if en_parts[i] or ru_parts[i]:
            result.write(b"{en}")
            result.write(en_parts[i])
            result.write(b"{/en}{ru}")
            result.write(ru_parts[i])
            result.write(b"{/ru}")
        result.write(en_parts[i + 1]) # {w}

    return True

def process_line(line, result):
    if b"{w" not in line:
        # Fast path.
        result.write(line)
        return True

    ok = True
    last_pos = 0
    for m in re.finditer(br"\{en\}(.*?)\{/en\}\{ru\}(.*?)\{/ru\}", line):
        skipped = line[last_pos:m.start()]
        last_pos = m.end()
        en = m.group(1)
        ru = m.group(2)
        if is_suspicious(skipped) or is_suspicious(en) or is_suspicious(ru):
            result.write(m.group())
            ok = False
        else:
            result.write(skipped)
            if not process_translation(en, ru, m.group, result):
                ok = False

    skipped = line[last_pos:]
    result.write(skipped)
    return ok and not is_suspicious(skipped)


def main():
    for filename in sys.argv[1:]:
        print("Processing %s..." % filename)

        result = BytesIO()
        line_num = 0
        errors = 0
        with open(filename, "rb") as f:
            for line in f:
                line_num += 1
                if not process_line(line, result):
                    errors += 1
                    print("Line %s: Manual resolution required." % line_num)

        print("Total: %s.\n" % errors if errors != 0 else "")
        with open("%s.1%s" % os.path.splitext(filename), "wb") as f:
            f.write(result.getvalue())

    if sys.platform.startswith("win") and sys.stdin.isatty() and sys.stdout.isatty():
        input("Press Enter to quit.")


if __name__ == "__main__":
    main()
