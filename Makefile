.PHONY: all clean

ESTOOL_VERSION := 1.001
MOBILE := mobile
MOBILE_SAM := $(MOBILE)/Samantha
MOBILE_PROSE := sam playedit
MOBILE_PROSE := $(MOBILE_PROSE:%=$(MOBILE_SAM)/%.rpy)

ifeq ($(OS),Windows_NT)
    EXTRA_DEPS := $(MOBILE)/conv_to_lf
else
    EXTRA_DEPS :=
endif

.PHONY: $(MOBILE_SAM) $(MOBILE_PROSE) $(MOBILE)/conv_to_lf

all: $(MOBILE_SAM) $(MOBILE_PROSE) $(EXTRA_DEPS)

clean:
	$(RM) -r $(MOBILE_SAM)

$(MOBILE_SAM):
	cd $(@D) && ../estool/estool-$(ESTOOL_VERSION).py --keep-cwd

$(MOBILE_PROSE): $(MOBILE_SAM)
	scripts/typography.sed -i $@

# Without braces Make runs `/Windows/System32/find.exe` instead of `/usr/bin/find`. :/
$(MOBILE)/conv_to_lf: $(MOBILE_PROSE)
	{ find $(MOBILE_SAM) -iregex '.*\.\(r?py\|txt\)' -exec dos2unix '{}' +; }
