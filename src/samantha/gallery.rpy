﻿init -10 python:
    style.ssbtn = Style(style.button)
    style.ssbtn.background = "samantha/image/menu/emp.png"
    style.ssbtn.hover_background = "samantha/image/menu/emp.png"
    style.ssbtn.selected_background = "samantha/image/menu/emp.png"
    style.ssbtn.selected_hover_background = "samantha/image/menu/emp.png"
    style.ssbtn.selected_idle_background = "samantha/image/menu/emp.png"

    style.sstxt60 = Style(style.default)
    style.sstxt60.font = "samantha/image/menu/Pribambas.ttf"
    style.sstxt60.size = 60
    style.sstxt60.color = "#004A7F" 
    style.sstxt60.hover_color = "#0094FF"
    style.sstxt60.selected_color = "#004A7F"
    style.sstxt60.selected_idle_color = "#004A7F"
    style.sstxt60.selected_hover_color = "#0094FF"
    style.sstxt60.insensitive_color = "#004A7F"
    
    style.sstxtyel = Style(style.default)
    style.sstxtyel.font = "samantha/image/menu/Pribambas.ttf"
    style.sstxtyel.size = 30
    style.sstxtyel.color = "#FFD800" 
    style.sstxtyel.hover_color = "#0094FF"
    style.sstxtyel.selected_color = "#FFD800"
    style.sstxtyel.selected_idle_color = "#FFD800"
    style.sstxtyel.selected_hover_color = "#0094FF"
    style.sstxtyel.insensitive_color = "#FFD800"    

    style.sstxt75 = Style(style.default)
    style.sstxt75.font = "samantha/image/menu/Pribambas.ttf"
    style.sstxt75.size = 75
    style.sstxt75.color = "#004A7F" 
    style.sstxt75.hover_color = "#0094FF"
    style.sstxt75.selected_color = "#004A7F"
    style.sstxt75.selected_idle_color = "#004A7F"
    style.sstxt75.selected_hover_color = "#0094FF"
    style.sstxt75.insensitive_color = "#004A7F"
    
    style.sstxt75blue = Style(style.default)
    style.sstxt75blue.font = "samantha/image/menu/Pribambas.ttf"
    style.sstxt75blue.size = 75
    style.sstxt75blue.color = "#0094FF" 
    style.sstxt75blue.hover_color = "#0094FF"
    style.sstxt75blue.selected_color = "#0094FF"
    style.sstxt75blue.selected_idle_color = "#0094FF"
    style.sstxt75blue.selected_hover_color = "#0094FF"
    style.sstxt75blue.insensitive_color = "#0094FF"    
    
    style.sstxt90 = Style(style.default)
    style.sstxt90.font = "samantha/image/menu/Pribambas.ttf"
    style.sstxt90.size = 100
    style.sstxt90.color = "#004A7F" 
    style.sstxt90.hover_color = "#004A7F"
    style.sstxt90.selected_color = "#004A7F"
    style.sstxt90.selected_idle_color = "#004A7F"
    style.sstxt90.selected_hover_color = "#004A7F"
    style.sstxt90.insensitive_color = "#004A7F"    
    
    style.ssbtn2 = Style(style.button)
    style.ssbtn2.background = "samantha/image/menu/ssbtn.png"
    style.ssbtn2.hover_background = "samantha/image/menu/ssbtn_hover.png"
    style.ssbtn2.selected_background = "samantha/image/menu/ssbtn.png"
    style.ssbtn2.selected_hover_background = "samantha/image/menu/ssbtn_hover.png"
    style.ssbtn2.selected_idle_background = "samantha/image/menu/ssbtn.png"

    style.ssbtn60 = Style(style.button)
    style.ssbtn60.background = "samantha/image/menu/btn60.png"
    style.ssbtn60.hover_background = "samantha/image/menu/btn60_hover.png"
    style.ssbtn60.selected_background = "samantha/image/menu/btn60.png"
    style.ssbtn60.selected_hover_background = "samantha/image/menu/btn60_hover.png"
    style.ssbtn60.selected_idle_background = "samantha/image/menu/btn60.png"    

    style.ssbtn75 = Style(style.button)
    style.ssbtn75.background = "samantha/image/menu/btn75.png"
    style.ssbtn75.hover_background = "samantha/image/menu/btn75_hover.png"
    style.ssbtn75.selected_background = "samantha/image/menu/btn75.png"
    style.ssbtn75.selected_hover_background = "samantha/image/menu/btn75_hover.png"
    style.ssbtn75.selected_idle_background = "samantha/image/menu/btn75.png"
    
    ssg = Gallery()
    
    ssg.transition = fade
    ssg.hover_border = None
    ssg.idle_border = None
    
 

                        
screen ssg_gallery:


    
    tag menu
    modal True
    python:

#        if persistent.ssg_103 == True:  
#            if persistent.ssg_108 == True:      
#                if persistent.ssg_113 == True:       
#                    if persistent.ssg_119 == True:        
#                        if persistent.ssg_131 == True:                        
#                            persistent.ssg_134 = True
#                            persistent.ssg_135 = True
#                            persistent.ssg_24 = True    
    
        ssg.button("ssg_1")                                       
        if persistent.ssg_1 == True:                     
            ssg.image("samantha/image/day_sam.jpg")   
        
        ssg.button("ssg_2")
        if persistent.ssg_2 == True:
            ssg.image("samantha/image/night_sam.jpg")
        
        ssg.button("ssg_3")
        if persistent.ssg_3 == True:
            ssg.image("samantha/image/beach_bar1_close.jpg")
        
        ssg.button("ssg_4")
        if persistent.ssg_4 == True:
            ssg.image("samantha/image/ss_washm.jpg")
        
        ssg.button("ssg_5")
        if persistent.ssg_5 == True:
            ssg.image("samantha/image/ss_secondfloor.jpg")
        
        ssg.button("ssg_6")
        if persistent.ssg_6 == True:
            ssg.image("samantha/image/ss_datroom_day.jpg")
        
        ssg.button("ssg_7")
        if persistent.ssg_7 == True:
            ssg.image("samantha/image/ss_datroom_light.jpg")
        
        ssg.button("ssg_8")
        if persistent.ssg_8 == True:
            ssg.image("samantha/image/ss_countryhouse.jpg")
        
        ssg.button("ssg_9")
        if persistent.ssg_9 == True:
            ssg.image("samantha/image/ss_countryside.jpg")
        
        ssg.button("ssg_10")
        if persistent.ssg_10 == True:
            ssg.image("samantha/image/ss_int_countryhouse.jpg")
        
        ssg.button("ssg_11")
        if persistent.ssg_11 == True:
            ssg.image("samantha/image/brl_less_cave.jpg")
        
        ssg.button("ssg_12")
        if persistent.ssg_12 == True:
            ssg.image("samantha/image/brl_up.jpg")
            


    add "samantha/image/menu/s_menu_bag_open.jpg"
    add ssg.make_button("ssg_1", im.Scale("samantha/image/day_sam.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=123, ypos=269)
    add ssg.make_button("ssg_2", im.Scale("samantha/image/night_sam.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=123, ypos=530)
    add ssg.make_button("ssg_3", im.Scale("samantha/image/beach_bar1_close.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=123, ypos=791)
    add ssg.make_button("ssg_4", im.Scale("samantha/image/ss_washm.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=562, ypos=269)
    add ssg.make_button("ssg_5", im.Scale("samantha/image/ss_secondfloor.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=562, ypos=530)
    add ssg.make_button("ssg_6", im.Scale("samantha/image/ss_datroom_day.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=562, ypos=791)
    add ssg.make_button("ssg_7", im.Scale("samantha/image/ss_datroom_light.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=1003, ypos=269)
    add ssg.make_button("ssg_8", im.Scale("samantha/image/ss_countryhouse.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=1003, ypos=530)
    add ssg.make_button("ssg_9", im.Scale("samantha/image/ss_countryside.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=1003, ypos=791)
    add ssg.make_button("ssg_10", im.Scale("samantha/image/ss_int_countryhouse.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=1441, ypos=269)
    add ssg.make_button("ssg_11", im.Scale("samantha/image/brl_less_cave.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=1441, ypos=530)
    add ssg.make_button("ssg_12", im.Scale("samantha/image/brl_up.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=1441, ypos=791)
    add "samantha/image/menu/frames.png"

            
            
   
    if _preferences.language == None:
    
        textbutton [" В МЕНЮ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["   ОБЛИК"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["   МЕСТА"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton [" СОБЫТИЯ"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")  

        textbutton [" ДАЛЬШЕ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_2")   

    else:
        textbutton ["   MENU"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["  PERSON"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["  PLACES"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton ["  EVENTS"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3") 

        textbutton ["   NEXT"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_2")
            
        textbutton ["?"]:
            style "ssbtn"
            text_style "sstxtyel"
            xpos 40
            ypos 80
            action Jump("ss_open_gal")            
            
screen ssg_gallery_sprites:
    tag menu
    modal True
    python:


        
        ssg.button("ssg_99")                                       
        if persistent.ssg_99 == True:                        
            ssg.image("samantha/image/menu/temp/arriv.jpg")   
        
        ssg.button("ssg_98")
        if persistent.ssg_98 == True:
            ssg.image("samantha/image/menu/temp/casual.jpg")
            
        ssg.button("ssg_97")                                       
        if persistent.ssg_97 == True:                        
            ssg.image("samantha/image/menu/temp/pj.jpg")   
        
        ssg.button("ssg_96")
        if persistent.ssg_96 == True:
            ssg.image("samantha/image/menu/temp/pv.jpg")            

        ssg.button("ssg_95")                                       
        if persistent.ssg_95 == True:                        
            ssg.image("samantha/image/menu/temp/reddress.jpg")
            
        ssg.button("ssg_94")                                         
        if persistent.ssg_94 == True:                        
            ssg.image("samantha/image/menu/temp/purpdress.jpg")
            
        ssg.button("ssg_93")                                       
        if persistent.ssg_93 == True:                        
            ssg.image("samantha/image/menu/temp/angel1.jpg") 
            
        ssg.button("ssg_92")                                       
        if persistent.ssg_92 == True:             
            ssg.image("samantha/image/menu/temp/angel2.jpg") 
            
    add "samantha/image/menu/s_menu_bag_open.jpg" 
    add ssg.make_button("ssg_99", im.Scale("samantha/image/menu/temp/arriv.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=223, ypos=230)
    add ssg.make_button("ssg_98", im.Scale("samantha/image/menu/temp/casual.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=623, ypos=230)
    add ssg.make_button("ssg_97", im.Scale("samantha/image/menu/temp/pj.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=1023, ypos=230)
    add ssg.make_button("ssg_96", im.Scale("samantha/image/menu/temp/pv.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=1423, ypos=230)

    add ssg.make_button("ssg_95", im.Scale("samantha/image/menu/temp/reddress.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=223, ypos=630)
    add ssg.make_button("ssg_94", im.Scale("samantha/image/menu/temp/purpdress.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=623, ypos=630)
    add ssg.make_button("ssg_93", im.Scale("samantha/image/menu/temp/angel1.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=1023, ypos=630)
    add ssg.make_button("ssg_92", im.Scale("samantha/image/menu/temp/angel2.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=1423, ypos=630)
    add "samantha/image/menu/sp_frames.png"
    if _preferences.language == None:
    
        textbutton [" В МЕНЮ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["   ОБЛИК"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["   МЕСТА"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton [" СОБЫТИЯ"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")

        textbutton [" ДАЛЬШЕ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_sprites2")            
  
    else:
        textbutton ["   MENU"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["  PERSON"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["  PLACES"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton ["  EVENTS"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")  
            
        textbutton ["   NEXT"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_sprites2")  




screen ssg_gallery_sprites2:
    tag menu
    modal True
    python:


        
        ssg.button("ssg_91")                                       
        if persistent.ssg_91 == True:                        
            ssg.image("samantha/image/menu/temp/coat.jpg")   
        
        ssg.button("ssg_90")
        if persistent.ssg_90 == True:
            ssg.image("samantha/image/menu/temp/forest.jpg")
            
        ssg.button("ssg_89")                                       
        if persistent.ssg_89 == True:                        
            ssg.image("samantha/image/menu/temp/shirt.jpg")   
        
        ssg.button("ssg_88")
        if persistent.ssg_88 == True:
            ssg.image("samantha/image/menu/temp/blouse.jpg")            

        ssg.button("ssg_87")                                       
        if persistent.ssg_87 == True:                        
            ssg.image("samantha/image/menu/temp/rushirt.jpg")
            
        ssg.button("ssg_86")                                         
        if persistent.ssg_86 == True:                        
            ssg.image("samantha/image/menu/temp/od_coat.jpg")
            

            
    add "samantha/image/menu/s_menu_bag_open.jpg" 
    add ssg.make_button("ssg_91", im.Scale("samantha/image/menu/temp/coat.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=223, ypos=230)
    add ssg.make_button("ssg_90", im.Scale("samantha/image/menu/temp/forest.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=623, ypos=230)
    add ssg.make_button("ssg_89", im.Scale("samantha/image/menu/temp/shirt.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=1023, ypos=230)
#    add ssg.make_button("ssg_84", im.Scale("samantha/image/menu/temp/pv.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=1423, ypos=230)

    add ssg.make_button("ssg_88", im.Scale("samantha/image/menu/temp/blouse.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=223, ypos=630)
    add ssg.make_button("ssg_87", im.Scale("samantha/image/menu/temp/rushirt.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=623, ypos=630)
    add ssg.make_button("ssg_86", im.Scale("samantha/image/menu/temp/od_coat.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=1023, ypos=630)
#    add ssg.make_button("ssg_85", im.Scale("samantha/image/menu/temp/angel2.jpg", 227, 350), locked="samantha/image/menu/sp_locked.png", hover_border = "samantha/image/menu/sp_hover_border.png", idle_border = "samantha/image/menu/sp_idle_border.png",  xpos=1423, ypos=630)
    add "samantha/image/menu/sp_frames_2.png"
    if _preferences.language == None:
    
        textbutton [" В МЕНЮ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["   ОБЛИК"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["   МЕСТА"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton [" СОБЫТИЯ"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")
            
        textbutton [" ДАЛЬШЕ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_sprites")            
            
            
  
    else:
        textbutton ["   MENU"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["  PERSON"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["  PLACES"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton ["  EVENTS"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")  

        textbutton ["   NEXT"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_sprites")



  
screen ssg_gallery_2: 
    tag menu
    modal True
    python:
    
        ssg.button("ssg_13")
        if persistent.ssg_13 == True:
            ssg.image("samantha/image/ss_cavehole.jpg")

        ssg.button("ssg_14")
        if persistent.ssg_14 == True:
            ssg.image("samantha/image/ss_openwagon.jpg")

        ssg.button("ssg_15")
        if persistent.ssg_15 == True:
            ssg.image("samantha/image/wml_ladder.jpg")

        ssg.button("ssg_16")
        if persistent.ssg_16 == True:
            ssg.image("samantha/image/ss_int_countryhouse_y2_nolight.jpg")

        ssg.button("ssg_17")
        if persistent.ssg_17 == True:
            ssg.image("samantha/image/ss_bear_calm.jpg")

        ssg.button("ssg_18")
        if persistent.ssg_18 == True:
            ssg.image("samantha/image/cbn.jpg")

        ssg.button("ssg_19")
        if persistent.ssg_19 == True:
            ssg.image("samantha/image/cbndark.jpg")

        ssg.button("ssg_20")
        if persistent.ssg_20 == True:
            ssg.image("samantha/image/room_is_full.jpg")

        ssg.button("ssg_21")
        if persistent.ssg_21 == True:
            ssg.image("samantha/image/short_balcony_hw.jpg")

        ssg.button("ssg_22")
        if persistent.ssg_22 == True:
            ssg.image("samantha/image/big_balcony_hw.jpg")

        ssg.button("ssg_23")
        if persistent.ssg_23 == True:
            ssg.image("samantha/image/short_balcony.jpg")
         
        ssg.button("ssg_24")
        if persistent.ssg_24 == True:
            ssg.image("samantha/image/wml_ladder_night.jpg")            
        
    add "samantha/image/menu/s_menu_bag_open.jpg" 
    add ssg.make_button("ssg_13", im.Scale("samantha/image/ss_cavehole.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=123, ypos=269)
    add ssg.make_button("ssg_14", im.Scale("samantha/image/ss_openwagon.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=123, ypos=530)
    add ssg.make_button("ssg_15", im.Scale("samantha/image/wml_ladder.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=123, ypos=791)
    add ssg.make_button("ssg_16", im.Scale("samantha/image/ss_int_countryhouse_y2_nolight.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=562, ypos=269)
    add ssg.make_button("ssg_17", im.Scale("samantha/image/ss_bear_calm.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=562, ypos=530)
    add ssg.make_button("ssg_18", im.Scale("samantha/image/cbn.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=562, ypos=791)
    add ssg.make_button("ssg_19", im.Scale("samantha/image/cbndark.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=1003, ypos=269)
    add ssg.make_button("ssg_20", im.Scale("samantha/image/room_is_full.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=1003, ypos=530)
    add ssg.make_button("ssg_21", im.Scale("samantha/image/short_balcony_hw.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=1003, ypos=791)
    add ssg.make_button("ssg_22", im.Scale("samantha/image/big_balcony_hw.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=1441, ypos=269)
    add ssg.make_button("ssg_23", im.Scale("samantha/image/short_balcony.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=1441, ypos=530)
    add ssg.make_button("ssg_24", im.Scale("samantha/image/wml_ladder_night.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=1441, ypos=791)

    add "samantha/image/menu/frames.png"

            
            

    if _preferences.language == None:
    
        textbutton [" В МЕНЮ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["   ОБЛИК"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["   МЕСТА"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton [" СОБЫТИЯ"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")  

        textbutton [" ДАЛЬШЕ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_6")  

    else:
        textbutton ["   MENU"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["  PERSON"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["  PLACES"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton ["  EVENTS"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")

        textbutton ["   NEXT"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_6") 
            
        textbutton ["?"]:
            style "ssbtn"
            text_style "sstxtyel"
            xpos 40
            ypos 80
            action Jump("ss_open_gal")


###########################
screen ssg_gallery_6: 
    tag menu
    modal True
    python:

        ssg.button("ssg_25")
        if persistent.ssg_25 == True:
            ssg.image("samantha/image/ss_wintercabin.jpg")

        ssg.button("ssg_26")
        if persistent.ssg_26 == True:
            ssg.image("samantha/image/menu/addcg/tower_top.jpg")

        ssg.button("ssg_27")
        if persistent.ssg_27 == True:
            ssg.image("samantha/image/hosp.jpg")

        ssg.button("ssg_28")
        if persistent.ssg_28 == True:
            ssg.image("samantha/image/ss_nighttrain.jpg")

        ssg.button("ssg_29")
        if persistent.ssg_29 == True:
            ssg.image("samantha/image/ss_daytrain.jpg")

        ssg.button("ssg_30")
        if persistent.ssg_30 == True:
            ssg.image("samantha/image/escalator.jpg")

        ssg.button("ssg_31")
        if persistent.ssg_31 == True:
            ssg.image("samantha/image/int_subway_mid.jpg")            

    add "samantha/image/menu/s_menu_bag_open.jpg" 
    add ssg.make_button("ssg_25", im.Scale("samantha/image/ss_wintercabin.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=123, ypos=269)
    add ssg.make_button("ssg_26", im.Scale("samantha/image/menu/addcg/tower_top.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=123, ypos=530)
    add ssg.make_button("ssg_27", im.Scale("samantha/image/hosp.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=123, ypos=791)
    add ssg.make_button("ssg_28", im.Scale("samantha/image/ss_nighttrain.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=562, ypos=269)
    add ssg.make_button("ssg_29", im.Scale("samantha/image/ss_daytrain.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=562, ypos=530)
    add ssg.make_button("ssg_30", im.Scale("samantha/image/escalator.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=562, ypos=791)
    add ssg.make_button("ssg_31", im.Scale("samantha/image/int_subway_mid.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_places.png",  xpos=1003, ypos=269)
    add "samantha/image/menu/frames5.png"

            
            

    if _preferences.language == None:
    
        textbutton [" В МЕНЮ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["   ОБЛИК"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["   МЕСТА"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton [" СОБЫТИЯ"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")  

        textbutton [" ДАЛЬШЕ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery")  

    else:
        textbutton ["   MENU"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["  PERSON"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["  PLACES"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton ["  EVENTS"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")

        textbutton ["   NEXT"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery") 
            
        textbutton ["?"]:
            style "ssbtn"
            text_style "sstxtyel"
            xpos 40
            ypos 80
            action Jump("ss_open_gal")



            
############################
screen ssg_gallery_3:
    tag menu
    modal True
    python:

    
        ssg.button("ssg_101")                                       
        if persistent.ssg_101 == True:                     
            ssg.image("samantha/image/ss_tie.jpg")   
        
        ssg.button("ssg_102")
        if persistent.ssg_101 == True:
            ssg.image("samantha/image/menu/addcg/ss_tie2.jpg")
        
        ssg.button("ssg_103")
        if persistent.ssg_103 == True:
            ssg.image("samantha/image/d1_snails.jpg")
        
        ssg.button("ssg_104")
        if persistent.ssg_104 == True:
            ssg.image("samantha/image/sweets.jpg")
        
        ssg.button("ssg_105")
        if persistent.ssg_104 == True:
            ssg.image("samantha/image/menu/addcg/sweets2.jpg")
        
        ssg.button("ssg_106")
        if persistent.ssg_106 == True:
            ssg.image("samantha/image/ss_d2window2.jpg")
        
        ssg.button("ssg_107")
        if persistent.ssg_107 == True:
            ssg.image("samantha/image/sam_leaving.jpg")
        
        ssg.button("ssg_108")
        if persistent.ssg_108 == True:
            ssg.image("samantha/image/ss_carry1.jpg")
        
        ssg.button("ssg_109")
        if persistent.ssg_109 == True:
            ssg.image("samantha/image/ext_square_day_alpinist2.jpg")
        
        ssg.button("ssg_110")
        if persistent.ssg_110 == True:
            ssg.image("samantha/image/ss_playkiss1p.jpg")
        
        ssg.button("ssg_111")
        if persistent.ssg_111 == True:
            ssg.image("samantha/image/ss_playkiss2r.jpg")
        
        ssg.button("ssg_112")
        if persistent.ssg_112 == True:
            ssg.image("samantha/image/ss_playkiss3r.jpg")
            


    add "samantha/image/menu/s_menu_bag_open.jpg"
    add ssg.make_button("ssg_101", im.Scale("samantha/image/ss_tie.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=123, ypos=269)
    add ssg.make_button("ssg_102", im.Scale("samantha/image/menu/addcg/ss_tie2.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=123, ypos=530)
    add ssg.make_button("ssg_103", im.Scale("samantha/image/d1_snails.jpg", 350, 200), locked="samantha/image/menu/locked_special3.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=123, ypos=791)
    add ssg.make_button("ssg_104", im.Scale("samantha/image/sweets.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=562, ypos=269)
    add ssg.make_button("ssg_105", im.Scale("samantha/image/menu/addcg/sweets2.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=562, ypos=530)
    add ssg.make_button("ssg_106", im.Scale("samantha/image/ss_d2window2.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=562, ypos=791)
    add ssg.make_button("ssg_107", im.Scale("samantha/image/sam_leaving.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1003, ypos=269)
    add ssg.make_button("ssg_108", im.Scale("samantha/image/ss_carry1.jpg", 350, 200), locked="samantha/image/menu/locked_special1.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1003, ypos=530)
    add ssg.make_button("ssg_109", im.Scale("samantha/image/ext_square_day_alpinist2.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1003, ypos=791)
    add ssg.make_button("ssg_110", im.Scale("samantha/image/ss_playkiss1p.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1441, ypos=269)
    add ssg.make_button("ssg_111", im.Scale("samantha/image/ss_playkiss2r.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1441, ypos=530)
    add ssg.make_button("ssg_112", im.Scale("samantha/image/ss_playkiss3r.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1441, ypos=791)
    add "samantha/image/menu/frames.png"

            
            

    
    
    if _preferences.language == None:
    
        textbutton [" В МЕНЮ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["   ОБЛИК"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["   МЕСТА"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton [" СОБЫТИЯ"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3") 

        textbutton [" ДАЛЬШЕ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_4")   

    else:
        textbutton ["   MENU"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["  PERSON"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["  PLACES"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton ["  EVENTS"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3") 

        textbutton ["   NEXT"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_4") 
        
        textbutton ["?"]:
            style "ssbtn"
            text_style "sstxtyel"
            xpos 40
            ypos 80
            action Jump("ss_open_gal")            
           
screen ssg_gallery_4:
    tag menu
    modal True
    python:

    
        ssg.button("ssg_113")                                       
        if persistent.ssg_113 == True:                     
            ssg.image("samantha/image/menu/addcg/moonstars2.jpg")   
        
        ssg.button("ssg_114")
        if persistent.ssg_113 == True:
            ssg.image("samantha/image/menu/addcg/moonstars1.jpg")
        
        ssg.button("ssg_115")
        if persistent.ssg_115 == True:
            ssg.image("samantha/image/ss_d8barrel.jpg")
        
        ssg.button("ssg_116")
        if persistent.ssg_116 == True:
            ssg.image("samantha/image/ss_d8tolyan.jpg")
        
        ssg.button("ssg_117")
        if persistent.ssg_117 == True:
            ssg.image("samantha/image/ss_d8barrel4.jpg")
        
        ssg.button("ssg_118")
        if persistent.ssg_118 == True:
            ssg.image("samantha/image/ss_din_hug.jpg")
        
        ssg.button("ssg_119")
        if persistent.ssg_119 == True:
            ssg.image("samantha/image/ss_leave_hug.jpg")
        
        ssg.button("ssg_120")
        if persistent.ssg_120 == True:
            ssg.image("samantha/image/ss_ct_wall.jpg")
        
        ssg.button("ssg_121")
        if persistent.ssg_121 == True:
            ssg.image("samantha/image/ss_ct_lookback.jpg")
        
        ssg.button("ssg_122")
        if persistent.ssg_122 == True:
            ssg.image("samantha/image/ss_ct_smile.jpg")
        
        ssg.button("ssg_123")
        if persistent.ssg_123 == True:
            ssg.image("samantha/image/ss_ct_kiss.jpg")
        
        ssg.button("ssg_124")
        if persistent.ssg_124 == True:
            ssg.image("samantha/image/ss_ct_sleep.jpg")
            


    add "samantha/image/menu/s_menu_bag_open.jpg"
    add ssg.make_button("ssg_113", im.Scale("samantha/image/menu/addcg/moonstars2.jpg", 350, 200), locked="samantha/image/menu/locked_special5.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/emp.png",  xpos=123, ypos=269)
    add ssg.make_button("ssg_114", im.Scale("samantha/image/menu/addcg/moonstars1.jpg", 350, 200), locked="samantha/image/menu/locked_special5.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/emp.png",  xpos=123, ypos=530)
    add ssg.make_button("ssg_115", im.Scale("samantha/image/ss_d8barrel.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=123, ypos=791)
    add ssg.make_button("ssg_116", im.Scale("samantha/image/ss_d8tolyan.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=562, ypos=269)
    add ssg.make_button("ssg_117", im.Scale("samantha/image/ss_d8barrel4.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=562, ypos=530)
    add ssg.make_button("ssg_118", im.Scale("samantha/image/ss_din_hug.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=562, ypos=791)
    add ssg.make_button("ssg_119", im.Scale("samantha/image/ss_leave_hug.jpg", 350, 200), locked="samantha/image/menu/locked_special2.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1003, ypos=269)
    add ssg.make_button("ssg_120", im.Scale("samantha/image/ss_ct_wall.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1003, ypos=530)
    add ssg.make_button("ssg_121", im.Scale("samantha/image/ss_ct_lookback.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1003, ypos=791)
    add ssg.make_button("ssg_122", im.Scale("samantha/image/ss_ct_smile.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1441, ypos=269)
    add ssg.make_button("ssg_123", im.Scale("samantha/image/ss_ct_kiss.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1441, ypos=530)
    add ssg.make_button("ssg_124", im.Scale("samantha/image/ss_ct_sleep.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1441, ypos=791)
    add "samantha/image/menu/frames.png"

            
     

    
    
    if _preferences.language == None:
    
        textbutton [" В МЕНЮ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["   ОБЛИК"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["   МЕСТА"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton [" СОБЫТИЯ"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")  

        textbutton [" ДАЛЬШЕ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_5")   

    else:
        textbutton ["   MENU"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["  PERSON"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["  PLACES"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton ["  EVENTS"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")  

        textbutton ["   NEXT"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_5")
        
        textbutton ["?"]:
            style "ssbtn"
            text_style "sstxtyel"
            xpos 40
            ypos 80
            action Jump("ss_open_gal") 
            
screen ssg_gallery_5:
    tag menu
    modal True
    python:


         
        ssg.button("ssg_125")                                       
        if persistent.ssg_125 == True:                      
            ssg.image("samantha/image/ss_ct_what.jpg")   
        
        ssg.button("ssg_126")
        if persistent.ssg_126 == True:
            ssg.image("samantha/image/explosion_of_feathers.jpg")
        
        ssg.button("ssg_127")
        if persistent.ssg_127 == True:
            ssg.image("samantha/image/ss_dv_firep.jpg")
        
        ssg.button("ssg_128")
        if persistent.ssg_128 == True:
            ssg.image("samantha/image/menu/addcg/dv_cave.jpg")
        
        ssg.button("ssg_129")
        if persistent.ssg_129 == True:
            ssg.image("samantha/image/dv_cave_flash.jpg")
        
        ssg.button("ssg_130")
        if persistent.ssg_130 == True:
            ssg.image("samantha/image/ss_wagonlight.jpg")
        
        ssg.button("ssg_131")
        if persistent.ssg_131 == True:
            ssg.image("samantha/image/foxy_big.jpg")
        
        ssg.button("ssg_132")
        if persistent.ssg_132 == True:
            ssg.image("samantha/image/Dreamcatcher.jpg")
        
        ssg.button("ssg_133")
        if persistent.ssg_133 == True:
            ssg.image("samantha/image/big_balcony_sam3.jpg")      
            
        ssg.button("ssg_134")
        if persistent.ssg_134 == True:
            ssg.image("samantha/image/owlnest.jpg")
            
        ssg.button("ssg_135")
        if persistent.ssg_135 == True:
            ssg.image("samantha/image/truthful_letter.jpg")
            
        ssg.button("ssg_136")
        if persistent.ssg_136 == True:
            ssg.image("samantha/image/dead_gustav.jpg")            
            
    add "samantha/image/menu/s_menu_bag_open.jpg"
    add ssg.make_button("ssg_125", im.Scale("samantha/image/ss_ct_what.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=123, ypos=269)
    add ssg.make_button("ssg_126", im.Scale("samantha/image/explosion_of_feathers.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=123, ypos=530)
    add ssg.make_button("ssg_127", im.Scale("samantha/image/ss_dv_firep.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=123, ypos=791)
    add ssg.make_button("ssg_128", im.Scale("samantha/image/menu/addcg/dv_cave.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=562, ypos=269)
    add ssg.make_button("ssg_129", im.Scale("samantha/image/dv_cave_flash.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=562, ypos=530)
    add ssg.make_button("ssg_130", im.Scale("samantha/image/ss_wagonlight.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=562, ypos=791)
    add ssg.make_button("ssg_131", im.Scale("samantha/image/foxy_big.jpg", 350, 200), locked="samantha/image/menu/locked_special4.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1003, ypos=269)
    add ssg.make_button("ssg_132", im.Scale("samantha/image/Dreamcatcher.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1003, ypos=530)
    add ssg.make_button("ssg_133", im.Scale("samantha/image/big_balcony_sam3.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1003, ypos=791)
    add ssg.make_button("ssg_134", im.Scale("samantha/image/owlnest.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1441, ypos=269)
    add ssg.make_button("ssg_135", im.Scale("samantha/image/truthful_letter.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1441, ypos=530)
    add ssg.make_button("ssg_136", im.Scale("samantha/image/dead_gustav.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1441, ypos=791)

    
    #    add "samantha/image/menu/locked.png" ypos 791 xpos 1003
#    add "samantha/image/menu/locked.png" ypos 269 xpos 1441
#    add "samantha/image/menu/locked.png" ypos 530 xpos 1441
#    add "samantha/image/menu/locked.png" ypos 791 xpos 1441 
    add "samantha/image/menu/frames.png"


    
    
    if _preferences.language == None:
    
        textbutton [" В МЕНЮ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["   ОБЛИК"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["   МЕСТА"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton [" СОБЫТИЯ"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")  

        textbutton [" ДАЛЬШЕ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_7")  

    else:
        textbutton ["   MENU"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["  PERSON"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["  PLACES"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton ["  EVENTS"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")  

        textbutton ["   NEXT"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_7")
            
        textbutton ["?"]:
            style "ssbtn"
            text_style "sstxtyel"
            xpos 40
            ypos 80
            action Jump("ss_open_gal")             
    
#########################################
screen ssg_gallery_7:
    tag menu
    modal True
    python:


         
        ssg.button("ssg_137")                                       
        if persistent.ssg_137 == True:                      
            ssg.image("samantha/image/millroof_sofa.jpg")   
        
        ssg.button("ssg_138")
        if persistent.ssg_138 == True:
            ssg.image("samantha/image/attic_normal.jpg")
        
        ssg.button("ssg_139")
        if persistent.ssg_139 == True:
            ssg.image("samantha/image/pesochek.jpg")
        
        ssg.button("ssg_140")
        if persistent.ssg_140 == True:
            ssg.image("samantha/image/sudden_truck_of_death.jpg")
        
        ssg.button("ssg_141")
        if persistent.ssg_141 == True:
            ssg.image("samantha/image/metro_meeting.jpg")
        
        ssg.button("ssg_142")
        if persistent.ssg_142 == True:
            ssg.image("samantha/image/window_plane.jpg")
            
        ssg.button("ssg_143")
        if persistent.ssg_142 == True:
            ssg.image("samantha/image/window_plane2.jpg") 

        ssg.button("ssg_144")
        if persistent.ssg_144 == True:
            ssg.image("samantha/image/unacceptable_stuff.jpg")            
            
    add "samantha/image/menu/s_menu_bag_open.jpg"
    add ssg.make_button("ssg_137", im.Scale("samantha/image/millroof_sofa.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=123, ypos=269)
    add ssg.make_button("ssg_138", im.Scale("samantha/image/attic_normal.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=123, ypos=530)
    add ssg.make_button("ssg_139", im.Scale("samantha/image/pesochek.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=123, ypos=791)
    add ssg.make_button("ssg_140", im.Scale("samantha/image/sudden_truck_of_death.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=562, ypos=269)
    add ssg.make_button("ssg_141", im.Scale("samantha/image/metro_meeting.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=562, ypos=530)
    add ssg.make_button("ssg_142", im.Scale("samantha/image/window_plane.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=562, ypos=791)
    add ssg.make_button("ssg_143", im.Scale("samantha/image/window_plane2.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1003, ypos=269)
    add ssg.make_button("ssg_144", im.Scale("samantha/image/unacceptable_stuff.jpg", 350, 200), locked="samantha/image/menu/locked.png", hover_border = "samantha/image/menu/hover_border.png", idle_border = "samantha/image/menu/idle_border_events.png",  xpos=1003, ypos=530)

    
    add "samantha/image/menu/frames4.png"
    

    
    if _preferences.language == None:
    
        textbutton [" В МЕНЮ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["   ОБЛИК"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["   МЕСТА"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton [" СОБЫТИЯ"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")  

        textbutton [" ДАЛЬШЕ"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_3")  

    else:
        textbutton ["   MENU"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 110
            ypos 60
            action Jump("sam_back")
        
        textbutton ["  PERSON"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 500
            ypos 60
            action Show("ssg_gallery_sprites")
        
        textbutton ["  PLACES"]:
            style "ssbtn75"
            text_style "sstxt75"
            xpos 830
            ypos 60
            action Show("ssg_gallery")

        textbutton ["  EVENTS"]:
            style "ssbtn75"
            text_style "sstxt75blue"
            xpos 1160
            ypos 60
            action Show("ssg_gallery_3")  

        textbutton ["   NEXT"]:
            style "ssbtn60"
            text_style "sstxt60"
            xpos 1620
            ypos 60
            action Show("ssg_gallery_3")
            
        textbutton ["?"]:
            style "ssbtn"
            text_style "sstxtyel"
            xpos 40
            ypos 80
            action Jump("ss_open_gal") 
#########################################    
    
label ss_open_gal:
    scene s_menu_bag_open 
    menu:
        "Open the whole gallery right now?"
        "YES":
            $ persistent.ssg_86 = True
            $ persistent.ssg_87 = True
            $ persistent.ssg_88 = True
            $ persistent.ssg_89 = True
            $ persistent.ssg_90 = True
            $ persistent.ssg_91 = True            
            $ persistent.ssg_92 = True
            $ persistent.ssg_93 = True 
            $ persistent.ssg_94 = True 
            $ persistent.ssg_95 = True 
            $ persistent.ssg_96 = True 
            $ persistent.ssg_97 = True 
            $ persistent.ssg_98 = True
            $ persistent.ssg_99 = True
            $ persistent.ssg_101 = True 
            $ persistent.ssg_102 = True 
            $ persistent.ssg_103 = True 
            $ persistent.ssg_104 = True 
            $ persistent.ssg_105 = True 
            $ persistent.ssg_106 = True 
            $ persistent.ssg_107 = True 
            $ persistent.ssg_108 = True 
            $ persistent.ssg_109 = True 
            $ persistent.ssg_110 = True 
            $ persistent.ssg_111 = True 
            $ persistent.ssg_112 = True 
            $ persistent.ssg_113 = True 
            $ persistent.ssg_114 = True 
            $ persistent.ssg_115 = True 
            $ persistent.ssg_116 = True 
            $ persistent.ssg_117 = True 
            $ persistent.ssg_118 = True 
            $ persistent.ssg_119 = True 
            $ persistent.ssg_120 = True 
            $ persistent.ssg_121 = True 
            $ persistent.ssg_122 = True 
            $ persistent.ssg_123 = True 
            $ persistent.ssg_124 = True 
            $ persistent.ssg_125 = True 
            $ persistent.ssg_126 = True 
            $ persistent.ssg_127 = True 
            $ persistent.ssg_128 = True 
            $ persistent.ssg_129 = True 
            $ persistent.ssg_130 = True 
            $ persistent.ssg_131 = True
            $ persistent.ssg_132 = True
            $ persistent.ssg_133 = True
            $ persistent.ssg_134 = True
            $ persistent.ssg_135 = True
            $ persistent.ssg_136 = True
            $ persistent.ssg_137 = True
            $ persistent.ssg_138 = True
            $ persistent.ssg_139 = True
            $ persistent.ssg_140 = True
            $ persistent.ssg_141 = True
            $ persistent.ssg_142 = True
            $ persistent.ssg_143 = True
            $ persistent.ssg_144 = True             
            $ persistent.ssg_1 = True
            $ persistent.ssg_2 = True
            $ persistent.ssg_3 = True            
            $ persistent.ssg_4 = True 
            $ persistent.ssg_5 = True 
            $ persistent.ssg_6 = True 
            $ persistent.ssg_7 = True 
            $ persistent.ssg_8 = True 
            $ persistent.ssg_9 = True 
            $ persistent.ssg_10 = True
            $ persistent.ssg_11 = True 
            $ persistent.ssg_12 = True 
            $ persistent.ssg_13 = True 
            $ persistent.ssg_14 = True 
            $ persistent.ssg_15 = True 
            $ persistent.ssg_16 = True 
            $ persistent.ssg_17 = True 
            $ persistent.ssg_18 = True 
            $ persistent.ssg_19 = True  
            $ persistent.ssg_20 = True 
            $ persistent.ssg_21 = True 
            $ persistent.ssg_22 = True 
            $ persistent.ssg_23 = True
            $ persistent.ssg_24 = True            
            $ persistent.ssg_25 = True 
            $ persistent.ssg_26 = True 
            $ persistent.ssg_27 = True 
            $ persistent.ssg_28 = True 
            $ persistent.ssg_29 = True 
            $ persistent.ssg_30 = True
            $ persistent.ssg_31 = True            
            
        "No, let's play fair":
            menu:
                "Good choice! Thank you!"
                "Don't thank me, I've probably clicked YES before...":
                    menu:
                        "So maybe you want to refresh the gallery?"
                        "(This will lock it again)"
                        "Yes, please":
                            $ persistent.ssg_86 = None 
                            $ persistent.ssg_87 = None 
                            $ persistent.ssg_88 = None 
                            $ persistent.ssg_89 = None 
                            $ persistent.ssg_90 = None
                            $ persistent.ssg_91 = None                        
                            $ persistent.ssg_92 = None
                            $ persistent.ssg_93 = None 
                            $ persistent.ssg_94 = None 
                            $ persistent.ssg_95 = None 
                            $ persistent.ssg_96 = None 
                            $ persistent.ssg_97 = None 
                            $ persistent.ssg_98 = None
                            $ persistent.ssg_99 = None
                            $ persistent.ssg_101 = None 
                            $ persistent.ssg_102 = None 
                            $ persistent.ssg_103 = None 
                            $ persistent.ssg_104 = None 
                            $ persistent.ssg_105 = None 
                            $ persistent.ssg_106 = None 
                            $ persistent.ssg_107 = None 
                            $ persistent.ssg_108 = None 
                            $ persistent.ssg_109 = None 
                            $ persistent.ssg_110 = None 
                            $ persistent.ssg_111 = None 
                            $ persistent.ssg_112 = None 
                            $ persistent.ssg_113 = None 
                            $ persistent.ssg_114 = None 
                            $ persistent.ssg_115 = None 
                            $ persistent.ssg_116 = None 
                            $ persistent.ssg_117 = None 
                            $ persistent.ssg_118 = None 
                            $ persistent.ssg_119 = None 
                            $ persistent.ssg_120 = None 
                            $ persistent.ssg_121 = None 
                            $ persistent.ssg_122 = None 
                            $ persistent.ssg_123 = None 
                            $ persistent.ssg_124 = None 
                            $ persistent.ssg_125 = None 
                            $ persistent.ssg_126 = None 
                            $ persistent.ssg_127 = None 
                            $ persistent.ssg_128 = None 
                            $ persistent.ssg_129 = None 
                            $ persistent.ssg_130 = None 
                            $ persistent.ssg_131 = None
                            $ persistent.ssg_132 = None
                            $ persistent.ssg_133 = None
                            $ persistent.ssg_134 = None
                            $ persistent.ssg_135 = None
                            $ persistent.ssg_136 = None
                            $ persistent.ssg_137 = None
                            $ persistent.ssg_138 = None
                            $ persistent.ssg_139 = None
                            $ persistent.ssg_140 = None
                            $ persistent.ssg_141 = None
                            $ persistent.ssg_142 = None
                            $ persistent.ssg_143 = None 
                            $ persistent.ssg_144 = None                             
                            $ persistent.ssg_1 = None
                            $ persistent.ssg_2 = None
                            $ persistent.ssg_3 = None            
                            $ persistent.ssg_4 = None 
                            $ persistent.ssg_5 = None 
                            $ persistent.ssg_6 = None 
                            $ persistent.ssg_7 = None 
                            $ persistent.ssg_8 = None 
                            $ persistent.ssg_9 = None 
                            $ persistent.ssg_10 = None
                            $ persistent.ssg_11 = None 
                            $ persistent.ssg_12 = None 
                            $ persistent.ssg_13 = None 
                            $ persistent.ssg_14 = None 
                            $ persistent.ssg_15 = None 
                            $ persistent.ssg_16 = None 
                            $ persistent.ssg_17 = None 
                            $ persistent.ssg_18 = None 
                            $ persistent.ssg_19 = None  
                            $ persistent.ssg_20 = None 
                            $ persistent.ssg_21 = None 
                            $ persistent.ssg_22 = None 
                            $ persistent.ssg_23 = None 
                            $ persistent.ssg_24 = None                             
                            $ persistent.ssg_25 = None
                            $ persistent.ssg_26 = None 
                            $ persistent.ssg_27 = None 
                            $ persistent.ssg_28 = None 
                            $ persistent.ssg_29 = None 
                            $ persistent.ssg_30 = None
                            $ persistent.ssg_31 = None                             
                        "No, I'm fine":
                            pass
                "Yup, bye!":
                    pass 
   
    call screen ssg_gallery
    
  
    

                              