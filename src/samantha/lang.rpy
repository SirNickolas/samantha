# Support for {en}, {ru}, and {enen} tags.
# Requires Ren'Py 7.0+ (Everlasting Summer 1.3+).
# IMPORTANT: You cannot have {w}, {nw}, {p}, or {fast} inside {en} or {ru}.

# This is because Ren'Py handles them specially, prior to custom tags.

init python:
    def ss_en_tag(tag, argument, contents):
        return contents if persistent.ss_text_lang == "en" else [ ]

    def ss_ru_tag(tag, argument, contents):
        if persistent.ss_text_lang != "ru":
            return [ ]
        result = [(renpy.TEXT_TAG, persistent.ss_text_style_ru)]
        result += contents
        result.append((renpy.TEXT_TAG, "/color"))
        return result

    def ss_enen_tag(tag, argument, contents):
        result = [(renpy.TEXT_TAG, persistent.ss_text_style_enen)]
        result += contents
        result.append((renpy.TEXT_TAG, "/color"))
        return result

    config.custom_text_tags["en"] = ss_en_tag
    config.custom_text_tags["ru"] = ss_ru_tag
    config.custom_text_tags["enen"] = ss_enen_tag

    def ss_parse_legacy_lang_color(color, default):
        if color == "Grey":
            return "CED1C2"
        elif color == "Green":
            return "ACDC13"
        elif color == "Purple":
            return "B3A5FF"
        elif color == "Blue":
            return "8EC8FF"
        elif color == "Pink":
            return "FFBAD9"
        elif color == "White":
            return "FFF5C6"
        elif color == "Default":
            return "FFDF89"
        elif color == "tw":
            return "FFFFFF"
        elif color == "pb":
            return "3968AC"
        elif color == "pg":
            return "246B43"
        elif color == "vdb":
            return "5B4224"
        elif color == "sg":
            return "6C8822"
        elif color == "mb":
            return "484850"
        elif color == "iy":
            return "DFB721"
        elif color == "ac":
            return "9A3724"
        else:
            return default

    if persistent.ss_text_lang is None:
        # Migrate from LolBot's i18n system.
        persistent.ss_text_lang = "ru" if persistent.text_tag_lang == "ru" else "en"

    if persistent.ss_text_style_ru is None:
        persistent.ss_text_style_ru = "color=#" + ss_parse_legacy_lang_color(
            persistent.ss_ru_color, "CED1C2",
        )
        try:
            del persistent.ss_ru_color
        except AttributeError:
            pass

    if persistent.ss_text_style_enen is None:
        persistent.ss_text_style_enen = "color=#" + ss_parse_legacy_lang_color(
            persistent.ss_eng_color, "ACDC13",
        )
        try:
            del persistent.ss_eng_color
        except AttributeError:
            pass


label ss_check_text_lang:
    $ tmp_text_tag_lang = "русский" if persistent.ss_text_lang == "ru" else "английский"
    menu:
        "По сюжету часть бесед в моде проходит на английском."
        "В этом меню можно включить перевод (будет выделен {color=#CED1C2}цветом{/color})."
        "{color=#DCFFD6}Сейчас в диалогах с Самантой используется %(tmp_text_tag_lang)s язык.{/color}"
        "..."
#        "Сейчас при наличии перевода используется %(tmp_text_tag_lang)s язык."
        "OK, оставить!":
            pass
        "НЕТ, поменять!":
            $ persistent.ss_text_lang = "ru" if persistent.ss_text_lang != "ru" else "en"
    return


label ss_check_text_lang2:
    $ tmp_text_tag_lang = "русский" if persistent.ss_text_lang == "ru" else "английский"
    menu:
        "{color=#DCFFD6}Сейчас в диалогах с Самантой используется %(tmp_text_tag_lang)s язык.{/color}"
        "Далее язык можно будет поменять в настройках мода."
        "OK, оставить текущий язык":
            pass
        "НЕТ, поменять":
            $ persistent.ss_text_lang = "ru" if persistent.ss_text_lang != "ru" else "en"
    return


label ss_detect_text_lang_conflicts_0:
    "{en}Nya,{/en}{ru}Ня,{/ru} {enen}definitely nya!{/enen}"


label ss_detect_text_lang_conflicts:
    python:
        try:
            tmp_conflict = renpy.game.script.lookup("ss_detect_text_lang_conflicts_0").block[0].block[0].what != "{en}Nya,{/en}{ru}Ня,{/ru} {enen}definitely nya!{/enen}"
        except Exception:
            tmp_conflict = False
    if tmp_conflict:
        stop music fadeout 2
        "Внимание! Обнаружен конфликт модов. Поздравляем, это очень редкий конфликт! Проверялка конфликтов была создана именно на такой невообразимый случай, и не зря!\nВ чём суть: с некоторых пор «Саманта» использует новую систему перевода реплик. При этом старая система, предложенная в своё время Лолботом, могла в каком-то виде сохраниться в других модах. Вероятнее всего, как раз в сиквелах «Саманты».\nЧто можно сделать: отписаться от таких модов (в Стиме) либо удалить их вручную, чтобы больше не получать это сообщение.\nЛибо: поискать в папках модов файлы lb_lang.rpy и lb_lang.rpyc и удалить только их (с присутствием «Саманты» функциональность тех модов не нарушится).\nЛибо: не делать ничего, но в таком случае не будут работать некоторые настройки «Саманты». При этом «Бесконечное лето» может запускаться существенно дольше."
        "Спасибо за внимание, и назад к игре!"
        play music music_list["a_promise_from_distant_days_v2"] fadein 3

        nvl clear
    return

#{color=#FFD6DD}РАЗРАБОТЧИКА{/color} {/color} CEDBFF CED1C2 CED1E1
