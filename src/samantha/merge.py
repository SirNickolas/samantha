import re

en = open("sameng.rpy.txt", "r").readlines()
ru = open("samrus.rpy.txt", "r").readlines()
z = zip(en,ru)

out = open("sam_merged.rpy", "w")

reg = re.compile(ur'^(.*?){i}(.*){/i}(.*)$', flags=re.UNICODE| re.DOTALL )

for i,(e,r) in enumerate(z):
    if  e == r:
        out.write(e)
    else:
        m = reg.match(r)
        if  m and e.startswith(m.group(1)) and e.endswith(m.group(3)):
            te = "{en}" + e[len(m.group(1)):-len(m.group(3))] + "{/en}"
            tr = "{ru}" + m.group(2) + "{/ru}"
            out.write(m.group(1) + te + tr + m.group(3))
        else:
            print i+1, e, r, m.group(1), m.group(3), "---"
            out.write("#FIXME LINE %d\n" % i+1)