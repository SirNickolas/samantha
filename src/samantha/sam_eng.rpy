﻿
    
label ss_d1_eng:

    $ new_chapter(1, u"Samantha: day one")
    $ day_time()
    $ persistent.sprite_time = "day"
    scene black with fade2
    $ renpy.pause (1.5)    
    play music  music_list["everyday_theme"] fadein 7
    scene bg ext_house_of_mt_sunset with fade
    play ambience ambience_day_countryside_ambience fadein 3 
    $ renpy.pause (1)
    th "What a beautiful morning...{w} to spend it in the house." 
    scene bg int_house_of_mt_day with dissolve  
    th "Too many adventures in the last few days. Too many events. Too much commotion all around, really."
    th "Though I did pretty well. Good old Semyon could pretend to be one of them. {w}But am I really one of them?"
    th "Life was so different before. {w} People don't change that easily. Not in my age, anyway."
    window hide
    show cg d2_mirror with dissolve
    $ renpy.pause (1)
    th "Still can't get used to it! {w}Well, yes, something HAS changed... Whatever!"
    hide cg d2_mirror with dissolve
    th "No, I'd say Semyon takes a day off. Unless this Olga Dmitrievna will find me another job..."
    th "It would be nice at least to disappear unnoticed."
    scene bg ext_house_of_mt_sunset 
    show mt smile panama pioneer at center     
    with dissolve
    mt "Already awake?"
    th "Did I ask for too much?"
    mt "Hey, do you know English? I wonder..."
    th "Of course I know English. Raised by the Western culture, proud member of the internet..."
    th "Though why she wants to know this is beyond me. Another chore perhaps? I can feel it in my bones."
    window hide

    menu:
        "Yes, I do":
            window show
            th "Let her consider me a bit less worthless. I'll only translate a word on a shampoo for her and that's it."
            th "However, local shampoos have only the \"Shampoo\" inscription."
            me "Yes, I picked up some English."
            show mt normal panama pioneer at center     with dspr

        "I suppose I kind of do":
            window show
            th "Let's be honest. My knowledge is limited to internet slang anyway. {w} Well, mostly."
            me "If pretty bad English is good enough, then yes."

        "No, I do not":
            window show
            th "It's better to lie now and keep low. I don't want to be stuck helping some D-graders, or something like that. "
            show mt angry panama pioneer close with dspr
            mt "I doubt that, young man.{w} I saw your electronic game which is in English. {w}Listen, it's really important and urgent. And I'm unable to find anybody else. So?"
            me "...What is it about?"
            mt "What is this about..."
            me "... ma'am?"
            th "I clearly made her upset. I can't really go on lying like this."
            me "I guess I picked up some English here and there. Hard not to. It's rusty at best, but it may do, depending on what you have on your mind."

    show mt smile panama pioneer  with dspr
    mt "That's good! You are our savior. {w}One person is coming to our camp...You're not going to believe it...{w}Samantha Smith!"
    "That name sounds familiar, but I still can't figure out who she's talking about. But Olga Dmitrievna announced it with such importance... I'll better portray surprise here."
#    show sslut at center   with dissolve2
    "At least I remembered another Samantha, a slutty dame from the \"Sex and the City\" series. Such an important person! Brought sex to the USSR. I tried not to giggle."
#    hide sslut 
    mt "What is it?"
    "It was getting harder and harder not to laugh. It felt like my face was inflating like a big red bubble. Which can be burst in any minute. I can't laugh now, can I?"
    "I went away to the bushes and sneezed several times." with hpunch
    "It seemed like that it wasn't very convincing."
    mt "Bless you..."
    "But she was too fascinated by her story to concentrate on my strange behavior."
    mt "...Well then. Last time, as you know, Samantha was in the Artek. But she was followed by the crowd there, reporters, and all that. And she lived there not for long. This year she is visiting us, can you imagine?"
    mt "They picked such a quiet place on purpose, she won't be bothered here. A simple pioneer's life. Even her translator should be one of us."
    mt "Alas the girl we picked for translating...she fell ill yesterday with a sore throat."
    mt "So congratulations, Semyon. From now on - you are an important person in the diplomatic world. At least until we find a replacement."
    me "But I haven't agreed yet. And I don't know the language that well."
    show mt angry panama pioneer close with dspr
    mt "You're a pioneer, Semyon. And this is for you - the most important pioneer task ever."
    me "I don't deserve such a responsibility. Thank you for your trust, but ..."
    show mt angry panama pioneer  with dspr
    mt "You can not refuse. If you refuse today, refuse tomorrow, if you stay like this... One day you'll stop being a human! You will become an animal!"
    th "An animal? Her analogy was lacking - after all, I was choosing not to accept this task, yet animals could not choose. Therefore, what I was doing technically was making me superior to animals."
    th "But then again, I could see her face changing between a questioning and all-out enraged look, which made me not want to disobey her."
    me "Sure, I get your point. If there is nobody else up to the task, I'll take it, of course."
    show mt smile panama pioneer 
    mt "That's the spirit!"
    th "And I wanted to stay indoors all day and relax for a while... Alright then, there was no way to escape."
    mt "It's time to meet the guest. Everything will be fine. You'll like it. And everyone likes you."
    stop ambience fadeout 2
    th "What is she talking about? She's even winking at me, odd."

    scene bg ext_no_bus with dissolve
    play ambience ambience_camp_entrance_day_people fadein 2
    "Some pioneers were already at the bus stop. Well, I was still here as well, not back home, but also not dead. I don't belong here, though."
    "Or do I belong here? I figure I should put my daily existential crisis off until later."
    "And there was a rumbling in my stomach, but there won't be any breakfast today until the arrival of the guest of honor."
    "In the crowd, I could see a few familiar faces."
    window hide

    menu:
        "Approach Slavya":
            window show
            $ sp_sl = (sp_sl) + (1)
            show sl normal pioneer  with dspr
            sl "Good morning."
            me "Morning."
            sl "So exiting! I've never spoken with a foreigner before. Well, except Miku. Have you?"
            me "Not in person. But I've had some... pen pals."
            "I wanted to say 'online', but promptly managed to correct myself."
            show sl happy pioneer  with dspr
            sl "Great. Who have you been writing to? Americans?"
            me "To different people. Poles, Germans."
            sl "That's great. Our nations were at war not so long ago, but we are friends now. Way to go!"
            me "Yeah."
            sl "And what are the Germans and Poles keen to discuss?"
            me "Politics, history, and humor."
            th "This conversation needed to end. Luckily, I could hear someone shouting \"they're coming\", drawing their attention."

        "Come to Alisa and Ulyana":
            window show
            show dv normal pioneer at cleft   
            show us normal pioneer at cright  
            with dissolve
            "On my approach, Ulyana hastily hid something oblong behind her back."
            me "What's in there? Preparing to provoke a nuclear war?"
            show us angry pioneer far at right with  dspr
            "Ulyana shot her tongue out and ran away."
            hide us with dspr
            dv "Aren't you afraid of me also? I might provoke something, too."
            th "It would be the proper finish of her rebellious career."
            window hide

            menu:
                "There is more common sense in you than you're trying to show":
                    window show
                    show dv angry pioneer close 
                    dv "My common sense tells me that Viola will use up her yearly supply of bandages just for you."
                    $ sp_dv = (sp_dv) + (1)
                    th "There would be no use for me as a cripple. I should better go back to the Olga."

                "I can't stop you in this":
                    window show
                    show dv smile pioneer 
                    dv "You got that right."
                    me "Just warn me first, please. So I can reach a bomb shelter in time."
                    dv "If you're going to behave."
                    window hide

        "Stay here":
            window show
            show mt normal panama pioneer 
            mt "They're running late."
            "She continued to whisper something for herself, bending her fingers. She's not up to talking right now."

    scene bg ext_bus with dissolve
    stop music fadeout 3    
    play sound sfx_ikarus_arrive
    "Finally, the bus had shown up - a pretty fancy \"Icarus\". It was already a crowd here - every single soul in the camp. There was even a small orchestra led by Miku."
    play music  music_list["miku_song_flute"] fadein 4  
    play sound sfx_ikarus_open_doors
    "Frankly, I also was captivated by the mutual excitement. Show us this American wonder already!"
    window hide
    
    $ persistent.ssg_99 = True 
         
    show ss arriv far    
    with dissolve
    window show
    "And then, a girl of about thirteen appeared. On the steps of the bus she said \"Privet!\" and waved to everyone."
    window hide
    $ renpy.pause (0.6) 
    show ss hardsmile arriv with dspr
    window show
    "A real American, without any doubt. Even that typical pioneer hat looked somewhat unusual on her, not to mention the other clothes."
    "They took some photos. Samantha smiled, but looked a bit tired. They probably drove all night."
    show ss normal arriv with dspr
    "Olga also noticed and cut the greeting procedure before they came up to me." 
    "However, this did not finish the ceremony. Not a single day without line-up." 
    scene cg d2_lineup with dissolve    
    "Usually Olga tended to stretch the morning speech for her pleasure, but today she appeared to be in a hurry."
    "The pioneers had not lined up properly, they actually were huddled together so this wasn't even a lineup - this day no one cared."
    "Taking advantage of the situation, I took the chance to vanish in the crowd. I was not in the mood to translate for her yet."
    "My past experience with celebrities came down to a meeting with the famous \"daddy\" in one of my Counter-Strike matches."
    "\"Roger that\", - I told him. And he answered - \"Affirmative\"."
    scene bg ext_square_day
    show mt normal panama pioneer
    with dissolve
    mt "...So, guys, show your best qualities. There is a lot to learn, but good pioneers learn fast."   
    dv "And those who do not - will bite the dust."
    "I was not the only one who took the opportunity to hide behind the other pioneers."
    mt "...we are capable of teaching as well. But only the best things! And what are we best at?”"
    "There were a lot of opinions."
    us "To swim!"
    mi "To sing!"
    dv "To make toy ships..."
    mt "Uhm, what?" 
    show mt surprise panama pioneer
    dv "...And fit the bottle."   
    show mt angry panama pioneer  
    mt "To hit the bottle?! Is that you, Dvachevskaya?"
    dv "...It's not me!"
    mt "Where are you? If anything goes wrong, you'll go home! You'll go on foot!"
    $ renpy.pause (1)   
    show mt normal panama pioneer
    mt "Okay, guys ... you are all different, someone likes to be naughty, someone just likes to be by themselves ... But this time all should try their best." 
    mt "Let's show that we are better than Artek!"
    "At the word \"Artek\" rumble in the lines of disinterested pioneers changed to wary silence. The spirit of competition is strong."
    $ renpy.pause (1)   
    mt "Samantha, come to me now, please."
    "The girl nodded happily, and stepped forward."
    window hide
    show ss hardsmile arriv at cleft
    show mt smile panama pioneer at cright
    with dspr
    window show  
    th "It seems she's kind of used to it." 
    show ss unsure arriv at cleft   
    "Olga Dmitrievna pointed to Samantha's gorgeous jacket. The girl was confused a bit - life is hard without a translator!"
    show ss nosmile arriv at cleft  
    "Finally realizing what they want from her, Samantha began to take off her tight jacket. Slavya appeared to help."
    window hide
    show sl smile pioneer far at fleft with dspr
    window show
    dv "She's gonna steal it!"
    window hide
    show sl angry pioneer far at fleft 
    show mt surprise panama pioneer at cright
    with dspr
    window hide
    $ renpy.pause (0.7) 
    scene cg ss_tie with dissolve
    
    $ persistent.ssg_101 = True 
          
    $ renpy.pause (1)
    window show
    "At last, everything was set to begin the ceremony.." 
    mt "We are greeting you! Let me put this tie where it belongs."
    mt "We symbolically accept you in our friendly family of \"Sovyonok\". Welcome!"
    all "Welcome!"
    ss "{enen}Thank you!{/enen}"
    th "It seems that I had to go there to translate. Not good..."
    th "Okay, okay, next time I will translate something. Don't let these Artek guys become conceited."   
    mt "And now...time to get breakfast!"
    window hide
    $ renpy.pause (1)   
    stop music fadeout 3
    scene bg int_dining_hall_people_day  with fade
    play ambience ambience_dining_hall_full
    show dv laugh pioneer far 
    dv "The food is better today. Look, even bananas! We really should invite more Americans."
    show us laugh pioneer far at right 
    us "Yeah, maybe I don't like guests, but I definitely tolerate oranges!"
    "The canteen was filled with joyful madness. Pioneers felt their power over the seniors who did not dare to raise a voice."
    window hide
    hide dv 
    hide us 
    with dspr   
    window show
    "Olga invited Samantha and Slavya to her table."
    th "Have they forgotten about me? I was about to offer my services... {w}Alright then! A bit disappointing, of course."
    show mt normal pioneer at fleft with dspr
    mt "Semyon, where are you hiding? Come here, sit down ..."
    th "Ashamed me, thank you very much. Okay, I can't really run away now. I've been through worse."
    window hide
    show ss nosmile tie arriv with dspr
    $ renpy.pause (0.3)     
    window show
    mt "This is Semen. Your interpreter."
    "Boom, headshot! I was puzzled how to present my ugly-sounded name, but Olga named me herself. {w}By the seminal fluid."
    th "...How can you even get this name? What am I - a breeding bull? That's how she might think?"
    ss "{enen}Hello.{/enen}"
    me "{enen}Hello. My name is Semyon, actually.{/enen}"
    window hide
    $ renpy.pause (0.5)
    menu:
        "Wait! What's about the colored text?":
            $ set_mode_nvl()
            "Highlighted text means that this phrase is intended to originally come in English (conversations between Samantha and Semyon mostly), while most phrases are intended to originally come in Russian (all other conversations)."
            "It may hint that sometimes, talking in a group, one person cannot understand what another person says unless someone translates it for them."
            "Though most group conversations include translation implicitly."
            $ set_mode_adv()
        
        "I'm fine, thanks!":
            pass
    
    window hide
    $ renpy.pause (0.5) 
    show ss softsmile tie arriv  with dspr
    "Samantha smiled at me kindly and went back to her fruit salad. Perhaps I overthought it a bit. Maybe she got used to the strange names of these barbarians from the East."
    mt "Ask her about the journey."
    me "{enen}How was the trip?{/enen}"
    show ss normal tie arriv    
    ss "{enen}Oh, great. Such beautiful places.{/enen}"
    me "It was good, she said. Very beautiful."
    show sl smile pioneer at fright with dspr
    sl "What is beauty in the dark?"
    "Acrimony from Slavya? What's wrong with her? Although she seems as friendly as always."
    me "Should I translate it?"
    mt "No!"
    show ss nosmile tie arriv  
    ss "{enen}I don't sleep so well on buses. And planes.{/enen}"
    th "It seems like she understands a lot by herself. {w}I found a good job here - no need to think about the words themselves, just pronouncing them. Easy communicating. With the girls."
    mt "You must be so tired, poor thing!"
    show ss normal tie arriv  
    ss "{enen}Not at all, I like traveling at night.{/enen}"
    me "{enen}Driving through the night is charming. Good music, neon city lights...{/enen}"
    window hide
    play music A_Day fadein 2
    show greysation with dissolve
    $ renpy.pause(2.0, hard=True)
    show night1:
        pos (0,0)
    $ renpy.pause(1.6, hard=True)       
    show night2:
        pos (960,0)
    $ renpy.pause(1.7, hard=True)
    show night3:
        pos (0,540)
    $ renpy.pause(1.7, hard=True)
    show night4:
        pos (960,540)
    $ renpy.pause(1.6, hard=True)       
    show night5:
        pos (0,0)
    $ renpy.pause(1.5, hard=True)   
    show night6:
        pos (960,0)
    $ renpy.pause(1.6, hard=True)   
    show night7:
        pos (0,540)
    $ renpy.pause(1.5, hard=True)   
    show night8:
        pos (960,540)       
    $ renpy.pause(1.0)
    mt "Semyon?" 
    hide greysation 
    hide night1
    hide night2 
    hide night3
    hide night4
    hide night5
    hide night6
    hide night7
    hide night8 
    with dissolve 
    stop music fadeout 7    
    me "{enen}I mean...{/enen}"
    show ss softsmile tie arriv 
    ss "{enen}Dark forests are great, too. Do you drive a car yourself?{/enen}"
    me "{enen}...Nope.{/enen}"
    show ss hardsmile tie arriv
    ss "{enen}What a couple of enthusiastic passengers we are!{/enen}"
    th "It's more about the films..."
    show ss softsmile tie arriv 
    ss "{enen}But soon you will be able to drive, right? How old are you?{/enen}"
    me "{enen}Twenty... Seventeen. Yes, seventeen.{/enen}"
    sl "You need to be eighteen to drive in our country. And in America?"
    show ss nosmile tie arriv  
    ss "{enen}It's sixteen, basically. But it depends on the state rules.{/enen}"
    "So we ate and had some small talk. Noticing that the glass of Samantha is empty, Slavya volunteered to go and fill it."
    window hide
    hide sl
    $ renpy.pause (1)
    show sl normal pioneer at fright with dspr  
    window show
    "After she returned, Slavya tried to put the glass on the table, but she somehow messed it up."
    "There was a fork underneath the glass, and this fork wasn't a proper support. It fell off the table..."
    play sound sfx_break_flashlight
    show mt sad pioneer at fleft 
    show sl scared pioneer at fright 
    show ss surprise tie arriv
    stop music fadeout 5
    "...sending part of its content on the Samantha's shirt."
    "After the necessary cleaning and lots of apologies, breakfast was concluded. With the spoiled mood of all four."
    play ambience ambience_camp_center_day
    scene bg ext_dining_hall_away_day  with fade
    play music music_list["afterword"] fadein 3
    "After breakfast we went to show Samantha her new home. On the way, we caught up with two others: the bus driver, loaded with Samantha's bags, and the woman who came with them."
    "The driver seemed dissatisfied. He gently hit his Adam's apple several times, which meant that he was sober for too long and keen to stop being so."
    "While they were discussing something with Olga Dmitrievna, we exchanged a few words with Samantha."
    show ss normal tie arriv with dspr
    ss "{enen}So, you are Sam. As I am. May I call you Sam?{/enen}"
    me "{enen}Sure.{/enen}"
    ss "{enen}I thought it should be the girl in your place. She sent me a letter.{/enen}"
    me "{enen}She's in GULAG now. KGB sent me instead.{/enen}"
    show ss surprise tie arriv 
    "Samantha looked at me surprised, but then laughed."
    show ss hardsmile tie arriv
    th "Good thing that she understood the joke, and not running screaming wildly towards the bus."
    show ss softsmile tie arriv
    me "{enen}She's got the flu and is not in the camp at the moment. Sorry.{/enen}"
    show ss nosmile tie arriv
    ss "{enen}She is a good girl, but a bit boring to be honest.{/enen}"
    $ renpy.pause (0.5) 
    scene bg ext_house_of_sl_day with dissolve
    
    $ persistent.ssg_99 = True 
    
    play sound sfx_open_door_2
    play ambience ambience_day_countryside_ambience
    "Olga settled the guest in the house on the left of our own. Someone had to move to make room for her."
    "We left Samantha for some time to unpack her bags and get some rest."
    show mt normal pioneer 
    me "Will she live by herself here?"
    mt "We'll have to see. For now, yes."
    scene bg int_house_of_mt_day  with dissolve
    show mt normal pioneer 

    mt "I have to ask you for something. At first I wanted her to go with Slavya, but it somehow... Never mind. You stay in the house for now, guarding Sam."
    me "In her house?"
    show mt angry pioneer 
    mt "In ours! She can't walk around without supervision. I need to go. There are other things to do."
    th "It's not like I planned outdoor activities, but just sit and wait... And what if she sleeps all day?"
    show mt normal pioneer 
    me "I thought walking freely is the thing. Without too much attention and control."
    mt "We are responsible for her. Besides, it will be more fun for her with company. And you've got an official reason - to translate."
    me "And what about Slavya? I think they would become friends easily."
    mt "I don't know. Well, you saw it. Slavya is in the center of attention usually. And here is the new girl. Maybe subconsciously..."
    me "Drop it. Slavya and jealousy are incompatible."
    mt "We'll not take any risk. Well, don't get bored. I will be around."
    hide mt with dspr
    "Left alone, I realized that our house is not much use as an observation post."
    play ambience ambience_day_countryside_ambience
    scene bg ext_house_of_mt_day  with dissolve
    "I went outside and began to walk back and forth."
    "There were pioneers passing by, carrying different implements. I tried to look pompous as I could, so they don't think that I'm slacking."
    show dv smile pioneer at center   with dspr
    dv "Hey, strengthening international relations?"
    th "Again with her jokes. Those can't hurt me though."
    me "I am."
    dv "How is she?"
    "I shrugged."
    me "Resting."
    show dv guilty pioneer at center with dspr
    dv "I see. Alright then."
    hide dv  with dissolve
    "Alisa had left. That's how I got rid of her. {w}And now I'm bored."
    "I sat in the lounge, but I felt sleepy. Those early wake-ups..."
    "I had no strength, not a particular reason to hold on, so I wrote a note, asking her to knock on my door if she wanted to go somewhere."
    play ambience ambience_int_cabin_day
    "I placed the note on her door, then went back into the house and jumped into the cool embrace of the bed."
    window hide
    scene black  with fade
    play sound sfx_knock_door2
    scene bg int_house_of_mt_day 
    show unblink


    "Knocking on the door woke me up.{w} I slept for a good three hours. And probably missed lunch."
    me "Just a minute!"
    play sound sfx_open_door_2
    "I tried to shake off the sleepiness and opened the door. There was Shurik on the doorstep."
    show sh normal pioneer with dspr
    sh "Hi... Olga Dmitrievna sent me so you could have some company. Bored here, huh?"
    me "Not at all. I've got... playing cards."
    sh "I see that the deck is sealed."
    show sh serious pioneer with dspr
    me "I'm on geometry studies. Just look at the desk. These planes will drive me crazy!"
    sh "Uhm... Yes, geometry is great."
    "There was a pause. It seemed like that Shurik was about to leave, but remembered something and whispered:"
    show sh scared pioneer with dspr
    sh "Hey, don't you think that she is a spy?!"
    me "I think she's Goodwill Ambassador. Exactly the opposite."
    sh "But it would be a great cover, would it not?"
    me "Definitely. I think it's time to see how our spy is doing."
    show sh upset pioneer with dspr
    "Shurik was upset because I didn't take his theory seriously."
    sh "It's dangerous to go alone. Take this."
    me "What's in the bag?"
    sh "Food. From Olga Dmitrievna. You missed lunch."
    hide sh with dspr
    show sh normal pioneer  with dspr
    me "Thank you. Coming with me?"
    stop music fadeout 5
    sh "No, I must get going. We're busy in the club."
    scene bg ext_house_of_sl_day  with dissolve
    play ambience ambience_day_countryside_ambience
    "I went after him and headed to the next door. The note had disappeared."
    th "Should I knock? And what if someone took the paper, or maybe the wind? But I can't wait - what if she left alone and is now in the urgent need of salvation from the Soviet children?"
    play sound sfx_knock_door2 
    "I'm knocking. At least there is a reason - the food."
    ss "{enen}Come in.{/enen}"
    window hide
    play ambience ambience_int_cabin_day    
    play music The_Cars_Drive fadein 1  
    scene cg d1_snails  with fade2
    
    $ persistent.ssg_103 = True 
          
    $ renpy.pause (1.0) 
    window show
    "Samantha sat on the bed surrounded by her things: fancy notebooks, boxes with some wrappers, souvenirs. There was even the real Japanese tape recorder, breaking the silence."
    "I was particularly fascinated by her nails, on which I shamelessly stared."
    me "{enen}Wow. Such a beauty.{/enen}"
    ss "{enen}Do you think so?{/enen}"
    scene cg d1_ssadnails with dissolve
    ss "{enen}I'm not gonna walk around the camp like this, of course. {w}It's just a glue with glitter. Easy to clean up.{/enen}"
    "Samantha began to brush away her manicure with a napkin."
    me "{enen}What a shame.{/enen}"
    scene cg d1_ssmile with dissolve    
    ss "{enen}No, it's okay, I've got plenty of those. Glitter is my mania...{/enen}"
    th "And the butterflies, as I see..."
    ss "{enen}...I just paint everything. It calms me down.{/enen}"
    me "{enen}That's a lovely hobby. I see you have a lot of them. {w}And who do we got here?{/enen}"
    "I pointed to the unsightly, yet somewhat cute homemade toy."
    ss "{enen}This is Gustav.{/enen}"
    me "{enen}Semyon. Nice to meet you.{/enen}"
    scene cg d1_ssad with dissolve
    "It seemed like Samantha almost joined the play, but finally looked away, as if remembering something. So Gustav remained silent."
    th "Perhaps she considers herself too grown up for this. Or maybe I embarrass her. That's right, I should keep distance with her personal things. Although, she doesn't seems disturbed."
    me "{enen}You have so much stuff here... so bright and colorful. What a nightmare for an epileptic!{w} I mean, it's really impressive. And the gifts, right?{/enen}"
    ss "{enen}Well, I brought all my favorite little things. {w}Sometimes it is sad to be that far from home, you know? So I took a little part of home with me, I guess.{/enen}"
    th "I think I came here in one of those moments of sadness, I could see that even through her friendliness. But is she just homesick, or there is something else also?"
    me "{enen}Did you... were somewhere out after breakfast?{/enen}"
    "Samantha took some seconds answering, then shook her head, denying."

    menu:
        "Ask about the note":               
            ss "{enen}You mean, it was yours?! Sorry, I don't think that's funny.{/enen}"
            th "I don't understand. What did I write that could upset her?"
            menu:
                "Apologize":
                    me "{enen}I'm sorry, there was some misunderstanding.{/enen}"
                    $ sp_ss = (sp_ss) - (1)
                    "Samantha said nothing. Let's hope that the incident is over."
                    jump ss_d1vn_eng                    
                    
                "Ask for the note":
                    me "{enen}I don't understand. Could I see the note, please?{/enen}"
                    ss "{enen}I probably should give it to miss Olga but here you are.{/enen}"
                    "In front of me there was a piece with a terrible drawing, and the words \"Deatx to USA\". So that's it, some clown had replaced the note. I had a better opinion of our pioneers."
                    th "What to do now? Show it to O.D.? There will be a total handwriting test... We don't need this at all."
                    "Samantha has already realized that the note is not my doing. She agreed that it's better to avoid the scandal. I promised her to find the culprit by myself."
                    jump ss_d1vn_eng 

        "Don't ask":
            window show
            th "Enough questions, or she might think that I'm really from KGB."
            jump ss_d1vn_eng    


label ss_d1vn_eng:   
    stop music fadeout 2
    window hide
    $ renpy.pause (1.3)
    play music Material_Girl fadein 1  
    scene cg d1_ssmile with dissolve
    "Samantha once again looked around her treasures."
    ss "{enen}So... As you see, I am a material girl.{/enen}"
    menu:
        "No way!":
            $ sp_ss += 1
            me "{enen}No, you're not! All the cool stuff doesn't make you...{/enen}"
            scene cg d1_ssmile_click with dissolve          
            stop music 
            play sound sfx_lock_open_close_1
            play music Turning_Japanese fadein 1            
            ss "{enen}Alright-alright! Just kidding. I'm just curious what you brought to me.{/enen}"
            scene cg d1_ssmile with dspr            
        "It's okay":
            $ sp_ss -= 1
            me "{enen}I'm okay with it. People are blaming good stuff only because they don't have it.{/enen}"
            ss "{enen}Hey, I was kidding!{/enen}"
            me "{enen}Oh.{/enen}"
            window hide
            $ renpy.pause (1.0)
            window show
            ss "{enen}Well, I don't know. Maybe I am materialistic. But in your country I feel like I've finally escaped something...{/enen}"
            me "{enen}A hyper-materialistic hell?{/enen}"
            ss "{enen}Yeah, something like that. And I love this feeling.{/enen}"
            me "{enen}Okay, so why are you materialistic? You said it.{/enen}"
            ss "{enen}I'm just curious what's in your bag. It is something materialistic, I'm totally sure.{/enen}"
 
         
    "I looked in the bag. There was enough food for the rest of the week."
    me "{enen}It's just a food. I've noticed that you Americans are also tend to have meals.{/enen}"
    ss "{enen}That is correct, I even like food. Especially when hungry.{/enen}"
    window hide
    scene bg day_sam  with dissolve 
    
    $ persistent.ssg_1 = True
    
    $ renpy.pause (0.5)  
    window show     
    "For a while we silently munched sandwiches, helping ourselves to mineral water."
    "To maintain a conversation, I asked about Artek."
    
    $ persistent.ssg_98 = True     
    
    show ss  normal casual with dspr
    ss "{enen}Artek is really big. It's not a camp, it's a city! And lots of activities there.{/enen}"
    th "And what do we have in Sovyonok? I should invite her for a walk, once we finish eating. It will be good to show the camp a bit."
    me "{enen}That's a pretty big stereo. Have you brought it with yourself too? It must be heavy.{/enen}"
    ss "{enen}Well, yes. Fortunately, I didn't have to carry it on myself. {w}I have a small player too, actually, but...{/enen}"
    me "{enen}But listening to it here alone  - would be selfish, right?{/enen}"
    show ss  smile casual with dspr
    ss "{enen}Yeah, probably. And not so fun.{/enen}"
    window hide
    show ss smile2 casual with dspr 
    $ renpy.pause (1.0) 
    window show     
    me "{enen}There is a real Japanese girl in the camp. At least she thinks so.{/enen}"
    ss "{enen}Really? Looking forward to meeting her.{/enen}"
    me "{enen}How about visiting her right now? Or we can go somewhere else.{/enen}"
    ss "{enen}Why not? Lead the way.{/enen}"
    me "{enen}Where to?{/enen}"
    ss "{enen}Wherever you want.{/enen}"
    stop music fadeout 5
    $ renpy.pause (0.5)     
    window hide
    



    $ disable_all_zones()
    $ set_zone('music_club', 'ss_musclub_eng')
    $ set_zone('clubs', 'ss_clubs_eng')
    $ set_zone('sport_area', 'ss_football_eng')
    $ show_map()

label ss_musclub_eng:

    play ambience ambience_camp_center_day fadein 3
    scene bg ext_musclub_day with dissolve
    window show
    "Going to the music club could be a good idea. Everyone loves music."
    window hide

    play sound sfx_open_door_2
    scene bg int_musclub_day with dissolve
    "There was no one inside. Where was Miku?"
    show ss  normal casual with dspr
    play ambience ambience_music_club_day fadein 1
    "We examined the available musical instruments. Samantha tried to play a melody on the piano."
    th "Perhaps it's better to come back later. We can go somewhere else."

    window hide


    $ disable_all_zones()
    $ set_zone('clubs', 'ss_clubs_eng')
    $ set_zone('sport_area', 'ss_football_eng')
    $ show_map()

label ss_clubs_eng:
    play ambience ambience_camp_center_day fadein 2
    scene bg ext_clubs_day 
    play music music_list["so_good_to_be_careless"] fadein 5
    with dissolve
    window show
    $ el_known = True
    "Why not boast about the latest achievements of cybernetics?"
    play sound sfx_close_door_1
    scene bg int_clubs_male_day with dissolve
    play ambience ambience_clubs_inside_day
    show ss  normal casual at left
    "Elektronik sat at the table and poked some chips with a screwdriver. He got up when we entered. Shurik wasn't there."
    show el surprise pioneer  with dissolve
    "I realized that I don't remember his last name. Or didn't know it in the first place. How do I introduce him?"
    me "{enen}Doctor Cheesekov, chief of science.{/enen}"
    "Elektronik bowed gallantly."
    show el smile pioneer with dspr
    el "{enen}At your service. Let me show you our work.{/enen}"
    "It turned out that Elektronik is fluent in English. At times I've needed a translator myself when I could not keep up with the conversation."
    show ss nosmile casual at left
    "Samantha listened attentively, sometimes asking a questions politely. But, overall, it was evident that robots were not her thing."
    "Soon, despite the gaps in the conversation, I noticed that the talk changed to other topics."
    "Struggling to follow the conversation, I could only pretend that I was obsessed with the study of the fancy box in my hands."
    th "Those wires, conductors, buttons and little lights... So much fun! {w}NOT."
    th "It seems that my services are no longer needed. None of them. Let Elektronik be the translator then. {w}Am I jealous?"
    menu:
        "Suggest that Elektronik become the translator":
            window show
            show ss  unsure casual at left
            show el shocked pioneer with dspr
            el "No-no, I can not. We're running out of time with the robot. And Shurik is going somewhere..."
            show el serious pioneer with dspr
            el "But if you guys join our club..."
            el "{enen}Would you like to join our club, Samantha?{/enen}"
            show ss  smile casual at left with dspr
            show el normal pioneer with dspr
            "She smiled perplexedly, glanced at me like if she is asking for help."
            me "She should see the other clubs first."
            el "True, true..."
            show el smile pioneer
            show ss normal casual at left           
            extend " What about you, Semyon? Ready to enlist?"
            me "Me? Uhm."
            el "Or did you put an eye on another club?"
            me "I'm not into clubs really."
            el "Why so?"
            me "As they say... I don't want to belong to any club that would have someone like me for a member."
            window hide
            show ss  smile casual at left           
            show el upset pioneer
            with dspr
            $ renpy.pause (0.8)             
            show el sad pioneer
            show ss  smile2 casual at left  
            with dspr   
            window show         
            me "...It's getting late. I think we should go."
            show el sad pioneer with dspr
            el "Oh. So long then."            
            ss "{enen}Goodbye. Spasibo.{/enen}"
            window hide
            $ renpy.pause (0.6)
            play sound sfx_close_door_campus_1          
            scene bg ext_clubs_day with dissolve
            window show
            th "Surprisingly, it's not even night yet."
            window hide         
            show ss nosmile casual with dspr
            $ renpy.pause (1)                     
            ss "{enen}Sam?{/enen}"
            me "{enen}Yes?{/enen}"
            ss "{enen}What did you ask him? When he freaked out.{/enen}"
            menu:
                "Say the truth":
                    window show
                    me "{enen}I suggested to Elektronik that he should be your translator.{/enen}"                   
                    show ss  sad casual with dspr
                    $ sp_ss = (sp_ss) - (1)
                    ss "{enen}I thought so.{/enen}"
                    me "{enen}I'm not trying to get rid of you, he's just better.{/enen}"
                    show ss  nosmile casual with dspr
                    ss "{enen}Even so, I understand. I used to babysit at home. Not much fun.{/enen}"
                    th "She did not behave as a child at all. Should I say that? No, enough has been said today. A lot of unnecessary things."

                "Lie":
                    window show
                    me "{enen}I told him to rebuild his stupid robot as a music box.{/enen}"
                    show ss  smile2 casual with dspr
                    "Samantha looked at me mistrustfully, but smiled."
        "Keep silent":
            window show
            "If I really want it, it is best to go directly to Olga."
            show el serious pioneer with dspr           
            el "By the way, guys. How about joining our club? Extra hands would help us a lot."
            el "{enen}Would you like to join our club, Samantha?{/enen}"
            show ss  smile casual at left
            "She smiled perplexedly, glanced at me like if she is asking for help."
            me "She should see the other clubs first."
            el "True, true..."
            show el smile pioneer
            show ss normal casual at left           
            extend " What about you, Semyon? Ready to enlist?"
            me "Me? Uhm."
            el "Or did you put an eye on another club?"
            me "I'm not into clubs really."
            el "Why so?"
            me "As they say... I don't want to belong to any club that would have someone like me for a member."
            window hide
            show ss  smile casual at left           
            show el upset pioneer
            with dspr
            $ renpy.pause (0.8)             
            show el sad pioneer
            show ss  smile2 casual at left  
            with dspr   
            window show         
            me "...It's getting late. I think we should go."
            show el sad pioneer with dspr
            el "Oh. So long then."            
            ss "Goodbye. Spasibo."
            window hide
            $ renpy.pause (0.6)
            play sound sfx_close_door_campus_1          
            scene bg ext_clubs_day
            show ss nosmile casual
            with dissolve
            window show
            th "Surprisingly, it's not even night yet."
    window hide 
    $ renpy.pause (0.8) 
    play sound sfx_dinner_horn_processed
    th "Here is the dinner horn."
    me "{enen}Are you hungry already?{/enen}"
    show ss nosmile casual
    ss "{enen}Not really. Let's go anyway. I know the rules.{/enen}"
    show ss smile2 casual   
    me "{enen}You don't have to follow all of them.{/enen}"
    show ss surprise casual     
    ss "{enen}Of course I have to!{/enen}"
    window hide
    $ renpy.pause (0.5)
    jump ss_day1_eng

label ss_football_eng:
    $ got_kicked = True
    scene bg ext_square_day  with fade
    stop music fadeout 5
    "As we explored the entirety of the camp, a few curious pioneers joined us along the road."
    play ambience ambience_soccer_play_background
    scene bg ext_playground_day  with dissolve
    show ss  normal casual
    me "{enen}Do you play football?{/enen}"
    show ss unsure casual
    ss "{enen}No, it's a boys game.{/enen}"
    me "{enen}I see... {w}Wait, I mean soccer.{/enen}"
    show ss smile2 casual 
    ss "{enen}Oh. Sure, why not.{/enen}"
    play music music_list["i_want_to_play"] fadein 3
    "There was a 3x3 game going on: Ulyana was among the players."
    pi "You're out of the team next time, Ulyana. You're bad. Not a single pass from you."
    th "Is Ulyana bad? Looks like we've got solid footballers here."
    show us dontlike sport at right with dspr
    us "OK, I'll give you a pass! Let's start over!"
    show us surp1 sport at right with dspr
    us "Hey, Semyon! Care to join us? All of you!"
    hide us with dspr
    hide ss
    "The teams were are: Ulyana with her friends - as one team, and all of the newcomers - in the other."
    play sound sfx_soccer_ball_kick
    "The game has begun."
    "It was clear from the start that our team was much weaker. And I was just a goalkeeper - didn't want to spoil the fun for the kids."
    play sound sfx_soccer_ball_catch
    "I was capable of dealing with most of their shots (there was a lot of them), yet conceding a couple of goals. Maybe several."
    play sound sfx_soccer_ball_gate
    "I noticed that Ulyana was arguing with her teammates again and decided not to try that hard when she was attacking. Let her have it."
    show us laugh sport far with dspr
    us "Goal!"
    pi "USSR-USA - faiv to zero!"
    hide us with dspr
    "Those words hurt me a bit. Taking possession of the ball, I ran into the front myself."
    play sound sfx_soccer_ball_kick
    "Dribbled past the two exhausted opponents, I yelled:"
    me "{enen}To the right!{/enen}"
    "Their goalkeeper already had chased me while I was moving the ball towards the flank."
    play sound sfx_soccer_ball_kick
    show ss  normal casual at right with dspr
    "The goal was empty. Samantha took a shot... {w} Goal!"

    play sound sfx_uliana_jumps_down
    with hpunch
    show ss  surprise casual at right with dspr
    "But at the same time, Ulyana reached Samantha and hit her in the back. Both fell on the grass."
    hide ss with dspr
    show us angry sport with dspr
    stop music fadeout 3
    "What a terrible foul! Red card, nothing else would work at this point." 
    hide us 
    "However, Ulyana was already off to somewhere. Gone without any excuses."    
    "The boys crowded around Samantha and helped her to stand up. She got hit pretty hard."
    $ sp_sp = (sp_sp) - (1)
    show ss  sad casual with dspr
    pi "Ulyana would not get away with it!"
    "I suggested guiding Samantha to the nurse, but she flatly refused."
    "Then we went away from the field and sat down on a bench."
    me "{enen}I'm so sorry for this. She didn't do it on purpose.{/enen}"
    show ss  normal casual with dspr
    ss "{enen}I'm fine. At least we scored. Worth it.{/enen}"
    show ss unsure casual  
    "She continued touching her neck slightly wincing, so her words weren't convincing at all."
    play sound sfx_lena_plays_tennis_fail
    play music music_list["tried_to_bring_it_back"]  fadein 6   
    "There was Lena there, practicing badminton. When she saw us, she came closer."
    show ss normal casual 
    show un smile sport at left 
    with dspr
    "I introduced the girls."
    show un normal sport at left with dspr
    un "So...Want to play?"   
    ss "{enen}You guys play, I'll watch.{/enen}"
    hide ss
    hide un with dspr
    play sound sfx_tennis_serve_2
    "We started the game. Our match was a pretty sad thing."
    play sound sfx_tennis_serve_1
    "Lena was hopeless, and there was no way to make it better. And the shuttlecock would never reach our rackets if I gave up."
    "We were interrupted by the voice of Miku."
    show mi smile pioneer with dspr
    mi "Guys! Olga Dmitrievna is looking for you. And you're here. I'm really glad I found you because who knows what could have happened, I mean she said so herself and I don't..."
    me "Why? What happened?"
    mi "No, nothing. Just to check on you two. How'd it go? How was the day? I noticed someone was in the..."
    me "No need to hurry then."
    show mi normal pioneer with dspr
    mi "Perhaps. Oh, you're playing so well! I wish I could do that! But you should really really really see her soon, really!" 
    "She was really energetic, as always. No place to run though."
    th "She left me on my own, why should I run after her? To be scolded in the end."
    mi "I've been helping her all day, no time to visit the club now."
    "I remembered about Slavya. Let's hope she wasn't punished for the morning."
    stop music fadeout 3
    me "Usually it's Slavya who helps her."
    show mi upset pioneer with dspr
    mi "It seems they don't speak with each other anymore. Do you know why that could be the case?"
    show mi normal pioneer with dspr
    me "I wonder. Hey, why won't you play with Samantha? While Lena and I rest."
    mi "I'd love to!"
    hide mi with dspr
    show un normal sport with dspr
    play sound sfx_tennis_serve_1
    "Both girls played well. Lena and I silently enjoyed their play for some time."
    play music music_list["goodbye_home_shores"] fadein 4
    un "Maybe Slavya is upset because of Samantha."
    un "She did some serious preparing for the arrival, made plans..."
    me "It's not fair. Olga Dmitrievna shouldn't..."
    me "On the other hand, to choose a friend for Samantha is also not correct."
    un "How long will she stay here?"
    me "I don't know. Couple of weeks?"
    un "And after that, she will never see us again. {w}Does it really matter who is going to be her formal friend?"
    menu:
        "Agree":
            me "You're right, one week is a short time. But friends can write to each other. And you can never know the future for sure."
            me "I think it is possible to be a friends for a lifetime without ever seeing each other."
            show un sad sport with dspr
            me "Well, maybe not now. But it'd be possible."
            un "Probably. But it's sad."
            th "I felt a bit lost. No longer focussed on the subject we were talking about. If we had any."


        "Disagree":
            me "I don't agree. Long or short term - this is same as our life. The most important things aren't planned per se."
            me "Speaking of something good, our memory holds to pleasant, bright moments. Are two weeks enough for such moments? They are!"
            me "Now we are sitting. It's nothing special, but not bad at all, right?"
            show un shy sport with dspr
            me "But if we want to repeat this exact moment - it's not going to happen, no matter how much time we've got. Even a perfect copy will not be the same."
            me "And now we are real and natural. Not formal, as long as we don't want it."
            me "So..."

    me "You two would certainly become friends."
    $ sp_un = (sp_un) + (1)
    show un shy sport with dspr
    un "I don't think it would be much fun for her."
    me "I'm not bored at all..."
    "Lena blushed quickly, better change the subject."
    me "Olga D. didn't prohibit their seeing each other. Why don't we call Slavya, if we spent some time together? You will see then."
    un "Good idea."
    play sound sfx_dinner_horn_processed
    "Our conversation was interrupted by the dinner signal."

    jump ss_day1_eng

label ss_day1_eng:

    scene bg int_dining_hall_people_day  with fade
    play ambience ambience_dining_hall_full
    "At this time, we went straight to the 'VIP' table of Olga Dmitrievna."
    show ss  normal casual at right
    show mt normal pioneer at left with dspr
    "Talking about our daily adventures (omitting some unpleasant moments, of course), I began to eat this navy-style pasta."
    "Surprisingly, I didn't get my scolding. On the contrary."
    mt "And forgive me, Semyon. It all was up to you. Such a hectic day."
    me "It's nothing. By the way, thanks for the sandwiches."
    mt "Which sandwiches?"
    me "Which you have sent with Shurik."
    show mt smile pioneer at left with dspr
    mt "I didn't. But I should... So he fed you, guys? Well done. As reward, I'm not going to ask where he took the food from."
    show mt normal pioneer at left with dspr
    "There were cakes, jelly and tea for a dessert."
    show ss  grin casual at right with dspr  
    ss "{enen}Is Slavya eating at another table this time? What a shame, right, Semyon? \"Slavya\" is all I can hear when you speak with others.{/enen}"
    show ss  grin_smile casual at right with dspr    
    "Samantha smiled broadly and apparently was very pleased with herself."
    th "Now she mocks me. She didn't hang out with Alisa, - I hope - so what does this come from?"
    "Olga Dmitrievna waited for translation."
    show ss smile2 casual at right with dspr  
    me "She asked why Slavya is not with us this time. Have you punished her?"
    show mt angry pioneer at left with dspr
    mt "Slavyana is already at an age when she can be punished only by herself."
    show mt sad pioneer with dspr
    show ss serious casual at right
    "That sounded unexpectedly harsh. It looked like a serious problem now. Samantha's smile has gone from her face."
    show ss unsure casual at right 
    th "Here it is. In the morning I was happy about \"don't think, just translate stuff\". And now everybody just speaks without thinking, and Semyon should think for all of them."
    me "{enen}She's with her friends, I guess.{/enen}"
    show ss  sad casual at right with dspr
    ss "{enen}...Sorry.{/enen}"
    stop music fadeout 3
    "And again, at the end of the meal, we all were sitting sad and guilty - traditions of our table."
    scene bg ext_dining_hall_away_sunset  with fade
    $ persistent.sprite_time = 'night'
    $ night_time()
    play ambience ambience_camp_center_evening 
    show mt normal pioneer at left with dspr
    mt "Okay, Semyon. Escort Samantha back home and that will be it for today. Time to let her rest."
    th "And what about my rest?"
    me "Understood."

    scene bg int_house_of_mt_night with dissolve
    "After completing the last task and wishing good night, I went to my room and sat on the bed. I wasn't that sleepy yet, so I had to find something to do."
    window hide
    menu:
        "Take a walk":

            $ bush_shirt = True
            play ambience ambience_forest_night
            play music music_list["door_to_nightmare"] fadein 3
            scene bg ext_house_of_mt_night_without_light  with fade
            window show         
            "I wasn't in the mood to go very far. I just stood and took in the cool air. In the house of Samantha the light was still on."
            play sound sfx_bush_leaves
            "Some suspicious rustling came out from the bushes between the two houses."
            "I stood still for a minute, like I didn't notice anything, then began walking to the bushes casually."
            play sound sfx_bush_body_fall
            "There were only three meters remaining when the person who hid jumped out of the bushes and ran towards the woods."
            play sound run_forest
            "I noticed only a white shirt on the nightly guest, but the night was too dark. It would not help recognising them."
            stop sound fadeout 2
            "I didn't follow the runner because of three reasons: the bushes between us were impassable, I was in slippers, and I didn't want to make a noise."
            th "Perhaps too many excuses. But I'm not a coward! Maybe just afraid a bit..."
            "I waited, expecting Samantha to come out after the fuss in the bushes, but it did not happen."
            "Then I went home to lay down again."

            scene bg int_house_of_mt_night2  with fade
            "I laid, trying to think over this day again and again, but my thoughts kept returning to the spy in the bushes."
            th "Probably some very curious pioneer wanted to look again at the American."
            "I fell asleep with this thought."
            window hide


        "Tell fortunes by cards":
            window show
            play ambience ambience_int_cabin_evening
            "I knew yes/no solitaire, but it would be boring."
            play music music_list["door_to_nightmare"] fadein 4
            "I took a pencil instead and wrote one name on each card - every person I knew in the camp."
            "What should be next? I checked the cupboard and found what I was searching for - a candle. I lit it and turned off the lamp."
            scene bg int_house_of_mt_night2
            show candle:
                pos (598,0)
            
            with dissolve           
            "Le entourage is ready. What next? What should I ask?"
            me "I appeal to you, the spirits of the camp! Who should I be afraid of the most?"
            "The candle flame fluttered. I shuffled the deck and slowly began to build a card house of the marked cards."
            "Ulyana, Slavya... Viola took her place too."
            "On Shurik's card, the house staggered, but remained standing. Who will destroy the «Sovyonok» of cards?"
            play sound sfx_open_cupboard
            "I heard a creak behind me and turned around. My house collapsed because of this movement." with hpunch
            "There was Olga Dmitrievna's card in my hand."
            th "Her?! But why?!"
            stop music fadeout 3
            play sound sfx_forest_fireplace
            show candlefire:
                pos (598,0)         
            "Being deep in thoughts, I haven't noticed how the cards fell on the candle and lit. A small fire broke out on the table."
            play sound sfx_open_door_1
            $ sp_mt = (sp_mt) - (1)
            show mt rage pioneer close
            play music music_list["that_s_our_madhouse"]        
            mt "SEMYON!!!" with hpunch
            $ renpy.pause (1)           
            window hide
            scene black  with fade
            $ renpy.pause (1)
            stop music fadeout 5
            "I was definitely in trouble. Cards do not lie."
            window hide         

    scene black  with fade
    $ renpy.pause (2)
    
    


    jump ss_d2_eng


label ss_d2_eng:


    $ new_chapter(2, u"Samantha: day two")
    $ persistent.sprite_time = 'day'
    $ day_time()
    scene bg int_house_of_mt_day  with dissolve
    play music music_list["my_daily_life"]  fadein 5
    show mt normal pioneer at center     with dissolve
    play ambience ambience_int_cabin_day fadein 3
    mt "Good morning! Get up! Wash yourself! And prepare yourself for something exciting!"
    me "Like what?"
    mt "I've found a job for the two of you!"
    th "Here we go again..."
    me "What is the new disaster?"
    mt "I'll tell you at breakfast."
    show mt smile pioneer at center     with dspr
    "Olga Dmitrievna got some clothes from the closet and looked at me expectantly."
    th "Uh, she wants to dress up."
    scene bg ext_washstand_day  with dissolve
    play ambience ambience_day_countryside_ambience fadein 5
    play sound sfx_close_door_1
    "I dressed quickly, and went out. On my way to the washbasins I've met Samantha who had a towel in her hands."
    show ss smile2 casual with dspr
    me "{enen}Good morning. Did you sleep well?{/enen}"
    show ss unsure casual
    
    $ persistent.ssg_98 = True     
    
    ss "{enen}Good. But... Those noises.{/enen}"
    me "{enen}What noises? Forest noises?{/enen}"
    show ss normal casual with dspr  
    ss "{enen}Yes, rustling and so on. It's like they are really close.{/enen}"
    "I've probably made some tragic face, but Samantha smiled and waved like it's nothing."     
    ss "{enen}I'm just easy to scare. Never mind. Well, see you at the breakfast.{/enen}"
    $ renpy.pause (0.5)     
    scene bg ext_house_of_mt_day  with dissolve
    play sound sfx_open_door_1
    stop music fadeout 3
    "On my way back, I saw Olga D. leaving our house."
    show mt normal pioneer far at center      with dissolve
    show mt normal pioneer at center     with dissolve
    play music music_list["sweet_darkness"] fadein 7
    me "It seems like Samantha doesn't feel safe in there. Living alone, next to the forest..."
    mt "Hmm. {w}Yeah, you're probably right. What can we do?"
    me "Why don't you find her a neighbor?"
    mt "I thought about it. But this is in favour of her ,like the whole house is at her disposal."
    mt "Well, who would you settle in there?"
    menu:
        "Slavya":
            show mt angry pioneer at center     with dspr
            mt "Slavya again? I knew that it must be about her. But can't you imagine that she doesn't want it herself?"
            me "What if she does? What she must do? Just to move in without asking? Opening the door with a kick?"
            mt "What to do? Well, how about nothing? The world doesn't revolve around her."
            me "Because it revolves around Samantha?"
            mt "You know what I mean. Yes, we are speaking about Samantha now. How to make it better for her."
            show mt normal pioneer at center     with dspr
            mt "Slavya is alright now.  And, between us: she lives with a girl who needed a friend, not less."
            $ sp_sl = (sp_sl) + (1)
        "Lena":
            mt "That's an odd choise. Lena is all by herself, you know."
            me "She is quiet and not intrusive. That's what we need."
            mt "Well, let it be a fallback option. Hey, you're not choosing a neighbor for yourself, are you?"
            show mt smile pioneer at center     with dspr
            "Olga Dmitrievna wagged her finger funnily."
            $ sp_un = (sp_un) + (1)

        "Ulyana":
            me "They are about the same age..."
            mt "No way! It's gonna be my living nightmare."

        "Alisa":
            mt "Strange choice. Why her?"
            me "This could benefit Alisa. I think she is already tired of this tomboy image of herself. If you give her this responsibility - she will be surprised and grateful."
            mt "Surprised - yes. But have you ever seen her grateful?"
            me "Put some faith in her. It will give her a lot of confidence. Besides, Samantha will be well-protected."
            mt "I see the point. But...some faith? It would take one hell of a mountain of faith! No, it's too risky."
            $ sp_dv = (sp_dv) + (1)

        "Me":
            show mt angry pioneer at center     with dspr
            mt "Very funny. Any another ideas?"
            menu:
                "Slavya":
                    show mt angry pioneer at center     with dspr
                    mt "Slavya again? I knew that it must be about her. But can't you imagine that she doesn't want it herself?"
                    me "What if she does? What she must do? Just to move in without asking? Opening the door with a kick?"
                    mt "What to do? Well, how about nothing? The world doesn't revolve around her."
                    me "It revolves around Samantha?"
                    mt "You know what I mean. Yes, we are speaking about Samantha now. How to make it better for her."
                    show mt normal pioneer at center     with dspr
                    mt "Slavya is alright now.  And, between us: she lives with the girl who needed a friend, not less."
                    $ sp_sl = (sp_sl) + (1)
                "Lena":
                    mt "That's an odd choice. Lena is all by herself, you know."
                    me "She is quiet and not intrusive. That's what we need."
                    mt "Well, let it be a fallback option. Hey, you're not choosing a neighbour for yourself, aren't you?"
                    show mt smile pioneer at center     with dspr
                    "Olga Dmitrievna wagged her finger funnily."
                    $ sp_un = (sp_un) + (1)

                "Ulyana":
                    me "They are about the same age..."
                    mt "No way! It's gonna be my living nightmare."

                "Alisa":
                    mt "That's an odd choice. Why her?"
                    me "This could benefit Alisa. I think she is already tired of this tomboy image of herself. If you give her this responsibility - she will be surprised and grateful."
                    mt "Surprised - yes. But have you ever seen her grateful?"
                    me "Put some faith in her. It will give her a lot of confidence. Besides, Samantha will be well-protected."
                    mt "I could see the point. But...some faith, huh? It would take one hell of a mountain of faith! No, it's too risky."
                    $ sp_dv = (sp_dv) + (1)


    mt "I will decide it later. Now get Samantha - time for the lineup!"
    window hide
    scene cg d2_lineup 
    with dissolve
    play ambience ambience_camp_center_day fadein 3
    "Lineup. Here comes the price for the moments of freedom we had yesterday. Today we pay double."
    "Olga D. has prepared a long speech. I couldn't translate it properly, keeping her tempo, but couldn't be silent either - Olga was watchful."
    "Then I tried to remember all the rousing speeches I know, and made them sound like hers."
    mt "{enen}{i}Fight, and you will die. Run, and you'll live. For Scotland! For freedom{/i}!{/enen}"
    "Samantha smiled blankly, but nothing has changed here."
    "Once Olga finished threatening the orcs of Mordor, we were able to go for breakfast."


    scene bg int_dining_hall_people_day  with dissolve
    show mt smile panama pioneer at left
    
    $ persistent.ssg_98 = True  
    
    show ss  normal fancy at right   
    play ambience ambience_dining_hall_full
    "She told about her \"surprise\" without any delay:"
    mt "Alright, guys. Ever dreamed of having a part in a theatrical production?"
    show ss surprise fancy at right with dspr  
    me "No!"
    show ss smile fancy at right with dspr 
    ss "{enen}Yes!{/enen}"
    "We answered simultaneously."
    me "{enen}What \"yes\"? You don't even know what was the question.{/enen}"
    show ss grin_smile fancy  at right with dspr  
    ss "{enen}It's about the theater, clearly.{/enen}"
    mt "Right! Let's stage a play!"
    th "Another offer you can't refuse."
    show ss smile2 fancy at right with dspr  
    me "I didn't know that there is a drama group in the camp."
    mt "There isn't... It's time to open it then!"
    me "No way! We don't have one because we don't need one!"
    mt "What do you have against the play?" 
    me "What's the play, actually?"
    show mt smile panama pioneer at left 
    mt "Romeo and Juliet."
    show ss  surprise fancy at right   
    th "Gosh! Why not \"Lolita\" then?"   
    ss "{enen}Oh, I adore the movie. So beautiful and so sad.{/enen}"
    show ss smile2 fancy at right 
    me "Shouldn't it be  \"The Auditor\" or what do they stage usually?"
    show mt normal panama pioneer at left  
    mt "We need something more... international."
    "Olga D. shook her head, looking at Samantha. Our carefree girl was eating an omelet."
    $ renpy.notify("Bon appetit")
    me "And how shall we do it? Shall I teach Samantha the whole Russian, or \"Kushat' podano\" sentence will be enough?"
    show mt angry panama pioneer at left
    mt "Stop it, Semyon. Go to the scene after the breakfast, and discuss all the details with Zhenya. She is in charge."
    stop music fadeout 4
    th "Zhenya is in charge. This is getting worse with every word of mine. No more words then. I am silent now. And angry."
    window hide
    $ renpy.pause (1.0) 
    if got_kicked == True:
        scene bg ext_dining_hall_near_day  with dissolve
        show us dontlike pioneer
        show ss  serious fancy at fleft       
        play music music_list["eat_some_trouble"]
        play ambience ambience_medium_crowd_outdoors fadein 2

        "Upon exiting the canteen I suddenly faced Ulyana. I grabbed her sleeve before she could run away."
        me "What have you done yesterday?"
        us "What are you talking about? I did nothing!"
        me "Football, remember?"
        us "Ulyana plays as she pleases!"
        me "You call that a play?"
        us "I thought you were a goalie, not a ref."
        me "Your move was terrible ,and your behavior was worse!"
        us "Let me go!"     
        menu:
            "Force her to apologize":
                me "You must apologize to her! Now!"
                show ss unsure fancy at fleft 
                us "Or what?"
                th "Uhm, I can't spank her, right?"
                me "Or I'm taking you to Olga Dmitrievna. Right now!"
                show ss surprise fancy at fleft             
                show us fear pioneer  with dspr
                us "I AM SOR-RY!"               
                "Ulyana hissed it with such hatred that I almost regretted my initiative."
                show ss sad fancy at fleft 
                hide us
                with dspr
                play sound run_forest
                "While I was thinking, whether to release her, she pulled herself and ran away."
                stop sound fadeout 2               
                ss "{enen}She hates me so much.{/enen}"
                me "{enen}No, she isn't. It's just...{/enen}"
                show ss serious fancy at fleft     
                ss "{enen}Maybe not before, but now she does. Well done, master negotiator.{/enen}"
                me "{enen}I'll talk to her later.{/enen}"
                show ss normal fancy at fleft 
                ss "{enen}It's OK. Forget it.{/enen}"
                window hide
                $ renpy.pause(0.5)
                $ sp_us = (sp_us) - (1)


            "Put her to shame":
                me "What's wrong with you?  Alright, you were carried away there, I understand. But how could you not apologize?"
                us "I did nothing special. It's a game how it is. Have you ever seen it on TV?  No broken bones means that the match was a flop!"
                me "Stop your tricks. There is no excuse for what you did. You should offer Samantha your apology."
                us "I don't need to apologize!"
                hide us  with dissolve
                $ sp_ss = (sp_ss) + (1)
                "Ulyana unclenched herself from my weakened grip and ran away."


    scene bg ext_stage_normal_day  with dissolve
    play ambience ambience_camp_center_day
    "We left the canteen and headed for the stage. Zhenya and Slavya were already there."
    show ss  normal fancy at right
    show mz normal glasses pioneer at center   with dspr
    show sl normal pioneer at left   with dspr
    mz "And here comes my troupe."
    th "Oh, I would say where your troupe is..."
    th "...Is eating a horse in a ditch! Yeah! That's where it is!"
    sl "Hello."
    ss "Privet."
    me "Just for the record: we didn't volunteer to anything."
    mz "But Olga Dmitrievna..."
    me "Her job is to suggest, our job is to refuse. Nothing is happening. We all came here just to show our politeness."
    show mz angry glasses pioneer at center with dspr   
    mz "It's only you. And you're not very good at it."
    me "You think that Olga cares? Next time she will remember about our drama group is... when we will be giving a tour in Paris."
    mz "We are staging this play, and you can't get rid of it. You are not able to hide while you are living at Olga's. {w}Well, you may run into the woods, but we will find you even there. Just accept the reality."
    show sl sad pioneer at left with dspr
    sl "Don't fight, guys. Nobody's forcing you, Semyon, but..."
    mz "What?! Of course we are forcing him! Hello?"
    stop music fadeout 3    
    ss "Privet."
    window hide
    $ renpy.pause(0.3)  
    show ss smile2 fancy at right
    show sl smile pioneer at left 
    show mz smile glasses pioneer at center
    with dspr
    $ renpy.pause(0.3)  
    window show
    "Everyone smiled after Sam's silly response. Things calmed down a bit."
    play music music_list["everyday_theme"] fadein 5
    mz "Okay, let's assign some roles."
    me "But you're gonna need many more people."
    sl "Sure, I'll try to find them later. Let's pick our roles first."
    show mz normal glasses pioneer at center with dspr
    me "Wait. Why \"Romeo and Juliet\", in the first place?"
    sl "Why not? It's classic."
    me "Why don't you pick something a bit less ...romantic?"
    "I was about to say \"girlish\", but who do I complain to?"
    mz "It is decided. Well. Despite of my directing functions, I'm also taking a role of..."
    th "Of Juliet."
    mz "...Of the old apothecary. And Samantha will be our Juliet, as you already may have guessed."
    show ss  surprise fancy at right 
    ss "{enen}Me? Juliet? Wow. But how should we do it? Like a silent show?{/enen}"
    show ss  normal fancy at right  
    me "Good question. You have calculated everything, but not the language barrier? That's adorable."
    show mz angry glasses pioneer at center 
    mz "Quit your mocking.  For Samantha, we have the text in English. For the rest - in Russian. Here you go, take it."
    me "But no one will understand her!"
    show ss nosmile fancy at right
    mz "They don't have to. Everyone knows the plot. The main things are intonation and gestures."
    sl "I think that's kinda interesting. It even can turn into a new genre!"
    me "But how would she know when to say her line?"
    mz "When others are silent. You know what to be silent is? Why don't you try it?"
    "Using the opportunity of secret negotiations, I turned to Samantha, speaking calmly:"      
    me "{enen}Waste of time. She's just... how to say... thriving on her power. Let's just leave and say we don't like acting or whatever.{/enen}"
    show ss unsure fancy at right
    ss "{enen}But I like the idea. {w}Okay, maybe Juliet is just too much for me. You're probably right, so let's leave then.{/enen}"
    th "This little girl wants to play Juliet. Back to the classics: \"I may be a bastard, but I'm not a f**king bastard\"."
    me "{enen}Alright, I'm staying. But just for now.{/enen}"
    show mz normal glasses pioneer at center
    show ss normal fancy at right
    with dspr
    mz "We need the Romeo now."
    "A small group of pioneers, who were watching our \"theatre\", after those words disappeared entirely."
    "And Zhenya was sitting on the stage with this arrogant look of hers. Like there are dozens of actors lined in front of her, and now she has to choose between Marlon Brando and Laurence Olivier."
    th "No freaking way! If I have to participate now, I'll pick the role myself. The lesser of two evils..."
    "I looked at the list of characters."
    me "I'll be Mercutio then. Romeo's best friend, young playboy."
    show sl serious pioneer at left with dspr
    sl "And who will be Romeo?"
    th "That's not the problem of Mercutio."
    me "You chose the play with a bunch of male roles, you should know how to drag the boys in it. Good luck with that."
    show sl normal pioneer at left with dspr
    sl "I'll dig into it, but I have to admit the problem. Almost everyone is busy in another groups by now. At least we should fill the small roles..."
    show sl normal pioneer at left with dspr
    play sound sfx_bush_leaves
    show ss surprise fancy at right 
    
    $ persistent.ssg_98 = True     
    
    $ renpy.pause (0.2)
    hide ss with dspr
    stop music fadeout 5
    "A gust of wind blew the papers out of the hands of Samantha. She rushed to pick them up, but few have already completed their way in the puddles."
    sl "Don't worry, there are more copies in the library. I'll get one."
    window hide
    $ renpy.pause (0.4)    
    menu:
        "Go with her":
            me "I'll walk with you. {w}Samanta, ty s nami?"
            $ renpy.notify("Sam, are you with us?")
            show ss  surprise fancy at right with dspr
            $ sp_sl = (sp_sl) + (1)
            ss "{enen}What?{/enen}"
            $ renpy.notify("As you wish.")          
            me "Kak hochesh. {enen}I'll be right back.{/enen}"
            scene bg ext_library_day  with dissolve
            play music music_list["reminiscences"] fadein 5
            show sl normal pioneer 
            "When we moved away from the scene a bit, I started questioning."
            me "What is the conflict between you and Olga Dmitrievna?"
            show sl serious pioneer 
            sl "What conflict, why?"
            me "I thought they would settle you with Samantha. Now it seems like that you are an outcast."
            show sl normal pioneer 
            sl "I thought so too. But then it somehow turned out badly. That joke at the table, and I ruined her shirt..."
            me "You should explain it. I can help."
            sl "Thank you for your concern. But I don't want to impose or something... It's fine."
            me "Well, as you say."

            scene bg int_library_day  with dissolve
            show un normal pioneer far at center   with dissolve
            show sl normal pioneer at right   with dissolve
            "We met Lena in the library. She sat at the table, reading one of our prints."
            play ambience ambience_library_day fadein 3
            show un normal pioneer at center   with dissolve
            me "Hey. So you heard about the play... Do you want to participate?"
            un "I can't."
            sl "Lena was so kind to substitute Zhenya here."
            th "On the contrary it would be better."
            me "It's a pity. We don't have enough actors."
            sl "Yeah, but we can't leave the library empty."
            th "What a treasure to guard! Who would possibly take away Lenin's works? They are as heavy as the building itself."
            me "At least come to see us later."
            show un shy pioneer at center 
            un "Sure, I will."


            "Slavya took the copy and we went outside."
            play sound sfx_close_door_campus_1
            play ambience ambience_camp_center_day fadein 3
            scene bg ext_library_day  with dissolve
            show sl normal pioneer  with dissolve
            sl "Why don't you want to play Romeo, Semyon?"
            me "Why should I?"
            sl "Why not?"
            me "You know, why. What about you? Do you want to play him?"
            sl "I will, if needed. But what Romeo will it be? I can't even hide this hair..."
            me "Just kidding."
            show sl smile2 pioneer 
            sl "Kidding or not, somebody should take this role."
            me "It was bad idea from the start. Somebody... {w}should... {w}kiss... {w}her."
            me "And what if someone photographs it? It would go to the \"New Angeles Times\" front page: \"Pioneer seduces American Princess\"."

            sl "You are funny."


        "Staying here":
            hide sl with dspr
            $ renpy.pause (0.4)
            show ss normal fancy at right with dspr
            $ renpy.pause (1)
            "While waiting, Samantha showed a desire to go home, get markers and other things."
            me "{enen}Sure, I'm with you. I mean, I need to pick some stuff too.{/enen}"
            "To stay alone with Zhenya wasn't in my plans."
            window hide
            $ renpy.pause (0.6)            
            scene bg ext_square_day
            show ss normal fancy
            with dissolve        
            play music music_list["so_good_to_be_careless"] fadein 5
            play ambience ambience_day_countryside_ambience fadein 3
            $ renpy.pause (0.5)             
            ss "{enen}Now I am Juliet. So exciting! Thank you.{/enen}"
            me "{enen}I'm just wondering how it's gonna work.{/enen}"
            ss "{enen}They need Romeo, right?{/enen}"
            me "{enen}Right... Any suggestions?{/enen}"
            if el_known == True:
                show ss  grin fancy with dspr
                ss "{enen}Maybe the robo-guy? What was his name?{/enen}"
                show ss smile2 fancy with dspr 
                th "Is she mocking me, is this some kind of test?"
                menu:
                    "Good idea":
                        me "{enen}Why not? His name is El. Let's go ask him?{/enen}"
                        show ss unsure fancy with dspr
                        ss "{enen}No, I was kidding. He's probably busy and... really busy.{/enen}"
                        "She was a little downcast, but then smiled again."
                        show ss smile2 fancy with dspr
                        ss "{enen}Weird name - El. El what? He doesn't look like Mexican.{/enen}"
                        $ sp_ss = (sp_ss) - (1)
                    "Not gonna work":
                        me "{enen}Not a chance. He's not leaving his club. At least not for some Juliet. Maybe if you put on some C-3PO costume...{/enen}"
                        ss "{enen}I like Star Wars but not that much.{/enen}"
                        show ss laugh fancy with dspr
                        "Samantha laughed."
            else:
                ss "{enen}I don't know any other boys here. It's only you, and you are busy.{/enen}"
                show ss normal fancy 
                ss "{enen}Wait. Is this a girls' camp?{/enen}"
                show ss grin fancy with dspr 
                me "{enen}Yeah, and I won a ticket in a lottery.{/enen}"
                show ss laugh fancy   
                ss "{enen}Lucky you.{/enen}"

            window hide 
            scene bg ext_house_of_sl_day  with dissolve
            stop music fadeout 7
            "We approached our houses."
            ss "{enen}Come in, I'll show you something.{/enen}"
            window hide
            $ renpy.pause(0.5)            
            play sound sfx_open_door_2
            $ sp_ss = (sp_ss) + (1)
            scene bg day_sam  with dissolve

            $ persistent.ssg_1 = True
            
            play ambience ambience_int_cabin_day fadein 3
            play music Genesis_Misunderstanding
            $ renpy.pause(0.7)
            me "{enen}Do you ever turn off your music?{/enen}"
            ss "{enen}Never!{/enen}"
            window hide             
            $ renpy.pause(1.0)
            play sound sfx_unzip_sleepbag           
            "Samantha took out a big box from her travel bag."
            me "{enen}What is it?{/enen}"
            ss "{enen}Wait, don't look.{/enen}"
            window hide
            $ renpy.pause(1)

##           play music  music_list["get_to_know_me_better"] fadein 1            
            show cg sweets with dissolve
    
            $ persistent.ssg_104 = True 
                  
            window show
            ss "{enen}Ta-daa!{/enen}"
            "The box was filled with sweets: Snickers, chewing gum, all sorts of candies. I've never seen before some of these. And I guess I'll never see them later."
            "There was a plastic rooster on the top - supplier of the \"PEZ \" sweets. He looked at me unkindly."
            window hide
            $ got_candies = True

            ss "{enen}Help yourself.{/enen}"
            me "{enen}...How?{/enen}"

            ss "{enen}Take something, silly.{/enen}"
            "As it is necessary to be polite, I took a licorice candy. Haven't seen those for a long time."
            me "{enen}Thanks.{/enen}"
            ss "{enen}Please, have some more! Take a Wham bar. Do you know these?{/enen}"
            me "{enen}Nope.{/enen}"
            ss "{enen}I strongly recommend.{/enen}"
            "With a bit of distrust, I took one bar."
            th "If they're so good, why they never make it to our market? {w}Well, it's not tasty at all... At least not that delicious... {w}I think I'll take another bar if Samantha turns away. Just to be sure."           
            window hide         

            $ renpy.pause(1)            
            hide cg
            show ss  surprise fancy 
            with dissolve           
            ss "{enen}I must give them away, but not sure how. What if we bring the whole box to Zhenya? Like the drama group candy fund?{/enen}"
            show ss nosmile fancy 
            me "{enen}I guess people of your job are either fat or very kind. {w}You are the second type, obviously.{/enen}"
            show ss shy fancy with dspr
            me "{enen}I mean, that's not something I should emphasize. {w}You are a thin girl without a doubt. {w}But I heard that some very thin girls still are concerned about their weight, so...{/enen}"
            me "{enen}Sorry.{/enen}"
            show ss  smile fancy 
            ss "{enen}That's okay. I'm not fat, because I'm just sick of sweets. Can you imagine?{/enen}"
            me "{enen}Hardly.{/enen}"
            show ss  nosmile fancy          
            ss "{enen}Well, it's true. Two years of \"have a candy, Samantha\" and then you can't look at them at all.{/enen}"
            me "{enen}What a sad story.{/enen}"
            show ss smile2 fancy 
            ss "{enen}I know how it sounds. You think \"she's got everything, yet complains!\".{/enen}"
            me "{enen}No, it's really sad. A tale of the lost candy joy.{/enen}"
            ss "{enen}Such a liar. {w}So we're taking the box?{/enen}"
            stop music fadeout 3
            me "{enen}Sure, if you wish. Just put it in something. Away from the eyes of candy-hunters.{/enen}"

            window hide
            $ renpy.pause(1) 
    window hide         
                    

    scene bg ext_stage_normal_day  with dissolve
    play ambience ambience_camp_center_day fadein 3
    show mz angry glasses pioneer far at left 
    show dv angry pioneer far at right 
    play music music_list["you_won_t_let_me_down"]
    "Back to the scene, we found Zhenya fiercely arguing with Alisa."
    dv "I'll take the guitar then."
    mz "Not a chance."
    dv "You don't need it here at all."
    mz "All the stuff remains in place, no discussion."
    dv "Do you intend play on this guitar?"
    mz "Miku will play if we need music. To play on what - it's up to her to decide."
    dv "She can take it back anytime."
    mz "No, because you're not taking it. No one will let you carry the guitar around the camp."
    dv "It will be in my place. Don't be stupid!"
    mz "We can't count the things already broken or lost in the very moment you laid hands on it. Forget it."
    dv "Damn, choke on it!"
    show dv angry pioneer at right   with dspr
    show dv angry pioneer close at right   with dspr
    "Alisa walked past without noticing us."
    hide dv  with dissolve
    menu:

        "Ask for guitar":
            show mz angry glasses pioneer far  with dspr
            show mz angry glasses pioneer  with dspr
            me "Why not give her the guitar? We don't need such stuff on the stage. It's only making mess."
            mz "No way. I am responsible for all the property."
            me "The whole camp will have a better sleep while Alisa is busy. Otherwise she might burn something just because of boredom. She may start with library."
            show mz rage glasses pioneer  with dspr
            mz "Are you threatening me now?"
            if got_candies == True:
                "I took Zhenya aside to speak in private."
                show mz angry glasses pioneer close  with dspr
                $ sp_dv = (sp_dv) + (2)
                $ gave_guitar = True
                me "Listen, we have goodies for drama club, a whole box. But only in exchange for the guitar. Under my responsibility."
                show mz normal glasses pioneer close with dspr
                mz "Hmm. People just crazy about this guitar. Why? Do you hide drugs in it, or what?"
                me "No, we hide our love letters. {i}Love is a drug for me.{/i}"
                mz "Whatever. Take it, but under your res..."
                me "Right!"
                hide mz  with dspr
                "It was easy to find Alisa - the last few minutes she was tying her shoelaces, not that far from us. She was intrigued by my effort, obviously."
                show dv normal pioneer far  with dspr
                show dv normal pioneer  with dspr
                dv "I don't know how and why..."
                me "Do you know \"thank you\"?"
                show dv smile pioneer with dspr
                dv "It's not like I need this guitar that much."
                me "Sure, sure... Hey, why don't you join our play?"
                show dv sad pioneer with dspr
                dv "No way. {i}She{/i} is in there... Those plays are such bollocks anyway."
                th "{i}She{/i}? I hope she meant not Samantha."
                show dv normal pioneer with dspr
                dv "But you may visit me. I mean, two of you. I'll play \"Beatles\" for you."
                menu:

                    "Accept the invitation":
                        me "Sure, we will come."
                        $ dv_inv = True


                    "Refuse":
                        th "Sam shouldn't hang out with them, probably. And there must be such a mess in their house... Olga would never allow it."
                        me "I don't know, we are so busy right now..."
                        $ sp_dv = (sp_dv) - (1)
                dv "Alright then, I'm waiting for you. Bye."
                hide dv  with dspr
            else:
                th "I did all what I could."

        "Offer a role to Alisa":
            show dv angry pioneer far  with dspr
            show dv angry pioneer  with dspr
            me "Wait!"
            dv "What?"
            me "I thought... Maybe you'll join our play too?"
            show dv rage pioneer with dspr
            dv "Me? In your play? Are you overheated? Put a hat on your stupid head."
            me "I didn't want to play too. But on the second thought, it may be fun."
            window hide
            $ renpy.pause(0.4)          
            show dv angry pioneer  with dspr
            window show     
            dv "So long."
            me "Wait. Look here. Tybalt, right? I think Tybalt is a pretty cool guy. He duels people and isn't afraid of anything."
            show dv normal pioneer with dspr
            dv "Maybe he happens to play on guitar?"
            menu:
                "Why not?":
                    me "Well, maybe. It doesn't say that he doesn't."
                    $ gave_guitar = True
                    $ sp_dv = (sp_dv) + (1)
                    $ dv_thi = True
                    show mz angry glasses pioneer far at left  with dspr
                    mz "Nobody plays on guitar in our play! Stop speaking nonsense."
                    me "But how do they play serenades without an instrument? Guitar is closest one to their balalaikas."
                    show mz rage glasses pioneer far at left with dspr
                    mz "Not this type of guitar!"
                    me "Why not? We already have Romeo and Juliet who speak in different languages. Guitar is not a big deal. Have you heard about rock operas?"
                    "Slavya moved closer to Zhenya and whispered something to her ear."
                    show mz angry glasses pioneer far at left 
                    mz "Okay. Do as you want. But I was against it, remember."
                    dv "You act like I already agreed on something. Don't you want to ask me?"
                    window hide
                    $ renpy.pause(1.3)  
                    dv "Fair enough, I'm taking the guitar. And I'll have a look at your text, but later. Maybe."
                    show dv smile pioneer 
                    show ss  normal fancy at right
                    with dspr
                    ss "{enen}Here.{/enen}"
                    "Samantha handed Alisa one of the copies and a marker. Leaflets were already sewn by our thoughtful Sam."
                    show dv shy pioneer with dspr
                    dv "Thanks... I'll go to my place. Too many people here."
                    hide dv with dspr
                    "Alisa looked somewhat wistful while leaving."
                    mz "And off it goes, never coming back."
                    sl "She will return."
                    mz "I'm speaking about the guitar."
                    window hide
                    $ renpy.pause(0.5)

                "He is not":
                    me "He does everything... except the guitar, apparently."
                    dv "Not interested. Bye."
                    window hide
                    $ renpy.pause(0.3)
                    hide dv  with dspr
                    $ renpy.pause(0.3)
                    
        "Let her go":
            $ sp_dv = (sp_dv) - (1)
            th "Perhaps she could play some role too. But no one will force her to."
    show sl normal pioneer 
    show mz normal glasses pioneer at left
    with dspr

    stop music fadeout 5
    sl "So, guys. I'll go look for more actors. And you here begin to learn your roles, okay?"
    me "Good luck."
    play ambience ambience_camp_entrance_day_people fadein 3
    window hide
    $ renpy.pause(0.3)      
    hide mz
    hide sl  
    with dspr
    window show
    "Slavya left, and Zhenya settled in the shade with a book."
    me "I regard this as \"feel free to leave\". We'll go then."
    mz "Be here after the lunch."
    $ renpy.pause(0.5)  
    scene bg int_house_of_mt_day  with fade
    play ambience ambience_int_cabin_day
    play music music_list["smooth_machine"] fadein 5
    "We returned to our houses. Everyone needs a little personal time. Or a lot."
    "Laying on the bed, I realized that I don't want to look at the text at all. How do I shut the whole thing down?"
    th "Samantha will be upset... But if this venture is destined to fail, it is better to end it fast."
    play sound sfx_knock_door2
    "I was half-sleeping for some time, until somebody knocked at the door. Samantha came to pick me up for the lunch - turned out I missed the signal."
    scene bg int_dining_hall_day  
    show sl normal pioneer at left 
    show ss nosmile fancy at right 
    with dissolve   
    play ambience ambience_dining_hall_full fadein 3
    "This time Olga Dmitrievna was missing, so we sat down with Slavya. Zhenya decided to eat alone, which in my opinion, was a good decision."

    me "Any success in your searchings?"
    sl "Not really. Nobody wants to learn a big role."
    me "Maybe we should shorten the text?"
    sl "We will, but we shouldn't cut the lines of main characters. It's not that much, actually."
    show sl serious pioneer at left with dspr
    sl "And don't forget: two languages means two versions. If we cut something, we should synchronize the rest. It's up to you, actually."
    me "Great, I feel so important. I am the chosen one."
    sl "Sorry. If only I knew English to help you."
    me "You are trying so hard, while Zhenya is just a barking dog. We would have a dozens of actors without her."
    sl "No-no, nothing would work without her."
    me "Well, nothing is working yet."
    show sl normal pioneer at left with dspr
    play sound sfx_borshtch
    show ss normal fancy at right
    ss "{enen}You know, after Artek, I made this Borscht at home. And it was good actually. Almost as this one.{/enen}"
    show ss unsure fancy at right
    ss "{enen}But it wasn't red. Haven't find those beetroots. I've put some tomatoes in - not red enough.{/enen}"
    me "{enen}Borscht without beetroots is not a borscht. It's an insult!{/enen}"
    show ss smile2 fancy at right
    sl "You may do so. But it will be another soup, not the borscht."
    show ss  surprise  fancy at right
    ss "{enen}And how do you call it? I hope it's easier than \"borscht\".{/enen}"
    show ss  normal  fancy at right 
    me "{enen}You don't ask.{/enen}"
    stop music fadeout 5    
    ss "{enen}No, you're must be kidding. You don't need a special name for a cabbage soup.{/enen}"
    me "{enen}Fine, you asked for it.{/enen}"
    show ss  surprise  fancy at right 
    extend "{enen}We call it SHCHI.{/enen}"
    show ss  smile  fancy at right with dspr
    window hide
    $ renpy.pause (1) 


    scene bg ext_stage_normal_day  with fade
    play music music_list["went_fishing_caught_a_girl"] fadein 5
    play ambience ambience_camp_center_evening fadein 3
    "After the lunch we gathered at the stage - same old faces."
    show sl normal pioneer at left   
    show mz normal glasses pioneer at center   
    show ss  normal fancy at fright
    with dissolve
    sl "I failed to find Romeo. I'm sorry. One guy promised, and now, at my approach... {w}He jumped in the water from the pier and swam out! Can you imagine? He was fully dressed."
    mz "We might even start a wave of suicides here."
    sl "Joking aside, It's time to decide something."
    "And again, everyone looked at me."
    menu:
        "So be it":
            th "I knew it in my heart since the beginning - to be Romeo is my only destiny. That's what it's all about."
            $ sp_sl = (sp_sl) + (1)
            $ sp_ss = (sp_ss) + (2)
            $ rom_eo = True
            me "I could take the role, if you don't mind. I don't see other options, except a girl as Romeo. It will be way too stupid."
            show sl smile pioneer at left 
            show mz normal glasses pioneer at center 
            hide ss with dspr
            "Slavya nodded happily, Zhenya just wrote a few words in a notebook with no expression. Samantha dropped something, and was looking for it behind the scene. On her return, she looked quite pleased."
            show ss smile2 fancy at fright with dspr
            sl "I will be Mercutio then. For now."
        "No way!":
            $ mer_cu = True
            me "...What weather today!"
            sl "...Weather?"
            me "Yeah, just look at the sky! It will be rain, I think."
            sl "...So we have no other options. I will be Romeo then."


        "Let's flip a coin":
            show mz normal glasses pioneer at center 
            show sl normal pioneer at left 
            sl "What do you mean?"
            show sl serious pioneer at left 
            me "It's simple: there is a coin. Toss it. Should it be tail - you are Romeo. And if it's head, that means that I am Romeo. There are no other candidates, right?"
            mz "Of all the stupid things this is your Stupid Thing of the Day."
            me "Feel free to be Romeo then."
            mz "Oh, do what you want!"
            show sl normal pioneer at left with dspr
            sl "I see. Fine, let the coin decide."
            show ss  nosmile fancy at fright with dspr
            "I explained the rules to Samantha. She asked again about the sides, seemed pretty interested."
            me "Here we go. The drums are rolling..."
            "Several onlookers, who were watching us, depicted a drumbeat."
            menu:

                "Throw it to Samantha's feet":
                    hide ss with dspr
                    "Samantha hesitated for a moment, but then quickly picked up the coin, before anyone else could see it."
                    show ss  normal fancy at fright with dspr
                    ss "{enen}It's Sam. Semyon.{/enen}"
                    $ rom_eo = True
                    $ sp_ss = (sp_ss) + (2)
                    me "{enen}Are you sure?{/enen}"
                    show ss  smile fancy at fright 
                    ss "{enen}Totally.{/enen}"
                    play sound sfx_simon_applause
                    show ss smile2 fancy at fright 
                    "Someone clapped, someone whistled. Zhenya has written something in her notebook. And Samantha smiled, as usual. Or maybe a bit wider."
                    show sl smile pioneer at left   with dspr
                    sl "Great. And I'll be Mercutio then."


                "Toss the coin and catch it":
###                 $ renpy.notify("Rollback is bugged here, but it's really random! Rollback + save-load refreshes the thing.")
                    $ rand = renpy.random.choice(["side1","side2"])
                    if rand == "side1":                    
                        "It's the tail side. Slavya is Romeo."
                        $ mer_cu = True
                        sl "So be it."
                        show sl smile pioneer at left   with dspr
                        show ss unsure fancy at fright 
                        "Slavya smiled to Samantha, who seemed a bit confused."
                        ss "{enen}Sure, we'll do great.{/enen}"
                    else:
                        "Head side it is. Can't trick the fate: I should be Romeo."
                        $ rom_eo = True
                        $ sp_ss = (sp_ss) + (1)
                        show sl smile pioneer at left   with dspr
                        show ss normal fancy at fright with dspr
                        play sound sfx_simon_applause
                        "Someone clapped, someone whistled. Zhenya has written something in her notebook."
                        sl "Great. And I'll be Mercutio then."


    "By assigning several secondary characters, we started reading the play by the roles."
    play sound sfx_thunder_rumble
    "Until we were interrupted by the weather: it became much darker suddenly, thunder could be heard from the distance."
    $ renpy.pause (0.5) 
    stop sound
    scene bg int_library_day  with dissolve
    play ambience ambience_library_day fadein 3
    "We decided to continue somewhere indoors and moved to the library. Lena joined to our readings."
    show un normal pioneer far at center  with dspr
    un "Enter Lady Capulet and Nurse."
    show mz normal glasses pioneer far at right   with dspr
    mz "{i}Nurse, where's my daughter? Call her forth to me.{i}"
    show un shy pioneer far at center  
    un "{i}Now, by my maiden... maidenhead, at twelve year old,{i}"
    un "{i}I bade her come. What, lamb! what, ladybird!{/i}"
    un "{i}God forbid! Where's this girl? What, Juliet!{/i}"
    un "Enter Juliet."
    window hide
    $ renpy.pause (0.9)     
    show un serious pioneer far at center  with dspr
    window show
    un "...Enter Juliet."
    window hide
    $ renpy.pause (0.6)     
    show mz angry glasses pioneer far at right  with dspr
    window show
    mz "Juliet enters!"
    show ss  surprise fancy at left with dspr
    ss "{enen}Oh. {i}How now! who calls?{/i}{/enen}"
    show ss  nosmile fancy at left 
    show un normal pioneer far at center 
    mz "{i}Your mother...{/i}"
    sl "What is it? Get back to the text."
    ss "{enen}{i}Madam, I am here.{/i}{/enen}"
    mz "It is the text, see it for yourself."
    sl "It is, but not in yours!"
    ss "{enen}{i}What is your will?{/i} {w}Uhm, guys?{/enen}"
    window hide
    show mz normal glasses pioneer far at right 
    hide ss
    with dspr
    $ renpy.pause(1)
    "Struggling a lot, we read the play till dinner. Everyone was tired. It was decided to cut the play significantly."
    show sl normal pioneer at left with dspr
    sl "I think that's enough for today. We'll see what can be done with the text. Right, Zhenya? We have work for this evening."
    me "Heard it, Zhenya? You have work to do. Don't make any plans."
    show mz angry glasses pioneer far at right  
    mz "My plans are not your business!"
    me "Yeah, and like nobody's business... Ever."
    show mz rage glasses pioneer far at right   
    sl "Stop it, Semyon. {w}Wait, what have I just said?"
    me "That we are free for today."
    sl "You are. We did a good job here. Thanks to all."    
    $ renpy.pause(1)
    scene bg ext_dining_hall_away_sunset  with dissolve
    "It was still raining. At least it's a warm rain - a mushroom rain, how do we call it. {w}This evening's meal is not for free - the price is to get wet."


    $ persistent.sprite_time = 'sunset'
    $ sunset_time()
    scene bg int_dining_hall_sunset  with dissolve
    show mt smile pioneer at left 
    show ss  normal fancy at right
    with dissolve
    play ambience ambience_dining_hall_full fadein 3
    "This time Olga is in here. Slavya came back to Zhenya, they discussed something passionately."
    mt "What, the rain, huh? How is the play doing? Tell me!"
    me "More or less. There are plenty of problems, but nothing critical."
    mt "Have you got a role too, Semyon?"
    th "Is she kidding or what? Like I was able to refuse."
    if rom_eo == True:
        show ss grin_smile fancy at right with dspr
        ss "{enen}He is my Romeo!{/enen}"
        show mt normal pioneer at left   
        mt "Hmm. That's a big role. I see you're up for a challenge."    
        show ss normal fancy at right with dspr  
        "It was clear that there will be a serious private conversation between us on this matter." 
        th "I am the last person in the camp who wanted it. I only agreed to not let down others. Remove me from the part, do me a favor."
        "But that answer was ready a little bit later. And my actual answer was only:"
        play sound sfx_inhale
        me "Jeez..."
    else:
        ss "{enen}He is Mercutio. Fancy name. Dies quickly though.{/enen}"
        me "Young playboy."
        show mt smile pioneer at left with dspr
        mt "When I was little, this film was extremely popular. Kids were not allowed to see it, but we still sneaked into the cinema."
        mt "So, the neighbors had the cat Mercutio. Was a playboy too. Before the some, ahem, manipulation."
        "I had difficulties translating this heartbreaking story."
        show ss nosmile fancy at right with dspr 
        me "{enen}...So there was a cat named Mercutio, and he was a pus...lady-lover. And suddenly... Not any more.{/enen}"
        show ss  scared fancy with dspr
        ss "{enen}Why? Oh, has he died?{/enen}"
        me "{enen}Worse.{/enen}"
        show ss unsure fancy at right with dspr 
    $ renpy.pause (1)
    stop ambience fadeout 1 
    stop music fadeout 3
    scene bg ext_houses_sunset  with dissolve
    "After dinner Olga Dmitrievna has declared that she takes Samantha under her wing, and I am free for today."
    scene bg int_house_of_mt_sunset  with dissolve
    show mt normal pioneer at left  
    play ambience ambience_int_cabin_evening fadein 3
    "On returning home, I pretended that I'm learning the role, and Olga began to prepare her bath accessories."
    play music music_list["raindrops"] fadein 7
    mt "It seems like Samantha is pretty attached to you already?"
    me "I don't think so. She's friendly with me as with the others."
    mt "Do I need to tell you that we want to avoid certain situations?"
    me "Which situations?"
    mt "If she fell in love with someone. I'll have to interfere when, there will be tears and all sort of drama. In the worst-case scenario, of course."
    me "And what if you do not interfere?"
    mt "Tears and drama will be a bit later, but worse. Might be a big scandal. And who is to blame? Me."
    show mt sad pioneer at left   with dissolve
    me "But she's only thirteen."
    mt "Yes, well, Juliet was thirteen. Say, Semyon, could you promise... No, never mind. You can't."
    me "To turn her back for the good of the homeland, if I have to?"
    mt "Something like that. But that's a dull question. Big love can overcome all the circumstances. Shakespeare, right?"
    mt "And I will be the circumstances. It's always me. Forever a witch in this tale, a wicked stepmother. But I don't get any pleasure from this role."
    th "She seems to be talking with herself now, on her personal vague matters. No need to answer."
    "As confirmation of my assumption, she began to pack up stuff which she's just unpacked. Her mind was somewhere out."
    hide mt  with dissolve
    "I had to find something to do for the rest of the evening. Sick of Shakespeare, I decided to go for a walk."


    window hide

    $ disable_all_zones()
    $ set_zone('clubs', 'ss_clubsd2_eng')
    $ set_zone('library', 'ss_libd2_eng')
    $ set_zone('beach', 'ss_beachd2_eng')
    $ show_map()

label ss_clubsd2_eng:
    scene bg ext_houses_sunset   with dissolve
    $ d2_cyb = True
    th "Shurik's behavior somewhat bothers me, it's time to investigate it. Set a course for the cyber-club, and unfurl the sails!"
    play ambience ambience_clubs_inside_day fadein 3
    scene bg int_clubs_male_sunset  
    show sh surprise pioneer at left    
    show el surprise pioneer at right
    with dissolve   
    "Both were in place. My appearance caused a bit of the stir."
    me "Hi there. What are we constructing today?"
    show el normal pioneer at right   with dspr
    sh "Well... something. For the robot. Yeah."
    show sh normal pioneer  with dissolve
    "I looked closer: in the front of Shurik there was another box with wires.  And the robot was moved to the far edge of the table."
    me "It looks like a bomb. And robot will set it off, right?"
    show el laugh pioneer at right   with dspr
    "El laughed."
    el "Perhaps we could assemble even a bomb here! But this is just a metering device."
    me "Speaking about devices... I'll show you one, you may be interested."
    "I pulled out my cellphone from the pocket. For me it had only use as a clock and only while the battery is still up."
    show el serious pioneer at right   with dspr
    "Elektronik turned it in his hands, then opened the shell to look at the insides. Finally, he turned on the phone and started to press the buttons."
    el "Wow! It's a radio phone, I saw them in the magazines. This one even has an electronic memory. It must be Japanese. Very interesting."
    el "Although it's not very practical. It says \"no signal\", you see? It's useless without a base."
    me "Yeah... Imagine such a thing calling without a base. Is this even possible? Tell me, you're the men of science."
    el "One day they will. And if you give them to everybody... Nobody will ever get lost with such a thing, and nobody will be left without help. What a life it would be!"
    me "Easy, dreamer! For now we are nowhere near even to a simple elevator to Mars."
    show el normal pioneer at right   with dspr
    show sh upset pioneer at left   with dspr
    "Shurik didn't even look at the phone. He looked at me very angrily."
    sh "Did {i}she{/i} gave it to you? Must be a very expensive gift."
    menu:

        "I borrowed it":
            me "It's hers. She just gave it to me to play for a bit. Wanna see some games?"
            if got_candies == True:
                sh "And that thing, did you borrow that too?"
                "Shurik has pointed out to the floor. There was a wrapper of the candy with which Samantha treated me."
                th "Must be fell out from my pocket, when I took out the cellphone."
                me "Candy it is. Everyone is treating Samantha with all sorts of sweets. She brought some candies too. What's wrong with that?"
                sh "Maybe it's nothing."
                me "What else it could be?"

            show sh serious pioneer at left   with dspr
            "Shurik turned away and began to dig into the jar with cogs, making it clear that the conversation was over."

        "It's mine":
            me "No, my parents brought it from abroad."
            show sh serious pioneer at left    with dspr
            sh "From abroad? From where, actually?"
            me "Uh... Does it matter? What is it, some kind of interrogation?"
            show sh upset pioneer at left   with dspr
            sh "Just curious. Interesting family you got there. No wonder that you have made friends with {i}her{/i} so quickly."
            $ sp_sh = (sp_sh) - (1)
            show el serious pioneer at right  with dspr
            el "That's enough, guys."

    stop music fadeout 5
    th "This Shurik is quite mad with his suspicions. I'd better speak with El about it, before Shurik turns insane completely."
    th "Should I call him for a private chat now? Probably not, whispering with El would ensure Shurik that I'm a threat. {w}Nothing to do here then."
    me "I think I'll get going."
    el "Okay then. See ya later."
    $ renpy.pause (0.5) 
    jump ss_d2eve_eng

label ss_beachd2_eng:
    scene bg ext_beach_sunset  with dissolve
    play ambience ambience_boat_station_night fadein 3
    me "Here I am at the beach, where the last visitors already are leaving. Alisa and Ulyana are among them."
    show dv normal pioneer far   with dspr
    show us normal swim far at right   with dspr
    dv "...Why should I search for you? At the beaches and everywhere?"
    us "But that's the only beach. Why weren't you swimming today?"
    dv "I haven't got time."
    "They noticed me."
    show dv normal pioneer  with dspr
    show us normal swim at right   with dspr
    if gave_guitar == True:
        dv "Hey, thanks again for the guitar."
        if dv_thi == True:
            me "Don't miss the rehearsal tomorrow."
            dv "Yeah, yeah..."

    dv "How is your girlfriend doing?"
    me "Ask her yourself, she is standing next to you."
    show us smile swim at right   with dspr
    show dv smile pioneer   with dspr
    us "You wish!"
    stop music fadeout 5
    show us upset swim at right   with dspr
    "Ulyana smiled for a second, but then pouted again."
    me "What's wrong?"
    us "Nothing. I'm going home."
    hide us   with dspr
    th "Is she jealous, or what?"
    $ renpy.pause(0.9)  
    "Alisa had made several steps in her direction, but then slowed down and finally stopped."
    play music music_list["dance_of_fireflies"] fadein 5    
    "I sat down on the decorative element \"huge painted tire\". The tire was dug into the ground firmly, challenging all the mischiefs to dig it out." 
    "Alisa glanced at my rubber sitting. I moved a bit to make room (there was a plenty of room, actually)."
    "Surprisingly, the invitation worked, Alisa sat down beside me."
    me "...What's going on with her?"
    if sp_dv >= 5:
        dv "She's not happy with your American."
        me "I noticed that. But why?"
        show dv guilty pioneer  with dspr
        "Alisa seemed about to answer something, but hesitated, and finally shrugged."
    else:
        dv "Do you think I will discuss it with you?"
        show dv normal pioneer   with dspr
        me "Alright, keep your secrets. But look after her."
        dv "Don't teach me..."

    $ renpy.pause(0.9)
    dv "Why are you here? It's late for swimming."
    $ sp_dv = (sp_dv) + (1)
    me "No swimming. I'm just looking around."
    dv "I see."
    show dv normal pioneer   with dspr
    me "The Law of the genre is broken..."
    dv "What do you mean?"
    me "No, never mind."
    dv "Spit it out!"
    me "If I say that I don't swim, then you are..."
    dv "...What?"
    me "You should drag me to this dark cold water instantly."
    show dv smile pioneer  with dspr
    dv "I thought about it, but Ulyana is watching. There, in the bushes."
    me "How did you notice her?"
    dv "Maybe I'm a gifted girl."
    me "Gifted? Well, talent can't do much without practice. Are you an experienced spy too?"
    dv "Maybe."
    me "Just as I thought. Are you still in the business?"
    dv "Enough questions."
    show dv normal pioneer  with dspr
    dv "No, I am not in the business. If it matters."
    me "Retired, then? Why?"
    dv "Maybe it's not my thing anymore. Now I'm less interested in the others."
    $ renpy.pause (0.5) 
    dv "She's gone. We are boring for her."
    menu:
        "I'm not going in the water":
            me "No swimming, please. It's cold enough already."
            show dv smile pioneer  with dspr
            dv "Damn, you tease me. If only I wore a swimsuit..." 
            me "Are you sparing me? Am I safe?"
            dv "For today. But tomorrow I'll made you Aquaman, you better start to grow gills!"
            th "Yeah, like you're gonna see me on the beach again. No way. Unless all the crowd would be gone somehow."
            dv "I almost drowned once... It was in winter."
            me "Why would you swim in winter?"
            dv "I fell through the ice, you idiot!"
            th "That was rude. Her manners are terrible, after all. {w}I'll say nothing, until she apologizes."
            $ renpy.pause (0.9)             
            dv "Let's go back to the camp?"
            "Sigh."
            stop music fadeout 3            
            me "Let's go."
            $ renpy.pause (0.5)             

        "We may kiss finally":
            show dv angry pioneer  with dspr
            "Alisa did not expect such a proposal. Her face became serious and her voice - hard."

            dv "Go on then. Do it."
            menu:
                "Do it":
                    "All of a sudden, I desperately wanted to do it."
                    window hide

                    scene cg d5_dv_argue  with dissolve
                    window show
                    $ sp_dv = (sp_dv) - (1)
                    $ kiss_dv = True
                    "Marveling at my own impudence, I approached and kissed Alisa on her closed lips. She did not answer anyhow, and wasn't even breathing. I recoiled."
                    scene cg d5_dv_argue_2  with dissolve
                    "Alisa turned away. We sat in silence for some time. I felt like I did something bad, yet some part of me was victorious. And for that, I was ashamed too."
                    "I saw that she drew a deep breath to say something, but silence wasn't broken. Finally, she got up, threw a quiet \"bye\" in my direction, and left."
                    scene bg ext_beach_sunset  with dissolve
                    th "What just happened? It was something special for me, definitely. But what about her? However, I'll never comprehend it on my own, so why do I even bother?"
                    stop music fadeout 3
                    th "I can only wait and see what will happen next. If something will. And now it's time to go back."

                "Make an excuse to her":
                    me "Just a joke, take it easy."
                    scene cg d5_dv_argue_3 with dissolve
                    dv "Oh, a joke. In our time, men are not the same as they used to be..."
                    me "The good old me would never even propose such a thing. You've met the alpha-male version of Semyon here."
                    dv "What did I do to deserve this honor?"

                    me "I don't know. It's not intended. Maybe they just put something in the kefir."
                    $ renpy.pause (1)                   
                    scene cg d5_dv_argue_2  with dissolve
                    stop music fadeout 3
                    "So the tension was eased, but further conversation struggled anyway. We made a silent agreement to return to the camp."
                    
    $ renpy.pause (0.5)     
    jump ss_d2eve_eng


label ss_libd2_eng:
    scene bg int_library_night2  with dissolve
    $ persistent.sprite_time = 'night'
    $ night_time()
    show un smile pioneer   with dspr
    play ambience ambience_int_cabin_evening fadein 3
    "In the Temple of the Books, I found only Lena. And she was also about to leave."
    un "Hi. Again."
    me "Hi again."
    un "So... You came because of the play or... do you want to take a book?"
    stop music fadeout 5    
    $ sp_un = (sp_un) + (1)
    me "Not sure. I think I've had enough reading for today."
    show un normal pioneer  with dspr
    "I pointed to the stack of play prints on the table." 
    un "I see."
    $ renpy.pause (1)   
    th "These awkward pauses."
    me "But I might take a book actually."
    un "Sure. Pick any."
    me "Hm... What would you recommend?"
    un "Let's see... \"The Count of Monte Cristo\", maybe?"
    me "I have read it."
    un "Oh."
    me "You had too? Is it a good reading for a girl?"
    play music music_list["tried_to_bring_it_back"]  fadein 5
    un "Good enough for me."
    me "But... But the Count never forgave his bride."
    un "Because she gave up. She didn't wait him until the end."
    me "Yeah, the Count was tough and she was like... meh."
    $ renpy.pause (1)   
    me "Would you wait 'til the end? If you were her?"
    show un shy pioneer  with dspr
    un "Maybe."
    me "As tough as your Count?"
    show un smile pioneer  with dspr
    un "Yes."
    me "Although I can't believe that you can spend 20 years in a cell, and get out... {w}If not as a superhuman like him, but just like a normal person."
    show un normal pioneer  with dspr
    un "Well... When I read it, I believed it. A tale, of course, but as an ordinary man - why not.."
    me "Let's say he could keep himself fit, avoid all the illness..."
    me "But how could he remain sane after all? I think if they release him after 20 years - he would find himself in some dark cave, and remain there till the end."
    un "He had a strong will. As well as motivation - revenge. And a wise mentor."
    me "His bride didn't know of it though. And after so many years he can't come back again. It is foolish to wait."
    un "Yet people are waiting. Sometimes they don't even know what they are waiting for, but still are waiting."
    un "But she knew what she's waiting for. Maybe, it was her only joy. Foolish, yes. But beautiful."
    stop music fadeout 4    
    me "It seems that foolishness and beauty are often go together..."
    show un shy pioneer  with dspr
    th "I shouldn't say that!"
    play sound sfx_open_door_2
    show mi smile pioneer at left   with dspr
    play music Hide_and_seek fadein 3  
    "Miku invaded the library just in time to save me from the awkwardness."
    show un normal pioneer  with dspr
    mi "Just close it already, Lena. Oh, Semyon is here!"
    me "Hey."   
    mi "Listen, I have a great idea! What if you rhyme up Russian and English lines in the play? It would be awesome!"
    $ renpy.notify("Boris Pasternak is the autor of the Russian adoptation")
    th "Here it is - my chance to stand on a par with Shakespeare and Pasternak."
    me "Great idea indeed. Top of my to-do-list."
    th "NOT."
    mi "Cool! Let's go then. I composed the music for the play, and now I need listeners. Are you with us, Semyon?"
    me "Uhm..."
    th "No, I can't disturb Lena so much at once."
    me "Thank you, but I gotta go home. I'll have to learn my role."    
    mi "Shame. If only I brought the guitar, you would never escape without listening! Again it's all on you, my poor Lena."
    un "I don't mind."  
    me "I'll check your music at tomorrow's rehearsal. Bye then."
    stop music fadeout 3
    mi "Goodbye."
    "Lena almost whispered that goodbye of hers, and I left the girls."
    $ renpy.pause (1)   
    jump ss_d2eve_eng
label ss_d2eve_eng:
    $ persistent.sprite_time = 'night'
    $ night_time()
    scene bg int_house_of_mt_night  with dissolve
    play music music_list["smooth_machine"] fadein 5
    play ambience ambience_int_cabin_evening fadein 3
    "Olga Dmitrievna was out somewhere on my return. I took the script and tried to focus on it. I've spent about a hour diligently learning the role."
    play sound sfx_open_door_1
    "And then my roommate has come. I found it indecent to ask about bathing, even if I suspect that our wooden bath could be nothing but a horror for Samantha."
    "Olga Dmitrievna was pensive, so the rest of the evening was almost in complete silence."

    scene bg int_house_of_mt_night2  with dissolve
    play ambience ambience_int_cabin_night fadein 3
    stop music fadeout 3
    "The lights were out, and I almost immediately fell asleep. I dreamed that I'm Romeo, and Tybalt is Lena somehow. So Lena the Tybalt stabbed Alisa the Mercutio, and I stabbed her in return."
    play music music_list["drown"] fadein 3
    "But when I bent over her body, suddenly she became Juliet. And then Olga D. appeared, dressed as a monk. She raised above me, getting bigger and bigger, and her anger grew as well."
    "Unable to bear her still silent reproach, I ran through the streets of Verona. And she ran after me, with a fearsome demonic roar."
    "And then I heard a scream. I couldn't tell if it was in the dream or not. I finally woke up."
    "I checked the clock - it was a little past midnight. I had this feeling of anxiety, and couldn't get rid of it. Was the scream real or not? {w}Finally I got up and went out."
    scene bg ext_house_of_mt_night  with dissolve
    play ambience ambience_forest_night fadein 5
    stop music fadeout 3
    "There was light in the house of Samantha. Coming closer, I heard sobbing from inside."
    menu:
        "Knock on the door":

            play music music_list["faceless"]  fadein 5
            play sound sfx_knock_door2
            "I gathered up the courage to knock on the door. Samantha opened it to me." 
            play ambience ambience_int_cabin_night fadein 3
            scene bg night_sam 
            show ss sad pij
            with dissolve
            
            $ persistent.ssg_97 = True             
            $ persistent.ssg_2 = True    
            
            "She looked frightened, and her face was pale as a bed sheet."
            me "{enen}I heard screaming. What happened?{/enen}"
            show ss scared pij 
            ss "{enen}This howl! Have you heard it too?{/enen}"
            show ss unsure pij  
            $ sp_ss = (sp_ss) + (1)
            me "{enen}Probably. I'm not sure.{/enen}"
            show ss surprise pij
            ss "{enen}But I am sure! It was so long and unreal. And really close.{/enen}"
            scene bg ext_house_of_mt_night  with dissolve
            play ambience ambience_int_cabin_night fadein 3
            "It was a little scary, but I still went out and walked around the house. I was almost ready to go check the edge of the forest, but Samantha called me back."
            scene bg night_sam
            show ss sad pij
            with dissolve
            play ambience ambience_int_cabin_night fadein 3
            me "{enen}There is nothing there.{/enen}"
            show ss serious pij with dspr 
            ss "{enen}What was it? What do you think?{/enen}"
            me "{enen}I don't believe in ghosts.{/enen}"
            th "Or do I? Well, that's not the way to calm down the girl, so I don't believe."
            me "{enen}...So it must be either animal or human. Someone is fooling around, probably.{/enen}"
            show ss unsure pij with dspr 
            ss "{enen}I think so. Yet it was so scary, I can't help myself to stop shaking like a leaf!{/enen}"
            hide ss with dspr
            "I promised to sit with her until she falls asleep. {w}This did not happen for a long time: Samantha chattered and chattered. Out of fear, of course."
            "Then I had to intimidate her that I would leave soon if she's not sleeping. Otherwise I risked to fall asleep there myself, which would be totally inexcusable. The wrath of Olga is horrible, and the punishment is inevitable."
            "We agreed that I will be telling stories, and Samantha will be quiet until she falls asleep. I bet we made a cute picture there: just like a father with a daughter. {w}But how sleepy was I..."
            show blink
            scene bg night_sam
            show ss surprise pij
            with fade

            $ persistent.ssg_2 = True 
            $ persistent.ssg_97 = True
            
            ss "{enen}Sam! Don't fall down!{/enen}" with vpunch
            show ss smile2 pij  
            th "Damn! Fell asleep, after all. Sleeping in a chair is possible: myth confirmed."
            me "{enen}Thank you. I'll go home. Don't be afraid of anything - I'm will be near.{/enen}"
            ss "{enen}Good night...{/enen}"
            window hide
            $ renpy.pause (0.5)             
            scene bg int_house_of_mt_night2 
            with dissolve
            "When I almost managed to get to the bed quietly..."
            play sound sfx_wood_floor_squeak
            "...The last floorboard treacherously creaked."
            mt "Huh... Semyon? Where did you go?"
            me "In the restroom, where else?"
            mt "I am watching you."
            window hide
            $ renpy.pause (1)           
            jump ss_d2epi_eng
        "Take a look around":
            play music music_list["faceless"]  fadein 5
            scene bg ext_house_of_mt_night  with dissolve
            play ambience ambience_forest_night 
            play sound sfx_bush_leaves
            "Trying to be as quiet as possible, I walked around the house, fighting my way through the dense bushes."
            scene bg ext_path2_night  with dissolve
            th "What if there is a bear here in the bushes? The one who is bothering Samantha. Came here to feast with some wild raspberries, just to end with something more nutritious."
            "I imagined the picture: my home town, usual students reunion. I am not there, of course. People reminisce about the good old days, recalling the study, boasting about everything."
            "Ivan here - the manager in a leading company. And Semyon? Remember that guy? What happened to him? {w}Ripped by a bear somewhere in the 80's."
            "I was already behind the house. The bear hadn't shown up. Perhaps its time hasn't come yet."
            play sound sfx_hiding_in_bush
            th "I'll check how is she doing."
            scene bg ss_d2window1 with dissolve
            "Parting the branches, I approached the window, trying to see anything."
            $ renpy.pause (2)
            play sound sfx_scary_sting 
            scene bg ss_d2window2 with dissolve
    
            $ persistent.ssg_106 = True 
                  
            "Suddenly her face showed up in front of me - pale white, distorted by a grimace of horror." 
            play sound sfx_bush_body_fall
            with hpunch
            play music music_list["scarytale"]
            play sound sfx_alisa_falls
            "Samantha screamed, and I screamed in return, falling back at the same. She screamed even louder because of my screaming. Finally, we used all the air of our lungs."
            stop music fadeout 5 
            "The lights go on in the houses around us. We woke up a good half of the camp."
            "I was lying in the improvised sofa of branches and giggled hysterically, until Olga Dmitrievna had pulled me out of the bushes. It was painful."
            $ sp_ss = (sp_ss) - (1)
            $ sp_sp = (sp_sp) - (1)
            scene bg int_house_of_mt_night  with dissolve       
            play ambience ambience_int_cabin_night fadein 3
            "She dragged me into the house, and went back to Samantha. It took a while to explain what happened. Olga believed me finally, but only because Samantha defended me."
            scene bg int_house_of_mt_night2  with dissolve
            mt "Gosh, you just can't have pity on my nerves, can you? Give me a break, you bushy dweller!"
            me "At least we have something to remember..."
            mt "Enough chatter. Sleep now. Tomorrow we'll figure out how to explain your screams to others."          
            window hide
            $ renpy.pause(0.5)
            jump ss_d2epi_eng
            
label ss_d2epi_eng:

    play music music_list["afterword"]
    scene black  with fade
    $ set_mode_nvl()
    window show
    "A few days passed, and they were good days. We learned the play and rehearsed, walked around in camp a lot, played games, and were never tired or bored."
    "Of course, cramming wasn't a pleasure, but that was the price of my privileged position. I wasn't charged with any job, and there were other concessions."
    "In addition, my success on the theatrical stage didn't go unnoticed - my play has attracted some fans."
    "Maybe because I was the only guy among the lead roles. Everyone knows that women can't play. And my rants caused the applause on a regular basis."
    nvl clear
    "Samantha. The camp began to slowly get used to the fact that we are always together. There were not so many jokes about it."
    "I couldn't say for sure, but there was a feeling that every day she became a little bit more sad."
    "Of course, there were some unpleasant incidents, like that howling at that night, but she perceived them with humor and nothing seemed so bad. Yet something was wrong. Maybe the role was too hard for her?"
    "Whether or not, I've seen her smile as often as always, but not her smiling sparkling eyes. {w}But a lot cleared in that day."
    window hide

    $ renpy.pause(1.5)
    $ set_mode_adv()
    scene black with fade   
    jump ss_d3_eng  
    

label ss_d3_eng:
    $ new_chapter(3, u"Samantha: day three")
    $ persistent.sprite_time = 'day'
    $ day_time()
    scene bg int_house_of_mt_day  with dissolve
    play sound sfx_knock_door7_polite
    play ambience ambience_int_cabin_evening
    $ renpy.pause (1.5)
    window show
    #"Стук в дверь. Ну почему это случается каждый раз, стоит мне прикорнуть на часок? Усилием воли продрал глаза. Оказывается, уже светло."
    "A knock at the door. Why does this happen every time I try to take a nap for an hour? I barely opened my eyes. It turned out that it was already daylight."
    #th "Отлично, Ольга Дмитриевна встаёт. Она займётся этим."
    th "Good, Olga is awake. She can handle it."
    
    #"Я завернулся в одеяло с головой. Пожар, война, наводнение? Надеюсь, одеяло меня защитит. А с места не сдвинусь."
    "I wrapped myself in the blanket. Fire? War? Flood? I hope the blanket will protect me. And I won't move."
    #"Слышу, как вожатая обменивается с кем-то парой фраз. Быстро одевается, уходит. Меня не тронули, какое счастье."
    "I can hear Olga talking to someone. She is dressing up and leaving. I am not disturbed. I am happy."
    show blink 
    scene black  with fade
    play music Predator fadein 3

    #"Я провалился в сон. Мне снилось, будто я попал в боевик. Действие происходило в джунглях."
    "I fell asleep. In my dream I was in some kind of action movie. The scene was set in the jungle."
    window hide
    scene bg ext_path2_day  with dissolve
    window show
    #"Со мной были мои верные товарищи."
    "I had my faithful comrades with me."
   
    window hide
    show sldvwar  with dissolve
    window show
    #"Наша миссия - лагерь злодеев. Они против наших пионерских ценностей. Мы должны разгромить их или умереть."
    "Our mission is the camp of villains. They are against our pioneer values. We must defeat them or die."
    #"Мы набросились и били их, а они кричали что-то на ломаном русском с дурным акцентом. Это была победа."
    "We rushed in and began beating them. They started to scream something in bad Russian with awful accents. The victory was ours."

    scene bg ext_path2_night  with dissolve
    show ss_che with dspr
    #"Кое-кого даже взяли в плен."
    "We even captured someone."
    
    $ renpy.pause(1)
    hide ss_che with dspr   
    #"Но потом что-то пошло не так."
    "But then something went wrong."
    
    scene bg ext_path2_predator  with dissolve2
    #"Что-то начало убивать нас по одному. Как будто сам лес ожил, чтобы не отпустить нас."
    "Something started to kill us one by one, as though the forest itself had come alive and was trying to stop us."
    #"Но оно кровоточит - значит, мы можем это убить. {w} Мы атаковали всеми силами, но тщетно. В конце нас осталось трое."
    "But if it bleeds, we can kill it. {w} We endured as hard as we could, but in vain. There were only three of us in the end."
    scene bg ext_path2_night  with dissolve
    show lbilly at center   with dissolve
    #"Лена пошла в рукопашную, а мы с Самантой бежали дальше."
    "Lena went into melee, while Samantha and I kept running."
    
    hide lbilly with dspr
    #"Впереди обрыв. И вот уже я падаю вниз, в воду. Стало очень мокро." with vpunch
    "Precipice ahead. And here I am, falling down into the water. It became very wet." with vpunch
    scene black  with fade

    #th "Почему так мокро? Я что, обмочился во сне? Или мне снится, что обмочился? Но что в таких случаях бывает на самом деле?"
    th "Why is it so wet? Did I wet my pants while dreaming? Or did I just dream that I wet my pants? But what really happens in such cases?"
    show unblink 
    play ambience ambience_int_cabin_day
    scene bg int_house_of_mt_day  with dissolve
    show foxsh at center 
    #"Я приоткрыл глаза. В комнате стояла лиса. И брызгала в меня водой. Но как? У лисиц нет рук."
    "When I opened my eyes, there was a fox in my room. And it splashed water on me. But how? Foxes don't have hands."
    
    #"Схватил подушку и запустил в проклятого лисо-тавра. Черт, у него там целое ведро. Ведро сильнее одеяла. Я окончательно проснулся."
    "I grabbed my pillow and threw it at the cursed fox-taur. Damn, it had a whole bucket of water. A bucket is stronger than a blanket. I finally woke up."

    #sh "Подъем, уродец!"
    sh "Get up, freak!"
    #th "Голос Шурика. Определённо, это Шурик в маске лисы. И ему надоело жить."
    th "This is Shurik's voice. It's definitely Shurik wearing a fox mask. And apparently he doesn't want to live anymore."
    #"Сейчас освобожу его от бремени бытия и лягу досыпать."
    stop music fadeout 3    
    "I am gonna help him out with that, and then I will return to bed."

    play music music_list["scarytale"] fadein 2
    #"Я бросился на своего врага, но Шурик оказался готов к такому развитию событий и моментально выскочил наружу. Я за ним."
    "I jumped on my enemy, but Shurik was ready for this and immediately ran away. I ran after him."
    
    scene bg ext_house_of_mt_sunset  with dissolve
    hide foxsh 
    #"Никогда так не бежал, ненависть меня окрылила. Но и Шурик показывал удивительную прыть, отрыв почти не сокращался. Убегал он в сторону леса."
    "I never ran so fast; hate gave me more power. Shurik was wonderfully fast, however, and I couldn't catch him. He ran into the woods."
    
    play sound run_forest
    scene bg ext_path_day  with dissolve
    play ambience ambience_forest_day
    #"Трава впивалась в мои босые ноги, но адреналин отодвинул эту проблему на второй план. Мы миновали первые деревья. Вот я уже в одном прыжке от него..."
    "Grass dug into my bare feet, but adrenaline made me forget about that. We passed the first trees. I was one jump away from him..." 
    scene bg ext_polyana_day  with dissolve
    #"И вдруг, летевший мимо меня со страшной скоростью лес..."
    "Suddenly, the forest flew past me with frightening speed..."
    play sound sfx_blanket_off_stand
    scene bg ext_polyana_reversed  with dissolve
    show foxshrev at center 
    #"...резко остановился и перевернулся." with vpunch
    "...then abruptly stopped and overturned." with vpunch
    stop music fadeout 2
    #"Так не честно! Шурик развернулся и подошел ко мне, слегка приплясывая."
    "That's not fair! Shurik turned around and came up to me, slightly dancing."

    play music music_list["drown"] fadein 5
    #"Я попытался проанализировать ситуацию, несмотря на весь сюрреализм происходящего. Ситуация: вишу головой вниз, ногами в петле. Попался в ловушку безумного Шурика."
    "I tried to analyze the situation, despite the surrealism of it. Here's the deal: I am hanging upside down with my legs tied. I'd fallen into the mad Shurik's trap."
    #sh "Попался, враг народа. Сейчас-сейчас..."
    sh "Got you, public enemy. Just a moment..."
    hide foxshrev  with dspr
    #"Если это сон, то есть ли смысл себя щипать, когда каждая мышца ниже (или выше?) шеи так дико болит?"
    "If it's a dream, should I pinch myself if every muscle below (or above?) my neck already hurts so badly?"
    #"Шурик стал копаться возле какой-то коряги поблизости, пока не извлёк оттуда два предмета. Первый, тяжёлый, был похож на аккумулятор. Ещё на нём появился пояс с висящими по кругу инструментами. Иные были весьма опасного вида."
    "Shurik started to rummage near some snag until he drew out two items. The first item was heavy; it looked like a battery. He also retrieved a belt with tools. Some of them looked very dangerous."
    if d2_cyb == True:
        #"Вторым была вчерашняя коробка с проводами из кружка кибернетики."
        "The second item was the box with wires that I'd seen yesterday in the cybernetics club."
    else:
        #"Вторым была ещё одна коробка с проводами, явно полегче."
        "The second item was a box with wires. It was clearly lighter."
    show foxshrev at center   with dspr
    #"Завершив приготовления, Шурик приблизился ко мне"
    "When Shurik finished setting up, he came closer to me."
    #"Я попытался его ударить, но попал только по воздуху." with hpunch
    "I tried to punch him, but I missed." with hpunch
    #sh "Ну-ну, веди себя прилично. Тогда может и отпущу."
    sh "Come on, behave yourself. Then maybe I will let you go."
       
    #th "Пожалуй, лучше ему подыграть, иначе это может плохо кончиться. Похоже, он совсем спятил."
    th "I guess I should play along, or else it could end badly. It seems he's gone crazy."
    #me "Ладно. Не знаю, во что мы играли, но ты выиграл. Снимай меня, и поговорим."
    me "Okay. I don't know what game we've been playing, but you've won. Get me down, and we'll talk."
    hide foxshrev  with dspr
    #sh "А мы и так. Только сначала руки тебе свяжем, чтобы полиграф подсоединить. Давай сюда."
    sh "We ARE talking. But first I will tie your hands so I can connect the polygraph. Stay calm."
    menu:
        #"Подчиниться":
        "Submit":
            #"Связав мне руки сзади, он достал пластырь и прикрепил пару проводков к моей груди."
            "After he tied up my hands, he attached a couple of wires to my chest with patches."
            show foxshrev at center   with dspr
            jump ss_d3f_eng

        #"Сопротивляться":
        "Resist":
            $ sp_sh -= 1
            #"Я попытался ещё раз ударить безумца, и на этот раз попал."
            "I tried to punch this madman again, and this time I got him."
            play sound sfx_boat_impact
            #"Без опоры удар получился не сильным, но Шурик пошатнулся и наступил на свой \"полиграф\", проломив фанерную крышку."
            "I couldn't hit hard without support, but Shurik staggered and stepped on his \"polygraph\", breaking a plywood cover."
            play sound sfx_break_cupboard
            #"Оказалось, что внутри коробки напиханы какие-то тряпки и прочий мусор. Но Шурика это ничуть не смутило."
            "It turned out there were only rags and other garbage inside. This didn't bother Shurik at all."
            #sh "Ничего, это мы починим... А ты!"
            sh "That's nothing, I can fix it... And you!"
            $ broken_box = True
            play sound sfx_face_slap
            #"Шурик влепил мне пощёчину, от чего моё тело начало раскачиваться как маятник, а затем закрутилось." with hpunch
            "Shurik slapped me so my body began to swing like a pendulum, and then began to twirl." with hpunch
            scene black with dspr
            #"Шурик расхохотался."
            "Shurik burst out laughing."
            show unblink 
            scene bg ext_polyana_reversed  with dissolve
            #"Меня начинало мутить, так что я бросил попытки отбиваться, лишь бы остановить это качание."
            "I became sick, so I gave up trying to fight back in order to stop the swinging."
            show foxshrev at center   with dspr
            #"Связав мне руки сзади, он достал пластырь и прикрепил пару проводков к моей груди."
            "When my hands were tied, he attached a couple of wires to my chest."
            #"Я не мог понять, правда ли он считает свою коробку детектором лжи, или старается для меня."
            "I couldn't tell if he really considered his box to be a lie detector or if he was just showing off for me."

label ss_d3f_eng:
    #"После всех приготовлений он, наконец, приступил к допросу."
    "When all the preparations were done, he finally started the interrogation."
    #sh "Работаешь ли ты на американцев? Отвечай \"да\" или \"нет\"."
    sh "Do you spy for the Americans? Answer \"yes\" or \"no\"."
    #me "Перевожу для одной из них. Это \"да\" или \"нет\"?"
    me "I translate for one of them. Is it \"yes\" or \"no\"?"
    #sh "Ты знаешь, о чём я! Отвечай! Ну!"
    sh "You know what I mean! Answer! Come on!"
    #me "\"Да\"."
    me "\"Yes\"."
    #sh "Что \"да\"?"
    sh "What do you mean by \"yes\"?"
    #me "А что \"ну\"?"
    me "And what do you mean by \"come on\"?"
    #"Шурик всё больше злился."
    "Shurik was getting angrier."

    #sh "Предал нашу советскую родину?! Отвечай!"
    sh "Have you betrayed our Soviet Motherland?! Answer!"
    #me "Нет!"
    me "No!"
    #"Шурик вглядывался в панель на боку своего ящика. Похоже, увиденное его не удовлетворило."
    "Shurik checked the panel on the side of his box. It seemed that it didn't satisfy him."
    #sh "Знаешь, что... Ты... Ты не шпион, нет. Но ты хуже. Беззаботный глупец. И всё равно предатель. Такие как ты, в погоне за кока-колами и джинсами, разрушат эту страну."
    sh "You know what... You... You are not a spy. No. You are worse: a carefree fool. Still a traitor.. People like you in pursuit of coke and jeans will destroy this country."
    #"Голос Шурика дрогнул, он опустился на траву. Но затем снова решительно встал."
    "Shurik's voice faltered. He sat down on the grass, but then he stood up resolutely."
    #sh "Со времён её первого приезда мы прогнулись. За каких-то два года на колени встали! Из-за таких вот..."
    sh "We started to cave in after her first arrival. We kneeled after a mere two years! Because of such..."

    #"Отвлёкшийся на минуту Шурик снова вспомнил про моё присутствие."
    "Shurik got distracted for a moment, but then he remembered my presence."
    #sh "Ну что, девчонка важнее родины?"
    sh "So, is the girl more important than the Motherland?"
    #me "С чего ты взял?"
    me "Why do you think so?"
    #sh "Родина или Саманта! Отвечай!"
    sh "Motherland or Samantha! Answer!"
    #me "Что за дурной вопрос?!"
    me "What a stupid question!"
    #sh "Дурной? Ну что же, посмотрим. Готов привести её сюда?"
    sh "Stupid? Well, we'll see. Are you ready to bring her here?"
    #me "Конечно, готов. Мог бы просто попросить. Снимай меня, я мигом."
    me "Of course I am. You could just ask. Get me down, I'll bring her in the blink of an eye."
    #sh "Врёшь!"
    sh "You're lying!"
    #me "Поразительно! Эта твоя штука работает!"
    me "Holy cow! Your thing is working!"
    #sh "Вот паяц. Что на верёвочке, что без."
    sh "What a clown. Either you are tied or not."
    hide foxshrev  with dspr
    play sound sfx_blanket_off_stand
    #"Шурик достал из-под коряги мешок и стал бережно складывать туда свои коробки и инструменты."
    "Shurik pulled out a bag from the snag and began to gently put all of his boxes and tools into it."

    #sh "Это нам ещё пригодится. Вот устрою твоей Саманте допросец, узнаешь много нового. А пока повиси тут, подумай."
    sh "This will come in handy. I'll arrange an interrogation for your dear Samantha, and you'll learn a lot new things. Until then... you may hang here and think."
    #"Он взвалил мешок на плечо и направился в лес, прочь от лагеря."
    "He slung the bag over his shoulder and moved towards the forest, away from the camp."
    #th "А ведь мы убежали не так далеко. Если звать на помощь, вполне можно докричаться. Но до кого? Придёт Саманта, увидит меня в таком виде. Бррр."
    th "We actually didn't run too far. If I call for help, someone may hear it. But who? What if Samantha were to come and see me like this? No, thanks."
    #"Я снова стал пытаться освободиться. Руки связаны не туго, пожалуй, смогу развязать. Но что дальше?"
    "I tried to get free again. My hands weren't tied so well, and perhaps I could get free. But then what?"
    menu:
        #"Звать на помощь":
        "Call for help":
            jump ss_d3help_eng
        #"Полагаться на свои силы":
        "Rely on yourself":
            jump ss_d3nohelp_eng
label ss_d3help_eng:
    #"Я стал неистово драть глотку, взывая к помощи на двух языках попеременно. Остановившись передохнуть, я услышал вскрик со стороны домиков."
    "I began screaming violently, calling for help while alternating between two languages. While taking a moment to breathe, I heard screaming come from the cabins."
    play music music_list["revenga"] fadein 2
    #th "Неужели Шурик уже добрался до неё?!"
    th "Oh god! Did Shurik already get to her?!"
    #"Эта мысль дала мне сил на решающий рывок, петля на ногах не выдержала, и я рухнул на землю."
    "The thought of this gave me enough strength for a decisive jerk, breaking the noose around my legs as I fell to the ground."
    play sound sfx_body_bump
    scene bg ext_polyana_day  with dissolve
    #"Поднявшись, я побежал к домикам. На второй раз это расстояние далось куда медленнее." with vpunch
    "I got up and ran to the cabins. This time, I covered this distance a lot slower." with vpunch
    window hide
    scene bg ext_house_of_sl_day
    show dv guilty pioneer 
    show ss serious  pij at fright
    with dissolve
    window show 
    play ambience ambience_day_countryside_ambience
    stop music fadeout 4
    #"У домиков я увидел Саманту и, почему-то, Алису. Вид у последней был сконфуженный."
    "I noticed Samantha outside the cabins, and was surprised to notice Alisa there too, looking abashed."

    #me "{en}What happened?{/en}{ru}Что случилось?{/ru}"
    me "{enen}What happened?{/enen}"
    show ss unsure pij at fright
    #ss "{en}Just another surprise on my door. Gosh, I hate frogs...{/en}{ru}Очередной сюрприз на моей двери. Ненавижу лягушек...{/ru}"
    
    $ persistent.ssg_98 = True 
    
    ss "{enen}Just another surprise on my door. Gosh, I hate frogs...{/enen}"
    show ss surprise pij at fright
    
    $ persistent.ssg_97 = True 
    
    #extend "{en}But what happened to YOU?!{/en}{ru}Но что с тобой-то случилось?!{/ru}"
    extend "{enen}But what happened to YOU?!{/enen}"
    play music music_list["you_lost_me"] fadein 5
    show ss unsure  pij at fright   
    #"Вид у меня был, должно быть, плачевный. Грязная порванная майка, ссадины, и половина леса застряла в волосах. Чтобы избежать объяснений, я начал расспросы сам."
    "Guess I looked quite deplorable; torn T-shirt covered in dirt, body covered in cuts and scratches, and half of the forest plants caught in my hair. I started making inquiries by myself to avoid unnecessary questions."
    #me "Что случилось, Алиса? О какой лягушке речь?"
    me "What happened, Alisa? Why did she mention a frog?"
    #dv "Похоже, ей кто-то жабу-ловушку на дверь приделал."
    dv "It seems like somebody put a frog-trap on her door."
    #"Уже тот факт, что Алиса не посмеялась над моим видом, вызывал большие подозрения."
    "Alisa's lack of laughter toward my appearance aroused suspicion."
    #me "Кто-то? А ты что здесь делаешь ни свет ни заря? Гуляешь?"
    me "Somebody? What are you doing here at such an early hour? Taking a walk?"
    show dv angry pioneer  with dspr
    #dv "А хоть бы и гуляю. Нельзя?"
    dv "What if I was walking? Is that prohibited or something?"
    #me "{en}Where was this girl when you opened the door?{/en}{ru}Где была эта девочка, когда ты вышла?{/ru}"
    me "{enen}Sam? Where was this girl when you opened the door?{/enen}"
    show ss sad pij  at fright
    
    $ persistent.ssg_97 = True 
    
    #ss "{en}Well... She was passing by. Forget it, it's nothing. You need to see a doctor.{/en}{ru}Ну... Мимо шла. Забудь, это ерунда. А тебе нужно к доктору.{/ru}"
    ss "{enen}Well... She was passing by. Forget it, it's nothing. And you need to see the doctor.{/enen}"
    #me "Алиса, ты точно не хочешь ничего сказать? Посмотри, ты же её до слёз довела."
    me "Alisa, are you sure that you don't wanna tell us something? Look, she is crying because of you."
    #dv "Сказала же, это не я."
    dv "I told you, it wasn't I."
    menu: 
        #"Это ты!":
        "No, it was you!":
            hide ss with dspr       
            #"Я черти что пережил за утро, чуть не погиб, можно сказать, а она тут пакостит. И кому. Добрейшему человечку - Саманте! Я вскипел."
            "I've had a shitty morning, almost died, and now she's causing mischief! And who did she hurt? Samantha, the kindest girl! I boiled."
            #me "Ну конечно! Всё сходится. Ульянка сразу невзлюбила Саманту, но она этого не скрывала, потому что нет в ней коварства. А с чего ей не любить Саманту? Тут кто-то должен был подтолкнуть."
            me "Certainly! All true. Ulyana didn't like Samantha from the beginning, but she didn't hide this; she isn't crafty. But why does she hate Samantha? Somebody made her feel this way."
            #me "И ты. Всегда пытаешься выделиться. Конечно, Саманта тебя раздражала, ведь всё внимание теперь ей. А ты улыбалась ей в глаза, натравливая Ульянку невесть какой ложью."
            me "And you. You're always trying to stand out. Naturally, Samantha would irritate you, since everyone is paying attention to her. But you smiled at her while stabbing her in the back with some lie you told Ulyana."
            window show
            #"Саманта вынесла мне халат (я всё ещё был в одних шортах и майке), и ушла в домик, оставив нас наедине. Алиса стояла, закрыв руками лицо."
            "Samantha brought me a warm bathrobe (I still was just in shorts and a T-shirt) and went back into her cabin, leaving us alone. Alisa stood with her face in her hands."
            if kiss_dv == True:
                stop music fadeout 3
                #me "Знаешь, что было вечером на пляже... Это много значило для меня, но сейчас я бы даже хотел стереть..."
                me "You know, the things that happened yesterday on the beach... They meant a lot to me; but now I just want to forget..."
                play music music_list["your_bright_side"] fadein 3
                #dv "Какой же ты дурак!"
                dv "Oh, you're so stupid!"
                show dv cry pioneer  with dspr
                #"Алиса не выдержала. Она опустилась на лавочку и не отрывая рук от лица зарыдала. Это происходило почти беззвучно, случайный свидетель мог бы подумать, что она смеётся."
                "Alisa couldn't take any more. She sat on the bench and cried into her hands. It was almost silent; an accidental bystander could mistake it for laughter."
                #th "Вот уж не ожидал увидеть Алису такой. Могу поспорить, мало кому доводилось. А всё я. Но есть ли чем гордиться?"
                th "I didn't expect to find Alisa looking like that. I'll bet that few people have ever seen Alisa cry. And it's all my doing. Should I feel proud of this?"
                #"И тут сработала магия. Я уже не помнил, что я хотел стереть из памяти, за что я её ненавижу, и ненавижу ли. Я захотел прикоснуться к ней, но имею ли право после таких обвинений? Я присел рядом. Алиса не отпрянула."
                "And something magical has happened. Thoughts of wanting to forget something vanished as well as hatred for Alisa. Did I even hate her? I wanted to touch her, but did I have the right after making those accusations? I sat down next to her; Alisa didn't move away from me."
                #"Наши плечи коснулись. Я осознавал, что здравый смысл, говоривший тащить эту девчонку к вожатой, позорно капитулировал перед её слезами. Но была ещё интуиция, и она говорила, что Алиса не виновата."
                "Our shoulders touched. I knew that common sense told me to turn this girl over to the camp leader, but her tears changed my mind. I had a gut feeling that Alisa wasn't guilty."
                #me "Прости меня."
                me "I'm sorry."
                #"И вот мы снова сидим молча. Не зная, что сказать, я попытался переосмыслить ситуацию. Если не Алиса, то кто тогда? Кого она стала бы выгораживать? Ульянку? Всё сходится."
                "We sat together again in silence. Without knowing what to say, I tried rethinking the situation. If it wasn't Alisa, then who? Who would Alisa screen? Ulyana? It all fit."
                #me "Это ведь всё Ульянка, так? Я должен поговорить с ней. Но потом."
                me "It was Ulyana, right? I should talk to her. But later."
                $ us_spy = True
                #"Алиса всё ещё пребывала в том состоянии, когда с каждым сказанным словом рискуешь разрыдаться вновь."
                "Alisa was in a state where any word could make her cry again."
                hide dv with dspr
                #"Я оставил её одну и пошёл одеться."
                "I left her alone and went to get dressed."
                window hide
                with fade2
                window show
                #"Когда я снова вышел, Алисы на месте уже не было."
                "When I returned, Alisa was gone."
                window hide
                $ renpy.pause (0.5)

                jump ss_d3later_eng
            else:
                #me "Какая-же ты..."
                me "Oh god! You are so..."
                #dv "Хватит!"
                dv "Stop it!"
                $ sp_dv -= 1
                if sp_dv <= 4:
                    show dv rage pioneer  with dspr
                    #"Гневному взгляду Алисы позавидовала бы Горгона Медуза. Хорошо хоть, не дерётся."
                    "Alisa's angry gaze would make even Medusa jealous. Well, at least she isn't fighting."
                    show dv angry pioneer  
                    #dv "Ты прав. Я завидовала, и всё такое. Пропусти к ней."
                    dv "You're right. I was envious and everything. Let me talk to her."
                    #me "Извиниться?"
                    me "Will you say sorry?"
                    #dv "Ну."
                    dv "Yep."
                    menu:
                        #"Пустить":
                        "Let her enter the house":
                            $ sp_sp += 1
                            hide dv 
                            #"Я отошёл от двери. Алиса постучала, зашла и через минуту вышла. По крайней мере, обошлось без драки. Дальше Алиса ушла, не сказав мне ни слова, а я решил зайти к Саманте."
                            "I moved away from the door. Alisa knocked, entered the house, then came back out a minute later. At least there was no fighting. Alisa left me without a word, and I decided to talk with Samantha."
                            scene bg day_sam 
                            show ss nosmile pij
                            with dissolve
                            
                            $ persistent.ssg_1 = True    
                            
                            #me "{en}What happened? Are you two okay?{/en}{ru}Что было-то? У вас всё в порядке?{/ru}"
                            me "{enen}What happened? Are you two okay?{/enen}"
                            #ss "{en}She just gave me a hug. I'm a little confused.{/en}{ru}Она просто обняла меня. Как-то неловко получилось.{/ru}"
                            ss "{enen}She just gave me a hug. I'm a little confused.{/enen}"
                            #me "{en}She had to apologize.{/en}{ru}Ей нужно было извиниться.{/ru}"
                            me "{enen}She had to apologize.{/enen}"
                            show ss surprise pij with dspr
                            #ss "{en}But what for? I don't think she did anything to me. She's a kind girl.{/en}{ru}Но за что? Уверена, что она мне никак не вредила. Она хорошая.{/ru}"
                            ss "{enen}But why? I don't think she did anything to me. She's a kind girl.{/enen}"
                            show ss unsure pij
                            #th "Она правда так думает? Если она права, то я был так несправедлив с Алисой, даже думать не хочется."
                            th "Does she really think so? If she's right, I was so unfair to Alisa that I don't even want to think about it."

                        #"Не пускать":
                        "Don't let her in":
                            #th "Она на взводе, лучше не подпускать её к Саманте. А то кто знает."
                            th "She is agitated, it's better not to let her to get to Samantha. Who knows what would happen."
                            #me "Думаю, вам лучше пока сохранять дистанцию."
                            me "I think it's better if you kept your distance."
                            $ sp_dv -= 1
                            #dv "Понятно."
                            dv "I see."
                            hide dv  with dissolve
                            #"Алиса бросила на меня последний презрительный взгляд, развернулась и ушла."
                            "Alisa took a last contemptuous look at me, turned back and walked away."
                            #th "Теперь ненавидит меня, наверное. Но какое мне дело до мнения этой змеюки?"
                            th "She probably hates me now. But I don't care about this snake's opinion."

                else:
                    show dv sad pioneer  with dspr
                    #"Алиса была близка к тому, чтобы разрыдаться. Даже будучи страшно зол на неё, этого я не хотел. Я присел на ступеньках, ожидая, пока она совладает с собой."
                    "Alisa was almost crying. Though I was very angry at her, I didn't want her to cry. I sat down on the stairs, waiting for her to calm down."
                    #"Наконец, Алиса немного успокоилась. Её раскрасневшееся лицо приняло решительный вид."
                    "Finally, Alisa calmed down. Her flushed face looked strong."
                    #dv "Неужели ты правда такого мнения обо мне? Или у тебя мозги от прилива крови повредились?"
                    dv "Do you really think I'm such a bad person? Or were your brains damaged from the rush of blood?"
                    #me "Не знаю... Погоди, какой прилив крови? Откуда ты знаешь, что я висел?"
                    me "I don't know... Wait, which rush of blood? Did you know that I was hung?"
                    #dv "Сперва скажи, веришь ты мне или нет?"
                    dv "First, answer me: Do you trust me or not?"
                    menu:
                        #"Верю":
                        "Yes, I trust you":
                            #"Я кивнул Алисе. Такой ответ её устроил."
                            "I nodded to Alisa. This suited her."
                            #dv "Ульянка вас увидела и меня позвала."
                            dv "Ulyana saw you guys, and she called for me."
                            play music music_list["raindrops"] fadein 3
                            $ sp_dv += 1
                            #me "И ты пришла меня выручать? Как мило."
                            me "And you came to help me? So cute."
                            show dv angry pioneer  with dspr
                            #dv "Нет, я думала, он тебя освежует. Нельзя пропускать такое зрелище."
                            dv "No, I thought he was going to skin you. I couldn't miss the sight of that."
                            #me "Да ладно тебе, не злись. Я и правда туговато сейчас соображаю..."
                            me "Oh come on, don't be angry. And I really can hardly think now..."
                            #dv "Но ведь это твоё обычное состояние, или ты каждое утро на турнике висишь?"
                            dv "But that's your common condition. Or do you hang upside down every morning?"
                            #me "Погоди, а что Ульянка-то тут делала? Это ведь она жабу повесила, так?"
                            me "Wait, what was Ulyana doing here? She hung the frog, didn't she?"
                            show dv guilty pioneer  with dspr
                            #"Алиса отвела глаза."
                            "Alisa looked away."
                            $ us_spy = True
                            #me "Всё сходится. И ты об этом знала? А о чём ещё? Это не она ли по ночам под окнами воет?"
                            me "All fits. So you've known about it? What else do you know? Is she the one who howls around Samantha's house at night?"
                            #dv "Я не знаю, чем конкретно она занимается. Ну шалит где-то, да. Будто ты не знал."
                            dv "I don't know what she does specifically. Well, she is naughty; it's not like you didn't already know that."
                            #me "А если бы знала, то выдала?"
                            me "But if you knew it was she, would you give her away?"
##выдать
                            show dv normal pioneer  with dspr                           
                            #dv "Я и так её выдала, получается. Даже не думай вожатой рассказывать, слышишь?"
                            dv "Well, it looks like I've already given her away. Don't you even think about telling the camp leader about this, do you hear me?"
##выдать
                            #me "И что же делать тогда?"
                            me "But what should I do?"
                            #dv "Утопить в колодце. Поговори с ней, что."
                            dv "Drown her in the well. Talk to her, what else?"
                            #me "Может лучше тебе?"
                            me "Maybe it would be better if {i}you{/i} talked to her?"
                            show dv guilty pioneer  with dspr                           
                            #dv "Ну... Нет, одна я не знаю..."
                            dv "Well... No, I don't even know what to say..."
                            #me "Ладно, поговорим, но позже. Сейчас главное - найти Шурика. Поможешь?"
                            me "Well, we'll talk to her together, but later. The most important thing now is to find Shurik. Will you help me?"
                            show dv normal pioneer  with dspr
                            #dv "Нет."
                            dv "No."
                            #me "Действительно, глупый вопрос. Ну, пока тогда."
                            me "Right, stupid question. Well, goodbye then."
                        #"Как тут поверить?":
                        "How can I trust you?":
                            #me "Всё слишком странно. Сперва расскажи свою версию, тогда может и поверю."
                            me "That's too strange. First tell me your version of what happened, and maybe I'll trust you."
                            show dv angry pioneer  with dspr
                            #dv "Раз не веришь, то смысл что-то объяснять? Я ведь снова соврать могу. {w}Пока."
                            dv "Well, if you don't trust me, I don't wanna explain anything to you. I could lie again, right? {w}Goodbye."
                            hide dv  with dspr
                            #"Алиса ушла, оставив меня в полной растерянности. Если это сделал Шурик, то почему Алиса так и не скажет? Они что, заодно? {w}Но некогда голову ломать, нужно одеваться и идти искать вожатую."
                            "Alisa left me totally perplexed. If Shurik had done that, why wouldn't Alisa tell me about it? Were they working together? {w}But there is no time to think about it. I have to get dressed and find the camp leader."


                jump ss_d3later_eng
#





        #"Вожатая разберётся":
        "Camp leader will puzzle it out":
            #th "Я запутался. Слишком много для одного утра. Алиса... Хочется ей верить. Но если врёт? А если она заодно с Шуриком? Или даже она его на всё подбила?"
            th "I'm confused. Too much has happened this morning. Alisa... I wanna trust her; but she could either be lying, working with Shurik, or she could even have incited him to do all of it."
            #me "Оставим это Ольге Дмитриевне. Я умываю руки."
            me "Let the camp leader figure it out, I'm done."
            if kiss_dv == True:
                show dv sad pioneer  with dspr
                #dv "Знаешь, я тут думала о вчерашнем. Боялась, что несправедливо к тебе отнеслась. Вот же дура. Ты такой и есть."
                dv "You know, I thought about what happened yesterday. I was worried that I'd been unfair with you. But I'm a fool; you're exactly like I thought you were."
                #me "Какой?"
                me "What do you mean?"
                #dv "Который только попользоваться. А когда речь о каком-то доверии... Куда уж мне, не заслужила."
                dv "You take whatever you want from others. But when it comes to trust... I want too much, I don't deserve it, huh?"
                #me "Ты говоришь о доверии, но сама не можешь сказать, что ты тут делала."
                me "You talk of trust, yet can't say for yourself what you're doing here."
                hide ss with dspr
                #"Саманта деликатно скрылась в домике. Алиса молчала."
                "Samantha entered her house and nicely left us alone. Alisa kept silence."
                menu:
                    #"Я тебе верю":
                    "I trust you":

                        $ sp_dv += 1
                        stop music fadeout 3
                        #me "Хорошо, буду первым. Я тебе верю. И сразу поверил. А теперь скажи честно, ты знаешь, кто это сделал?"
                        me "Well, OK, I'll go first. I trust you. I've trusted you from the beginning. Now, answer me truthfully: do you know who did this?"
                        play music music_list["raindrops"] fadein 3
                        show dv guilty pioneer  with dspr
                        #dv "Знаю..."
                        dv "Yes, I know..."
                        #me "Хм. И ты можешь устроить так, что этот кто-то пойдёт и извинится?"
                        me "Could you convince that someone into apologizing?"
                        #dv "Не уверена."
                        dv "I'm not sure."
                        #th "Ох и устал от этих шарад, голова раскалывается. Стоп!"
                        th "I'm so tired of these charades, my head is ready to blow up. Wait!"
                        $ us_spy = True
                        #me "Речь ведь об Ульянке, так? {w}Ладно, не отвечай. Я должен найти вожатую и рассказать о Шурике. С Ульянкой потом уладим."
                        me "We're talking about Ulyana, aren't we? {w}Well, don't answer. I have to find camp leader and tell her about Shurik. We'll solve Ulyana's troubles later."
                    #"Я не знаю":
                    "I don't even know":
                        #me "Это всё так странно, и вообще, у меня голова гудит. Давай потом поговорим, ладно? Извини, если что не так. Надо искать вожатую и Шурика."
                        me "This all is so weird, and generally speaking I have a headache. Let's talk later, OK? I'm sorry if something is wrong. I have to find camp leader and Shurik."
                        #th "Пожалуй, вожатой знать про жабу пока не нужно. А то Шурик, Алиса, Ульянка какая-то буйная бегает. Подумают, что это я тут спятил, обвиняю всех. Сам разберусь."
                        th "Well, perhaps camp leader shouldn't learn about the frog yet. Because of Shurik, Alisa, riotous Ulyana... everyone might think I'd gone mad and started blaming everybody. I'll deal with everything myself."
            else:
                #dv "Беги, жалуйся. Я буду у себя, высылайте воронок туда."
                dv "Hurry up, complain about me. I'll be in my cabin, you can send the police there."
                $ sp_dv -= 1
                hide dv with dspr
                #th "Странно это всё. Может, не стоит пока рассказывать? Сначала нужно поймать Шурика, это мог быть он."
                th "That's so weird. Should I wait before telling camp leader about this? First I need to find Shurik, he could be responsible for this."

    jump ss_d3later_eng
label ss_d3nohelp_eng:

    #"Я продолжил попытки освободиться. Пот стекал мне в глаза, тело ломило. Я вспомнил, что людей когда-то казнили таким образом."
    "I continued attempting to free myself. Sweat poured into my eyes, my body ached. I recalled that in older times, people were executed in this manner."
    stop music fadeout 5
    #"Через какое-то время удалось освободить руки, но на остальное не было сил. {w}Когда я уже было отчаялся и хотел звать на помощь, кто-то показался на опушке."
    "After a while I freed my arms, but didn't have enough strength to continue. {w}When I despaired, and wanted to call for help, someone appeared at the border of the forest."
    play music music_list["gentle_predator"] 
    show dv normal pioneer  with dissolve:
        yanchor 0.145
        rotate 180



    if kiss_dv == True:
        #dv "Ты чего тут болтаешься?"
        dv "Why are you hanging here?"
        #me "Это всё Шурик. Сними меня!"
        me "It was Shurik. Help me!"
        show dv smile pioneer:
            yanchor 0.145
            rotate 180          
        #dv "Заслуживаешь повисеть ещё немного. {w}Ладно."
        dv "You deserve to hang a bit more. {w}Ok."
        scene bg ext_polyana_day  with dissolve
        play sound sfx_body_bump        
        show dv normal pioneer at cright with dspr
        #"Алиса осторожно меня спустила." with vpunch
        "Alisa gently moved me down." with vpunch

    else:
        show dv smile pioneer:
            yanchor 0.145
            rotate 180  
    
        #dv "Так-так, кто тут у нас. Весь в моём распоряжении."
        dv "Well-well, what do we have here? Poor old Semyon completely at my disposal."
        #me "Сними меня!"
        me "Get me down!"
        #dv "Ну это мы ещё..."
        dv "I'll think about it..."
        if sp_dv >= 5:
            #me "Скорее! Шурик сошёл с ума, он может напасть на Саманту."
            me "Faster! Shurik's gone mad! He might attack Samantha!"
            #"Алиса поняла, что для шуток не время, и наконец..."
            show dv normal pioneer:
                yanchor 0.145
                rotate 180  
            "Alisa understood that there was no time for jokes, and finally..."
            scene bg ext_polyana_day  with dissolve
            play sound sfx_body_bump            
            show dv normal pioneer at cright with dspr 
            #"...вернула меня за землю." with vpunch
            "...brought me back on the ground." with vpunch

        else:
            #me "Скорее, дура! Шурик сошёл с ума, он может напасть на Саманту."
            me "Hurry up, you fool! Shurik's gone mad, he might attack Samantha!"
            show dv serious pioneer:
                yanchor 0.145
                rotate 180          
            #"Алиса сдвинула брови, но всё-таки потянулась к верёвке."
            "Alisa frowned, but even so, she reached for the rope."
            scene bg ext_polyana_day  with dissolve
            play sound sfx_body_bump            
            show dv normal pioneer at cright  with dspr  
            #th "{en}Ah Gravity, Thou Art A Heartless Bitch{/en}{ru}О, Гравитация, за что со мной ты бессердечна?{/ru}." with vpunch
            th "Ah Gravity, Thou Art A Heartless Bitch." with vpunch
            #me "Ты извини уж за \"дуру\"..."
            me "I'm sorry for \"fool\"..."
            #dv "Ладно. Вижу, тебе досталось тут. Рассказывай, кто тебя подвесил."
            dv "Ok. I see, you've got troubles. Tell me, who hanged you?"


    #"Я бегло обрисовал ситуацию. Не похоже, что Алиса осознала всю её серьёзность."
    "I briefly described the situation. Alisa looked like she didn't realize how grave it was."
    #me "Теперь мы должны искать Шурика. Нет, сначала Ольгу Дмитриевну."
    me "Now we have to find Shurik. No, let's find Olga Dmitrievna first."
    show dv smile pioneer at cright   with dspr
    #dv "А одеться не хочешь?"
    dv "Don't you want to get dressed?"
    #th "И правда. Я же в одних трусах и майке."
    th "Well, right. 'Cause I'm wearing only shorts and a T-shirt now."
    #"Мы пошли к домикам. Лагерь потихоньку просыпался."
    "We went to the cabins. The camp slowly woke up."
    scene bg ext_house_of_mt_day  with dissolve
    play ambience ambience_day_countryside_ambience
    #me "Слушай, а как ты в лесу оказалась?"
    me "Listen, why were you in the forest?"
    show dv normal pioneer  with dspr
    #dv "Ульянка прислала. Разбудила, дело серьёзное, мол."
    dv "Ulyana sent me there. She woke me up and said that it was a serious matter."
    #me "Хм. А она-то тут что делала?"
    me "Hm. What was she doing here?"
    #dv "Ну, это же Ульянка. Она повсюду лазает."
    dv "Oh come on, that's Ulyana. She always roams everywhere."
    #me "Всё равно странно, в такую рань. Ладно, я пойду оденусь."
    me "All the same, it's weird, at such an early hour. Well, I'll leave you to get dressed."
    #dv "Я к себе пойду. Шурики, вожатые - это без меня уже."
    dv "I'm going to my cabin. Shuriks, camp leaders - sort them out without me."
    hide dv  with dspr
    $ renpy.pause (0.5)     
    scene bg int_house_of_mt_day  with dissolve
    #"Зайдя в домик, я вспомнил, что так и не поблагодарил Алису."
    "Upon entering my house, I remembered that I hadn't thanked Alisa yet."
    scene bg ext_house_of_sl_day  with dissolve
    show dv normal pioneer 
    #"Снова открыв дверь, я увидел её на пороге домика Саманты."
    "When I came back outside, I saw her at the doorstep to Samantha's cabin."
    #me "Ты чего там делаешь?"
    me "What are you doing here?"
    #"Алиса сняла что-то с двери и показала мне. В руке она держала проволоку, на конце которой болталась жаба."
    "Alisa pulled something from the door and showed it to me. She held a wire with a frog at the end of it."
    #me "Это что?"
    me "What is that?"
    #dv "А на что похоже? Атакующая жаба. Тонкая работа. Открываешь дверь, получаешь жабу. Иногда прямо в рожу, если не повезёт."
    dv "What's it look like? Attacking frog; well-crafted. Someone could open the door and get hit by the frog. Sometimes right in the face, if they 're unlucky."
    show dv smile pioneer  with dspr
    #"Алиса хихикнула. Похоже, на её памяти кому-то не повезло."
    "Alisa giggled. It looked like she recalled someone who was once unlucky."
    #me "Это ты сделала?"
    me "Have you done that?"
    show dv normal pioneer  with dspr
    #dv "Во дурной. Я за утро уже и тебя спасла, и подружку твою. И всё равно виновата."
    dv "What a chump. I helped you this morning, and helped your friend too. And I'm still the guilty one."
    #me "Но кто тогда?"
    me "But who was it then?"
    #dv "Не знаю. Подозреваешь кого-то?"
    dv "I dunno. Do you suspect someone?"
    menu:
        #"Это Ульянка":
        "It was Ulyana":
            jump ss_usch_eng
        #"Это Шурик":
        "It was Shurik":
            #me "От Шурика теперь всего ждать можно. И ловушки - вполне в его стиле, как я убедился."
            me "I can expect anything from Shurik now. And traps are his style, as I'm now convinced."
            if sp_dv <= 5:
                #dv "Похоже на то..."
                dv "Yes, looks like this is his doing..."
                $ eto_sh = True
                #me "Ладно, мне надо срочно найти вожатую. Бывай."
                me "Well, I have to find our camp leader quickly. Goodbye."
                jump ss_d3later_eng
            else:
                show dv guilty pioneer  with dspr
                #"Алиса, похоже, не разделяла моего мнения."
                "It seemed that Alisa didn't share my opinion."
                #me "Думаешь, нет? А кто тогда?"
                me "You think it wasn't him? Who was it then?"
                jump ss_usch_eng
label ss_usch_eng:
    #me "Погоди... Ульянка! Вот что она тут делала!"
    me "Wait... Ulyana! That's what she was doing there!"
    show dv shy pioneer  with dspr
    $ us_spy = True
    #me "И ты была в курсе её проделок?"
    me "And you knew about her escapades?"
    #dv "Нет. Она мне не отчитывается. Иногда хвалится... Об этом не знала."
    dv "No, she doesn't report to me on everything she does. Sometimes she brags... But I didn't know that she'd done that."
    #me "Ты представляешь, как она могла навредить Саманте? Ведь та жаловаться лишний раз не будет."
    me "Can you imagine how much she could hurt Samantha? After all, the poor girl wouldn't even complain."
    menu:
        #"Это твоя вина":
        "It's your fault":
            $ sp_dv -= 1
            show dv sad pioneer  with dspr
            #me "Ты же знала, что что-то не так, но не уследила."
            me "You knew something was wrong, but you didn't look after Ulyana."
        #"Это и моя вина":
        "This is my fault too":
            #me "Мне надо было поговорить с ней как следует."
            me "It would be better if I properly told her."
    show dv sad pioneer  with dspr
    #dv "Ну и что теперь?"
    dv "Well, what now?"
    #me "Хм. Честно говоря, я сдал бы её вожатой. Но сейчас главная проблема - Шурик. Следи, чтобы она больше ничего не натворила, а я должен найти Ольгу Дмитриевну. А с Ульянкой потом решим."
    me "Hm. Honestly speaking, I wanna give her over to the camp leader. But the main problem is Shurik. Look after Ulyana, and don't allow her to do anything else. I'm going to find Olga Dmitrievna. We'll solve Ulyana's problem later."
##выдать
    #dv "Ладно."
    dv "OK."
    show dv angry pioneer  
    #extend " Но только попробуй сдать Ульянку!"
    extend " But don't you even think about giving Ulyana over!"
##выдать: turn in; give away; give up; give over

    jump ss_d3later_eng



label ss_d3later_eng:
    stop music fadeout 4 
    window hide
    $ renpy.pause (0.5) 
    hide dv with dspr
    $ renpy.pause (0.5)
    scene bg ext_square_day  with dissolve2

    play ambience ambience_camp_center_day
    #"Начать поиски Ольги Дмитриевны я решил с площади, ведь скоро линейка. Но там я застал только Славю с метёлкой в руках."
    "I decided that the square was the best place to start looking for Olga Dmitrievna – there soon would be a lineup, after all. But the only person I could find was Slavya, a broom in her hands."
    play music  music_list["get_to_know_me_better"] fadein 5
    show sl normal pioneer  with dspr
    #sl "Доброе утро!"
    sl "Good morning!"
    #me "Добрейшее. {w}Извини, правда ужасное утро."
    me "Best morning ever. {w}Sorry, but this morning is really terrible."
    #sl "Ты про то, что Шурик пропал? Ну, найдётся!"
    sl "You mean Shurik's missing? Well, we'll find him!"
    #me "Пропал, ага. И я чуть не пропал вместе с ним."
    me "Missing, yeah, and I almost ended up missing with him."
    show sl surprise pioneer  with dspr 
    #sl "Так ты его видел? Где?"
    sl "So you've seen him, haven't you? Where?"
    #me "В лесу. Он совсем спятил."
    me "In the forest. He's gone completely insane."
    #sl "Что случилось?"
    sl "What happened?"
    #me "Он считает всех вокруг шпионами и предателями, напал на меня... Этот парень опасен!"
    me "He's treating everyone like they're traitors and spies, he even attacked me... That guy is dangerous!"
    show sl sad pioneer  with dspr  
    #sl "Ольге Дмитриевне расскажи. Она в медпункт пошла."
    sl "You should tell Olga Dmitrievna. She went to the infirmary."
    #me "Зачем туда? У вас там оружейный погреб?"
    me "Why? Do you have an armoury there?"
    #sl "Кажется, Шурик мог заболеть."
    sl "I think Shurik got sick."
    #me "Ладно. Берегись Шурика, увидишь - бей метлой на упреждение."
    me "Fine. Beware of Shurik – if you see him, strike first with that broom of yours."
    scene bg ext_aidpost_day  with dissolve
    show mt normal pioneer at cleft 
    show mz normal glasses pioneer at cright 
    with dissolve
    #"Возле медпункта я наконец-то нашёл Ольгу Дмитриевну. К сожалению, в компании Жени. Это были не те уши, которым я хотел поведать об утренних приключениях, но что поделать."
    "I finally found Olga Dmitrievna near the infirmary. Unfortunately,  she was with Zhenya. She was not a person I would like to tell about my morning adventure, but it could not be helped."
    #"По ходу рассказа, судя по выражению лица вожатой, её посещали мысли отправить к медсестре и меня."
    "Judging by the camp leader's face while listening to the story, she was ready to send me to the nurse too."
    show mt angry pioneer at cleft 
    #mt "Нелепица какая-то. Рассказывай, что у вас за вражда с Шуриком?"
    mt "That’s just ridiculous. What's the enmity between you and Shurik? Tell me."
    #me "Да какая вражда! Это у него вражда... с рассудком."
    me "Enmity? The only enmity he's at is with his own mind!"
    #mt "Женя, а ты что думаешь?"
    mt "And what do you think, Zhenya?"
    #mz "Ну... Насколько я знаю этих двоих... {w}Семён может и выглядит как идиот, и говорит как идиот, но пусть это вас не обманет..."
    mz "Well... As far as I know those two... {w}Semyon may look like an idiot and talk like an idiot, but don’t let it deceive you..."
    window hide
    $ renpy.pause (0.4) 
    show mz laugh glasses pioneer at cright   with dspr
    window show 
    #mz "Он действительно идиот!"
    mz "He really is an idiot!"
    show mt smile pioneer at cleft   with dspr
    hide mz  with dissolve  
    #"Женя с торжествующим видом удалилась."
    "Zhenya left with triumph on her face."
    #me "Не хотите - не верьте. Поверите, когда он станет отплясывать голышом вокруг Генды, размахивая чьим-нибудь скальпом. {w}Ну хорошо, а вы что узнали?"
    me "You don’t want to believe me - fine. You will believe when you find him dancing naked around Genda with someone's scalp in his hands. {w}Anyway, what did you find out?"
    show mt normal pioneer at cleft   with dspr
    #mt "Меня разбудил Сыроежкин. Шурик пропал, говорит. А на его подушке - кровь. Это всё, в общем-то. Ищем."
    mt "I was awakened by Cheesekov. He told me that Shurik is missing, and there was blood on his pillow. That's all. Now we are searching."

    #me "Странно, что он стал вас будить из-за пропавшего из постели пионера. Только за это утро таких пропавших было несколько, включая меня."
    me "It is strange that he came directly to you just because a pioneer went missing right from his bed. There were plenty of such pioneers this morning, including me."
    #mt "Да, но они всё время вместе. С утра пораньше всегда мастерить идут. Да и кровь."
    mt "Well, yes, but those two are always together. Every day they wake up early and go contriving. And now: blood?"
    #me "А что кровь?"
    me "What about the blood?"
    #mt "Медсестра говорит, что со здоровьем у Шурика был полный порядок. А кровь может быть и из носа."
    mt "The nurse said that Shurik was completely healthy. And it could be a nosebleed."
    #me "Сейчас он в лесу. Какие планы?"
    me "He is in the forest right now. What are we gonna do?"
    #mt "На линейке организуем поисковый отряд, потом позавтракаем, потом пойдём к старому корпусу. Сыроежкин сказал, стоит искать там. Это как раз через лес."
    mt "At the lineup we'll organize a rescue team, then we'll have breakfast. Then we'll go to the old camp. Cheesekov said that it is the best place to look. It's just through the forest."
    window hide
    $ renpy.pause (0.5)
    scene cg d2_lineup  with dissolve
    window show
    #"На линейке выяснилось, что желающих идти к старому корпусу, о котором ходила дурная слава, особо нет. {w}Образовалась команда из вожатой, Электроника, Слави и меня."
    "At the lineup, it turned out that there were very few people willing to go to the infamous old camp. {w}Our team consisted of the camp leader, Electronik, Slavya and me."

    #"После линейки нас ждал обещанный завтрак."
    "After the lineup, we had our promised breakfast."
    window hide
    $ renpy.pause (0.5) 
    jump ss_d3el_eng
label ss_d3el_eng:
    scene bg int_dining_hall_day 
    show ss normal fancy at left 
    with fade
    play ambience ambience_dining_hall_full
    #"В столовой Ольга Дмитриевна схватила лишь булку, и снова куда-то убежала по организаторским вопросам."
    "In the canteen Olga Dmitrievna quickly grabbed some snacks and left again, probably due to organizational issues."
    show el normal pioneer at cright with dspr
    #"Я воспользовался моментом - позвал к нам за стол одинокого Электроника. Нужно было поговорить с ним наедине. Саманта не в счёт."
    "I took a moment – and invited lone Electronik to our table. I had to talk to him privately. Samantha didn't count."
    #"Безымянный палец его левой руки был основательно замотан бинтом."
    "His left hand's ring finger was substantially bandaged."
    #me "Робот укусил?"
    me "Robot bite?"
    #el "Да так, порезался немного."
    el "It's nothing, just a little cut."
    show ss unsure fancy at left    
    ss "{enen}I can't stand the sight of blood. {w}You think I could still be a vet?{/enen}"
    if d2_cyb == True:
        #me "Слушай, та коробка, что вы вчера мастерили, что это?"
        me "Listen, that box you were making yesterday -- what was it exactly?"
        #el "Это... полиграф. Детектор лжи."
        el "It is... a polygraph. A lie detector."
    else:
        #me "Слушай, а ты видел у Шурика такую коробку, полиграф что ли?"
        me "Listen, have you seen Shurik holding some kind of a box, a polygraph or something like that?"
        #"Электроник промедлил с ответом."
        "Electronik hesitated to answer."
        #el "Видел, и помогал ему собрать."
        el "I've seen it, and helped to construct it."

    show ss nosmile fancy at left
    #me "Как интересно. И зачем он вам?"
    me "How interesting. And why did you need it?"
    #el "Не знаю, честно говоря. Шурик загорелся сделать, ничего не хотел слушать. Он какой-то странный последнее время. Я ему помог, чтобы поскорее вернуться к работе над роботом."
    el "Honestly, I don’t know. Shurik was obsessed with this idea and I couldn’t reason with him. He's been acting a little weird lately. I helped him so we could sooner return to our robot's work."
    #me "Ясно. Ну и как, получилось у вас? Работал полиграф?"
    me "I see. And did you succeed? Was that polygraph operational?"
    #el "Да, наверное. Мы особо не успели проверить."
    el "Well, perhaps. We actually didn’t have time to check it."
    if broken_box == True:
        #me "Но ты же врёшь. Я видел, что там внутри. Это просто коробка с мусором."
        me "But you're lying. I've seen what's inside. It is just a box full of garbage."
        show el sad pioneer at cright with dspr
        #"Электроник вздохнул."
        "Electronik sighed."
        #el "Ладно. Не по душе мне всё это. Да, это всего лишь коробка. Не собрать такую вещь за день. Я подыграл Шурику."
        el "Fine. I've never liked it anyway. Yes, it's just a box. You can't construct such a thing in one day."
        #me "В чем ещё подыграл? На подушке - твоя кровь?"
        me "And what else? Was it your blood on the pillow?"
        #el "Догадался, значит. Зря я самый бесполезный палец порезал. Как на анализе крови... "
        el "You got it. Looks like I've cut my finger in vain. As for the blood test..."
        #me "Зачем?"
        me "For what?"
        #el "Напугать вожатую. Выманить, чтобы Шурик смог с тобой поговорить."
        el "To scare the campleader. To lure her out, so Shurik could speak with you."
        #me "Поговорить? Ничего себе. Ты понимаешь, насколько он опасен? Как он тебя в это завлёк?"
        me "Speak? You call it \"speak\"?! Do you have any idea how dangerous he is? How did he drag you into this?"
        show el normal pioneer at cright with dspr
        #el "Я думал, наиграется в шпионов, и мы вернёмся к роботу. Без него не справляюсь."
        el "I thought he'd soon get tired of playing spies and we would return to our robot. I can’t make it without him."
        #me "Ты знаешь, где он сейчас? Какие у него планы?"
        me "Do you know where he is now? What is his plan?"
        #el "Не знаю. Но насчёт старого корпуса... Он хотел, чтобы его там искали. Так что думай сам. Возможно, его там и нет. {w}Не сдавай меня, а?"
        el "Dunno. But about the old camp... He wanted everyone to look for him there. Think about it. Maybe he isn't there. {w}Don't tell Olga about me, please."
##выдать
        #me "Ладно уж. Иначе тебя оставят сидеть в лагере, а Шурика может и вовсе искать не будут. Ты нужен в поисковом отряде. Вот искупи свою вину теперь."
        me "If I would, you would be grounded in camp, and no one would search for Shurik at all. You are needed for the search team. Redeem yourself that way."

        show el smile pioneer at cright with dspr
        #el "Постараюсь."
        el "I will try."
    else:
        #me "Зато он проверил его на мне, и не по доброй воле. Вы для этого его делали?"
        me "But he checked it on me, and without my permission. Was that your goal?"
        #el "Не знаю, для чего. Жаль, что так вышло. Надо найти его поскорее."
        el "I didn't know what it was for. I'm sorry it turned out like that. We should find him as soon as possible."
        #me "Думаешь, он в старом корпусе?"
        me "Do you think he is in the old camp?"

        #el "Под корпусом. Он там бывал, стоит поискать. Да и больше особо негде. Но Шурик может этого ожидать."
        el "Under the old camp. He visits that place from time to time, so we should check. Actually, there is simply no other place. But Shurik may expect it."
        #th "Если он ещё совсем не спятил."
        th "If he hasn't completely lost his marbles yet."

    #"Саманта звякнула ложкой в стакане с чаем."
    "Samantha's spoon dinged in her glass."
    show el normal pioneer at cright with dspr
    stop music fadeout 5
    me "{enen}Are you not entertained, my lady?{/enen}"
    show ss smile fancy at left with dspr
    ss "{enen}No, I'm just fighting with this sugar cube.{/enen}"
    $ renpy.pause (1)   
    scene bg ext_dining_hall_away_day  with fade
    play ambience ambience_camp_center_day
    play music music_list["my_daily_life"] fadein 5
    #"После завтрака мы собрались на последний брифинг."
    "After breakfast we had our last briefing."

    #"Электроник посоветовал взять с собой фонари. Кроме того, были компасы, сухой паёк, фляги с водой, даже одеяла - всё, что может понадобиться. Кроме ружья с транквилизатором, разве что."
    "Electronik suggested taking flashlights. We also took compasses, rations, water flasks, even blankets – everything that could come in handy. Except, maybe, a tranquilizer gun."
    #"Нужно было решать, стоит ли мне идти с ними."
    "I had to decide whether I would go with them or not."
    menu:
        #"Лучше пойти":
        "Better go":
            #th "Ольга Дмитриевна недооценивает его опасность, если что случится - будет на моей совести. Да и больше искать негде."
            th "Olga Dmitrievna underestimates his danger, so if anything happens, it would be my fault. And there is no other place to search anyway."
            jump ss_dem_mines_eng
        #"Останусь в лагере":
        "Stay in camp":
            #me "Шурик угрожал Саманте. Мне кажется, он может вернуться. Я лучше останусь в лагере."
            me "Shurik threatened Samantha. I think he may return. I'd rather stay in the camp."
            show mt angry pioneer  with dspr
            #mt "Не хочешь идти - так и скажи. У нас достаточно сознательных пионеров."
            mt "If you don't want to go – just say it. We have enough conscientious pioneers."
            $ sp_mt -= 1
            $ sp_ss += 1
            show sl serious pioneer at fleft   with dspr
            #sl "Разве достаточно? А если придётся нести Шурика? Втроём мы даже носилки не утащим."
            sl "Have we? What if we need to carry Shurik? The three of us could hardly carry a stretcher."
            show mt normal pioneer 
            #mt "У нас их всё равно нет."
            mt "We don't have one anyway."
            show el normal pioneer at fright   with dspr
            #el "Есть верёвка и одеяла, а веток в лесу хватает. Что-нибудь придумаем."
            el "We have a rope and blankets, and there are enough sticks in the forest. We'll manage something."
            #sl "Семён?"
            sl "Semyon?"
            show el smile pioneer at fright   with dspr
            #el "Нам не помешает лишний фонарь."
            el "An extra set of hands never hurts."
            #th "Похоже, выбора нет. Электроник отвечает за своего друга, вожатая - за всех пионеров, но Славя тут ни при чём. Буду ей товарищем по несчастью."
            th "It seems I have no choice. Electronik is responsible for his friend; the campleader for every pioneer; Slavya has nothing to do with this. It would be cruel to leave her."
            #me "Ладно, я с вами."
            me "Ok, I'm in."
            show sl smile pioneer at fleft
            window hide     
            $ renpy.pause (0.7)             
            jump ss_dem_mines_eng
            
label ss_dem_mines_eng:
    scene bg ext_no_bus  with dissolve
    show sl smile pioneer at left   with dspr
    play ambience ambience_camp_entrance_day
    #sl "Настоящий поход, правда? Может, нам и Саманту с собой взять?"
    sl "A real hike, yeah? Maybe we should take Samantha with us?"
    show mt normal pioneer  with dspr
    #mt "Это не увеселительная прогулка. Возможно, придётся лезть под землю."
    mt "It's not a pleasure trip. We may have to crawl under the ground."
    #me "Но кто-нибудь должен за ней присмотреть."
    me "But someone must look after her."
    #"Из немногочисленных провожающих я знал только Лену."
    "Of the few attendants, I only knew Lena."
    hide sl 
    hide mt 
    with dspr
    $ renpy.pause (0.3)     
    show un normal pioneer at right   with dspr
    show un normal pioneer close at right   with dspr
    #me "Привет, ты в библиотеке сегодня?"
    me "Hi, are you going to be in the library today?"
    #un "Не знаю. Женя может меня подменить. Без вас репетиций не будет."
    un "I don't know. Zhenya could replace me. We can't rehearse without you."
    #me "Тогда присмотришь за Самантой? Да хоть пьесу почитайте."
    me "Can you look after Samantha then? You can read the play, for example."
    show un smile pioneer close at right   
    #un "Ну хорошо."
    un "Well, okay."
    #me "Только не теряй её из виду. И если Шурик вдруг появится... пусть держится подальше."
    me "Just keep an eye on her. And if Shurik appears... let's keep him away."
    #th "Лучше её особо не пугать."
    th "Better not to scare her."
    window hide
    $ renpy.pause (0.5) 
    scene bg ext_path_day  with dissolve
    #"Мы вышли из ворот, прошли немного по дороге и свернули на лесную тропинку."
    "We came out of the gate, went a bit down on the road and turned onto a forest path."
    play ambience ambience_forest_day
    #"Впереди шёл Электроник, показывая дорогу, за ним Ольга Дмитриевна. "
    "Electronik went ahead, showing the way, followed by Olga Dmitrievna. Slavya kept at a distance, so I kept her company."
    window hide
    show sl normal pioneer at cleft   with dspr
    $ renpy.pause (0.6)
    window show
    #me "Как думаешь, что случилось с Шуриком?"
    me "What do you think happened to Shurik?"
    #sl "Не знаю. Это же ты его видел, а не я. Кстати, ты уверен, что это был он?"
    sl "No idea, you were the one who saw him, not me. By the way, are you sure it was him?"
    #me "О чём ты? Думаешь, что-то надело на себя Шурика?"
    me "What do you mean? You think something has put on Shurik as a costume?"
    #sl "Ну, он же был в маске."
    sl "I mean, was he wearing a mask?"
    #me "Ах, это. Уверен."
    me "Ah, this. Yes, I'm sure."
    #th "Разве я говорил, что он был в маске? Не помню... Не говорил я ей, точно нет."
    th "Did I tell her about the mask? Can't remember... No, I didn't tell her, that's for sure."
    menu:
        #"Разоблачить её":
        "Unmask her":
            #me "Я не говорил, что он был в маске. Откуда знаешь?"
            me "I haven't told you that he was wearing a mask. How do you know?"
            #sl "Ульяна сказала. Она видела его у твоего дома утром. Если это был он."
            sl "Ulyana told me. She saw him at your house in the morning; if it was him."
            if us_spy == True:
                #th "Вот так просто. Хорошо, что я не накинулся на Славю с обвинениями."
                th "So simple. Good thing that I didn't accuse Slavya of anything."
            else:
                #me "Хм. А что она сама там делала в такую рань?"
                me "Huh. And what was she doing there so early?"
                show sl smile pioneer at cleft   with dspr
                #sl "Не знаю. Но это же Ульяна. Она ничего не пропустит!"
                sl "I don't know, but it's Ulyana. She doesn't miss anything!"
                #th "Ульянка была там... Возможно, жаба - её работа. А Алиса защищала подругу. Как нехорошо получается."
                th "Ulyana was there... Perhaps the frog-trap was her job. And Alisa defended the friend. How inconvenient."
                #sl "О чём задумался?"
                sl "What are you thinking about?"
                if eto_sh == True:
                    #me "Да так, об утреннем. Шурик может быть не единственной проблемой в лагере."
                    me "Nothing serious. Shurik might be not the only problem in the camp."
                else:
                    #me "Похоже, я незаслуженно обидел кого-то."
                    me "It seems that I unfairly hurt someone."
                    show sl sad pioneer at cleft   with dspr
        #"Выжидать":
        "Wait":
            #th "Сложно поверить, но если она в этом замешана, то лучше пока не подавать вида. Нужны доказательства."
            th "Hard to believe, but if she's involved in this, it's better not to show that I know. I need some proof."
            $ sl_bad = True
    #mi "Шурик нашёлся!"
    mi "... Shurik!.. Found!.."

    show sl surprise pioneer at cleft   with dspr
    #th "Вот тебе и на. Где это он нашёлся?"
    th "That's unexpected. Where was he found?" 
    #"Наша процессия остановилась, чтобы подождать Мику, которая стремительно приближалась по тропинке." 
    "Our procession paused to wait for Miku, who was rapidly approaching the path."
    show mi surprise pioneer far at right    with dspr
    show mi surprise pioneer at right    with dspr
    #mi "Нашёлся?"
    mi "Was Shurik found?"
    #th "Тьфу, ты!"
    th "Damn it!"
    #sl "Нет, не нашёлся ещё."
    sl "No, he wasn't."
    show mi normal pioneer at right   with dspr
    #mi "А я вам аптечку принесла. А то вы самое главное-то забыли. Вдруг пригодится. Обязательно пригодится! То есть, лучше бы не пригодилась, конечно. Держи, Семён!"
    mi "I brought you a first-aid kit. You forgot the main thing! It will be useful, I'm sure about it! I mean, it would be better not useful, of course. Take it, Semyon!"
    show mi smile pioneer at right   with dspr
    #"Мику взвалила мне на плечо тяжеленную аптечку, и это помимо основного вещмешка. Вот удружила." with hpunch
    "Miku shouldered the heavy first-aid kit on me in addition to the main haversack. Thank you so much!" with hpunch
    #mt "Молодец, Мику! Ты с нами?"
    mt "Attagirl, Miku! Are you with us?"
    #mi "Да, конечно!"
    mi "Yes, of course!"
    hide mi 
    hide sl 
    with dspr
    stop music fadeout 5
    #"Идти стало громче, идти стало веселее. К счастью, через несколько сотен шагов лес закончился."
    "When Miku joined us, we started to walk more loudly and cheerfully. Fortunately, a few hundred paces later the forest ended."

    scene bg ss_old_building_day  with dissolve
    #"Мы вышли к старому корпусу, похожему на классический дом с привидениями. Мы с опаской подошли и заглянули внутрь. Тонированные пылью и грязью окна почти не пропускали света."
    "We came to the old camp building, which looked like a typical haunted house. We approached cautiously and looked inside. Windows tinted with dust and dirt hardly let light through."
    #mt "Вперёд."
    mt "Let's go."
    scene bg int_old_building_night  with dissolve

    $ persistent.sprite_time = 'night'
    $ night_time()
    play music music_list["just_think"] fadein 3
    show el serious pioneer far at cright   with dspr
    #el "Видите, тут люк вниз. Он-то нам и нужен."
    el "See, there's a hatch, which seems to lead underground. That's what we need."
    show mi scared pioneer far at cleft   with dspr
    #mi "Ой, там совсем темно. И сыро. И слизняки."
    mi "Oh, over there is quite dark. And damp. {w}Ew! I see slugs down there!"
    #el "У нас фонари."
    el "We have good flashlights."
    #mi "Нет-нет, не полезу. Извините."
    mi "No-no, I'm not going there. Sorry."
    #"От страха Мику даже примолкла."
    "Miku even fell silent because of fear."
    show mt normal pioneer far at fright   with dspr
    #mt "Тогда оставайся тут, что ли. Вдруг Шурик появится."
    mt "Then stay here, I guess. Shurik might appear."
    #me "Это не лучший сценарий."
    me "That's not the best option."
    #mi "Оставаться одной в этом ужасном доме?!"
    mi "Stay in this dreadful house?!"
    menu:
        #"Я её провожу":
        "I can walk her out":
            show mt angry pioneer far at fright   with dspr
            #mt "Семён! Ты нам тут нужен! А Мику спокойно дойдёт одна."
            mt "Semyon! We need you here! Miku can handle it."
            menu:
                #"Согласиться":
                "Agree":
                    #me "Пожалуй..."
                    me "Perhaps..."
                    show mt normal pioneer far at fright   with dspr
                    jump ss_mines1_eng
                #"Настаивать":
                "Insist":
                    #me "Нельзя отпускать её одну в лес, где спятивший Шурик бродит. Я пойду с ней, а потом вернусь."
                    me "You can't let her go alone in the woods, where the mad Shurik is wandering. I'll go with her and then come back."
                    show mi shy pioneer far at cleft   with dspr
                    #mi "Ах, это так благородно с твоей стороны!"
                    mi "Oh, that is so noble of you!"
                    #mt "Подводишь нас. Не возвращайся тогда, ждать не будем."
                    mt "You let us down. Don't come back then, we won't wait for you."
                    show sl serious pioneer far at fleft   with dspr
                    #sl "Всё в порядке, мы справимся. Идите."
                    sl "It's okay, we'll handle it. Go."
                    #th "Это она в пику Ольге Дмитриевне."
                    th "She is doing this to spite Olga Dmitrievna."
                    $ sp_mt -= 1
                    jump ss_miwalk_eng
        #"Решайте сами":
        "Decide it yourself":
            #mi "Ладно, я пойду в лагерь. Извините."
            mi "Okay, I'll go to the camp. I'm sorry."
            show mt normal pioneer far at fright   with dspr
            #mt "Мы бы тебя проводили, но каждый человек на счету."
            mt "We would have walked you out, but every person counts."
            jump ss_mines1_eng

label ss_mines1_eng:
    hide mi  with dspr
    play ambience ambience_catacombs
    #"Отпустив Мику, мы двинулись в подземелье. Через несколько ступенек вниз по лестнице мы увидели открытую дверь."
    "Letting Miku go, we went underground. A few steps down the stairs we saw an open door."
    scene bg int_catacombs_living  with dissolve
    #"На удивление, за ней оказалось не царство Мышиного Короля, а вполне приличного вида комната. Если бомбоубежище может состоять из одной комнаты, то это оно и было."
    "Surprisingly, this wasn't the kingdom of the Rat King, but quite a decent room. If a bomb shelter would consist of a single room, this would be it."
    show el upset pioneer at right   with dspr
    #el "Столько полезных вещей."
    el "So many useful things."
    #me "Но нет вредного Шурика. Ты тут в первый раз?" 
    me "But no harmful Shurik. Have you been here before?"
    #el "Да. Шурик рассказывал, что тут есть выход в туннели. Видимо, за этой дверью."
    el "No. Shurik told that there is access to the tunnels. Apparently, behind this door."
    show mt normal pioneer  with dspr
    #mt "И что в тех туннелях?"
    mt "And what's in those tunnels?"
    #el "Хм. Он не говорил."
    el "Hm. He never told that."
    #mt "Тогда узнаем сами. Вперёд."
    mt "Then we can learn that ourselves. Let's go."
    scene bg int_catacombs_entrance  with dissolve
    #"Теперь я был замыкающим. Некоторое время мы шли по узкому коридору, пока шедшая во главе цепочки Ольга Дмитриевна не сказала \"стоп\" и тут же \"ох\"."
    "I was the last in the chain now. We were walking along a narrow corridor when Olga Dmitrievna, who was marching at the head of the chain, said \"stop\" and then immediately..."
    window hide
    play sound sfx_alisa_falls
    scene cg d4_catac_sl  with dissolve
    #"Выглянув из-за спины Слави, я обнаружил, что нас осталось только трое."
    "Peering out from behind Slavya, I found out that only three of us remained."
    #"Электроник склонился над зияющей дырой в полу почти во всю ширину туннеля. Вперёд дороги не было."
    "Electronik bent over the gaping hole in the floor that was almost the entire width of the tunnel. There was no path ahead."
    #mt "Я в порядке. Мягкая посадка. Давайте сюда."
    mt "I'm fine. A soft landing. Come down here."
    scene bg int_catacombs_hole  with dissolve
    #"Электроник смело устремился вниз."
    play sound sfx_body_bump    
    "Electronik bravely rushed down."
    #"Уровнем ниже был другой туннель, похожий на шахтёрский. Он шёл в обе стороны. Я спустился сам и помог Славе."
    "A level down, there was another tunnel, like a mine shaft. It went in both directions. I went down, then helped Slavya."
    scene bg int_mine  with dissolve
    $ renpy.pause(0.5)  
    show mt normal pioneer  with dspr
    #mt "Смотрите, что я нашла. Та самая маска, Семён?"
    mt "Look what I found. Is this the mask, Semyon?"
    #me "Да, та самая лиса."
    me "Absolutely, yes."
    #mt "Значит, мы на верном пути. Надо идти дальше."
    mt "So we're on the right track. We have to go further."
    show sl normal pioneer far at left   with dspr
    #sl "Только куда нам теперь?"
    sl "But where should we go?"
    show el normal pioneer at right   with dspr
    #el "Я не знаю. Позовём Шурика? {w}Шуууурииик!"
    el "I don't know. Should we call Shurik? {w}Shuuuriiik!"
    #"На призыв звать Шурика откликнулось только эхо, но этого было более чем достаточно."
    "Only echoes responded to him, but that was enough."
    #me "Спугнёшь его только."
    me "You'll only scare him."
    window hide
    $ renpy.pause(1)
    #mt "Разделимся. Затем и нужна большая команда. Пойдём парами в обе стороны. Кто с кем?"
    mt "Let's split up. That's why a big team is needed. Pairs can go in both sides. Who with whom?"
    show sl normal pioneer far at fleft   with dspr

    #"Славя немного отодвинулась от Ольги Дмитриевны, и оказалась рядом со мной."
    "Slavya slightly moved away from Olga Dmitrievna and appeared next to me. "
    show mt smile pioneer  with dspr
    stop music fadeout 3
    #mt "Ладно. Мужчина должен быть в каждой связке, не так ли? Далеко не уходите, если будут ещё развилки - лучше возвращайтесь. Удачи."
    mt "Alright. A man should be in each pair, shouldn't he? Don't go far away, if you meet a fork in the road - it's better to return. Good luck."
    window hide
    $ renpy.pause(0.3)  
    hide mt  with dspr
    $ renpy.pause(0.1)  
    hide el  with dspr
    $ renpy.pause(0.8)
    window show  
    play music music_list["door_to_nightmare"] fadein 3 
    #"С уходом нашей невозмутимой вожатой и Электроника, стало немного жутковато."
    "With the departure of our imperturbable camp leader and Electronik, it became a little creepy."
    #me "Идём?"
    me "Shall we go?"
    #sl "Идём."
    sl "Let's."
    show sl normal pioneer  with dspr
    #"Этот туннель был шире предыдущего. Посередине шли рельсы, а сбоку оставалось место, чтобы шахтёр мог разминуться с летящей вагонеткой. Впрочем, на месте этого несчастного шахтёра, я бы ещё подумал."
    "This tunnel was wider than the previous one. There were rails in the middle, there was also enough space for a miner to avoid a riding trolley. However, in the place of the unfortunate miner, I would have thought twice."
    #me "...И как они тут работали?"
    me "...And how could they work in here?"
    #"Окончание мысли вырвалось у меня вслух."
    "I blurted out the end of my thoughts."

    #sl "Кто? Шахтёры?"
    sl "Who? Miners?"
    #me "Да. Я бы на такое добровольно не пошёл."
    me "Yes. I wouldn't go for it voluntarily."
    scene bg int_mine_halt  with dissolve
    #sl "Но кто-то должен. Иногда выбора работы нет. А человек должен работать."
    sl "But someone has to. Sometimes there is no choice of work... But a person must work, so..."
    menu:
        #"Согласиться":
        "Agree":
            jump ss_d3mend_eng
        #"Поспорить":
        "Argue":        
            #me "А если не должен?"
            me "And what if they mustn't?"
            show sl serious pioneer  with dspr
            #sl "Как это?"
            sl "How is it possible?"
            #me "Допустим, если вся работа сделана."
            me "For example, if all the work is done."
            #sl "Вся никогда не сделана."
            sl "All the work is never done."
            show sl normal pioneer  with dspr
            #me "Обломова помнишь? В школе проходят. Вот не нужно ему ничего. Лежит себе, никого не трогает. Чем он плох, почему его осуждают?"
            me "Do you remember Oblomov? We read it in school. He doesn't need anything. Lives alone, touches nobody. What's wrong with him and why is everyone trying to condemn him?"
            #sl "Но на него крестьяне работали. Это не нормально. Вот и кончилась эпоха таких как он. Теперь все работают."
            sl "But peasants worked for him. That's not good. The era of people like him has ended. Everybody works now."
            #me "А если и крестьяне не нужны? Допустим, роботы всё делают."
            me "And what if we didn't need peasants? Let the robots do everything."
            #sl "Нет. Нельзя без работы. Так люди просто вымрут, даже если их не трогать. Вот что им делать? Без работы какая скука будет, прямо ложись и помирай."
            sl "No. People can't live without work, they will vanish. What should they do? This life is so boring without the work, just lie and die."
            #me "Узнавать что-то новое, мечтать... Любить."
            me "Learn something new, dream... Love."
            #sl "Разве не затем нам всё это, чтобы побуждать к действиям? А если ничего не делать, не приносить никаких жертв, а только пропускать через себя - грош всему цена."
            sl "Don't we need all these things to act? If we do nothing, don't sacrifice anything, then it's not real."
            #me "...Удивительно."
            me "...Interesting."
            #sl "Что?"
            sl "What?"
            #me "Мы сейчас в таком страшном месте. И вот просто так болтаем."
            me "We're in such a creepy place. Yet we are just chatting."
            show sl smile pioneer 
            #sl "Потому и не страшно."
            sl "That's why it's not creepy."
            window hide
            $ renpy.pause(1)
            #me "Хорошо, ну а в раю что, по-твоему? Тоже все с тяпками и мотыгами?"
            me "Okay, and what do you think is in heaven? That everyone is with hoes and mattocks too?"
            show sl surprise pioneer  with dspr         
            #sl "В раю?"
            sl "In heaven?"
            #me "Ты не веришь в рай, наверное, но представь идеальное место. Что бы ты хотела там видеть? Непаханые поля?" 
            me "You probably don't believe in heaven, but imagine a perfect place. What would you like to see there? Untilled land?"
            show sl normal pioneer  with dspr
            #sl "Хм. Наверное, всё как у нас, но чтобы всегда был второй шанс. Было бы неплохо."
            sl "Hm. Maybe similar to our world, but... with a second chance, if you need it. {w}That would be nice."
            show sl smile pioneer  
            #sl "Ха. Анекдот вспомнила. Какой ад лучше - капиталистический или социалистический? Конечно, социалистический! То спичек нет, то котёл на ремонте, то у чертей партсобрание."
            sl "Ha. Here is an anecdote. Which hell is better - capitalist or socialist? Of course, the socialist one! Matches are gone, boiler is off for repair, devils are having a party meeting."

##Внимание, анекдот!
##A young, ruthless executive died and went to hell. When he got there, he saw one sign that said Capitalist Hell, and another that said Socialist Hell. In front of the Socialist Hell was an incredibly long line, while there was no-one in front of the Capitalist Hell. So the executive asked the guard, "What do they do to you in Socialist Hell?" "They boil you in oil, whip you, and then put you on the rack," the guard replied. "And what do they do to you in Capitalist Hell?" "The same exact thing," the guard answered. "Then why is everybody in line for Socialist Hell?" "Because in Socialist Hell, they're always out of oil, whips, and racks!"
##"Ha. Here is the anecdote. Which hell is better - capitalist or socialist? Of course, the socialist one! Because they're always out of oil, whips, and racks!"

            #me "Не ожидал от тебя политических анекдотов."
            me "Didn't expect you to tell political anecdotes."
            #sl "Почему?"
            sl "Why?"
            menu:
                #"Ты же девушка":
                "You are a girl after all":
                    $ sp_sl += 1
                    #me "Девушек обычно другие вещи интересуют. Наверное."
                    me "Girls are usually interested in other things, I guess."
                    #sl "И как..."
                    sl "And how..."
                    play sound sfx_boat_impact                  
                    hide sl with dspr
                    
                    #"Славя споткнулась и инстинктивно схватила меня за руку. Некоторое время не отпускала, шли уже молча." with hpunch
                    "Slavya stumbled and instinctively grabbed my arm. She kept holding it for a while as we walked in silence." with hpunch
                #"Ты такая правильная"
                "You are so right-minded":
##right-minded citizen; Mr. Proper
                    # "Мне казалось, ты скорее сама окажешься на партсобраниях, чем будешь шутить про них."
                    "I thought you would rather find yourself at party meetings than joke about them."
                    #sl "Но ведь одно другому не мешает."
                    sl "But one does not interfere with another."

            jump ss_d3mend_eng
            
            
label ss_d3mend_eng:
    $ renpy.pause (0.5)
    window show 
    scene bg int_mine_crossroad  with dissolve
    #"Мы подошли к развилке. Туннель расходился на два идентичных, не оставляя нам никакой подсказки."
    "We came to a fork. The tunnel divided into two identical ones, leaving us with no clues."
    show sl sad pioneer  with dspr
    #sl "Разделимся?"
    sl "Should we split up?"
    #me "Ни в коем случае. Идём назад?"
    me "Absolutely not. Let's go back."
    #sl "Нет. Значит, просто пошли направо."
    sl "No. Let's keep going right, then."
    show sl normal pioneer  with dspr
    #me "А не заблудимся?"
    me "Won't we get lost?"
    #sl "Нет, мы будем идти всё время направо, а если решим вернуться, то развернёмся и всё время налево."
    sl "No, we'll keep right all the time, and if we decide to come back, we'll just turn around and keep left." 
    scene bg int_mine_halt  
    with dissolve   
    #me "В мифе про лабиринт Минотавра до такого не додумались, дорогие нитки тратили."
    me "They didn't hit upon it in the myth of the Minotaur's labyrinth and spent expensive threads."
    scene bg int_mine_crossroad  with dissolve
    #"На пути нам попались ещё несколько развилок. Направо, так направо."
    "We came to some more forks on the way. Well, right is fine too."
    scene bg int_mine_door  with dissolve

    #"А вот и деревянная дверь. Возможный конец нашего путешествия. Я поднажал, и она отворилась."
    "And here's a wooden door. A possible end of our journey. I pressed, and it opened."
    window hide
    play sound sfx_open_door_mines
    scene bg int_mine_room  with dissolve
    $ renpy.pause (0.5)
    show sl normal pioneer  with dspr   
    #"Мы стояли в центре небольшой комнаты с исписанными стенами и кучами мусора на полу. Никого. Пусто."
    "We stood in the middle of a small room with scribbled walls and piles of garbage on the floor. Nobody. It was empty."
    show sl surprise pioneer  with dspr
    #sl "Смотри, тут лестница наверх. Ну-ка. Там небо!"
    sl "Look, here's a ladder leading upstairs. Hey, there's the sky!"
    scene bg ss_int_mine_exit with dissolve
##    #th "Там что, ночь уже? Должно быть, какой-то обман зрения."
##    th "Wait, is it night outside? It must be some kind of optical illusion."
    stop music fadeout 5
    #"От свободы нас отделяла солидная преграда - решетчатый люк, закрытый на мощный навесной замок снаружи."
    "A solid barrier kept us from freedom - a latticed hatch with a powerful padlock on the outside." 
    #me "Судя по следам на ржавчине, решётку недавно открывали. Да и замок явно новый. Думаешь, Шурик вылез и повесил замок?"
    me "Judging by the marks in the rust, this hatch has been opened recently. But the padlock is shiny; it's obviously new. Do you think Shurik got out and changed the lock?"
    #sl "Возможно. Но не обязательно, мы же не все коридоры осмотрели."
    sl "Maybe. But that's not definite; we didn't examine all the corridors."

    #me "Ну и что нам теперь делать?"
    me "So what should we do now?"
    play music music_list["smooth_machine"] fadein 5
    #sl "Придётся вернуться тем же путём. Но сначала давай устроим привал. Можно перекусить."
    sl "We have to retrace our steps. But first, let's have a rest for a meal."
    scene bg int_mine_room with dissolve
    show sl normal pioneer with dspr
    #"Произведя ревизию продуктов, мы уселись на видавшем виды матрасе возле стены, стали жевать и болтать."
    "After inspecting the goods, we sat on a battered mattress near the wall and began to chew and chat."
    #sl "Ты можешь себя представить лет через 10? Каким ты будешь?"
    sl "Can you imagine yourself in 10 years? What would you become?"
    #me "Примерно таким же, наверное."
    me "About the same, I guess."
    #sl "Как же так? Но что-то же изменится?"
    sl "How so? Something would change, wouldn't it?"
    #me "Сейчас я спокоен. То есть, день сегодня беспокойный, конечно. Но в целом. Кажется, что я именно там, где должен быть. {w}Не знаю, что будет через 10 лет, но спокойствия не будет."
    me "Now I am calm. Well, yes, this day is restless, but in general. It seems that I am exactly where I should be. {w}I don't know what will happen in 10 years, but there won't be peace in my mind."
    #sl "Что помешает быть спокойным через 10 лет?"
    sl "What's the problem with being calm in 10 years?"
    #me "Ну, это как дамоклов меч взрослой жизни. Жизнь уже не катится словно по рельсам, как это было в детстве. Нужно будет делать выбор, а как? Просто идти вправо, как делаешь ты? Возможно, я из тех, кто просто остаётся на месте."
    me "It's like the sword of Damocles of adult life. Life won't run itself as it did in childhood. You'll have to make a choice, but how? Just \"keep going right\" as you do? Maybe I'm one of those who would just get stuck in place."
    show sl smile2 pioneer  with dspr
    #sl "Буриданов осёл?"
    sl "Buridan's donkey?"
    #me "Да. Меч, осёл. ...Минотавр. Древние греки нами довольны."
    me "Yes. Sword, donkey. ...Minotaur. Ancient Greeks are satisfied with us."
    #sl "Прохладно тут. Хорошо, что свитер захватила."
    sl "It's a bit cold here. Good thing I took a sweater."
    me "I feel okay."

    window hide
    scene cg ss_d3mines  with dissolve
    #sl "Неохота возвращаться в шахты. Спать хочется..."
    sl "Don't want to return to the mines. I feel sleepy..."
    #"Славя положила голову мне на плечо. Вдыхая запах её волос, я поймал себя на мысли, что поход удался. Время шло."
    "She laid her head on my shoulder. Breathing in her hair's smell, I caught myself thinking that the hike was absolutely successful. Time moved on."

    if sl_bad == True:
        #me "Слушай. Помнишь, разговор в лесу... А как ты узнала, что Шурик был в маске?"
        me "Listen. The conversation in the forest... How do you know that Shurik was wearing a mask?"
        #sl "Разве не ты мне сказал?"
        sl "Didn't you tell me that?"
        #me "Точно не я."
        me "Certainly not me."
        #sl "Странно."
        sl "Weird."
        stop music fadeout 1
        #me "Ну да не важно."
        me "Well, no matter."
        play music music_list["orchid"] fadein 3
        #sl "Это почему же? А если мы с Шуриком заодно?"
        sl "Why? What if I'm with Shurik?"
        #me "Сложно представить."
        me "That's difficult to imagine."
        #sl "А то, что я сама на него эту маску и надела?"
        sl "And what if I put that mask on him myself?"
        #me "Но зачем бы ты стала мне это говорить?"
        me "But why would you tell that to me?"
        scene bg int_mine_room  with dissolve
        show sl happy pioneer  with dspr
        #sl "А какая разница, отсюда уже никуда не денешься. Добрая сестрица заманила дурачка в ловушку."
        sl "It doesn't matter, you can't get out of here anyway. Good sissy lured the fool into a trap."
        #me "Ловушка? Сестрица? Что ты говоришь такое?!"
        me "Trap? Sissy? What are you talking about?!"
        #sl "Ha! Даже не догадывался. Не многовато ли у нас голубоглазых и белокурых в лагере? Совпадение? Ну и видел бы ты свою рожу сейчас!"
        sl "Hа! Not even a clue. Don't you think there are too many blue-eyed and blond in this camp? Coincidence? I wish you could see your face now!"
        #sl "А я сюда, в эту гниль и грязь, просто так попёрлась, по-твоему? Затхлым воздухом подышать? Нет, мозги включить тебе не дано. Да не дёргайся, дело уже сделано."
        sl "And you think I came in this rot and dirt just for nothing? To breathe some stale air? No, you definitely can't use your brain. And don't fret, the business is done already."
        #me "Дело!? Да я..."
        me "Business?! I will..."
        #sl "Что ты сделаешь, тряпка? Жаловаться побежишь?"
        sl "What will you, milksop? Make a complaint?"
        $ renpy.pause(1)        
        scene black  with fade

    stop music fadeout 3
    scene bg int_mine_room 
    show unblink    
    play sound sfx_dinner_horn_processed
    $ renpy.pause(1)
    show sl surprise pioneer  with dspr 
    #sl "Проснись, Семён! Слышал? Сигнал на обед!"
    sl "Wake up, Semyon! Did you hear that? It's the signal for lunch!"
    window hide
    $ renpy.pause(1)
    if sl_bad == True:  
        #th "Приснится же такое!"
        th "What a dream!"
    #me "Что? Да мы уже пообедали..."
    me "What? But we had a lunch..."
    #sl "Но сигнал! Мы рядом с лагерем! Давай позовём, вдруг кто-нибудь услышит."
    sl "But the signal! We are near the camp! Let's call for help, someone may hear us."
    window hide
    $ renpy.pause(0.5)  
    scene bg ss_int_mine_exit  with dissolve
    #"Мы поднялись к решётке и начали кричать."
    "We climbed up to the hatch and started to shout."
    window hide 
    $ renpy.pause(2)
    #us "Чего буяните?"
    us "What's all the fuss about?"
    #sl "Ульяна! Позови кого-нибудь из взрослых. Пусть нас выпустят."
    sl "Ulyana! Call one of the adults. Have them release us."
    #us "А я чем плоха?"
    us "Adults? Am I no help for you?"
    #me "Если сладишь с этим замком - ничем."
    me "You are, if you can deal with the padlock."
    window hide
    $ renpy.pause(1)    
    play sound sfx_alisa_picklock
    window show
    #"Ульянка минут 20 ковыряла замок проволокой, пока не сдалась и не уступила место поварихе с ломиком."
    "Ulyana messed with the lock for 20 minutes, but then gave up and gave way to the cook, who had a crowbar."
    window hide
    play sound sfx_open_metal_hatch
    $ renpy.pause(1)    
    $ persistent.sprite_time = 'day'
    $ day_time()
    scene bg ext_square_day  with dissolve
    show sl normal pioneer at left   with dspr
    show us normal pioneer  with dspr
    $ renpy.pause(0.5)  
    window show 
    play ambience ambience_camp_center_day
    play music music_list["eternal_longing"] fadein 3
    #"Мне стало немного стыдно, что даже не попытался выломать решётку сам. Тем не менее, мы были на свободе. А именно - на площади, люк скрывался в траве за статуей Генды."
    "I felt a little ashamed; I didn't even try to break down the hatch on my own. Nevertheless, we were free. Namely - at the square. The hatch was in the grass behind the Genda statue."
    show us grin pioneer  
    show sl shy pioneer at left
    with dspr
    #us "А вы чего там делали? Укромный уголок искали?"
    us "What were you doing in there? Looking for a secluded corner?"
    #me "А ты чего тут делала? Прямо как ждала нас."
    me "And what were you doing here? Looked like you were waiting for us."
    show us dontlike pioneer  with dspr
    #us "Знаменитая благодарность Семёна. Я из-за вас обед пропустила, между прочим."
    us "Semyon's famous gratitude. I missed lunch because of you!"
    show sl normal pioneer at left   with dspr
    #sl "У нас тут от сухого пайка осталось, держи."
    sl "We've got some dry rations left, take them."
    jump ss_aftermines_eng

label ss_aftermines_eng:
    show sl normal pioneer at left   with dspr
    show us normal pioneer  with dspr
    #sl "Кстати, Ульян, а Шурик не нашёлся?"
    sl "By the way, have you found Shurik?"
    #us "Так он вернулся давно уже."
    us "He returned a long time ago."
    #me "Что же ты молчала?! Столько времени потеряли! Где он?"
    me "Why didn't you say something?! We've lost so much time! Where is he?"
    #us "А ты не спрашивал. В кружке своём небось, где ему быть ещё."
    us "You didn't ask. Probably in his club, where else could he be?"
    #sl "А где он был? И как он всё объяснил?"
    sl "And where was he? And how did he explain everything?"
    #us "Сказал, что на лодке ходил за дальний остров. Рыбу ловить. Врёт он."
    us "Said he went fishing over at the far island. He's lying, I think."
    hide sl  with dspr
    #me "Идём! ...Славя?"
    me "Let's go! ...Slavya?"

    #"Славя сидела на лавочке и потирала ногу."
    "Slavya sat on a bench, rubbing her leg."
    #sl "Кажется, подвернула всё-таки в шахтах. Думала, пройдёт. Хорошо, что назад идти не пришлось."
    sl "Looks like I sprained it in the mines. Thought it would be alright. Good thing we didn't have to go back."
    #me "Так тебе в медпункт надо. Как не вовремя!"
    me "You need to get to the infirmary. What bad timing!"
    $ sl_ft = True
    #us "Ой, ладно, иди за своим Шуриком. Я провожу её в медпункт. Только не деритесь там без меня."
    us "Meh, go for your Shurik, I'll help her. Just don't fight without me."
    stop music fadeout 5
    #"Но сначала я решил заскочить в библиотеку, проведать Лену с Самантой." 
    me "Thank you! I'll go."
    window hide
    $ renpy.pause(0.5)
    hide us with dspr
    $ renpy.pause(0.3)  
    window show 
    "But first, I decided to go to the library to check on Lena and Samantha." 
    window hide
    $ renpy.pause(0.5)  
    play ambience ambience_library_day  
    scene bg int_library_day  with dissolve
    show un surprise pioneer  with dspr
    #un "Вернулись?"
    un "Have you come back?"
    #me "А ты чего тут одна? Где Саманта?"
    me "Why are you alone? Where is Samantha?"
    #un "Ну... Только ты не злись. Она в порядке."
    un "Well... Just don't get mad. She's alright."
    show un scared pioneer 
    #me "Где?!"
    me "Where?!"
    #"От испуганной Лены ответа была не добиться."
    "I coudn't get any answer from the scared Lena."
    show un surprise pioneer  
    #me "Ладно, я не злюсь. Где она?"
    me "Okay, I'm not mad, calm down. {w}Where is she?"
    #un "С Шуриком."
    un "With Shurik."
    #th "Ну да, где же ещё."
    th "That was expected."
    #me "Спасибо, хорошо присмотрела!"
    me "Thank you, you looked after her so well!"
    show un serious pioneer  with dspr
    #"Переборов желание грязно выругаться, я поспешил покинуть библиотеку."
    "I overcame the desire to swear and hurried to leave the library."
    un "Semyon, wait."
    menu:
        "Stay": 
            $ sp_un += 1
            me "What?"
            #un "Ты ошибаешься насчёт Шурика. Он увлечён...но не так, как ты думаешь."
            un "You're wrong about Shurik. He is obsessed... But not in the way you think."
            me "What do you mean? Obsessed with what?"
            un "With Samantha."
            #me "Откуда тебе знать вообще? Он на меня напал, и на неё нападёт."
            me "How do you know? He attacked me, and will attack her."
            #un "Не нападёт. А ты пострадал, потому что он... ревнует. Только сам не понимает."
            un "He won't. You have suffered because he was... jealous. He himself just doesn't understand it."
            #me "Читай меньше любовных романов. Хотя, это уже Фрейд какой-то."
            me "Try to read less love stories. Although this is some Freud's stuff already."
            window hide
            $ renpy.pause(0.5)  
        "Run":
            th "Not now..."


    scene bg ext_library_day with dissolve
    #"И вот я второй раз за день показываю свои лучшие скорости."
    "I need to show my best speed for the second time this day."
    play music music_list["awakening_power"]
    scene bg ext_clubs_day  with dissolve
    #"Кружок кибернетиков. Только бы не пусто... Не пусто!"
    "Cybernetics club. Please, don't be empty... It's not!"
    play sound sfx_open_door_strong
    play ambience ambience_clubs_inside_day
    scene bg int_clubs_male_day  with dissolve
    show sh surprise pioneer  with dspr
    #"Когда дверь отворилась, передо мной предстала следующая картина: за столом Шурик и Саманта, юный кибернетик прилаживает к ней проводки из своей любимой коробки правды."
    "When the door opened, the following scene appeared before me: Shurik and Samantha together, the young cybernetician attaching wires to her, which were coming from his beloved box of truth."
    menu:
        "First hit, then ask":
            play sound sfx_punch_washstand

            #"Не медля ни секунды, я что есть силы двинул Шурику в челюсть, да так, что он улетел под стол." with hpunch
            "Not wasting a second, I hit him in the jaw so hard that he fell under the table." with hpunch
            hide sh with dspr
            #"Должно быть, со стороны это смотрелось эффектно. И пусть скажет спасибо, что не по очкам."
            "Spectacular move! And he should be grateful for that I didn't break his glasses."
            show ss scared fancy at right with dspr
            #"Саманта вскочила и в ужасе забилась в угол комнаты. Кажется, опасалась, что двинься она оказывать первую помощь, и её может ждать та же участь. Вот до чего её Шурик довёл!"
            "Samantha jumped up in horror and huddled in the corner of the room. It seemed she feared not only for Shurik, but also for her own fate."
            "Second wave of anger hit me: regardless of my actions, this was all Shurik's fault!"
            #"Особо не церемонясь, я взял её за плечи и вытолкнул в дверной проём. Шурик, похоже, был побеждён, так что можно было покинуть поле боя."
            "Without further ado, I took Sam by the shoulders and pushed her through the doorway. Shurik seemed to be defeated, so I could leave the battlefield."
            scene bg ext_clubs_day  with dissolve
            play ambience ambience_camp_center_day
            play sound sfx_close_door_campus_1
            show ss vangry fancy with dspr
            ss "{enen}What are you doing?! Are you insane?!{/enen}"
            show ss angry fancy 
            me "{enen}Just saving you. He spied on you and almost killed me this morning. He's crazy.{/enen}"
            show ss surprise fancy with dspr
            ss "{enen}But why? He was nice to me, and you just run in and hit him? Let me go, we must help him, no matter what!{/enen}"
            hide ss with dspr
            #"Саманта сделала пару шагов в сторону клуба, но я быстро нагнал её и загородил проход."
            "Samantha made a couple of steps toward the club, but I quickly caught her up and blocked the passage."
            me "{enen}Where are you going?{/enen}"
            show ss angry fancy             
            ss "{enen}Let me go. Even if you don't like each other, we must help him.{/enen}"
            show ss sad fancy  with dspr
            menu:
                "Let her go":
                    th "I can't hold her against her will."
                    stop music fadeout 5
                    me "{enen}Go ahead then. But I warned you.{/enen}"
                    hide ss with dspr
                    #"Теперь я был зол на них обоих, поэтому не стал дожидаться Саманты и пошёл к себе."
                    "Now I was angry at both of them, so I didn't wait for Samantha and went to my cabin."
                    jump ss_d3aftersh_eng
                "Forbid her":
                    me "{enen}I won't let you in. Go to your room, and I'll check if he needs help.{/enen}"
                    $ sp_sp -= 1
                    $ sp_ss -= 1
                    show ss angry fancy with dspr
                    window hide
                    $ renpy.pause (0.6) 
                    window show                 
                    ss "{enen}You are mean.{/enen}"
                    stop music fadeout 5                    
                    #"Саманта была очень сердита, но всё-таки послушалась и пошла к себе."  
                    "Samantha was very angry, but still obeyed and went to her cabin."                 
                    hide ss with dspr
                    #"Немного выждав, я заглянул в окно. Шурик сидел за столом, держась за щеку. Жить будет. Посчитав своё обещание выполненным, я пошёл к себе. Нужно отдохнуть и собраться с мыслями."
                    "A short time later, I looked into the window. Shurik was sitting at the table, holding his cheek. He'll be alright. Considering my promise to Samantha fulfilled, I went to my house. I needed to relax and gather my thoughts."
                    window hide
                    $ renpy.pause (0.4)
                    jump ss_d3aftersh_eng
                "Go with her":
                    scene bg int_clubs_male_day  with dissolve
                    play sound sfx_open_door_strong
                    play ambience ambience_clubs_inside_day
                    show sh serious pioneer 
                    stop music fadeout 7                    
                    #"Шурик уже пришёл в себя. Похоже, желанием дать сдачи он не горел. Саманта осмотрела его раны. Синяку - быть, а так - ничего серьёзного. "
                    "Shurik had already come to life. It seemed that he didn't desire to fight back. Samantha examined his wounds. There would be some bruises, but nothing serious."
                    show ss serious fancy at fright with dspr                  
                    ss "{enen}Don't you want to say something?{/enen}"
                    me "{enen}He knows it was deserved.{/enen}"
                    $ sh_adm = True
                    $ sp_sp += 1                   
                    #"Шурик отвернулся. Какое-то время мы стояли молча. К счастью, Саманта не стала требовать от нас рукопожатий, она кивнула и мы оставили Шурика одного."
                    "Shurik turned away, and we kept silent for a while. Fortunately, Samantha did not require us to shake hands, she nodded, and we left him alone."
                    scene bg ext_clubs_day  with dissolve
                    play sound sfx_close_door_campus_1
                    play ambience ambience_camp_center_day
                    #"Я рассказал Саманте про утро и наш поход. Во всяком случае, она согласилась, что извиняться пока рано. Нужно подождать Ольгу Дмитриевну и её суда. {w}Мы разошлись по домикам."
                    "I told Samantha about the morning, and she agreed that it was too early to apologize. We had to wait for Olga Dmitrievna and her judgement."
                    window hide
                    $ renpy.pause(0.5)
                    scene bg ext_house_of_mt_day with dissolve
                    "We went to our cabins."
                    jump ss_d3aftersh_eng

        "STAHP right there criminal scum!":
            show ss surprise fancy at right with dspr
            #me "Попался, сукин сын! Ты что делаешь?!"
            me "Got you, you son of a witch! What are you doing?!"
            show ss unsure fancy  at right            
            show sh upset pioneer  with dspr
            #sh "Эээ. Испытываем новый прибор. Саманте интересно."
            sh "Uh... We are testing our new device. Samantha is interested in it."
            if broken_box == True:
                show sh rage pioneer  with dspr
                play sound sfx_chair_fall
                show ss surprise fancy at fright 
                #"Я выломал заклеенную крышку по второму разу. Позорные внутренности \"прибора\" показались наружу."
                "I broke the sealed up cover for the second time. The shameful insides of the \"device\" were exposed."
                "At least Samantha was on my side now."

                $ sp_ss += 1
                $ sp_sp += 1
                show ss serious fancy at fright with dspr
                stop music fadeout 5               
                #sh "Эй! Ну и что? Он ещё не закончен..."
                sh "Hey! So what? I haven't finished it yet..."                   
                #me "Бесполезно, Электроник тебя сдал. Говори правду, иначе попадёт и ему. Имей смелость уже."
##выдать; done
                me "Useless. Electronik told me everything. He had the courage to tell the truth. And you'd better behave the same."
                show sh upset pioneer  with dspr
                #"Шурик поник, былая воинственность исчезла."
                "Shurik drooped, and the former bellicosity was gone."
                play music music_list["you_won_t_let_me_down"]
                sh "Okay, I admit that  I was following you and so on. At night, as well."
                me "AND?"
                sh "In the morning after our... meeting... I threw the mask into the mines, and then returned to the camp by boat."
                #me "Ты правда думал, что я тебя не узнаю? Ещё и со всеми этими штуковинами? Ты вёл себя, как безумец."
                me "Did you really think I wouldn't recognize you? With all that stuff? You behaved like a madman."
                #sh "Но я никому плохого не сделал."
                sh "But I didn't hurt anyone."
                #me "Ну да, только подвесил меня, Славя ногу подвернула, а те двое всё ещё где-то в шахтах бродят. По-хорошему, теперь их искать надо."
                me "Yeah, you just hung me, Slavya sprained her leg, and those other two are still somewhere in the mines. Now we have to look for them, in fact."
                #sh "Хорошо, я сейчас же пойду искать их."
                sh "Okay, I'll go and look for them right now."
                #me "Но зачем ты всё это затеял?"
                me "But why did you start all this?"
                $ sh_adm = True
                #sh "Я ошибался. Со мной бывает такое, зацикливаюсь на чём-нибудь. Лена тут ещё мне книжку про шпионов дала... А вот познакомился с Самантой поближе... Извините."
                sh "I was mistaken. Sometimes I become obsessed with something. Also, Lena gave me a book about spies... And now I got to know Samantha better... Sorry."
                if us_spy == False:
                    #me "Кстати, это ты Саманте жабу на дверь повесил?"
                    me "By the way, was it you who hung the frog-trap on the Samantha's door?"
                    #sh "Нет-нет. Ульянка, наверное. Я её несколько раз видел. То в дверь из рогатки выстрелит, то ещё что. На стене мелом гадость написала - я стёр." 
                    sh "No-no. I guess it was Ulyana. I saw her a couple of times. Shooting with a slingshot at the door or something. Wrote some bad stuff on the wall - I cleaned it."
                    $ us_spy = True
                    #me "Ну смотри, если врёшь. А чего не поговорил с ней, или не пожаловался кому?"
                    me "You'd better not lie to me. And why didn't you talk to her or complain to someone?"
                    show sh serious pioneer  with dspr
                    #sh "Она бы меня тоже выдала. А так у нас было вроде негласного договора."
                    sh "She'd betray me too. So we had a kind of tacit agreement."
                me "I need to think about it... {w}If you really want to find them, take the flashlight."
                me "{enen}Samantha, let's go.{/enen}"
                scene bg ext_clubs_day  with dissolve
                play sound sfx_close_door_campus_1
                play ambience ambience_camp_center_day
                show ss serious fancy               
                #"Мы с Самантой вышли из клуба, я поведал ей суть нашего разговора."
                "Samantha and I left the club, and I told her the gist of our conversation."
                ss "{enen}So it was him who made noises and frightened me?{/enen}"
                me "{enen}Not sure. But it ends today. I promise.{/enen}"

                #"Мы разошлись по домикам. Неплохо бы немного отдохнуть."
                "We went to our houses. Would be nice to relax."
                jump ss_d3aftersh_eng


            #me "Не юли, ты спёкся. Рассказывай всё."
            me "You've lost, tell me everything."
            #sh "Что рассказывать?"
            sh "What everything?"
            #me "Да хоть где ты был сегодня. Начиная с утра."
            me "Where were you today. Starting with the morning."
            #sh "Ходил рыбу ловить. Хотел записку оставить, написал вот, но забыл из кармана выложить."
            sh "I went fishing. I wanted to leave a note, even wrote it, but forgot to take it out of my pocket."
            #me "Ну-ну. И много рыбы поймал?"
            me "Well, well! And was the fishing good?"
            show sh serious pioneer  with dspr
            #sh "Пару рыбёшек. Только отпустил потом."
            sh "Caught a couple of fish, but released them afterwards."
            me "Damn you, Olga Dmitrievna will handle this. Samantha, let's go."
            stop music fadeout 5            
            scene bg ext_clubs_day  with dissolve
            play sound sfx_close_door_campus_1
            play ambience ambience_camp_center_day
            show ss serious fancy               
            #"Я рассказал Саманте про наш поход. Про утро она всё ещё не знала, не хотелось её пугать лишний раз. А теперь неплохо бы отдохнуть и привести себя в порядок.{w} Мы разошлись по домикам."
            "I told Samantha about our hike. She still doesn't know about the morning, and I don't want to scare her. For now it would be nice to relax and freshen up.{w} We went to our cabins."
            jump ss_d3aftersh_eng

            
label ss_miwalk_eng:            


    stop music fadeout 4
    #"Я передал аптечку Электронику, и мы с Мику с позором двинулись обратно."
    "I gave the medkit to Electronik, and along with Miku we walked back in disgrace."
    $ persistent.sprite_time = 'day'
    $ day_time()
    scene bg ext_path_day  with dissolve
    show mi normal pioneer  with dspr
    play ambience ambience_forest_day
    #mi "Брр, ну и домище. А что, Шурик и правда так опасен? Может напасть на кого-то?"
    mi "Ough, what a house. So, is Shurik really that dangerous? Could he really attack someone?"
    play music music_list["lightness"] fadein 3
    #me "По идее, на тебя ему нападать не за чем. Но кто знает. Ты тоже иностранка."
    me "Well, I don't think he'd have a reason to attack you. But still, who knows? You're a foreigner, too."
    show mi scared pioneer  with dspr
    #mi "А он нападает на иностранок?"
    mi "Does he attack foreigners?"
    #me "Пока нет."
    me "Not yet."
    show mi normal pioneer with dspr
    #mi "Значит, просто плохое предчувствие?"
    mi "So, just a bad feeling?"
    #me "Можно и так сказать. Нет, не насчёт тебя, не пугайся. Просто мне кажется, нет его там."
    me "You may put it that way. No, not about you, don't worry. I just suspect that he is somewhere around, not underground."
    #mi "Ну да. Зачем же лезть в темноту и сырость, если его там нет? Ладно бы в какое хорошее место."
    mi "Sure thing. Why go to such a dark and scary place at the first place? If that place was nice, it would be a different story."
    #th "Так. Будь я на месте Шурика, куда бы я..."
    th "Alright. So, if I were Shurik, where would I…"
    show mi smile pioneer  with dspr
    #mi "Один раз я залезла в конюшню к лошади с жеребёнком, и пряталась в их сене почти весь день. Думаешь, Шурик мог бы прятаться в конюшне?"
    mi "Once I got into a stable with mares and foals, and I hid in the hay for nearly a whole day. Do you think Shurik might be hiding in a stable?"
    #me "Что? Тут нет конюшни, насколько я знаю."
    me "What? There isn't a stable here, as far as I know."
    #mi "Но если была бы? Все любят лошадей. Да, здорово было бы. Надо подать идею Ольге Дмитриевне."
    mi "But what if there were? Everyone loves horses. Yeah, that would be great. We should give the idea to Olga Dmitrievna."
    #th "На месте Шурика..."
    th "If I were Shurik..."
    #mi "Я пью чай с одним кубиком сахара. А могла бы с двумя. Думаешь, мне отдадут разницу в рафинаде, если я попрошу? Лошади так любят сахар!"
    mi "I drink tea with one sugar cube. But I could take two. Do you think they'll give me the difference in lump sugar if I ask? Horses just love sugar!"
    #"Мы вышли на опушку леса. Поход занял не так много времени. Может, стоит вернуться? Нет, хватит с меня этого леса."
    "We reached the edge of the forest. Our hike didn't take long. I thought that we should go back, but by now I'd had enough wandering."
    scene bg ext_houses_day  with dissolve
    show mi sad pioneer 
    #mi "Вот и пришли. Спасибо за компанию и защиту. Хотя, ты меня сам и напугал. Сегодня меня все пугать пытаются. Лена вот. Начитается всяких ужасов."
    mi "Here we are. Thanks for the company and protection, even though you did scare me yourself. Today everyone's been trying to scare me. Earlier, Lena read me a horror story..."
    #mi "Про то, как в США во время Второй Мировой американских японцев в лагеря сгоняли. Вот зачем мне такое рассказывать, а? Нет бы что-нибудь хорошее."
    mi "About Japanese-Americans being held in camps during World War II. Why would anyone tell me such a thing? I'd rather hear anything else but that!"
    #me "Историей интересуется, наверное."
    me "Maybe she's just interested in history."
    show mi normal pioneer  with dspr
    #mi "Ага. Надо бы в клуб зайти. А ты куда теперь?"
    mi "Right. I should go to the music club. And where are you going?"
    #me "Буду искать Шурика. Ещё где-нибудь."
    me "I'll search for Shurik. Maybe he's somewhere else."
    #mi "Нужна помощь? Нет? Но ты обязательно сообщи, если он найдётся. Или если не найдётся. Интересно же."
    mi "Do you need help? No? Well, at least tell me if you find him. Or if you don't. It's very intriguing."
    #me "Хорошо, сообщу."
    me "Okay, will do."
    window hide
    $ renpy.pause(0.3)  
    hide mi  with dspr
    $ renpy.pause(0.6)      
    play ambience ambience_day_countryside_ambience
    stop music fadeout 3
    #"Куда теперь? Для начала, я решил проведать Лену с Самантой. Значит, путь мой лежит в библиотеку."
    "Where should I go now? I decided to start by visiting Lena and Samantha, so I made my way to the library."
    scene bg ext_square_day  with dissolve
    play music music_list["revenga"] fadein 3
    #"Подходя к площади, я встретился с пионером. Он шёл от лодочной станции. Блондин...в очках. Шурик! Идёт себе с с самодовольным видом."
    "Approaching the square, I spotted a pioneer walking from the boat house. Blonde… with glasses. Shurik! He was casually passing by with a smug look on his face."
    show sh smile pioneer far  with dspr
    show sh scared pioneer  with dspr
    #"Заметив меня, он явно растерялся и замедлил шаг. Я же стал шарить глазами вокруг в поисках подходящего оружия."
    "When he noticed me, he was clearly caught by surprise and slowed a bit. I started to look for something to use as a weapon."
    menu:
        #"Драться":
        "Fight":
            window hide
            scene cg ss_battleshurik  with dissolve
            #"Я бросился на Шурика и повалил его в пыль. {w} Какое-то время мы катались без особого ущерба друг для друга, пока он не ткнул меня коленом в пах." with vpunch
            "I rushed Shurik and wrestled him to the ground. {w} We brawled for a while without significant results until he kicked me in the groin." with vpunch
            scene bg ext_square_day  with dissolve
            #"На этом бой и окончился. Шурик немного отполз и стал поправлять очки."
            "That ended the fight. Shurik crawled out from under me and adjusted his glasses."
            #me "Хана...тебе. В тюрьму пойдёшь...или в дурдом. Или в особый тюремный дурдом."
            window hide
            $ renpy.pause(0.5)  
            show sh upset pioneer  far at left with dspr
            window show         
            me "You are… finished. You'll go to jail… Or asylum. Or special jail-asylum."
            #sh "Не знаю, о чём ты. Это тебе в дурдом дорога. Идёшь себе с рыбалки спокойно, а тут бросаются всякие."
            sh "I don't know what are you talking about... You're the one who belongs in an asylum. I was peacefully returning from fishing when some madman attacked me."

            #me "Мне-то можешь не врать уже."
            me "Stop lying; at least to me."
            jump ss_shfish_eng

        #"Говорить":
        "Talk":
            #me "Попался, террорист? А мы тебя обыскались. Откуда путь держим?"
            me "Got you, terrorist. We've been turning this place upside-down looking for you! Where have you been?"

            show sh upset pioneer  with dspr
            #sh "О чём речь? Ой, я же записку забыл выложить. Ну, что рыбачить пошёл. Нехорошо получилось."
            sh "What? Oh, I must have forgotten to leave a note. Well, I just went fishing. My bad."
            #me "Кому ты гонишь, кот Базилио? Думаешь, не узнал я тебя?"
            me "Who do you think you're lying to, Br'er Fox? Do you really think I don't recognize you?"
##Братца Лиса можно заменить на Пинокио (Pinocchio).
            jump ss_shfish_eng

label ss_shfish_eng:
    #sh "Ты меня с кем-то спутал. Или у тебя с головой плохо."
    sh "You mistake me for someone else. Or you are sick in the head."
    if broken_box == True:
        #me "Можешь не отбрыкиваться, твой друг сдал тебя с потрохами."
        me "Spare me your excuses. Your friend already snitched on you."
##выдать
        show sh surprise pioneer  with dspr
        #sh "Да? Так себе друг, значит."
        sh "Yeah? Not much of a friend, huh?"
        #me "Ну так кто врёт, он или ты?"
        me "So who's lying, he or you?"
        #sh "Мне нечего тебе сказать, и незачем. Ты даже не вожатый."
        sh "I have nothing to say to you. And I don’t have to. You are not the camp leader, after all."
    #me "Ну что же, подождём вожатых тогда. Вряд ли Ольга Дмитриевна в хорошем настроении вернётся."
    me "Well, in that case we'll just wait for the camp leader! I don't think Olga Dmitrievna will be happy when she gets back."
    show sh serious pioneer  with dspr
    #sh "Подождём, так подождём. А теперь оставь меня в покое."
    stop music fadeout 5    
    sh "So be it. Now leave me alone."  
    #"После этой непродуктивной беседы я всё-таки дошёл до библиотеки."
    $ renpy.pause (0.3)
    hide sh with dspr
    $ renpy.pause (0.5)     
    "After that idle talk, I headed for the library."
    window hide
    $ renpy.pause (0.3)     
    play ambience ambience_library_day
    scene bg int_library_day  
    show un normal pioneer 
    show mz normal glasses pioneer at right
    show ss normal fancy at left    
    with dissolve   
    #"Здесь я застал не только Саманту с Леной, но и Женю. Они склонились над большим листом картона."
    "Samantha and Lena were here, along with Zhenya. They were leaning over a large sheet of cardboard."
    #me "Нашёлся Шурик. Что тут у вас?"
    me "I found Shurik: he is in the camp."
    ss "{enen}How is he? Is everything all right?{/enen}"
    me "He is all fine, unfortunately... {w}What are you guys doing?"
    play music  music_list["everyday_theme"] fadein 5
    #ss "{en}We're making our own Monopoly game.{/en}{ru}Мы делаем Монополию.{/ru}"
    ss "{enen}We're making our own Monopoly game.{/enen}"
    #"На картонке красовалась схематичная карта «Совёнка». Тут был и Генда, и медпункт, и даже \"Линейка\"."
    "On the cardboard was a scaled-down map of \"Sovyonok\". There were Genda and the Infirmary, and even \"Lineup\"."
    #th "Домик Алисы и Ульяны - \"шанс\". Интересно."
    th "House of Alisa and Ulyana - \"chance\". Interesting."
    #me "А почему в роли \"тюрьмы\" музыкальный клуб?"
    me "But why is the music club depicted as \"prison\"?"
    #mz "Что-то должно быть тюрьмой."
    mz "Something had to be the prison."
    #me "Почему не библиотека? Лена тут сидит в заточении."
    me "Why not the library? Lena is sitting here imprisoned."
    show un shy pioneer 
    #un "Да нормально..." with dspr
    un "It's ok..." with dspr
    #me "А с гостиницами и отелями как быть?"
    me "And what will you do about hotels?"
    #mz "Не решили ещё. Вот и придумай."
    mz "We haven't decided yet. Why don't you come up with something?"
    #th "Чего это она? За \"идиота\" стыдно небось."
    th "Me? To come up? What's wrong with her? Ashamed of \"idiot\"?"
    #me "Да я только проведать заскочил. Пойду посмотрю, что там с Шуриком. Как он врать всем будет."
    show ss smile2 fancy at left
    ss "{enen}With Sam we will finish this work in no time!{/enen}"
    me "{enen}Sorry, but no, I just dropped in to see you. I have to go. I need to see what's with Shurik and how he's going to lie to everyone.{/enen}"
    show ss nosmile fancy at left   
    window hide
    $ renpy.pause (0.6)
    scene bg ext_library_day  with fade2
    #"Новость о его возвращении уже облетела лагерь. Я выяснил, что сейчас он в медпункте. По поводу крови объясняется, наверное."
    "News about Shurik's return flew around the whole camp. I found out that he was at the infirmary. He was probably reporting about the blood."
    scene bg int_musclub_day  with dissolve
    #"Я решил зайти с новостями к Мику, но она уже будто забыла о Шурике, и была полностью поглощена изучением журнала о конном спорте."
    show mi smile pioneer with dspr 
    "I decided to visit Miku with the news, but it looked like she'd already forgotten about Shurik, absorbed in reading a magazine about equestrian sports."
    #mi "Список участников забега: Пройдоха Младший, Буду К Вечеру, Всё На Рыжего, Адмирал... Адмирал? Вот выпендрились."
    mi "List of participants: Little Rascal, Back By Evening, All On Red, Admiral... Admiral? So original."
    play sound sfx_dinner_horn_processed
    #"К вечеру я бы выучил все имена призовых лошадей, если бы не сигнал на обед."
    "If it hadn't been for the lunch call, I would've learned the names of all the prize horses by evening."


    scene bg int_dining_hall_people_day  with dissolve
    show un normal pioneer at fleft 
    show mi normal pioneer at center 
    show ss normal fancy at fright
    play ambience ambience_dining_hall_full
    #"В столовой мы встретились с Леной и Самантой и сели вчетвером. Сегодня была уха. Нужно ли сделать вывод, что сегодня четверг? Впрочем, какая разница."
    "In the canteen we met Lena and Samantha and sat together. That day we were having fish soup. Does it mean it was Thursday? Well, no matter."
    #mi "Всё-таки невероятная история с этим твоим письмом, Саманта. А если бы не дошло? Нужно было послать сразу пять ли десять. А если разным лидерам?"
    mi "But still the story about your letter is incredible, Samantha. What would've happened if it never arrived? Maybe it would be better if you sent five or ten at once. What do you think would happen if you sent them to different leaders?"
    #ss "{en}That wouldn't be fair.{/en}{ru}Это не очень честно.{/ru}"
    ss "{enen}That wouldn't be fair.{/enen}"
    #mi "Ну а что. Для хорошего дела же."
    mi "Why? It's for the common good."
    #me "Некоторые только таким и занимаются. Целые организации. Пишут незнакомым людям, звонят им. Надеюсь, для таких есть особое местечко в аду."
    me "A lot of people do just that. Whole organisations. They write to unfamiliar people, phone them. I hope there is a special place in hell for such people."
    #un "Ты про тех, кто анонимки пишет? Неужели целые организации..."
    un "Are there really whole organizations that send people anonymous letters?"
    #me "Нет, про рекламу. Саманта знает."
    me "No, I'm talking about advertising. Samantha knows."
    show un sad pioneer at fleft   with dspr
    #un "На моего деда как-то пришла анонимка."
    un "My grandfather was accused by an anonymous letter once."
    show ss serious fancy at fright  with dspr
    #mi "Мы тоже в школе анонимки пишем! Так интересно. Как-то написала одному дурню \"Приходи к качелям. Будем целоваться. Прекрасная незнакомка.\" И он пришёл! А я не пришла, ха-ха."
    mi "We write anonymous letters at school, too! It's so exciting! Once I wrote to one simpleton, \"Come to the swings. We will kiss. Signed, Beautiful stranger.\" And he came! But I didn't, ha-ha!"
    show mi laugh pioneer at center    with dspr
    #mi "Ну, то есть, пришла, конечно. Посмотреть, пришёл он или нет. Но он-то пришёл!"
    mi "Well, I mean, I came, of course, just to see if he would. But he did!"
    show ss normal fancy at fright  with dspr
    #ss "{en}What happened next{/en}{ru}И что было дальше{/ru}?"
    ss "{enen}And what happened next?{/enen}"
    show mi normal pioneer at center    with dspr
    show un shy pioneer at fleft   with dspr
    #mi "А? Да ничего, сбежал он. Я же с подружкой пришла. Всегда так делаю, если какой растяпа свидание назначает. Отказывать неудобно, а так поделом им. Всегда сбегают почти сразу."
    mi "Hm? Well, nothing, he ran away. I brought a girl with me. I always bring friends when a weirdo wants a date. It's uncomfortable to refuse, but this way it's easier. They almost always run straight away!"
    #ss "{en}And if a cool guy asks you out{/en}{ru}А если классный парень зовёт на свидание{/ru}?"
    ss "{enen}And if a cool guy asks you out?{/enen}"
    #"Я осознал, что так много времени провожу в сугубо женском обществе, что меня перестают стесняться. Возможно, уже и за особь мужского пола не считают. Через неделю стану ходячей вешалкой для белья."
    "I realized that I was spending so much time in a purely female society that they weren't ashamed around me anymore. They probably no longer saw me as a man. In a week I'd become a walking clothes rack."
    window hide
    $ renpy.pause (0.6)
    scene bg ext_dining_hall_near_day  with fade
    play ambience ambience_camp_center_day
    stop music fadeout 5    
    #"После обеда я оставил Саманту с Леной, а сам хотел продолжить слежку за Шуриком, но он засел один в своём клубе и ничего интересного не происходило. Я решил пойти отдохнуть."
    "After lunch I left Samantha with Lena. I intended to shadow Shurik, but he went alone to the cybernetics club and wasn't coming out. Instead, I decided to relax for a bit."

label ss_d3aftersh_eng:
    scene bg int_house_of_mt_day 
    with dissolve
    #"По правде, хотелось спать, но не стоит брать послеобеденный сон в привычку, не в детском саду уже. Не помешало бы пойти освежиться. Я направился к умывальникам."
    "In truth, I wanted to sleep. But I shouldn't get used to the afternoon nap; I'm not in kindergarten anymore. I decided to freshen myself up and headed to the washing stands."
    window hide
    $ renpy.pause (0.5)
    play ambience ambience_day_countryside_ambience
    scene bg ext_washstand_day  with dissolve
    $ renpy.pause (0.5) 
    play sound sfx_borshtch
    play music music_list["i_want_to_play"] fadein 2
    #"Там я увидел Ульянку, набиравшую воду в трёхлитровую банку."
    "I sighted Ulyana, who was filling up a three-liter jar with water."
    #th "Какую-нибудь шалость готовит, но лучше не спрашивать."
    th "She's probably coming up with some prank, but it's better not to ask."
    #me "Привет."
    me "Hi."
    show us normal sport  with dspr
    #us "Тьфу, чего подкрадываешься?! Ну, как там твой Шурик?"
    us "Ugh, why are you sneaking around?! Hey, how's your Shurik?"
    if sh_adm == True:
        #me "Сознался. Теперь пусть Ольга Дмитриевна с ним разбирается."
        me "He confessed. Now we'll let Olga Dmitrievna deal with him."
        show us laugh sport  with dspr
        #us "Но рожу-то ты ему начистил? Или он тебе, хыхых."
        us "But didn't you break his face? Or did he break yours? Hehehe!"
    else:
        if broken_box == True:
            #me "Есть улика - этот его сломанный детектор лжи. Не отмажется. Может, и ты чего знаешь?"
            me "I have evidence - his broken lie detector. He won't get out of this one. Maybe you know something too?"
        else:
            #me "Изображает невинную овечку. Надеюсь, Ольга Дмитриевна ему не поверит. Может, ты чего знаешь?"
            me "He's acting like he's an innocent lamb. I hope Olga Dmitrievna won't believe him. Maybe you know something?"

        show us normal sport  with dspr
        #us "А если и знаю, что я, стукачка что ли?"
        us "Even if I do, I'm not a snitch!"
##выдать
        #me "То есть, не быть стукачкой важнее правды? Даже если другие пострадают?"
        me "You think it's better not to be a snitch than to tell the truth? Even if others will suffer for it?"
        show us calml sport  with dspr
        #us "Может быть. Ничего ведь ужасного не случилось."
        us "Maybe. After all, nothing bad happened."
        #me "Как знать, когда Саманта замешана - любая шалость может иметь серьёзные последствия."
        me "You don't know that. When it comes to Samantha, every prank could have a serious consequence."
        show us angry sport  
        #us "Пф. Тоже мне, пуп мира."
        us "Pf! Come on, what is she? The navel of the world?"
        show us normal sport  with dspr
        #me "Мне с тобой нужно серьёзно поговорить о Саманте."
        me "You and I need to have a serious talk about Samantha."
        #us "На вот, неси банку. А по дороге можно и поговорить."
        us "Here you are, carry the jar for me. And while we walk, we can talk."
        jump ss_d3dvus_eng
    show us normal sport  with dspr
    #me "А где Алиса, кстати?"
    me "And where is Alisa, by the way?"
    #us "Сегодня почему-то дома сидит ведь день."
    us "She's been lounging at the cabin all day long for some reason."
    if us_spy == True:
        #me "Вот и хорошо. Нам надо бы поговорить втроём, если ты не против. Пошли к вам?"
        me "That's good. It would be better if all three of us had a talk, if it's fine with you. Let's go to your cabin."
        #us "Звучит не очень. Ну пошли."
        us "Sounds not too tempting. Well, let's go."
        jump ss_d3dvus_eng
    #me "Чего это с ней? Заболела?"
    me "What's with her? Is she sick?"
    #us "Не знаю, пойди и спроси."
    us "I don't know, ask her."
    if dv_inv == True:
        #me "Пожалуй, так и сделаю. Тем более, она сама приглашала."
        me "Perhaps I will. Besides, she invited me."
        jump ss_d3dvus_eng
    #me "Почему бы и нет, делать особо нечего."
    me "Why not? I have nothing better to do."
    jump ss_d3dvus_eng
label ss_d3dvus_eng:
    window hide
    hide us with dspr
    $ renpy.pause (0.6)
    scene bg int_house_of_dv_day  with dissolve
    play ambience ambience_int_cabin_day
    play sound sfx_open_door_2
    #"Ульянка бесцеремонно распахнула дверь в своё жилище. К счастью, мы не застали Алису ни в каком неприглядном виде: она лежала на кровати с книгой, которую поспешно убрала под одеяло."
    "Ulyana unceremoniously opened the door to her cabin. Fortunately, we didn't walk in on Alisa in some indecent moment: she was lying on a bed with a book, which was hastily hidden under the blanket."
    show us smile sport 
    #us "Подъём, у нас гости!"
    us "Wake up, we have a guest!"
    if sp_dv <= 3:

        show dv angry pioneer at right   with dspr
        #dv "А по какому случаю?"
        dv "On what occasion?"
        if us_spy == True:
            #me "Да я вообще-то об Ульянке поговорить хотел."
            me "Well, I wanted to talk about Ulyana."
            #dv "У меня нет настроения разговаривать. Поговорите вдвоём. Мне выйти?"
            dv "I'm not in the mood to talk. You two can talk. Should I leave?"
            #th "-Тук-тук. {w}-Кто там? {w}-Открывай, поговорить надо. {w}-А сколько вас? {w}-Двое. {w}-Ну вот и поговорите."
            th "-Knock-knock. {w}-Who's there? {w}-Open up, we have to talk. {w}-How many of you are there? {w}-Two. {w}-Talk to each other then."
            #me "Тогда лучше в другой раз, пожалуй. Я пойду."
            stop music fadeout 2
            me "Well, maybe another time then. I'll be going."
            jump ss_d3dvusfail_eng
        else:
            if dv_inv == True:
                #me "Ты же сама приглашала, обещала на гитаре сыграть."
                me "Hey, you invited me. You promised to play me something on guitar."
                #dv "Ты один там? Не хочу я сегодня играть."
                dv "Are you alone out there? I don't wanna play today."
                #me "Ну ладно, потом как-нибудь. Я пойду тогда."
                stop music fadeout 2                
                me "Well, ok, next time. I'll go then."
                jump ss_d3dvusfail_eng
            else:
                #th "Похоже, мне тут не рады."
                th "It looks like I'm not welcome here."
                #me "Да просто Ульянку проводил. Пока."
                stop music fadeout 2
                me "It's just me and Ulyana. Well, bye."
                #us "А как же разговор твой?"
                us "Hey, but what about your talk?"
                #me "Это потерпит."
                me "Later."
                jump ss_d3dvusfail_eng
        jump ss_d3dvusfail_eng
    show dv normal pioneer at right   with dspr
    #dv "Предупредила бы хоть."
    dv "You could've warned me at least."
    #us "Ага, чтоб ты меня убираться заставила!"
    us "Yeah, and then you'd make me clean up!"
    #me "Ничего, это всего лишь я."
    me "Come on, it's just me."
    #dv "Где свою американку потерял? Или прошла любовь?"
    dv "Where's your American girl? Or is the love gone?"
    #me "Эта шутка не стареет, а?"
    me "Some jokes never get old, huh?"
    show us calml sport  with dspr
    #us "Эй, а где моя лягушачья ферма? Алиса?!"
    us "Hey, where is my frog farm? Alisa?!"
    #dv "Выкинула. Хватит их мучить."
    dv "I threw it out. Stop torturing them."
    show us sad sport  with dspr
    #us "Да им тут был курорт. Ты же видела счастливую морду Мистера Надутого?"
    us "It wasn't torture, it was a resort for them! You saw how happy Mr. Fat's face was, didn't you?"
    #dv "Отвратительно."
    dv "Disgusting."
    show us normal sport  with dspr
    #us "Я-то со всей любовью к ним, воду вот притащила. Ладно, они мне тоже надоели. Тогда устроим гонки жуков. Сегодня нашла одного - прирождённый победитель! Сейчас покажу."
    us "I loved them! Look, I even brought water for them! Well, whatever, I'm bored with them too. Let's race beetles instead! I found one today - a born winner! Let me show you."
    hide us  with dspr

    #"Ульянка безуспешно копалась в переполненных карманах. Вот кому не помешала бы жилетка Вассермана. Не сдаваясь, она начала вытряхивать их содержимое на постель."
    "Ulyana dug through her crowded pockets, but unsuccessfull: her hands were filled with wrong stuff over and over again. Not giving up, she started to shake her pockets' contents out onto her bed."
##notify Vasserman
    #us "Да где этот коробок..."
    us "Come on, where is that box?.."
    stop music fadeout 4
    #"Среди кучи полезных предметов моё внимание привлекли комок бумаги и рогатка. Ульянка, спохватившись, начала спасать свои пожитки от любопытных глаз."
    #"In a heap of useful stuff, my eyes caught a wad of paper and a slingshot. Ulyana suddenly noticed and started to shield her belongings from curious eyes."

    menu:
        #"Схватить бумажку":
        "Grab the wad of paper":
            show us surp2 sport  with dspr
            #us "Эй, не твоё!"
            us "Hey, that's not yours!"
            #me "А похоже на моё. Это же моя записка Саманте!"
            me "But it sure looks like mine. Hey, this is my note to Samantha!"
##            play music music_list["goodbye_home_shores"]  fadein 3
            if got_note == False:
                #th "Вот-так так. А какую же записку Саманта получила? Какую-нибудь гадость!"
                th "Surprise, surprise. And what note did Samantha get then? Some foul things!"
            show us dontlike sport  with dspr

            #us "Эээ... Не знаю, бумажка какая-то! На дороге нашла! Мусорить нехорошо, знаете ли!"
            us "Uh... I don't know, it's just some scrap of paper that I found on the ground. Littering is really bad, you know!"
            
            if us_spy == True:
                show us dontlike sport  with dspr

                #me "Ульян, я всё знаю о твоих похождениях. Но не стал никому говорить, давайте решим это тихо-мирно. Зачем ты это делала?"
                me "Ulyana, I know everything about your \"adventures\". But I haven't told anyone yet, so let's solve this calmly and peacefully. Why did you do it?"
                jump ss_ustr_eng
            else:
                show us dontlike sport  with dspr
                #th "Ну, Ульянка! И как я раньше не догадался. Это что же..."
                th "Oh, Ulyana! And why didn't I get it before? So what..."
                if eto_sh == True:
                    show dv grin pioneer at right   with dspr
                    #dv "А ты бы меньше со своим Шуриком носился."
                    dv "Maybe if you didn't care about Shurik so much..."

                    #me "Ульяна?"
                    me "Ulyana?"
                    jump ss_ustr_eng
                else:
                    show dv sad pioneer at right   with dspr
                    #dv "Понял теперь?"
                    dv "Do you get it now?"
                    #me "Прости..."
                    me "I'm sorry..."
                    #dv "Вот так просто?"
                    dv "Just like that?"

                #me "Ульян, но зачем?"
                me "Ulyana, but why?"
                show us angry sport  with dspr
                #us "Да ну вас. Я ухожу."
                us "I'm tired of this. I'm leaving."
                if sp_dv <= 5:
                    show dv normal pioneer at right   with dspr
                    #dv "Погоди, мне тоже неохота оставаться с тем, кто обо мне такого мнения. А оставлять его тут одного будет глупо. Так что..."
                    dv "Wait, I don't wanna stay here with someone who thinks so badly about me. And leaving him alone here is stupid too. So..."
                    #me "Вот не стройте из себя потерпевших. Одна пакостила Саманте, вторая знала и покрывала."
                    me "You're both trying to play the victim here. One of you wronged Samantha, the other knew about it and covered it up."
                    show us angry sport  with dspr
                    #us "И ничего не пакостила!"
                    us "I didn't do anything!"
                    show dv angry pioneer at right   with dspr
                    #dv "Расскажи это Самантам и вожатым, а тут тебе не рады. По крайней мере, пока обвиняешь других, хотя не видишь дальше своего носа."
                    dv "Tell that to Samanthas and camp leaders, but here you're not welcome. At least while you blame others, yet can't see farther than your own nose."
                    $ sp_dv -= 1
                    #me "Я-то уйду, но всем было бы лучше, если бы мы разобрались с этим сами. Как хотите, пока."
                    me "Fine, I'm leaving, but it would be better if we dealt with it by ourselves. As you wish. Goodbye."
                    scene bg ext_house_of_dv_day  with dissolve
                    play ambience ambience_day_countryside_ambience
                    stop music fadeout 2
                    #"Я вышел на улицу. Вот же горе-детектив. Пожалуй, стоит дать им второй шанс, прежде чем идти к вожатой. Это только обострит конфликт. Может, завтра одумаются."
                    "I left the cabin. I'm a horrible detective. Maybe I could give them one more chance before going to the camp leader. Telling her will only make things worse. Maybe they'll change their mind tomorrow."
                    jump ss_d3dvusfail_eng
                else:
                    show dv normal pioneer at right   with dspr
                    #dv "А ну стой. Ты виновата. И попалась, что хуже. Нужно отвечать за свои поступки."
                    dv "Hey you, stop. You are guilty. And you were caught, that's even worse. You have to be responsible for your actions."
                    stop music fadeout 2
                    #th "Это что-то новенькое."
                    th "That's something new."
                    jump ss_ustr_eng



        #"Схватить рогатку":
        "Grab the slingshot":
            show us surp2 sport  with dspr
            #us "Эй! Осторожнее с ней, это моя лучшая работа. Минимальный размер и огромная убойная сила."
            us "Hey! Be careful with that, that's my best creation. It's so small, but it hits so hard!"
            #me "Где-то я её уже видел. Да у тебя же в руках, в день, когда Саманта приехала."
            me "I've already seen it; in your hands, the day that Samantha arrived."
            show us normal sport  with dspr
            #us "Ну и что?"
            us "So what?"
            #me "Просто. Хотя, постой. Чем стреляешь-то?"
            me "Nothing. But wait. What do you use for ammo?"
            show us laugh2 sport  with dspr
            #us "Да хоть чем. Вишнёвыми косточками, кузнечиками. Попаду тебе в рот с 10 метров, если не улетит! А что?"
            us "Everything. Cherry pits, grasshoppers... I can hit you in your mouth at 10 meters, as long as it doesn't fly away! Why?"
            #me "Один раз в столовой у Саманты стакан с края стола упал и разбился. А рядом орех валялся. Как-то я одно с другим не увязал..."
            me "Once in the canteen, Samantha's glass fell off the table and broke, and there was a nut nearby. It didn't connect at the time..."
            show us upset sport  with dspr
            #us "Ну и?"
            us "And what?"
            if us_spy == True:
                #me "Я в курсе всей твой подрывной деятельности. Поэтому и пришёл. Рассказывай, зачем Саманту донимаешь?"
                me "I know everything about your shady business. That's why I'm here. Tell me, why are you pestering Samantha?"
                show us angry sport  with dspr
                #us "Ничего такого я не делала!"
                us "I didn't do anything!"
                if sp_dv >= 6:
                    show dv guilty pioneer at right    with dspr

                    #dv "Иногда можно и признаться. Все свои."
                    dv "Sometimes it's better to confess. No strangers here."
                    jump ss_ustr_eng
            else:
                show us angry sport  with dspr              
                #me "Лучше признавайся, иначе придётся отдать рогатку Ольге Дмитриевне."
                me "You'd better confess, or I'll have to give this slingshot to Olga Dmitrievna."
                #us "Приходишь к нам в гости как к друзьям, а потом угрожаешь заложить. За кого ты вообще, а?"
                us "You came to us as a friend, and now you're threatening to turn me in. Whose side are you on, huh?"
##выдать
                #me "Вот я и хочу помочь как друг, но без твоего участия не получится."
                me "Well, I wanna help you as a friend, but it's impossible without your participation."
                #us "Да не била я твой стакан дурацкий! Я вообще с другой стороны сижу, за вашими спинами даже не видела, что там разбилось."
                us "I didn't break your stupid glass! I was on the other side of the canteen and didn't even see what happened."
                #dv "Это правда."
                dv "It's true."
                #me "Хм. Ну извини тогда. Держи свою рогатку."
                me "Mhm. I'm sorry then. Take your slingshot."
                #us "Что-то мне расхотелось устраивать гонки жуков."
                us "You know what? I don't wanna make a beetle race anymore."
                #"Ульянка всё ещё восседала над горой своих сокровищ из карманов. Вид у неё был обиженный."
                "Ulyana was still sitting on the heap of her riches from her pockets. She looked resentful."
                #us "Я купаться пойду. Если ты дашь мне переодеться."
                us "I'll go swimming. Turn around so I can change into my suit."
                show dv grin pioneer at right    with dspr
                #dv "Какая скромница! А вот вчера..."
                dv "What a prude! But yesterday..."
                #us "Молчи!"
                us "Silence!"
                #me "Ладно, удаляюсь. Пока вам."
                me "Okay, I'm leaving. So long, you."
                scene bg ext_house_of_dv_day  with dissolve
                play ambience ambience_day_countryside_ambience
                stop music fadeout 4
                #"Лучше не дразнить несправедливо обвинённую. Хотя и вряд ли она вершила с этой рогаткой добрые дела. Я оставил девочек и побрёл к своему домику."
                "It's best not to tease the unjustly accused, even if nothing good can come of her having a slingshot. I left the girls and plodded back to my cabin."
                jump ss_d3dvusfail_eng

label ss_ustr_eng:
    show us upset sport  with dspr
    stop music fadeout 3
    #"Ульянка отвела взгляд и сжала губы. Оправдываться она не желала."
    "Ulyana looked away and pursed her lips. She didn't want to excuse herself."
##shut
    show dv guilty pioneer at right    with dspr
    #dv "Это из-за её отца."
    dv "It's because of her father."
    #me "А что с ним?"
    me "What's the matter with him?"
    #th "Неужели они с Самантой сёстры? Отец бросил беременную мать Ульянки и рванул в Штаты, в лучших традициях индийского кино."
    th "Maybe Ulyana and Samantha are sisters? Father left Ulyana's pregnant mother and fled to the US, like in the Indian movies..."
    #dv "Она думает, его убили американцы. На войне."
    play music music_list["tried_to_bring_it_back"] fadein 3    
    dv "She thinks that he was killed by Americans, in the war."
    show us cry sport  with dspr
    #us "Потому что так и было! Он был военный, так мама говорила. Погиб на задании."
    us "Because it really happened! My mother said that he was a soldier. He was killed on a mission."
    #"На глазах у неё навернулись слезы."
    "Her eyes were filled with tears."
    #me "А что за война такая, что-то я не соображу?"
    me "I don't understand, what war do you mean?"
    show us angry sport  with dspr
    #us "Во Вьетнаме, дурак! Слышал, там наши инструкторами были? Ну вот, а мой отец пропал. Сопоставь одно с другим!"
    us "The Vietnam War, fool! Do you know that our military were instructors there? And my father disappeared. Compare one fact with another!"
    #dv "Это ей ребята какие-то тут идею подали. А она и поверила. Ты отца видела хоть?"
    dv "Some of the guys here have proposed that idea to her, and she believes them. Have you ever seen your father?"
    #us "Видела, конечно! Только не помню. Маленькая была слишком."
    us "Yes, of course! But I don't remember. I was too young."
    show us sad sport  with dspr
    #me "Ну а вещи какие-нибудь после него остались? Фотографии?"
    me "Well, is there any stuff that remained after him? Photos?"
    #us "Да ничего не осталось, ну пара фотографий в штатском. Но это потому что работа сек-рет-ная!"
    us "There's nothing, only a couple of photos with a plain-clothed father. But it's because his job was con-fi-den-ti-al!"
    menu:
        #"Ты это всё придумала":
        "You fancied it":
            #me "Ты же уже взрослая, а веришь в детскую сказку про отца-космонавта. Подумай головой. {w}Твой отец мог попасть в тюрьму, да просто бросить вас. Это куда вероятнее."
            me "You're grown up already, but you still believe in childish tales about some astronaut father. Think with your own head. {w}Your father could be in a prison, or even left you with your mother. That is much more likely."
            show us angry sport  with dspr
            #us "Нет! И чего ты вообще к моей семье лезешь? Какое тебе дело?"
            us "No! And why do you even care about my family? Why are you interested in it?"
##What is that to you? What do you need?
            #me "Ты знаешь, какое мне дело."
            me "You know why."
##You know what
            #us "Всё равно мы с {i}ними{/i} враги, это все знают. А то, что все ей улыбаются - это мерзко. Вот начнётся война..."
            us "But still, we and {i}they{/i} are enemies, everyone knows it. And the fact that everyone smiles at her - that's unacceptable. One day the war will begin..."
            #me "Не начнётся. И благодаря таким людям как Саманта. Перестань, прошу тебя. Перестань и извинись."
            me "It won't ever begin, thanks to people like Samantha. Stop it, please. Stop it and apologize."
            #us "Перестану, но не извинюсь."
            us "I'll stop, but I won't apologize."
            #me "Это нужно сделать. Я могу сказать, что ты просишь прощения, но это не то."
            me "You have to do it. I can tell her that you ask for forgiveness, but that's not enough."
            #us "Даже не думай. Вообще, этого разговора не было! Всё, точка."
            us "Don't even think that I'll do it! Anyway, this conversation never happened. That's final."
            hide us  with dspr
            show dv normal pioneer at right    with dspr
            #"Ульянка выдернула из под себя одеяло и завернулась в него с головой в знак того, что разговор окончен. Алиса придвинулась ко мне и шепнула:"
            "Ulyana pulled a blanket from under herself and rolled herself up in it over her head, as a sign that the conversation was over. Alisa moved up close to me and whispered:"
            show dv normal pioneer close  with dspr
            #dv "Только не выдавай её. Я с ней поговорю ещё."
            stop music fadeout 3
            dv "Don't give her away. I'll talk to her later."
##выдать
            #"Лишь бы Ульянка хулиганить перестала, а извинения не к спеху. Я кивнул, попрощался и вышел. "
            "The most important thing was to stop Ulyana's hooliganism, whereas there was no rush for apologies. I nodded, said goodbye and walked out."
            scene black  with fade
            $ sp_sp += 1
            jump ss_d3dvusfail_eng


        #"Саманта не виновата":
        "It's not Samantha's fault":
            show us upset sport  with dspr
            show dv normal pioneer at right    with dspr
            #me "Саманта - последний человек, кому ты должна мстить. Вот представь, живёт в Америке такая девочка как ты, и ненавидит нашу страну. Или американский генерал, сидящий под табличкой \"Убей Ивана\"."
            me "Samantha is the last person you should attack. Now imagine that there is a girl in America, just like you, and she hates our country. Or there is an American general who sits under the \"Kill Ivan\" tablet."
            #me "Думаешь, им нравится Саманта, которая хочет помирить наши страны? Да они её ненавидят как предательницу. А нам она - друг в любом случае."
            me "Do you think they like Samantha, who wants to reconcile our countries? They hate her like a betrayer. And for us she's a friend anyway."
            #us "Тебе, может, и друг, а я сама себе друзей выбираю."
            us "Maybe she's a friend to you, but I choose my friends by myself."
            #me "Обещаю, вы с ней подружитесь, если познакомитесь поближе."
            me "I promise, she and you will become friends if you know each other better."
            show us sad sport  with dspr
            #us "Ну не знаю."
            us "Well, I don't know."
            #me "Может, сходим и посмотрим, чем она занимается?"
            me "Maybe we can go and see what she's doing now?"
            #dv "Хорошая идея. Сходите."
            dv "Good idea. Go."
            #us "А ты не с нами?"
            us "Aren't you going with us?"
            #dv "Да ну, это целый парад уже будет. Только ты извиниться не забудь."
            dv "It would be an entire parade. But you should apologize to her, don't forget that."
            show us upset sport  with dspr
            #us "Не хочу."
            us "I don't want to."
            show dv smile pioneer at right    
            #dv "Надо. Смотри, иначе дружить с тобой не буду!"
            dv "But you have to. Or you and I won't be friends anymore!" 
            #us "Зачем оно тебе?"
            us "Why do you want me to?"
            #dv "А я сегодня за справедливость."
            dv "Today I demand justice."
            #us "Ладно, посмотрим. Вот вы пристали."
            us "Ok, I'll think about it. You've both laid into me enough."
            stop music fadeout 3
            #"Я взглядом поблагодарил Алису и мы вышли из домика."
            "I glanced at Alisa gratefully, and we came out from her house."
            window hide
            $ sp_dv += 1
            $ renpy.pause (0.7)
            jump ss_d3uswin_eng

label ss_d3uswin_eng:
    scene bg ext_house_of_dv_day with dissolve
    play ambience ambience_day_countryside_ambience
##    play music Poirot fadein 2
    play music music_list["sweet_darkness"] fadein 3
    show us normal sport
    #me "И кто же тебе такую идею подал? Про Вьетнам?"
    me "And who gave you that idea? About Vietnam?"
    #us "Лена, кажется. А может и Славя."
    us "As I remember... It was Lena. Or Slavya, maybe."
    #me "Как так вышло? Писали сочинение \"Мой папа\"?"
    me "And how did it happen? You were writing a \"My father\" essay?"
    #us "Вроде того. Перед приездом Саманты, у костра. Обсуждали отцов. Но мой лучше всех, конечно!"
    us "Something like that. Before Samantha arrived, near the bonfire. We talked about our fathers. But my father is the best one, of course!"
    #me "Значит, не помнишь, кто из них?"
    me "So you don't remember, who it was, Lena or Slavya?"
    #us "Не-а. Да какая разница! Идём?"
    us "Nope. Who cares? Let's go!"
    scene bg ext_house_of_sl_day  with dissolve
    #me "Думаю, она в библиотеке, но сперва давай проверим домик."
    me "She's in the library, I guess, but let's check her house first."
    show us upset sport  with dspr
    #us "Ой, да ладно тебе."
    us "Oh, come on."
    #me "Что?"
    me "What?"
    #us "Будто вы это всё заранее не продумали. Конечно, она сидит там и точит на меня клыки."
    us "As though you and Alisa didn't set it up beforehand. Of course, she's sitting there and holding a grudge against me." 
    #me "Ничего подобного, Саманта даже имени твоего не знает. Успокойся."
    me "It's nothing like that. Samantha doesn't even know your name. Calm down."
    #us "Успокоюсь, если её там не будет."
    us "I'll calm down if she isn't there."
    window hide
    hide us with dspr
    $ renpy.pause (0.4)
    play sound sfx_knock_door2
    $ renpy.pause (0.7)
    ss "{enen}Come in!{/enen}"
    #"Я постучал в дверь. Саманта отозвалась, уже вернулась, значит. Подозрения Ульянки развеять не удалось."
    window hide
    show us dontlike sport at left with  dspr
    $ renpy.pause (0.7) 
    window show
    me "Come on. I'll dispel all your suspicions." 
    window hide
    $ renpy.pause (0.7) 
    play sound sfx_open_door_2
    scene bg day_sam  with dissolve
    
    $ persistent.ssg_1 = True 
    
    play ambience ambience_int_cabin_day
    show us normal sport at left
    show ss nosmile casual at cright
    with dspr
    #me "{en}Hi again. This is Ulyana, remember her? Can we come in?{/en}{ru}Привет ещё раз. Это Ульяна, помнишь её? Нам можно войти?{/ru}"
    me "{enen}Hi again. This is Ulyana, remember her? Can we come in?{/enen}"
    show ss normal casual at cright 
    #ss "{en}Sure. How are you, Uliana?{/en}{ru}Конечно. Как ты, Ульяна?{/ru}"
    ss "{enen}Sure. How are you, Ulyana?{/enen}"
    #us "Что?"
    us "What?"
    #me "Спрашивает, как ты. Это как приветствие, можешь не рассказывать."
    me "She asks how are you. It's like a greeting, you don't have to tell her how you really are."
    show us upset sport at left with dspr
    #us "Да мне не жалко. Палец вот на ноге натёрла, утром упала в крапиву, а ещё всякие в душу лезут. Так и живём."
    us "It's fine. Well, I stubbed my toe yesterday, fell down into nettles this morning, and also did some soul-searching. That's how I am."
    #me "She's fine."
    me "{enen}She's fine.{/enen}"
    show us normal sport at left with dspr
    #us "Какой у вас короткий язык, однако."
    us "What a brief language still."
    #me "Лаконичный."
    me "Laconic."
    if got_kicked == True:
        #ss "{en}You are a great soccer player. Let's play on the same team next time.{/en}{ru}Ты отличная футболистка. Давай будем за одну команду в следующий раз.{/ru}"
        ss "{enen}You are a great soccer player. Let's play on the same team next time.{/enen}"
        show us smile sport  with dspr
        #us "Ладно, но я всегда в атаке."
        us "Ok, but I'm always a forward."
    show us normal sport at left with dspr
    #"Пока я придумывал, как начать важный разговор, Саманта показывала нам собранную во время путешествий коллекцию: открытки, значки, фотографии. Ульянка реагировала сдержанно."
    "While I was thinking of how to start this important conversation, Samantha was showing us her travelling collection: cards, badges, and photos. Ulyana reacted with restraint."
    #"В голове мелькнула мысль, что гостья вполне могла бы что-нибудь стащить. Возможно, зря я её сюда привёл? Впрочем, сама Ульянка отказывалась принимать подарки Саманты."
    "The idea shot across my mind that Ulyana might steal something. Maybe I was wrong to bring her here? However, Ulyana rejected Samantha's gifts." 
    #"После долгих уговоров, она всё-таки согласилась взять йо-йо. Я был в курсе, что это не одна из безделушек на раздачу, а любимая игрушка Саманты. Ульянка, боюсь, не оценила этой жертвы."
    "After a long suasion, she finally accepted a yo-yo as a gift. I knew that it wasn't just some trinket, but Samantha's favourite toy."
    #"Ульянка стала искать ответный подарок, из её карманов показалась знакомая рогатка."
    "Ulyana began to search for a gift in return, and a well-known to me slingshot showed up from her pockets"
    stop music fadeout 2
    show us smile sport at left with dspr 
    #us "Не знаешь, что такое? Сейчас покажу."
    us "Do you know what this is? Let me show you."
    play music music_list["that_s_our_madhouse"] fadein 2
    #"Один из резиновых шариков (прыгун - так мы их звали?) из коробки Саманты был моментально заряжен и отправлен в противоположную стенку."
    "One of the rubber balls (leaper - is that how we called them?) from Samantha's box was immediately charged and shot into an opposite wall."
    #"Однако, снайперша не учла его плотности и упругости..."
    "Howewer, sniper-girl did not consider its density and elasticity..."
    show ss surprise casual at cright   with dspr
    play sound sfx_break_flashlight
    show us surp2 sport at left with dspr
    #"...Снаряд отскочил в окно, не пробив, но оставив на нём заметную трещину."
    "...The projectile bounced into the window - not breaking it, but leaving a noticeable crack."
    #us "Простите, я нечаянно..."
    us "I'm sorry, I didn't mean to..."
    show us sad sport at left with dspr
    show ss serious casual at cright with dspr
    stop music fadeout 3    
    #"Ульянка с досады отшвырнула рогатку. Подойдя к окну, я заметил ещё пару похожих отметин на стекле, но уже снаружи. Похоже, Саманта тоже догадывалась об их происхождении. Она подняла рогатку."
    "Ulyana threw her slingshot away in vexation. When I came to the window, I saw a couple of similar marks on the glass, but from the other side. It seemed that Samantha guessed about their appearance, too. She picked up the slingshot."
    #ss "{en}So... A Farewell to Arms{/en}{ru}Ну, что... Прощай, Оружие{/ru}?"
    ss "{enen}So... A Farewell to Arms?{/enen}"
    play music music_list["dance_of_fireflies"] fadein 4
    show us shy2 sport at left with dspr
    #"Ульянка покраснела. На всякий случай, я встал около двери. Пусть даже не думает сбежать. {w} Время шло, девочки молчали. Значит, даже у доброты Саманты есть предел. На этот раз она не могла заочно простить свою мучительницу."
    "Ulyana turned red. I stood close to the door, just in case. Don't she even think about escaping. {w} Time waxed on, the girls kept silent. So, even Samantha's kindness has its end. This time she was unable to forgive her torturer."
    #me "{en}Uliana is sorry...for everything. Which is a lot.{/en}{ru}Ульяна просит прощения...за всё. А там много чего.{/ru}"
    me "{enen}Ulyana is sorry...for everything. Which is a lot.{/enen}"
    #ss "{en}I see. That's a relief. Thank you, Sam.{/en}{ru}Понимаю. Какое облегчение. Спасибо тебе, Сэм.{/ru}"
    ss "{enen}I see. That's a relief. Thank you, Sam.{/enen}"
    #"Она поднялась и протянула руку Ульянке."
    "She stood up and offered a hand to Ulyana."
    show ss normal casual at cright with dspr   
    #ss "{en}Friends?{/en}{ru}Друзья?{/ru}"
    ss "{enen}Friends?{/enen}"
    show us cry2 sport  at left  with dspr
    #"Ульянка посмотрела на руку, всхлипнула, а уже через секунду сжимала Саманту в объятьях."
    "Ulyana casted an eye at her hand, sobbed, and immediately hugged Samantha."
    hide us
    hide ss

    #us "Ты такая добрая... А я была такая свинья... Я не знала... Не знала... Прости..."
    us "You're so kind... And I was a dirty dog... I didn't know... Didn't know... I'm sorry..."
    $ sp_sp += 3
    $ sp_ss += 1
    #"Здесь моя миссия выполнена. Теперь следует проявить деликатность."
    "My mission here is accomplished. Now I should show tact."
    scene bg ext_house_of_sl_day  with dissolve
    #"Когда я закрывал за собой дверь, за спиной уже кто-то ревел. Возможно, обе."
    "When I was closing the door behind me, I heard one of them weeping. Or maybe both of them."
    scene bg int_house_of_mt_day  with dissolve
    #"Вдоволь насладиться дипломатической победой не удалось: оказалось, Ольга Дмитриевна уже вернулась, а вместе с ней и вернулись и проблемы Шурика."
    "But my plans to fully enjoy this diplomatic victory failed: Olga Dmitrievna had come back, and Shurik's problems had returned with her."
    

    jump ss_d3spr_eng

label ss_d3spr_eng:
    play ambience ambience_int_cabin_day
    scene bg int_house_of_mt_day 
    show mt normal pioneer 
    with dissolve
    stop music fadeout 5
    #mt "Семён! Ты где был? Не важно. Я уже поговорила с Шуриком. Ох и заставил он нас побегать."
    mt "Semyon! Where have you been? It doesn't matter. I already spoke to Shurik. Oh, he made us worry a little."
    #me "Некоторых и буквально."
    me "Someone even had to run a little."
    ## выше была игра слов, хз, как перевести (побегать = побеспокоиться, поволноваться в переносном смысле)
    #mt "Ладно, я в столовую, сейчас ужин уже. Догоняйте, там поговорим."
    mt "Okay, it's dinner time now, I'm going to the canteen. Follow me; we can talk there."
    window hide
    $ renpy.pause (0.5)     
    scene bg int_dining_hall_people_day 
    show mt normal pioneer at left
    show ss normal casual at right
    with dissolve
    play ambience ambience_dining_hall_full
    #"На ужин была говядина с рисом. Определённо, лагерю повезло, что Саманта не вегетарианка. Или..? Никто ведь не спрашивал."
    "For dinner we had beef and rice. It's definitely good that Samantha isn't vegetarian. Or..? Nobody ever asked her about it."
    #mt "Вот что с ним делать, ты мне скажи?"
    mt "Give me some advice. What should I do?"
    #me "А вы ножом его."
    me "Try using a knife."
    window hide
    show mt shocked pioneer  with dspr
    $ renpy.pause (0.5) 
    show mt laugh pioneer  with dspr    
    #mt "Да не с мясом! С Шуриком."
    mt "Not about the meat! I meant about Shurik!"
    #me "Ну а что он говорит?"
    me "Well, what did he say?"
    show mt normal pioneer  with dspr
    if sh_adm == True:
        #mt "Он сознался и просит прощения, пострелёнок. Даже про ловушку сознался. А я-то тебе не поверила... Это же ты его расколол?"
        mt "That imp confessed and asked for forgiveness. Even admitted to the trap. I didn't believe you at first... It was you who exposed him, right?"
        #me "Можно и так сказать, наверное."
        me "Perhaps I can say so."
        #mt "Вот молодец. Но что теперь?"
        mt "Well done. But what should we do with him now?"
        #me "А какие варианты?"
        me "What options do we have?"
        #mt "Ну, можно сделать пометку у него в медкарте, и присматривать за ним до конца смены. А можно отправить домой. С другой... пометкой."
        mt "Well, we can tick his medical records and look after him until the end of camp-shift. Or we can send him home. With another... tick."
        menu:
            #"Лучше оставить":
            "It's better for him to stay":
                #me "Шурик будет в порядке. Он ведь не особо опасен, что уж там... А во второй заскок по той же теме - вообще не верится."
                me "Shurik will be ok. After all, he isn't that dangerous... And I don't think that he'll go mad again."
            #"Лучше выгнать":
            "It's better for him to leave the camp":
                #me "Лучше не рисковать. Даже если он будет держать себя в руках, Саманту может расстроить его присутствие."
                me "It's better not to risk it. Even if he contains himself, his presence could upset Samantha."
        #mt "Ну что же. Твоя точка зрения понятна. Я ещё подумаю."
        mt "Well. Your point of view is clear to me. I'll think about it."
        #th "...и сделаю всё по-своему."
        th "...and will do everything in my own way."
    else:
        #mt "Не сознаётся. Городит про какую-то рыбалку. Врёт, конечно. Думаешь, он специально заставил нас по шахтам лазить?"
        mt "He hasn't confessed. He told about some fishing. Lied, of course. Do you think that he purposely made us sweat, looking for him in the mines?"
        #me "Похоже на то."
        me "Looks like it."
        #mt "А какова была его цель?"
        mt "What purpose would he have for that?"
        #me "Хотел подобраться к Саманте, пока нас нет. А там - чёрт его знает."
        me "Maybe he wanted to get close to Samantha while we were gone. God knows what would have happened next."
        show ss nosmile casual at right
        show mt surprise pioneer
        with dspr
        #mt "Ужас какой!"
        mt "How horrible!"
        menu:
            #"Не всё так страшно":
            "All is not so bad":
                #me "Честно говоря, не думаю, что он желал ей зла. Или даже наоборот."
                me "To be honest, I don't think that he wished her harm. Maybe even vice versa."
                show mt normal pioneer  with dspr
                #mt "Интересная гипотеза. Надеюсь, он её не очень напугал. Может, её саму спросить?"
                mt "Interesting hypothesis. I hope he hasn't scared her too much. Maybe we should ask her?"
                #me "И она скажет, что он лучший из пионеров, как и про любого. Сами знаете."
                me "And she would say that he is the best pioneer, just like she would say about everyone, you know."
                show ss smile2 casual at right              
                #"Саманта, как бы в подтверждение моих слов, улыбнулась."
                "Samantha smiled as if she confirmed what I said."
                #mt "Это верно."
                mt "Yep, right."
            #"Не то слово":
            "It's not the word":
                #me "Я бы и сам держался подальше от этого психа, не то что Саманту к нему подпустить."
                me "I would stay rather far away from that psycho and never let Samantha near him."
                show mt normal pioneer  with dspr
                #mt "Ну, не будем совсем уж сгущать краски. Мы знали Шурика как хорошего пионера, да и ничего страшного, в сущности, не произошло..."
                mt "Well, let's stop dramatizing. We know Shurik as a good pioneer, and nothing bad has happened in fact..."
                #th "Хоть бы раз со мной согласилась. Нужно было похвалить Шурика, тогда век сидеть бы ему в комнате с мягкими стенами."
                th "She never agrees with me. If I praised Shurik, he would get put into a padded cell."
        #me "И что же теперь с ним будет?"
        me "And what will you do to him?"
        #mt "Ещё не решили. А ты бы как поступил?"
        mt "I haven't decided yet. What would you do if you were me?"
        menu:
            #"Оставил бы в покое":
            "I would leave him alone":
                #me "Думаю, опасности от него уже не исходит. Сыроежкин мог бы за ним присматривать."
                me "I don't think he's a threat anymore. Cheesekov could look after him."
                #mt "Очень великодушно с твой стороны. Но вряд ли мы можем его простить, если он даже не признался."
                mt "So generous. But I don't think we can forgive him just like that. He hasn't even confessed, after all."
            #"Отослал бы домой":
            "I would send him home":
                #me "Лучше отправить его домой, от греха подальше. Пусть там доктора с ним разбираются, или ещё кто. Надеюсь, он не сбежит по дороге, и не вернётся на тропу войны."
                "It's better to send him home, out of harm's way. Let the doctors or someone else take care of him there. Hopefully he won't escape on the way and be back on the warpath."
                #mt "Домой... Да, это может быть лучшим решением."
                mt "Home... Yes, that could be the best option."
                #me "Так и давайте."
                me "So let's solve the problem that way."
                #mt "Но отправить-то не так просто. Ладно, это уже не твоя забота."
                mt "But it's not that easy to send him home. Well, that's not your problem."
            #"Запер бы в медпункте":
            "I would lock him in the infirmary":
                #me "Привязать его к койке в медпункте, колоть галоперидол, а на ночь - запирать на амбарный замок. Как-то так. {w}Да, и повесьте связки чеснока на окна. Лишним не будет."
                me "Let's tie him to a bed in the infirmary, give him haloperidol, and padlock the infirmary at night. Something like that, I guess. {w}Yeah, and hang garlic on windows. That wouldn't be excessive."
                show mt smile pioneer  with dspr
                #mt "Понятно. Вижу, тебе с ним в одной комнате лучше не оставаться."
                mt "It's clear. I see, you shouldn't stay in the same room with him."
                #me "И никому."
                me "And nobody should."
    #mt "Ладно, это мы ещё решим, не только от меня зависит. Наша медсестра может быть немного легкомысленной в таких вопросах."
    mt "Ok, we'll go over it later; it doesn't depend solely on me. Our nurse could be a bit careless in situations like this."
    #ss "{en}Guys, what are you talking about?{/en}{ru}О чём вы говорите?{/ru}"
    ss "{enen}What are you talking about?{/enen}"
    show mt normal pioneer  with dspr
    #"Вожатая подала мне недвусмысленный знак."
    "The camp leader gave me an unequivocal sign."
    #me "{en}Sorry, it's a personal matter{/en}{ru}Прости, это личное{/ru}."
    me "{enen}Sorry, it's a personal matter.{/enen}"
    show ss smile casual at right 
    #ss "{en}With my name in every sentence? I'm honored.{/en}{ru}С моим именем в каждой фразе? Я польщена.{/ru}"
    ss "{enen}With my name in every sentence? I'm honored.{/enen}"
    #mt "Скажи ей, что всё в порядке. Завтра костёр будет."
    mt "Tell her that everything is ok. Tomorrow we'll have a bonfire."
    show ss unsure casual at right with dspr
    stop music fadeout 5
    #ss "{en}Come on, I know it's about Shurik. Please don't punish him.{/en}{ru}Да ладно, я поняла, что про Шурика. Пожалуйста, не наказывайте его.{/ru}"
    ss "{enen}Come on, I know it's about Shurik. Please, don't punish him.{/enen}"
    show mt smile pioneer  
    #mt "Ну что ты!"
    mt "Don't worry about it!"
    window hide
    $ renpy.pause (1)
    jump ss_d3eve_eng



label ss_d3dvusfail_eng:
    stop music fadeout 3
    $ sp_sp -= 1
    $ d3_usfail = True
    scene bg ext_house_of_mt_day  with dissolve
    play ambience ambience_camp_center_evening
    #"Подойдя к домику, я вставил ключ в замок. Меня остановил крик:"
    "I came to the house and put my key into the lock. I was stopped by a shout:"
    play sound sfx_lock_click
    #mt "Не входи, Семён! Погоди минутку."
    mt "Semyon, don't come in! Wait a minute."
    #th "Ох, Ольга Дмитриевна вернулась. Опять со своими проблемами и Шуриками."
    th "Oh, Olga Dmitrievna came back. Again with her problems and Shuriks."
    scene bg ext_square_day  with dissolve
    #"Воспользовавшись моментом, я сбежал в сторону площади. Там на одной из скамеек я нашёл Славю."
    "I seized the opportunity and escaped to the square. On one of the benches I found Slavya."
    play music music_list["she_is_kind"] fadein 2
    show sl normal pioneer  with dspr
    if sl_ft == True:
        #me "Эй! Как твоя нога?"
        me "Hey! How's your leg?"
        #sl "Ничего страшного, ходить могу. Там Ольга Дмитриевна с Сыроежкиным вернулись."
        sl "Nothing terrible; I can walk. Olga Dmitrievna and Cheesekov came back."
        #me "Да, я видел. Хорошо, что не заблудились."
        me "Yes, I know. It was a pointless walk, but at least they didn't get lost."
        #sl "Догнал ты Шурика? Как всё прошло?"
        sl "Did you catch Shurik? How was it?"
        #me "Да вроде неплохо. Никто не умер."
        me "It was ok. Nobody died."
        show sl smile pioneer  with dspr
        #sl "Дай-ка посмотрю. Ну, синяков у тебя с утра не прибавилось."
        sl "Let me take a look at you. No new bruises since this morning?"
        #me "Сегодня был насыщенный день. И многим досталось. Тебе вот."
        me "Today was a crazy day. So much trouble for all of us. You've been hurt too."
        #sl "Заживёт. Понравился тебе наш поход?"
        sl "It will heal. I think it was kinda fun today. Did you enjoy our hike?"
        menu:
            #"Нет":
            "No":
                #me "Дай подумать. Темнота, паутина, торчащие ржавые гвозди, червяки, и мы одни под тоннами земли, которая может обвалиться в любую минуту? Не особо."
                me "Let me think. Darkness, cobwebs, rusty nails sticking out, worms, alone under tons of ground, which could fall down at any minute? Not very much."
                #sl "Зато приключение."
                sl "But it was an adventure."
                #me "Я за безопасные приключения."
                me "I prefer safe adventures."
                show sl smile pioneer  with dspr
                #sl "Это какие же?"
                sl "Give me an example."
                #me "Ну..."
                me "Well..."
                #sl "Вот-вот. Не бывает таких!"
                sl "Exactly. There are no safe adventures!"
                #me "Ладно. Меня там Ольга Дмитриевна ждёт, наверное, так что пойду. Увидимся."
                me "Ok. Olga Dmitrievna is probably waiting for me, so I've got to go. See you."
            #"Да":
            "Yes":
                #me "А то. Особенно концовка. Наше чудесное спасение."
                me "Of course. Especially the ending. Our marvelous salvation."
                #sl "Спасение? Разве не уютно сидели?"
                sl "Salvation? But didn't we feel comfortable there?"
                $ sp_sl += 1
                #me "Уютно. Я почти полюбил походы."
                me "Yeah, comfortable. I almost fell in love with hikes."
                show sl smile2 pioneer  with dspr
                #sl "Вот. Завтра повторим?"
                sl "Well. Shall we repeat this tomorrow?"
                #th "Это она шутит так? Издевается? Вызов принят."
                th "Is she kidding? Or mocking me? Challenge accepted."
                #me "Обязательно. Только надо бы убраться там."
                me "Sure. But we need to clean up there."
                show sl surprise pioneer  with dspr
                #sl "Ты про что? Про комнату под Гендой, что ли?"
                sl "To clean up where? {w}You're talking about the room... under Genda?"
                show sl tender pioneer  with dspr
                #me "Эээ... А ты про что?"
                me "Uh... And what are you talking about?"
                #sl "Завтра все в поход идут, костёр и всё такое."
                sl "About the big hike. The whole camp is going out tomorrow. We're going to have a bonfire and so on."
                #th "Ох!"
                th "Oh!"
                show sl shy pioneer  with dspr
                #sl "...Мне надо ещё домой зайти перед ужином. Побегу."
                sl "...I need to drop into my house before dinner. I'll go."
    else:
        #me "Привет. Как вы сходили?"
        me "Hi there. How was your hike?"
        show sl serious pioneer  with dspr
        #sl "Безуспешно, как ты знаешь. Шурик-то тут был. Мы ходили по коридорам, потом слезли в какую-то дыру, там была развилка. {w}Мы с Сыроежкиным пошли влево, а Ольга Дмитриевна вправо. Мы долго шли, но ничего не нашли и вернулись. Только страху натерпелись зря."
        sl "Unsuccessful, as you might know. Shurik was here. We walked through corridors, then climbed down into some hole. We found a fork. {w}Cheesekov and I went left and Olga Dmitrievna went right. We walked for a long while, but found nothing and came back. We got frightened over nothing."
        #me "А вожатая?"
        me "And what about the camp leader?"
        #sl "Она нашла много развилок, целый лабиринт. И решила вернуться за нами. Только мы туда уже не пошли, устали."
        sl "She found even more forks - a whole labyrinth. She decided to come back for us, but we didn't go further. We were too tired."
        #me "Жаль."
        me "It's a pity."
        #sl "Что не пошли?"
        sl "That we didn't go there?"
        #me "Нет, что ходили впустую."
        me "No, that you went for nothing."
        show sl angry pioneer  with dspr
        #sl "Ну хоть ты спасся. Чутьё тебя не подвело."
        sl "At least you saved yourself. Your flair didn't betray you."
        $ sp_sl -= 1
        #me "Думаешь, я струсил?"
        me "You think that I quailed?"
        #sl "А ты думаешь, нам очень хотелось туда идти?"
        sl "And what, you think we really wanted to go there ourselves?"
        #me "Подожди, ты же сама сказала, что вы и так справитесь."
        me "Wait, you said that you guys would deal with it, even without me."
        #sl "Я так думала."
        sl "I thought so."
        #me "Что-то ещё случилось?"
        me "Did something else happen?"
        #sl "Случилось. Мы потревожили стаю летучих мышей. Не спрашивай, что было потом."
        sl "Yes. We disturbed a flock of bats. Don't ask what happened next."
        stop music fadeout 2
        #me "Очень жаль."
        me "Sorry to hear that..."
        show sl smile pioneer  with dspr
        play music GoodbyeHorses fadein 1
        #sl "Но Сыроежкин до сих пор отмывается, наверное."
        sl "Cheesekov is probably still trying to wash himself."
        window hide
        scene cg d4_el_wash  with fade3
        $ renpy.pause(5)
        hide d4_el_wash 
        scene bg ext_square_day  with fade2
        show sl normal pioneer 
        #sl "Чего замечтался?"
        sl "Are you dreaming?"
        stop music fadeout 5
        #sl "Так. Скоро ужин уже. Мне надо домой зайти. Пока."
        sl "Well, dinner time will come soon. I need to drop by my cabin. Bye."
    window hide
    $ renpy.pause(0.6)      
    scene bg int_house_of_mt_day  with dissolve
    #"Я вернулся к нашему уже такому родному домику."
    "I returned to my own cabin."

    jump ss_d3spr_eng

label ss_d3eve_eng: 
    play ambience ambience_int_cabin_evening
    scene bg int_house_of_mt_sunset  with dissolve
    play music music_list["so_good_to_be_careless"] fadein 3
    #"После ужина мы разошлись по домикам. У меня был план поваляться, или даже залечь спать, но покой нам только снится."
    "After dinner we headed to our houses. I was about to get some sleep, but... Peace is but a lovely dream."
    ##"After dinner we headed to our houses. I had a plan to lie down or even to go to sleep, but \"we can only dream of leisure\"."
##?"peace is but a dream" (trans. by I. Walshe and V. Berkov); "tranquillity is but a lovely dream" (trans. by M. Whyte)
##?notify Alexander Blok: http://max.mmlc.northwestern.edu/mdenner/Demo/texts/kulikova.html AND http://www.poetryloverspage.com/poets/blok/on_field_of_kulicovo.html AND http://forum.lingvo.ru/actualthread.aspx?tid=9774
    show mt normal pioneer  with dspr
    #mt "Так, я опять убегаю, а ты не оставляй Саманту одну. Репетируйте вашу пьесу."
    mt "So, I have to go again, and you shouldn't leave Samantha alone. Rehearse your play."
    #me "Это как-то навязчиво, нет?"
    me "Isn't it a bit importunate?"
    #mt "Нормально. Если я чего-то не знаю в этой истории с Шуриком... Тебе я больше доверяю, чем ему. На данный момент."
    mt "It's fine. Even if I don't know everything about this incident with Shurik... I still trust you more than him. At the moment."
    #me "Переводчик, гид, актер... Цербер. Мне не положена зарплата?"
    me "Translator, guide, actor... Cerberus. I should be receiving a salary, shouldn't I?"
    #mt "Заработал, не спорю. Можешь взять печенье из моей тумбочки. Я ушла."
    mt "Yes, of course. You can take cookies from my nightstand. I'm leaving."
    window hide
    hide mt  with dspr
    $ renpy.pause (0.3) 
    play sound sfx_close_door_campus_1
    $ renpy.pause (0.5) 
    window show
    #me "Друг, любовник, поэт..."
    me "Friend, lover, poet..."
    #mt "Я всё слышала!"
    mt "I can hear you!"
    window hide
    $ renpy.pause (1.3)
    #"Делать нечего, я пошёл к Саманте. Даже повод придумывать поленился, обычно он не нужен."
    "There was nothing to be done. I went to see Samantha. I was too lazy to find a reason for it, and there is usually no need for a reason."
    window hide
    $ renpy.pause (0.4)
    $ persistent.sprite_time = 'sunset'
    $ sunset_time()
    scene bg day_sam  
    show un shy pioneer at cleft
    show ss normal casual at right
    with dissolve

    $ persistent.ssg_1 = True
    
    th "Lena is here? This is unexpected as my own visit, as I can see."
    #un "Я тут зашла передать кое-что. Уже ухожу."
    un "I... I came here to give her something. I'm already leaving."
    show ss surprise casual at right 
    #ss "{en}No, no, no! Stay!{/en}{ru}Нет-нет-нет! Останься!{/ru}"
    ss "{enen}No-no-no! Stay!{/enen}"
    show ss smile2 casual at right with dspr    
    if sl_ft == True:
        #th "Надо бы извиниться перед Леной за сегодня. Пусть она и не права, но всё равно неловко."
        th "It would be good if I could apologize to Lena for the things that had happened today. Even if she wasn't right, the situation is still awkward."
        #me "Слушай, я сегодня был резок..."
        me "Listen, today I was rude..."
        #un "Ничего. Я понимаю."
        un "Yes, I understand."
        #me "...Прости."
        me "...I'm sorry."
        #un "Хорошо."
        un "Ok."
        #th "Интересно, что именно она там понимает, ну да ладно."
        th "Interesting. What specifically does she understand? Anyway..."
    else:
        #me "Вы всё со своей Монополией."
        me "Right, Monopoly stuff..."
        window hide
        $ renpy.pause (0.6)
    show un normal pioneer at cleft
    show ss grin casual at right 
    with dspr
    $ renpy.pause (0.3) 
    window show
    #ss "{en}So, guys, let's do something. Let's tell stories?{/en}{ru}Ну что, чем займёмся? А давайте рассказывать истории?{/ru}"
    ss "{enen}So, guys, let's do something. Maybe tell stories?{/enen}"
    show ss smile2 casual at right with dspr
    #ss "{en}Okay, right. It's gonna be a torture for Sam. How about movies? What have you watched recently?{/en}{ru}Ладно, это будет пыткой для Сэма. Как насчет фильмов? Смотрели что-нибудь недавно?{/ru}"
    ss "{enen}Ok, right. It would be torture for Sam. How about movies? What have you watched recently?{/enen}"
    #me "{en}I'm not sure.{/en}{ru}Точно не помню.{/ru}"
    me "{enen}I'm not sure.{/enen}"
    #un "\"Гостья из будущего\"."
    un "\"Guest from the future\"."
    #ss "{en}From the future? I saw \"Back to the Future\" twice this month. It's brilliant. Is it the same movie?{/en}{ru}Из будущего? Я ходила на \"Назад в Будущее\" дважды за этот месяц. Такой здоровский. Мы про один фильм говорим?{/ru}"
    ss "{enen}From the future? I saw \"Back to the Future\" twice this month. It's brilliant. Is it the same movie?{/enen}"
    #me "{en}No, it's the Soviet movie series.{/en}{ru}Нет, это советский многосерийный фильм.{/ru}"
    me "{enen}No, it's a Soviet movie series.{/enen}"
    #ss "{en}I'd love to see it. But you know \"Back to the Future\", right?{/en}{ru}Я бы его посмотрела. Но вы видели \"Назад в Будущее\", так?{/ru}"
    ss "{enen}I'd like to see it. But you know \"Back to the Future\", right?{/enen}"
    #me "{en}Sure, it's a classic{/en}{ru}Конечно, это классика{/ru}."
    me "{enen}Sure, it's a classic.{/enen}"
    #un "А я не слышала о таком фильме."
    un  "I've never heard about this film."
    show ss surprise casual at right with dspr      
    #ss "{en}Classic? No, it's recent!{/en}{ru}Классика? Да нет же, это новинка!{/ru}"
    ss "{enen}Classic? It only just came out!{/enen}"
    show ss smile2 casual at right with dspr    
    #me "{en}I mean classy.{/en}{ru}Классный, в смысле...{/ru}"
    me "{enen}I mean classy.{/enen}"
    #ss "{en}You know, it's strange. You really shouldn't know it. As I barely know Russian movies, they never put it on cinema or tv. It's a shame.{/en}{ru}Если подумать, то это странно. Ты правда не должен знать этот фильм. У нас вот совсем не слышно о ваших фильмах, а жаль.{/ru}"
    ss "{enen}You know, it's strange. You really shouldn't know about it. Just like I barely know Russian movies since they never put them on cinema or TV in my country. It's a shame.{/enen}"
    #un "У нас показывают и американские фильмы. Но чаще старые."
    un "Sometimes American films are shown in our country. But often they're old ones."
    show ss grin casual at right with dspr  
    #ss "{en}Tell us, Semyon. How do you know \"Back to the Future\"?{/en}{ru}Признавайся, Семён. Откуда ты знаешь \"Назад в Будущее\"?{/ru}"
    ss "{enen}Tell us, Semyon. How come you know about \"Back to the Future\"?{/enen}"
    #me "{en}Well... Maybe I am from the future myself. And I know all the stuff.{/en}{ru}Ну... Может, я сам из будущего. И знаю такие вещи.{/ru}"
    me "{enen}Well... Maybe I am from the future myself. And I know all the stuff.{/enen}"
    show ss grin_smile casual at right 
    #ss "{en}What future are you from? Distant future?{/en}{ru}И из какого ты будущего? Из далёкого?{/ru}"
    ss "{enen}And what future are you from? The distant one?{/enen}"
    #me "{en}Not really, maybe 25 years. What year are we in again{/en}{ru}Не особо, какая-нибудь четверть века. В каком мы сейчас году, кстати{/ru}?"
    me "{enen}Not really, maybe 25 years from now. By the way, what year it is now?{/enen}"
    show un smile pioneer at cleft 
    #un "Ну и как вам там живётся? Коммунизм наступил?"
    un "And how... did you live there, in that future? Did communism come?"
    #me "Да примерно так же. Машины не летают, а роботов стало даже меньше. Коммунизм... отступил."
    me "Well, it's almost like it is now. Cars don't fly and robots' count even decreased. Communism... decreased too."
    show ss smile2 casual at right with dspr    
    #ss "{en}How am I doing in the future? You said you knew all the stuff. Am I fat and ugly?{/en}{ru}А про меня ты что-нибудь слышал? Я там совсем толстая и страшная, наверное?{/ru}"
    ss "{enen}How am I doing in the future? Am I all fat and ugly there?{/enen}"
    #th "Хороший вопрос, вот только если бы я помнил."
    th "Good question, but if only I could remember the answer."
    #me "{en}You became US' first female president. Congratulations.{/en}{ru}Ты стала первой женщиной-президентом Соединённых Штатов. Поздравляю.{/ru}"
    me "{enen}You became the first female President of the US. Congratulations.{/enen}"
    show ss laugh casual at right with dspr
    #ss "{en}Wow. I must consider this career now. So much power!{/en}{ru}Ух ты! Теперь я должна рассмотреть политическую карьеру. Такая власть!{/ru}"
    ss "{enen}Wow! I should consider a political career, then! So much power!{/enen}"
    #me "{en}But not as much as Barack Obama possesses. The glorious commander of our United Space Fleet.{/en}{ru}Но не та власть, которой обладает Барак Обама. Наш блистательный командор Объединённого Космического Флота.{/ru}"
    me "{enen}But not as much as Barack Obama possesses. The glorious commander of our United Space Fleet.{/enen}"
    show ss grin_smile casual at right
    #ss "{en}Space fleet, huh. Such a liar! You've never seen the movie.{/en}{ru}Космический Флот, ну конечно! Вот же врун. Не видел ты фильма.{/ru}"
    ss "{enen}Space fleet, huh? What a liar! You've never seen the movie.{/enen}"
    #un "Ладно, ребята. Я правда на минуту зашла."
    un "Ok, guys. I've really dropped in for a minute."
    show ss normal casual at right 
    stop music fadeout 3    

    menu:
        #"Оставайся":
        "Stay":
            #me "Оставайся, в Монополию поиграем. Или ещё во что."
            me "Stay. We can play Monopoly. Or something else."
            show un shy pioneer at cleft with dspr
            #un "Ну... Можно."
            un "Well... Ok."
            #me "Вот и хорошо."
            me "That's good."
            $ sp_un += 1
            jump ss_d3invite_eng
        #"Пока":
        "Bye":
            #me "До завтра тогда."
            me "See ya tomorrow."
            window hide
            $ renpy.pause(0.3)
            hide un  with dspr
            $ renpy.pause(0.5)  
            jump ss_d3secrets_eng


label ss_d3secrets_eng:
    play music music_list["lets_be_friends"] fadein 6
    $ renpy.pause(1)    
    show ss unsure casual with dspr
    #ss "{en}Can I ask something personal{/en}{ru}Можно спросить что-то личное{/ru}?"
    ss "{enen}Can I ask something personal?{/enen}"
    #me "{en}Go ahead.{/en}{ru}Давай.{/ru}"
    me "{enen}Why not?{/enen}"
    show ss shy casual with dspr    
    #ss "{en}Do you like Lena?{/en}{ru}Тебе нравится Лена?{/ru}"
    ss "{enen}Do you like Lena?{/enen}"
    #me "{en}What do you mean?{/en}{ru}В каком смысле?{/ru}"
    me "{enen}What do you mean?{/enen}"
    #ss "{en}Okay. Do you like some other girl here?{/en}{ru}Ладно. Тебе нравится какая-то другая девочка здесь?{/ru}"
    ss "{enen}Ok. Do you like some other girl here?{/enen}"
    #me "{en}Here in this room{/en}{ru}Здесь - в этой комнате{/ru}?"
    me "Here, in this room?"
    show ss shy2 casual with dspr   
    #ss "{en}Come on{/en}{ru}Ну брось{/ru}!"
    ss "{enen}Oh, come on!{/enen}"
    show ss shy casual with dspr    
    menu:
        #"Лена":
        "Lena":
            $ sp_un += 1
            #me "{en}Lena is a nice girl.{/en}{ru}Лена в порядке.{/ru}"
            me "{enen}Lena is a nice girl.{/enen}"
            show ss serious casual with dspr           
            #ss "{en}Good.{/en}{ru}Хорошо.{/ru}"
            ss "{enen}Good.{/enen}"
            #me "{en}And?{/en}{ru}И?{/ru}"
            me "{enen}And?{/enen}"
            #ss "{en}Nothing. It's all for now.{/en}{ru}Это всё. Пока что.{/ru}"
            ss "{enen}Nothing. That's all for now.{/enen}"
            $ sp_spts = 1
        #"Другая":
        "Another one":
            #ss "{en}Which is Slava? Or Alice?{/en}{ru}Значит, Славя? Или Алиса?{/ru}"
            ss "{enen}So is it Slavya? Or Alisa?{/enen}"
            menu:
                #"Одна из них":
                "One of them":
                    #me "{en}Maybe one or another. Who knows?{/en}{ru}Одна или другая. Кто знает?{/ru}"
                    me "{enen}Maybe one or another. Who knows?{/enen}"
                    show ss serious casual with dspr                   
                    #ss "{en}Okay I went too far{/en}{ru}Ладно, я далеко зашла{/ru}."
                    ss "{enen}Ok, I went too far.{/enen}"
                    $ sp_spts = 2
                #"Никто из них":
                "None of them":
                    $ sp_ss += 1
                    show ss smile2 casual with dspr                 
                    #ss "{en}Interesting...{/en}{ru}Интересно...{/ru}"
                    ss "{enen}Interesting...{/enen}"
                    $ sp_spts = 3
                #"Хватит расспросов":
                "Enough questions":
                    show ss nosmile casual with dspr
                    #ss "{en}As you wish{/en}{ru}Как пожелаешь{/ru}."
                    ss "{enen}As you wish.{/enen}"


        #"Все хороши":
        "All girls are nice":
            #me "{en}Too many girls around to pick one.{/en}{ru}Столько девчонок вокруг - как тут выбрать одну.{/ru}"            
            me "{enen}There are too many girls around to choose one.{/enen}"
            #ss "{en}I see.{/en}{ru}Понятно.{/ru}"
            ss "{enen}I see.{/enen}"

    window hide     
    $ renpy.pause(0.5)              
    show ss smile casual with dspr 
    window show
    #ss "{en}Now you ask me something!{/en}{ru}Теперь ты меня спроси что-нибудь!{/ru}"
    ss "{enen}Now your turn! Ask me something!{/enen}"
    menu:
        #"Тебе нравится Шурик?":
        "Do you like Shurik?":
            show ss shy casual with dspr        
            #ss "{en}No. I mean, he's ok, but the answer is no.{/en}{ru}Нет. То есть, он ничего, но ты понял.{/ru}"
            ss "{enen}No. I mean, he's ok, but... You know.{/enen}"
        #"Ты в порядке тут в «Совёнке»?":
        "Are you alright here in \"Sovyonok\"?":
            if sp_sp >= 5:
                show ss nosmile casual with dspr                
                #ss "{en}Yes, I'm fine.{/en}{ru}Да, я в порядке.{/ru}"
                ss "{enen}Yes, I'm fine.{/enen}"
                #me "{en}I'm glad to hear it.{/en}{ru}Рад слышать.{/ru}"
                me "{enen}I'm glad to hear it.{/enen}"
            else:
                window hide                 
                show ss unsure casual with dspr  
                $ renpy.pause(0.7)              
                window show    
                #ss "{en}Not really. But I don't want to discuss it now.{/en}{ru}Нет, не в полном. Но не надо сейчас об этом.{/ru}"
                ss "{enen}Not really. But I don't want to discuss it now.{/enen}"
                #me "{en}Fine, it was a long day. But we'll discuss it tomorrow.{/en}{ru}Ладно, это был долгий день. Обсудим это завтра.{/ru}"
                me "{enen}Yep, it's been a long day. Let's discuss it tomorrow then.{/enen}"

    window hide         
    $ renpy.pause(1)
    show ss smile casual with dspr 
    #ss "{en}Hey, wanna listen to some music?{/en}{ru}Эй, хочешь послушать музыку?{/ru}"
    ss "{enen}Hey, do you wanna listen to some music?{/enen}"
    #me "{en}Sure.{/en}{ru}Конечно.{/ru}"
    me "{enen}Sure.{/enen}"
    #"Я вспомнил, что в нашем с вожатой домике есть чай и печенья, и пригласил Саманту туда. Магнитофон взяли с собой."
    stop music fadeout 3
    "I remembered that there were tea and cookies in my house, so I invited Samantha there. We took the tape recorder with us."
    window hide         
    $ renpy.pause(0.6)
    play music HeartOfGlass fadein 2
    scene bg int_house_of_mt_night  with fade   
    $ persistent.sprite_time = 'night'
    $ night_time()
    $ renpy.pause(3)
    show ss normal casual with dspr  
    jump ss_d3zz_eng

label ss_d3invite_eng:
    show ss nosmile casual at right with dspr
    #ss "{en}I'm gonna tell you guys a secret. Can you keep it?{/en}{ru}Я вам, ребята, секрет расскажу. Сдержите?{/ru}"
    ss "{enen}I'm gonna tell you guys a secret. Will you keep it?{/enen}"
    show un serious pioneer at cleft with dspr
    play music music_list["reminiscences"] fadein 3
    #"Мы с Леной кивнули."
    "Lena and I nodded."
    #ss "{en}When I go back to the States I can bring one person with me. Or invite to visit me later. It's nothing special, just a friend's visit. It's paid for and there would be no problems from any side.{/en}{ru}Когда буду возвращаться в Штаты, я смогу взять одного человека с собой. Или пригласить, а приедет потом. Ничего особенного, просто дружеский визит.{/ru}"
    ss "{enen}When I go back to the States, I'll be able to bring one person with me. Or invite one to visit me later. It's nothing special, just a friend's visit. It was paid already, and there will be no problems from either side.{/enen}"
    #ss "{en}Sorry, Sam. It can't be a boy. It would be weird.{/en}{ru}Как в школе по обмену. Конечно, поездка оплачена, и никаких проблем не будет. Прости, Семён, мальчики не рассматриваются. Это было бы странно.{/ru}"
    ss "{enen}Sorry, Sam. It can't be a boy. It would be weird.{/enen}"
    #th "Да уж... Семён с Самантой в Америке. Это был бы номер."
    th "Yeah... Semyon and Samantha together in America. It would be odd."
    #ss "{en}How about you, Lena? Would you go?{/en}{ru}А ты как, Лена? Поедешь?{/ru}"
    ss "{enen}What about you, Lena? Would you?{/enen}"
    $ un_inv = True
    show un surprise pioneer at cleft with dspr
    #un "Что? Нет-нет. Это совсем не для меня. Такое внимание."
    un "What? Oh, no. It is really not what I'm used to. So much attention."
    #me "Она говорит, внимания особо не будет. Никаких встреч и приёмов. Просто поживёшь у неё, погуляете там."
    me "She said that there wouldn't be too much attention. No formal meetings or something. You would just live in her house and walk with her, you know."
    show un serious pioneer at cleft with dspr
    #un "Спасибо, но нет. Я только займу там чьё-то место."
    un "Thanks, but no. I would just supplant someone."
    #me "Я понимаю твои опасения, но иногда можно просто махнуть рукой и сделать."   
    me "I understand your apprehension, but sometimes you just need to forget about them and do something."
    #un "Не уговаривайте, не нужно. Вот ты сам бы поехал, Семён? Если бы мог."
    un "Don't try to persuade me, there is no need in it. Would you go, Semyon, if you could?"
    #me "У меня случай особый... Этот лагерь был для меня не менее странным и чужим, чем для тебя сейчас Америка. Но тут мне вполне понравилось... Так что, почему бы и нет?"
    me "My case is special... This camp was, for me, as strange and alien as America seems for you now. But I like this place... So, why not?"
    #ss "{en}I would invite Sam, but it's about other people. And I can't ignore their opinion. It's kind of my job now, to be a good girl.{/en}{ru}Я бы пригласила Сэма, но другие не поймут. А игнорировать их мнение нельзя. Это вроде как моя работа - быть хорошей девочкой.{/ru}"
    ss "{enen}I would invite Sam, but other people wouldn't understand me. And I can't ignore them. It's a kind of my job now, to be a good girl.{/enen}"
    #me "Ага... Будь хорошей, но в меру." 
    me "Yep... Be nice, but don't overdo."
    show ss unsure casual at right with dspr
    #ss "{en}What do you mean?{/en}{ru}То есть?{/ru}"
    ss "{enen}What do you mean?{/enen}"
    #me "С этим тоже лучше не перегибать. Может плохо кончиться."
    me "Don't be too nice. It can end badly."
    show ss serious casual at right with dspr   
    #ss "{en}How?{/en}{ru}Как?{/ru}"     
    ss "{enen}How?{/enen}"
    #me "Люди не любят слишком хороших, это же вызов. Они распяли того парня, помните? И застрелили Ганди. И Леннона."
    me "People don't like those who are too nice, it's like a provocation. They crucified that guy, remember? And shot Gandhi. And Lennon."
    show un serious pioneer at cleft  with dspr
    #un "Но ведь стреляет один человек, а не все люди."
    un "But one man shoots, not everyone."
    #me "Одного стрелка хватает с лихвой. Но это только верхушка айсберга ненависти."
    me "One shooter is enough. And that's only the top of hate's iceberg."
    #un "Неужели таким как Саманта надо подстраиваться под каких-то отщепенцев и безумцев?"
    un "Should people like Samantha adjust themselves to get along with renegades and madmen?"
    #me "Не такие уж они безумцы, если это тенденция. Стреляет один, а радуются многие. К любому объекту народной любви будет и ненависть в массах, и наоборот."
    me "They aren't madmen, if it is a tendency. One man would shoot, but many others would be pleased of this deed. There always is a hate to every object of people's love, and vice versa."
    #ss "{en}Wait, I'm not Lennon, Kennedy or Martin Luther King...{/en}{ru}Постой, но я не Ганди, не Кеннеди, и не Мартин Лютер Кинг...{/ru}"
    ss "{enen}Wait, I'm not Lennon, Kennedy or Martin Luther King...{/enen}"
    #me "{en}Wow, this list is growing!{/en}{ru}Ух ты, списочек-то растёт!{/ru}"
    me "{enen}Wow, the list is growing!{/enen}"
    show ss angry casual at right with dspr 
    #ss "{en}Yeah... I mean, I'm not a great person, I'm just a girl who has to be decent. Could you spare me?{/en}{ru}Ага... Просто говорю, что я не великая и не святая, а просто девочка, которой нужно быть приличной. Пощадишь меня?{/ru}"
    ss "{enen}Yeah... I mean, I'm not a saint or a great person, I'm just a decent girl. Could you have mercy on me?{/enen}"
    #me "{en}You are a celebrity here. You know better.{/en}{ru}Это ты у нас знаменитость, тебе виднее.{/ru}"
    me "{enen}You are a celebrity here. You know better.{/enen}"
    show ss unsure casual at right with dspr   
    #ss "{en}Okay, you're right. Some people hate me for nothing, thanks for the reminder.{/en}{ru}Ладно, ты прав. Некоторые меня не любят, спасибо за напоминание.{/ru}"
    ss "{enen}Ok, you're right. Some people hate me for nothing, thanks for reminding.{/enen}"
    show un sad pioneer at cleft  with dspr
    #un "Ну что ты, Семён не это имел в виду."
    un "Hey, Semyon didn't mean that."
    show ss nosmile casual at right 
    #ss "{en}Back to the subject: any advice? Who do I need to invite?{/en}{ru}Назад к теме: посоветуете что-нибудь? Кого мне пригласить?{/ru}"
    ss "{enen}Back to the subject: any advice? Who should I invite?{/enen}"
    show un normal pioneer at cleft with dspr   
    
    menu:
        #"Алису":
        "Alisa":
            show un angry2 pioneer  with dspr
            #ss "{en}I don't know her well. But if you think so let's find her tomorrow and see.{/en}{ru}Мы с ней едва знакомы, но если ты так считаешь, давай найдём её завтра. А там посмотрим.{/ru}"
            ss "{enen}I don't know her well. But if you think so, let's find her tomorrow and see.{/enen}"
            show un serious pioneer  with dspr
            $ sp_dv += 1
            $ sp_un -= 1
        #"Ульяну":
        "Ulyana":
            if d3_usfail == True:
                show ss serious casual at right
                #ss "{en}I don't like the idea.{/en}{ru}Мне не нравится эта идея.{/ru}"
                ss "{enen}I don't like this idea.{/enen}"
                $ sp_ss -= 1
            else:
                #ss "{en}It might work. Let me think about it.{/en}{ru}Может быть. Но мне надо подумать.{/ru}"
                ss "{enen}Well, maybe. Let me think about it.{/enen}"
        #"Славю":
        "Slavya":
            #ss "{en}That was my second thought. Sure, lets ask her tomorrow.{/en}{ru}Я тоже о ней подумала. Тогда завтра её спросим.{/ru}"
            ss "{enen}That was my second thought. Sure, let's ask her tomorrow.{/enen}"
            $ sp_sl += 1
        #"Лену":
        "Lena":
            show ss normal casual at right with dspr        
            show un shy pioneer at cleft with dspr
            #ss "{en}Okay, let's give her time to think. Try to persuade her.{/en}{ru}Ладно, дадим ей время подумать. Постарайся уговорить.{/ru}"
            ss "{enen}Ok, let's give her some time to think. Try to persuade her later.{/enen}"
    stop music fadeout 4 
    #un "Если это всё... Спокойной ночи, ребята."
    un "If that's all... Good night, guys."
    #me "Но мы ведь так и не поиграли."
    me "But we haven't played yet."

    #un "Успеется."
    un "We'll be able to do it later."
    window hide
    $ renpy.pause (0.3)
    hide un  with dspr
    $ renpy.pause (0.5)
    window show
    #"Лена всё-таки вырвалась, и мы остались вдвоём."
    "Lena finally escaped, and we were left alone."
    show ss normal casual with dspr 
    play music music_list["silhouette_in_sunset"] fadein 4
    #ss "{en}Can't stop her one more time, she might think we hate to stay together.{/en}{ru}Остановить её ещё раз будет уже навязчиво. Она может подумать, что мы избегаем оставаться наедине.{/ru}"
    ss "{enen}Stopping her one more time would be weird, she might think we hate being alone together.{/enen}"
    #me "{en}Are we not?{/en}{ru}А это не так?{/ru}"
    me "{enen}And don't we?{/enen}"
    show ss surprise casual 
    #ss "{en}Of сourse not!{/en}{ru}Конечно, нет!{/ru}"
    ss "{enen}Of course we don't!{/enen}"
    show ss normal casual 
    #me "{en}But it's weird. As you said.{/en}{ru}Но это странно. Как ты сама сказала.{/ru}"
    me "{enen}But it's weird. As you said.{/enen}"
    #ss "{en}Come on! We are friends and there is nothing weird about it. And and whoever thinks differently - weirdo!{/en}{ru}Да брось! Мы - друзья, и нет тут ничего странного. Кто думает иначе - странный сам!{/ru}"
    ss "{enen}Come on! We're friends, and there is nothing weird about it. And whoever thinks differently is a weirdo!{/enen}"
    #me "{en}Yes. Friends forever. Because we never gonna see each other again and nothing ever will change.{/en}{ru}Да, друзья навек, и тут ничего не изменится. Хотя бы потому, что скоро мы разъедемся, и всё...{/ru}"
    me "{enen}Yes. Friends forever. Because we're never gonna see each other again, and nothing will ever change.{/enen}"
    if d3_usfail == True:
        show ss sad casual with dspr    
        #ss "{en}Why are you saying this? Am I not depressed enough?{/en}{ru}Зачем ты так сказал? Я недостаточно подавлена?{/ru}"
        ss "{enen}Why are you saying this? Am I not depressed enough?{/enen}"
        #me "{en}I don't know, just a joke. Just me. You don't look depressed.{/en}{ru}Не знаю, просто шутка. В моём духе. Ты не выглядишь подавленной.{/ru}"
        me "{enen}I don't know, just a joke. It's my way of joking. And you don't look depressed.{/enen}"
        #ss "{en}Yeah, I smile. It's a part of my job. It takes your life and payes in candies. And letters.{/en}{ru}Ну да, я улыбаюсь. Это часть моей работы. Которой отдаёшь всю жизнь, а оплачивается она конфетами. И письмами.{/ru}"
        ss "{enen}Yeah, I smile. It's part of my job. It takes my life and it is paid in candies. And letters.{/enen}"
        #th "Кажется, неудачная шутка вскрыла старые раны. Что теперь делать? Психолог из меня - так себе. Может, лучше оставить её в покое?"
        th "Looks like my unsuccessful joke re-opened her old sores. What should I do now? I am a bad psychologist. Maybe it would be better if I left her alone?"
        #me "{en}I'm really sorry. I guess I should go now.{/en}{ru}Прости, пожалуйста. Кажется, мне лучше уйти.{/ru}"
        me "{enen}I'm really sorry. I guess I should leave you alone now.{/enen}"
        show ss vangry casual with dspr 
        #ss "{en}And THAT is unfair. So you did something wrong and now I must comfort you and ask you to stay... or you will leave. Sure, go ahead leave this broken girl who doesn't smile anymore.{/en}{ru}А вот это нечестно. Теперь это я должна тебя успокаивать, просить остаться? Или ты уйдёшь. Ну давай, уходи, брось эту сломанную Саманту, которая больше не улыбается.{/ru}"
        ss "{enen}And THAT is unfair. You did something wrong, and now I have to comfort you and ask you to stay... or you'll leave. Sure, go ahead, leave this broken girl who doesn't smile anymore.{/enen}"
        window hide
        show ss angry casual        
        $ renpy.pause (0.7)         
        show ss cry casual with dspr 
        #th "Тебе правда только тринадцать?"
        th "Girl, are you really only 13 years old?.."
        #"Мы немного посидели молча. Итак, я только что устроил нервный срыв самой известной девочке в мире. Ещё один пункт в списке моих сомнительных достижений."
        "We sat in silence for a while. Well, I made the most famous girl in the world cry. Another point on the list of my questionable achievements."
        show ss nosmile casual with dspr 
        #ss "{en}Alright, i'm fine. Sorry for this. You may leave if you want.{/en}{ru}Ладно, я в порядке. Прости. Можешь идти, если хочешь.{/ru}"
        ss "{enen}Alright, I'm fine. Sorry for this. You may leave if you want.{/enen}"
        #me "{en}Let's see... I'm in a stupid play because I must. Ye olde Italian gay crap. And I'm not leaving. Why should I leave this cozy house? No, I don't want to. Friends, remember?{/en}{ru}Давай посмотрим... Я в дурацкой пьесе, потому что должен. В этом выпендрёжном старье для девиц XVII века. И ведь не бросаю, не ухожу. А у тебя мне вполне нравится. Зачем уходить, я не хочу. Друзья, помнишь?{/ru}"
        me "{enen}Let's see... I have to portray some ye olde Italian gay jester, yet I'm not leaving the play. Why should I leave this cozy house if I like being here? No, I don't want to leave. Friends, remember?{/enen}"
        show ss shy casual with dspr 
        #ss "{en}Hey, wanna listen to some music{/en}{ru}Эй, хочешь послушать музыку{/ru}?"
        ss "{enen}Hey, wanna listen to some music?{/enen}"
        #me "{en}Sure{/en}{ru}Конечно{/ru}."
        me "{enen}Sure.{/enen}"
        #"Я вспомнил, что в нашем с вожатой домике есть чай и печенье, и пригласил Саманту туда. Магнитофон взяли с собой."
        "I remembered that there were tea and cookies in my house, so I invited Samantha there. We took the tape recorder with us."
        play music ForeignAffair fadein 2
        scene bg int_house_of_mt_night  with fade
        $ persistent.sprite_time = 'night'
        $ night_time()
        $ renpy.pause(3)
        jump ss_d3zz_eng

    else:
        show ss serious casual with dspr    
        #ss "{en}Are you trying to upset me again? No way, I'm in a good mood after Uliana's visit.{/en}{ru}Ты пытаешься меня снова расстроить? Ну уж нет, я в слишком хорошем настроении после визита Ульяны.{/ru}"
        ss "{enen}Are you trying to upset me again? It won't do - I'm in a good mood after Ulyana's visit.{/enen}"
        #me "{en}Me too.{/en}{ru}Я тоже.{/ru}"
        me "{enen}Me too.{/enen}"
        show ss nosmile casual with dspr        
        #ss "{en}I can get some sleep finally. Yay. Thanks again.{/en}{ru}И я смогу выспаться, наконец. Ура. Спасибо тебе ещё раз.{/ru}"
        ss "{enen}Finally, I can get some sleep. Yay. Thanks again.{/enen}"
        #me "{en}Alice helped too. But why haven't you told me if it was so bad?{/en}{ru}Алиса тоже помогла. Но почему ты не сказала, что всё было так плохо?{/ru}"
        me "{enen}Alisa helped too. But why haven't you told me that it was so bad?{/enen}"
        if sp_ss >= 6:
            show ss serious casual with dspr        
            #ss "{en}I've been thinking about leaving the camp, actually{/en}{ru}Вообще-то, я подумывала уехать из лагеря.{/ru}."
            ss "{enen}I've been thinking about leaving the camp, actually.{/enen}"
            #th "Неужели она вот так бы всё бросила, даже не посоветовавшись со мной?"
            th "She decided to give it up without even consulting with me? Really?"
            #me "{en}Next time you should tell me first. That's what friends do. Promise me.{/en}{ru}В следующий раз ты сначала расскажешь мне. Друзья делают так. Обещай.{/ru}"
            me "{enen}Next time you should tell me first. That's what friends do. Promise me.{/enen}"
            show ss normal casual with dspr 
            #ss "{en}I will.{/en}{ru}Обещаю.{/ru}"
            ss "{enen}I promise.{/enen}"
        else:
            #ss "{en}I don't like to complain. It can make things worse.{/en}{ru}Не люблю ябедничать. Это может сделать всё только хуже.{/ru}"
            ss "{enen}I don't like complaining. It can make things worse.{/enen}"
        #"Мы немного почитали пьесу, но без особого энтузиазма. Саманта никак не хотела меня отпускать. Она предложила послушать музыку."
        "We read the play a little, but without special enthusiasm. Samantha didn't want me to leave. She proposed listening to music."
        #"Я вспомнил, что в нашем с вожатой домике есть чай и печенья, и пригласил Саманту туда. Магнитофон взяли с собой."
        "I remembered that there were tea and cookies in my house, so I invited Samantha there. We took the tape recorder with us."
        window hide
        $ renpy.pause(0.4)      
        scene bg int_house_of_mt_night  with fade
        play music Luftballons fadein 2
        play ambience ambience_int_cabin_evening
        $ persistent.sprite_time = 'night'
        $ night_time()
        $ renpy.pause(3)

        #"Мы стали слушать музыкальные новинки на магнитофоне. Некоторые песни и правда были мне в новинку."
        "We started to listen to music novelties on the recorder. Some songs really were new to me."
        jump ss_d3zz_eng

label ss_d3zz_eng:
    show ss normal casual with dspr 
    #ss "{en}What music do you like? Tell me your favorite bands.{/en}{ru}Какую музыку предпочитаешь? Любимые группы?{/ru}"
    ss "{enen}What music do you like? Tell me your favorite bands.{/enen}"
    #"Сходу мне ничего не пришло в голову, кроме банальных \"Битлов\"."
    "At first, nothing crossed my mind, except the commonplace \"Beatles\"."
    #ss "{en}Bob Marley maybe{/en}{ru}Боб Марли, может{/ru}?"
    ss "{enen}Bob Marley maybe?{/enen}"
    #me "{en}Yes. Is he already... dead? Right{/en}{ru}Да... Он уже... умер? Так ведь?{/ru}?"
    me "{enen}Yes. Is he already... dead?{/enen}"
    show ss unsure casual with dspr   
    #ss "{en}Already? But he's died young, really young.{/en}{ru}Уже? Но он умер молодым, ему даже 40-ка не было.{/ru}"       
    ss "{enen}Already? But he died young, really young.{/enen}"
    #me "{en}I mean... I heard he was sick.{/en}{ru}В смысле... Я слышал, что он болел.{/ru}"
    me "{enen}I mean... I heard he was sick.{/enen}"
    show ss serious casual
    #ss "{en}He's died three or four years ago.{/en}{ru}Он умер три или четыре года назад.{/ru}" 
    ss "{enen}He died three or four years ago.{/enen}"
    #me "...{en}Terrible news.{/en}{ru}Ужасные новости.{/ru}"  
    me "{enen}...Terrible news.{/enen}"
    show ss unsure casual  with dspr
    #ss "{en}And I hate to tell them.{/en}{ru}И мне не по душе их сообщать.{/ru}"
    ss "{enen}And I don't like telling them.{/enen}"
    show ss nosmile casual 
    #extend "{en}So don't even ask me about Elvis.{/en}{ru}Так что, лучше и не спрашивай про Элвиса.{/ru}"
    extend "{enen} So don't even ask me about Elvis.{/enen}"
    #me "{en}At least Kurt Cobain is still there. He must be about your age now...{/en}{ru}Хотя бы Курт Кобейн еще с нами. Сейчас он чуть старше тебя, наверное...{/ru}"
    me "{enen}At least Kurt Cobain is still here. He must be about your age now...{/enen}"
    show ss serious  casual with dspr
    #ss "{en}Who is it, I'm sorry?{/en}{ru}А кто это, прости?{/ru}"  
    ss "{enen}I'm sorry, but... Who is he?{/enen}"
    #me "{en}Some talented boy, nevermind...{/en}{ru}...Один талантливый парнишка.{/ru}"      
    me "{enen}Some talented boy, nevermind...{/enen}"
    #th "Срочно сменить тему."
    th "I need to change the subject, and fast."
    menu:
        #"Мне нравится твоя музыка":
        "I like your music":
            #me "{en}Actually I like this music.{/en}{ru}Вообще-то, мне нравится эта музыка.{/ru}"
            me "{enen}Actually I like this music.{/enen}"
            show ss smile casual with dspr             
            #ss "{en}Really{/en}{ru}Правда{/ru}?"
            ss "{enen}Really?{/enen}"
            $ sp_ss += 1
        #"Вообще-то я больше по классике":
        "I prefer classical music actually":
            #me "{en}I prefer classical music anyway.{/en}{ru}Всё равно мне больше по нраву классическая музыка.{/ru}"         
            me "{enen}Anyway, I love classical music the most.{/enen}"
            show ss nosmile casual with dspr        
            #ss "{en}Oh. Serious Sam{/en}{ru}А. Серьёзный Сэм{/ru}."
            ss "{enen}Oh. Serious Sam.{/enen}"
        #"Я слушаю музыку будущего":
        "I listen to music of the future":
            #me "{en}I prefer our modern music. Music of the future.{/en}{ru}Я предпочитаю нашу современную музыку. Музыку будущего!{/ru}"
            me "{enen}I prefer our modern music. Music of the future!{/enen}"
            show ss normal casual with dspr         
            #ss "{en}Could you sing me something?{/en}{ru}Можешь спеть мне что-нибудь?{/ru}"
            ss "{enen}Could you sing something for me?{/enen}"
            #me "{en}I'm a bad singer and there's nothing to sing actually. More music than words.{/en}{ru}Из меня плохой певец, да и петь там особо нечего. Больше музыки, чем слов.{/ru}"
            me "{enen}I'm a bad singer, and there's not much to sing actually. It's more music than words.{/enen}"
            show ss laugh casual with dspr 
            #ss "{en}I bet you are better singer than a liar.{/en}{ru}Уверена, певец из тебя лучше, чем лжец.{/ru}"
            ss "{enen}I bet you're a better singer than a liar.{/enen}"
            me "{enen}All right then! {w}Ahem. {w}{i}We're all living in America! Amerika ist wunderbar.{/i}{/enen}"
            window hide
            show ss surprise casual with dspr 
            window show                       
            me "{enen}{i}We're all living in America! Coca-Cola, sometimes...{/i} Well.{/enen}"
            show ss smile casual with dspr
            
            
    window hide      
    $ renpy.pause(1)
    show ss smile2 casual   
    stop music fadeout 5
    #"После того, как мы обсудили перспективы молодой певицы Мадонны, я решил, что пора-таки гнать Саманту спать, да и самому на боковую."
    "After we discussed perspectives of a young singer Madonna, I decided that it was time for Samantha to go to sleep."
    window hide
    if sp_sp >= 5:
        $ renpy.pause(1)    
        jump ss_d4start_eng
    else:
        jump ss_d3letters_eng
        
label ss_d3letters_eng:
    show ss sad casual  with dspr
    ss "{enen}Evening was good...{/enen}"
    me "{enen}Very good. But Olga is gonna return soon. She'll not be pleased with our late party.{/enen}"
    ss "{enen}Right...{/enen}"
    me "{enen}Sleep well. Nothing will bother you tonight.{/enen}"
    ss "{enen}...Goodbye.{/enen}"
    window hide 
    $ renpy.pause(1)
    hide ss with dspr
    $ renpy.pause(0.3)  
    play sound sfx_close_door_1
    $ renpy.pause(0.7)    
    scene black with fade
    $ renpy.pause(2.0)  
    play ambience ambience_int_cabin_night  
    scene bg int_house_of_mt_night2 
    show unblink
    $ renpy.pause(2) 
    th "What happened? Where am I?"
    #mt "Разбудила? Спи, ещё рано."
    mt "Did I wake you up? Sleep, it's still early."
    #th "У меня дежавю."
    th "I had a feeling of deja vu."
    #me "Опять Шурик сбежал?"
    me "Has Shurik run away again?"
    #mt "Нет. Не Шурик."
    mt "Nope. Not Shurik."
    #th "А хоть бы и Шурик."
    th "Well, I wouldn't be sad if Shurik had really run away."
    #"Я зарылся лицом в подушку, но сон уже отступил."
    "I bury my head into a pillow, but dreams have already retreated."
    #me "А кто?"
    me "So... Who?"
    play sound sfx_click_1  
    scene bg int_house_of_mt_night  with dissolve
    show mt sad pioneer  with dspr
    #mt "Саманта уехала."
    mt "Samantha."
    #me "Что? Куда?"
    me "What? Where is she?"
    #mt "Домой."
    mt "She has gone home."
    #me "Как уехала? На чём?"
    me "How has she gone?"
    #mt "У нас была машина на такой случай."
    mt "We had a car for cases like that."
    #me "Вы серьёзно? Но почему?"
    me "Are you serious? But why did she?.."
    #mt "Не знаю. Может, ты мне объяснишь? Она тебе письмо оставила."
    mt "I don't know. Maybe {i}you{/i} will explain it? She left a letter for you."
    #"Я схватил листок. В переводе на русский написано там было следующее:"
    "I grabbed the piece of paper. On it was written:"
    if sp_ss >= 6:
        jump ss_lettergood_eng
    else:
        jump ss_letterbad_eng


label ss_lettergood_eng:
    scene black 
    with fade

    $ set_mode_nvl()
    play music music_list["a_promise_from_distant_days"] fadein 2

    #"Дорогой Сэм!"
    "Dear Sam!"
    #"Я страшная трусиха, бросить всё вот так. Мне очень стыдно, но по-другому не могла. Если бы я сказала, то ты бы меня отговорил."
    "I am a big coward because I left it all like this. It's a shame, but I was unable to do anything else. If I had told you, you would have stopped me."
    #"«Совёнок» был очень добр ко мне, но ты был прав - где любовь, там и ненависть, и печаль. Нужно быть сильным, а я сейчас слаба. Я искала спокойный уголок, а получилось наоборот."
    "\"Sovyonok\" was really kind to me, but you were right. If there is love, than there is hate and sorrow too. One should be strong, but I'm weak now. I've tried to find a peaceful place, but the opposite has happened."
    #"Наверное, ты подумаешь, что кто-то меня обидел, но дело не в этом. Никто не виноват. Всё равно я бы скоро уехала, но с каждым днём мне становилось всё грустнее, поэтому лучше сейчас. Пока не стало хуже."
    "You may think that someone has offended me, but that's not the point. No one is to blame. I would have gone home soon anyway. But day by day I was getting more sad, so it was better to leave right now, before it became worse."
    #"Не вышло у нас с пьесой, попроси у ребят прощения за меня. Я бы хотела им всем написать, но сейчас нет ни времени, ни сил. Но я оставлю им что-нибудь на память."
    "The play failed because of me. Tell the guys that I'm sorry. I wanna write them, but I have no time and no strength for it. But I will leave something for them, in remembrance of me."
    #"Не знаю, что тебе подарить. Подумала про медальон со Святым Христофором, но ты ведь не любишь путешествовать. Пусть он приносит мне удачу и дальше."
    "I don't know what to give you. I thought about the medallion with St. Christopher, but you don't like traveling. Let it stay with me as my sign of luck."
    #"Оставляю тебе магнитофон, мы не послушали одну песню, это Simple Minds - Don't You Forget About Me. Если я включу её сейчас, то совсем испорчу бумагу слезами."
    "I left the tape recorder for you. There's only one song we haven't listened to: \"Don't You Forget About Me\" by Simple Minds. If I listened to this song now, I would totally screw up this letter with my tears."
    ## "I left the tape recorder for you. We haven't listened to one song, Simple Minds - Don't You Forget About Me. If I started playing this song, I would totally screw paper up with tears."
    #"Прости мне это сентиментальное письмо, но у меня предчувствие, что ты был прав (снова), и мы больше не увидимся. Но ты мне обязательно напишешь, я верю. Ругай меня, ненавидь меня, но напиши. Оставляю тебе свой конверт, чтобы не пропустить его в ворохе писем."
    "I'm sorry for this sentimental letter, but I have a feeling that you were right (again), and we will never see each other again. But I believe that you will write me for sure. Scold me, hate me, but write me. I'm leaving you my envelope because I don't wanna miss your letter in a heap of other letters."
    #"Всегда твоя, Саманта."
    "Forever yours, Samantha."
    
    #"P.S. {i}Моё лицо спасает темнота, А то б я, знаешь, со стыда сгорела, Что ты узнал так много обо мне.{/i}"
    "P.S. {i}Thou knowest the mask of night is on my face; Else would a maiden blush bepaint my cheek For that which thou hast heard me speak to-night.{/i}"
    #"{i}Хотела б я восстановить приличье, Да поздно, притворяться ни к чему.{/}"
    "{i}Fain would I dwell on form, fain, fain deny What I have spoke; but farewell compliment!{/i}"
    #"{s}Помнишь, как там дальше?{/s} Прости."
    "{s}Remember, what next?{/s} I'm sorry."
    $ renpy.pause(3)
    $ set_mode_adv()
    jump ss_d3end2_eng


label ss_letterbad_eng:
    scene black  with fade

    play music music_list["confession_oboe"] fadein 3
    $ renpy.pause(1)
    $ set_mode_nvl()

    #"Дорогой Семён!"
    "Dear Semyon!"
    #"Извини, что вот так уезжаю, даже не попрощавшись. Но я должна, и так это сделать проще. "
    "I'm sorry that I'm leaving like this, without even saying goodbye. But I have to do it, and this is the easiest way."
    #"{s}Почти{/s} все были очень добры ко мне, а ты - особенно. И сделал всё, чтобы мне понравилось в «Совёнке». Но иногда тяжело плыть против течения. Я поняла, что мне здесь всё-таки не место."
    "{s}Almost{/s} everyone was kind to me, especially you. You've done everything to make me like \"Sovyonok\". But it's so hard to go against the stream. I've realized that it isn't the place for me."
    #"Я всё ещё верю, что люди из разных стран могут жить рядом в мире и дружбе, но за это нужно бороться. Любовью и терпением. Наверное, мне этого не хватает. Сейчас мне хочется отдохнуть."
    "I still believe that people from different countries can live in peace and friendship, but we have to struggle for it. Struggle by love and patience. Maybe I'm in lack of it. Now I want to relax."
    #"Простите, что подвела с пьесой. Уверена, что вы меня замените и всё пройдёт хорошо! (Лена знает мою роль, уговори её, тебя она послушает)."
    "I'm sorry that I let you down with the play. I'm sure that you guys will find someone to substitute for me and everything will be fine (Lena knows my role, try to persuade her, she will accept your suggestion)."
    #"Не знаю, что тебе подарить на память, поэтому оставляю свой магнитофон (хоть таскать его не придётся). Передавай всем приветы, скажи, чтобы не обижались. Я оставлю несколько записок, но времени мало."
    "I don't know what to give you for a memory, so I'm leaving my tape player for you. At least I won't need to carry it with me anymore. Tell others not to take offense about me. I'm leaving a couple of letters for them, but I don't have much time."
    #"И обязательно напиши мне, ведь я твоего адреса не знаю. Будем жить!"
    "Please, write me a letter, because I don't know your address. Everything will be ok!"

    #"Твоя Саманта."
    "Your Samantha."
    $ renpy.pause(2)
    $ set_mode_adv()
    jump ss_d3end1_eng

label ss_d3end1_eng:
    scene bg int_house_of_mt_night  with dissolve
    show mt sad pioneer  with dspr
    #me "Так не должно было быть. Мы можем вернуть её?"
    me "It's not supposed to happen. Can we bring her back?"
    #mt "Нет."
    mt "No."
    #me "Это я виноват."
    me "It's my fault."
    #mt "Нет, конечно. Ты старался больше всех. Как ты можешь быть виноват?"
    mt "Of course not. You tried more than the others. How can you be the guilty one?"
    #me "У меня ощущение, будто я не сказал или не сделал чего-то. Или... Забыл что-то."
    me "But I feel that I haven't said or done something. Or... Like I've missed something."
    show mt smile pioneer  with dspr
    #mt "Не терзай себя. Даже если ты чего-то не сказал, Саманта тебя знает. Она девочка наблюдательная, и вы провели много времени вместе. Слишком много, быть может."
    mt "Don't blame yourself. Even if you haven't said something, Samantha knows you. She is an observant girl, and you've spent a lot of time together. Too much time maybe."
    #th "Даже пытаясь поддержать меня, она не упустила случая и укорить."
    th "Even trying to cheer me up, she doesn't miss a chance to blame me."
    show mt normal pioneer  
    #mt "Ты можешь сказать, что там в письме? Почему она уехала?"
    mt "Could you explain what's there in that letter? Why has she left?"
    #me "Ей было тут тяжело, но никого конкретно не винит."
    me "It was hard for her to stay here. But she doesn't blame anyone."
    #mt "Надо придумать, что всем сказать. Может, что заболела. А ты не унывай. Письмо ей напишешь."
    me "I have to think what to say to other pioneers. Maybe that she became ill. And don't you be sad. You'll write a letter for her."
    show mt smile pioneer  
    #mt "Лето продолжается."
    ##mt "Summer hasn't ended yet."
    mt "Summer still is going on."
    window hide

    $ renpy.pause(2)
    scene black with fade  
    $ renpy.pause(1)    
    scene cg sam_leaving with fade2
    
    $ persistent.ssg_107 = True 
          
    $ renpy.pause(6.5)
    $ set_mode_nvl()
    window show
    nvl clear  
    #"Эта история Семёна заканчивается. {w}Но он вернётся в следующих сериях."
    "This story of Semyon ends. {w}But he will return in a new series."

    $ renpy.pause(1.5)
    play sound sfx_achievement
    nvl clear   
    #"Концовка: сдержанное письмо."
    "Ending: restrained letter."
    $ renpy.pause(1.5)    
    $ set_mode_adv()
    window hide 
    menu:
        "Exit":    
            return
        "Back to the beginning":
            stop music fadeout 3        
            jump sam  
  

label ss_d3end2_eng:
    scene bg int_house_of_mt_night  
    show mt sad pioneer 
    with dissolve
    #"Я бросился в домик Саманты."
    "I rushed into Samantha's house."
    scene bg night_sam_empty
    show stereo
    with fade
    #"Пусто. О Саманте напоминали только игрушки и прочие подарки, разложенные по постели. Над каждым был листочек с именем. И магнитофон. Я нажал кнопку \"play\"."
    stop music fadeout 3
    "Empty. Only toys and other gifts, which lied on bed, reminded me about Samantha. At every thing was a label with the name. And the tape recorder. I pressed the \"play\" button."
    play music DontYou
    $ renpy.pause(3)
    show mt sad pioneer  with dspr
    #mt "Семён..."
    mt "Semyon..."
    #me "Не включайте свет."
    me "Don't turn on the light."
    #"Причину можно было понять по моему голосу."
    "The reason was clear in my voice."
    #mt "Оставить тебя?"
    mt "Should I leave you alone?"
    #me "Да. Спасибо."
    me "Yes. Thanks."
    window hide
    $ renpy.pause(0.5)  
    hide mt  with dissolve
    $ renpy.pause (1)
    window show
    #th "Что дальше? Конечно, Саманта - это не весь мир, и не весь лагерь. Но без неё тут будет пусто. Всё будет уже не так."
    th "What next? Of course, Samantha isn't the whole world or the whole camp. But this place seems empty for me without her. Everything will no longer be the same."
    #th "Со мной даже никто говорить не станет, ведь она уехала из-за меня. А её все любили."
    th "No one will even talk to me because she left because of me. And everyone loved her."
    #th "Нет, это должен быть сон. Сейчас я крепко зажмурюсь, потом открою глаза, и наваждение отступит. Наступит утро, и всё будет как раньше. Это всё, чего я хочу."
    th "No. This must be a dream. I will shut my eyes tight, then open them, and the delusion will retire. Morning will come, and everything will be as before. That's all I want."
    window hide
    show blink
    $ renpy.pause(1)    
    scene black 
    $ renpy.pause(2)
    scene bg night_sam_empty
    show stereo
    show unblink    
    $ renpy.pause(1.7)
    #th "Не сработало. Но я не хочу так. И не буду. {w}Должна быть возможность вернуть всё, я верю. Только бы найти способ вырваться."
    th "It didn't work. But I don't want and I won't... {w}There must be a way to turn it all back, I believe. I only need to find a way to escape."
    #"На столе стояла недобитая бутылка колы, а в конфетнице валялась пачка ментоса. Я усмехнулся (хотя получился всхлип)."
    "There was an unfinished Coca-Cola bottle on the table and a pack of Mentos in a candy dish. I wanted to grin, but rather, sobbed."
    #th "Это оставим на крайний случай."
    th "Let's put it aside for an emergency."
    window hide
    $ renpy.pause(1.5)
    scene black with fade  
    $ renpy.pause(1)    
    scene cg sam_leaving with fade2
    
    $ persistent.ssg_107 = True 
          
    $ renpy.pause(6.5)
    $ set_mode_nvl()
    window show
    nvl clear      
    #"Эта история несчастного Семёна заканчивается. {w} Но он вернётся в следующих сериях. {w} Ещё более несчастным."
    "This story of an unhappy Semyon ends. {w} But he will return in a new series. {w} Even more unhappy."
    $ renpy.pause(2)
    scene black  with fade
    play sound sfx_achievement
    nvl clear   
    #"Концовка: мокрое письмо."
    "Ending: wet letter."
    $ renpy.pause(1.5)    
    $ set_mode_adv()
    window hide 
    menu:
        "Exit":    
            return
        "Back to the beginning":
            stop music fadeout 3        
            jump sam  
            



label ss_d4start_eng:

    scene black  with fade2
    $ renpy.pause(1.5)
    $ persistent.sprite_time = 'day'
    $ day_time()
    play ambience ambience_int_cabin_day
    scene bg int_house_of_mt_day
    show unblink
    $ renpy.pause(2)    
    #"Я проснулся в обычное время, совершенно не выспавшись, как и положено в лагере. Но вожатой уже и след простыл, поэтому я без особых колебаний завалился спать дальше, и некому было меня остановить."
    "I woke up at my normal time, feeling sleepy as always. With the camp leader gone, there was nobody around to drag me out of bed; I went back to my dreams."
    window hide
    $ renpy.pause (1)
    scene black  with fade
    $ renpy.pause(1.2) 
    play music music_list["door_to_nightmare"] fadein 5
    show ss serious fancy with dissolve2
    $ renpy.pause(0.5)     
    #"Мне приснилась Саманта. Она у меня что-то спрашивала на незнакомом языке. Вопрос всё повторялся и повторялся.{w} Я сделал усилие, попытался понять вопрос. И понял."
    "I had a dream about Samantha. She was asking me something in an unfamiliar language. One question, repeated over and over.{w} I made an effort, trying to understand the question. And I did."
    #ss "Что со мной будет?"
    ##ss "What will be with me?"
    ss "What will become of me?"
    me "I don't know."
    ss "You do."
    me "I don't remember."
    #ss "Теперь помнишь."
    ##ss "You remember it now."
    ss "Now you do."
    "..."
    #me "Но что мне теперь делать? Это так тяжело."
    me "But what should I do now? It's so hard."
    #ss "Если хочешь покоя, то уходи. {i}Они{/i} тебе помогут. Это путь на остров."
    ss "If you want peace, leave. {i}They{/i} will help you. It's the way to the island."
##
    #me "Остров?"
    me "Island?"
    #ss "Ты поймёшь."
    ss "You'll understand."
    #me "А если не хочу уходить?"
    me "What if I don't want to leave?"
    show ss nosmile fancy   
    #ss "Тогда загляни в лес."
    ss "Then you should go to the forest."
    window hide    
    $ renpy.pause(1)
    hide ss with dissolve
    $ renpy.pause(1)   
    window hide
    
    jump ss_d4_eng

label ss_d4_eng:
    $ new_chapter(4, u"Samantha: Day Four")
    $ day_time()
    $ persistent.sprite_time = "day"
    play ambience ambience_int_cabin_day
    scene bg int_house_of_mt_day   
    show unblink 
    $ renpy.pause(1.5)    
    #"Я проснулся в холодном поту. Мертва! Вот, что с ней будет! Девочка, письмо, Артек, смерть. Как я мог это забыть?"
    "I woke up in terror."
    th "Dead! That's what will happen of her!"
    th "Samantha Smith. The letter to Andropov. Girl, letter, Artek, death. How could I forget it?"
    #"Я пытался спорить сам с собой, убеждать себя, что это только кошмар. {w}Но нет, это я знал точно: там, в моей жизни, она была давно мертва."
    "I was very confused still. I argued with myself, tried to convince myself that it was just a nightmare."
    "But nothing helped, because I knew it for sure: in my reality this girl had been dead for a long time."
##she was long since dead? She had died a long time ago?
    #th "Но как? Какая-то авария, катастрофа? Пожалуй. Но какая именно?"
    "I couldn't remember yet what exactly happened to her. It must've been some kind of an accident, a big catastrophe that everyone heard about, even me."
    "I was sitting on the bed for some time, trying to recall something else."
    "..."
    th "A catastrophe... I don't know..."
    th "Well, now I got a catastrophe of my own, that's for sure! How do I get through all this? What should I do?"
    "I took a big breath."
##But how/why? (what was the reason of death)
##I guess -> Probably
    #th "...Не знаю. Зато у меня тут точно катастрофа. Но нужно собраться. Я сильно проспал: и линейку и завтрак."
    th "At first I need to calm down. What is my situation right now? I'd majorly overslept and definitely missed both the lineup and breakfast."
    #"Я заметил какую-то еду на столике: плавленые сырки, хлеб и кефир. Это значило, что меня официально оставили в покое."
    "I noticed some food on the table: processed cheese, bread and kefir. That meant I was officially on my own - first good news of the morning. I've put a piece of bread in my mouth."
    th "There is no need for panic. We are still in the camp, Samantha is certainly safe for now. I shouldn't act if I'm not sure what I'm doing."
    th "What is my plan then? Can I meet her now?.. {w}I don't think so."
    th "I need to be prepared for the big talk. It's better to avoid her until I'm ready." 
    #"Для начала я заверил себя, что с пока Самантой всё в порядке. Но и рассудил, что сейчас мне нельзя её видеть, я не готов." 
    #th "...Да и лучше бы никого не видеть. Уйти куда-нибудь в тихое место."
    th "...Don't want to see anyone at all in fact. I need to find a quiet place."
    menu:
        #"Отправиться на остров":
        "Go to the island":
            #"Я решил, что неплохим вариантом будет взять лодку и уплыть куда-нибудь подальше. Но сначала надо добраться до лодочной станции незамеченным."
            "I decided that it would be a good idea to take a boat and row away somewhere. But first I had to get to the boathouse unnoticed."
            jump ss_d4notsomain_eng

        #"Пойти в лес":
        "Go to the forest":
            #"К счастью, до леса рукой подать. Я вылез через окно, не допустив даже возможности встречи с кем-либо."
            "Fortunately, the forest was no distance at all. I climbed out the window, eliminating the possibility of bumping into someone."
            jump ss_d4main_eng

label ss_d4notsomain_eng:
    scene bg ext_house_of_mt_day  with dissolve
    play ambience ambience_day_countryside_ambience
    #"Приоткрыв дверь и убедившись, что никто не ждёт меня снаружи, я вышел и крадучись направился к берегу. {w}Особых умений ниндзя не понадобилось - мне никто не встретился. Должно быть, в этот жаркий день все на пляже. Но я обойду их."
    "Carefully opening the door and ensuring that nobody was waiting for me outside, I went out and sneaked toward the shore."
    "Ninja skills were not needed - I didn't see anybody. During hot days like this, there was no one on the camp's streets. Everyone was at the beach, hiding from the sun in the water." 
    stop music fadeout 5
    scene bg ext_boathouse_day  with dissolve
    play ambience ambience_boat_station_day
    #"А вот и лодочная с её мостками. Я запрыгнул в лодку. Вёсел в ней не оказалось, в соседней - тоже. И нигде. Должно быть, убрали после похождений Шурика. Как плохо."
    "Here is the boathouse. I stepped onto wooden footbridges..."
    play sound sfx_put_sugar_cart    
    extend " and jumped into a boat." with vpunch
    "There were no oars in it, as well as in the next one. And as anywhere. They must have been taken away after the incident with Shurik." 
    #"Уходить не хотелось. Немного подтащив лодку к мосткам, чтобы быть в тени и вне видимости с берега, я залёг на дно. В обоих смыслах."
    "It was upsetting but I didn't want to leave, not that easy. I pulled the boat closer to the footbridges so I could stay in the shade and out of sight from the shore. I decided to lie low in both meanings."
    play music music_list["goodbye_home_shores"] fadein 5  
    #th "Что дальше? Могу ли я сказать ей? Но как? Могу ли я не говорить? Смогу ли сделать вид, что всё в порядке?"
    th "What next? Should I tell her? But how?"
    th "Or should I stay silent? Will I be able to pretend that everything is okay?"
    #"От этих мыслей совсем не становилось легче. Я стал жевать завтрак, который прихватил с собой в пакете. Опять эти сырки \"Дружба\"..."
    "These thoughts didn't make anything better. I chewed the food that I'd taken with me, even the processed cheese \"Friendship\", without even noticing it. I must've been in a real trance."
    #"Я понял, что мне нужно делать. Нужно плакать - вот что. Когда это было в последний раз? Иногда это сделать весьма сложно. Иногда тем сложнее, чем серьёзней повод. Но мне удалось."
    "I understood what I needed to do. I needed to cry, that's what. {w}How long had it been since the last time?"
    "Sometimes it's really difficult. The more serious the reason is - the more difficult it is to do so. {w}But I managed to."
    #"И вот, когда я только входил во вкус, послышались шаги. Всё, пропал!"
    ##"And now, just when I made some progress, I heard footsteps. I'm dead!"
    "And then, just when I was in the middle of the process, I heard footsteps."
    th "Gosh, not now! I'm a goner!"
    #un "Ой!" with hpunch
    play sound sfx_boat_impact
    un "Oh!" with hpunch
    show un surprise pioneer at left   with dspr
    #"Лена едва на меня не наступила. И чем ей приглянулась моя лодка?"
    th "She nearly stepped on me! Am I not miserable enough?"
    show un shy pioneer at left   with dspr
    #un "Прости, я только хотела..."
    un "Sorry, I just wanted to..."
    #me "Вёсел всё равно нет."
    me "Get a boat? There are no oars anyway."
    #un "Жаль. Но они на станции, наверное. Можно сходить попросить ключ."
    un "I see... They must be inside the boathouse. We can ask for the key. {w}I mean, you can.{w} Anyone can..."
    show un sad pioneer at left   with dspr
    #me "И правда. Зря я так расстраивался."
    me "I suppose so... It seems I was upset over nothing."
    #"Я улыбнулся и попытался вытереть слезы. Лена всё ещё смотрела на меня с сочувствием."
    "I smiled and tried to wipe away my tears. Lena was still looking at me with sympathy."
    #un "Ну я пойду тогда?"
    un "Well, I should probably get going."
    #me "А как же лодка?"
    me "And what about the boat?"
    show un normal pioneer at left   with dspr
    #un "Обойдусь, сама не знаю, зачем пришла."
    un "It's not like I need it. Don't even know why I came here."
    if sp_un >= 6:
        #th "Если мне нужен слушатель, то Лена будет отличным вариантом. А он мне нужен."       
        th "If I need a listener, Lena is an excellent option. And I do need a listener."
    else:
        #th "Мне так хочется рассказать кому-нибудь... Но нужно ли? Должна ли это быть Лена?"
        th "It would be nice to talk to someone... But should I? And should it be Lena?"
        menu:
            #"Лучше уйти отсюда":
            "Better get out of here":
                #th "Я хотел покоя. Поищу его в другом месте."
                th "I wanted some peace. I can find it somewhere else."
                #me "Нет ты оставайся, а я пойду. Всё равно уже уходить хотел."
                me "No, you can stay. I was about to leave anyway."
                #un "Как хочешь. Но я не..."
                un "As you wish, but I..."
                #me "Увидимся."
                me "See you."
                window hide
                $ renpy.pause(0.9)
                scene bg ext_houses_day with dissolve
                $ renpy.pause(0.7)
                window show             
                #th "Теперь только в лес."
                th "I guess forest is the only way... Forest it is."
                window hide             
                $ renpy.pause(0.5)              
                jump ss_d4main_eng              
            #"Остаться с Леной":
            "Stay with Lena":
                #th "Я поделюсь с ней этим."      
                th "I'll share it with her."   
    #me "Останься. Пожалуйста."
    me "Can you stay?.. Please?"
    #un "Что-то случилось? Серьёзное?"
    un "Did something happen? Something serious?"
    #me "Да."
    me "Yes. Maybe."
    #"Пока я думал, с чего начать, пришли какие-то ребята и открыли станцию. С ними была Женя."
    "While I was thinking about where to start, a few others came along and opened up the station. Zhenya was among them."
    show un shy pioneer at left   with dspr
    #mz "Эй, вы! У гондольера выходной, берите вёсла сами."
    mz "Hey, you! The gondolier has the day-off, so help yourself."
    #"Женя и компания забрали несколько коробок и потащили в сторону лагеря. Мы снова остались одни. Пожалуй, для откровений нужно более укромное место."
    "Zhenya and her company took several boxes and dragged them toward the camp, looking at us enviably because we were idle."
    "Once again, we were alone, but it was too easy to spot us there. A place for revelations should be more private."
    stop music fadeout 4
    #me "Солнце припекает. Может, правда возьмём вёсла и на остров сплаваем? Там тенёк."
    me "The sun is rather hot. Maybe we should take the boat and go to the island? There're a lot of trees there, we can relax in their shadows for a while."
    #un "Давай."
    "Lena nodded timidly."
    me "I'm sorry, it must be a strange invitation..."
    un "No, it's... fine. Let's go."
    window hide
    $ renpy.pause(0.5)    
    scene cg un_boat with dissolve
    $ renpy.pause(0.5)
    window show
    #"Гребля отнимала много сил - на важный разговор их не оставалось. Я решил подождать с этим до острова."
    "The rowing required too much energy to start the important conversation on our way. I decided to wait until we got to the island."
    #me "Ты ведь... Туда и собиралась? На остров?"
    me "You... planned to go there? To the island?"
    un "Yes."
    #me "...Часто там бываешь?"
    me "...Do you go there often?"
    #un "Иногда."
    un "Sometimes."
    #me "Тяжело каждый раз грести, наверное?"
    me "It must be difficult to row every time on your own."
    #un "Немного. Но я потихоньку."
    un "A bit. But I save my strength. {w}Do you need help?"
    #me "Подплываем."
##coming closer to the island
    me "No-no! We're almost there."
    window hide
    $ renpy.pause(0.9)
    play ambience ambience_day_countryside_ambience     
    scene bg ext_island_day
    show un normal pioneer  
    with dissolve2
    $ renpy.pause(0.5) 
    "Tying the boat to the nearest tree, we slowly walked around, following the shore."    
    play music music_list["trapped_in_dreams"] fadein 7
    #th "С чего начать? Пожалуй, со всего. Я не хочу рассказывать про плохое предчувствие, не хочу сохранять лицо, пусть она услышит всё. Не растреплет же."
    ##th "Where should I start? From the very beginning, I guess. I don't want to tell about just a bad feeling, don't want to save face. I'll tell her everything. She can keep secrets."
    th "Where should I start? From the very beginning, I guess."
    th "I don't want to tell her about just a bad feeling, don't want to save face. I'll tell her everything. She won't let the secret out, will she?"
    #th "Это вообще не в духе Лены, по-моему. А тут еще и разговор такой странный назревает, кто вообще поверит? {w}Ладно, вперёд!"
    ##th "She's that kind of person. Especially when it's such a strange conversation that no one would ever believe. {w}Okay, let's start!"
    th "Yeah, Lena can keep a secret, I suppose. It's such a strange conversation, no one would believe anyway. {w}Okay, let's start!"
    #me "Для начала, я не из этого места."
    me "To begin with, I'm not from this place."
    #un "Ну, я... Мы с Алисой тоже не совсем отсюда."
    un "Well... Alisa and I are not exactly from here either."
    #me "Я не об этом. Помнишь, я вчера говорил про будущее?"
    me "That's not what I mean. Yesterday I told you about the future, remember?.."
    #"И я рассказал всё, начиная от ночи с автобусом. Это заняло какое-то время. Лена внимательно слушала, и почти не перебивала."
    "And I told her everything, starting from the night with the bus. Lena listened to me attentively, never interrupting."
    window hide
    with fade2
    window show    
    #me "Что скажешь? Я не прошу верить."
    me "...What can you say about all of that? {w}I'm not asking you to believe me."
    #un "Это так странно... Но ты веришь в это, я вижу."
    un "That is so strange... But you do believe in it, I see."
    #me "На сто процентов. Получается, если ты веришь хоть чуточку, то... Это будет скорее правдой, чем неправдой. Хотя бы тут, на этом острове."
    me "One hundred percent. So, if you believe at least a little, then... It will be more true than untrue. At least here, on this island."
    window hide
    show un shy pioneer  with dspr
    window show 
    #un "Наверное, если так посмотреть... Ведь нас тут только двое." 
    un "Probably, if you look at it that way... Since there are only two of us here."
    #me "Пусть это будет только нашей правдой. Ну что, сделаем вид, что ты мне веришь?"
    me "Let it be only our truth. Can we pretend that you believe?"
    #un "Хорошо, наша правда. Верю."
    un "Alright, our truth. I believe."
    #me "...Тогда посоветуй что-нибудь."
    me "...Then give me some advice."
    window hide
    show un serious pioneer  with dspr
    window show
    un "Hm."
    #"Лена на некоторое время задумалась."
    "Lena thought for a while."
    #un "В любом случае, нельзя ей такое рассказывать. Поверит она или нет - это ужасно. {w}Не поверит - это разрушит вашу дружбу, а если поверит, то будет жить в вечном страхе и сомнениях. И снова будет винить тебя."
    un "In any case, you shouldn't tell her. Whether she believes it or not - it's terrible."
    un "If she doesn't - it'll destroy your friendship. If she does - it will be a nightmare for her; it'll be a life of fear and doubt. And, after all, no friendship either."
    #me "Но оно будет стоить того, если это её спасёт?"
    me "So, no friendship... If she'll be safe, it will be worth it?"
    #un "Спасёт ли? Ты ведь даже не знаешь, от чего именно спасаешь. А если верить в судьбу, то этого не избежать."
    un "But will it save her? You don't even know what you're saving her from."
    me "Maybe I will remember some day..."
    un "If it is her destiny, then... {w}it's unavoidable."
    #me "Да, вспомнить бы, от чего... А если не верить в судьбу?"
    me "...Let's discuss more optimistic options. What if there is no destiny?"
    #un "Не знаю. Если речь о какой-то аварии, может, она и так не произойдёт. Таки вещи обычно случайны."
    un "I don't know. If we're talking about some kind of crash, maybe it won't happen, even if it did once... Such things are usually accidental."
    if d3_usfail == True:
        jump ss_d4islandcs_eng
    else:
        #me "Ты права, такое ей слышать нельзя. Ей и так уже досталось."
        me "You're right, she shouldn't hear it. She's already got enough trouble for now."
        #th "Лена молчит. То есть, она знает, как именно досталось? Или просто молчит?"
        th "Lena is silent. Does it mean that she knows what trouble I am talking about? Or it's just a usual silence, nothing more?"
        menu:
            #"Нужно узнать":
            "I need to figure it out":
                #me "Ты ведь знаешь, как именно досталось?"
                me "Do you know what happened?"
                #un "От Шурика?"
                un "Something about Shurik?"
                window hide
                show un normal pioneer  with dspr
                window show
                #me "Не только. От Ульянки, в том числе."
                me "Not only about him. Ulyana bothered her too."
                #un "Могу представить."
                un "I can imagine."
                #me "Кстати, она кое-что упоминала. Что недавно у костра ей кто-то сказал... Про её отца. Это не ты была?"
                me "By the way, Ulyana mentioned something. That, during a common bonfire chat, someone told her... About her father. Was it you?"
##
                show un angry2 pioneer  with dspr
                #un "Не знаю, о чём речь. Мы с ней вообще не общаемся. Ты меня в чем-то обвиняешь?"
                ##un "Don't know what you mean. I'm not friends with her. Are you blaming me for something?"
                un "Don't know what you mean, we never speak to each other. Are you blaming me for something?"
                menu:
                    #"Просто спросил":
                    "Just asking":
                        #me "Нет, конечно. Забудь."
                        me "Of course not. Forget it."
                        "After a short pause, Lena nodded."
                        jump ss_d4islandcs_eng
                    #"Были и другие вещи":
                    "There were other things too":
                        $ sp_un -= 1
                        if sl_ft == True:
                            #me "Вчера, когда я просил беречь её от Шурика..."
                            me "Yesterday, when I asked you to save her from Shurik..."
                            #un "Но разве это навредило? Разве я не оказалась права? Ты даже извинился."
                            un "But did it harm anyone? Wasn't I right? You even said sorry."
                            #me "Ладно, оставим эту тему."
                            me "Okay, let's drop it."
                            "Still a bit grumpy, Lena nodded."                            
                            jump ss_d4islandcs_eng
                        else:
                            #me "Ты сказала Мику, что американцы плохо обошлись с японцами. Спасибо, что Хиросиму не напомнила."
                            me "You told Miku that the Americans mistreated the Japanese. Thank you for not reminding her of Hiroshima."
                            #un "Сказала. Нам теперь нельзя обсуждать историю, чтобы не попасть под твоё расследование?"
                            un "I told her many things. Can't we discuss history without falling under your investigation?"
                            #th "Как-то она болезненно реагирует. Выходит, я всё-таки прав? Но чего я добьюсь этим разговором? Она просто уйдёт."
                            th "Her reactions are strange. I could be right, but... What will I get from this conversation? She will just leave eventually."
                            #me "Прости. Сейчас это всё уже не так важно, в свете новых проблем. Ты поможешь мне?"
                            me "I am sorry. It's not that important now, we have some bigger problems. Will you help me?"
                            show un serious pioneer  with dspr
                            #un "...Помогу."
                            un "...I will."
                            jump ss_d4islandcs_eng
                    #"Ничего страшного, даже если так":
                    "Don't worry even if this is true":
                        $ sp_un += 1
                        #me "Я вывалил на тебя ворох проблем, а ведь даже не знаю, интересно ли тебе это. Не все в лагере были рады Саманте."
                        me "I dumped a heap of problems on you, and in fact I don't even know if you're interested. Not everyone in the camp is happy to see Samantha."
                        show un normal pioneer  
                        #"Лена выразительно на меня посмотрела. Что я должен прочесть в этих глазах? \"Ты и сам всё знаешь\"?"
                        "Lena looked at me meaningfully. What am I supposed to read in those eyes? \"You already know\"?"
                        #un "Будь дело всегда только в ней - все были бы рады."
                        un "If it was always only about her, everyone would be happy."
                        #th "Если не в ней, то в ком? Во мне?"
                        th "If not about her, who then? Me?"
                        #"Чтобы проверить свою мысль, я пошёл на решительный шаг - взял Лену за руку."
                        "To test my idea, I took the plunge - I took Lena's hand."
                        window hide
                        show un smile pioneer close  with dspr
                        window show
                        #"Её улыбку можно считать утвердительным ответом."
                        "Her smile could be considered as \"yes\"."
                        #"Неужели, она ревновала меня к Саманте? Пусть так, сейчас только Лена может помочь нам. Я верю, что она хочет помочь, и не буду её корить за прошлое."
                        th "Was she jealous of Samantha?"
                        th "Even if she is, only she can help me right now. I believe that she wants to help, and I won't blame her for the past."
                        jump ss_d4islandcs_eng

            #"Это не важно":
            "It's not important":
                jump ss_d4islandcs_eng

label ss_d4islandcs_eng:

    #me "Итак. Если нельзя рассказать, то как же быть?"
    me "So. If we can't tell her, let's come up with something we can do..."
    window hide
    $ renpy.pause(1)
    window show
    if sp_un <= 3:
        jump ss_islandno_eng
    if sp_un >= 7:
        jump ss_islandyes_eng

    menu:
        #"Если бы я мог с ней поехать":
        "I wish I could go with her":
            #me "...Но это невозможно."
            me "...But that's impossible."
            jump ss_islandyes_eng

        #"Если бы она могла не уезжать":
        "I wish she could stay":
            #me "...Но так не получится."
            me "...But it won't work."
            jump ss_islandno_eng

label ss_islandyes_eng:
    stop music fadeout 5 
    "We sat under the big oak tree, thinking about our matter. It was hot, but there was a nice breeze."
    "Finally, Lena said something, as quiet as always:"
    play music Warm_evening fadein 3       
    show un serious pioneer close  with dspr  
    #un "Я поеду с ней."
    un "I'll go with her."
    if un_inv == False:
        #th "О чём это она? Может быть, Саманта её пригласила?"
        th "What does she mean? {w}Maybe Samantha invited her?"
    #me "Куда? В Америку?"
    me "Where? To the US?"
    #un "Да."
    un "Yes."
    #me "Но это опасно!"
    me "But... That is dangerous!"
    #un "Нужно ведь что-то сделать. Ты ведь знал бы, если бы и русская девочка с ней..?"
    un "We need to do something. You would know if a Russian girl was with her, when she... Right?"
    #me "Наверное. Но всё может быть. Мы не знаем, как это работает."
    me "Probably. But there's no guarantee. We don't know how it works."
    window hide
    show un normal pioneer close  with dspr  
    window show
    if un_inv == True:
        #un "Что-то уже изменится. Не волнуйся, ты же сам говорил, что мы просто поживём у неё. Никаких разъездов и торжественных приёмов. Со мной ничего не случится."
        un "But something will change. Don't worry, you said we would just live at her place. No trips, no ceremonies. I'll be fine."
    else:
        #un "Что-то уже изменится. Не волнуйся, не будет никакого риска. Никаких горных лыж и прыжков с парашютом, я прослежу. Со мной ничего не случится."
        un "But something will change. Don't worry, there won't be any risk. I'll make sure there will be no skeeing or skydiving. I'll be fine."
    #th "Значит, она правда мне поверила. Невероятно."
    th "So she really believes me. Incredible."
    #me "А что потом? Я не знаю, когда это произойдёт. Может, через полгода. Ты же не пробудешь там столько."
    me "...And what's next? I don't know when {i}it{/i} will happen. Maybe in a year! You can't stay there that long."
    window hide
    show un smile pioneer close  with dspr 
    window show
    #un "Ну... Я что-нибудь придумаю за это время. Сделаю так, что она мне пообещает, что будет сидеть дома."
    un "Well... I'll figure something out. She will promise me to stay at home."
    show un laugh pioneer close   
    #un "Ногу ей сломаю!"
    un "I'll break her leg!"
    if un_inv == True:
        #me "Но ведь ты не хотела ехать. Даже когда не знала об опасности."
        me "But you didn't want to go. Even when you hadn't known about the danger."
        show un smile pioneer close  with dspr 
        #un "Я и сейчас не хочу. Но готова."
        un "And I still don't want to. But I'm ready."
    else:
        #me "Удивлён, что ты согласилась ехать. Не похоже на тебя."
        me "I'm surprised you agreed to go. That is something... not exactly in your style, right?"
        show un smile pioneer close  with dspr 
        #un "Я и не соглашалась, да и сейчас не горю желанием. Но готова."
        un "I didn't agree, and I'm still not eager to. But I'm ready."

    #me "Это большая жертва."
    me "It's a big sacrifice."
    show un shy pioneer close  with dspr 
    #un "Просто не знаю, как ещё помочь тебе..."
    un "I just don't know how else to help you..."
    #un "...То есть, Саманте. И всем. Если это случится, то беда для всех будет. Как я себе прощу?"
    un "...I mean, Samantha. And everybody. If it happens, it will be a disaster for everyone. How will I forgive myself?"
    stop music fadeout 3
    #"Я потянулся к Лене. Она не ожидала такого порыва и немного отпрянула, но я всё-таки настиг, и заключил её в своих объятьях. Раз уж мой мирок рушится, то можно и так."
    "I reached out for Lena. She didn't expect it and slightly pulled back, but nevertheless I caught and hugged her."
    "It was really brave of me, and perhaps unadvisedly, but I didn't ask myself, what I'm doing. I knew that there is nothing to lose - if my world gets torn apart, I can do this too."
    window hide
    $ renpy.pause (0.6)
    play music music_list["sparkles"]
    show un surprise pioneer close  with dspr 
    scene black  with fade
    $ renpy.pause(1)
    $ un_usa = True
    window hide
    scene cg d5_un_sleep  with fade
    $ renpy.pause(1)
    stop music fadeout 5    
    #un "Проснулся?"
    un "Awake?"
##Too long. I'd suggest "Awake?" if that's acceptable
    #me "Да."
    me "Yes."
    #th "Как я мог заснуть? Этот остров... А что было до этого? Кажется, я переборщил с благодарностью Лене. Но вот насколько?"
    th "How could I fall asleep? This island..."
    th "...And what was before it? {w}I think I {i}overthanked{/i} Lena... But how much?"

    #un "В лагере скоро обед. Вернёмся?"
    un "The dinner will start soon. Shall we go back?"
    #me "Придётся. А то ещё искать начнут."
    me "I guess so. Or others will start looking for us."
    un "No need to bother them..."
    window hide
    $ renpy.pause(0.8)    
    scene bg ext_island_day with dissolve  
    $ renpy.pause(0.6)    
    "We got up from the ground, cleaned our shirts a bit, and went back to the boat." 
    window hide
    $ renpy.pause(0.6)
    play ambience ambience_boat_station_day    
    scene bg ext_boathouse_day  with dissolve2
    show un normal pioneer close at left   with dissolve
    $ renpy.pause(0.5)     
    #un "Слушай... Ты иди, а я следом. Скажи ей, что я поеду. Только не говори, что мы всё утро... Вместе."
    un "Listen... Go first, and I'll follow you. Tell Samantha I'll go with her."
    me "Will do!"
    un "But don't tell her about the morning... that we were here."
    #me "Хорошо. Но почему так?"
    me "Alright. But why?"
    show un smile pioneer close at left   with dspr 
    #"Лена одарила меня снисходительной улыбкой."
    "Lena gave me a gracious smile."
    #un "Она может расстроиться."
    un "She may get upset."
    #th "С чего бы ей? Ну да ладно."
    th "Why would she? Meh, never mind."
    window hide
    $ renpy.pause(0.7)
    scene black with fade    
    jump ss_d4dinner_eng

label ss_islandno_eng:
    show un normal pioneer  with dspr
    

    #"Мы долго сидели. Иногда выдавали какие-то идеи, например, позвонить родителям Саманты, но тут же отвергали их. Наконец, Лена сказала:"
    "We were sitting for a while, suggesting one solution after another, but they all were not good enough. Our last idea was to call Samantha's parents, but we denied it, just like all ideas before it."  
    #un "Я не знаю, честно говоря. Скоро обед. Не хочешь вернуться?"
    un "I don't know what else to suggest... It's near dinner-time. Maybe we should go back?"    
    #"Мне совсем не хотелось есть, как и возвращаться, но пришлось согласиться с Леной. Не держать же её тут до вечера. Один остаться я тоже не мог, ещё не хватало, чтобы за мной прислали лодку."
    "I neither wanted to eat nor to go back, but I had to agree - I couldn't hold Lena there till the evening. We went back to the boat."
    window hide
    $ renpy.pause(0.5)
    scene black  with fade
    jump ss_d4dinner_eng

label ss_d4dinner_eng:
    scene bg int_dining_hall_day  
    show ss serious fancy  at cright
    show mt normal pioneer at cleft   
    with fade
    play ambience  ambience_dining_hall_full
    #"По настоянию Лены, на обед мы явились по отдельности. Лена, пришедшая позже, прошла за свой привычный столик к Мику, даже не взглянув в нашу сторону."
    "As we agreed, Lena and I went to the dinner separately. I saw her walking to her usual table with Miku, not even looking in my direction."
    #"Саманта и вожатая были уже на месте. Обе сверлили меня взглядом, но не решались начинать допрос в компании другой."
    "Samantha and Olga both stared at me intently, but one couldn't start the interrogation in face of another."
    #me "Так хорошо сегодня выспался! Спасибо. То, что нужно."
    me "I had such a good sleep today! Just what I needed, much obliged."
    #mt "Только не бери в привычку. А где был потом?"
    mt "Don't get used to it. And where were you after that?"
    #me "Ходил на лодке."
    me "I went to the boathouse."
    show ss nosmile fancy  at cright    
    #me "...А потом сел, и поплыл на ней. На остров. Захотелось побыть одному."
    me "...And then went to the island. {w}Just to have some alone time..."
    #mt "Тоже не бери в привычку. Человек - животное социальное."
    mt "Don't get used to that too. Human is a social animal."
    #me "Не всем же быть животными."
    me "We can't all be animals."
    #mt "Ну-ну."
    mt "Ugh?"
    #"Каждый взгляд в сторону Саманты возвращал меня к тяжёлым мыслям, но нельзя же совсем её игнорировать."
    "Every look at Samantha made me return to my heavy thoughts, but I couldn't totally ignore her."
    me "{enen}How was your morning{/enen}?"
    show ss serious fancy at cright 
    ss "{enen}There was a lot of waiting... and idling... and then more waiting{/enen}."
    me "{enen}Forgive me, I shouldn't have left without a note.{/enen}"
    if un_usa == True:
        play music In_the_void fadein 5
        show ss normal fancy at cright      
        #"Я объявил о решении Лены. Саманта приняла эту новость позитивно, а вот Ольгу Дмитриевну это изрядно всполошило."
        "It was the right time to announce Lena's decision. Samantha took it positively, in contrast to Olga Dmitrievna."
        show mt surprise pioneer at cleft   with dspr
        #mt "Лена? В Америку? Почему я узнаю об этом последней? Нет, погодите, это надо ещё обсудить. Это очень важно." 
        mt "Lena? To America? Why am I the last who gets to know it? No-no, wait, firstly we must discuss it. This is very important."
        show mt sad pioneer at cleft   with dspr
        #me "Это совместное решение девочек. Не нужно вмешиваться."
        me "This is girls' joint solution. You must not interfere."
        #mt "А кто вмешивается? Но нельзя же просто вот взять и... Мы могли бы найти кого-нибудь. Узнать бы мне раньше. Что же ты не сказал?"
        mt "I am not. But you can't just... We could find someone, you know?"
        me "Stop it, please."
        mt "If only you could tell me earlier... Why didn't you?"
        #me "Это решение непросто далось Лене, поддержите её. Вы поможете ей с документами и прочим?"
        me "This wasn't Lena's easiest decision, you should support her. Could you help her with papers?"
        show mt angry pioneer at cleft   
        show ss nosmile fancy at cright
        with dspr
        #mt "А какая твоя роль во всём этом? А то я сегодня утром не могла найти Лену."
        mt "And what is your role in all this? Strange thing: I couldn't find Lena this morning."
        #"Я растерялся, но Саманта пришла мне на помощь."   
        "I got confused a bit, but Samantha came up to help."     
        ss "{enen}You don't want her to go? Why?{/enen}"
        show mt normal pioneer at cleft   with dspr
        #mt "Нет-нет. Лена - умница, но... Семён, ты закончил с едой? Можно тебя на минутку?"
        mt "No, no. Lena is a good girl, but... {w}Semyon, have you finished your meal? Can I ask you for a moment?"
        window hide
        $ renpy.pause(0.6)
        hide mt with dspr
        $ renpy.pause(0.5) 
        me "In fact I hadn't finished... {w}Okay."
        show ss normal fancy at cright        
        #"Вообще-то я не закончил, но пришлось выйти вслед за вожатой на улицу."
        window hide
        $ renpy.pause(0.7)
        play ambience  ambience_medium_crowd_outdoors
        scene bg ext_dining_hall_near_day  
        show mt normal pioneer at cleft   
        with dissolve        
        $ renpy.pause(0.7)         
        #mt "Ехать должна Славяна."
        mt "Slavyana must be the one to go."
        #me "Сменили гнев на милость?"
        me "Back to temper justice with mercy?"
        #mt "Гнев? Впрочем, ты прав. Я была к ней несправедлива. Пора это исправить."
        mt "Was I injust to her? {w}Well, maybe I was... If it is so, she deserves this reward even more, don't you think?"
        #me "Не нам это решать. Да и решено уже всё."
        me "You can't decide it. Everything's been decided already."
        #mt "Лена... Не знаю, как вы её уговорили, но ты ведь понимаешь, что такое внимание не для неё. За Славяну я буду спокойна."
        mt "Lena... I don't know how did you make her agree, but you should understand... it's not her thing. I will only be calm if it will be Slavyana."
        #me "И как вы объясните это Лене? Имейте совесть."
        me "And how will you explain it to Lena? Have some shame."
        #mt "Не забывайся. С Леной я поговорю, она ещё и рада будет."
        mt "You have some shame! And respect. {w}I'll talk to Lena, she will be even happy."
        #"Саманта уже вышла из столовой и ждала нас в сторонке."
        "Samantha had already left the dining hall and was waiting for us on the sidelines."

        stop music fadeout 3        
        #mt "Саманта! Подойди. Вы же ладите со Славей? {w}Вот и отлично."
        mt "Samantha! Come here please."
        window hide
        show ss nosmile fancy at right with dspr
        $ renpy.pause (0.5)        
        mt "You get on with Slavya, don't you?"
        me "They're barely know each other."
        mt "I'm not asking you! {w}Come on, Samantha. Do you like Slavya?"        
        ss "{enen}Uhm, yeah...{/enen}"
        show mt smile pioneer at cleft 
        mt "Good then."
        jump ss_d4afdinner1_eng
    else:
        show ss nosmile fancy at cright with dspr   
        #mt "Как ваша пьеса продвигается? Роли учите?"
        mt "How's the play going? Are you learning your parts?"
        show mt smile pioneer at cleft   with dspr
        #me "C костюмами проблема."
        me "We've got problems with costumes."
        #mt "Так давайте помогу. Что нужно сделать?"
        mt "I can help you with that. What do you need?"
        #me "Ничего."
        me "Nothing."
        #mt "Да почему? Нужно кому-то поручить..."
        mt "But why? We need someone to entrust it to..."
        show mt normal pioneer at cleft   with dspr
        #me "Вы всё время кому-то что-то поручаете, поэтому работа и стоит. Попробуйте ничего не поручать."
        me "This is what you do all the time, that's why we don't have any progress. Try to stop it for the moment."
        #mt "Вот как."
        mt "Is it so?.."
        #th "Я начинаю срываться на людей. Сейчас лучше поменьше общаться со всеми. Кроме Лены, разве что."
        th "I'm beginning to take out on people. It's better to talk less with anyone now. Except Lena probably."
        show mt smile pioneer at cleft   with dspr
        #mt "А я и сама шить умею. В детстве кукол себе шила. Так их любила. Не то, чтобы с игрушками так плохо было. Но так интересней же!"
        mt "And I myself know how to sew. As a child, I was sewing dolls for myself. I really loved them. Can't say there were problems with toys that much. But that was much more interesting!"
        show ss normal fancy at cright  with dspr
        ss "{enen}I have a lot of new toys, but I barely look at them. I love my old toys.{/enen}"
        #mt "А братья бумажных солдатиков делали. И ещё был пуговичный футбол. И много чего... Самодельные-то игрушки намного душевней этих ваших."
        mt "Sure thing! My brothers made paper soldiers back then. And there was soccer with buttons. And a lot of other things. Handmade toys are much better."
        #mt "Так что возьму процесс под личный контроль, не переживайте."
        mt "So I'll look after the process, don't worry."
        window hide         
        $ renpy.pause(1)        
        jump un_usafail1_eng

label ss_d4afdinner1_eng:
    scene bg ext_dining_hall_near_day  with dissolve
    play ambience  ambience_medium_crowd_outdoors
    play music music_list["you_lost_me"] fadein 3
   #"Наш с Леной план находился под угрозой срыва. Нужно было срочно действовать."
    "Our plan was about to fail. I had to act immediately to save it somehow."
    menu:
        #"Поговорить с Самантой":
        "Talk with Samantha":
           #"Ольга Дмитриевна вернулась в столовую, а остался с Самантой."
            "Olga Dmitrievna returned to the canteen, while I stayed with Samantha."
            show ss normal fancy with dspr          
           #me "{en}I hope you won't change your mind.{/en}{ru}Надеюсь, ты не передумала.{/ru}"
            me "{enen}I hope you didn't change your mind.{/enen}"
            show ss unsure fancy with dspr         
           #ss "{en}No but if Lena reconsider it could go miss Olga's way. I'm OK with Slavya, are you not?{/en}{ru}Нет, но если Лена передумает, то всё может пойти по сценарию Ольги. Ты ведь не против Слави?{/ru}"
            ss "{enen}I won't, but if Lena changes her mind, it may go Miss Olga's way. I'm OK with Slavya, what about you?{/enen}"
            menu:
               #"Для меня это не так важно":
                "It's not that important for me":
                   #me "{en}I just want to help Lena. It will be a good experience for such a shy girl. And it will be unfair to turn her down now.{/en}{ru}Просто хочу помочь Лене. Для неё это будет хороший опыт. И потом, это просто нечестно - отказывать ей, когда она уже согласилась.{/ru}"
                    me "{enen}I just want to help Lena. It will be a good experience for her; plus, it wouldn't be right to turn her down now.{/enen}"
                   #ss "{en}I agree. But it's not up to us only.{/en}{ru}Согласна. Но не всё зависит от нас.{/ru}"
                    ss "{enen}That's true. But we're not the only ones to decide.{/enen}"
                    if sp_ss >= 8:
                        $ un_usawin = True
               #"Это должна быть Лена":
                "It should be Lena":
                    me "{enen}It can't be Slavya or any other girl. Lena {b}has to{/b} go with you. It's very important.{/enen}"
                    ss "{enen}And now I am intrigued.{/enen}"
                    me "{enen}I can't really explain right now... Can you trust me on this?{/enen}"
                   #me "{en}I can't really tell why it has to be Lena. Can you trust me on this? It's very important.{/en}{ru}Не могу объяснить, но это должна быть Лена. Это очень важно. Просто поверь мне.{/ru}"
                    show ss serious fancy with dspr  
                   #ss "{en}Now I am intrigued. But I'm already invited Lena, what do I have to do?{/en}{ru}Я заинтригована. Но если я уже пригласила Лену, что ещё можно сделать?{/ru}"
                    ss "{enen}I sure can... But I already invited Lena. What else can I do now?{/enen}"
                   #me "{en}Let's wait and see.{/en}{ru}Давай подождём и посмотрим.{/ru}"
                    me "{enen}Nothing, I guess. We'll just have to wait and see.{/enen}"
                    if sp_ss >= 6:
                        $ un_usawin = True
                #"У меня другие планы на Славю" if (sp_sl >= 6):
                "I have something else for Slavya" if (sp_sl >= 6):
                   #me "{en}Actually I don't want Slavya to go. I'd like to keep her around. You are a bright girl, I am sure you can imagine why.{/en}{ru}Вообще-то, я не хочу отпускать Славю. Пусть она останется рядом. Уверен, что ты поймёшь, ты же умница.{/ru}"
                    me "{enen}I actually don't want Slavya to leave. I'd like her to stay here. I'm sure you understand why, you're smart.{/enen}"
                    show ss surprise fancy with dspr
                    $ sp_ss -= 1
                   #th "Иногда я сам себе поражаюсь."
                    th "Sometimes I surprise even myself."
                   #ss "{en}Wow. I didn't expected that.{/en}{ru}Ух ты. Такого я не ожидала.{/ru}"
                    ss "{enen}Wow. I wasn't expecting that...{/enen}"
                    if sp_spts == 1:
                        show ss angry fancy                    
                       #ss "{en}You told me yesterday that you like Lena.{/en}{ru}Но ты мне вчера говорил, что тебе нравится Лена.{/ru}"
                        ss "{enen}Yesterday you told me that you liked Lena.{/enen}"
                       #me "Ну...{en} I like her as a friend.{/en}{ru}Нравится. Как подруга.{/ru}"
                        me "{enen}Well... I meant that I like her as a friend.{/enen}"
                        $ sp_ss -= 1
                    if sp_spts == 2:
                        show ss nosmile fancy                  
                       #me "{en}But I told you yesterday, remember? When you asked about Slavya.{/en}{ru}Но ты ведь вчера спрашивала про Славю, помнишь?{/ru}"
                        me "{enen}You must remember what I told yesterday, when you asked about Slavya.{/enen}"
                       #ss "{en}Yes, I remember. You are the most honest person.{/en}{ru}Помню. Всё честно.{/ru}"
                        ss "{enen}Yes, I remember. You are the most honest person.{/enen}"
                        $ sp_ss += 1
                    if sp_spts == 3:
                        show ss angry fancy                    
                       #ss "{en}Why you lied to me yesterday? You said it's not Slavya.{/en}{ru}Значит, ты соврал вчера? Ты сказал, что тебе нравится не Славя.{/ru}"
                        ss "{enen}Does that mean you were lying to me yesterday? You told me that you don't like Slavya.{/enen}"
                        $ sp_ss -= 1
                       #me "{en}I'm sorry. I wasn't sure.{/en}{ru}Прости. Я сам немного запутался.{/ru}"
                        me "{enen}I'm sorry. I wasn't sure.{/enen}"
                    $ sp_spts = 4
                    show ss sad fancy                  
                   #"Саманта заметно погрустнела. Пожалуй, это не самая удачная моя интрига, но давать задний ход уже поздно."
                    "Samantha wasn't as cheerful as before after my last words. I'd failed at telling a good lie, but it was too late to stop."
                   #ss "{en}You think she might forget you after the trip?{/en}{ru}Думаешь, она может забыть тебя после поездки?{/ru}"
                    ss "{enen}You think she might forget you after the trip?{/enen}"
                   #me "{en}There will be new faces, new impressions. Who knows?{/en}{ru}Там будут новые лица, новые впечатления. Кто знает?{/ru}"
                    me "{enen}There will be new faces, new impressions. Who knows?{/enen}"
                    if sp_ss >= 4:
                        $ un_usawin = True
                   #ss "{en}I'll do what I can.{/en}{ru}Сделаю, что смогу.{/ru}"
                    ss "{enen}I'll do what I can.{/enen}"
            show ss nosmile fancy                  
           #"Некоторое время мы просто ждали, пока вожатая не вышла к нам."
            "We waited for Olga for some time, not breaking the silence anymore."
            window hide
            $ renpy.pause(0.7)            
            show mt normal pioneer at fleft with dissolve
            $ renpy.pause(0.6)            
            window show         
           #mt "Я поговорила с девочками. Всё улажено. Славя будет рада поехать, а Лена... Тоже в порядке. Славя более рада, чем Лена расстроена, так скажем."
            mt "I talked to the girls, everything is settled. Slavya is glad to go, and Lena... well, she's fine. {w}She's not that upset, while Slavya is really happy."
            show ss serious fancy  
           #ss "{en}I really think it has to be Lena{/en}{ru}Мне бы хотелось, чтобы это была Лена{/ru}."
            ss "{enen}I would rather like Lena to go with me.{/enen}"
           #mt "Но она не хочет. Она согласилась только ради вас. Или проблема со Славей?"
            mt "Why force her to go, if she doesn't want it? {w}Is there a problem with Slavya going?"
            if un_usawin == True:
                show mt sad pioneer at fleft   
                show ss angry fancy 
                with dspr               
               #ss "{en}I might invite someone but I don't have to. It's optional. Let's put it that way: it's either Lena or nobody.{/en}{ru}Я могу пригласить кого-то, но это вовсе не обязательно. На моё усмотрение. Придётся поставить вопрос так: либо Лена, либо никто.{/ru}"
                ss "{enen}I could bring someone else along with me, but it's not necessary. I could return alone{/enen}."
                mt "Of course, but think about it..."
                ss "{enen}I did. And I decided: either it's Lena who goes, or nobody.{/enen}"
               #"Ольга Дмитриевна грозно на меня посмотрела, будто я несу отсебятину. Но решительный тон Саманты не оставлял вопросов."
                "Olga looked at me angrily, like I was putting my words there. But there could be no real doubt - she saw how resolute Samantha was."
               #mt "Хорошо, я всё поняла. Лена, значит. Или никто. Я тоже ничего не гарантирую."
                mt "Very well, I understand. So it will be Lena. Or nobody. I can't promise anything too."
                show ss serious fancy with dspr 
               #me "О чём вы?"
                me "What do you mean?"
               #mt "Не каждого могут вот так отпустить в США."
                mt "Not everyone can travel to the USA just like that."
               #me "Значит, письмо напишем..."
                me "We'll wright a letter if we have to..."
               #th "Горбачёву, или кому там? Лучше не рисковать."
                th "...To who? Gorbachev - or who is it, now? Better not to take the chance with guessing."
               #ss "{en}We'll figure something out.{/en}{ru}Мы что-нибудь придумаем.{/ru}"
                ss "{enen}We'll figure something out.{/enen}"
                stop music fadeout 4
               #mt "Как знаете."
                mt "Suit yourselves."
               #"Ольга Дмитриевна оставила нас одних."
                "Olga Dmitrievna left us."

                jump un_usawin1_eng

            show ss surprise fancy with dspr    
           #ss "{en}No, but{/en}{ru}Нет, но{/ru}..."
            ss "{enen}No, but...{/enen}"
           #mt "Тогда решили?"
            mt "So that's it?"
            window hide
            show mt smile pioneer at fleft   
            show ss sad fancy 
            with dspr  
            window show         
           #ss "{en}I guess so.{/en}{ru}Похоже на то...{/ru}"
            ss "{enen}I guess so...{/enen}"
           #mt "Вот и отлично. Это только к лучшему. Потом ещё обсудим детали."
            mt "Great then. It's all for the best. We'll go over the details later."
            hide mt  with dspr 
            #"Ольга Дмитриевна оставила нас одних."
            "Olga Dmitrievna left us."

            jump un_usafail1_eng

       #"Поговорить с Леной":
        "Talk to Lena":
            scene bg int_dining_hall_day  with dissolve
            play ambience  ambience_dining_hall_empty
           #"Я поспешил вернуться в столовую раньше вожатой. Лена уже закончила трапезу, и теперь ждала Мику."
            "I rushed to get to the canteen before Olga. Lena was there; she'd already finished her meal and was waiting for Miku."
            window hide
            show un normal pioneer at cleft   
            show mi normal pioneer at cright   
            with dspr
            window show
            #mi "Привет, Семён! Мы обсуждали, почему голубцы не имеют отношения к голубям. Или имеют?"
                    #tr Не уверен насчет этого, может сойдет. Вариант перевода с обсуждением названия еды.
            mi "Hi, Semyon! We were just talking about pineapples! Do you know why they call pineapples pineapples? Isn't it weird?"
           #me "Эээ... Мне бы с Леной поговорить."
            me "Umm... I just want to talk with Lena."
           #un "Выйдем тогда?"
            un "Maybe we can go outside, then?"
           #mi "А сырники - к сыру! И к сырости. И едят их не сырыми."
                    #tr как вариант.
            mi "Pineapples don't even taste like apples!"
           #me "Там Ольга Дмитриевна снаружи."
            me "Olga Dmitrievna's outside, so..."
           #mi "Что вы на меня так смотрите? Да ладно вам, меня тут нет!"
            mi "What? Don't look at me like that! Look, just pretend I'm not here."
            show mi happy pioneer at cright   with dspr
           #"Мику заткнула руками уши и стала покачивать головой, будто слушает музыку."
            "Miku stuffed her fingers into her ears and started shaking her head, as though she was listening to music."
            show un serious pioneer at cleft   with dspr
           #me "Кхм."
            me "Ahem."
            show mi grin pioneer at cright   with dspr
           #mi "Ладно, секретничайте. Я пошла. Лен, потом расскажешь всё!"
            mi "Fine, start gossiping. I should go. Tell me everything later, Lena!"
            hide mi  with dspr
           #me "Короче, она хочет послать Славю вместо тебя."
            me "In short, she wants to send Slavya instead of you."
           #un "А Славя что?"
            un "And what did Slavya say?"
            show un normal pioneer at cleft   with dspr            
           #me "Не знаю пока. Но не против, наверное. А ты не соглашайся, ладно? Знаю, что многого прошу."
            me "She probably doesn't mind. But anyway, dont agree to go, okay? I know that I'm asking much, but still..."
           #un "Я не могу совсем уж настаивать."
            un "I couldn't really insist."
           #me "Если мы это допустим, то в опасности будет не только Саманта, но и Славя."
            me "If we let it happen, Slavya will be in danger too."
           #un "А если и Славе рассказать?"
            un "We could warn Slavya, what if we tell her what we know?"
            menu:
               #"Она может не поверить":
                "She might not believe us":
                   #me "Вряд ли она поверит. И это будет выглядеть так, будто мы её запугиваем, чтобы она отказалась от поездки."
                    me "It's unikely that she'll believe it... Plus it will look like we're trying to scare Slavya into refusing to go."
               #"Только ты могла поверить":
                "Only you can believe in that":
                    show un shy pioneer at cleft   with dspr
                   #me "Такое я мог рассказать только тебе."
                    me "It's the kind of thing that I can trust to you only."
                    $ sp_un += 1

            if sp_un >= 7:
                $ un_usawin = True
           #un "Ну посмотрим."
            un "We'll see."
            show un serious pioneer at cleft   with dspr
           #"Лена кивнула в сторону двери. Оказалась, это вернулась вожатая."
            "Lena motioned to the door - Olga had arrived."
            show mt normal pioneer at cright   with dspr
           #mt "Лена, ты уже в курсе всей ситуации. Семён сказал, что ты согласна ехать. Ребята тебя уговорили, да?"
            mt "Lena, you know our situation here... Semyon said that you agreed to go."
           #un "Да."
            un "Yes."
           #mt "Я поговорила со Славяной, и она очень хочет поехать. И Саманта не против. Как думаешь?"
            mt "I spoke with Slavya and she really wants to go, and Samantha has nothing against this. What do you think?"
           #un "Вообще-то я правда хотела бы поехать."
            un "Honestly, I want to go too."
            show mt sad pioneer at cright   with dspr
           #mt "Хм. Я не хотела поднимать эту тему, но может не получиться. У тебя были проблемы, ты понимаешь..."
            mt "Hm. I'm sorry, but I don't think that this will work out. I didn't want to bring this up, but you had some problems before, you understand..."
           #th "О чём это она?"
            th "What is she talking about?"
           #mt "В свою очередь, Славяна..."
            mt "Slavya has better chances to get a permit."
            if un_usawin == True:
                show un angry2 pioneer at cleft   with dspr
               #un "Саманта сказала, что бюрократических проблем не будет. Я обещала, и от поездки не откажусь."
                un "Samantha said that there will be no problem with papers or anything else. I promised to go, and I won't give my word up."
                show mt shocked pioneer at cright   with dspr
               #"Вожатая оцепенела от такого неожиданного отпора. Да и я такой решительной Лены ещё не видел."
                "Olga was truly suprised by such a resistance. I hardly expected this too, since I never seen Lena nearly determine and resolute as now."
               #mt "Вы, ребята, имеете влияние на Саманту, но нельзя этим вот так пользоваться, подумайте о других."
                mt "You guys have an influence on Samantha, but you can't just take advantage like that. There are others to think about!"
               #"Настал мой черёд проявить решительность."
                "It was my turn to take the wheel."
               #me "А вы подумали о Лене, прося отказаться от такого приключения? Такой шанс раз в жизни может быть."
                me "And you? Did you even bother to think about Lena's feelings before denying her an adventure like this?"
                window hide
                show mt sad pioneer at cright  
                show un shy pioneer at cleft
                with dspr
                window show
               #mt "Не делай из меня злодейку. Это вы можете поддаваться эмоциям, а мне лежит ответственность. Будь по-вашему. Свои функции я выполнила."
                mt "Don't make me into a villain. I can't allow myself to be driven by emotions. It's all about my duties here, my responsibility, while you have none."
                me "We are responsible to others in exact same way. And we both have no right to demand anything, it's all up to Lena and Samantha."
                mt "Well, I demand nothing. I said what I had to say. If you don't need this advice - so be it." 
                hide mt  with dspr
               #"Ольга Дмитриевна покинула стол и здание."
                "Olga Dmitrievna left the table and the building."
                stop music fadeout 5
               #me "Спасибо."
                me "Looks like we won?.. Thank you!"
               #un "И тебе."
                un "You too."
                #"Я спохватился, что Саманта может ждать меня на улице, кивнул Лене и вышел из столовой."
                me "So... Samantha must be waiting for me outside..."
                un "You better go then."               
                me "Yep... Thanks again. See you later."
                jump un_usawin1_eng

            else:
                show un sad pioneer at cleft   with dspr
               #un "Хорошо, я поняла. Пусть она едет."
                un "I understand that. She can go instead of me."
               #mt "Ладно. Извини, что так вышло. Я была бы рада вас всех туда оправить. Да и сама бы съездила. Но обстоятельства пока сильнее нас."
                mt "Good then. I'm sorry that it had to be this way. I wish I could send all of you, and join you on that trip too... But it's impossible now, you understand."
               #"Ольга Дмитриевна похлопала Лену по лежащей на столе ладони, и встала из-за стола."
                "Olga Dmitrievna gently patted her on the back and left the room."
               #un "Прости."
                un "Sorry."
               #me "О чём она говорила?"
                me "What problems was she talking about?"
               #un "Должно быть, моё школьное досье, или что они там ведут, не так идеально, как у Слави."
                un "It must be about my school records; they're not perfect, like Slavya's."
               #me "Это подло!"
                me "What a dirty trick!"
               #un "Не вини её, не она эти правила устанавливает."
                un "Don't blame her, she's not the one who makes the rules."

               #me "Будем думать дальше. Меня Саманта ждёт, наверное. Я пойду, увидимся."
                me "We'll figure something out. But not now, I think Samantha is waiting for me..."
                un "You should go then."
                me "Yes, I should... See you later."
                jump un_usafail1_eng


       #"Поговорить со Славей":
        "Talk to Slavya":
            scene bg int_dining_hall_day  with dissolve
            play ambience ambience_dining_hall_empty
            show sl normal pioneer far at center   with dspr
            show sl normal pioneer at center   with dspr
           #"Я вернулся в столовую и поймал Славю почти у дверей."
            "I returned to the canteen and caught Slavya near the door."
           #me "Привет. Слушай, тут такое дело..."
            me "Hey... Can I have you for a second?"
           #sl "Да?"
            sl "Sure."
           #me "Саманта обещала Лене, что пригласит её в Штаты. А теперь Ольга Дмитриевна узнала, и хочет послать туда тебя."
            me "Here is the deal... Samantha invited Lena to go with her to the US. Just a small trip."
            sl "Oh... Great."
            me "Yes, it was great until Olga Dmitrievna found out, and now she wants to send you instead."
            show sl surprise pioneer at center   with dspr
           #sl "Меня? Как странно. Ну, если надо - я готова. Интересно, конечно, чего греха таить."
            sl "Me? How strange... {w}Well, sure - I'm ready to step up, if I needed."
           #me "Но это будет несправедливо к Лене, не считаешь?"
            me "It may seem promising, but... It would be unfair to Lena, don't you think?"
            show sl sad pioneer at center with dspr
           #sl "А. Ну да, наверное."
            sl "Oh. Maybe, I suppose..."
            menu:
               #"Откажись":
                "Refuse the offer":
                   #me "Откажись, а? Не то, чтобы я против твоей поездки. Просто вожатая суётся не в своё дело. Девочки подружились и договорились, а тут она."
                    me "Can't you refuse? I'm not against you going, but the girls became friends and already made an agreement. Olga just wants to do things her way."
                    $ sp_sl -= 1
                    if sp_sl >= 4:
                       #sl "Честно говоря, мне хотелось бы поехать. Но ты прав, я не должна пользоваться ситуацией в ущерб кому-то. Это не по-пионерски."
                        sl "Honestly, I'd love to go, but you're right about this. I can't go in Lena's place. A true pioneer doesn't act like that."
                       #me "Спасибо!"
                        me "Thanks a lot!"
                        $ un_usawin = True
                    else:
                        show sl angry pioneer at center   with dspr
                       #sl "Я не напрашиваюсь, но поехать мне хочется. Пусть другие говорят за себя, а я врать не буду. Извини."
                        sl "I never asked for such a favor, but I can't say that I don't want to go. That would be a lie."
                        me "Don't lie, just think about others a bit, as you always do."
                        sl "Others... Let the others speak for themselves, I will speak for myself. Excuse me."


               #"Решать тебе":
                "It's up to you":
                   #me "Ну, я просто ввёл тебя в курс дела."
                    me "Well, I just wanted you to know, that's all."
                   #sl "Спасибо."
                    sl "Thank you for telling me."
                    if sp_sl >= 6:
                        $ un_usawin = True

            "Olga passed in front of us, walking toward Lena."
            show sl normal pioneer at center            
            sl "...Care to help us with dishes?"
            me "Uhm... Yes, sure."
            sl "Thanks. Collect all the glasses then. Put them on the trays."           
            hide sl  with dspr            
            "I nodded. There was no joy in the job, but I needed a reason to stay in canteen to see what would happen next."                       
           #"Через несколько минут Лена была уже свободна и прошла к выходу, виновато поглядев на меня. Всё ясно, теперь надежда только на Славю. Вожатая подошла к ней."
            "After some time Lena looked at me sadly and headed for the exit. That meant it was all up to Slavya now. Olga Dmitrievna came up to her."
            show mt normal pioneer at cright   with dspr
            show sl normal pioneer at cleft   with dspr
           #mt "Славяна, ты, наверное, уже в курсе?"
            mt "Slavya, I believe you've already heard about the deal?"
            if un_usawin == True:
               #sl "В курсе чего?"
                sl "What deal?"
               #mt "Есть возможность поехать в Америку. Семён тебе не рассказал?"
                mt "There is an opportunity to travel to the USA. Semyon already told you about it, didn't he?"
               #sl "Рассказал."
                sl "He did."
               #mt "Ну и как? Поедешь?"
                mt "So what do you say? Would you like to go?"
               #sl "А как же Лена?"
                sl "Does it matter? What about Lena?"
               #mt "Она... Не очень хочет."
                mt "Well... I don't think she really wants to go."
               #sl "Не очень хочет? Так хочет или нет?"
                sl "Is that what you think? Or is that what she thinks?"
               #mt "Она сама знает, что это не лучший вариант для всех."
                mt "Lena understands she is not the best option for this."
               #sl "Лена считает себя плохим вариантом? И я тоже вариант теперь? А хотелось бы остаться человеком."
                sl "Lena thinks of herself as a bad option? Am I an option too now? I would prefer to remain a person instead."
                show sl smile2 pioneer at cleft   with dspr
               #"Славя мне подмигнула."
                "Slavya gave me a wink."
               #mt "Не придирайся к словам. Хочешь читать мне мораль - становись в очередь. Ты слушаешь? Поставь ты этот чайник."
                mt "Don't pick on the words! You want to lecture me here? There are many who want, get in line then. {w}And put that kettle down already!"
               #sl "Я слушаю."
                sl "I am listening."
               #mt "Если я тебя чем-то обидела, считай это извинением. Это большой шанс. Ты старалась всё это время, и вот награда. Ты поедешь или нет?"
                mt "If I mistreated you before, consider this as an excuse. You earn a reward for all your hard work. It's a big reward and a rare chance. So are you going, or not?"
                show sl angry pioneer at cleft   
                show mt surprise pioneer at cright   
                with dspr

               #sl "Я НЕ поеду. И никто не поедет, кроме Лены. Ни один хороший пионер тут. Если я что-то понимаю в пионерах. Никто не отнимет это у Лены."
                sl "I am NOT. I won't go. {w}And nobody will, except for Lena."
                mt "Why?.."
                sl "If I know something about pioneership - a true pioneer won't go for this. Nobody will take it away from Lena."
                show mt smile pioneer at cright   with dspr
               #mt "Ну что же... Нет, отличные слова. Вот поэтому я и хотела, чтобы ты поехала. Молодец."
                mt "What can I say... {w}Well said, Slavya. That's why I wanted you to go. Good girl."
               #"Ольга Дмитриевна смахнула слезу, или мне показалось?"
                th "Did Olga blink away a tear, or am I just seeing things?"
                stop music fadeout 5
               #mt "Пойду, скажу Лене."
                mt "I should go inform Lena..."
               #th "А меня там Саманта ждёт, наверное."
                th "Right... And I think Samantha is waiting for me."
                jump un_usawin1_eng


            else:
               #sl "В курсе."
                sl "I heard about it."
               #mt "Поедешь?"
                mt "And do you want to go?"
               #sl "...Поеду."
                sl "...I do."
                show mt smile pioneer at cright   with dspr
               #mt "Вот и сл...отлично."
                mt "Alright then, it's settled."
                stop music fadeout 3
               #th "И всё? Вот так просто? Они все меня предали. Все, кроме Саманты. Нужно вернуться к ней."
                th "That's it? Just like that?"
                window hide
                $ renpy.pause(0.5)
                hide mt with dspr 
                $ renpy.pause(0.3)                
                hide sl with dspr
                $ renpy.pause(0.4)                
                th "They betrayed me! Everyone did, except for Samantha. {w}I should go talk to her."
                jump un_usafail1_eng


label un_usawin1_eng:
    window hide
    stop music fadeout 2
    $ renpy.pause(0.7)    
    scene bg ext_dining_hall_near_day  with dissolve
    $ renpy.pause(0.5)    
    play ambience ambience_medium_crowd_outdoors
    play music music_list["my_daily_life"] fadein 4
    
    #"К добру или нет, но похоже, мы всё-таки отправим Лену в Америку. А сейчас нужно вернуться к делам насущным. Мы пошли к сцене."
    #"I do not know whether this is good or bad, but apparently Lena will go to America. As for now, we should get back to our duties. We went to the scene."
    "Nobody knows, would it lead to good or not, but apparently Lena will go to America. Or at least we did everything we could to make it happen."
    window hide
    show ss normal fancy with dissolve   
    "In the meantime we had to get back to our duties. We went to the stage."
    window hide
    $ renpy.pause(0.3)
    scene bg ext_stage_normal_day
    show mz normal glasses pioneer at cleft
    with dissolve
    play ambience ambience_day_countryside_ambience
    $ renpy.pause(0.4) 
    show ss normal fancy at cright with dspr
    $ renpy.pause(0.4)     
    window show
    #mz "Где вас носит? Впрочем, репетиции не будет, все заняты. И костюмы тоже никто не шьёт."
    mz "Where have you been? {w}Anyway, the rehearsal is cancelled because everyone is busy. And there's nobody to make costumes for us."
    #me "А почему?"
    me "Why so?"
    #mz "Ольга Дмитриевна объявила мобилизацию на ремонт. Весь день что-то таскают, сейчас вот клубы красят. Даже здесь краской пахнет."
    mz "Olga Dmitrievna announced mobilization for repairs. They're dragging stuff here and there all day... Gettin' tired of this!"
    me "Are they disturbing your rest?"
    mz "Yes they are! For now they're painting the clubhouses. Ew! I can smell the paint even from here."
    me "I like the smell of acetone."
    mz "Go on paint then. Faster they finish - sooner we start."    
    #me "А ты чем тут занята? Могла бы тоже помочь."
    me "And what are you doing here? You could help them too."
    mz "Me? Why me?"   
    show mz angry glasses pioneer  with dspr
    mz "You are slacking too, so don't you dare to reproach me of something!"    

    ss "{enen}We want to help. Right, Sam?{/enen}"
    me "{enen}Not really. I'm just teasing her.{/enen}"
    show ss smile fancy at cright   
    ss "{enen}Come on, guys. Let's go.{/enen}"   
    window hide
    $ renpy.pause(0.3)
    hide ss with dspr
    window show
    th "I should talk less... It seems I just signed us for some construction works."
    mz "Where is she going?"
    me "Take a guess. You are invited too."
    stop music fadeout 4
    mz "Damn you..."
    window hide
    $ renpy.pause(0.7) 
    $ persistent.sprite_time = 'day'    
    scene bg ext_musclub_day  with dissolve
    $ renpy.pause(0.3) 
    "We came to the music club. However, we didn't see anyone who could give us instructions."
    window hide
    $ renpy.pause(0.3) 
    scene bg paintedmclub  with dissolve

    play music music_list["i_want_to_play"] fadein 2
    #"Внутри носилась перемазанная краской Ульянка со шваброй в руках и горланила какую-то песню. На конце швабры была тряпка, которую она макала прямо в банку с краской."
    #"Ulyana with a mop in her hands was inside. There was a rag on the end of the mop that she dipped into the paint can. She was singing something, and her face was smeared with paint."
    "There was Ulyana inside, running around the club, belting some song."
    "She had a mop in her hands, a rag on the end of it was fully covered with paint, as Ulyana was dipping the mop right into the paint can. White splatters of paint were everywhere." 
    #"Ulyana, covered with paint and [with] a mop in her hands was rushing about inside, belting some song. There was a rag on the end of the mop, and she was dipping it right into the paint can."
    #"Последствия для комнаты были плачевны: пол был заляпан до такой степени, что белый цвет уже преобладал. Но Ульянка была будто в трансе. Краски нанюхалась?"
    #"The consequences were deplorable for the room: floor was stained to the extent of predominance of white color. Buy Ulyana was in kind of trance. Did she get intoxicated with the smell of paint?"
    "As the results, the floor was stained to the extent that white color prevailed. However, it seemed Ulyana was in some kind of trance."
    #us "Красить! Стены! Красить! Быстро!"
    #us "Paint! The walls! Paint! In Hurry!"
    us "Paint! Walls! Paint! Thickly!"
    #us "Будет! Всё! Бело! И чисто!"
    #us "It! All! Will! Be! Blurry!"
    us "Be! All! White! Quickly!"
    #us "Олька! Красить! Нам! Сказала!"
    th "Poor girl is intoxicated by the paint vapors. It must be it."
    us "Olga! Told us! \"Get to work\"!" 
    #us "А Ульянка оправдала! ...Ой!"
    us "\"Or I'll poke you with a fork!\" ...Oh!"
    show mz normal pioneer at cright 
    show us surp2 pioneer at left
    with dspr
    #"Заметив нас, Ульянка резко развернулась, вместе с конструкцией в её руках. Тряпка не была готова к такому повороту и, разомкнув свои объятия со шваброй, полетела в нашу сторону."
    "The moment Ulyana noticed us, she whirled around quickly - way too fast for the stuff in her hands. The rag said goodbye to the mop and flew toward us."
    play sound sfx_drop_alisa_bag
    hide mz
    show mzptd at cright   with dspr
    #"Я инстинктивно пригнулся, а вот Женя не успела, и приняла основной удар. Саманте тоже досталась изрядная доля брызг. Плохой из меня телохранитель."
    #"I instinctively crouched, but Zhenya did not and took the brunt. Samantha got a fair share of the paint too. I am a bad bodyguard." 
    #"I instinctively crouched, but Zhenya did not and took the brunt. The paint got Samantha as well. I'm obviously not the best bodyguard."
    "I instinctively crouched, but Zhenya wasn't so quick and the rag got her. Some paint hit Samantha as well, which was a complete failure for me as a bodyguard." 
    
    #"Женя замерла. Сейчас верхняя её часть напоминала гипсовый бюст." 
    "Zhenya froze for a second. The top part of her body was like a plaster bust."
    #us "Насилие - это ведь не наш метод?"
    #us "Violance is not our method?"
    us "Violence is not our way, is it?"
    hide us  with dspr  
    #"Ульянка спряталась от возмездия за меня. Я же не горел желанием оказаться в центре драки - мою одежду ещё можно было спасти."
    #"Ulyana hid behind me. I didn't want to be in the center of the fight - I could still save my clothes."
    "Ulyana hid from Zhenya's revenge behind me. I didn't want to be in the middle of the fight - my clothes were still clean."
    hide mzptd with dspr 
    #"Женя попыталась прорваться по правую сторону от меня, Ульянка же преградила ей путь, пихнув мне в руки швабру. Таким образом, я сработал чем-то вроде турникета." with hpunch
    "Zhenya tried to circumvent on the right hand side, but Ulyana blocked the way by thrusting the mop into my hands. I became some kind of a turnstile for a moment." with hpunch
    #"Ульянка выскочила наружу, Женя - за ней, не жалея самых страшных проклятий. Саманта смеялась."
    "Ulyana popped out of the building, and Zhenya went after her, sparing no insults. Samantha was laughing."
    show ss laugh fancy with dspr 
    #"Я оценил ущерб. Вердикт был таков: одежду - в стирку, девочку - в помывку. Я сдал Саманту вожатой, которая прибежала на шум, а сам вернулся в домик."
    #"I assessed the damage. The verdict was: We need to wash clothes, and girls. Counselor to Samantha, and I got back in my cabin."  Passing Samantha to the camp leader, I went back to the cabin."
    "I examined the damage. It was clear that both clothes and the girl were in need of washing and cleaning."
 
    window hide
    $ renpy.pause(0.7) 
    scene bg ext_musclub_day  with dissolve 
    show ss smile fancy at right
    $ renpy.pause(0.6)
    "Someone came up to us on the porch of the music club. It was Olga Dmitrievna, who looked pretty frightened." 
    show mt surprise panama pioneer at cleft with dspr
    mt "What's all the noise? {w=.4}And the screams? {w}And what happened to Samantha?!"
    show mt angry panama pioneer at cleft with dspr
    mt "What a mess! I've had enough from these kids today!" 
    th "You better stay away from the club then."
    show mt sad panama pioneer at cleft with dspr   
    mt "Alright, girl, we're gonna clean you now... Gosh, we don't have another blue tie!"
    stop music fadeout 5     
    window hide
    $ renpy.pause(0.5)     
    hide mt with dspr
    $ renpy.pause(0.4)     
    hide ss with dspr    
    $ renpy.pause(0.4)
    window show 
    "Leaving Olga to take care of Samantha, I headed to my cabin."
    window hide
    $ renpy.pause(0.5)    
    scene bg int_house_of_mt_day  with dissolve
    play ambience ambience_int_cabin_day
    $ renpy.pause(0.5) 
    window show 
    #"Появилось свободное время, и у меня была идея, чем его занять."
    "I finally got some free time, and I had some ideas how to spend it."
    play music Hide_and_seek fadein 5   
    #"Я подумал, что если Лена решит рассказать Саманте наш секрет, но не сможет объясниться, либо ей понадобится подтверждение, то на такой случай будет неплохо снабдить её письмом."
    #"I thought, if Lena will tell Samantha our secret, but couldn't explain, or if she will need proofs, I should write a letter."
    "I began writing a letter for Samantha. A letter, which Lena should keep for the case if she decides to explain everything. Then it should help her a lot." 
    #"I thought that if Lena would tell Samantha our secret, but wouldn't be able to explain, or she would need some proof, she'd better have a letter from me with her."
    #"Письмо мне давалось с трудом. Прошёл почти час, прежде чем меня прервали шаги на крыльце, а оно так и не было закончено. Я спрятал листок под подушкой."
    "I had a hard time writing it. After an hour, I was interrupted by footsteps on the porch. I've put the unfinished letter under the pillow."
    show mt normal pioneer  with dspr
    #mt "Зря я эту покраску затеяла. Теперь весь лагерь мыть-стирать нужно. А видел, что в музыкальном клубе? Вот что теперь делать, хоть всё в белый перекрашивай. Эй, да ты же постель заляпал!"
    #mt "I shoudn't initiate it. Now everybody in this camp need to get a shower. And have you seen the music club? I don't know what to do now, if not to paint everything in white. Even you soiled your bed!"
    mt "I shouldn't have started this anarchy. There is not a single clean spot in the whole camp now!"
    me "Right. Don't start... things. That's my motto."
    mt "...And have you seen the music club? What should we do with that? To repaint everything in white? {w}Hey, you stained your bed!"
    #me "Ой... Не видел, что на меня краской попало."
    me "Oh... I didn't notice that I got paint on me."
    #mt "Растяпа! Ладно, иди, тебя там подруга ждёт."
    #mt "Wally! Just go, your friend is waiting you outside."
    mt "How clumsy you are. All right, go now. Your friend is waiting outside."
    window hide
    $ renpy.pause(0.3)
    scene bg ext_house_of_mt_day  with dissolve
    play ambience ambience_day_countryside_ambience
    #"Подруга действительно ждала на скамейке. Однако, я даже не успел подойти к ней, как меня окрикнула Ольга Дмитриевна."
    #"My friend did wait me on the bench. However, I didn't approached her, because Olga Dmitrievna called out to me."
    "My friend indeed awaited me on the bench. However, I hadn't even approached her before Olga Dmitrievna called out to me."
    window hide 
    $ renpy.pause(0.3) 
    play sound sfx_close_door_1 
    scene bg int_house_of_mt_day  with dissolve
    play ambience ambience_int_cabin_day
    show mt angry pioneer with dspr
    $ renpy.pause(0.4)     
    #mt "Это что такое?"
    mt "What is this?"
    #"В руках у неё было моё незаконченное письмо."
    "She had my unfinished letter in her hands."
    #me "А, у нас сегодня обыск в камерах."
    me "Is it a rummage day in our cells?"
    #mt "Обыск? Да я постельное бельё постирать! Что там написано?"
    #mt "Rummage? I just want to wash your linens! What does it say?"
    mt "Rummage? I'm just changing the bedding! What does it say?"
    #me "Какая разница, вы же бельё постирать. Есть право частной переписки, и всё такое."
    #me "It doesn't matter, you are just washing my linens. There is a right of private correspondence, isn't it?"
    me "What does it matter, if you are just changing the bedding? There is a right of private correspondence, and so on."
    #th "А есть ли?"
    th "Is there, actually?"
    #mt "Будь это что-то безобидное, ты бы не увиливал. Ты знаешь, какая разница. Говори! А не то найду того, кто мне его прочитает."
    #mt "You would not shirk like that, if it's not a big deal. You know it does metter. Speak! Or I'll find someone else, who will read it."
    mt "You would not shirk like that if it's harmless. Now tell me what is it, young man! Or I'll find someone else to read it to me."
    menu:
        #"Это любовное письмо":
        "It is a love letter":
            #mt "Что-о?"
            #mt "Say what?"
            show mt shocked pioneer  
            mt "Sorry?"            
            #me "А вы разве не это подумали? Конечно. Я уже с десяток написал, да вот почту никак не найду отправить. У нас ведь с Самантой всё серьёзно. Свадьбу планируем..."
            #me "Do not you think it is? Of course it is. I wrote more then 10 alredy. I just can't find post office. It's so serious with Samantha . We are planning a wedding..."
            me "And isn't it what you thought? Of course it is. Oh, I write letters every day! Too bad I can't find a post office though."
            show mt surprise pioneer with dspr 
            mt "Why do you need a post office? She lives next door..."
            me "Hmmm, you're right. And if we go further, why do I need a letter at all?"
            #"Я вырвал листок из неё в рук, запихал в рот и начал жевать, ожидая своей кары. Неожиданно, вожатая прыснула."
            "I snatched the sheet out of her hands, put it in my mouth and started to chew it, waiting for my punishment."
            show mt laugh pioneer with dspr            
            "Instead, the camp leader suddenly burst out laughing." 
            #mt "Наверное, правда ничего...Тебе бы в клоуны!"
            #mt "I guess it doesn't metter... You have to work as a clown!"
            mt "Looks like it really doesn't matter... What a clown you are!"

            #"Инцидент был исчерпан, но жёсткая бумага пошла у меня весьма плохо. Note to self: писать крамольные письма на салфетках."
            #"The incident was closed, but it was hard to chew stiff paper. Note to self: write a seditious letters on napkins."
            "The incident was closed, but not for my belly, which had to deal with some hard chewed paper. Note to self: write seditious letters on napkins."
            stop music fadeout 5 
        #"Это не любовное письмо":
        "This is not a love letter":
            #me "Это не то, что вы подумали. Но показывать никому нельзя."
            #me "That's not what you thought. But you can't show it to anyone."
            me "It's not what you think. But I can't show it to anyone."
            #mt "И я должна в это поверить?"
            #mt "And I must belive it?"
            mt "And why should I believe that?"
            #me "Ну... Лена знает, что там. И письмо хотел отдать ей. Согласитесь, глупо было бы передавать через девушку..."
            me "The letter is not about me, alright? Lena knows what's in here. I was going to give it to her."
            show mt normal pioneer  with dspr
            stop music fadeout 5 
            #mt "Я у неё узнаю. Но мне не нравятся эти ваши секреты."
            mt "I'll ask her then. But I don't like your secrets."

        #"Оно моё":
        "It's mine":
            #me "Это не такое письмо. Просто поверьте. Ну хотите, поклянусь вам?"
            me "It's not what you think. Just trust me. I can swear, if you want."
            show mt normal pioneer  with dspr
            #mt "Я не очень верю в такие вещи. А других вариантов не вижу."
            mt "I don't trust in such things too much. And I don't see any other options."
            #me "Если вы покажете это Саманте, она очень расстроится. Поверьте хотя бы в это."
            me "Samantha will get upset if you show it to her. Believe this at least." 
            #mt "А зачем ей? Перевёдем и без неё как-нибудь."
            mt "Why would I show her? We can translate it without her."
            #me "Так нельзя, это моя тайна. Можете посадить меня под замок, или отправить домой, но письмо я забираю."
            #me "You can't do this, it's my secret. You can lock me, or send home, but I will take my letter."
            me "You can't do this, it's my secret. You can lock me up, or send me home, but I'm taking my letter back."
            stop music fadeout 5             
            show mt angry pioneer  with dspr
            #mt "Эй, не нужно у меня вырывать. Забирай. Думаешь, я краду чужие письма? Зачем же позвала тогда?"
            #mt "Hey, you don't have to take it by force. You can have it. You think I'm stealing other's letters? Why do you think I called you."
            mt "Hey, don't you dare get physical! {w}Here, take it. If I wanted to steal your letter, why would I ask you about it?"
            #me "Простите."
            me "I'm sorry..."

            #mt "Мне не нравится твоё поведение, и вся эта история. Отправить домой, говоришь? Я ещё подумаю..."
            mt "I don't like your behavior, nor do I like your alibi. Send you home, you say? I'll give that some thought..."

    show mt normal pioneer  with dspr
    #mt "Кстати! Лена завтра уезжает, если тебе интересно."
    mt "By the way. Lena is leaving tomorrow, if you are curious."
    #me "Куда? В Америку?"
    me "Going where? To America?"
    play music music_list["two_glasses_of_melancholy"] fadein 5     
    #mt "Ну, можно и так сказать. Ей нужно кое-какие документы подготовить, чтобы уехать вместе с Самантой. А сделать это можно только в городе."
    #mt "Well, kind of. She needs too prepare some documents, so she can go with Samantha. And she can do it only in the city."
    mt "Well, kind of. She needs to prepare some documents so she can leave with Samantha. And she can only do it in the city."
    #me "Но ведь им не обязательно вместе уезжать? Можно и после всё сделать?"
    #me "But they don't have to leave at the same time. Can she do it later?"
    me "Why can't she wait and join Samantha a bit later? They don't have to travel precisely together, right?"
    #mt "В теории - да, но в нашем случае им лучше уехать вместе. Иначе могут быть проволочки, и будет риск, что она вообще никуда не уедет."
    #mt "In theory - yes, she can. But in our case they should go together. Or there is a chance she won't go anywhere."
    mt "For our sake it'd be better if they went together, or there is a chance she won't go anywhere at all."
    #me "Как жаль."
    #me "That's sad."
    me "That's too bad."
    #mt "Ничего, через год сможет ещё приехать, ей же близко."
    mt "It's okay, she can come back to the camp next year; she doesn't live far from here."
    #th "Это что же, я опять останусь один со своими проблемами?"
    th "Am I going to be alone with my problems again?"
    #mt "Ну чего стоишь, со стиркой помогать будешь?"
    mt "So what are you waiting for? Going to help me with laundry?"
    #me "Эээ..."
    me "Uh..."
    show mt grin pioneer  with dspr
    #mt "Да шучу я. Свободен!"
    mt "Just kidding. At ease!"
    # mt "Да шучу я. Свободен!"
    window hide
    $ renpy.pause(0.8) 
    $ day_time()
    $ persistent.sprite_time = "day"     
    scene bg ext_house_of_mt_day  with dissolve
    play ambience ambience_day_countryside_ambience
    #"Саманта была уже в курсе новостей. Мы стали искать Лену, но в домике и в библиотеке её не оказалось."
    "Samantha was already aware of the news, so we began to look for Lena."
    $ persistent.ssg_98 = True     
    window hide
    $ renpy.pause(0.4)     
    scene bg ext_library_day  with dissolve
    $ renpy.pause(0.8)    
    "...But she was neither in her cabin, nor in the library."    
    show ss smile2 fancy with dissolve    
    #ss "{en}Let's check somewhere else{/en}{ru}Поищем ещё где-нибудь{/ru}?"
    ss "{enen}Let's check somewhere else.{/enen}"
    #me "{en}Don't bother, we'll catch her in canteen later{/en}{ru}Не волнуйся, подловим её после ужина{/ru}."
    me "{enen}Don't bother, we'll catch her in the canteen later.{/enen}"

    #ss "{en}We have nothing to do anyway. Where she might be?{/en}{ru}Делать всё равно нечего. Где она может быть?{/ru}"
    ss "{enen}We have nothing to do anyway. Where might she be?{/enen}"
    #me "{en}Maybe on the island.{/en}{ru}На острове, может быть.{/ru}"
    me "{enen}Maybe on the island.{/enen}"
    #ss "{en}Lead on!{/en}{ru}Веди!{/ru}"
    ss "{enen}Lead on!{/enen}"
    window hide
    $ renpy.pause(0.4)     
    scene bg ext_boathouse_day  with dissolve
    play ambience ambience_boat_station_day
    show ss normal fancy with dspr
    me "{enen}Let's check the boats. One, two... Hm.{/enen}"
    ss "{enen}What?{/enen}" 
    me "{enen}It seems like they're all in place, if I remember their number right.{/enen}"    
    show ss unsure fancy
    ss "{enen}Oh. But you can't be sure?..{/enen}" 
    "I was pretty sure. Yet Samantha seemed really exited about the trip."
    show ss nosmile fancy
    $ day_time()
    $ persistent.sprite_time = "day"    
    menu:
        #"Сказать, что плыть нет смысла":
        #"There is no point in this":
        "Tell her that there's no point in it":
            #th "Моя дневная норма работы на вёслах уже выполнена, не кататься же туда-сюда впустую."
            me "{enen}Sorry, but I don't want to waste time in vain.{/enen}"
            show ss unsure fancy 
            ss "{enen}As you wish, Sam...{/enen}"
            me "{enen}And I'm done rowing for today.{/enen}"  
            ss "{enen}No problem, Sam...{/enen}"            
            window hide
            $ renpy.pause(0.8)
            hide ss with dspr
            $ renpy.pause(0.5)            
            #"Мы немного посидели на мостках, потом я решил всё-таки покатать Саманту по озеру."
            "After we spent some time sitting on the jetty, I gave up and invited Samantha on a little boat ride - not for the sake of searching, just for fun."
            scene cg ss_boat  with dissolve
            play ambience ambience_lake_shore_day
            #"Саманта сидела на носу, свесившись вниз. Мне было боязно - а вдруг упадёт? Я не такой хороший пловец."
            "And the fun was there, but not for me. Samantha was sitting in the bow, leaning over, making me nervous."
            th "What if she falls? {w}I guess she will become all cute and pitiful like a little wet kitten."
            th "But someone should drag her out before that. And I am not a good swimmer."
            #"В какой-то момент мне начало казаться, что она это специально. И обязательно свалится, чтобы я её спас. Но всё обошлось. Наконец, я получил разрешение повернуть к берегу."
            "At some point I began to feel that she is risking on purpose. That she was looking up to fall so I could save her."
            "..."
            stop music fadeout 5
            "But that didn't happen. Soon I got permission to return to the pier."
            jump ss_d4b4din_eng
        #"Свозить её на остров":
        "Take her to the island":
            scene cg ss_boat  with dissolve
            play ambience ambience_lake_shore_day
            $ sp_ss += 1
            #"И снова я в роли гребца, ещё толком не отдохнув после дневных заплывов. Я берег силы, плыли медленно. В конце концов, прогулка чисто увеселительная."
            "Once again, I'm the oarsman. I was saving my strength, rowing slowly, still tired after the previous ride. There was no point in hurrying - it was just a pleasure-ride anyway."
            #"Впрочем, меня она совсем не увеселяла, потому что Саманта сидела на носу, опасно свесившись вниз. И мне совсем не хотелось спасать её из воды. {w}Ну, разве что чуточку."
            #"Well, I didn't realy had fun, because Samantha was leaning in the bow. And I didn't want to rescue her out of the water. {w}Well, maybe a little."
            "In fact, it wasn't pleasurable at all  - Samantha was sitting on the bow, dangerously leaning down. And I definitely had no desire to rescue someone from the water. {w}Well, maybe a smallest desire."
            #"Но всё обошлось, доплыли до острова без потерь."
            "But nothing happened. We made it to the island losslessly."
            window hide
            $ renpy.pause(0.4) 
            play ambience ambience_day_countryside_ambience fadein 1            
            scene bg ext_island_day  with dissolve
            $ renpy.pause(0.4)
            #"Обойти весь остров не заняло у нас много времени."            
            "It didn't take much time to walk around the entire island."
            window hide            
            show ss normal fancy with dspr  
            $ renpy.pause(0.2)            
            #ss "{en}What a beautiful place! She is not here, but can we stay for a bit?{/en}{ru}Какое чудесное место! Лены тут нет, но, может, останемся тут ненадолго?{/ru}"
            ss "{enen}What a beautiful place! Lena isn't here, but can we stay for a bit?{/enen}"
            me "{enen}Sure.{/enen}"
            show ss smile2 fancy 
            #ss "{en}You were here in the morning, right?{/en}{ru}Ты уже был тут утром, так?{/ru}"
            ss "{enen}You've already been here this morning, right?{/enen}"
            me "{enen}Right...{/enen}"
            #ss "{en}Were you alone?{/en}{ru}Ты был один?{/ru}"
            ss "{enen}Did you come here alone?{/enen}"
            show ss unsure fancy with dspr          
            #ss "{en}Don't answer. It's not my business.{/en}{ru}Не отвечай. Это не моё дело.{/ru}"
            ss "{enen}Don't answer. It's not mine concern.{/enen}"
            #th "Очевидно, чужое дело ей очень интересно. Соврать, что был один? Слишком долго думал."
            #th "Obviously, she does care. Should I lie? I took a bit to think about it."
            th "And yet she is obviously concerned. Should I lie and say I was alone? {w}Ugh, I've wasted too much time thinking for that."
            menu:
                #"Я был с Леной":
                #"I was with Lena":
                "I've been here with Lena":
                    #th "Хоть она и просила не рассказывать..."
                    th "Though she asked me not to tell..."
                    if sp_spts == 4:
                        $ sp_ss -= 1
                        show ss serious fancy                    
                        #ss "{en}But you said that you like Slavya.{/en}{ru}Но ты сказал, что тебе нравится Славя.{/ru}"
                        ss "{enen}But you said that you liked Slavya.{/enen}"
                        #me "{en}So? I was here with Lena, as I am here with you now. It means nothing.{/en}{ru}Что с того? Я был тут с Леной, а теперь с тобой. Это ничего не значит.{/ru}"
                        me "{enen}So? I've been here with Lena, as I am here with you now. It means nothing.{/enen}"
                        show ss angry fancy with dspr 
                        #"Саманта промолчала. Кажется, это её задело. Дальше разговор не клеился. Сославшись на приближение ужина, я предложил вернуться в лагерь." #FIXME EN обидело
                        "Samantha remained silent. She seemed upset with my words, but I decided not to make up any excuses this time. Our conversation wouldn't jell from now."
                        "After a brief walk around the island, I suggested that we return to the camp."
                        jump ss_d4b4din_eng
                    if sp_ss <= 7:
                        show ss nosmile fancy                  
                        #ss "{en}It's so cute. I'm happy for you two.{/en}{ru}Это так мило. Я рада за вас двоих.{/ru}"
                        ss "{enen}It's so cute. I'm happy for you two.{/enen}"
                        #me "{en}It wasn't a date or something{/en}{ru}Это было не свидание вовсе.{/ru}."
                        me "{enen}It wasn't a date or something like that.{/enen}"
                        show ss grin fancy with dspr
                        #ss "{en}Sure, sure{/en}{ru}Ну да, ну да{/ru}."
                        ss "{enen}Sure, sure.{/enen}"
                        show ss grin_smile fancy                        
                        #th "А почему я оправдываюсь? Пускай думает, как хочет."
                        #th "And why am I making excuses? Let she thinks as she wants."
                        th "Why am I even making excuses? Let her think what she wants."
                        show ss nosmile fancy  with dspr               
                        #ss "{en}But she is leaving tomorrow. And it's me who separated you. Don't you hate me?{/en}{ru}Но она завтра уезжает. И это я вас разлучила. Злишься на меня?{/ru}"
                        ss "{enen}But she is leaving tomorrow, and it's me who separated you. You must hate me for that.{/enen}"
                        #me "{en}This is ridiculous. As I told you, we are not a couple.{/en}{ru}Это просто смешно уже. Говорю же, мы не пара.{/ru}"
                        me "{enen}This is ridiculous. As I told you, we are not a couple.{/enen}"
                        show ss surprise fancy 
                        #ss "{en}Okay, okay, no need to be angry.{/en}{ru}Окей, окей. Не сердись.{/ru}"
                        ss "{enen}Fair enough...  No need to be angry.{/enen}"
                        show ss nosmile fancy 
                        #th "И чего я так разозлился? Неужели мы уже не сможем общаться как раньше, после моего открытия из недр памяти?"
                        th "And am I angry? Why am I?.."
                        th "I hope we'll be able to talk as we did... before my discovery from the depths of my memory."
                        #"Какое-то время посидели молча. Приближалось время ужина, мы решили возвращаться в лагерь."
                        "We sat in silence for a while. It was almost dinner time, so we decided to return to the camp."
                        jump ss_d4b4din_eng
                    else:
                        stop music fadeout 2
                        show ss shy fancy with dspr                     
                        #ss "{en}Good for you.{/en}{ru}Молодец.{/ru}"
                        ss "{enen}Good for you.{/enen}"
                        play music music_list["what_do_you_think_of_me"] fadein 3                       
                        #"Саманта отвела глаза, и стала ожесточённо обрывать лепестки с ромашки."
                        "Samantha averted her eyes and began to pick off the petals of a daisy." 
                        #ss "{en}I shouldn't ask.{/en}{ru}Я не должна была спрашивать.{/ru}"
                        ss "{enen}I shouldn't have asked.{/enen}"
                        #me "{en}I don't mind answering. It was nothing.{/en}{ru}Тут секрета нет. Мы просто друзья.{/ru}"
                        me "{enen}I don't mind. We're just friends with her.{/enen}"
                        show ss shy2 fancy                      
                        ss "{enen}Really?{/enen}"
                        menu:
                            #"Правда":
                            "Really":
                                me "{enen}Really.{/enen}"
                                show ss shy2 fancy                              
                                #th "Почему я отчитываюсь о своей личной жизни девочке? Почему оправдываюсь? Это не нормально."
                                th "Why do I have to report on my personal life to a little girl? Why am I making excuses? That's not okay."
                                #ss "{en}May I ask again? If I were older... and maybe a little bit prettier... would I have any chanсe... {/en}{ru}А можно ещё спросить? Будь я постарше... И, может, чуть красивее... Был бы у меня шанс...{/ru}"
                                ss "{enen}May I ask again? If I were older... and maybe a little bit prettier... would I have any chance...{/enen}"
                                #th "Ситуация и так была достаточно неловкая."
                                th "And I thought the situation couldn't get any more awkward."
                                #ss "...{en}Against Lena and other girls?{/en}{ru}Рядом с Леной и другими девушками{/ru}?"
                                ss "...{enen}Against Lena and other girls?{/enen}"
                                #ss "{en}This is awkward, I know. But I'm leaving soon, never to see you... guys. So I want to ask.{/en}{ru}Это неловко, знаю. Но всё равно я скоро уеду, и мы больше не увидимся. Так что я хочу узнать.{/ru}"
                                ss "{enen}This is awkward, I know. But I'm leaving soon anyway, never seeing you again. So I want to ask.{/enen}"
                                #me "{en}Sure. What was the question again?{/en}{ru}Конечно. Какой был вопрос, ещё раз?{/ru}"
                                me "{enen}Sure.{/enen} {w}{enen}What was the question again?{/enen}"
                                show ss shy2 fancy
                                #"Саманта игриво толкнула меня в плечо. Она всё ещё ждала ответа."
                                "Samantha playfully pushed me. But she was still awaiting my answer."
                                show ss shy fancy
                                #me "{en}I'm sure that you need nothing more to win a heart of any boy.{/en}{ru}Конечно, у тебя есть всё, чтобы завоевать сердце любого кавалера.{/ru}"
                                me "{enen}I'm sure that you need nothing more to win the heart of any boy.{/enen}"
                                #ss "{en}That's a nice thing to say. Thank you, Mom.{/en}{ru}Очень обнадёживает. Спасибо, мам.{/ru}"
                                ss "{enen}That's a nice thing to say. Thank you, Mom.{/enen}"
                                #"Очевидно, такой ответ её не устроил."
                                "Obviously she wasn't satisfied with my answer."
                                me "{enen}I don't know what else to say.{/enen}"
                                show ss surprise fancy                          
                                #"Я позволил встретиться нашим глазам. Саманта всё-таки ждала от меня чего-то ещё. Доказательства, может быть."
                                "I let our eyes meet. Samantha was still waiting for something more. Maybe she was waiting for a proof."                              
                                menu:
                                    #"Скоро ужин.":
                                    "Dinner is coming":
                                        show ss unsure fancy
                                        #"Мы словно вышли из транса. Да что это за остров такой? Дальше мы успешно делали вид, что этого разговора и не было."
                                        "It were as though we'd come out of a trance. It was so strange so I could swear that something was wrong with this island. {w}This damned atoll, made from a pure pervertion, it almost made me kiss a little girl!" 
                                        "At any rate, we treated this conversation like it never happened."
                                        show ss normal fancy with dspr                                          
                                        ss "{enen}Yes, let's go back.{/enen}"
                                        jump ss_d4b4din_eng

                                    #"Поцеловать её?":
                                    "Kiss her?":
                                        $ sp_ss += 2
                                        #th "Сейчас, глядя в её глаза, я знаю, что ничего плохого не происходит. Плохое? Кто это придумал вообще? Плохим может быть только обидеть её."
                                        #th "When I am looking in her eyes, I know that nothing bad is happening. Bad? Who made it up anyway? The only bad thing I can do is hurt her."
                                        th "Now, looking in her eyes, I know that nothing bad is happening."
                                        th "Bad? Who ever came up with that? The worst thing I can do is upset her, hurt her somehow."
                                        show ss shy2 fancy with dspr                                        
                                        #"Я приблизился и поцеловал её. Это были несколько секунд чистой нежности. {w}Но что же дальше?"
                                        "I approached, and kissed her. It was a few seconds of pure tenderness. {w}But what next?"  
                                        #"Я отвернулся. Меня охватил страх. Я вспомнил то, о чём на время забыл, на глазах моментально выступили слезы. Что я делаю? Теперь всё ещё хуже."
                                        "I turned away. I was gripped by fear. {w=.5}I remembered the things that I had forgotten, and my eyes immediately became wet."
                                        th "What am I doing? It's even more complicated now."
                                        show ss sad fancy                                     
                                        #"Саманта положила руку мне на плечо."
                                        "Samantha put her hand on my shoulder."
                                        #ss "{en}Don't be afraid. There will be no trouble with me. I know my place, and I will be strong. But...Thank you.{/en}{ru}Не переживай об этом. Я больше не доставлю проблем. Я знаю своё место, и буду сильной. Но... Спасибо.{/ru}"
                                        ss "{enen}Don't worry. There will be no trouble with me. I know my place, and I will be strong. But...{w} Thank you.{/enen}"
                                        show ss cry fancy                        
                                        #"Я хотел утешить, прикоснуться к её волосам, но заметил, что по её щеке ползёт слеза. Не надо усугублять. Пусть будет сильной. Хотя бы для меня."
                                        #"I wanted to cheer her up, touch her hair, but I noticed she started to cry. There is no need to deteriorate the situation. She must be strong. At least for me."
                                        "I wanted to comfort her, touch her hair, but I noticed a tear running down her cheek."
                                        th "No need to aggravate it now. May she be strong. At least for me."
                                        show ss crysmile2 fancy 
                                        #ss "{en}So. We are friends again, and everything will be as before. {w}Agreed?{/en}{ru}Итак. Мы всё ещё друзья, и всё будет как раньше. Договорились?{/ru}"
                                        ss "{enen}So. We are still friends, and everything will be as it was before... {w}Agreed?{/enen}"
                                        me "{enen}Agreed.{/enen}"
                                        window hide
                                        $ renpy.pause(0.5)
                                        hide ss with dspr
                                        $ renpy.pause(0.2)
                                        "Samantha walked up to a large oak tree. I gave her some time to be alone. I myself didn't need that - I was tired of thinking about the troubles."
                                        "I was just sitting by the water, looking at the clouds, until Sam has returned to me. She gave me a sad smile and I stood up. It was the time to go back to the camp."
                                        jump ss_d4b4din_eng

                            #"Пока да":
                            "For now":
                                me "{enen}Nothing is going on... yet.{/enen}"
                                show ss serious fancy with dspr
                                #ss "{en}I see. She is leaving tomorrow; you'd better stop wasting your time here.{/en}{ru}Понимаю. Она уезжает завтра, так что хватит тебе тратить тут время впустую.{/ru}"
                                ss "{enen}I see. She's leaving tomorrow, so you'd better stop wasting your time here.{/enen}"
                                me "{enen}But I'm not...{/enen}"
                                ss "{enen}Come on. Let's go back to the camp.{/enen}"
                                "I nodded submissively."
                                window hide
                                $ renpy.pause (1)
                                jump ss_d4b4din_eng



                #"Не отвечать":
                "Don't answer":
                    show ss serious fancy with dspr             
                    #"Поняв, что я не намерен откровенничать, Саманта приуныла. Мы посидели ещё немного, и решили вернуться в лагерь."
                    "I kept silence. Samantha wasn't very happy about that, but she had to realize that I am not going to oblige." 
                    "We sat for a little more, both silent and sad, and even this beautiful island couldn't help it. Finally we decided to go back to the camp."
                    window hide
                    $ renpy.pause (0.5)
                    jump ss_d4b4din_eng

label ss_d4b4din_eng:
    window hide
    stop music fadeout 3    
    $ renpy.pause(1) 

    $ sunset_time()
    $ persistent.sprite_time = "sunset" 
    scene bg ext_boathouse_day  with fade2
    play ambience ambience_boat_station_day
    $ renpy.pause(0.8)     
    play sound sfx_dinner_horn_processed
    $ renpy.pause(0.6)  
    #"Едва пришвартовав лодку, мы услышали сигнал на ужин и отправились в столовую."
    "Just when I moored the boat, we heard the dinner call, and headed toward the canteen." 
    scene bg int_dining_hall_people_day  with dissolve
    play ambience ambience_dining_hall_full
    $ renpy.pause(0.6)  
    show mt normal pioneer at cleft with dspr
    if sp_ss >= 10:
        show ss unsure fancy at cright with dspr
    else:       
        show ss nosmile fancy at cright with dspr
    $ renpy.pause(0.6)        
    #"Лена была на месте, но в присутствии вожатой поговорить с ней не удалось."
    "Lena was here, but I couldn't talk to her in the presence of the camp leader."   
    #"За нашим столом разговор тоже не клеился - сегодня все трое были поглощены своими мыслями."
    "Today, all three of us were absorbed in our own thoughts, so there wasn't much talking at our table." 
    play music music_list["reflection_on_water"] fadein 5
    #"У меня была идея, и я всё больше убеждался в необходимости её реализации. Я больше не вижу себя в этом лагере."
    "My thoughts were all about one idea, and I became more and more convinced that it needed to be realized."
    #me "Ольга Дмитриевна?"
    me "...Olga Dmitrievna?"
    #mt "Да?"
    mt "Yes?"
    #me "Лена завтра поедет в райцентр? А на чём?"
    me "How will Lena get to the district center tomorrow?"
    #mt "На микроавтобусе."
    mt "On a minibus."
    #me "А потом автобус вернётся?"
    me "And then it will return?"
    #mt "Ну да. Пока Саманта у нас, он должен быть тут. На всякий случай. Жалко, своего у нас нет."
    mt "Yes, it will. It must be here while Samantha is here, just in case. It's a pity we don't have one of our own..."
    #me "А можно я с ней поеду?"
    me "Can I go with Lena?"
    show mt surprise pioneer at cleft   with dspr
    #mt "А зачем?"
    mt "Huh? Why?"
    show ss serious fancy at cright 
    #me "Да так, проводить. И домой позвонить бы."
    me "Well, you know, to accompany her. And I thought I could call home."
    show mt normal pioneer at cleft   with dspr
    #mt "Да можно, наверное. Но с кем Саманта останется? Я тоже еду."
    mt "Hm. You can, I suppose... {w}However I am going too, so who will stay here with Samantha?"
    #me "Она не пропадёт. Отпрошусь уж у неё как-нибудь."
    me "She'll be alright. I'll get her permission."
    #mt "Ну, нет. Это почти на весь день. {w}Но если ты выполнишь кое-какие поручения, то можешь ехать вместо меня, пожалуй. А я останусь."
    mt "No way. It'll take most of the day, we can't leave her by herself."
    "Sigh."
    mt "...I don't really like those trips. If you can do some assignments for me there, there will be no need to go for me. I'll stay with Samantha."
    #me "Хорошо. А кто водитель?"
    me "Sure! That will do. {w}And who is the driver?"
    #mt "А какая разница? Виола отвезёт, наверное."
    mt "Does it matter? Viola will drive you, I guess."
    #th "Виола и Лена. Интересная поездка намечается."
    th "Viola and Lena. That's going to be an exciting trip."
    th "...Whatever. I couldn't see myself here anymore. I must get out."
    window hide
    $ renpy.pause(0.5)  
    $ night_time()
    $ persistent.sprite_time = "night"
    scene bg ext_square_night  with fade2
    play ambience ambience_camp_center_evening
    $ renpy.pause(0.5)     
    window show
    
    #"После ужина весь лагерь отправлялся в поход. Толпа пионеров стала формировать подобие колонны по двое, чтобы уместиться на тропинке. Мне не терпелось поговорить с Леной, но не бросать же Саманту."
    "After dinner, everyone gathered for the planned hike."
    "Our crowd of pioneers formed a column, two by two, so everybody could fit on the path. I couldn't wait to talk to Lena, but I also couldn't leave Samantha behind."
    #"Похоже, она прочитала мои мысли, и сама подошла к Мику. Эти двое вполне могли общаться и без моей помощи. Таким образом, я оказался в паре с Леной."
    "It looked like Samantha read my thoughts, because she paired up with Miku. These two could talk to each other without my help. So, I ended up with Lena."
    window hide
    $ renpy.pause(0.5)     
    scene bg ext_path_night  with dissolve
    play ambience ambience_ext_road_night
    show un normal pioneer  with dspr
    $ renpy.pause(0.5)     
    #me "Привет, мы тебя искали."
    me "Hey... I was looking for you."
    #un "Да? А я вас тоже искала. Разминулись, наверное."
    un "You were? I was looking for you too. We must have missed each other." 
    #me "Ты завтра уезжаешь домой?"
    me "So, you are going home tomorrow?"
    #un "Да, утром."
    un "Yes, tomorrow morning."
    #me "Кончился твой отдых. Извини, что так вышло."
    me "Your vacation is over. I'm sorry for that."
    #un "Ничего... И ты же не знал."
    un "That's okay... I had enough fun here."
    #me "Я попросился поехать завтра с тобой."
    me "I asked if I could go with you tomorrow."
    show un shy pioneer  with dspr
    #"Даже в сумерках было заметно, как Лена покраснела."
    "Even in the twilight, I could see Lena blushing."
    #un "В смысле?"
    un "What do you mean?"
    #me "В город. И обратно, если придётся."
    me "To the city. And back, if I need to."
    #"Лена краснела всё гуще."
    "Lena blushed even more."

    #un "Если придётся?"
    un "If you need to?"
    #me "Да... Нет, я не к тебе собрался! В смысле, если дальше надо будет куда-то ехать. Но я так не думаю."
    me "Yeah... {w=.4}No, it's not like I'm going to your place... {w}I mean, I'll go somewhere, if I have to. But I don't think so." 
    #Not sure of context here, tried to make sense of it - Brion
    #un "А что же будет?"
    un "So what's going to happen?"
    show un serious pioneer  with dspr
    #me "Не знаю. Но я почему-то уверен, что не получится уехать из «Совёнка» и вернуться обратно."
    me "I don't know. But I'm certain for some reason that I can't leave «Sovyonok» and come back."
    #un "Это так странно."
    un "That's so strange."
    #me "Если я ошибаюсь, то ничего не потеряю."
    me "Well... If I'm wrong, I won't lose anything anyway."
    #un "А зачем ты это делаешь?"
    un "So why do you need to go tomorrow?"
    show un shy pioneer  with dspr
    #me "Помимо того, чтобы тебя проводить? Не могу я тут больше. Меня от одного вида лагеря тоска берёт."
    me "Despite the fact that I am accompanying you? I just can't be here anymore. I've grown tired of everything in the camp."
    #un "Мне тоже немного грустно. Хорошо, что не одна завтра поеду."
    un "I'm a little bit tired too. I'm glad that I won't be alone tomorrow."
    window hide
    $ renpy.pause(0.5)
    scene bg ext_polyana_night  with dissolve
    show un normal pioneer  with dspr
    #"Процессия остановилась - мы были уже на месте. Теперь нужно было соорудить костёр."
    "The procession stopped – we arrived at the glade. Now we just have to light the bonfires." 
    #un "А ты Саманте сказал уже?"
    un "Did you talk to Samantha about this?"
    stop music fadeout 5    
    #me "Она знает, что я поеду. Но не знает, что не вернусь. Как такое сказать?"
    me "She knows that I'm going with you... but she doesn't know that I'm not coming back. How do I even tell her this?.."
    #un "Наверное, вам нужно попрощаться? Хоть как-нибудь... А мы завтра поговорим."
    un "Maybe you should at least say your farewells. And we can talk tomorrow."
    #me "Пожалуй."
    me "I guess you're right..."
    window hide
    $ renpy.pause(0.8)      
    hide un  with dissolve
    $ renpy.pause(0.6)  
    window show
    #th "Попрощаться не прощаясь - легко сказать. Я подошёл к Саманте и позвал собирать хворост."
    th "Saying goodbye without saying goodbye – no problem."
    "I approached Samantha and invited her to collect some firewood with me."
    window hide
    $ renpy.pause(0.4)     
    play ambience ambience_forest_night 
    scene bg ext_path2_night with dissolve   
    show ss nosmile pilot fancy with dissolve
    play music music_list["tried_to_bring_it_back"] fadein 9
    $ renpy.pause(1) 
    window show 
    me "{enen}Do you believe in irrational things?{/enen}"#Ты веришь в необъяснимые вещи, мистику{/ru}?"
    ss "{enen}What do you mean? Like magic?{/enen}"#@{ru}А во что конкретно? В магию{/ru}?"
    me "{enen}Like premonitions and superstitions.{/enen}"#{ru}В предчувствие, предсказания.{/ru}"
    show ss normal pilot fancy    
    ss "{enen}I guess so. Some things can't be explained.{/enen}"#{ru}Наверное. Хоть предсказания погоды и часто врут.{/ru}"
    me "{enen}There is a possibility that I won't be coming back tomorrow.{/enen}"#{ru}А я серьёзно. Мне кажется, что я могу не вернуться завтра из города{/ru}."
    show ss scared pilot fancy with dspr
    ss "{enen}What? Why?{/enen}"#{ru}Что? Почему?{/ru}"
    show ss surprise pilot fancy 
    me "{enen}I can't really explain it. None of it would make sense.{/enen}"#{ru}Не могу объяснить. Это как раз иррационально.{/ru}"
    ss "{enen}Is this a polite way to say that you're going home?{/enen}"#{ru}Это вежливый способ сообщить, что ты собрался домой?{/ru}"
    me "{enen}It's not up to me.{/enen}"#{ru}Тут дело не во мне.{/ru}"
    show ss vangry pilot fancy with dspr   
    ss "{enen}Of course it's up to you. Why are you leaving in the first place? Just don't go.{/enen}"#{ru}Разумеется, в тебе! Зачем ты туда вообще собрался? Просто не езжай.{/ru}"
    show ss angry pilot fancy 
    if sp_ss >= 10:
        me "{enen}I'm not going home, not taking my stuff... But there's one thing I really have to do. Trust me.{/enen}"#{ru}Я не собрался домой, не беру свои вещи. Ну, одежду. Но мне правда нужно кое-что сделать. Поверь.{/ru}"
        show ss sad pilot fancy with dspr
        ss "{enen}You said that things were going to be like before.{/enen}"#{ru}Ты говорил, что всё будет как раньше.{/ru}"
        show ss cry pilot fancy
        me "{enen}It's not that easy. Maybe they will, maybe they won't.{/enen}"#{ru}Не всё так просто. Может и будет.{/ru}"
        ss "{enen}Are you leaving me or not?{/enen}"#{ru}Ты меня бросаешь, или нет?{/ru}"
        me "{enen}At any rate, it's going to happen, either tomorrow or next week.{/enen}"#{ru}Всё равно это случится. Завтра, или через неделю.{/ru}."
        show ss cry2 pilot fancy
        #th "Зря я всё это затеял. Какой смысл в этом разговоре? Я растворюсь в воздухе, и автобус вернётся без меня? Разве так это работает? Нет, лагеря больше не будет. Если это вообще сработает."
        th "I shouldn't have brought it up. This conversation is pointless."
        th "I'll just disappear into thin air, and the bus will return without me? No way. If the camp is not real then it should disappear with me. Otherwise nothing will happen, and I'll come back here."
        me "{enen}Hey, don't cry. I'm not leaving you.{/enen}"#{ru}Эй. не плачь. Я тебя не брошу.{/ru}."
        show ss cry pilot fancy     
        ss "{enen}Have you changed your mind?{/enen}"#{ru}Ты передумал?{/ru}"
        me "{enen}The bus won't return without me, I promise. You are not gonna be alone.{/enen}"#{ru}Без меня автобус не вернётся, обещаю.{/ru}"
        show ss crysmile2 pilot fancy
        ss "{enen}Thank you.{/enen}"#{ru}Спасибо.{/ru}"
        #"Со стороны поляны послышался шорох и треск веток. Из-за кустов к нам вышла Славя."
        "We heard a rustle from the glade. It was Slavya, passing through bushes."
        show ss shy2 pilot fancy         
        show sl normal pioneer at right
        with dspr   
        #sl "Ребята!"
        sl "Guys!"
        show ss shy pilot fancy      
        #"Полная луна давала достаточно света, чтобы выдать заплаканную Саманту. Славя смутилась."
        "She could easily see Samantha's tearful face in the light of the full moon. Slavya became concerned."
        show sl surprise pioneer at right with dspr
        #sl "Простите. Вас Ольга Дмитриевна потеряла."
        sl "I'm sorry... Olga Dmitrievna is looking for you."
        #me "Тогда пойдём. А Саманте нужна минутка, она догонит."
        me "Let's go, then."
        show sl surprise pioneer close at right
        hide ss
        with dspr
        "I stepped forward, leaving Sammy behind me."
        sl "And Samantha?.."
        me "She'll catch up, give her a minute."

        jump ss_d4fplace_eng


    me "{enen}I'm not lying to you, but I guess you're right. I want to go home.{/enen}"#{ru}Я тебя не обманываю. Но наверное ты права. Я хочу вернуться домой.{/ru}"
    show ss serious pilot fancy 
    ss "...{enen}Another one of Ulyana's victims? Can you explain?{/enen}"#{ru}ещё одна жертва Ульяны, или что? Объяснишь?{/ru}"
    me "{enen}I just don't feel good being here, not anymore. No, I can't say why.{/enen}"#{ru}Мне стало некомфортно тут в последние дни. Не могу объяснить.{/ru}"
    if sp_ss >= 8:
        show ss sad pilot fancy with dspr 
        ss "{enen}I understand what's going on. Maybe it's for the best... {w}It's all my fault.{/enen}"#{ru}Я догадываюсь, что происходит. Может, так будет лучше. Это моя вина...{/ru}"
        me "{enen}No, no! You are amazing and I would spend...{/enen}"#{ru}Нет-нет! Ты замечательная, и я бы провёл{/ru}..."
        me "{enen}None of this is your fault.{/enen}"#{ru}Ты ни в чём не виновата.{/ru}"
        ss "{enen}But you're leaving.{/enen}"#{ru}Но ты уезжаешь.{/ru}"
        me "{enen}It's not for certain. I just want to say goodbye, just in case.{/enen}"#{ru}Это не точно. Но я хотел попрощаться, на всякий случай.{/ru}"
        ss "{enen}I guess, we would have to say it anyway, sooner or later... {w}Goodbye then.{/enen}"#{ru}Рано или поздно прощаться придётся. Прощай тогда.{/ru}"
        show ss crysmile pilot fancy   
        #"Саманта улыбнулась, но в её глазах блеснули слезы. Она протянула мне руку."
        "Samantha smiled, but her eyes were glistening with tears. She gave me her hand."
        show ss crysmile2 pilot fancy with dspr       
        #"Я пожал её, а потом вложил в свою левую, и мы пошли к костру. Пусть хоть так."
        "I shook it, then carefully took her by the arm. We went to the bonfire together. At least it was something."
        jump ss_d4fplace_eng

    else:
        show ss serious pilot fancy with dspr     
        ss "{enen}You're acting like a little girl. Have I done something wrong?{/enen}"#{ru}Ты ведёшь себя как девчонка. Я сделала что-то не так?{/ru}"
        me "{enen}Not at all. Look, I'm not running away. I intend to return tomorrow. Let's just try saying our farewells...{/enen}"#{ru}Вовсе нет. А я не сбегаю, я намерен вернуться завтра. Давай просто попрощаемся понарошку...{/ru}"
        ss "{enen}Stop your tricks. I'm not letting you go until we talk. We can talk tomorrow, right?{/enen}"#{ru}Хватит этих уловок, я тебя не отпущу, пока мы не поговорим. Мы сможем поговорить завтра?{/ru}"
        me "{enen}Yes. We're leaving after breakfast.{/enen}"#{ru}Да. Мы уезжаем после завтрака.{/ru}"
        show ss nosmile pilot fancy         
        ss "{enen}I hope you'll be more reasonable tomorrow. {w}Okay, let's go and burn some stuff.{/enen}"#{ru}Надеюсь, завтра у тебя таких мыслей не будет. А пока пошли сожгём что-нибудь.{/ru}"
        #"Мы вернулись к костру, который уже вовсю полыхал."
        "We returned to the bonfire."
        jump ss_d4fplace_eng
#Заменил гимн пионеров на гимн бойскаутов
#Не смог придумать рифму на 52 строчке


label ss_d4fplace_eng:
    window hide
    stop music fadeout 5    
    $ renpy.pause (0.7)
    scene cg d5_us_ghost  with dissolve  
    play ambience ambience_ext_road_night
    $ renpy.pause (0.4)    
    #"Я решил оставить Саманту в покое на этот вечер. От меня ей одни расстройства."
    "Today I was one big dissappointement for Samantha, so I decided to give her a break, and left her alone for the night."
    play sound sfx_forest_fireplace
    #"Костров у нас было несколько. Я присел возле самого непопулярного, и вскоре обнаружил, что оказался рядом с Ульяной."
    "There were a few bonfires going. I sat down next to the most unpopular one, and soon realized that Ulyana was sitting next to me."
    #me "Сильно попало за краску?"
    me "So, what was the punishment for the paint incident?"
    play music music_list["sweet_darkness"] fadein 5    
    #us "Да ну их. Стены я здорово покрасила. Подумаешь, краска не та. А полы всё равно хотели перекрашивать."
    us "Oh, screw them. I painted the walls fine. Who cares if it was the wrong color? They wanted to repaint the floor anyway."
    #me "А Женя?"
    me "And what about Zhenya?"
    #us "Её перекрашивать не будут. Тяжёлый случай."
    us "Zhenya? No, they won't repaint her. Terrible case."
    #us "Только я ей теперь новые очки должна. Тогда старые мне бы отдали. Такие крутые очки. Бывают чёрные, а эти - белые."
    us "But now I owe her new glasses."
    me "Fair enough..."
    us "Only if they let me keep the old ones. They're so cool! Black sunglasses are nothing compared to the white ones!"
    #me "Зачем тебе, через них же не видно?"
    me "Why do you need them? You can't see through them."
    #us "Да хоть милостыню просить!"
    us "To ask for alms!"
    #me "Это унизительно."
    me "This is humiliating."
    #us "Ну так в очках же. Я никого не вижу, будто и меня никто не видит. А денежки капают."
    us "Not with such glasses! I can't see them, they can't see me. I'm fine with anonymous donations."
    #me "Ловко."
    me "Shrewd."
    #us "Ты чего такой кислый? И костёр у нас унылый. Наш костёр должен быть самым лучшим и самым громким. Давай песню петь!"
    us "Why are you so grim? Everyone at this fire is down in the dumps. We should be the loudest and wildest here! Let's sing!"
    #me "Не знаю ни одной."
    me "I don't know any songs."
    #us "Даже гимн пионеров?"
    us "Not even the Boy Scout's anthem?"
    #me "Не хочу я ничего петь."
    me "I don't want to sing."
    #us "Поём, короче!"
    us "Yet we are starting!"
    #us "{i}Взвейтесь кострами, синие ночи!{/i}"
    us "{i}A scout is a young man who lives by a code,{/i}"
    #me "{i}...За день мы устали очень{/i}"
    #me "{i}...I am turning on my sleeping mode.{/i}"
    me "{i}...To know how to shoot and how to reload.{/i}"    
    #us "Это не та песня! {i}Мы - пионеры, дети рабочих!{/i}"
    us "That's not the right song! {i}To protect his honor, and take the high road,{/i}"
    me "{i}To throw grenades high and watch them explode.{/i}"    
    #us "{i}Близится эра светлых годов{/i}"
    us "{i}He faces temptation, but doesn't give in,{/i}"
    me "{i}Yet soldiers still might have some fun in Berlin.{/i}"    
    #me "{i}Кончится эра тёмных годов{/i}"
    #me "{i}The scout law gives him the foundation to win.{/i}"
    #us "{i}Юны и смелы, Дружной гурьбою, Будем готовы{/i}"
    us "{i}A scout is trustworthy, he's loyal, true blue,{/i}"
    #me "И к мордобою?"
#    me "And he's going to fist fight with you!"
    me "{i}He doesn't need reasons to take a piece of you!{/i}"
    #us "Ну хватит тебе!"
    us "Would you stop it?!"
    #me "Но я правда не помню, как там дальше."
    me "But I really don't remember the lines. You should've taught me first."
    #us "Ладно, пой как хочешь, лишь бы громко!"
    us "What? No way. I won't teach anybody."
    me "Why?"
    us "Because I hate teachers! And teaching! And this song precisely! But we should sing it!"
    me "So?.."
    us "...Fine! Sing whatever you want, just make it louder!"
    #me "За громкость в нашем дуэте отвечаешь ты."
    me "Loud noises are {i}your{/i} responsibility."
    #us "{i}Радостным шагом, с песней весёлой{/i}"
    us "{i}Helpful, friendly, courteous to me and to you{/i}"
    #me "{i}За гамбургером и кока-колой!{/i}"
#    me "{i}He's eating a sandwich, and drinks mountain dew!{/i}"
    me "{i}You see he is sober today - you say \"phew!\"{/i}"
    #us "{i}Близится эра светлых годов, Клич пионеров{/i}"
    us "{i}Kind, obedient, cheerful, thrifty every cent,{/i}"
    #me "Всегда бей котов!"
    me "{i}He sees jews like that and they run from the land!{/i}"    
#    me "{i}Beat people and animals 'till the very end!{/i}"
    #us "{i}Грянем мы дружно, Песнь удалую, За пио...{/i}"
    us "{i}On my honor I will do my best...{/i}"
    #me "{i}...нерок, что я целую{/i}"
    me "{i}...in kissing the pioneers just for the test!{/i}"
    #us "Что?"
    us "What?"
    #me "Что?"
    me "...What?"
    #us "Кого ты там целуешь?"
    us "Who are you kissing?"
    #me "Никого. Из песни слова не выкинешь."
    me "No one... You can't change the lyrics."
    #us "Это не песня. Вот у Алисы песни. Жалко, нету её. Вот бы спели сейчас."
    us "What lyrics? It's not even a song... {w}Alisa knows a lot of songs. It would be fun if she was here!"

    #"Хотел спросить, почему нет Алисы, но к нашему костру стали подходить другие пионеры, привлечённые нашей песней. Пиар-план Ульянки удался. Вместе мы спели ещё несколько песен."
    "I wanted to ask why Alisa wasn't around, but some other pioneers started to approach our bonfire to listen to our song. Ulyana's PR plan was successful. We proceeded to sing a few songs together."
    window hide
    $ renpy.pause(0.5) 
    stop sound fadeout 2
    scene bg ext_path_night  with fade2
    play  ambience ambience_camp_entrance_night
    # "Пришла пора возвращаться в лагерь. Шли уже не колонной, а как придётся. Саманта была с Ольгой Дмитриевной, а я не без труда, но нашёл в темноте Лену."
    "It was time to get back to the camp. There were no columns now; everyone walked as they wished. Samantha was with Olga Dmitrievna. With some difficulty, I managed to find Lena in the darkness."
    window hide
    show un normal pioneer with dspr
    $ renpy.pause(0.3)     
    #me "Как тебе поход?"
    me "So, how was the hike?"
    #un "Хорошо. Истории рассказывали."
    un "It was good. We told stories."
    stop music fadeout 5
    #me "И ты тоже?"
    me "You too?"
    #un "Только слушала. А у вас как дела?"
    un "No, I was only listening. And how are you doing?"
    #me "Да так. Затея с прощанием была не очень."
    me "Meh. Saying goodbye was a terrible idea."
    show un sad pioneer  with dspr
    #un "Извини."
    un "I'm sorry."
    #me "Ничего. Может, потом мне от этого будет легче. Попрощался, вроде как. Или попытался."
    me "Don't be. Maybe it will be easier tomorrow. I did say goodbye, at least. Well, sort of. At least, I tried to."
    #un "Как-то не верится, что ты завтра пропадёшь куда-то."
    un "I still can't believe you're gonna disappear tomorrow."
    #me "Это нормально, если ты в своём уме."
    me "It's fine. Believing that would mean you're out of your mind."
    play music music_list["memories"] fadein 3
    #un "А я вот точно уеду."
    un "...But I am leaving tomorrow - that's for sure."
    #th "А ведь завтра нам с Леной прощаться. Вероятно, при посторонних."
    th "So tomorrow is my last day with Lena. And there probably won't be time to talk privately."
    #me "Тоже попрощаемся заранее?"
    me "...Should we say goodbye in advance?"
    #un "Не надо."
    un "Why?.. If you want to..."
    me "Not really. Nevermind."
    "Lena nodded gratefully. It was a pretty hard day for her too."
    window hide
    $ renpy.pause(0.5)
    scene bg ext_camp_entrance_night  with dissolve
    #"Впереди показалась пара сиротливых огоньков - это лагерь. Сейчас пионеры разбегутся по домикам, и их станет больше. Пионеры разбежались, а мы с Леной остались. Заговорили одновременно:"
    "A couple of lonely lights appeared in front of us - it was our camp. Soon the pioneers would go to their cabins, and there would be more lights."
    scene bg ext_square_night
    show un normal pioneer  
    with dissolve
    $ renpy.pause(0.5)    
    play ambience ambience_camp_center_night    
    "Lena and I ended up alone. We started talking at the same time:"
    #un "Ну я пойду..."
    un "Well, I'll be going..."
    #me "Хочешь ещё погулять?"
    me "Do you want to take a walk?"
    show un smile pioneer  with dspr
    #un "Да."
    un "Yes."
    #"Мы немного прошлись, и присели на скамейку."
    "We walked for a while, until we found a very appealing bench."
    window hide
    $ renpy.pause(0.2)    
    scene cg d6_un_evening_1 with dissolve2
    $ renpy.pause(1.2)
    window show 
    #me "В городе таких звёзд не увидишь."
    me "You can't see so many stars in the city."
    #un "Да..."
    un "Yeah..."
    # me "Надо было сказать, что я со звезды упал. А то какой-то автобус. Не романтично."
    me "I should have said I fell down from a star, not from the bus. It would've been much more romantic."
    #un "Не важно... Хоть со звезды на автобусе."
    un "I don't care... Bus is fine too."
    #me "Знаешь, спасибо тебе. Без тебя это был бы совсем другой день."
    me "I must thank you. It would've been a different day without you."
    scene cg d6_un_evening_2  with dspr
    #un "Что будешь дальше делать?"
    un "So what are you gonna do next?"
    #me "Не представляю. А ты? После Америки?"
    me "I have no idea. What about you? After America?"
    #un "Первым делом... Тебе напишу, наверное. Если скажешь куда."
    un "First... I will send you a letter, I guess. If you tell me where to send it."
    #me "С этим проблема."
    me "That might be a problem."
    #un "Тогда ты мне напиши сначала."
    un "Then you should write me first."
    #me "Не уверен, что письмо дойдёт."
    me "I don't think you'll receive it."
    #un "Всё равно напиши. Мало ли."
    un "Just write me. You don't know it for sure."
    stop music fadeout 7
    # me "Хорошо. Я вам обеим напишу. В прошлое. Вот на почте удивятся."
    me "Deal. I will write both of you. {w}A letter to the past... I bet the post office will be very surprised."

    #un "Обеим... Ты сегодня два раза на острове побывал?"
    un "Both of us... You visited the island twice today?"
    #me "Откуда знаешь?"
    me "How did you know?"
    play music  music_list["raindrops"] fadein 7
    #un "Я была на причале и видела, что лодки нет."
    un "I was on the pier, and I saw that one boat was missing."
    #me "Мы тебя искали."
    me "We were looking for you."
    #un "Долго искали."
    un "It took some time."
    #"Лена встала со скамейки."
    "Lena got up."
    scene bg ext_square_night  
    show un angry2 pioneer 
    with dissolve    
    #un "Может быть, будет честно, если ты только одной из нас {i}напишешь?{i}"
    un "Maybe it will be more fair if you {i}write{i} only one of us?"
    if sp_ss >= 8:
        #th "Такого я не ожидал, но если подумать, претензии обоснованы. Врать не буду."
        th "I didn't expect this at all, but I can understand her concern. I shouldn't lie here."
        #me "Понимаю. Может и будет честно."
        me "I see. Maybe you are right. That would be fair."
        #un "Ну и кому же тогда? Не бойся меня расстроить, я не передумаю насчёт поездки."
        un "So, who are you going to write to? {w}Don't worry about upsetting me. I'm not changing my mind about going."
        menu:
            #"Тебе, конечно":
            "Of course I'll write you":
                show un cry_smile pioneer  with dspr
                #un "Правда?"
                un "Is it true?"
                show un smile3 pioneer  with dspr
                #me "Правда."
                me "It is."
                if sp_ss >= 10:
                    #th "Лучше предать Саманту на словах, чем всё испортить."
                    th "It's better to betray Samantha just in words, rather than screw everything up." 
                jump ss_d4toun_eng
            #"Не буду отвечать":
            "Don't answer":
                #me "Я не считаю, что кого-то обманываю, и выбирать не буду."
                me "On the second thought, I don't want to choose."
                jump ss_d4tosa_eng
            #"Ей":
            "I will write to her":
                # un "Надо же. И впрямь Ромео и Джульетта..."
                un "Amazing... Like a real Romeo and Juliet, indeed."
                jump ss_d4tosa_eng

    else:
        # me "О чём ты? Ты что, ревнуешь к ней?"
        me "What's wrong? Are you... jealous?"
        show un serious pioneer  with dspr
        #un "Да разве я имею право?"
        un "Do you think I have the right to be?"
        #me "Имеешь, наверное. Если оно тебе надо. Но ей всего тринадцать."
        me "It's up to you, I guess. Go ahead, If you want to be, but why? She is only thirteen."
        #un "Мне тоже было тринадцать. Я помню, что к чему в этом возрасте."
        un "I was thirteen once. I know how things are like at that age."
        #me "Но мы с ней просто друзья."
        me "Our things are simple: we're just friends."
        #un "Возможно, только с твоей стороны. Ты считаешь такое хорошей дружбой, правильной?"
        un "Maybe to you it's simple. What about her? Do you think it's a good friendship? Do you think it is right?"
        #me "Ты ошибаешься. Это правда хорошая дружба. А если станет ещё лучше, то будем как брат и сестра."
        me "You're wrong, we do have a good friendship. Make it a little bit better, and we will be like brother and sister."
        jump ss_d4toun_eng

label ss_d4tosa_eng:
    show un sad pioneer  with dspr
    #un "Ладно. Похоже, я занимаю чужое место на этой скамейке. Увидимся завтра."
    un "Fine. It looks like I'm taking someone else's place here. I'll see you tomorrow."
    hide un  with dspr
    stop music fadeout 5
    #th "Надеюсь, я ничего не испортил. А ведь ещё как испортил. Как они теперь будут общаться?"
    th "I hope I didn't screw everything up..."
    th "In fact, I'm sure that I did. How are they gonna talk from now on?"
    #th "Впрочем, я был честен с близкими мне людьми, и это главное. Наверное."
    th "Well, at least I was honest to people closest to me. And that's what's important. {w}I guess."
    jump ss_d4epi45_eng



label ss_d4toun_eng:
    show un smile pioneer  with dspr
    #un "Хорошо. Я рада."
    un "Good. I'm happy."
    #me "И я рад."
    me "Me too."
    #un "Ты говоришь \"спасибо\", но ведь сам ты сделал для лагеря куда больше... И для меня."
    un "You are thanking me, but you really did a lot yourself. For this camp... And for me."
    #me "Вряд ли. Ну, может быть. Ладно, мы оба молодцы."
    me "I don't know. Well, maybe. I guess we both did okay... Yes, we did good."
    #un "Молодцы..."
    un "Good..."
    #me "Думаешь, нас ждёт награда впереди?"
    me "Do you believe in some kind of... reward? For us?"
    show un sad pioneer  with dspr
    #un "Я боюсь об этом думать. О том, что {i}нас{/i} ничего не ждёт."
    un "I'm afraid to think about it. What if there is actually nothing for {i}us{/i}?"
    #me "Всякое может быть. Чудеса случаются, как я убедился."
    me "Anything could happen. Miracles happen. I know it for sure... Because I am in a miracle right now."
    stop music fadeout 5
    #un "Значит, надежда только на чудо?"
    un "So, there's only hope for another miracle?"
    #me "Не грусти. {w}У нас ещё остался этот вечер."
    me "Don't be sad. {w}We still have the rest of the evening."
    window hide
    $ renpy.pause(2)
    scene black  with fade2
    $ renpy.pause(1.5)
    $ sp_un += 4
    jump ss_d4epi45_eng

label ss_d4epi45_eng:
    window hide
    $ renpy.pause(1.5)
    window show
    play music music_list["farewell_to_the_past_full"] fadein 5
    $ set_mode_nvl()
    #"Заканчивается этот день. Сегодня я столкнулся с проблемой, которую, быть может, и нельзя решить. Мы попытались спасти Саманту, чем это кончится - я вряд ли узнаю."
    "The day is ending. Today I faced a problem that I probably couldn't solve. We tried to help Samantha, but I guess I'll never know how that will end."
    #"Возможно, я вернусь домой, и мир вокруг будет прежним. Наверное, так и будет."
    "Maybe I will get home, and everything will be the same as it was. I guess it is the most probable way."
    #"Но будет ли это значить, что нам не удалось, и моя подруга мертва? Я скажу - нет. Не нужно ставить знак равенства между такими разными мирами. Там всё другое, даже я."
    "But does it mean that we failed, and my friend is dead? {w}No, I don't think so."
    "Our worlds are not equal. Everything is different there, even me."
    nvl clear     
    #"Завтра я покину «Совёнок». Навсегда или нет? Сейчас этот вопрос - что-то из области философии. Недавно я знал только один мир, и мог с умным видом о нём рассуждать."
    "Tomorrow I will leave «Sovyonok». Forever or not? The question is in the realm of philosophy."
    "Recently, I knew of only one world, and I could talk sagely about it."
    nvl clear   
    #"Четверть века я изучал правила игры в своём мире, верил в его надёжность. И что он сейчас? Не больше, чем сон. Но и «Совёнок» пока не могу принять, как реальность. Всё слишком странно."
    "For a quarter of a century, I had studied the rules of the game in my world, and I believed it to be true. But what is it now? Nothing more than a dream. But I can't accept «Sovyonok's» reality either. Everything is so weird."
    # "Если есть два сна, то какой из них главнее и первичнее? Пока не разобрать."
    "If there are two dreams, then which one is true? I don't know yet."
    # "Правил этой игры мне не постичь. Допустим, кто-то мне их расскажет. Но где гарантии, что всё не перевернётся с ног на голову ещё раз? Лучше ничего не ждать, чтобы не остаться в дураках."
    "I can't understand the rules of this game. And even if someone tells me all of them, there is no guarantee that everything won't change all over again. It is better not to wait for anything, rather than end up with nothing."
    # "Сосредоточусь на том, что у меня есть. Куртка, разряженный телефон и... они. Между нами пропасть, но мы так близки. Такие призрачные, и такие настоящие. Сейчас они - главное в моей жизни."
    nvl clear 
    "I will focus on what I have: my jacket, my dead mobile phone, and... them."
    "There is a gulf between us, yet we are so close. So illusive, and yet so real. They are now the main things in my life."
    # "Вернусь ли я сюда, встречу ли кого-то из них ещё раз? Не узнаю, пока не встречу, или пока не пройду все уровни, которые мне уготованы. Но ничто не убедит меня, что не встречу никогда."
    "Will I return here? Will I meet them again? Any of them? I won't know until it happens, or until I finish my way."
    "But nothing will convince me that it I won't meet them anymore."
    $ renpy.pause(0.9) 
    if sp_un >= 10:
        # "Так мы с Леной пообещали друг другу."
        "Me and Lena promised that to each other."
        window hide
    $ renpy.pause(0.5)      
    $ set_mode_adv()

    stop music fadeout 4 
    $ renpy.pause(2)

    scene black  with fade
    $ set_mode_nvl()

    play sound sfx_achievement
    # "Концовка: на Запад."
    "Ending: to the West."
    $ set_mode_adv()
    $ renpy.pause(1)
    menu:
        #"В меню":
        "Main menu":
            return
        #"В начало":
        "To the begining": 
            jump sam
        #"Ещё модов!":
        "More mods!":
            play music BBtheme
            window hide
            show cg bbsov 
            $ renpy.pause(20)
            scene black  with fade
            #"Мод пройден"
            "Mod is completed"
            show cg bbsov 
            menu:
               #"В меню":
                "To the begining":
                    stop music fadeout 10
                    jump sam
                #"Выход":
                "Main menu":
                    return



#
label un_usafail1_eng:
    window hide
    $ renpy.pause(0.5)  
    play music music_list["i_dont_blame_you"] fadein 3
    scene bg ext_houses_day  with fade
    $ set_mode_nvl()
    $ renpy.pause(0.5)
    if un_usa == True:
        #"Мы с Самантой обсудили произошедшее. Нужно было признать поражение. Я старался не подавать виду, как я расстроен."
        "Samantha and I discussed what had happened. We had to admit defeat. I tried not to show that I was upset."
        #"Но оказалось, что эта безумная идея для меня даже важнее, чем я думал. Мне нужна была эта соломинка. Мы пробовали читать пьесу по ролям, но я просто не мог сейчас заниматься этой чепухой."
        "It turned out that this crazy idea was even more important to me than I had thought. I needed this straw. We tried to read our roles for the play, but I just couldn't spend time on such trifles now."
    else:
        #"После обеда мы с Самантой пытались репетировать и заниматься прочими привычными вещами, но сегодня я просто не мог себя заставить. Мои мысли были о другом."
        "After dinner, Samantha and I tried to rehearse the play and do usual stuff, but I just couldn't force myself to. My thoughts were away from there."

    #"Саманта, конечно, заметила. Опередив её вопросы, я ушёл к себе в домик, сославшись на то, что мне нездоровится. К счастью, вожатой там не было. Но на это можно было рассчитывать."
    "Samantha noticed it, of course. Saying that I was unwell, I went to my cabin before she could ask any questions. Fortunately, Olga was not there. But that was expectable."
    nvl clear
    #"Я стал писать письмо на английском. Наверное, моя старая учительница пришла бы от него в ужас, но смысл понять было можно. В письме было всё то, что я рассказал Лене этим утром."
    "I began to write a letter in English. In retrospect, my old teacher would be horrified by it, but one could at least get its main point and it contained everything that I told Lena that morning."
    #"Теперь у меня была новая соломинка. Я пошёл к Саманте, отдал тщательно запечатанное письмо и заставил поклясться, что она его не откроет, пока не вернётся на родину."
    "Now I had another straw. I went to Samantha, gave her the well-sealed letter and made her swear that she wouldn't open it before she returned to America."
    #"Кроме того, я заставил её спрятать его подальше и пообещать, что никому о нём не скажет. Конечно, Саманта была в полном недоумении. Я почти что видел недовольную рожицу с жирным красным минусом над её головой."
    "On top of that, I had her safely hide my letter and promise not to tell anybody about it. Of course, Samantha was absolutely confused. I could almost see a dissatisfied smiley with a thick red minus above her head."
##it's partially a reference to relationship worsening in The Sims, smth like this http://redshirtknitting.com/images/2011/09/905-17.jpg 
###picked up on that, great reference
    nvl clear
    #"Сегодня я весь день игнорировал её, пугал, требовал чего-то. Я постарался загладить вину, как мог. До ужина мы играли в самую девичью ерунду, и слушали самую девичью музыку. Я был её послушной куклой."
    "I've been ignoring, scaring and demanding something from her for the whole day. I tried to make amends. Up until dinner we played the most girlish things and listened to the most girlish music. I was her obedient doll."
    #"Хорошо, что у неё не нашлось одежды моего размера и косметики, иначе быть бы мне и накрашенным, и втянутым в прочие дочки-матери (не знаю, как они это делают)."
    "I'm glad she didn't have any clothes or make-up for me. Otherwise I would be rouged and dragged into playing house (I have no idea how they do it)."
##child game about imitating family, in Russian literally - 'daughters and mothers', https://en.wikipedia.org/wiki/House_%28game%29
    nvl clear
    #"После ужина весь лагерь отправлялся в традиционный поход, с неизменным костром в финале. Я решил, что должен как минимум проводить Саманту, а там посмотрим."
    "After dinner, everyone in the camp prepared to go on a traditional hike, with a campfire in the end as always. I decided to at least accompany Samantha, and after that see how things would go."
    window hide

    $ renpy.pause(0.5)
    $ set_mode_adv()
    $ night_time()
    $ persistent.sprite_time = "night"
    scene bg ext_square_night  with fade
    #mt "Все в сборе? Тогда выступаем!"
    mt "Everybody here? Let's go, then!"
    scene bg ext_path_night  with dissolve
    play ambience ambience_ext_road_night
    show ss nosmile pilot fancy 
    #"Пионеры растянулись в цепочку по двое, с Ольгой Дмитриевной во главе. Мы с Самантой шагали прямо за ней."
    "Pioneers lined up in pairs, with Olga Dmitrievna at the head. Samantha and I were right behind her." 
    ss "{enen}What's wrong with you today?{/enen}"
    me "{enen}Just a bit tired.{/enen}"
    show ss serious pilot fancy with dspr 
    ss "{enen}Lies.{/enen}"
    me "{enen}What?{/enen}"
    ss "{enen}Is it about the letter?{/enen}"
    me "{enen}I can't say.{/enen}"
    ss "{enen}This letter's gonna make me sad, right?{/enen}"
    me "{enen}We should not discuss it. You've promised.{/enen}"
    show ss unsure pilot fancy with dspr  
    #"Вместо ответа Саманта взяла меня за руку."
    "Instead of answering, Samantha grabbed my hand."
    menu:
        #"Так не пойдёт":
        "This won't work":
            show ss sad pilot fancy with dspr         
            #"Я высвободил руку. Саманта отвернулась."
            "I freed my hand. Samantha turned away from me."
            me "{enen}It's not {i}that{/i} type of letter. Not what you think.{/enen}"
            $ sp_ss -= 1
        #"Ничего страшного":
        "There's no harm in it":
            #"Пусть думают, что хотят. Всё равно я больше не увижусь ни с ней, ни с ними."
            "I don't care what people think. I'll never see neither them nor her again anyway."
            $ sp_ss += 1
    #"На самом деле, я обдумывал, как бы сбежать. Из лагеря, насовсем. Это удачный момент. Усадить Саманту у костра, раствориться в ночи, и бегом в лагерь. Будет куча времени на сборы, которые мне не особо и нужны."
    "Actually, I was thinking of how to run away. Away from the camp, forever. This was a good time. Leave Samantha by the campfire, dissolve into the night, and run to the camp. I'd have plenty of time to pack, though I didn't have much."
    stop music fadeout 5
    window hide
    $ renpy.pause (0.5)
    scene bg ext_polyana_night  with dissolve
    $ renpy.pause (0.5)
    #"Вот мы и на месте. Пионеры стали организовывать костёр. Пора прощаться с Самантой? Но как? Все прощания немного грустные, а тут такое... От одной мысли щипало в глазах. Лучше просто уйти."
    "Finally, we arrived. Pioneers began to kindle the campfire."
    th "I must say my goodbye to Samantha... But how?"
    th "All farewells are sad, and this case... Just thinking of it scratches my eyes. Maybe it will be better to just leave?"
    #"Я заметил одиноко сидящую Лену. Нужно попрощаться хотя бы с ней. Я сделал вид, что собираю ветки, подошёл к Лене со стороны леса и тихонько позвал."
    "I noticed Lena sitting alone. It was a chance to say goodbye at least to her; I pretended I was collecting firewood and quietly called her out."
    window hide
    $ renpy.pause (0.4)
    scene bg ext_path2_night  with dissolve    
    play ambience ambience_forest_night
    $ renpy.pause (0.4)    
    show un normal pioneer far  with dspr 
    show un normal pioneer  with dspr
    $ renpy.pause (0.3)    
    play music music_list["reminiscences"] fadein 7
    #un "Как ты?"
    un "Hey. How are you?"
    #me "Плохо. Я не могу больше делать вид, что всё в порядке. Поэтому ухожу из лагеря. Прямо сейчас."
    me "I... I've been better."
    show un sad pioneer  with dspr    
    un "Is it so bad?"
    me "I can no longer pretend that everything's fine. That's why I'm leaving the camp. Right now."
    #un "Уходишь? И что же будет дальше?"
    un "Leaving? And what's next?"
    #me "Понятия не имею. Но нужно что-то изменить."
    me "I have no idea. But I have to change something."
    #un "Тебя искать будут. А с нами что будет? С Самантой?"
    un "They will look for you. And what about us? What about Samantha?"
    #me "Не знаю. Наверное, это эгоистично, но по-другому уже не могу. У неё моё письмо, там всё. Подозреваю, что они его всё-таки откроют. Подумают, совсем спятил."
    me "I don't know. It is selfish, I guess, but I can't act otherwise."
    un "I hope you know what you're doing."
    me "...She has my letter. Everything's in it. {w}I suspect they'll open it nevertheless - and think that I went insane."
    #me "Что же. У меня мало времени."
    me "Well. I don't have much time left."
    show un cry pioneer  with dspr 
    #un "Значит, мы больше не увидимся?"
    un "So this means that we'll never see each other again?"
    menu:
        #"Пошли со мной":
        "Come with me":
            #un "Что?"
            un "What?"
            #me "Прости. Это уже действительно эгоистично. Но я так устал. И не хочу быть один. Если бы ты пошла со мной... Но это безумие."
            me "I'm sorry. It really is selfish, but I am so tired. And I don't want to be alone. If you could come with me... But this is madness."
            if un_usa == True:
                #me "Ухожу. Передай Славе, чтоб..."
                me "I am leaving now. Tell Slavya to..."
            else:
                #me "Ухожу. Скажи Саманте... Хотя как ты..."
                me "I am leaving now. Tell Samantha... Though how would you do it..."
            if sp_un >= 6:
                show un cry_smile pioneer  with dspr 
                #un "Я с тобой."
                un "I'm with you."
                #me "Что?!"
                me "What?!"
                #un "Я пойду с тобой."
                un "I'm coming with you."
                #me "Нет, я не могу. Мне терять нечего, а у тебя тут вся жизнь."
                me "No, you can't do that. I have nothing to lose, but your whole life is here."
                #un "Значит, вот так я ей распоряжусь."
                un "Then that's how I will use it."
                show un cry_smile pioneer close  with dspr
                #"И тут Лена приподнялась на цыпочках и поцеловала меня."
                "And then Lena stood on her tiptoes and kissed me."
                window hide
                $ renpy.pause (1)    
                show blink
                $ renpy.pause (1)             
                #"Наши губы сомкнулись всего на пару секунд, но этого хватило, чтобы решить спор в её сторону."
                stop music fadeout 7                
                "Our lips touched only for a couple of seconds, but that was enough to admit her victory in the dispute."
                window hide
                $ renpy.pause(1)                
                jump un_runaway_eng
            else:
                #un "Ты точно не передумаешь?"
                un "You are not going to change your mind, are you?"
                #me "Нет... Прощай."
                me "No... Goodbye."
                show un cry pioneer close  with dspr 
                #"Я робко поцеловал Лену в щеку, и не поднимая на неё глаз, пошёл прочь."
                "I shyly kissed Lena on the cheek and went away, without looking at her."
                #un "Прощай."
                un "Goodbye."
                jump ss_ungb_eng

        #"Прощай":
        "Goodbye":
            #me "Спасибо за всё. И не плачь, не стою я того."
            me "Thank you for everything. And don't cry, I'm not worth it."
            #"Я неловко поцеловал Лену в щеку, развернулся, и пошёл прочь."
            "I awkwardly kissed Lena on the cheek, turned around and went away."
            #un "Нет, ты хороший."
            un "...No, you are good."
            window hide
            hide un with dissolve
            window show
            #"Эти слова донеслись уже откуда-то сзади. Я продирался через ночной лес, стараясь найти обходной путь к тропинке. Прощай, Лена."
            "These words reached me from somewhere behind. I waded through the night forest, trying to find a direct way to the path. Goodbye, Lena."
            jump ss_ungb_eng

label ss_ungb_eng:

    scene bg ext_path_night  with dissolve 
    play ambience ambience_camp_entrance_night
    stop music fadeout 4
    #"Однако, уйти незамеченным мне не удалось."
    "However, I didn't manage to get away unnoticed."
    show us normal pioneer far  with dspr 
    show us normal pioneer  with dspr 
    #us "Эй, ты!"
    us "Hey, you!"
    #th "Не до тебя..."
    th "Not now..."
    play music music_list["you_won_t_let_me_down"] fadein 5
    #us "Куда бежишь-то?"
    us "Where are you running?"
    #me "Сбегаю, разве не видно?"
    me "I'm running off, can't you see?"
    show us sad pioneer  with dspr 
    #us "А почему?"
    us "But why?"
    #me "...Деревья - наши братья. Я не могу делить жилье и пищу с варварами, которые жгут трупы деревьев, поют и беснуются по этому поводу."
    me "...Trees are our brothers. I can't share my home and food with the barbarians, who burn the corpses of trees, then sing and rave about it."
    show us grin pioneer  with dspr 
    #us "Эээ... А давай затушим им тут всё?!"
    us "Uuuh... Then let's extinguish the fires?!"
    #me "Я пошутил. Кажется, я кипятильник забыл выключить. Как бы пожар не устроить. Побегу."
    me "It's a joke. I think I forgot to turn off the boiler. Don't want to cause a fire, so I need to run."
    show us normal pioneer  with dspr 
    #us "Хотя бы в одном месте хороший костёр будет. Скучно в этот раз, Алисы тоже нет. А с тобой можно?"
    us "At least we'll have a nice fire finally! It's so boring this time, and Alisa is off too... Can I go with you?"
    #me "Нельзя! Пока."
    me "No... Gotta run, sorry! Bye!"
    window hide
    $ renpy.pause(0.5)  
    scene bg int_house_of_mt_night  with fade
    play ambience ambience_int_cabin_night
    stop music fadeout 5
    #"Без помех добравшись до домика, я первым делом переоделся в одежду, в которой приехал. Почти готов к побегу. Кое-что я приготовил заранее, осталось только собрать еды, какая есть."
    "I reached the cabin without any more problems. At first I changed into the clothes I arrived here in - with that I was almost ready to run."
    "Some other things I've prepared in advance, now I only had to gather some food."

    #"Пожалуй, мне могло пригодиться одеяло и много чего ещё, но кража госсобственности в мои планы не входила."
    window hide
    $ renpy.pause(0.5)     
    "I could use a blanket and perhaps something else, but the theft of state property wasn't the part of my plans. "
    scene bg ext_house_of_mt_night_without_light  with dissolve
    play ambience ambience_camp_center_night

    #"Собравшись, я вышел на улицу. Чтобы быть пойманным Алисой."
    play music music_list["gentle_predator"] fadein 2    
    "Having gathered, I went outside..."
    show dv grin pioneer with dspr    
    "...only to be caught by Alisa."
    #dv "А мне вот интересно стало, чего это у вас свет горит."
    dv "I was wondering why there was a light on in there."
    #me "...Да так, решили на костре пожарить чего-нибудь... Вот я и заскочил..."
    me "I went for some food... To fry over a campfire, you know?.."
    #dv "За печеньем?"
    dv "So you got cookies?"
    #me "А ты чего тут? Не нравятся костры?"
    me "Why are you here anyway? Don't like fires?"
    window hide
    show dv normal pioneer  with dspr 
    window show
    #dv "Неа. И вообще огонь не люблю, знаешь ли."
    dv "I don't. Don't like fire at all."
    #me "Кто бы мог подумать."
    me "Who would have thought?"
    #dv "...У меня же дед работал на спичечной фабрике, ему иногда зарплату спичками давали. Всю комнату ими заставил."
    dv "My grandfather worked in a match factory..."
    me "So? You got tired of fires?"
    dv "No. Sometimes he was given his salary in matches. He had enough to fill a whole room."
    #dv "А мне говорил \"не играй со спичками, внучка, ты у нас такая взрывная, мигом сгоришь\".
    dv "And he told me \"Don't play with matches, you're such a blast, you'll immediately burn!\"."
    #me "А ты, конечно, не слушалась?"
    me "And of course you didn't listen to him?"
    #dv "Ещё как слушалась. После того, как этот старый курильщик полыхнул у себя."
    dv "Of course I did, I had a great example. One day this old smoker burned his room in seconds."
#Not sure of the intent here; is she saying that he burned down the house?
    window hide
    show dv laugh pioneer  with dspr
    window show
    #me "Ужас какой."
    me "That's horrible."
    #dv "Ха-ха. Сочинила я всё. Хотя дед-то был. Но умер совсем не так."
    dv "Haha. Just a joke. {w}Well I did have a grandfather, but he died in another way."
    #me "...Утонул?"
    me "...Drowned?"
    show dv normal pioneer  
    #dv "Эй! Поуважительнее к моему деду!"
    dv "Hey! Show some respect!"
    #me "Ты сама только что... А что с ним случилось?"
    me "I'm sorry... What happened to him?"
    #dv "Ладно, расскажу. Когда его отправили на пенсию, он купил себе гроб, объявил, что будет умирать. Лёг в него... и ничего."
    dv "Fine, I'll tell you. When he retired, he bought a coffin and announced that he was going to die. He laid down in it... and nothing happened."
    #dv "Тогда он начал пить. А бутылки в гроб ставить. Со временем много бутылок скопилось, трогать их он запрещал. Шутил, что это моё приданое."
    dv "Then he started to drink, and would put the empty bottles into the coffin. After a while there were a lot of them, but he forbade anyone to touch them. He joked that they were my dowry."

    #dv "Один раз в дымину пьяный, заявил, что он йог, и попытался лечь на все эти стоящие бутылки. Неудачно упал, сломал ребро. Из больницы уже не вернулся. Вот так."
    dv "Once he got wasted and declared that he was a yogi and tried to lie on the standing bottles. He fell awkwardly and broke a rib. After that he didn't return from the hospital. That's the story."
    stop music fadeout 3 
    #dv "Слушай, а ты чего не в форме? Из партии исключили?" 
    dv "...Hey, why are you not in the uniform? Have you been expelled from the party?" 
    #me "Да я сам... Исключаюсь. Ухожу я вообще-то."
    me "I'm expelling myself. I'm leaving, in fact."
    play music Doubt_everyone fadein 5
    show dv guilty pioneer  with dspr
    #"Улыбка мигом исчезла с лица Алисы."
    "The smile instantly disappeared from Alice's face."
    #dv "А я тут болтаю... И куда же? Домой на попутках решил?"
    dv "And I'm blabbing here... And where? Hitchhiking back home?"
    #me "Вроде того."
    me "Something like that."
    #dv "Идея так себе. А почему уходишь?"
    dv "Not your greatest idea. Why are you leaving?"
    #me "Боюсь, не смогу объяснить. Не весело мне тут, и ничего с этим не поделать."
    me "I'm afraid I can't explain it. It's not fun to be here and nothing can be done about that."
    show dv normal pioneer  with dspr
    #dv "Кажется, я поняла, в чём дело."
    dv "I think I understand what's going on."
    #me "Вряд ли."
    me "Doubtable."
    #dv "Поспорим? На что? На то, что ты остаёшься?"
    dv "Want to bet on it? If I win, you stay here."
    #me "Я не останусь. Слушай, у меня мало времени."
    me "I won't stay here. Listen, I don't have much time."
    #dv "Наш Ромео слишком вошёл в образ, и теперь пытается сохранить честь пионера."
    dv "Our Romeo took his role too seriously and now is trying to save a pioneer's honor."
    if mer_cu == True:
        #me "Для начала, я играю не Ромео."
        me "For the start, I'm not Romeo."
        #dv "Какая разница. Ты понял, о чём я."
        dv "Whatever. You know what I mean."
    #me "Это не так. Впрочем, думай как хочешь. Ничего лучше я уже не придумаю. Я пойду."
    me "That's not true. However, think as you wish. I can't come up with anything better. I'm leaving."
    if sp_dv <= 5:
        show dv angry pioneer  with dspr
        #dv "Как знаешь. Вот продрогнешь в лесу-то, и сразу вернёшься. Или тебя вернут."
        dv "As you wish. When you freeze in the forest, then you will return. Or will be returned."
        #me "Пока."
        me "Bye."
        #dv "Дурак."
        dv "Idiot."
        #"Я пожал плечами, и двинулся прочь из лагеря."
        "I shrugged and walked away from the camp."
    else:
        show dv angry pioneer close  with dspr
        #"Алиса схватила меня за руку."
        "Alisa grabbed my hand."
        #dv "Ты должен мне всё рассказать. Иначе не отпущу."
        dv "Tell me everything, or I won't let you go."
        #me "Времени нет. Зачем тебе это? Пусти."
        me "There's no time left. Why are you even insisting? Let me go."
        #dv "Не пущу."
        dv "No."
        #me "Не будем же мы драться?"
        me "We aren't going to fight, are we?"
        #dv "Будем!" with vpunch
        dv "We are!" with vpunch
        play sound sfx_bush_body_fall
        show dv cry pioneer  with dspr
        #"Я всё-таки высвободился, и легонько толкнул Алису в ответ на новый выпад. Она упала в траву и всхлипнула."
        "I readied myself and gently pushed Alice in response to her attack. She fell into the grass and sobbed."
        #th "Жалко её, но нужно срочно уходить. А то ещё подмогу позовёт."
        th "I feel sorry for her, but I have to leave. If I wait she could get some help."
        #me "Пока. Извини, что так вышло."
        me "Goodbye. I'm sorry about all of this."
    window hide 
    scene bg ext_road_night2  with dissolve
    #"Очень хотелось оглянуться, но нельзя. Теперь только вперёд."
    "I really wanted to look back, but I couldn't. Now I could only go forward."
    window hide
    $ renpy.pause(1)    
    $ set_mode_nvl()
    $ renpy.pause(0.5)  
    #"Итак, я всё-таки сбежал от «Совёнка», в лице его последней представительницы. Что же ждёт меня впереди? Если подумать..."
    "Just like that, I ran away from \"Sovyonok\". What awaited me? In fact..."
    play sound Record_Scratch

    $ set_mode_adv()
    play ambience ambience_ext_road_night
    play music music_list["gentle_predator"] fadein 2
    #dv "Стоять!"
    dv "Freeze!"
    show dv angry pioneer  with dspr
    #me "Ну что ещё?"
    me "What now?"
    if sp_dv >= 6:
        play sound sfx_face_slap
        #dv "Для начала - вот это!" with vpunch
        dv "This, for starters!" with vpunch
        #me "Окей, квиты теперь. Что-то ещё?"
        me "Okay, now that that's over... Anything else?"
        #dv "Да."
        dv "Yes."
    hide dv  with dspr
    #"Алиса с невозмутимым видом пошла рядом."
    "Alisa calmly walked along with me."
    #me "Со мной решила?"
    me "Leaving with me?"
    #dv "Ещё чего. Просто провожу. Или ты против?"
    dv "Of course not. I'm just keeping you company. Or are you against it?"
    #me "Эээ... Ну провожай."
    me "Ummm... Okay then."
    #"Мы пошли вперёд. А куда идём? У меня и плана особо нет, даже как переночевать. Нужно избавиться от Алисы раньше, чем она это поймёт."
    "We moved forward. But where to? I didn't even have a plan on how to spend this night. I needed to get rid of Alisa before she could figure that out."
    #"Какое-то время мы шли молча и соблюдая дистанцию."
    "For a while, we walked in silence and kept some distance from one another."

    #dv "Холодно становится."
    dv "It's getting colder."
    #me "Держи куртку. А вообще, тебе возвращаться пора."
    me "Take my jacket. In fact, it's time for you to go back."
    #dv "А ты всю ночь идти решил?"
    dv "You're going to spend the whole night walking?"
    #me "Нет..."
    me "No..."
    #dv "Ну так давай тут остановимся."
    stop music fadeout 7    
    dv "Let's take a break then."
    #"Я согласился, место было неплохое. Мы собрали веток и разожгли костёр. Первый-то я пропустил сегодня, но всё-таки костру быть."
    "I agreed, it was a good place. We gathered some twigs and lit a fire."
    window hide
    $ renpy.pause (0.4)
    play sound sfx_forest_fireplace
    scene cg d7_dv with dissolve
    $ renpy.pause(1)
    me "So... We have a bonfire, after all."    
    #dv "Я соврала, если ты не понял."
    dv "...I lied, if you didn't notice."
    #me "А?"
    me "Eh?"
    #dv "Что не люблю огонь. Люблю, конечно... {w}Ну, рассказывай."
    dv "That I dislike fire... Of course I love it. {w}Well, I'm listening."
    #me "Что?"
    me "Listening for what?"
    play music music_list["waltz_of_doubts"] fadein 5
    #dv "Чего там у тебя за проблема такая."
    dv "Tell me about your big problem."
    #me "Ладно, раз тебе так интересно. В общих чертах... Веришь в предсказания? Ванга там... Слыхала?"
    me "Oh. Alright, if you're really that curious..."
    dv "I'm not. But I'd rather hear it right now, getting tired of this miserable face of yours." 
    me "Whatever. Speaking generally... Do you believe in predictions? Vanga or anything?"
    #dv "Что-то слыхала. Но я в такую чушь не верю."
    dv "I've heard some things, but I don't believe in them."
    #me "А я вот верю, так уж получилось. И я знаю, что кое с кем в лагере должно случиться что-то плохое."
    me "But I do. And I know something bad will happen to someone in the camp."
    #dv "Мы сейчас страшилки у костра рассказываем?"
    dv "Are we telling campfire stories?"
    #me "К сожалению, нет."
    me "Unfortunately, no."
    #dv "Вот теперь мне страшно. Ну и с кем должно случиться? С Леной?"
    dv "Now that's creepy. Who are you talking about? Lena?"
    #me "С Самантой. Когда она уедет."
    me "Samantha."
    dv "Hm? You think she's not safe here?"
    me "Quite opposite, I'm afraid it will happen when she leave. Maybe on the way home..."
    #dv "Так. Ну и с чего ты решил?"
    dv "Why do you think so?"
    #me "Не смогу объяснить. Но переубеждать меня бесполезно, и не пытайся. Я в это верю больше, чем в то, что мы тут с тобой сидим."
    me "I can't explain it. But it's useless to persuade me it's not true, so don't even try. I believe in it more than the fact that I'm sitting here with you."
    #dv "Будто мы каждый день сидим. Я вот не знаю, что я тут делаю. С сумасшедшим!"
    dv "Like {i}that{/i} happens a lot. I have no idea what I'm doing here - with a nutcase!"
    #me "И я не знаю."
    me "Me neither."
    #dv "Да ладно тебе. Я увидела, что ты не в себе. Нельзя было тебя отпускать."
    dv "Come on. I saw that you weren't okay. I couldn't just let you leave."
    #me "Если так, то я и завтра буду не в себе, и послезавтра. Проблема не во мне, на самом деле."
    me "Then, I won't be okay tomorrow or the day after, either. The thing is, the problem isn't with me."
    #dv "Так надо решить её. А ты что делаешь? Сбегаешь?"
    dv "So you have to do something about it. And yet, what are you doing? Running away?"
    #me "Не знаю, как решить. Не хочу никого расстраивать, вот и сбегаю."
    me "I don't know what to do, I am useless... I just upset people here. I'll run away and put a stop to it..." 
    #dv "А если ты прав окажешься? Как ты дальше простишь себе, что сбежал? Плохо ему было, видите ли."
    dv "What if something bad really happens? You're ready to fail her without even trying? Just because you feel bad?" #@MARK
#Not sure if intention was to have her somewhat mock him in the last sentence.
    #me "Может, ты и права. Но что ты предлагаешь?"
    me "Maybe I'm wrong. But what do you suggest?"
    #dv "Вернёмся в лагерь. Дай мне сутки, я что-нибудь придумаю."
    dv "Let's go back to the camp. Give me a day and I'll figure something out."
    #me "Но я, как бы, сбежал."
    me "But... I've kinda already ran away."
    #dv "Если завтра не передумаешь - я сама тебе вещей соберу."
    dv "If you don't change your mind tomorrow - I'll gather a backpack for you myself."
    #dv "Уж я-то в побегах понимаю. А этот твой побег - просто жалок!"
    dv "I know how to prepare for a runaway, unlike you - this is pathetic!"
    #me "Знаешь, что? Я сдаюсь. Возвращаемся."
    me "Know what? I give up. Let's go back."
    #dv "Так быстро сдался? Я думала, ещё посидим."
    dv "Wow, that fast? Just when I thought we could sit here some more."
    #me "Завтра посидим. Надо скорее вернуться, а то попадёт нам. И оправдания лучше придумать заранее."
    me "We can do it tomorrow. Now we should get back before they'll start searching for us. Maybe we should even come up with good excuses in advance."
    dv "Excuses? Ha-ha. Like I need them."
    window hide
    $ renpy.pause (1)
    stop sound fadeout 2
    scene bg ext_camp_entrance_night with fade2
    show dv smile pioneer with dissolve
    #me "Спасибо тебе."
    me "Thank you."
    #dv "Обращайся."
    dv "My pleasure."
    #me "Пока?"
    me "Bye?"
    #dv "Ты ведь не побежишь обратно, когда я уйду?"
    dv "You won't run away when I leave, right?"
    #me "Нет, набегался уже сегодня."
    me "No, I've done enough running for today."
    if kiss_dv == True:
        #th "Наверное, можно поцеловать её? Был неудачный опыт, но в щёчку-то можно."
        th "Maybe I should kiss her? This wasn't a good experience, but I think I can at least kiss her on the cheek."
        show dv surprise pioneer close  with dspr
        play sound sfx_face_slap
        $ renpy.pause(0.5)      
    else:
        #dv "Тогда бывай."
        dv "Bye then."
        $ renpy.pause(0.9)      
    scene black  with fade
    $ renpy.pause(2)
    scene bg ext_square_night with fade
    $ renpy.pause(0.5)      
    $ set_mode_nvl()
    window show
    #"Иногда, когда сил уже не осталось - нужен кто-то, кто скажет: \"не волнуйся, я обо всем позабочусь\". Как хорошо, что Алиса оказалась рядом."
    "Sometimes, when you've lost hope - you need someone who'll say: \"Don't worry, I'll take care of it\". Good thing that Alisa was around."
    #"Не знаю, что будет дальше, но никуда уходить я точно не хочу. Как я мог вообще подумать об этом?"
    "I had no idea what would come next but I definitely wasn't leaving. How could I even think about that?"
    #"Все здешние проблемы - пустяки по сравнению с кошмаром, в котором я окажусь там - один, и не способный ничего изменить."
    "All local problems - nothing compared to the nightmare in which I'd find myself there - alone, not able to change a thing. "
    #"Вместе мы что-нибудь придумаем. Алиса меня не бросила, и я никого не брошу. Я остаюсь. И это звучит гордо."
    "We'd figure that out. Alisa didn't leave me, and I didn't leave anyone. I was staying. It sounded good to me."
    $ renpy.pause(0.5)
    $ set_mode_adv()    
    window hide
    stop music fadeout 4
    $ renpy.pause(1)
    scene black with fade
    jump ss_notready_eng
#    jump ss_d5_eng



label un_runaway_eng:

    scene bg ext_path_night  with dissolve
    play music music_list["into_the_unknown"] fadein 5
    #"Мы без проблем добежали до лагеря. Не все взрослые были в походе, поэтому надо было сохранять осторожность."
    "We reached the camp without any problems. Some adults were still around, so it was necessary to remain cautious."
    scene bg int_house_of_mt_night  with fade
    play ambience ambience_int_cabin_night
    #"Я зашёл к себе в домик и быстро переоделся в одежду, в которой приехал. Сейчас она уже казалось чужой, а форма - такой родной."
    "I went to my cabin and quickly changed into the clothes I'd arrived in. They looked so strange now; the uniform had become so familiar to me."
    #"Вещей у меня особо не было, я захватил заранее приготовленную бутылку с водой и всю еду, которую нашёл. Ольга Дмитриевна не умрёт с голоду."
    "I didn't have many things, so I took the prepared bottle of water and all the food I could find. Olga Dmitrievna wouldn't die from starvation."
    #"Может, оставить записку? И что написать? \"Не ищите меня\"? Или \"ищите, но без особого рвения\"? Подумав, я написал \"Простите за Лену\". А теперь - к ней."
    th "Maybe I should leave a note? Something like... \"don't search for me\"? {w}Or \"search for me, but don't try too hard\"?"
    "Finally I wrote \"sorry about Lena\". After that I went for her."
    window hide
    $ renpy.pause (0.7)
    scene bg int_house_of_un_night  with dissolve
    play sound sfx_knock_door7_polite    
    $ renpy.pause (1.3)
    show un smile pioneer  with dspr
    #un "Заходи."
    un "Come in."
    #me "Ты в порядке?"
    me "You alright?"
    #un "Конечно. А что может случиться?"
    un "Of course. Why shouldn't I be?.."
    #me "Да так. Готова?"
    me "Nevermind. Are you ready?"
    #un "Нет."
    un "No."
    hide un  with dissolve
    #"Ждать пришлось долго, а мои попытки помочь собраться, похоже, только мешали делу."
    "I had to wait for a while, and all of my attempts to help just made it worse."
    window hide
    $ renpy.pause (0.5)    
    scene bg ext_house_of_mt_night_without_light  with dissolve
    play ambience ambience_camp_center_night
    $ renpy.pause (0.5)    
    #"Когда Лена всё-таки была готова, на улице всё ещё было тихо. Оставалось только выскользнуть из лагеря."
    "When Lena was finally ready, it was still quiet outside. The only thing we needed was to slip out the camp."
    show un angry2 pioneer at cright 
    show dv normal pioneer far at cleft   with dspr
    show dv normal pioneer at cleft   with dspr

    #dv "Попались!"
    dv "Got you!"
    #me "Ты чего не в походе?"
    me "Hey... Why are you not camping?"
    #dv "Ненавижу походы. А вам, я вижу, так понравилось, что решили повторить?"
    dv "I hate camping. How about you?"
    me "Me?.."
    dv "Both of you. I see you like it so much that you decided to head out again?"
    #me "Похоже на то."
    me "...Something like that."
    #dv "Ну и ну... Сбегаете, значит?"
    dv "Are you running away? Seriously?"
    #me "Сбегаем."
    "We exchanged glances with Lena. Maybe she wasn't happy about my honest answer, but I felt that there is no point to lie."
    me "Yes."
    if sp_dv >= 5:
        show dv sad pioneer at cleft   with dspr
        #"Алиса прикусила губу. Мне стало неуютно от её взгляда."
        "Alisa bit her lip. I felt uncomfortable under her eyes."
        #dv "Ну, удачи вам."
        dv "Well, good luck then."
        hide dv  with dspr
        #"Алиса быстро ушла, я даже не успел попрощаться. Разговор продолжали уже на ходу."
        "Alisa quickly left, not giving me a chance to say goodbye."
        hide un with dissolve
        "We moved on, keeping a conversation while walking."
        #me "Чего это с ней? Она же не пошлёт за нами погоню с собаками?"
        me "What's wrong with her? Is she going to chase us with dogs?"
        show un serious pioneer at cright   with dspr
        #un "Думаю, тебе лучше знать."
        un "You should know better."
        #me "В каком смысле? Не важно, мы с ней больше не увидимся."
        me "What do you mean?.. {w}Doesn't matter; I won't be seeing her again."
        #un "Разве? Я думала, там, куда мы бежим, на это есть все шансы."
        un "Really? I think we'll have plenty of chances for that, where we're going."
        #me "И правда. Совсем забыл, что вы соседки."
        me "Right. I forgot that you're neigbours."
        scene bg ext_camp_entrance_night  with dissolve
        #"Лена остановилась."
        "Lena stopped."
    else:
        #dv "Ты делаешь большую ошибку."
        dv "You're making a big mistake."
        #"Она обращалась к Лене."
        "She said this to Lena."
        #un "А ты ревнуешь."
        un "And you're jealous."
        show dv angry pioneer at cleft   with dspr
        #dv "Нет. Я всегда пыталась защитить тебя, даже после всего. И сейчас должна это сказать, хотя мне должно быть наплевать."
        dv "No. I've always tried to protect you, even after everything. Now I have to say it, even though I shouldn't care."
        #un "Не устраивай сцену при нём."
        un "Don't start this in front of him."
        #dv "Пусть слышит. И пусть скажет, если я не права. Это закончится для тебя кошмаром."
        dv "Let him hear it. And let him decide if I'm wrong. What you started here - it will be a nightmare for you."
        #me "Не исключено."
        me "That is possible."
        #un "Оставь нас в покое, я тебя прошу."
        un "Just leave us, I'm begging you."
        #dv "Оставляю, мне и так больше нечего сказать. {w}Не прощаюсь. Мир тесен."
        dv "I'm leaving. I have nothing else to say. {w}I'm not saying goodbye. The world is small."
        window hide
        $ renpy.pause(0.5)
        hide dv with dissolve
        $ renpy.pause (1)
        scene bg ext_camp_entrance_night  with dissolve
        #"Мы прошли несколько шагов по направлению к воротам, но Лена снова остановилась."
        "We walked a few steps toward the gate, but Lena stopped again."
    show un serious pioneer at cright   with dspr
    #un "Слушай. Если ты хотел бы, чтобы на моём месте была она. Или кто-то. Или сомневаешься. Думаю, самое время сообщить. Я просто вернусь."
    un "Listen. If you would rather be with her instead of me... Or anyone else..."
    me "Huh? What made you think that?"
    un "Maybe you have doubts... I think it's the right time to tell me. I'll just go back."
    un "Don't answer too fast, please. It's important."    
    stop music fadeout 4 
    #th "Чего я хотел бы? Возможно, следует удержать Лену от побега, даже если и придётся соврать. Она бы пошла со мной, и уже это греет душу."
    th "Alright, she want me to think about it... So what do I want?"
    th "Perhaps I must ask Lena to stay, even if I have to lie. It might be better for her."
    th "She is ready to follow me, that fact is heartwarming enough... {w}Is it?"
    menu:

        #"Оставайся":
        "I'll go alone":
            play music music_list["confession_oboe"]  fadein 5
            show un sad pioneer at cright   with dspr
            #me "Тебе нужно остаться."
            me "You need to stay here."
            #un "Ясно. Знаешь, я бы пошла с тобой. В первый же день пошла бы. Вот так, да."
            un "I see..."
            me "I'm sorry."
            un "You know, I would have gone with you. On the first day. Just like this."
            #me "Глупости. И даже не вздумай жалеть об этом. Такие вещи проходят только в книжках, да не каждый раз. Мы в другом мире. Правда, я всё ещё не знаю, в каком..."
            me "Nonsense. And don't you dare feel sorry about it. Such things can happen only in books, and not even every time. We're in another world. I just don't know in what one..."
            #me "...Но не в таком. Тут есть и горечь, и ответственность. И нельзя просто взять и уйти в закат."
            me "...But it's not a book's world. It's a bitter world, where we have the responsibilities. We can't just walk away into the sunset."
            show un cry pioneer at cright   with dspr
            #un "Но ведь можно. Всё можно. Просто никто не пытается. Я бы пошла с тобой даже зная, что ты {i}её{/i} любишь. Если бы ты мне позволил."
            un "But we can. Everything is possible. It's just that no one ever tried. I would have gone with you even if you told me that you love {i}her{/i}. If you'd only allow me."
            # un "Но ты прав кое в чём. Ответственность. Ты заботишься обо мне, а я... Я не должна так поступать с тобой. Поэтому я остаюсь."
            un "But you're right about something. Responsibility. You care about me, and I... I shouldn't do this to you. That's why I'm staying."
            #"Лена плакала. Я попытался улыбнуться."
            "Lena was crying. I tried to smile."
            #me "Не грусти. У тебя здесь отличные подруги. Мику, Саманта... Алиса. Обсудите с ней, какой я никчёмный. Но всё-таки не настолько, чтобы тащить тебя с собой. А у меня... У меня есть вы все."
            me "Don't be sad. You have great friends here. Miku, Samantha... Alisa. You can tell her about how worthless I am; but still not enough to drag you with me. And I... I have all of you."
            show un sad pioneer close  with dspr
            #"Мы обнялись в последний раз. Все слова были сказаны, все слова никогда не будут сказаны. Я ушёл, так и не сказав \"прощай\". Кто знает?"
            "I hugged her one last time. All words had been said; all words never can be told. I left without saying \"goodbye\". Who knows?"
            window hide
            $ renpy.pause(1.5)
            scene bg ext_road_night2  with fade2
            $ set_mode_nvl()
            window show
            $ renpy.pause(0.5)
            #"И всё-таки ухожу. Впереди темнота, во всех смыслах. Иногда это не худший вариант."
            "And yet I'm leaving. The only thing ahead is the darkness. Sometimes it's not the worst choice."
            #"Темнота. И почему её так не любят, связывают со злом? Она всегда рада людям, окутывает их по первому желанию. И всех любит одинаково, в темноте все равны. А свет придирчив и непостоянен."
            "Darkness. Why is it that nobody loves it and relates it to evil? It always welcomes any person, it covers and protects them as they want."
            "It loves everyone equally, and everyone is equal in the dark. While light is picky and inconsistent."
            #"Здесь темнота всё стирает, а потом возвращается свет, и появляется что-то новое. Или старое. Мне нужно новое. Проснуться в холодном лесу не очень хочется, я рассчитываю на другое."
            "In this world, darkness erases everything, and then the light can return and bring something new. Or old."
            nvl clear
            "I need new this time. Waking up in a cold forest isn't what I want. I'm counting on something else."
            #"В любом случае, темнота мне будет всегда доступна. Та или другая. Она лучше боли."
            "In any case, the darkness will always be available. One way or another. It's better than pain."
            $ renpy.pause (1)

            $ set_mode_adv()
            window hide
            scene black  with fade2
            $ renpy.pause(1)
            $ set_mode_nvl()
            window show
            play sound sfx_achievement
            #"Концовка: темнота."
            "Ending: The Darkness."
            window hide
            $ set_mode_adv()
            $ renpy.pause(1)
            menu:
                #"В меню":
                "Menu":
                    return
                #"В начало":
                "To the beginning":
                    jump sam
                #"Ещё модов!":
                "More mods!":
                    play music BBtheme
                    window hide
                    show cg bbsov 
                    $ renpy.pause(20)
                    scene black  with fade
                    #"Мод пройден"
                    "Mod is completed"
                    show cg bbsov 
                    stop music fadeout 9
                    menu:
                        #"В меню":
                          "Menu":
                            jump sam
                        #"Выход":
                          "Exit":
                            return



        #"Идём":
        "Let's go":
            play music music_list["a_promise_from_distant_days_v2"] fadein 5
            show un sad pioneer at cright   with dspr
            #me "По хорошему-то, конечно, тебе нужно вернуться."
            me "To be fair, you need to return. Otherwise it will be a big mistake."
            #"Лена поникла."
            "Lena wilted."
            #me "Но на твоём месте не может быть никого. И не будет."
            me "But I'd love to make this mistake. Then I go, there can't be anyone instead of you. Not with me."
            show un smile3 pioneer at cright   with dspr
            show un smile3 pioneer close  with dspr
            #"Лена вцепилась в меня и прижалась так сильно, что стало больно. А моему плечу - ещё и мокро."
            "Lena clung to me, pressing so tightly against me that it hurt. My shoulder became wet from her tears."
            window hide             
            $ renpy.pause(0.7)      
            scene bg ext_road_night2  with dissolve2 
            $ renpy.pause(0.7) 
            $ set_mode_nvl()
            window show
            #"И вот мы сбежали. Получится ли у нас? Будем ли мы завтра вместе? Если сможем, то будем. Если же нас разделит непреодолимая сила... Что ж, ей придётся быть действительно непреодолимой."
            "So we ran away."
            "Could we handle it? Would we be together tomorrow? {w}We would, if it possible."
            "And if some force are going to divide us... Well, this force has to be really, really strong. Unstoppable."
            #"Проблема Саманты осталась, но я был не один. Этот грустный день закончился почти счастливо. Или совсем счастливо. Полагаю, я нашёл ответ?"
            nvl clear
            "The Samantha problem remained, but not all problems can be solved. Sometimes you just close your eyes and ignore it - perhaps this is all that remains for me." 
            "I didn't find the answer, but at least I wasn't alone at the end. This sad day had ended almost happily. Or even better."
            #"И пусть это равносильно тому, чтобы закрыть глаза и игнорировать проблему - возможно, это всё, что мне остается. Не все проблемы можно победить."
            nvl clear
            # "Что будет, если завтра я окажусь в своей комнате? Потеряю ли я {i}всё{/i}? Пожалуй, нет. Всё-таки, чего-то я тут и добился. И часть груза, который со мной разделила Лена, уже не окажется на моих плечах."
            "I know, together we can bear it. But what will happen if I appear in my room tomorrow? Will I lose {i}everything{/i}?"
            "Probably not. I'd certainly achieved something. At least I think so now."
            #"Пожалуй, я смогу пережить и это. Сейчас мне так кажется. Ведь я не потеряю её полностью. Где-то там будет Лена. Моя Лена."
            "It feels like I've already shared a lot with Lena. Whatever happens, I won't lose her completely."
            "Somewhere out there will be always Lena. My Lena."
            #"Но возможен и лучший исход."
            "But I hope for something closer than \"somewhere\". {w}I hope for something better."
            $ renpy.pause(1.6)              
            $ set_mode_adv()
            window hide
            stop music fadeout 5            
            $ renpy.pause(1.6)  
            scene black with fade2
            $ set_mode_nvl()
            window show
            play sound sfx_achievement
            #"Концовка: в закат."
            "Ending: into the sunset."
            $ set_mode_adv()
            $ renpy.pause(1)
            menu:
                #"В меню":
                "Menu":
                    return
                #"В начало":
                "To the beginning":
                    jump sam
                #"Ещё модов!":
                "More mods!":
                    play music BBtheme
                    window hide
                    show cg bbsov 
                    $ renpy.pause(20)
                    scene black  with fade
                    #"Мод пройден"
                    "Mod is completed"
                    show cg bbsov 
                    menu:
                        #"В меню":
                          "Menu":
                            jump sam
                        #"Выход":
                          "Exit":
                            return





    return



label ss_d4main_eng:
    scene bg ext_polyana_day  with dissolve
    play ambience ambience_forest_day
    #"А вот и знакомая поляна, на которой довелось повисеть вчера утром. Верёвка на месте. Может пригодиться... Я прислонился спиной к дереву."
    th "And here is the familiar field. The rope is still here... It could become useful."
    "I leaned on the tree."
    #th "Что же случилось с этим лагерем? Всё шло так хорошо. Вчерашний день устроил мне встряску, но сегодня я проснулся в настоящем кошмаре. За что?"
    th "...What happened to this camp? Everything used to be fine. {w}Okay, yesterday was bad enough, but today I woke up in a true nightmare."
    #th "«Совёнок» был хрупким приятным сном, о котором нельзя задумываться, чтобы не проснуться, и не разрушить его."
    th "The camp used to be a fragile and pleasant dream... The one you couldn't think about without waking up and destroying it."
    #th "Я не задавал вопросов, пока всё шло по-моему. Но сейчас мне нужны объяснения. Я не знаю, что делать."
    th "I didn't ask questions when it was good for me... But now I don't know what to do. {w}I demand explanations."
    stop music fadeout 5
    #me "Мне нужны ответы, слышите!"
    me "I need answers, do you hear me?!" with vpunch
    window hide
    $ renpy.pause(1.5)
    stop ambience fadeout 5    
    voice "Yes, we do hear you."
    window hide
    play music music_list["no_tresspassing"] fadein 5    
    show sspi normal at center with dissolve
    #pi "Слышим, как же не слышать."
    #th "Что за пионер? Откуда тут взялся?!"
    th "What? A pioneer? Where did he come from?!"
    #me "Ты кто?"
    me "Who are you?"
    #pi "Не помнишь меня, значит. Вспомнишь."
    pi "What a pity. Seems you don't remember me. Well, you will."
    #me "Что-то никак. Ты из лагеря?"
    me "Something is wrong. Are you from the camp?"
    #pi "Как всё запущено. И да, и нет. Когда-то я был таким как ты."
    pi "How do I begin... Yes and no. I used to be just like you."
    #me "Это ещё что значит?"
    me "What does that mean, exactly?"
    #pi "Да брось, ты уже не тот овощ, которым я привык тебя видеть. Ты же позвал меня."
    pi "Come on, you're not the bonehead that I knew before. You called me, after all."
    #me "Тебя? И кто же ты? Deus Ex Machina с ответами?"
    me "You? And who are you? {w}Deus Ex Machina with all answers?"
    #pi "Довольно глупо давать ответы парню, страдающему амнезией. Хотя какие страдания - ты ей наслаждаешься. Ну да ладно."
    pi "It's kind of pointless to give answers to a guy suffering from amnesia."
    me "Am I suffering from it?"
    pi "Well, I suppose you're enjoying it. But that's not the point."
    #me "О чём речь? Что именно я забыл?"
    me "What are you talking about? What did I forget exactly?"
    #pi "Свои циклы. Каждый раз как первый. Я уж думал, ты совсем куклой стал. Как они."
    pi "Your cycles of course. Each one feels like the first for you. I even thought you were a doll. Just like them."
    #me "Хочешь сказать, что история повторяется?"
    me "My circles?.. Are you saying that history repeats itself?"
    pi "Not really that. This conversation is happening for the first time, but we've had others."
    #th "Пожалуй, я правда его уже встречал. Но значит ли это, что он говорит правду?"
    th "I have a feeling that I met him before. But does that really mean he's telling the truth?"
    #pi "Не было... А может и был? Кто знает. О, у меня была уйма времени подумать обо всём этом."
    pi "The first... Or maybe not? Who knows? I've had plenty of time to think about it."
    #me "Ну и о чем мы говорили? Напомни."
    me "Can you remind me of what we were talking about?"
    #pi "Я сразу честно тебе рассказал, куда ты попал. Но тут мы не сошлись во мнениях. Ты даже назвал это место раем."
    pi "I told you everything about this place. But we had different opinions about it. You even called this place a heaven."
    #pi "Это меня немного разозлило. И я... эээ... помог тебе взглянуть на вещи по-своему."
    pi "I got a little mad because of that and... helped you to see it from my point of view."
    #me "Как же это?"
    me "How?"
    #pi "Ну... С твоими любимыми куклами происходили... вещи."
    pi "Well.. Some... things happened to your favorite dolls."
    show sspi smile at center 
    #me "Вещи?"
    me "Things?"
    #pi "Которые ты  никак не смог бы ассоциировать с раем. Скорее уж с адом."
    pi "Things you wouldn't associate with heaven. Rather with hell."
    #pi "Ну, чего насупился? Сейчас-то они все на месте."
    pi "Why are you frowning? They're all in one piece now."
    #me "Почему куклы?"
    me "Why do you call them dolls?"
    pi "What are they, if not?"
    #me "Личности. Ты говоришь, что я был как они. А ты был как я. Ну и чем они хуже тебя? Чем не настоящие?"
    me "People... Confused people?"
    show sspi normal at center 
    pi "Not a chance."    
    me "...You said I was just like them. And you were like me. So are they worse than you?"
    show sspi smile at center     
    pi "Oh, no, I am the worst! But I'm not a doll. They are stupid and predictable."
    me "Yet they may be real..."
    stop music fadeout 3
    #pi "Настоящие? Ха! Давай прикинем, как оно было бы на самом деле."
    pi "Real? Ha! Let's check out how it would be in real life."
    window hide
    $ renpy.pause(0.7)
    
    scene bg ext_camp_entrance_day  
    show sl serious pioneer 
    with fade2
    play ambience ambience_camp_entrance_day 
    $ renpy.pause (1)
    #th "Что? Как? Куда он меня закинул?"
    th "What? How?.. How did he bring me here?"
    #th "Так, ладно. Славя. Похоже, меня не узнаёт."
    th "Fine, fine... Perhaps it's not real, but let's evaluate the situation."
    th "Looks like Slavya doesn't recognise me. It must be the day when I arrived."
    #th "Неохота играть в эти игры, но что ещё предпринять? Начать скандалить - это то, чего он ждёт."
    th "Don't want to play these games, but what can I do? Starting a scandal is the only thing he's waiting for."
    me "Uh... Hello?"
    #"Славя смерила меня взглядом, но не ответила. По крайней мере, я не невидимый."
    "Slavya looked at me, but didn't answer. At least I knew that I'm not invisible."
    #me "Это же лагерь Совё..."
    me "Is this Sov..."
    #sl "Проходии. Ты мне солнце загораживаешь."
    sl "Who are you talking to? Me?"
    me "...Sorry. I am new here."
    sl "Do I care?"    
    #th "Кое-что уже понятно. Что же, не буду загораживать. Куда же Славя без знаменитого загара."
    th "Well, something is getting clear."
    sl "Move on. You're blocking the sun."
    th "...Won't block it then. Can't imagine Slavya without her famous tan."
    window hide
    $ renpy.pause (0.5)
    scene bg ext_clubs_day  with dissolve
    #"Я продолжил исследовать лагерь..."
    "I kept exploring the camp..."
    window hide
    show un normal pioneer far at left   with dspr
    $ renpy.pause(0.5)
    show un normal pioneer at left   with dspr  
    window show
    #"И возле клубов наткнулся на Лену. Она меня тоже не узнала."
    "And stumbled upon Lena near the clubs. She didn't recognise me either."
    #me "Девушка, вы очень милы. Можно пригласить Вас?"
    me "You're such a pretty thing, young girl. May I invite you?"
    show un surprise pioneer at left     
    #th "Ну а что? Столько разговоров про любовь с первого взгляда, пора бы и проверить. Ради науки."
    th "Why not? So many talk about \"love at first sight\", someone should check it once and for all. In the name of science."
    un "...Invite me? Where?"
    #me "На танец. Или погулять. Да хоть на лодке покататься."
    me "Dancing, walking... How about boating?"
    hide un  with dspr
    #"Лена в ужасе метнулась обратно в клуб. \"Псих\" - послышалось из-за двери."
    "Lena rushed in terror back to the club. \"Crazy\" - could be heard from behind the door."
    #th "Стоп, нельзя так себя вести. Но ведь и лагерь какой-то неправильный. Ладно, попробую поговорить ещё с кем-нибудь."
    th "I think I should stop. This isn't right."
    th "...But this camp isn't right as well. Okay, I'll try to speak with someone else."
    window hide
    $ renpy.pause(0.5)    
    scene bg ext_houses_day  with dissolve    
    play music music_list["heather"] fadein 3
    $ renpy.pause(0.5)    
    #dv "В пальтишке не зябнешь, гражданочка?"
    dv "The weather's not hot enough for you?"
    show dv normal pioneer2 far at center     with dissolve      
    show dv normal pioneer2 at center     with dspr
    #th "И правда, я же опять в куртке."
    th "What? Oh, right. I'm in my jacket again."
    show dv smile pioneer2 at center     
    #dv "Ну что, такой молодой, а уже бомж?"
    dv "So young and yet a tramp?"
    #th "Есть надежда, что хотя бы Алиса в порядке."
    th "At least Alisa might be alright."
    show dv normal pioneer2 at center     
    dv "Are you mute? Disabled?"
    me "Not really."
    dv "Have a smoke?"
    me "Nope."
    #dv "Не брешешь? Всё равно Гвоздь отнимет."
    dv "You sure? Don't hide it, Brick will get it anyway."
    #me "Какой Гвоздь?"
    me "I don't... Wait, who again?"
    #dv "Да тут один. Козлина. Думает, если... То мне и поболтать теперь ни с кем нельзя. Шурику зуб выбил."
    dv "Someone. An idiot, to be exact."
    dv "He thinks that if I... Then I shouldn't even speak to anyone. {w}Knocked out Shurik's tooth!"
    #dv "Шурику! Ну какой может быть Шурик, а? Да ты не знаешь Шурика..."
    dv "Shurik and me - can you imagine? What a crazy thought!"
    dv "Eh, you probably don't know Shurik anyway..."
    #"Алиса махнула рукой."
    "Alisa waved her hand."
    #me "Знаю Шурика. Немного."
    me "No, actually I know him... A bit."
    #dv "Откуда? Видел его?"
    dv "Really? Where did you see him?"
    #me "...Бывал с ним в одном лагере."
    me "Well... We were in the camp together. Once or twice..."
    #dv "Как будто один Шурик на весь Союз."
    dv "How do you know who I'm talking about? Like there is only one Shurik in the world."
    #me "Не один, конечно. Но твоему Шурику зуб выбили. А у моего как раз зубы были. Всё сходится."
    me "Not one of course. But you said that your Shurik lost a tooth."
    dv "Oh. So your Shurik missing a tooth?"    
    me "No. But mine certainly had some tooth to lose. Logic!"
    "Slightly giggled, Alisa looked behind me."
    # dv "Так. Ты меня не видел."
    dv "Ok. You haven't seen me."
## Had a lot of trouble with this entire exchange with Alisa. Still not sure on most of it.    
    hide dv  with dspr
    stop music fadeout 3
    #"Алиса исчезла в кустах. Я обернулся - по дорожке ко мне приближалась Ульянка."
    "Alisa disappeared into the bushes. I turned around - Ulyana approached me."
    show us grin sport far  with dspr
    show us grin sport  with dspr
    play music music_list["i_want_to_play"] fadein 2
    #us "Физкульт-привет! Я - Магдалена! А тебя как звать?"
    us "Howdy! Name's Magdalena! And what's your name?"
    me "Well... Ulyan."
    show us normal sport  with dspr  
    us "What?"
    me "That's my name. Ulyan."
    us "What's the name? {w}Oh, I got it. Is that even a name?"
    #me "Есть, конечно. А хоть бы Ленин? Ульянов."
    me "Of course it is. Do you like it?"
    us "I suppose, but I never heard of it..."
    me "Can't be. You sure know Lenin's real name - Ulyanov."
    us "That's a surname!"
    #me "Так Ульянов - значит, сын Ульяна."
    me "Yes, but Ulyanov means Ulyan's son."
    show us grin sport    
    #us "Или Ульяны."   
    us "Or Ulyana's."
    #me "Ну, нет! Много ты знаешь Машиных-Наташиных?"
    me "No, it doesn't work that way! Surnames are made from father's name."    
    show us smile sport  
    # us "Знаю одного Маринина."
    us "What about Marinin? This surname means \"son of Marina\"!"
    me "...Fine. There was one sissy boy."
    us "Lots of them!"
    me "Or a lot of them, but you can't imply that Lenin was something like that..."
    us "Lenin, haha! Lena's son!"
    "..."    
    # me "Ладно, был какой-то маменькин сынок, но ты же не скажешь, что и Ленин из таких... Черт. Ленин."
    #us "Плевать. А чего ты в куртке?"
    us "Whatever. Why are you in that jacket?"
    me "Uh... Didn't want to take it off. There were some Gypsies on my bus."
    us "Because they steal things?.. But there are no Gypsies here."
    #me "Или прячутся где-то, а ты с ними заодно."
    me "Maybe. Or maybe they're hiding. What if you're with them?"
    show us normal sport  
    #us "Вот же балабол. Ну расскажи, чего там они."
    us "What a chatterbox. I think you're making it up. Public buses don't come here, I never saw one."
    #me "Ладно... Одна из них ходила по автобусу, гадала. Для действа необходима купюра - туда же вся порча уходит. В конце она рвёт купюру на 2-3 части и оставляет себе, конечно."
    me "It's not often, but they do. Full of the Gypsies, I swear."
    us "I thought they only use horses and wagons."
    me "Wrong guess. One of them went on the bus, a fortune-teller, big woman. She were doing her stuff..."
    us "What stuff?"
    me "A Gypsie stuff, you know... At first she asks for a banknote."
    us "She needs money?"
    me "Who doesn't? But she asks because all the corruption goes into it."
    us "A-ah! Sure, it's their magic."
    me "...In the end she tears it into 2 or 3 pieces and keeps them, of course. That's the point."
    #us "Зачем ей рваная купюра?"
    us "Why does she need a torn banknote?"
    #me "Так склеить. Банк такие принимает. И даже если кусочка не хватает."
    me "It's easy to stick it together again."
    us "With magic?"
    me "With glue! A bank will accept it even if there's a piece missing."
    #us "А если из многих купюр по кусочку нарвать и склеить новую?"
    us "Hm-m. And if we use many small pieces from many banknotes to create a new one?"
    #me "То... это будет мошенничество."
    me "That would be fraud."
    show us grin sport  
    #us "Ну и что? Это государство мне должно!"
    us "So what? The state owes me everything!"
    me "How so?"
    show us dontlike sport  
    #us "Да жулят везде! В игровых автоматах у ружья прицел сбит, а монетки хавает за милую душу. {w}Так что там с цыганкой?"
    us "They cheat everywhere! A gaming machine's gun has a broken scope but still eats the money. {w}So what else about the gypsy?"
    show us normal sport       
    #me "Так, цыганка... {w}Подходит к мужику. Тот говорит, мол, рубля купюрой нет, только десять. Десятку отдать не может, это все его деньги. Ему бы хоть трёшку оставить."
    me "Oh, the gypsy... {w}So she approaches a guy. He says he doesn't have a one-rouble banknote, only ten, but he can't give her ten, because it's all his money. He says he needs at least three roubles for his way back."
    #me "Жадная цыганка даёт ему свою трёшку. А он вместо того, чтобы дать ей червонец - кричит: \"А дайте я сам погадаю! И платить не надо!\""
    me "The greedy gypsy gives him a three-rouble note. But instead of giving his ten he shouts: \"Hey, with that I'll tell your fortune myself!\""
    show us normal sport  
    #me "Скорчил страшную рожу, стал бубнить какую-то бессмыслицу. Цыганка аж присела от неожиданности. Потом он трёшку на мелкие части рвёт и кидает ей в рожу."
    me "He made a scary face, became to chant some gibberish, like casting a spell. The gypsy even sat down in surprise. Then he tore the three-ruble note into small pieces and threw it right in her face!"
    show us laugh sport  
    # us "Наш человек! А она чего?"
    us "That's how it's done! And what was her reaction?"
    #me "Проклинать его начала, а он сам её..."
    me "She started to curse him, obviously, and he..."
    stop music fadeout 2
    window hide
    scene bg ext_polyana_day  
    show sspi normal at center 
    with fade   
    $ renpy.pause (0.5)
    play ambience ambience_forest_day
    #pi "Ты меня уже д-до...стал со своими историями."
    pi "I'm f-f... fed up with your stories."
    #me "Это только завязка была, давай обратно."
    me "Hey, take me back! That was only the beginning."
    #pi "Каков упрямец. Не ври только, что ты и в таком лагере жить готов."
    pi "What a bullhead. Don't lie you would like to live in a camp like that."
    # me "Почему нет? Ульянка была совсем обычной."
    me "Why not? Ulyana was as usual as always."
    #pi "А с ней тебе общаться вообще не положено. По статусу. Тебе бы местные авторитеты объяснили."
    pi "You're not supposed to talk with her at all. Local authorities would have explained it."
    #me "Это Гвоздь который? И его подручные Киянка и Рубанок?"
    me "That Brick guy? And his followers Cement and Tile?"
    #pi "Прусак, Рысак и Гусак. А сам он..." 
    pi "Mordecai and Roland. You need to focus on the main thing."
    play music music_list["no_tresspassing"] fadein 5
    #me "Лучше расскажи, как ты меня туда отправил."
    me "Then tell me how you sent me there."
    #pi "Я никуда не отправляю. Я только показал что-то. По сути, это такой же обмен информацией, что и наш разговор."
    pi "I didn't send you anywhere. I only {i}showed{/i} you."
    me "How so?"
    pi "In fact, this was the same exchange of information as our conversation."
    #me "Так ты тут вроде бога, получается? Ну и какой может быть ад?"
    me "So you're a local god?.. Something like that? Why even talk about hell, if you're almighty?"
    #pi "Скорее, мы вроде карапузов, застрявших в песочнице. Я, ты, другие... Лепи, что хочешь, но песок остаётся песком."
    pi "In fact, we're more like toddlers, stuck in a sandbox. Me, you, others... You may create what you want, but sand is still sand."
    me "...Can you teach me how to create it?"
    pi "You've already done it. You should be happy about it, don't ask for more."
    me "What do you mean?"
    pi "You did it subconsciously, good for you. When you know what you're doing - you can't enjoy it. It will fall apart. You'll see only sand." 
    me "Like... literally?"
    pi "No! What a fool..."
    "Pioneer sighed heavily."
    pi "I can change nothing for me."
    me "...What about me? {w}If something needs to be changed, will I change it?"
    pi "Something?"
    #me "Значит, я и Саманту могу спасти? Подсознательно?"
    me "Is it possible to save... a person? Subconsciously?"
    show sspi smile at center 
    #"Пионер усмехнулся."
#    "Pioneer grinned."
    #pi "Ты спрашиваешь меня, как играть в твою собственную игру? Да делай, что хочешь. Ничего не изменится."
    pi "You asking me how to play your game? Do whatever you want. Nothing will be changed."
    stop music fadeout 5
    #me "Но ты сказал, что я тут налепил. Значит, что-то меняется?"
    me "But you said I've created something. Isn't it... a change itself?"
    #pi "Ты завёл новую куклу, и теперь борешься с проблемами, идущими в комплекте, с самой её сутью. Забавно. Знаешь, что? Я тебе помогу."
    pi "You found a new doll and now you're struggling with problems that come with her existence. Funny. {w}Know what? I'll help you."
    stop ambience fadeout 5
    play music music_list["sunny_day"] fadein 3
    #pi "Иди и убей её. Или хотя бы ударь. Чего ты так смотришь? Ладно, просто пройдись по лагерю голышом. За это я покажу тебе хэппиэнд: прошёл год, все живы и счастливы."
    pi "Go kill her. Or at least hit her. {w}Oh, don't look at me like this!"
    th "I'd rather do it to you."    
    pi "Ok, just run through the camp naked. For that, I'll show you the happy end: a year has passed, everyone's alive and happy."
    #th "Лучше я тебя ударю... Вот только я ещё не всё узнал."
    th "This conversation becomes really unpleasant. But I still need to know more."
    #me "С чего ты решил, что она кукла, а не кто-то вроде нас, например?"
    me "Why do you think she's a doll and not like me and you?"
    show sspi normal at center  
    #"Кажется, Пионер на секунду растерялся."
    "It seemed that Pioneer became confused for a second."
##Is Semyon referring to "Pioneer" by name, or would he be preceded by "the", to denote that it's a pioneer of unknown identity?
    # pi "Дурацкий вопрос. В эту игру нельзя играть вдвоём. Но если ты до сих пор их одухотворяешь, что тебя переубедит? Разве что, это."
    pi "What a stupid question. You know there is no such person in your world..."
    me "There is no persons like you either. I never heard about dolls and circles...  You can't proof that you're real... and that they aren't."
    pi "You are stubborn. But you'll change your mind eventually. {w}Perhaps this can help?"
    #"Пионер кивнул в сторону висящей верёвки."
    "Pioneer nodded towards a hanging rope."
    #pi "Иди на новый цикл, увидишь. Думаешь, она вспомнит? Да ты сам забудешь опять. Могу подменить тебя по-братски."
    pi "Go on to the next cycle. You think she will remember you? I would doubt your memory too. {w}Hey, I can help you with that."
    me "You mean, to remind me next time?.."
    pi "No, it's boring. But I can replace you next time."
    show sspi smile at center with dspr 
    #"Глаза Пионера недобро блеснули."
    "Pioneer's eyes flashed ominously."
    me "Can you, actually?"
    show sspi normal at center  
    #th "Похоже, он врёт мне. Или сам мало что знает. Пора заканчивать эту беседу, помогать он мне не станет."
    th "It looks like he's lying to me. Or he doesn't know much. Time to finish this; he won't help me."
    #me "Спасибо за ответы... И советы, но я лучше вернусь в лагерь."
    me "Thanks for the answers... and the advice, but I better get back to camp."
    pi "Aren't you curious how I came to you?"
    #me "Наверное, для таких дел надо стать таким как ты. Сейчас мне важнее другое."
    me "Guess for that I'd have to be more like you. That's not my priority for now."
    stop music fadeout 5
    #pi "Глупец. Я мог бы испортить тебе этот денёк, но ты и сам отлично справишься. А я понаблюдаю за твоими потугами."
    pi "You're stupid. I could ruin your day, but you'll do that on your own. And I'll watch over your vain attempts."
    window hide 
    $ renpy.pause(1)
    scene black with fade2
    play ambience ambience_forest_day fadein 2    
    scene bg ext_polyana_day 
    show unblink
    $ renpy.pause(2)
    #th "Я что, заснул? Сколько же человек может спать? А Пионер, это был сон? Было очень похоже на явь."
    th "Did I fall asleep? Was Pioneer just a dream? He looked so real."
    #"Наяву или во сне, слова Пионера меня не успокоили, но немного остудили. Лезть в петлю не хочется точно."
    "His words didn't reassure me, but they did calm me down a bit. At least I didn't want to hang myself, that's for sure."
    "I had to find Samantha, so I went back to the camp."
    window hide
    $ renpy.pause (0.5)
    scene bg ext_house_of_sl_day  with dissolve
    play ambience ambience_day_countryside_ambience
    #"Я вернулся в лагерь. Что теперь? Нужно найти Саманту, конечно, а то меня долго не было. Я подошёл к её домику и постучался."
    play sound sfx_knock_door7_polite    
    "Here was her cabin. I knocked."
    play sound sfx_open_door_1
    #"Саманта без лишних церемоний втащила меня внутрь."
    "Once the door opened, Samantha dragged me inside."
    $ persistent.ssg_1 = True    
    window hide
    $ renpy.pause (0.5)    
    play music music_list["raindrops"] fadein 5
    scene bg day_sam  
    show ss surprise casual
    with dissolve
    play ambience ambience_int_cabin_day

    #ss "Где ты был? Пропустил обед. Тебя ищут."
    ss "{enen}Where were you? You missed lunch. We've been looking for you!{/enen}"
    #me "Гулял, хотел немного один побыть. Присел в тенёчке, да и опять заснул."
    me "{enen}I was strolling just outside the camp, wanted to be alone for a while...{/enen}"
    ss "{enen}For a half of a day?{/enen}"
    me "{enen}...I sat down near some tree, and fell asleep again.{/enen}"    
    #ss "Столько спать! Голодный, наверное? Сейчас найду чего-нибудь."
    ss "{enen}How can you sleep for so long? Do you sleep at nights?{/enen}"
    me "{enen}Sure... It must be the air... {w}I'm sorry. Shouldn't disappear like that.{/enen}"
    show ss nosmile casual    
    ss "{enen}Are you hungry? I'll look for something.{/enen}"
    window hide
    # hide ss with dspr
    $ renpy.pause(0.3)

    me "{enen}Thanks, I'm not very hungry...{/enen}"
    ss "{enen}Has someone fed you already?{/enen}"
    me "{enen}No, it was an intense day so far, and...{/enen}"
    show ss surprise casual 
    ss "{enen}Intense? But you were sleeping the whole day!{/enen}"    
    me "{enen}Yes, but I had this crazy dream...{/enen}"    
    show ss unsure casual 
    ss "{enen}You don't need to make up a story for me.{/enen}"    
    th "Damn. I do have a crazy story for her, but she doubts me even before I started... Am I that lame?"
    ss "{enen}I mean, I'd like to know what happened, but if you can't tell me - just don't.{/enen}"    
    me "{enen}Whatever I'll say to you today, it will be very unconvincing. Yet true.{/enen}"    
    show ss nosmile casual
    ss "{enen}True?.. So you're going to tell me about your... intense dream?{/enen}"
    me "{enen}Don't rush with it... What kind of food do you have again?{/enen}" 
    show ss normal casual    
    ss "{enen}Come on, Sam! Food's on the table, but start talking, please! I am intrigued.{/enen}"
    
    #Начало русского конфуза     
    # th "А мы что, по-русски говорим? У неё даже акцента нет. Это всё ещё сон, или я схожу с ума?"
    # th "Are we speaking Russian right now? She doesn't even have an accent. Am I still in a dream, or am I crazy"
    # me "Do you speak Russian?"
    # window hide
    # $ renpy.pause(0.5)  
    #show ss nosmile casual with dspr
    #window show
    # ss "Несколько слов знаю."
    # ss "I know some words"
    # me "Но мы сейчас по-русски говорим."
    # me "But we're speaking Russian now."
    # show ss serious casual     
    # ss "За всё время ничего остроумнее не придумал?"
    # ss "Couldn't come up with anything smarter?"
    # th "Это не розыгрыш и не сон. Это Пионер надо мной издевается! Опять закинул меня куда-то. Вернуться на поляну?"
    # th "It's neither a joke nor a dream. The Pioneer is mocking me again! I'm in another camp, for sure. Should I return to that glade?"
#Translations were missing here, or intentional?
    # me "Мне нужно идти."
    # me "I have to go."
    # ss "Куда? К Ольге? Пошли вместе."
    # ss "Where? To Olga? Let's go together."
    # me "Нет. Мне бы только отойти на минутку."
    # me "No. I just need a minute."
    # show ss sad casual     
    # ss "Да что с тобой такое сегодня?"  
    # th "Тут Саманте и до слез недолго. Жалко её, ждала меня весь день. Даже если тут всё понарошку, всё равно нельзя так."
    # me "Ладно, никуда не иду."
    # show ss nosmile casual     
    jump ss_d4s1_eng

label ss_d4s1_eng:
    # th "И зачем куда-то идти, когда есть возможность обсудить всё с Самантой? Пусть я и не совсем понимаю, что происходит."
    # me "Для начала, давай выясним, почему мы говорим на одном языке."
    # ss "В каком смысле? Ты знаешь английский, вот и говорим на нём."
    # me "На нём? Не может быть, что я говорю как обычно. Могу даже читать скороговорки."
    # me "У Алисы дома крысы, а у Лены голы стены. У Ульяны нету ванны, протекают Жени краны. Ну, в квартире..."
    # show ss unsure casual     
    # ss "Ну, не знаю... Необычно как-то, но это потому что ты перегрелся на солнце, или ещё что."
    # me "Вот слушай: валенки, простокваша, Мойдодыр, Филателист... нет, не то. А теперь повтори."
    # show ss surprise casual       
    # ss "Валенки... Это глупо! Зачем?"   
    # me "Ты не знаешь эти слова, и что они значат. Но говоришь идеально."
    # show ss smile casual 
    # ss "Какое уж тут идеально."
    # me "Ладно, пока оставим этот феномен, потом кто-нибудь нас рассудит."
    # show ss unsure casual  
    # ss "Ты меня пугаешь. Лучше не говори никому, а то увезут тебя лечить."
    # me "Я и сам был бы в ужасе, но потихоньку привыкаю к такому."
    # show ss nosmile casual      
    # ss "К какому? Расскажи."
    #Конец русского конфуза
    #me "Помнишь вчерашний разговор про будущее? Это правда, я оттуда. Нужно ли продолжать?"
    
    #me "No, thanks. {w}I have to go..."
    #ss "Where? To Olga? Let's go together."
    #th "Wait, where do I have to go? Why did I even say that {w}Is that Pioneer messing with my head now?"
    #me "Uhm... Actually, I don't have to."
    #show ss sad casual with dspr
    #ss "What's wrong with you?"
    #th "If only I knew..."
    me "{enen}Let's try... Do you remember our conversation about the future?{/enen}"
    ss "{enen}When you said you're from there? Sure.{/enen}"    
    me "{enen}It's true; I am from the future. {w}Should I continue?{/enen}"
    show ss unsure casual  
    ss "{enen}Wow. I knew you didn't make it up out of nowhere, but that's a strong statement... Go on, I'm listening.{/enen}"     
    #ss "Мне и вчера показалось, что ты это не на месте придумал. Продолжай, я слушаю."
    #"Я вкратце поведал свою историю: от посадки в 410-ый, и до вчерашнего вечера. Про свою жизнь решил пока не распространяться."    
    "I took a big breath and began to tell her my story: from the bus № 410 until last night. I didn't tell her much about my old life, though."
    stop music fadeout 5    
    window hide
    $ renpy.pause (0.3)
    with fade2
    $ renpy.pause (0.6)    
    window show
    #me "Ну и как тебе?"
    me "...So?"
    show ss serious casual with dspr    
    #"Саманта сделала скептическую гримасу."    
    "Samantha made a skeptical grimace."    
    #ss "В волшебный автобус ещё могу поверить, но я - и вдруг президент? Хорошо, это ты знал ещё вчера, а что случилось сегодня?"
    ss "{enen}I could believe in a magic bus, but me - a president? You should know where to stop, young man.{/enen}"
    me "{enen}It was the only exaggeration, I believe. No, you're not gonna be a president...{/enen}"    
    ss "{enen}Oh. Like never?.. It's kinda sad.{/enen}"    
    show ss nosmile casual  
    ss "{enen}Wait, but everything you told me - you knew it before, right? What happened today then?{/enen}"    
    play music music_list["you_lost_me"] fadein 5
    #me "Сегодня я вспомнил кое-что. Тяжело это говорить, но..."
    me "{enen}Today I remembered something. {w}It's hard to say this...{/enen}"
    window hide
    $ renpy.pause(0.5)
    show ss angry casual     
    window show 
    #ss "Выкладывай уже."    
    ss "{enen}Just tell me already.{/enen}"  
    #me "В моем будущем тебя нет. {w}Уже нет."  
    me "{enen}You don't exist in my future. {w}Not anymore.{/enen}"  
    show ss surprise casual 
    #ss "То есть, ты хочешь сказать..."
    ss "{enen}You want to say...{/enen}"
    #"Я поспешно кивнул, чтобы не слышать этих страшных слов."
    "I nodded quickly so not to hear those dreaded words."
    #ss "И сколько же мне ещё отпущено? Двадцать лет? Двадцать пять?"  
    ss "{enen}I'm gonna die?.. And how much time do I have? Twenty years?{/enen}"  
    me "{enen}Our worlds are different, so events could be different too. Don't take it too seriously.{/enen}"
    show ss serious casual with dspr    
    #ss "Но ты-то принял, как вижу. Ну так сколько?"
    ss "{enen}But you are taking it so. Well, how much do I have?{/enen}"
    me "{enen}Lesser.{/enen}"
    ss "{enen}Fifteen years? Ten?{/enen}"
    "I shook my head."
    show ss angry casual    
    #ss "Ты не очень честно торгуешься. Ну так сколько?" 
    ss "{enen}You're driving a hard bargain. How much?{/enen}" 
    #me "Год. Или даже месяцы. Потом какая-то авария. У нас это было так."
    me "{enen}A year. Or months. Then will happen some kind of an accident. It did in my reality.{/enen}"
    show ss scared casual
    ss "{enen}Months?!{/enen}"
    show ss unsure casual   
    #th "Вот я и сказал это. Гора с плеч? На другие плечи."
    th "So, I said it. A great weight is off one's mind... Onto other."
    #"Растерянность на лице Саманты потихоньку сменялась гневом."
    "The confusion on Samantha's face slowly gave way to anger."
    show ss vangry casual 
    #ss "Слушай. Я не знаю, чего там тебе кажется, но сейчас-то ты в своём уме. Ну кто говорит такое людям?"
    ss "{enen}Listen. I don't know about your dreams or visions, but now you seem to be sane. Who even says such things to someone?{/enen}"
    show ss angry casual    
    me "{enen}...Sorry.{/enen}"  
    ss "{enen}Sorry? People don't say things like that even to their enemies! But you... {w}You'd better go.{/enen}"
    #me "Ну а что бы ты сделала на моем месте? Я в это верю, понимаешь? Как бы я с этим жил?"
    me "{enen}What would you do in my place? Do you understand that I believe in it? How could I hide it from you?{/enen}"
    stop music fadeout 3
    play sound sfx_open_door_2
    #"Дверь отворилась, и на пороге показалась Ольга Дмитриевна. Должно быть, нас выдал разговор на высоких тонах."
    "The door opened and Olga Dmitrievna appeared in the doorway. She must have heard our loud conversation."
    window hide
    show mt angry panama pioneer at fleft with dspr
    $ renpy.pause(0.5)  
    window show
    mt "Semyon. Out."
    th "As always at the wrong time."
    window hide
    $ renpy.pause(0.5)  
    play ambience ambience_day_countryside_ambience fadein 1    
    scene bg ext_house_of_sl_day  with dissolve
    $ renpy.pause(0.5)    
    show mt angry panama pioneer  with dspr
    $ renpy.pause(0.5)
    play music music_list["trapped_in_dreams"] fadein 5
    # mt "Где ты был? Я к тебе по-хорошему, утром будить не стала, а ты так подводишь."
    mt "Where were you? I was nice to you, didn't wake you up! That's how you pay for favors?"
    me "My fault, won't happen again."
    show mt normal panama pioneer  
    #mt "Да? Ловлю на слове. А чего это ты не оправдываешься? Заболел?"
    mt "Huh? Why are you not trying to justify yourself? Not feeling well?"
    me "No. I only suffering the pangs of conscience."
    #mt "Похвально. Тогда идите погуляйте. {w}Ну, чего застыл?"
    mt "Commendable. So where were you?"
    me "In the forest. Just walking."
    mt "You shouldn't go there alone. No pioneer should."
    me "Noted. I didn't like it there anyway. Too many trees, almost no other stuff..."
    mt "What stuff do you want to see there?"
    me "Like pathways and roads... maybe some signs? And the big lumberjack machines that will cut the rest of it!"    

    show mt smile panama pioneer  
    #mt "А, ну ещё бы. Тогда иди помоги Славе у кружков, а мы пойдём купаться. Иногда и отдыхать нужно!"
    mt "I see, I see... "
    show mt normal panama pioneer 
    extend " Hey, if you have nothing to do, then go and help Slavya at the clubhouse."
    me "What about Samantha?"    
    mt "Don't worry, we'll go for a swim. Everyone needs to rest once in a while!"
    #th "Саманта сейчас явно не в настроении для пляжа, надо спасти её от вожатой."
    th "That's not what Samantha needs right now. I have to save her from the camp leader."
    menu:
        #"Саманта не в духе, оставьте её в покое":
        "Leave her alone, please": 
            show mt normal panama pioneer         
            me "Samantha's not in the mood for it right now, give her some rest."
            mt "Oh, is she upset? Why?"
            "..."
            mt "Because of your disappearance this morning? I'll handle that. Here is the task for you: go find Slavya."        
            # th "Теперь и мне попадёт, что Саманту довёл. Надо было сразу к ней вернуться."
            me "Wait... Listen, you mustn't come in. Let me speak with her first."
            #mt "Опять, Семён? Ну почему у тебя вечно какие-то проблемы? В лагере нужно отдыхать! Ладно, жду на пляже."
            mt "Semyon-Semyon... Why do you always have problems? The camp is made for recreating!"
            me "No problems... Just need to explain her a thing or two. Then we will join you."
            mt "Faster then. I will be waiting on the beach."
            $ sp_mt -= 1              
            jump ss_d4s2_eng
        "I need to go to Samantha for a couple of words":
            #me "Знаете, я ещё не успел извиниться за утро. Вы одна идите. Встретимся там, если Саманта захочет пойти."
            me "You know, I didn't have time to apologize for this morning. Give us a time, please? We'll meet you there, if Samantha wants to go. "
            mt "Of course she wants to go. Okay, catch me later."
            jump ss_d4s2_eng

label ss_d4s2_eng:
    window hide
    $ renpy.pause(0.4)    
    hide mt with dspr
    $ renpy.pause(1)    
    play sound sfx_knocking_door_2  
    $ renpy.pause(2)
    #th "Неужели не пустит меня обратно?"
    th "Will she let me back in?"
    play sound sfx_knock_door2
    #"Со второго раза я всё-таки получил разрешение войти."
    "On the second attempt I got permission to enter."
    window hide
    $ renpy.pause(0.4)
    play ambience ambience_int_cabin_day fadein 1    
    scene bg day_sam  
    show ss cry casual
    with dissolve
    
    $ persistent.ssg_1 = True     
    play ambience ambience_int_cabin_day
    $ renpy.pause(0.6)
    #"Лицо Саманты порядком раскраснелось и опухло за те минуты, что меня не было. Хорошо, что я не пустил к ней вожатую."
    "Samantha's face had become red and swollen in the time that I'd been absent. Good thing I didn't let the camp leader come in."
    #me "Это снова я."
    me "{enen}It's me again.{/enen}"
    #ss "Отлично."
    ss "{enen}Great.{/enen}"
    hide ss with dspr
    #"Поймав мой взгляд, Саманта выдернула из заправленной постели подушку и прижала её к лицу. Я присел на кровать у противоположной стены."
    "Samantha caught my stare, pulled the pillow from the bed and pressed it to her face. I sat down on the bed opposite hers."
    #me "Так жаль, что пришлось это всё сказать..."
    me "{enen}I'm so sorry I had to tell you all those things...{/enen}"
    #ss "Не надо об этом."
    ss "{enen}Don't talk about it again.{/enen}"
    #me "Хорошо... Ты ведь не хочешь сейчас на пляж?"
    me "{enen}Okay... I guess you don't want to go to the beach right now?{/enen}"
    #ss "А ты прочитай мои мысли. Консервных банок с проводом нет, но ты и так справишься, на правах волшебника из будущего."
    ss "{enen}Again with your jokes? Not now.{/enen}"
    me "{enen}It's not a joke...{/enen}"
    ss "{enen}We don't have cans with wires, or other mindreading stuff, but you'll handle it, since you're a wizard from the future. Go on, read my thoughts.{/enen}"
    #me "Твои мысли?"
    me "{enen}Your thoughts?..{/enen}"
    #ss "Ага. Ну, чего я хочу?"
    ss "{enen}Yes. What do I want?{/enen}"
    #me "...Чтобы отстали?"
    me "{enen}...You want some peace?{/enen}"
    #ss "Бинго."
    ss "{enen}Bingo.{/enen}"
    #me "Тут как раз загвоздка. У Ольги Дмитриевны на тебя планы, как бы она не вернулась." 
    me "{enen}There is a problem with that: Olga Dmitrievna has plans for you. She might come back soon.{/enen}"        
    #ss "И что делать?"
    ss "{enen}Please, kill me.{/enen}"
    #me "Можно уйти куда-нибудь."
    me "{enen}We can go somewhere. {w}To avoid her, not to kill you.{/enen}"
    #ss "Похоже, выбора нет."
    ss "{enen}Guess there's no choice.{/enen}"
    #"Саманта отложила подушку и встала с кровати."
    "Samantha put down the pillow and got out of bed."
    show ss shy2 casual with dspr
    #ss "Я нормально выгляжу?"
    ss "{enen}Do I look alright?{/enen}"
    #me "Вполне."
    me "{enen}Yeah, you look great.{/enen}"
    show ss shy casual     
    #ss "Пошли."
    ss "{enen}Let's go.{/enen}"
    #me "Только заплаканно очень."
    me "{enen}...Great - for a person who's been crying an awful lot.{/enen}"#не уверен насчёт этого ##Thinking he says that she looks like she's been crying an awful lot.
    show ss shy2 casual 
    #ss "Так сильно?"
    ss "{enen}It's so noticeable?{/enen}"
    stop music fadeout 5
    #me "Да шучу я, идём."
    me "{enen}Just kidding, let's go. {w}You are pretty little tomato.{/enen}"
    window hide
    show ss angry casual with dspr    
    $ renpy.pause(0.8)  
    scene bg ext_house_of_sl_day  with dissolve
    play ambience ambience_day_countryside_ambience
    #"Библиотека вполне подходила для временного пристанища, туда мы и двинулись."
    "The library seemed to be a good place for a temporary shelter, so we went there."
    window hide 
    $ renpy.pause(0.6)    
    play ambience ambience_int_cabin_day fadein 1    
    scene bg int_library_day  with dissolve
    show un shy pioneer at cleft
    show ss nosmile casual at cright
    with dissolve   
    #un "Ой! Привет."
    un "Oh! Hi."
    #me "Привет. Чем занимаешься?"
    me "Hi. Are you busy here?"
    play music music_list["my_daily_life"] fadein 5
    #un "Да так, ничем."
    un "Not really..."
    #"Я пригляделся: перед ней на столе лежала карта Монополии, только почему-то разорванная на три части. Ей-то Лена и занималась, когда мы нагрянули."
    "I looked closer: in front of her on the table lay a map of Monopoly, but somehow ripped into three parts. Lena was trying to fix it when we arrived."
    show un normal pioneer at cleft 
    #me "Это кто же такое сделал?"
    me "Who did this?"
    #un "Не важно, я склею."
    un "Doesn't matter, I'll fix it."
    #me "Да как же не важно? Скажи!"
    me "What do you mean it doesn't matter? Someone's ruined our work!"
    #un "Хулиганы какие-нибудь. Я забыла библиотеку на обед закрыть. Моя вина."   
    un "Hooligans. I forgot to close the library for dinner. My fault."     
    #ss "Что с ней случилось?"
    # ss "What happened to it?"
    # "Саманта почему-то обращалась ко мне."
    # "Samantha for some reason referred to me."
    # show un shy pioneer at cleft 
    #me "Кхм. С Леной?"
    # me "You mean with Lena?"
    #ss "С Монополией."
    # ss "No. With the game."
    #me "Лена же сказала."
    # me "But Lena said it."
    #ss "Ну и? Переводи."
    # ss "So? Translate it."
    # show un normal pioneer at cleft     
    #"Судя по реакции Лены, идиотом из нас выглядел я. {w}Ничего не поделать - придётся переводить с русского на русский."
    # "Judging by Lena's reaction, I was the idiot in this pair. {w}Well, let's translate from russian to russian."
    show ss nosmile casual at cright with dspr  
    #ss "...Зачем хулиганам заходить в библиотеку? Может, кто-то заходил вернуть книгу?"
    ss "{enen}...What would hooligans need in the library?{/enen}"
    me "Do we even have them in the camp?"
    un "I don't know who it was."     
    ss "{enen}Maybe someone came to return a book?{/enen}"    
    #th "Опять расследования? Почему бы и нет, если это отвлечёт Саманту."
    th "A new investigation? Heck, why not? Maybe it will distract Samatha from bad thoughts."
    show un serious pioneer at cleft
    #me "Вы же ведёте журнал? Если злодей и оставил книгу, то уж точно не записал это."
    me "Let's see... Do you keep a log? If the villain left a book, he certainly didn't write about it."
    #un "Журнал ведётся, конечно. Но как проверить, книг много."
    un "We do, of course. You want to look for a book that shouldn't be here? But there are far too many books around..."
    #me "А мы проверим только те, которые брали. Вряд ли там очень много записей. Ну так сверим по журналу?"
    me "We will check only the ones that were taken. It is unlikely that there are a lot of entries. So let's check it."
    #un "Ребята, да ерунда же. А журнал кому попало читать нельзя."
    un "Don't bother, guys, that's not worth it. And the logs aren't available to everyone."
    #ss "Да и не надо. Нужно искать, кому это выгодно."
    ss "{enen}Let's go from another side: if there is a crime, we have to look for someone who would benefit from it.{/enen}" # What the heck does this mean:  "Cui bono? Cui prodest?"
    show un normal pioneer at cleft 
    #me "По-моему, никому. Кому может навредить Монополия? {w}Жене, разве что. У неё к любой вещи претензии найдутся."
    me "{enen}How could Monopoly hurt anyone? {w}Except for Zhenya. She hates almost everything.{/enen}"
    #ss "Но она же сама её делала. Может, это ты порвал? Тебя как раз не было на обеде! Есть алиби?" #Я не очень понял это предложение в контексте предыдущего
    ss "{enen}But she made it herself! Why would she ruin it?{/enen}"
    th "Because she probably hates herself and every her creation..." 
    show ss grin casual at cright     
    ss "{enen}What if it wasn't her? Could it be you who did it?{/enen}"
    me "{enen}Nope, I would have known.{/enen}"
    ss "{enen}But you missed the dinner! What's your alibi?{/enen}"     
    #me "Нету, поэтому я должен найти преступника. Ну же, Лен, помогай. Ты только книги называй, а я поищу по полкам." 
    me "{enen}I don't have one; that's why I want to find the culprit.{/enen}" 
    show ss nosmile casual at cright with dspr     
    me "Come on, help me, Lena. Just call out the book's name, and I'll look for it on the shelves."     
    show un angry2 pioneer at cleft 
    #un "Не буду. Это глупо."
    un "I won't. This is silly."
    #me "Ты препятствуешь расследованию. Стоп."
    me "You're interfering with the investigation. {w}Wait a minute..."
    show ss surprise  casual at cright    
    #ss "Что?"
    ss "{enen}What?{/enen}"
    #me "Эта книга на полке, самая ближняя к столу - кажется, я видел её у Алисы в домике."
    me "This book on the shelf, the one closest to the table - I think I saw it in Alisa's cabin."
    show ss normal casual at cright    
    show un sad pioneer at cleft 
    #"Девочки выразительно на меня посмотрели."
    "The girls stared at me questioningly."
    #me "Что? Да мы вчера с Ульяной заскочили на минутку. Так вот, эта книга... Проверишь журнал, или идти искать Алису?"
    me "What? Ulyana and I visited her yesterday for a few seconds, but it's not the point... {w}This book - can you check the list? Or should we go and ask Alisa?"
    #"Тут я, конечно, блефовал. Но нежелание Лены участвовать в нашей игре подогревало мой интерес."
    "I was bluffing, of course. Lena's reluctance to participate in our game fueled my interest."
    #un "Ладно, это я порвала. Нечаянно."
    un "Fine, I ripped it. Unintentionally."
    show ss surprise casual at cright   
    stop music fadeout 5
    #th "Это же надо умудриться так порвать, да ещё в двух местах."
    th "Huh? How did she manage to rip it that hard, and in two places simultaneously?"
    #ss "Нет, ты не могла ничего порвать. Ты кого-то защищаешь. Ладно, давайте оставим это."
    ss "{enen}No, you couldn't have done that. You're defending someone... Please, let's forget about it.{/enen}"
    show ss unsure casual at cright    
    #"Я деликатно \"перевёл\" только первое и третье предложения."
    "I delicately translated only the first and third sentences."
    show un normal pioneer at cleft 
    #un "Ах, ты такая милая. Но нельзя быть такой наивной."
    un "Oh, you're so cute. But you can't be that naive."
    play music music_list["orchid"] fadein 2    
    #th "Что-то с ней не так."
    th "Something's wrong with her."
    show un evil_smile pioneer at cleft 
    show ss surprise casual at right    
    #un "Не могу порвать какую-то картонку. Смешно! Да я почти выжила тебя из этого лагеря, подруга. Как тебе это?"
    un "Like I can't break some kind of a cardboard. Ridiculous! I almost kicked you out of the camp, my dear. What do you think of that?"
    #me "Что ты говоришь?"
    me "What are you talking about?"
    show ss serious casual at right     
    #un "Ульяна хорошо выполняла свою задачу, ты бы её не остановил. Ты мог поймать её с поличным, но публичный скандал меня бы устроил."
    un "Ulyana did well; you failed to catch her in time. You've managed to avoid the big scandal though. Good job!"

    #un "Да вот Алиса вмешалась, только она могла уладить всё мирно, единственный авторитет для Ульяны. Всегда лезет, куда не надо..."
    un "Actually, you must thank Alisa for that. Only she could settle everything peacefully, being the only authority over Ulyana. She always intervene..."
    #un "А ещё был Шурик. Сначала самой не верилось, как повезло с ним. Даже страшновато было, что перегнёт. Но быстро же он сдулся."
    un "Let's not forget about Shurik. I thought I was so lucky with him! I was even worried that he would be too intense. Alas, his fighting spirit weathered fast."
    #un "Остальные мои карты не сыграли. Нужно признать поражение. А что ты не переводишь?"
    un "My other plans didn't worked. I have to admit defeat. {w}Why are you not translating?"
    #me "Сама переводи такое..."
    me "Do it yourself..."
    show un sad pioneer at cleft 
    #un "Не хочешь расстраивать Саманточку? Она же и так у нас расстроена, я замечаю такие вещи. Вот тут я и признаю поражение."
    un "Don't want to upset our little Sammy? She is already upset, actually; I notice things like that. That's why I admit defeat."
    #un "Сегодня, когда я уже опустила руки, ко мне приходите вы, и что я вижу? Саманта - белым бела. И довела её не я, а кто? Ты?"
    un "Today, when I chose to give up, you come to me and what do I see? Samantha, pale and sad."
    ss "{enen}What's... going on? Guys?{/enen}"

    un "...Looks like we finally made her hate this camp. But it's not me who did it - it was you!" #not sure about this sentence; I equate it to her saying that she's given up?
    #me "И зачем тебе всё это?"
    me "Why... Why would you need that?"
    show un normal pioneer at cleft 
    #un "Всё началось с шутки про отца Ульянки. Ей оказалось так легко управлять. А потом... Тут ты не поймёшь."
    un "It all started with that joke about Ulyana's father. And later... No, you won't understand."
    # th "Надо помнить про проделки Пионера и не волноваться."
    th "I have to remember about Pioneer's tricks and keep calm."
    #un "Переводи-переводи, это ей на пользу. Нельзя такой наивной быть."
    un "Go on, translate it, it will be good for her. She shouldn't be that naive."
    if un_inv == True:
        #un "И меня чуть в Америку не взяла - смех."
        un "And she almost took me to the US her! Isn't that funny?"
    #me "Это у тебя проблемы, а не у неё."
    me "Seems like you're the one with problems, not her."
    show ss sad casual at right     
    #ss "Я тебя чем-то обидела?"
    ss "{enen}Have I hurt you somehow, Lena?{/enen}"
    #un "Могла ли ты? Нет. Ну а я тебя чем? Просто показала чьи-то пороки. Мы ведь против лицемерия? Ну, вы."
    un "Could you?.. {w}To be honest, I did no harm to you either. What I did - I just showed someone's faults."
    me "It sounds like you hurt plenty of people."
    un "I didn't lie or force someone into something... It's about their true nature. We're against hypocrisy, right? Well, Samantha must be at least."
    show un serious pioneer at cleft 
    #un "Ты взываешь к чему-то хорошему, это всё выглядит мило, но сладкими речами ничего не изменишь. Дитя, одним словом."
    un "You are good girl and you appeal for something what seems right, it all looks cute, but sweet words can't change anything. You're just a naive child."
    #me "Ты действительно хотела навредить Саманте, чтобы научить чему-то?"
    me "Did you really want to hurt Samantha just to teach her a lesson?"
    show un evil_smile pioneer at cleft 
    #un "Нет-нет, сначала мне до неё не было дела. А потом стало интересно. Прогнать Саманту - это достижение, знаешь ли. Большая политика!"
    un "No-no, at first I didn't care about her. But later it became interesting. Suddenly I loved that idea - to get rid of Samantha. That is an achievement, you know. Big politics!"

    #me "Если ты такая коварная, то зачем всё это рассказываешь?"
    me "If you are so insidious, why are you telling us about this?"
    show un normal pioneer at cleft    
    #un "Может, я не такая коварная, как хотелось бы. В какой-то момент ваша беззубость стала меня раздражать. Какая политика? Детские игры."
    un "Maybe I'm not as insidious as I should be. At some point, your gutlessness began to annoy me. Politics? Hah! Child's games."
    #un "Я обману - вы поверите. Попрошу прощения - вы простите. Предложу дружбу - вы примете. Это всё даже неспортивно. Что с вами такое, люди?"
    un "I lie - you believe. I ask for forgiveness - you forgive me. I offer friendship - you accept it. There's no fun. What's wrong with you people?"
    #me "Да... Наверное, воздух тут такой."
    me "Yeah... Maybe it's something in the air."
    window hide
    show ss sad casual at right 
    hide un
    with dspr   
    window show
    #"Я отвёл Саманту в сторонку (Лена меня смущала) и попытался передать суть разговора. Лена ждала нас как ни в чем не бывало. Такое даже нервным срывом не назовёшь."
    "I took Samantha aside, and tried to convey the essence of the conversation. Lena was waiting for us as though nothing had happened. This couldn't even be called a nervous breakdown."
    window hide
    $ renpy.pause (0.3)
    show un normal  pioneer at cleft
    show ss unsure casual at right  
    with dspr 
    $ renpy.pause (0.5)    
    window show
    #ss "Что сказать? Я могу заблуждаться, но ты тоже можешь, Лена. Может, ты слишком часто ошибалась в людях, и тебе тяжело им поверить."
    ss "{enen}What can I say? I may be mistaken, but so could you, Lena. Maybe you've been wrong too many times by others, and now it's hard for you to believe someone.{/enen}"
    show un angry2 pioneer at cleft 
    #ss "Ты сомневаешься в добре, втайне желая ошибиться. Но ты и насчёт меня ошибаешься."
    ss "{enen}You doubt the good, but secretly wanting to be wrong. I'll say, you're wrong about me as well.{/enen}"
    #ss "Я ведь тут не несу никакую миссию, не делаю ничего важного и хорошего, просто живу. Может, это ты должна быть на моем месте, если хочешь за что-то бороться.{/enen}"
    ss "{enen}I came here without a mission, I'm not doing anything important or good. I don't fight for anything, I just live. If you have such passion to fight, maybe you should be in my place...{/enen}"
    show un angry pioneer at cleft 
    show ss scared casual at fright    
    #un "Психоанализ от девчонки - последнее, что мне нужно. Жизнь тебя ещё научит о добре и зле, по-своему. А пока - идите-ка вон из моей библиотеки."
    un "Psychoanalysis from a little girl - the last thing I need. Life will teach you all about good and evil. I'm done - get out of my library."
    window hide    
    stop music fadeout 4
    $ renpy.pause (0.8) 
    hide ss with dspr
    $ renpy.pause (0.4)
    show un angry2 pioneer at cleft with dspr    
    $ renpy.pause (1)
    un "You too."
    me "You are... not healthy person. You need to speak with Viola about your head... about all this."
    un "Leave me."    
    window hide
    play sound sfx_dinner_horn_processed
    $ renpy.pause (2)
    me "Of course I will..."     
    window hide
    $ renpy.pause (1)
    play ambience ambience_camp_center_evening
    play sound2 sfx_close_door_1
    scene bg ext_library_day  with dissolve
    $ renpy.pause (0.5)    
    show ss serious casual with dspr 
    $ renpy.pause (0.8)
    me "{enen}I'm sorry for that... {w}Everyone in this camp is offensive and crazy, and do stupid things. What a great trip for you.{/enen}" 
    show ss normal casual with dspr     
    ss "{enen}Yep... At least I know one thing for sure - there is no KGB here.{/enen}" 
    me "{enen}Always look for silver linings, eh? {w}Hey, can we go the canteen now? I'm really starving.{/enen}"
    ss "{enen}Let's go!{/enen}"    
    window hide
    $ renpy.pause (0.8)     
    scene bg int_dining_hall_people_day
    with dissolve
    $ renpy.pause (0.5)
 
    play ambience ambience_dining_hall_full
    #"На счастье, Ольги Дмитриевны за столиком не оказалось. За весь день я съел только пару бутербродов у Саманты, так что гречка с подливкой стала благословением."
    "We headed for our table. Fortunately, Olga Dmitrievna wasn't there."
    window hide
    show ss serious casual with dissolve
    window show    
    "During the whole day I'd only eaten a couple of sandwiches from Samantha's cabin, so buckwheat was a blessing."
    play music music_list["afterword"] fadein 5
    #me "Похоже, ты угадала насчёт Лены, раз она так отреагировала. Значит, она еще не совсем потеряна? Но это всё очень странно, конечно."
    me "{enen}Guess you were right about Lena. I mean, when you spoke to her.{/enen}"
    ss "{enen}You think so?{/enen}"
    me "{enen}Yeah, judging by her reaction. Your words got her... So she can't be completely lost, right?{/enen}"
    show ss nosmile casual with dspr     
    ss "{enen}Of course. I believe in her, someone like her couldn't possibly be that bad. She's just confused...{/enen}"  
    me "{enen}Heck, me too! It was so strange...{/enen}"    
    #ss "Конечно, я в неё верю. Не может кто-то вроде неё быть совсем уж плохим."

    #me "Как же мы можем ей помочь?"
    extend " {enen}You think we can help her?{/enen}"
    show ss unsure casual with dspr     
    #ss "Вряд ли это должны быть {i}мы{/i}, если ты понимаешь. Может, один из нас. Может, кто-то ещё."
    ss "{enen}I'd like to, but... I doubt that it have to be {i}us{/i}. {w}Maybe it could be one of us. Maybe someone else.{/enen}"
    #me "Думаешь, она со многими была откровенна, вот как с нами сейчас?"
    me "{enen}Is there someone else, actually? I mean, she was very frank with us... Maybe it's for the first time.{/enen}"
    #ss "Так ли это важно? Давай не будем сплетничать."
    ss "{enen}I can't offer her much since she don't want to see me. It's her right. {w}And I think we're just gossiping right now, maybe we should stop.{/enen}"
    #me "Но я поражаюсь её перевоплощению. Как же она выдерживает свой образ? Такая выдержка...мимика. Ей бы в ГРУ или ЦРУ. Или в кино играть."
    me "{enen}Yeah... But I am amazed by her transformation. With such acting skills she could work for the GRU, or the CIA.{/enen}"
    show ss nosmile casual with dspr 
    #ss "Да, наша пьеса без неё много потеряет. Но ты не думаешь, что это всё было по-настоящему?"
    ss "{enen}Yes, our play will suffer without her. But I'm not sure if her words was pure acting. Don't you think it was kinda real?{/enen}"
    #me "Напомню, что я затерялся во времени и пространстве, и сейчас говорю по-русски с девочкой, сошедшей с почтовой марки."
    me "{enen}Real... Let me remind you that I got lost in time and space, and I befriended a girl who I only saw before on postage stamps.{/enen}"
    #me "Любое \"по-настоящему\" - весьма относительно."
    me "{enen}Any \"real\" - is relative here...{/enen}"
    show ss surprise casual 
    #ss "Меня окружают психи!"
    ss "{enen}I am surrounded by psychos!{/enen}"
    show ss normal casual   
    #extend " ...И всё-таки, со старой Леной мы лучше знакомы. Я её так быстро не забуду, новой Лене ещё придётся постараться."
    extend "{enen} And yet, we know the old nice Lena better. This new Lena won't make me forget her so easy.{/enen}"
    #me "Совсем не злишься на неё? Разве я пропустил ту часть, где она натравила на тебя Ульянку и Шурика?"
    me "{enen}Don't you feel angry at all? Did I forget the part where Shurik and Ulyana harassed you because of her?{/enen}"
    show ss unsure casual    
    #ss "Нет, ты говорил. Глупости, она очень преувеличивает. Дала какую-то книжку Шурику, и всё такое." 
    ss "{enen}You told me, she gave a book to Shurik and so on... What a nonsense. She is exaggerating.{/enen}" 
    #me "Откуда знаешь?"
    me "{enen}What book? How do you know?{/enen}"
    show ss nosmile casual  
    #ss "Он утром сказал, когда подходил извиняться. Ещё раз."
    ss "{enen}I don't know exactly, as we hardly communicate... He told me this morning, when he came to say sorry. Again.{/enen}"
    #th "Некоторые люди слишком много ...ходят. В этом их проблема. Сидели бы на месте себе..."
    th "Damn. Some people just moving too much. And they pick totally wrong directions for that. That's their second problem. Why can't he sit at his club..."
    show ss normal casual   
    #ss "Да не сердись, за завтраком это было."
    ss "{enen}Don't frown like this. It was at breakfast.{/enen}"
    #me "Я и не сержусь."
    me "{enen}I have my reasons to frown about him.{/enen}"
    #ss "Ольга идёт. Только ты не жалуйся ей на Лену."
    ss "{enen}Hey, Olga is coming. Don't tell her about Lena!{/enen}"
    window hide
    $ renpy.pause (0.6)
    show mt normal panama pioneer at fleft   with dspr
    $ renpy.pause (0.4)    
    window show
    #mt "Не пришли? Не любите купаться? А так весело."
    mt "Why didn't you come? Don't you like swimming? You missed all the fun."
    #ss "Я - люблю. Мы просто погулять решили."
    ss "{enen}Oh, no, I'd love to! We'll do it next time. Today we had a nice walk instead.{/enen}"
    #mt "Семён, а ты плавать не умеешь, что ли?"
    mt "I see. {w}What about you, Semyon? It's suspicious how you're avoiding the beach. Can you swim?"
    #me "Как-то так вышло, что умею. Сам себе поражаюсь."
    me "Somehow it happened that I can. I'm amazed with myself."
    #ss "Можем вечером на пляж сходить."
    ss "{enen}We can go swimming in the evening.{/enen}"
    #mt "Нет, после ужина - поход. Здорово будет!"
    mt "No, after dinner we'll go for a hike. No one can avoid that!"
    show ss smile2 casual with dspr     
    #ss "Я умею ставить палатку. Ходили в горы прошлым летом."
    ss "{enen}Great, I can finally be useful. I know a lot about camping, I can set up a tent. We went to the mountains last summer.{/enen}"
    #mt "У нас всё скромнее, ночёвки не будет. Могу обещать только костёр."
    mt "We won't go that far; we're not spending the night in the forest. But I can definitely guarantee a campfire."
    #ss "Костры разводить я тоже умею."
    ss "{enen}That's something I can handle too.{/enen}"
    #mt "А расскажи про ваш поход попродробней." 
    mt "Tell us more about your camping experience." 
    show ss smile casual    
    #ss "Ну... Одним утром меня разбудил олень."
    ss "{enen}Well... One morning I was awaken by a deer.{/enen}"
    #me "Пролез в палатку и лизнул в нос?"
    me "Got into your tent and licked your nose?"
    show ss grin_smile casual    
    #ss "Нет, только гремел снаружи посудой. Мы так перепугались! Были уверены, что там гризли."
    ss "{enen}No, it just rattled the dishes outside. We were so scared! We thought for sure that it was a grizzly.{/enen}"
    show ss nosmile casual with dspr    
    #ss "Но он свалил котелок - испугался шума, сам убежал."
    ss "{enen}But he dumped the pot - the noise scared him and he ran away.{/enen}"
    show ss sad casual 
    #ss "...А на обратном пути мы нашли мёртвого оленя на дороге."
    ss "{enen}...And on the way back, we found a dead deer on the road.{/enen}"
    show mt sad panama pioneer at fleft   
    #me "Того же?"
    me "The same one?"
    #ss "Не знаю, вряд ли. Но всё равно жалко."
    ss "{enen}No, unlikely... But it was sad anyway.{/enen}"
    window hide
    stop music fadeout 3
    $ renpy.pause(1.5)    
    $ sunset_time()
    $ persistent.sprite_time = "sunset"
    play ambience ambience_camp_entrance_day_people fadein 2    
    scene bg ss_ext_camp_entrance_sunset  with fade2
    window show
    #"После ужина пионеры собирались у ворот лагеря. Я заметил Лену - выглядела она вполне умиротворённо, но лучше не искушать судьбу. Ко мне подошла Славя."
    "After dinner, the pioneers gathered at the gates of the camp. Lena was there - she looked quite peaceful, but it was better not to tempt fate by speaking to her."
    "I noticed Slavya aswell. I smiled to her briefly, she waved and approached me."
    window hide
    show sl normal pioneer far  with dspr
    show sl normal pioneer  with dspr  
    window show 
    #sl "Привет, Семён. А я тебя сегодня искала для Ольги Дмитриевны. Заставил побегать."
    sl "Hi, Semyon. I was looking for you today; as Olga Dmitrieva asked me. You made me run around."
    #me "Извини."
    me "Sorry for that."
    #sl "Ничего. Только вот в кружках была генеральная уборка, а я из-за тебя прогуляла. Нехорошо."
    sl "It's alright. But there was general cleaning in the clubs, and I missed it because of you. Not good."
    #me "Если тебе доставляет такое удовольствие уборка, могу намусорить где-нибудь."
    me "If you enjoy cleaning that much, I can drop some garbage anytime."
    show sl sad pioneer  
    #sl "Зачем же так."
    sl "Why? Don't do that please." #Double sad GLORY!!11
    #me "Прости."
    me "Just a joke... Sorry."
    show sl smile pioneer  
    #sl "Да что ты извиняешься всё время?"
    sl "Hey, stop excusing all the time."
    #me "Изв...{w} А где ты меня искала?"
    me "Sorr... I mean, so...{w} Where were you looking for me?"
    #sl "Везде в лагере. И в лесу."
    sl "Everywhere in the camp. And in the forest."
    #me "И не нашла в лесу?"
    me "And you didn't see me in the forest?"
    #sl "Нашла, ты спал. Решила не будить."
    sl "I did. You were sleeping. I decided not to wake you up."
    play music music_list["eternal_longing"] fadein 4   
    #me "Лучше бы разбудила, мне такие кошмары снились."
    me "It would have been better if you did. I was having terrible nightmares."
    show sl surprise pioneer  
    #sl "Какие?"
    sl "About what?"
    #me "Например, ты меня прогнала, потому что я солнце тебе загораживал."
    me "For instance, you yelled at me because I was blocking the sun for you."
    show sl smile pioneer  
    #sl "Звучит не очень жутко."
    sl "That doesn't sound very terrible."
    #me "И всё в таком духе. Но ты же не такая?"
    me "It was stuff like that. I know you're not that kind of girl."
    #sl "Не знаю, ты же мне сейчас ничего не загораживаешь. {w}Проведём эксперимент?"
    sl "I don't know, you're not blocking anything right now. {w}Let's do an experiment?"
    window hide
    show sl smile pioneer close  with dspr
    window show
    #"Славя придвинулась ко мне почти вплотную и взяла за плечи, чтобы слегка развернуть. Наверное, со стороны это смотрелось странно."
    "Slavya leaned very close to me and grabbed my shoulders to gently move me. It probably looked strange."
    #"Как назло, именно в этот момент подошла Саманта. Она заметила нас и застыла в нерешительности. Из-за моей реакции Славя тоже смутилась и отпрянула."
    "And of course Samantha approached at this moment. She saw us and froze in confusion. My reaction made Slavya embarrassed as well."
    show sl surprise pioneer  with dspr  
    menu:
        #"Подойти к Саманте":
        "Go to Samantha":
            #me "Прости, долг зовёт."
            me "Sorry, duty calls."
            hide sl  with dspr
            $ sp_sl -= 1
            #"Славя кивнула, и я подошёл к Саманте. Похоже, эта сценка произвела на неё впечатление."
            "Slavya nodded, and I went over to Samantha."
            jump ss_d4afd_eng
        #"Подозвать Саманту":
        "Call Samantha":
            #"Я постарался принять беззаботный вид и жестом пригласил Саманту к нам. Слегка промедлив, она подошла."
            "I tried to take a lighthearted look and called to Samantha. She hesitated a bit, but came over to us."
            show ss serious pilot casual at fright with dspr
            #me "Чего ты так долго? Мы уже почти без тебя ушли."
            me "{enen}What took you so long? We almost left without you.{/enen}"
            show sl sad pioneer  
            show ss angry pilot casual at fright            
            #"В качестве ответа Саманта только стрельнула взглядом."
            "In response, Samantha only shot a brief glance."
            #sl "Никуда мы не уйдём такими темпами. Тут и половины лагеря нет, пора идти по домикам - народ собирать. Спрошу вожатую."
            sl "We're not going anywhere at this pace. Half of the camp is absent, so we should go around to all the cabins - to gather the folks. I'll ask Olga Dmitrievna."
            window hide
            $ renpy.pause (0.4)
            show ss unsure pilot casual at fright 
            hide sl
            with dspr
            $ renpy.pause (0.4)            
            window show
            #"Славя ушла, а мы с Самантой какое-то время неловко топтались на месте."
            "Slavya left, and Samantha and I stood awkwardly for a while."
            
            jump ss_d4afd_eng
        #"Продолжить эксперимент":
        "Continue the experiment":
            $ sp_sl += 1
            $ sp_ss -= 1
            if sp_spts == 2:
                $ sp_ss += 1
            if sp_ss >= 6:
                $ sp_ss -= 1
            #me "Ну так что?"
            me "So... what?"
            #sl "Ээ... Кажется, меня вожатая звала. Потом поговорим."
            sl "Um... I think Olga is calling me. Talk to you later."
            hide sl  with dspr
            #"Я проводил её взглядом, а затем вернулся к Саманте."
            "I looked at her for a bit and then returned to Samantha."
            #me "Всё в порядке?"
            me "{enen}You alright?{/enen}"
            stop music fadeout 3
            show ss angry pilot casual with dspr         
            #ss "В полном."
            ss "{enen}Absolutely.{/enen}"
            window hide
            $ renpy.pause (0.7)
            jump ss_d4afdr_eng

label ss_d4afd_eng:
    window hide
    $ renpy.pause (0.4)
    show ss serious pilot casual with dissolve  
    $ renpy.pause (0.4)    

    menu:
        #"Оправдываться":
        "Make excuses":       
            #me "Мы тут со Славей болтали..."  
            me "{enen}Me and Slavya were only chatting...{/enen}"              
            #ss "Ничего, у нас старшеклассники и не так {i}болтают{/i} на людях."
            ss "{enen}It's okay, our high school students {i}chat{/i} even more in public.{/enen}"
            #me "Это было не то, что тебе показалось."
            me "{enen}It wasn't what you thought.{/enen}"
            show ss unsure pilot casual with dspr         
            #ss "Всё равно это не моё дело."
            ss "{enen}That's not my business anyway.{/enen}"
            #me "Не хочу, чтобы ты плохо думала о Славе."
            me "{enen}I don't want you to think badly of Slavya.{/enen}"
            if sp_ss >= 6:
                #ss "Не вижу повода."
                ss "{enen}I don't see a reason for that.{/enen}"
                if sp_spts == 2:
                    #ss "И вообще, это уже не важно."
                    ss "{enen}Besides, it doesn't matter anymore.{/enen}"
                    me "{enen}Anymore?{/enen}"
                    # ss "Ты вчера мне ответил насчёт Слави, так что давай закроем эту тему."
                    ss "{enen}You told me about Slavya yesterday, so let's just drop it.{/enen}"
                    jump ss_d4afdr_eng
                #me "По тебе и не скажешь."
                me "{enen}I'm looking at you and I see something else.{/enen}" #Not sure what this means.
                show ss serious pilot casual with dspr                
                #ss "Ну извини. Просто немного шокировало, что вы... у всех на глазах..."
                ss "{enen}Sorry, I was just a bit shocked that you did it... in public...{/enen}"
                #me "Ну что мы, что?!"
                me "{enen}What?! We did what?!{/enen}"
                show ss shy pilot casual with dspr    
                #ss "Целовались. Думала, у вас так не принято."
                ss "{enen}Kissing. I thought it wasn't accepted here.{/enen}"
                #"Наш разговор стал привлекать внимание окружающих. Я перешёл почти на шёпот."
                "Our conversation has begun to attract the attention of others. I was almost whispering."
                #me "Не было ничего такого."
                me "{enen}It was nothing like that.{/enen}"
                show ss unsure pilot casual   
                #ss "А чего тогда так испугался, когда меня увидел?"     
                ss "{enen}Why were you so scared when you saw me then?{/enen}"  
                #me "Наверное, предвкушая этот разговор. Такая пытка, и из-за чего?"
                me "{enen}Perhaps in anticipation of this conversation. Such torture, and because of what?{/enen}"
                show ss nosmile pilot casual                  
                #ss "Оправдываешься, потому что виноват."
                ss "{enen}You're making excuses because you're guilty.{/enen}"
                #me "Железная логика. В чём я виноват, если ничего не было?"
                me "{enen}Iron logic. What am I to blame for if nothing happened?{/enen}"
                show ss shy pilot casual                     
                #ss "Ты почувствовал себя виноватым ещё тогда. В чём? Мне тоже интересно. Это ведь... Всего лишь я."
                ss "{enen}You felt guilty. Why, for what?.. I'm interested as well. It's... just me.{/enen}"
                menu:
                    #"Не следовало оправдываться":   
                    "Should not make excuses":                      
                        #me "С тобой это не связано. Славя меня немного смутила, а ты была случайным свидетелем - вот и всё."
                        me "{enen}It's not connected to you. Slavya confused me a bit and you were a random witness - that's it.{/enen}"
                        show ss sad pilot casual     
                        #ss "Понятно."
                        ss "{enen}I see.{/enen}"
                        #me "И ты сама заставила меня оправдываться, а теперь как бы и не при делах."
                        me "{enen}You forced me to justify myself and now it's like it's about you.{/enen}"
                        #th "И не делай эти щенячьи глаза, это меня тут хотят на поводок посадить."
                        th "And don't make those puppy-dog eyes. Seems it's me on the leash here, not you!"
                        $ sp_ss -= 1
                        stop music fadeout 3
                        show ss serious pilot casual with dspr                
                        #"Саманта промолчала. Похоже, обиделась."
                        "Samantha didn't answer. She seemed quite upset, but I had nothing to add."
                        jump ss_d4afdr_eng

                    #"Не всего лишь":
                    "Not just":
                        #me "Всего лишь? Не прибедняйся."
                        me "{enen}What a modesty. You're not \"just\".{/enen}"
                        window hide
                        show ss smile2 pilot casual                     
                        $ sp_ss += 1
                        $ renpy.pause (1)
                        window show
                        #"Пионеры вокруг стали строиться в колонну по двое. Я поклонился и подал Саманте руку."
                        "Pioneers began to arrange themselves in lines of two. I bowed and gave Samantha a hand."
                        #me "Можно пригласить мадам на лесную прогулку?"
                        me "{enen}May I invite the lady for a walk in the forest?{/enen}"
                        ss "{enen}Yes, you may.{/enen}"                        
                        window hide
                        $ renpy.pause (0.5)
                        stop music fadeout 3
                        jump ss_d4afdr_eng

                    #"Закроем глупую тему":
                    "Let's close this silly theme":
                        stop music fadeout 3
                        #"Хватит об этом. Смотри, пионеры уже строятся."
                        me "{enen}Enough of this. Look, the pioneers are almost ready.{/enen}"
                        jump ss_d4afdr_eng
            #ss "...Да, ты прав. Показалось, значит. Наверное, это просто я испорченная."
            ss "{enen}...Yes, you're right. Maybe I'm just spoiled.{/enen}"
            stop music fadeout 3
            #me "Ну что ты. Вы обе очень порядочные, и вообще молодцы. Хочу быть как вы, когда вырасту."
            me "{enen}Oh, you. You're both very decent, and generally good people. I want to be like you when I grow up.{/enen}"
            show ss normal pilot casual 
            jump ss_d4afdr_eng

        #"Не оправдываться":
        "Don't make excuses":
            if sp_ss >= 6:
                $ sp_ss -= 1
            stop music fadeout 3        
            #th "Даже если она что-то не так поняла - неловко это обсуждать."
            th "Even if she misunderstood it - it's embarrassing to discuss."
            jump ss_d4afdr_eng
label ss_d4afdr_eng:
    window hide
    $ renpy.pause (0.7)
    stop ambience fadeout 3 
    $ night_time()
    $ persistent.sprite_time = "night"
    scene bg ext_no_bus_night  with dissolve
    #"Когда мы всё-таки выдвинулись в поход, уже совсем стемнело."
    "It was already dark when we finally set off for the hike."
    window hide
    $ renpy.pause (0.5)
    play ambience ambience_forest_evening    
    scene bg ext_path_night  with dissolve    
    $ renpy.pause (0.5)    
    #"Мы с Самантой шли сразу позади Ольги Дмитриевны, что ограничивало нас в темах для разговора."
    "We were right behind Olga Dmitrievna, so we had limited topics of conversation."
    play music Warm_evening fadein 5
    window hide
    show ss nosmile pilot casual with dissolve
    window show    
    #ss "Совсем мы пьесу забросили. Завтра же генеральная репетиция?"
    ss "{enen}We haven't rehearsed our play as it needed to be. Tomorrow is the final rehearsal, right?{/enen}"
    #me "Не будет, костюмы ещё не готовы."
    me "{enen}No it can't be. Our costumes aren't ready yet.{/enen}"
    #ss "А если без костюмов?"
    ss "{enen}And if we rehearse without the costumes?{/enen}"
    #me "Это будет не {i}генерально{/i}. И вообще, мне вот не до пьесы."
    me "{enen}Then it won't be that {i}final{/i}. Anyway, I can't do any rehearsals right now.{/enen}"
    show ss sad pilot casual     
    #ss "Почему? Из-за меня?"
    ss "{enen}Why? Is it because of me?{/enen}"
    #"Мне пришлось перейти на шёпот."
    "I had to speak in a whisper."
    #me "Конечно, из-за тебя."
    me "{enen}Of course it is.{/enen}"
    show ss nosmile pilot casual     
    #ss "...Но играть-то всё равно надо."
    ss "{enen}...But we can't abandon the play. We must continue anyway.{/enen}"
    #th "Как она может думать сейчас о пьесе? Детская беззаботность? Оно и к лучшему."
    th "How can she think about the play right now? What is it, some childish carelessness? {w}If it so, it's probably for the best..."
    #me "Как скажешь. Будем играть."
    me "{enen}Fine, if you say so. We will play.{/enen}"
    #ss "Обязательно будем."
    ss "{enen}Of course we will.{/enen}"
    if rom_eo == True:
        #me "Только я роль не доучил ещё."
        me "{enen}...But I haven't finished studying my role.{/enen}"
        #ss "Учи. Авось и в жизни пригодится. Поможет с девушками."
        ss "{enen}Keep on studying. Perhaps it will come in handy one day.{/enen}"
        #me "Уже помогло."
        me "{enen}It already has. {w}I just got this role, yet I'm already surrounded by girls.{/enen}"
        show ss normal pilot casual       
        #me "Да. Едва получив роль, я оказался в окружении одних только девушек. {w}Не считая Жени."
        me "{enen}...Excluding Zhenya, of course.{/enen}"
        show ss nosmile pilot casual         
        #ss "Зачем ты так? Это жестоко."
        ss "{enen}Don't say things like that. That's cruel.{/enen}"
        #me "Она заслужила."
        me "{enen}She deserves it.{/enen}"
        #ss "Заслужила оскорблений за глаза? Тебе должно быть стыдно. Тебе стыдно?"
        ss "{enen}She deserves insults to be told about her behind her back? You should be ashamed. {w}Are you?{/enen}"
        menu:
            #"Да":
            "Yes":
                # me "Теперь, когда ты это так повернула - да."
                me "{enen}Well, when you put it that way - yes, I am.{/enen}"
                show ss grin_smile pilot casual
                #ss "Ты слишком податлив."
                ss "{enen}You're so compliant.{/enen}"
                #me "Издеваешься?"
                me "{enen}Hey, are you mocking me?{/enen}"
                show ss laugh pilot casual              
                #ss "Да!"          
                ss "{enen}Yep!{/enen}"   
                #"Саманта подпрыгнула, чтобы хлопнуть меня сзади ногой, и рассмеялась. Ну что за ребячество?" with hpunch
                "Samantha jumped to slap my back with her leg, we're both laughed." with hpunch               
            #"Нет":
            "No":
                #me "Каждая шутка высмеивает кого-то или что-то. Что же теперь, и не шутить совсем?"
                me "{enen}Every joke mocks someone or something. Now what, I can't even joke?{/enen}"
                #ss "Но не каждая шутка обидна."
                ss "{enen}Not every joke is offensive.{/enen}"
                #me "Каждая."
                me "{enen}Every.{/enen}"
                #ss "Ладно, слушай. Водитель подобрал на дороге женщину. Она ему говорит \"за 300 баксов я сделаю всё, что угодно\"."
                ss "{enen}Okay, listen to this one. The driver picks up a hitch-hiking woman on the side of the road. She tells him \"for 300 bucks, I will do anything you want\".{/enen}"
                show ss nosmile pilot casual 
                ss "{enen}He agrees, then they drive to his place...{/enen}"

                #me "Ничего себе! Да это уже оскорбительно для любой феминистки."
                me "{enen}Wow! That's offensive to any feminist out there.{/enen}"
                show ss unsure pilot casual 
                me "{enen}...And also it's an adult joke, you must forget it right now.{w} But first you need to tell me the ending.{/enen}"                
                show ss nosmile pilot casual 
                ss "{enen}The ending was \"Paint my house\". {w}Fine, here is another:{/enen}"                
                #me "У нас и за меньшую сумму покрасят..."
                me "{enen}In our country you can get your house painted for less than that...{/enen}"
                show ss surprise pilot casual                 
                #ss "Да ты слушай!"
                ss "{enen}Keep listening!{/enen}"
                show ss normal pilot casual               
                #ss "Парень сидит дома, слышит стук в дверь. Открывает - на улице никого, только улитка на крыльце. Он взял её и закинул к соседу за забор."
                ss "{enen}This guy is sitting at home, when he hears a knock on the door. He opens the door - the street is empty, but there is a snail on the porch. He picks it up and throws it over his neighbor's fence.{/enen}"
                #ss "Прошёл год. Парень опять слышит стук в дверь, открывает - та же улитка на крыльце. Улитка говорит:\"ну и какого хрена?\""
                ss "{enen}A year passes. The guy hears the knock again, and when he opens the door, he sees the same snail on the porch. The snail asks: \"what the heck?\"{/enen}"
                #me "Ругательство в конце. Без него не будет эффекта."
                me "{enen}Swearing in the end. The joke won't be funny without it.{/enen}"
                show ss unsure pilot casual              
                #ss "Ладно, сейчас..."
                ss "{enen}Alright, wait...{/enen}"
                #me "Бесполезно."
                me "{enen}This is pointless.{/enen}"
                #ss "Трое попали на необитаемый остров. Когда уже почти умерли с голода, к берегу прибило бутылку с джинном. Первый загадывает желание..."
                ss "{enen}Three guys end up on an uninhabited island. When they are almost dead from starvation, a bottle with a genie inside washes ashore. The first guy wishes...{/enen}"
                #me "Эту я знаю. Третий загадывает ящик водки и всех обратно."
                me "{enen}I know this one. The third guy wishes for a box of vodka and both of them back.{/enen}"
                show ss grin_smile pilot casual with dspr          
                #ss "Ужасный слушатель. Ну и?"
                ss "{enen}You're a terrible listener. So?{/enen}"
                #me "Суть шутки - злорадство над теми двумя."
                me "{enen}The point of this joke is that he gets one over on those two. So it's evil!{/enen}"
                show ss surprise pilot casual with dspr             
                #ss "Ты придираешься. Пусть так, но есть же полно невинных детских шуток."
                ss "{enen}You are seeking fault. Even if you right, there are a lot of innocent jokes for kids.{/enen}"
                show ss normal pilot casual                 
                #me "Ребёнку пальчик покажи - он засмеётся. А смешная шутка должна быть злой."
                me "{enen}A small kid would laugh just seeing his own nail. But a real joke must be offensive... At least for someone.{/enen}"

    else:
        #me "Охота же посмотреть на вашу со Славей любовь."
        me "{enen}Ahh, the love between you Slavya... this Romeo of yours. I can't miss it.{/enen}" #Wait, what??? #Slavya is Romeo. - proofreader of proofreader of translator
        show ss surprise pilot casual         
        # "Саманта толкнула меня плечом."
        "Samantha nudged me."
        show ss normal pilot casual       
        #ss "Из всего лагеря только ты шутишь по этому поводу."
        ss "{enen}Out of everyone in the camp, only you are joking about this.{/enen}"
        #me "Просто другие стесняются тебя."
        me "{enen}The others are more cautious near you.{/enen}"
        #ss "Не только это."
        ss "{enen}Not just that.{/enen}"
        #me "А что тогда?"
        me "{enen}What then?{/enen}"
        show ss unsure pilot casual 
        #ss "Ты не похож на остальных пионеров. Ты больше похож на американца в этом плане."
        ss "{enen}You are not like the other pioneers, speaking of that. You think... more like an American.{/enen}"
        #me "Чем же?"
        me "{enen}What do you mean?{/enen}"     
        #ss "Сидит в тебе какая-то чертинка. Вот Славя даже и не поймёт, о чём ты."
        show ss normal pilot casual        
        ss "{enen}There is a voice of little devil in you!{/enen}"
        show ss nosmile pilot casual         
        extend " {enen}For example, Slavya won't even understand what are you talking about.{/enen}"
        #me "Я должен на это обидеться? Впрочем, ты права. Конечно, я не такой, ведь я из другой эпохи. Веришь мне теперь?"
        me "{enen}Should I take offense? However, you are right. Of course I'm not like them; I'm from another era. {w}Do you believe me now?{/enen}"
        show ss serious pilot casual   
        #ss "Ах, опять про это."
        ss "{enen}Oh, not this again.{/enen}"

    window hide
    $ renpy.pause(0.5)  
    scene bg ext_polyana_night  
    with dissolve   
    $ renpy.pause(0.5)     
    #mt "Вот мы и на месте. А теперь собираем хворост. Только не вздумайте живые ветки ломать!"
    mt "Here we are."
    window hide
    show ss nosmile pilot casual at left
    show mt normal panama pioneer 
    show us normal pioneer at right 
    with dissolve
    $ renpy.pause(0.5)    
    mt "Now, we must collect some firewood. But be careful, look under your feet.{w} And don't break down any green branches!"
    #"Пионеры стали неохотно расходиться по пугающему ночному лесу."
    "Pioneers began to reluctantly disperse into the scary night forest."
    #mt "Семён, далеко не отходите."
    mt "{enen}Semyon, don't go too far.{/enen}"
    window hide
    hide mt  with dspr
    window show
    #me "Конечно, я не хочу пойти на ужин Леопольду."
    me "Of course I wouldn't. I don't want to be Leopold's dinner."
    #us "Какому ещё Леопольду? Коту?"
    us "What Leopold? The cat from the cartoon?"
    show ss surprise pilot casual at left 
    #me "Льву! Из зоопарка сбежал, разве не слышала? Ох и здоровый!"
    me "No, a lion! He escaped from the zoo, haven't you heard about it? Oh, he's so massive!"
    show us dontlike pioneer at right   
    #us "Тут нет зоопарков!"
    us "There is no zoo around here!"
    show ss normal pilot casual at left 
    #me "Зоопарк мимо ехал. Как передвижной цирк, только зоопарк. А дороги тут - сама знаешь."
    me "The zoo was passing by. Like a traveling circus, only it's a zoo. But it didn't make it far through our roads..."
    #us "Ну?"
    us "And?"
    #me "Поломались, застряли, потом смотрят - льва нет. А от дрессировщика одна шапка осталась."
    me "They broke down, got stuck for a while. The repairs was noisy, and Leopold was a smart lion. It broke down it's cage in the right moment. By the time they was ready to move - Leopold was gone."
    me "They looked for him, searched the woods, but all what they found was a hat. The hat was all that was left of the animal trainer."

    #us "Не было такого! Я бы знала!"
    us "Can't be! It couldn't happen here, I would know!"
    #me "Это засекречено, иначе все разбегутся. Вот ты думаешь, почему старый корпус забросили?"
    me "It's confidential, otherwise everyone's gonna panic and the camp will be a dead place. Why do you think the old campus is abandoned?"
    #us "Почему?"
    us "Why?"
    #me "Слишком близко к лесу был. Леопольд туда ходил завтракать как в столовую."
    me "Too close to the forest - it was Leopold's favorite feeding place. He visited it every day as though it were a canteen!"
    show ss smile pilot casual at left 
    #us "Врёшь!"
    us "You're lying!"
    #me "Не веришь - иди вперёд тогда. Но помни: лев ступает бесшумно."
    me "If you don't believe me - then keep going. But remember: the lion steps silently."
    show us sad pioneer at right   
    #us "Эээ, я лучше старые головешки помогу убрать."
    us "Uh... I'll just help with the old firebrands."
    window hide
    $ renpy.pause (0.4)
    hide us  with dspr
    $ renpy.pause (0.7)
    window show
    #ss "Зачем ты её напугал?"
    ss "{enen}Why did you frighten her?{/enen}"
    #me "Это традиция. Пошли. Или льва боишься?"
    me "{enen}Just for tradition... {w}Let's go? Or are you afraid of the lion?{/enen}"
    window hide
    $ renpy.pause (0.4)    
    scene bg ext_path2_night  
    show ss normal pilot casual
    with dissolve   
    #ss "Я видела чучела двух африканских львов-людоедов в музее."
    ss "{enen}I saw the lions only once. In was two stuffed African lions in the museum.{/enen}"
    #me "Отлично, ты должна рассказать про них у костра. Можешь немного приврать. Если сожрали пятерых, скажи - двадцать."
    me "{enen}Perfect, you should tell about them by the fire. A-and you can add a little lie too. If they ate five men, say there were twenty, it won't harm.{/enen}" 
    show ss surprise pilot casual  
    #ss "Вообще-то жертв было полторы сотни."
    ss "{enen}Actually, there were a hundred and fifty victims. I remember that.{/enen}"
    show ss unsure pilot casual   
    #me "Ой, нет, это много. Эти негры там что, в очередь выстраивались?"
    me "{enen}Oh, no, that's too many. How it that possible? Were those negroes lining up?{/enen}"
    show ss serious pilot casual with dspr   
    #ss "Это что за расизм?"
    ss "{enen}What kind of a racist speech is that?{/enen}"
    #me "У нас это слово не оскорбительное."
    me "{enen}Why? That word is not offensive here.{/enen}"
    #ss "Я слышала нотки пренебрежения."
    ss "{enen}Really? I also heard notes of neglect.{/enen}"
    #me "Ну, да, а за что их тут похвалить? Царь природы унижен царём зверей."
    me "{enen}Well, there was... Because there's nothing to praise them for. The king of nature was humbled by the king of animals... One hundred and fifty times?{/enen}"
    show ss nosmile pilot casual   
    #ss "Вообще-то, это на строительстве железной дороги было. Вряд ли там были только чёрные."
    ss "{enen}Actually, it was during the construction of a railway. I don't think there were only black people.{/enen}"
    stop music fadeout 5
    #me "Считаешь, что чёрные сами построить не в состоянии? Звучит, как расизм."
    me "{enen}You think black people can't build it by themselves? Sounds racist to me.{/enen}"
    window hide
    scene bg ext_polyana_night  with dissolve    
    #ss "Это не то! ...Смотри, там Лена. Подойдём к ней?"
    ss "{enen}That's not it!.. Look, there's Lena. Should we talk to her?{/enen}"
    window hide
    $ renpy.pause(0.5)     
    show un angry2 pioneer far at center   with dspr
    window show
    play music music_list["faceless"]  fadein 5
    #"В свете луны Лена казалась особенно бледной. Было жутковато, но раз уж мы не побоялись льва..."
    "In the light of the moon, Lena looked very pale even for her. It was spooky enough, but since we didn't fear the lion..."
    #"Мы сделали несколько шагов вперёд, но вдруг услышали голос - Лена была не одна."
    "We took a few steps forward, but suddenly heard a voice - Lena wasn't alone."
    #"Саманта потянула меня за рукав назад и вниз, чтобы я пригнулся вслед за ней. Теперь будем подслушивать?"
    "Samantha pulled me back and down by the sleeve so I would duck behind her."
    th "What, we're going to spy now? It looks so."
    #un "...виновата во всех твоих бедах, но теперь-то что?"
    un "...guilty in all your troubles, but now what?"
    #un "Говорю же, услугу тебе оказала. А ты опять недовольна."
    un "I'm telling you, I did you a favor. Yet you're unhappy again."
    #th "Сейчас выяснится, что она сама с собой говорит. Душить мерзких хобитцев, моя прелесть."
    th "And now it will come out that she talks to herself. {i}Choke nasty hobbits, my precious...{/i}"
    window hide
    $ renpy.pause(0.5) 
    show dv guilty pioneer far at left with dspr
    window show
    #dv "Нельзя так. Я сама не ангел, но мне за тебя стыдно."
    dv "This is all wrong. Yes, I'm not an angel myself, but this time I'm ashamed of you."
    #un "Ну... Не хочешь - можешь щёлкать клювом дальше, в знак протеста. Белобрысая такой подарок примет с радостью."
    un "Well... If you don't want to - you can keep clicking your beak in protest. The blondie would love this present." #Clarification on tow-haired? Translates to "blonde". Is she referring to Slavya?
    #dv "Нельзя дарить то, что тебе не принадлежит."
    dv "You can't gift something that doesn't belong to you."
    #un "Благодарности и не ожидала. Просто поставила тебя в известность."
    un "Ah, didn't expect your gratitude anyway. I'm just letting you know."
    #dv "Всё равно я тебе не верю. Ты будешь и дальше добиваться своего."
    dv "I don't believe you anyway. You're gonna keep pursuing this."
    show un smile2 pioneer far at center   
    #un "После той сцены? С ума сошла?"
    un "After that scene? Are you crazy?"
    #dv "Это только добавит тебе интереса, я же знаю. И говоришь ты это всё не из благих побуждений."
    dv "I know you too well, it will only make things more interesting for you. And what you're saying to me - it comes not from the good intentions."

    show un normal pioneer far at center    
    #un "Плохая девочка читает мораль хорошей. Вот бы увидел кто."
    un "The bad girl lecturing the good girl. If anyone could see this..."
    stop music fadeout 5
    show dv smile pioneer far at left   
    #dv "Осторожней с желаниями."
    dv "Careful with your wishes."
    show un surprise pioneer far at center   
    #"Алиса покосилась в нашу сторону. Лена насторожилась."
    "Alisa glanced in our direction. Lena cringed."
    show un rage pioneer far at center  
    #un "Что? И ты не сказала? Ты!"
    un "What?.. And you didn't warn me? You!"
    window hide
    $ renpy.pause(0.5)     
    hide un  with dspr
    window show
    play sound sfx_bush_leaves
    #"Лена метнулась прочь, в темноту. А нам пришлось выйти из укрытия."
    "Lena ran away in the darkness. Now we had to come out of our hiding spot."
    window hide
    $ renpy.pause(0.4)     
    show ss sad pilot casual at cright    
    show dv guilty pioneer at left  
    with dspr
    $ renpy.pause(0.5)     
    window show
    #dv "Вот не хотела в этот дурацкий поход идти, да Лене поговорить приспичило. Ну поговорили. Чуть не ударила меня."
    dv "I didn't want to go on this bloody hike, but Lena was itching to talk. Well, we talked. {w}She nearly punched me."
    #ss "Это так неловко. Всё моя вина."
    ss "{enen}This is so embarrassing. It's my fault.{/enen}"
    #me "Мы не хотели подслушивать."
    me "We didn't mean to eavesdrop."
    show dv normal pioneer at left   
    #dv "А был другой повод прилечь в кустах?"
    dv "Hah. Is there any other reason for laying down in the bushes?"
    show ss shy2 pilot casual at cright 
    #me "...Надо вернуть Лену."
    me "...I guess we should bring Lena back."
    #dv "Это без меня, лично я иду к костру. Замёрзла. Да ничего с ней не сделается."
    dv "That's without me - I'm going to the campfire. I'm freezing."
    show dv normal pioneer at fleft with dspr   
    dv "Don't waste your time; she's gonna be fine."
    window hide
    $ renpy.pause(0.5)     
    hide dv  with dspr
    $ renpy.pause(0.3)      
    window show
    # "Алиса ушла. Я посмотрел на Саманту - она закрывала рот рукой, а взгляд у неё был ошалелый."
    "Alisa left. I looked at Samantha - she'd covered her mouth with her hand, and her look was spacy."
    show ss shy pilot casual at cright 
    #th "Это её фраза про кусты так смутила? Да, это был перебор."
    th "Did the phrase about the bushes embarrass her that much? Yeah, it was something..."  
    show ss laugh pilot casual at cright   with dspr  
    #"Но Саманта заливисто рассмеялась, а через секунду я к ней присоединился. {w}Когда мы успокоились, я вспомнил про нашу беглянку."
    "Samantha started to laugh, and in a second I joined her. {w}When we calmed down, I remembered our forest fugitive." 
    show ss grin_smile pilot casual at cright 
    #me "Ты иди к костру, а я поищу Лену."
    me "{enen}Go to the campfire, okay? I'll look for Lena a bit...{/enen}"
    show ss smile2 pilot casual at cright with dspr
    #ss "Я с тобой."
    ss "{enen}I'm with you.{/enen}"
    stop ambience fadeout 3
    #me "Вожатая тебя быстро хватится, нам влетит. Особенно мне. А холод тут какой? Да и потеряться можно."
    me "{enen}Won't do, the counselor will notice that you're missing. We'll end up in trouble, especially me. Plus, it's cold here...{/enen}" 
    show ss nosmile pilot casual at cright   
    #ss "Высказался? А теперь пошли. Не бойся, я умею ориентироваться в лесу."
    ss "{enen}Done talking? Let's go then. {w}Don't worry, I know how to navigate in the woods.{/enen}"
    th "I probably can't talk her out of this..."
    window hide
    $ renpy.pause(0.5)     
    scene bg ext_path2_night
    show ss normal pilot casual 
    with dissolve
    play ambience ambience_forest_night
    play music music_list["into_the_unknown"]
    #"И мы углубились в лес. С каждым шагом мне всё больше хотелось вернуться к костру, но мужская гордость не позволяла мне это озвучить." 
    "We went deep into the forest. With every step I wanted to get back to the fire, but my dignity wouldn't let me say that."
    show ss unsure pilot casual   
    #ss "Так стыдно за то, что заставила тебя подслушивать. Это не в моем духе, честно."
    ss "{enen}I'm really sorry I made you to eavesdrop. This isn't my thing, honestly.{/enen}"
    #me "Я тоже виноват. Ты-то даже не понимала, о чем говорят."
    me "{enen}There is my fault as well. And, technically, you can't overhear people here. You don't understand a word.{/enen}"
    show ss nosmile pilot casual      
    #ss "Но ведь я понимала."
    ss "{enen}I understood enough.{/enen}"
    #me "Как это? Даже я мало чего понял."
    me "{enen}How so? It was a strange chat even for me.{/enen}"
    #ss "Сама не знаю. Но вот так."
    ss "{enen}I'm not sure how it happens... Maybe it's something between us, girls?{/enen}"
    #me "А ведь я и про кусты тебе не переводил. Да, интересные дела творятся. Ну и что ты поняла из разговора?"
    me "{enen}Hm. Okay, and what did you understand?{/enen}"
    show ss shy pilot casual       
    #ss "Мне не очень удобно это обсуждать."
    ss "{enen}I... I don't feel comfortable discussing it.{/enen}"
    #me "Хорошо, а почему Лена так отреагировала?"
    me "{enen}I see. Can you tell then why did Lena react like that?{/enen}"
    show ss nosmile pilot casual    
    #ss "Похоже, Алиса её хорошо знает, а открываться чужим Лене не хочется."
    ss "{enen}Seems like Alisa knows her well and Lena doesn't want to open up to others.{/enen}"
    #me "Но ведь нам-то она открылась?"
    me "{enen}But she did to us.{/enen}"
    show ss serious pilot casual   
    #ss "Нет. Это было не то, что у неё на душе."
    ss "{enen}It doesn't looks so. That wasn't really what she had on her mind.{/enen}"
    #"Луна окончательно скрылась за тучами, оставив нас почти в кромешной тьме."
    "The Moon finally disappeared behind the clouds, leaving us in near-darkness."
    window hide
    $ renpy.pause (0.4)
    scene bg dfoggyforest  
    show ss nosmile pilot casual
    with dissolve   
    window show
    #me "Мы же к лагерю идём?"
    me "{enen}Are we going to the camp?{/enen}"
    #ss "Без понятия."
    ss "{enen}No idea.{/enen}"
    #me "Я думал, ты нас ведёшь!"
    me "{enen}I thought you were leading us!{/enen}"
    #ss "Нет, мы просто идём вперёд. Давай ещё раз позовём Лену."
    ss "{enen}No, we're just going forward. Let's call Lena again.{/enen}"
    #me "Пора и звать на помощь сразу - мы заблудились. Самое время применить это твоё ориентирование."
    me "{enen}Let's call for help as well - we're lost!{/enen}"
    show ss normal pilot casual
    ss "{enen}Hey. Don't panic so fast in front of a girl.{/enen}"    
    me "{enen}I don't panic... But I don't like our position either! It's time to apply your orientation.{/enen}"
    
##Not sure if the line is meant to be translated this way-I am assuming that he means that it's too late to call for help. Or does he mean that it IS time to call for help?
    #ss "У меня есть компас в перочинном ноже. Только не видно же ничего."
    show ss unsure pilot casual    
    ss "{enen}I have a compass in my penknife. But it's so dark, I can't see the arrow...{/enen}"
    #me "Это дерево поросло мхом с одной стороны. Что это значит?"
    me "{enen}Let's wait for more light...{w} Or we can do this the old way. This tree is covered with moss on one side: what does that mean?{/enen}"
    #ss "Что тут можно отдохнуть на мягком?"
    show ss normal pilot casual    
    ss "{enen}That we can rest here?{/enen}"
    #me "...Угу. Давай."
    me "{enen}...Yeah. Exactly.{/enen}"
    window hide 
    $ renpy.pause(0.3)    
    hide ss with dspr
    $ renpy.pause(0.5)
    window show 
    #th "Мы пропадём тут."
    th "We're going to die here."
    #ss "Да не бойся, сейчас луна выглянет, и пойдём на юг. Тогда выйдем либо к лагерю, либо к дороге, которая его пересекает."
    ss "{enen}Don't be afraid, soon the sky will be cleared, we'll see the way, or just go south. Then we'll get to the camp or the road leading to it.{/enen}"
## cg?
    stop music fadeout 5
    #"Мы разместились под деревом и стали ждать просветления неба."
    "We sat down under a tree and began our wait for the Moon."
    #me "Ты всё ещё не хочешь говорить о... будущем?"
    me "{enen}I'd like to speak about... the future. If you're in the mood for that.{/enen}"
    window hide
    $ renpy.pause(0.4)
    show ss nosmile pilot casual with dspr
    window show     
    ss "{enen}Okay. Speak.{/enen}"
    #me "Что ты будешь делать, когда вернёшься домой?"
    me "{enen}What will you do when you get back home?{/enen}"
    play music music_list["dance_of_fireflies"] fadein 5
    #ss "Меньше чем через месяц у меня начинаются съёмки на телевидении. Здорово, да? Ну и учёба потом."
    ss "{enen}In less than a month, I'll start shooting for TV. Sounds great, isn't it? And also I will be studying, of course.{/enen}"
    #me "Значит, ты будешь в разъездах? Это опасно. Как и перелёт до Америки."
    me "{enen}What about TV? Is it some local channel?{/enen}"    
    ss "{enen}No, we don't have that. At least no local channels had contacted us.{/enen}"    
    me "{enen}So you'll be traveling? That's dangerous. {w}As well as your flight back to the US.{/enen}"
    show ss normal pilot casual 
    #ss "Предлагаешь остаться в СССР?"
    ss "{enen}Do you suggest that I stay in the USSR?{/enen}"
    #me "Боюсь, у меня нет таких полномочий."
    me "{enen}I'm afraid I don't have the authority for that.{/enen}"
    show ss serious pilot casual     
    #ss "У меня тоже. Это перечеркнёт всё..."
    ss "{enen}Neither do I. It would... simply destroy everything.{/enen}"
    #me "Не осталась бы ни при каких обстоятельствах?"
    me "{enen}You won't stay under any circumstances?{/enen}"
    #ss "А какие ещё могут быть обстоятельства?"
    ss "{enen}What circumstances could there be? What else?{/enen}"
    #me "Они и так достаточно серьёзные, на мой взгляд."
    me "{enen}I think they're already serious enough.{/enen}"
    show ss unsure pilot casual   
    #ss "Надеюсь, когда-нибудь я смогу вернуться и пожить у вас подольше. Но не сейчас."
    ss "{enen}Perhaps one day I could come back and stay longer. But not now.{/enen}"
    #me "Можно придумать какой-нибудь повод."
    me "It's possible to find an excuse."
    show ss nosmile pilot casual  
    #ss "Это прозвучит высокопарно, но у меня есть долг. Хотя бы перед семьёй и близкими, если не перед страной."
    ss "{enen}It may sound pompous, but I have a duty. At least to my family and friends, or even for my country.{/enen}"
    #me "Долг важнее всего?"
    me "{enen}Duty before self?{/enen}"
    show ss unsure pilot casual   
    #ss "Я не солдат, присяги не давала. Конечно, в глубине души..."
    ss "{enen}I'm not a soldier, and didn't take an oath. Of course deep in my heart...{/enen}"
    #me "Что?"
    me "{enen}What?{/enen}"
    #ss "...Верится, что рождена я не только для службы обществу. {w}Ну, обычные девичьи мечты."
    ss "{enen}...I believe I've been born not only for public service. {w}Well, the usual girl's dreams.{/enen}"
    #me "Значит, есть что-то, что может заставить тебя забыть о долге, семье и мире во всём мире?"
    me "{enen}So there is something that could make you forget about duty, family and peace for everyone?{/enen}"
    show ss shy pilot casual 
    #ss "Может быть."
    ss "{enen}Maybe.{/enen}"
    #me "И что это, если не секрет?"
    me "{enen}What is that - if it's not a secret?{/enen}"
    show ss shy2 pilot casual     
    #ss "Секрет, конечно!"
    ss "{enen}Of course it's a secret!{/enen}"
    play sound sfx_owl_far
    show ss nosmile pilot casual    
    #"Где-то над нами крикнула сова. Я вспомнил про Лену. Она-то уже в тёплой постельке небось, пока мы тут страдаем."
    "An owl shouted somewhere above us. I remembered Lena. That she's probably in a warm bed by now while we're suffering here."
    #me "...Ну а тебе нравится в «Совёнке»? Не считая этого момента."
    me "{enen}...Do you like living in «Sovyonok»? Except for this moment, I mean.{/enen}"
    #ss "Нравится. Бывает беспокойно, конечно. Я не совсем такого ожидала."
    ss "{enen}I do. Though sometimes it's worrisome. It's not what I expected.{/enen}"
    #me "А ты бы хотела остаться тут надолго?"
    me "{enen}Would you like to stay here for a long time?{/enen}"
    #ss "Это как? Сторожем на зиму?"
    ss "Like how? I thought it closed for a winter."
    #me "Нет. Пусть всегда будет лето, солнце и прочие хорошие вещи."
    me "{enen}Forget about winter. What If the sun and other good things stay forever? Would you?{/enen}"
    #ss "Может быть. А ты?"
    ss "{enen}Maybe. And you?{/enen}"
    #me "...Да."
    me "{enen}...Yes.{/enen}"
    show ss smile2 pilot casual
    #ss "Ну тогда и я - да."
    ss "{enen}Then I would stay as well.{/enen}"
    window hide
    $ renpy.pause(0.7) 
    window show
    #me "Хотел бы я подарить его тебе."
    me "{enen}I wish I could gift it to you.{/enen}"
    #ss "Его?"
    ss "{enen}It?{/enen}"
    #me "«Совёнок»."
    me "{enen}«Sovyonok».{/enen}" 
    #ss "Как мило. Но в каком смысле - подарить?"
    ss "{enen}How sweet. But what do you mean - gift it?{/enen}"
    #me "Ну... У меня есть над ним какая-то власть. Почему бы и не такая."
    me "{enen}Sometimes it feels I have some power over it. Maybe even the power of that kind.{/enen}"
    #ss "А зачем его дарить, и почему именно мне?"
    ss "{enen}Why do you want to give it anyway? And why me?{/enen}"
    #me "У меня есть куда вернуться. Не бог весть что, но хоть знать бы, что с тобой всё в порядке."
    me "{enen}I have a place to go to. It's nothing special, but... I'll be fine there if I'll be sure there that you're okay.{/enen}"
    show ss unsure pilot casual 
    #ss "Слишком сложно для моего понимания."
    ss "{enen}Thank you, but... What you're saying is too hard to understand. I'm sorry.{/enen}" 
##Was Samantha meant to speak in Russian here? Translated: "I don't understand."
    #"Наконец, небо прояснилось, и мы смогли увидеть миниатюрную стрелку компаса. Теперь можно продолжить путь."
    "Finally the sky cleared and we could see the tiny compass needle. Now we could go."
    window hide    
    $ renpy.pause (0.5)
    scene bg ext_path2_night  with dissolve
    #"Шагать в темноте, зачастую на ощупь - было очень страшно, но перспектива мёрзнуть в лесу пугала не меньше. Потихоньку мы продвигались вперёд."
    "Walking in the dark, sometimes moving only by touch, was very scary, but so was the prospect of staying in the place, in the middle of freezing nowhere. Slowly we kept moving further."
    #ss "Значит, ты вернёшься в это своё не-бог-весть-что, а я без переводчика останусь? И зачем такой «Совёнок»?"
    ss "{enen}So you will go back to your nothing-special-but-still-a-place, and I will stay without a translator? Why will I need «Sovyonok» then?{/enen}"
    stop music fadeout 2    
    #me "Дарёному «Совёнку» в клюв не смотрят." 
    me "{enen}You don't judge a gift.{/enen}"
    window hide
    $ renpy.pause(0.4)     
    scene bg ssnightforest  
    with dissolve
    $ renpy.pause(0.5)     
    play sound sfx_alisa_falls
    show ss scared pilot casual with dspr    
    #ss "Ай!"
    ss "{enen}Ouch!{/enen}"
    window hide
    play music music_list["pile"] fadein 2
    hide ss with dspr
    window show
    #"Силуэт Саманты исчез, и тут же раздался второй крик, уже откуда-то снизу. Я упал на колени и попытался нащупать опору cпереди, но мои руки хватали только воздух. Передо мной была какая-то яма."
    "Samantha's silhouette disappeared, and I heard her second scream from somewhere below. I fell to my knees and tried to find the ground in front of me, but my hands grabbed only air. I was at the edge of some pit."
    #me "Саманта?!"
    me "{enen}Sam?!{/enen}"
    #"Ответа не последовало. {w}Зато луна сжалилась и выглянула из-за туч: проступили очертания оврага. К счастью, он оказался неглубоким."
    "There was no answer. At least the moon took pity on me and appeared from behind the clouds so I could see the pit. Fortunately, it wasn't too deep."
    #"Я помог Саманте выбраться." 
    "I helped Samantha to get out." 
    window hide
    $ renpy.pause(0.3) 
    show ss scared casual with dspr 
    window show
    #"На вид она была цела, но находилась в каком-то оцепенении."
    "She looked alright at first sight, was able to walk, but seemed very shocked."
    #ss "Меня что-то укусило. Очень больно."
    ss "{enen}Something bit me. It really hurts.{/enen}"
    show ss sad casual  
    #me "Дай посмотрю."
    me "{enen}Where? Let me see it.{/enen}"
    stop music fadeout 5
    #"Я стал разглядывать её голень. Наконец, присмотревшись, нашёл два тёмных пятнышка, на расстоянии сантиметра одно от другого."
    "I began to examine her shin. Finally, after looking, I found two dark spots with a distance of a centimeter between each other. "
    #me "Плохо видно. Но похоже на змеиный укус."
    play music the_sea_scene fadein 5 
    me "{enen}Hard to see. But looks like a snake bite.{/enen}"
    #ss "Я так и думала. Что же делать?"
    ss "{enen}Just what I thought. What should we do?{/enen}"
    
    show ss unsure casual   
    menu:
        #"Нужно скорее в лагерь":
        "We need to hurry to the camp":
            jump snake1_eng

        #"Нужно успокоиться":
        "You need to calm down":
            $ sp_snake += 1
            #me "Без паники. Такая маленькая змея не навредит такой большой девочке."
            me "{enen}No panic. Such a small snake can't hurt such a big girl.{/enen}"
            show ss sad casual               
            #ss "Но мне уже нехорошо."
            ss "{enen}But I feel bad already.{/enen}"
            menu:
                #"Нужно наложить жгут":
                "It is necessary to apply a tourniquet":
                    #ss "Кажется, так делают при укусе кобры. У вас водятся кобры?"
                    ss "{enen}It is what they do in case of a cobra's bite. Do you have cobras here?{/enen}"
                    #me "Нет, но невелика разница, наверное."
                    me "{enen}No, but there's not much difference, I think.{/enen}"
                    #"Я оторвал кусок рубашки и затянул на ноге пониже колена."
                    "I tore off a piece of my shirt and wrapped it around her leg, below the knee."
                    #ss "Ну а что теперь?"
                    ss "{enen}What's next?{/enen}"
                    jump snake1_eng

                #"Нужно отсосать яд":
                "I need to suck out the poison":
                    $ sp_ss += 1
                    $ sp_snake += 1
                    #ss "Наверное, это опасно?"
                    ss "{enen}Won't that be dangerous?{/enen}"
                    #me "Я ничем таким не болею."   
                    me "{enen}I'm not infected with anything.{/enen}"                         
                    #ss "Для тебя опасно."
                    ss "{enen}Dangerous for you, I mean.{/enen}"
                    #me "Безопасно. Абсолютно."
                    me "{enen}It's safe. Absolutely.{/enen}"
                    show ss serious casual  with dspr                    
                    #ss "Твоё \"абсолютно\" не добавляет уверенности."
                    ss "{enen}Your \"absolutely\" doesn't add any confidence.{/enen}"
                    #me "Всё будет хорошо."
                    me "{enen}Everything will be fine.{/enen}"
                    show ss unsure casual  with dspr                    
                    #"Я выдавил из ранок немного крови и стал изображать вампира, не забывая сплёвывать."
                    "I squeezed a little bit of blood from the wounds and began to act like a vampire, not forgetting to spit."
                    
                    window hide
                    $ renpy.pause(1)
                    #"Через две-три минуты процедуры взял передышку. Подумалось, что этого может быть достаточно."
                    "After two or three minutes, I took a break. I thought it might be enough."
                    menu:
                        #"Продолжать":
                        "Continue":
                            $ sp_snake += 1
                            #th "Но я только вхожу во вкус. Ещё пара минут не помешает."
                            th "But I've only started. Two more minutes won't harm."
                            window hide
                            $ renpy.pause(1.5)
                            show ss sad casual   with dspr                              
                            #ss "А сколько это нужно делать?"
                            ss "{enen}How much time do you need to do it?{/enen}"
                            #me "Пожалуй, хватит."
                            me "{enen}I guess that's enough.{/enen}"
                            #ss "Необычный опыт. Хотя приятного мало."
                            ss "{enen}It's a strange experience. Although not very pleasant.{/enen}"
                            jump snake1_eng
                        #"Достаточно":
                        "It's enough":
                            show ss nosmile casual                            
                            #ss "Спасибо."
                            ss "{enen}Thank you.{/enen}"
                            jump snake1_eng

label snake1_eng:
#    play music music_list["sunny_day"] fadein 5
#   me "Надо доставить тебя в лагерь."
    me "{enen}Let's take you to the camp now.{/enen}"
    show ss serious casual    with dspr     
#   ss "Голова кружится, но идти смогу, наверное."
    ss "{enen}I'm a bit dizzy, but I can walk, I suppose.{/enen}"
#   me "Погоди, лучше не беспокоить ногу. Носилки бы достать. Жди здесь, а я пойду за помощью."
    me "{enen}Wait, it is better not to strain your leg. I'd rather find a stretcher. Wait here, I'll call for help.{/enen}"
    show ss scared casual with dspr     
#   ss "Нет, только не уходи. Мне страшно."
    ss "{enen}No, don't leave! I'm scared.{/enen}"
    show ss serious casual  with dspr     
#   th "Пожалуй, не стоит оставлять её тут одну в темноте. Страх может быть похуже яда."
    th "Maybe it is better not to leave her here alone in the darkness. Fear may be worse than poison."
    menu:
#       "Тогда пошли":
        "Let's go, then":
            $ sp_snake -= 2
#           me "Обопрись на меня. Постарайся не беспокоить ногу."            
            me "{enen}Lean on me. Try not to strain your leg.{/enen}"
#           ss "Легко сказать."
            ss "{enen}Easier said than done.{/enen}"
            jump snake2_eng
#       "Я тебя понесу":
        "I'll carry you":
            $ sp_ss += 1
            show ss shy casual            
#           ss "Ты мой герой, Сэм."
            ss "{enen}You're my hero, Sam.{/enen}"
#           th "Сэм не может нести Кольцо за Вас, но Сэм может нести Вас!" 
            th "Sam can't carry the Ring for you, but Sam can carry you."
            window hide
            hide ss with dspr 
            $ renpy.pause(0.3) 
            window show         
#           me "Держись." with hpunch
            me "{enen}Hold tight.{/enen}" with hpunch
            window hide
            scene cg ss_carry1 with dissolve
            
            $ persistent.ssg_108 = True 
            
            $ renpy.pause(1.5)             
#           "Конечно, годы тренировок не прошли зря. {w}Но опыт онлайн игр в такой ситуации - слабый помощник."
            "Of course, years of practice were not spent in vain. {w}But online game experience is a poor aid in such situations."
#           ss "Столько проблем из-за меня. Ну куда я полезла?"
            ss "{enen}I've caused so many problems. Why did I go there?{/enen}"
#           me "Переживём... Это ведь только в фильмах..."
            me "{enen}We'll make it... You know, it only happens in movies...{/enen}"
#           ss "Что?"
            ss "{enen}What?{/enen}"
#           me "Две крайности бывает... \"Всего лишь царапина\" и \"бросьте меня, это конец\"."
            me "{enen}Always one of the two extremes... And nothing between \"just a scratch\" and \"leave me, I'm dead\".{/enen}"
#           me "А в жизни обычно помучаешься-помучаешься... Да и пройдёт... Потерпи."
            me "{enen}And in real life you usually lying down for a week and suffer... and then is over, you're healthy again... So hold on.{/enen}"
            $ renpy.pause(1.0)         
#           th "Ну почему мне не выдали тело Аполлона? И ночное зрение тоже не помешало бы."  
            th "Why wasn't I given the body of a god, like Apollo? And night vision would be useful too."
            $ renpy.pause(0.5)             
#           ss "Я тяжёлая, да?"
            ss "{enen}I'm heavy, aren't I?{/enen}"
#           me "Глупости..."         
            me "{enen}Nonsense...{/enen}"
#           "Через несколько десятков метров мне понадобился отдых. К счастью, луна снова спряталась за тучи, и я смог сослаться на то, что не видно дороги."
            "After a few tens of meters, I needed a rest. Fortunately the moon hid behind the clouds again and I excused myself, saying that I couldn't see the path."
            window hide
            scene bg dfoggyforest  with dissolve                
            window show         
#           me "Как ты там?"
            me "{enen}How are you doing?{/enen}"
            window hide
            $ renpy.pause(0.3)         
            show ss unsure casual with dspr
            if sp_snake >= 5:
                window show
#               ss "Нормально. Думаю, дальше смогу идти сама."                              
                ss "{enen}More or less. I think I can go further myself.{/enen}"
                window hide
                menu:
#                   "Давай попробуем":
                    "Let's try":
                        $ sp_snake -= 1
                        jump snake2_eng
#                   "Нет, пошли на рученьки":
                    "No, hop back up":
                        $ renpy.pause(0.5)            
                        hide ss with dspr
#                       "Второй заход мне дался куда тяжелее. От напряжения я не уследил за дорогой..." with vpunch
                        "The second go was much harder for me. Despite all of my efforts, I couldn't keep an eye on the path..." with vpunch
                        play sound sfx_bush_body_fall
#                       "...зацепился за какой-то корень, и мы упали." with vpunch
                        "...caught my foot in a root, and we fell down." with vpunch
                        show ss serious casual with dspr
#                       me "Ты в порядке?"
                        me "{enen}Are you OK?{/enen}"
#                       ss "Нет, я всё ещё укушена змеёй."
                        ss "{enen}No, I'm still bitten by a snake.{/enen}"
#                       me "Жить будешь."
                        me "{enen}You'll get over it.{/enen}"
                        show ss nosmile casual                        
#                       ss "Если ты меня не уронишь куда-нибудь ещё."
                        ss "{enen}If you don't drop me somewhere else.{/enen}"
#                       me "Раз ты в настроении шутить, может и идти сможешь?"
                        me "{enen}If you're in the mood for joking, then maybe you can walk?{/enen}"
#                       ss "Смогу."
                        ss "{enen}I'll try.{/enen}"
                        jump snake2_eng
            else:               
                $ sp_snake -= 1
                show ss sad casual with dspr 
                window show             
#               ss "Не очень хорошо. Кажется, нога отекла."
                ss "{enen}Not so good. I think my leg is swollen.{/enen}"
#               me "И правда. Давай я всё-таки схожу за помощью?"
                me "{enen}Oh. Yes, it seems so... Again, should I go find help?{/enen}"
                show ss scared casual                  
#               ss "Пожалуйста, нет."               
                ss "{enen}Please, no!{/enen}"
#               me "Ладно, давай попробуем пойти. Так будет быстрее. Я помогу."
                me "{enen}Alright, let's try to walk. That'll be faster. I'll help.{/enen}"
                show ss serious casual with dspr                 
                jump snake2_eng

label snake2_eng:
#    stop music fadeout 3
#   "Я подставил своё плечо для опоры, и мы потихоньку двинулись вперёд."
    "{enen}I lent my shoulder for support and we slowly started moving forward.{/enen}"
    window hide
    $ renpy.pause (0.5)
    scene bg ext_path2_night  with dissolve 
    if sp_snake >= 5:
    
        stop music fadeout 1
        $ renpy.pause (1)
        
        play music music_list["you_won_t_let_me_down"] fadein 5
#       "Саманта шагала довольно уверенно, помощь особо не требовалась. Мне оставалось только следить за дорогой."
        "Samantha walked rather steadily, and my help wasn't very necessary. I should just keep an eye on the path."
#       "Поскольку видно было мало, всё сильнее чувствовались запахи ночного леса. Он пах мокрой землёй, цветущей липой, и тайной."
        "It was a clear night, smells of the forest were strong: the scent of wet soil, flowering linden, and mystery."
        "In other circumstances I could find such a night forest walk surprisingly enjoyable."
        jump snake3_eng
    show ss unsure casual
#    play music Take_Shelter fadein 5     
#   ss "Как думаешь... Что это была за змея?"
    ss "{enen}So... What kind of snake do you think it was?{/enen}"
#   me "Гадюка какая-нибудь."
    me "{enen}Perhaps some sort of viper.{/enen}"
#   ss "Она очень опасна?"
    ss "{enen}Is it very dangerous?{/enen}"
    menu:
#       "Смотря какая":
        "It depends":
#           me "Гадюка гадюке рознь. Вот если это одна пионерка в змеином обличье..."
            me "{enen}Vipers differ. If was some insidious girl-pioneer in snake's skin...{/enen}"
#           ss "Перестань."
            ss "{enen}Stop it.{/enen}"
#       "Не в нашем случае":
        "Not in this case":
            $ sp_snake += 1
            show ss nosmile casual           
#           me "Судя размеру укуса, эта какая-то мелкая. Такая и болонку не свалит."
            me "{enen}Jugding by the size of the bite, it was a small snake. It won't even take down a lap-dog.{/enen}"
#       "Не опасна":
        "Not dangerous":
            $ sp_snake -= 1
#           me "Поболит и перестанет, убить человека такая не может." 
            me "{enen}It'll just ache for a while, that's it. No way it can kill a human.{/enen}"
            show ss serious casual with dspr    
#           ss "Врёшь ведь. Всё настолько плохо?"
            ss "{enen}Why are you lying? Is it that bad?{/enen}"
#           me "Не выдумывай. {w}Ладно, я не знаю, если честно."
            me "It's not, I'm sure! {w}OK, I don't know, to be honest... But I truly believe it was some lame little snake..."
            show ss surprise casual  with dspr           
#           "Глаза Саманты округлились. Надо было уж врать до конца."
            "Samantha's eyes rounded. I should've stand by my words till the end."
    window hide
    $ renpy.pause(0.5)
    hide ss with dspr
    $ renpy.pause(0.7)
    window show 
#   "Мы прошли еще пару сотен мучительных метров, пока Саманта в очередной раз не споткнулась."
    "We walked a few more painful hundreds of meters until Samantha stumbled once more."
#   me "Давай отдохнём немного."    
    me "{enen}Let's have a little rest.{/enen}"
    show ss unsure casual with dspr 
#   ss "Кажется, я потеряла шапку... В том овраге."
    ss "{enen}I think I just lost my hat... In that ravine.{/enen}"
#   me "Пилотку? {w}Ну что же ты, пошли теперь искать!"
    me "{enen}Your field cap? {w}Clumsy girl! Now let's go back and look for it.{/enen}"
    show ss nosmile casual  
#   "Саманта улыбнулась, но только слегка. Болезненная сосредоточенность не отпускала. "
    "Samantha smiled, but just a little. A painful concentration didn't ease off."
#   me "Завтра вернусь, поищу..."
    me "{enen}I'll search for it tomorrow...{/enen}"
    show ss unsure casual   
#   ss "Не нужно..."
    ss "{enen}There's no need...{/enen}"
#   me "Всё же нехорошо лагерные вещи терять. Вожатую подставим. {w}Тебя она не уберегла, ну хоть пилотка будет..."
    me "{enen}Anyway, it's no good losing the camp's things. We'll let the camp leader down. {w}She failed to keep you out of harm, at least let her keep your hat as consolation.{/enen}"

    show ss nosmile casual
#   ss "Но она моя... Из магазина." 
    ss "{enen}But the hat is mine... It's from a shop.{/enen}"
#   me "Магазинное тоже терять нельзя. Теперь тебя из страны не выпустят, пока не найдёшь."
    me "{enen}The shop-bought should not be lost too... It's the concept of socialism, you know. You can't own any things at all, you just borrow them from the state.{/enen}"
    ss "{enen}Uh...{/enen}" 
    me "{enen}So they won't let you of the country without giving the hat back. {w}But don't worry, I'll find it.{/enen}"     
    show ss unsure casual   
#   "Я старался отвлечь Саманту этим разговором, но она уже вовсе не слушала."    
    "I tried to divert Samantha's thoughts with this conversation, but she seemed too distracted by the leg, barely listening me."
    $ renpy.pause(0.3)  
#   me "Как себя чувствуешь?"
    me "{enen}How are you feeling?{/enen}"
    if sp_snake >= 3:
        show ss sad casual with dspr     
#       ss "Бывало и получше. Но ты не волнуйся."
        ss "{enen}I've been better. But don't you worry.{/enen}"
        menu:
#           "Как тут не волноваться?":
            "How can I not worry?":
#               me "Лучше бы она меня укусила."
                me "{enen}That damn thing... It should have bitten me instead.{/enen}"
                $ sp_ss += 1
                show ss shy casual               
#               ss "Даже так?"
                ss "{enen}Really?..{/enen}"
#               me "Ну... Во мне и веса больше."
                me "{enen}Well... I weigh more.{/enen}"
                jump snake4_eng
#           "Я и не волнуюсь":
            "I don't worry":
                play music music_list["you_won_t_let_me_down"] fadein 3
#               me "Чего волноваться? Неприятно, конечно, но тебе ничего не грозит."
                me "{enen}What should I worry for? It's unpleasant story, of course, but you are not in danger.{/enen}"
                $ sp_ss -= 1
                show ss unsure casual              
#               ss "Ты так уверен? Значит, это мне с перепугу так плохо?"
                ss "{enen}Are you sure? Why it feels so lousy then?.. Just because I'm scared?..{/enen}"
#               me "Ну, как-то так."
                me "{enen}Yes, it may be so. Try not to think about it.{/enen}"
                show ss serious casual with dspr                  
#               ss "Ладно, тогда пошли дальше."
                ss "{enen}OK, let's get busy then. Let's go further.{/enen}"
                window hide
                $ renpy.pause(0.3)             
                hide ss with dspr
                window show
#               "И мы снова двинулись навстречу темноте: поджидающим в ней острым сучьям, хлещущим веткам и кусачим корягам."
                "We walked again into the darkness: into sharp boughs, lashing branches and prickling snags waiting for us."
                jump snake3_eng
    else:
        jump snake4_eng

label snake3_eng:
    window hide
    $ renpy.pause(1)
    scene bg ext_path2_night  
    show ss nosmile casual 
    with dissolve   
    window show
#   ss "Кажется... Мы с тобой..."
    ss "{enen}It seems... Me and you...{/enen}"
#   me "Да?"
    me "{enen}Yes?{/enen}"
#   ss "Попали в приключение."
    ss "{enen}Are caught in an adventure.{/enen}"
#   me "Поздравляю. И как тебе?"
    me "{enen}Congratulations. And how is it?{/enen}"
#   ss "Так себе. Успокаивают только твои слова, что я умру в аварии. Так что, если на нас не упадёт самолёт..."
    ss "{enen}So-so. The only soothing thing is your saying that I'll die in a crash. So if no plane falls on us...{/enen}"
#   me "Очень смешно."
    me "{enen}Funny as a crutch!{/enen}"
#   ss "А мне не очень."
    ss "{enen}And for me it's not very funny...{/enen}"
#   me "Ну как тебе помочь?"
    me "{enen}I want to help, but how can I?.. Tell me.{/enen}"
    if sp_ss >= 7:
        show ss shy casual with dspr    
#       ss "Просто будь рядом и дальше."
        ss "{enen}Just stay beside me. As you did.{/enen}"
    else:
#       ss "Ты и так помогаешь. Не слушай моё нытьё."
        ss "{enen}Oh, you're helping as you can. Don't listen to my whining.{/enen}"
    window hide
    $ renpy.pause(0.5)  
    hide ss with dissolve    
    stop ambience fadeout 2
    window show
#   "Наконец, мы вышли на опушку. Впереди показались редкие огоньки «Совёнка»."
    "At last we came to the edge of the forest. We could see a few lights from «Sovyonok» ahead."
    play ambience ambience_camp_entrance_night  
    scene bg ext_camp_entrance_night  
    with dissolve   
#   me "Ну что, спасены? И даже совсем не отклонились - прямо к лагерю вышли."
    me "{enen}So, are we saved? It's like we walked by the straight line - went directly to the camp!{/enen}"
#   ss "Видишь, а ты боялся."
    ss "{enen}You see? And you were afraid.{/enen}"
#   me "Я? Ну ладно."
    me "{enen}So it was me? Ha-ha. Alright then...{/enen}"
    window hide
    $ renpy.pause(1)
    scene bg ext_square_night
    show darkwindows
    with dissolve  
#   me "Похоже, остальные ещё не вернулись. Может, нас ищут?"
    me "{enen}It seems the rest are not back yet. Maybe they are looking for us?{/enen}"
#   ss "Не так долго мы и плутали."
    ss "{enen}We weren't lost for too long.{/enen}"
    window hide
    $ renpy.pause(0.5)    
    scene bg ext_aidpost_night     with dissolve  
    $ renpy.pause(0.4)      
    play sound sfx_close_door_campus_1
    $ renpy.pause(0.8)    
    show ss nosmile casual 
    with dspr 
    $ renpy.pause(0.4)     
#   me "А медпункт-то закрыт. Что же делать?"
    me "{enen}And the infirmary is closed. Hm, what to do...{/enen}"
#   ss "Ничего, зачем нам медпункт без медсестры? Прилечь я и у себя могу."
    ss "{enen}Nothing - we don't need the infirmary without a nurse. I can lay down in my cabin as well.{/enen}" 
#   me "Тогда отведу тебя и пойду за медсестрой."
    me "{enen}Then I'll see you to the cabin and go find the nurse.{/enen}"
#   ss "Только давай домик Лены проверим сначала? Вернулась или нет?"
    ss "{enen}What if we check Lena's cabin first? I wonder if she's back.{/enen}"
#   me "Ладно, я к ней загляну. А тебе покой нужен."
    me "{enen}OK, I'll check that. And you need to rest.{/enen}"
#   ss "Да тут всего пару домов прой ти. Я с тобой."
    ss "{enen}It's only a couple of cabins away. I'll come with you.{/enen}"
    stop music fadeout 5
#   me "По правде, не очень хочется с ней встречаться."
    me "{enen}The truth is that I'm not eager to meet her...{/enen}"
#   ss "Я могу сама к ней зайти."
    ss "{enen}I can visit her myself.{/enen}"
#   me "Нет уж. Я зайду."
    me "{enen}No. I'll do it.{/enen}"
    window hide
    $ renpy.pause(0.5)    
    scene bg ss_ext_house_of_el_night  with dissolve
    play music music_list["mystery_girl_v2"] fadein 5
    show ss nosmile casual with dspr    
#   "Мы подошли к домику Лены. Свет не горел, но дверь оказалась чуть приоткрытой. На стук никто не отозвался."
    "We went to Lena's cabin. The lights were off, but the door was slightly ajar. Nobody answered my knocking."
#   ss "Войдём?"
    ss "{enen}Should we go in?{/enen}"
#   me "И что дальше? Будем рыться в её вещах, читать дневник? Хватит шпионажа на этот вечер."
    me "{enen}What for? You wanna go through her things, read her diary? I think that's enough espionage for this evening.{/enen}"
    play sound sfx_open_door_kick   
#   "Я захлопнул дверь. Кажется, Саманта рассчитывала на другой ответ, но в итоге растерянно кивнула."
    "I shut the door. Samantha nodded perplexedly."
    show ss unsure casual 
#   ss "Я только посмотреть, нет ли её там..."
    ss "I just wanted to see if she is in..."
#   me "А если и так? Давай оставим её в покое. Тебе бы о себе подумать."
    me "{enen}Even if it so - let's leave her for tonight. You need to think about yourself now.{/enen}"
    window hide
    $ renpy.pause(0.3)    
    show ss surprise casual with dspr
    $ renpy.pause(0.2)     
    show un normal pioneer far at fleft   with dspr
    window show
#   un "Какая порядочность."
    un "Oh, what decency."
    window hide
    $ renpy.pause(0.3)     
    show un normal pioneer at fleft 
    window show 
#   th "И откуда она появилась? Вот сейчас застукала бы взломщиков."
    th "And where did she come from? She nearly walked in on burglars."
    show ss serious casual 
#   me "...Саманту змея укусила."
    me "...Samantha was bitten by a snake."
    show un serious pioneer at fleft with dspr  
#   un "Серьёзно?"
    un "Really?"
#   "Лицо Лены выразило неподдельную озабоченность. Она осмотрела укушенную ногу."
    "Lena's face showed genuine anxiety. She examined the bitten leg."
#   un "Да что же ты расхаживаешь? Семён, ей в медпункт надо."
    un "Why are you walking then? Semyon, she must go to the infirmary."
#   me "Он закрыт."
    me "It is closed."
#   un "Идёмте, я открою."  
    un "Let's go, I'll open it."
    window hide
    $ renpy.pause(0.5)  
    scene bg int_aidpost_night
    with dissolve     
    show un serious pioneer 
    show ss nosmile casual at left 
    with dissolve   
    play ambience ambience_medstation_inside_night 
    $ renpy.pause(0.4)  
    window show     
#   me "Что теперь? Думаешь, укус опасен?"
    me "What now? Do you think the bite is dangerous?"
#   un "Не для жизни, но нужно лечение, иначе симптомы долго не пройдут."
    un "Not life-threatening, but it needs treatment, otherwise the symptoms will get worse."
#   me "И в чём лечение заключается?"
    me "And what should the treatment be?"
#   un "Сами ничего не делайте. Попить ей принеси, разве что. А я схожу за медсестрой."
    un "Don't do anything yourself. Except perhaps you may give her a drink. And I'll call for the nurse."
    window hide
    $ renpy.pause(0.5)  
    hide un with dspr
    $ renpy.pause(0.4)  
    window show
#   "Такая самоотверженность Лены меня немало удивила. Я вышел вслед за ней."
    "Lena's selflessness was a surprise to me. I followed her out."
    window hide
    $ renpy.pause(0.3)     
    scene bg ext_aidpost_night  
    show un normal pioneer
    with dissolve   
    play ambience ambience_camp_center_night
    $ renpy.pause(0.5)  
    window show     
#   me "Спасибо... И прости, что в лесу так получилось."
    me "Thanks... And sorry for what happened in the forest."
#   un "А зачем вы за мной полезли? Решили следить до конца?"
    un "Why have you followed me? Decided to spy till the end?"
#   me "Саманта боялась, что ты заблудишься."
    me "Samantha was afraid you might get lost."
#   un "Ещё и змея, это же умудриться надо. Ты её видел?"
    un "...How did you even find a snake? Did you see it?"
#   me "Нет, совсем темно было, поэтому так и вышло."
    me "No, it was quite dark already. She doesn't seen it too."
#   un "Тогда с чего решили, что это змея?"
    un "So how do you know it was a snake?"
#   me "А кто ещё?"
    me "What else it could be?"
    show un laugh pioneer  
#   un "Может, это была я? Так вы подумали?"
    un "Maybe it was me, huh? Haven't you thought of that?"
#   me "Нет, ты не змея."
    me "No, you are not a snake..."
    show un normal pioneer  
#   un "Такого комплимента мне ещё не делали. А кто же я?"
    un "Hm. I haven't been paid such a compliment yet. {w}And who am I, then?"
#   th "А кто она? Кто-то вроде ежика, может быть."
    th "And who is she? Maybe something like a hedgehog."
    window hide
    $ renpy.pause(0.9) 
#   me "Хотел бы я узнать..."
    me "...I wish I could know."   
#   un "Я пошла."
    un "I'm leaving."
    window hide
    $ renpy.pause(0.5)     
    hide un with dspr
    $ renpy.pause(0.5) 
    scene bg int_aidpost_night  
    show ss nosmile casual 
    with dissolve       
    play ambience ambience_medstation_inside_night  
#   me "Как у нас дела?"
    me "{enen}How are we doing?{/enen}"
#   ss "А, это вы, доктор. Голова болит. И нога. Вся."
    ss "{enen}Oh, it's you, doctor. My head is aching. And my leg. And everything.{/enen}"
#   me "Ты укушена, тебе больно, ты хочешь поговорить об этом?"
    me "{enen}You are bitten, it hurts you, do you want to talk about it?{/enen}"
    show ss surprise casual 
#   ss "Нет, я хочу выздороветь."
    ss "{enen}No! I want to recover.{/enen}"
#   me "Осознание проблемы - путь к решению."
    me "{enen}To realize the problem is the way to solve it.{/enen}"
    show ss sad casual 
#   ss "Мне так паршиво, а ты издеваешься."
    ss "{enen}I feel so lousy, and you're mocking me.{/enen}"
#   me "Хотел бы помочь, но Лена сказала самим ничего не делать."
    me "{enen}I'd like to help, but Lena told me not to do anything myself.{/enen}"
    show ss nosmile casual 
#   ss "А тебя кусал кто-нибудь? Кроме комаров."
    ss "{enen}Have you ever been bitten? Except for mosquitos.{/enen}"
#   me "Осы."
    me "{enen}Hmm... Wasps?{/enen}"
#   ss "Расскажи."
    ss "{enen}Tell me.{/enen}"
#   me "Да что там... Ну вот помню,  что когда я был маленьким, то не боялся ос, а отец очень боялся. И всегда звал меня или маму, чтобы мы их убивали."
    me "{enen}There is not much to tell... I remember that in my childhood I wasn't afraid of wasps, and my father was. And he always called me to kill them.{/enen}"
#   me "Но так продолжалось ровно до того, пока меня самого не ужалили. После этого мы оба стали звать маму."
    me "{enen}But it was only until I was stung myself. Since then in cases of wasp danger both of us called mum.{/enen}"
#   ss "Ос боишься, значит? А ещё чего?"
    ss "{enen}You are afraid of wasps then? And what else?{/enen}"
#   me "Довольно-таки... всего."
    me "{enen}What am I afraid of? I'm will not brag here - I'm afraid pretty much... of everything.{/enen}"
#   ss "Как это?"
    ss "{enen}How come?{/enen}"
#   me "Знаешь эти длинные списки фобий, которые должны быть смешными? По мне, так можно увидеть смысл почти в каждой."
    me "{enen}Do you know those long lists of phobias which are meant to be laughable?{/enen}"
    ss "{enen}Like the fear of... toy cars?{/enen}"    
    me "{enen}Right. As for me, you can find sense nearly in every one.{/enen}"  
    show ss normal casual     
    ss "{enen}Really? What's so terrifying about the toy cars?{/enen}"  
    me "{enen}I don't know... You could step on it and it will pierce your feet!{/enen}"
    show ss surprise casual
    ss "{enen}Hey, hey! Don't remind me of that...{/enen}"
    show ss unsure casual    
#   ss "Но ведь фобия - это как болезнь."
    ss "{enen}No, you can't share every phobia. Every one of them is like an own disease.{/enen}"
#   me "Не знаю. Ну вылечишь ты фобию. Каждый взрослый человек всё равно боится чего-то, если он не идиот. Кто-то боится фломастеров, кто-то боится смерти. Кому живётся лучше?"
    me "{enen}Why does every fear have to be an own disease? Every adult is still afraid of anything if he is not stupid. Some have a fear of colored pens, others are afraid of death. Whose life is better?{/enen}"
    show ss normal casual 
#   ss "Тому, кого не отправят в психушку."
    ss "{enen}The one's who's not going in a nut house.{/enen}"
#   me "Как радикально. А если человек боится только попасть в психушку? Как с ним быть?"
    me "{enen}That's harsh! And what if a man's only phobia is to end up in a nut house? What's to be done with him?{/enen}"
    show ss smile casual 
#   ss "В психушку его!"
    ss "{enen}Into a nut house he goes!{/enen}"
#   me "...А если боится не попасть? А?"
    me "{enen}Right... And if one's afraid of NOT to go in clinic? Well?{/enen}"
    show ss normal casual 
#   ss "Черт, я не знаю. Я с вами сама попаду."
    stop music fadeout 6
    ss "{enen}Damn, I don't know. I'll be put there myself because of you.{/enen}"
#   me "Ладно, отдыхай. Медсестра скоро придёт."
    me "{enen}Okay, have a rest. The nurse will come soon.{/enen}"

    jump getsumwater_eng

label snake4_eng:
    stop music fadeout 3
    show ss sad casual      
#   ss "Я всё вспоминаю про твоё предчувствие."
    ss "{enen}I can't stop thinking about your premonition.{/enen}"
##earlier we've called it a "bad feeling"... are they synonyms enough?
###"foreboding" is indeed a synonym of "bad feeling", but sounds odd in common English usage. "Premonition" (translated directly) is neutral but a better fit here.
#   me "Какое?"
    me "{enen}What premonition?{/enen}"
    if sp_ss >= 8:
        play music music_list["confession_oboe"] fadein 8
    else:
        play music music_list["i_dont_blame_you"] fadein 8       
#   ss "Что я скоро умру." 
    ss "{enen}That I'll die soon.{/enen}"
#   me "Ты что! Это не... Даже не думай об этом!"
    me "{enen}No way! Don't even think about it!{/enen}"
    show ss cry casual  
#   ss "Ты ведь сам всё твердил, как это важно. Всё сходится."
    ss "{enen}You were very certain that it's important. Now it all adds up.{/enen}"
#   me "Нет, не так. Такого быть не должно."
    me "{enen}No, you're wrong! It has nothing to do...{/enen}"
#   th "Если подумать, то и лагеря быть не должно. А раз он есть, то кто знает. А если судьба и тут берёт своё?"
    th "...Or has it? To be honest, I have no idea how the things work here... What if that evil fate takes its course here too?"
#   ss "Зря я тебе не верила. Это мне наказание."
    ss "{enen}I should have believed you. This is my punishment.{/enen}"
#   me "Но тебе же не так плохо?"
    me "{enen}But you aren't feeling that bad, are you?{/enen}"
#   ss "Очень плохо, если честно. И нога опухла."
    ss "{enen}It's really bad, Sam. My leg is swelling up.{/enen}"
#   me "Я побегу в лагерь."
    me "{enen}I'll run to the camp.{/enen}"
    show ss cry2 casual     
#   ss "Нет, не уходи. Я не дождусь."   
    ss "{enen}No, don't leave me. I can't wait here... I won't hold for so long.{/enen}"
##she probably won't bear the waiting till he comes back. it can be understood as a veiled 'it may be too late when you come back, I won't hold for so long'
#   me "Тогда я тебя понесу. Давай-ка..."
    me "{enen}Then I'll carry you. Let me...{/enen}"
##or 'let me'
    window hide
    $ renpy.pause(0.3)     
    scene cg ss_carry2 with dissolve
    $ persistent.ssg_108 = True     
    $ renpy.pause(1.0)     
#   "Эта ноша показалась мне даже лёгкой. {w}Но когда не видишь, куда ступать - тут уж никакой энтузиазм не поможет. Продвигались мы очень медленно." 
    "This burden felt not heavier than a feather, but we still moved forward very slowly. When you can't see where to set your foot - no amount of enthusiasm can help."
#   me "Не переживай... Скоро будем в лагере... {w}Какао пить... Закусывая шоколадом..."
    me "{enen}Don't worry... We'll be in the camp soon... {w}Drinking cocoa... Eating chocolate...{/enen}"
#   "Я прошёл достаточно далеко, пока силы меня не оставили. И снова привал." 
    "{enen}I covered a rather long distance, going until exhaustion overtook me. We took one more stop for a rest.{/enen}"
    window hide
    scene bg dfoggyforest  
    show ss cry casual 
    with dissolve   
    $ renpy.pause(0.5)   
    window show 
#   ss "Сэм..."
    ss "{enen}Sam...{/enen}"
#   "Саманта говорила уже совсем тихо."
    "By now, Samantha's words were barely audible."
    if sp_ss >= 8:
#       ss "Я думаю... Ты должен знать..."
        ss "{enen}I think... you should know...{/enen}"
#       me "Знаю, всё знаю. Ты лучше береги силы."
        me "{enen}I know, I know everything. You'd better save strength.{/enen}"
##save strength
    else:
        show ss cry2 casual 
#       ss "Ты был настоящим другом. Лучшим... Какого можно пожелать."
        ss "{enen}You were a true friend. The best... one could wish for.{/enen}"
#       me "Не раскисай, вылечим тебя."
        me "{enen}Don't give up, we'll patch you up.{/enen}"
#       "Мой голос предательски дрожал."
        "My voice was trembling deceitfully."
##trembling

    $ renpy.pause(0.5)         
#   me "Ну что, последний рывок? Готова?"
    me "{enen}Well, one last dash. Ready?{/enen}"
    window hide
    $ renpy.pause(0.3)     
    hide ss with dspr
    $ renpy.pause(0.5) 
    scene bg dfoggyforest  with dissolve 
#   "К сожалению, последнего рывка не хватило. Мои силы кончились раньше, чем этот проклятый лес. И теперь короткий отдых не поможет."
    "Unfortunately, the last dash was not enough. I ran out of breath before this damned forest ended. And I felt that a short rest won't help this time."
#   "Но я всё равно не отпускал Саманту. Не мог признать поражение. Сил идти не было, и отпустить - тоже не было."
    "I still held Samantha tightly. I couldn't accept defeat, but I hadn't the energy to go or to let her go."
    window hide
    scene cg ss_carry3  with dissolve
    $ persistent.ssg_108 = True     
    $ renpy.pause(1.5)
#   ss "Ты сделал всё."
    ss "{enen}You've done all you could.{/enen}"
#   "Я едва не плакал от собственной беспомощности. Набирая воздуха в грудь, я несколько раз позвал на помощь - так громко, как мог. Вот теперь всё."
    "I almost cried from my feebleness. Pumping air into my lungs, I called for help several times with all my strength. By that moment it was really all that I could. That was it."
#   ss "Семён... Я смогу жить... В «Совёнке»? Как ты говорил. Вечное лето."
    ss "{enen}Semyon... will I be able to live, as you said... in «Sovyonok»?.. Eternal summer?{/enen}"
#   me "Сможешь."
    me "{enen}You will, girl.{/enen}"
#   ss "Обещаешь?"
    ss "{enen}Promise?{/enen}"
#   me "Да. Это твой «Совёнок». Ничего не бойся."
    me "{enen}Yes. It is your «Sovyonok». Afraid nothing.{/enen}"
#   ss "А ты там будешь?"
    ss "{enen}And will you be there?{/enen}"
#   me "...Увидишь."
    me "{enen}...You'll see.{/enen}"
    if sp_ss >= 8:
#       ss "Ты должен там быть. {w} Я ведь люблю тебя."
        ss "{enen}You must be there.{w} Because I love you.{/enen}"
        window hide
        $ renpy.pause(1.3)
        menu:
#           "Я тебя тоже":
            "And I love you":
                $ sp_mizul = 2
                $ sp_ss += 2
#               me "А я тебя. И я тебя спасу. И от этого, и от всего. Ты только потерпи немного."
                me "I love you too. And I'll save you. From this and from everything. Just bear it for a bit longer."
#               ss "Просто возьми меня за руку."
                ss "{enen}Take my hand.{/enen}"
#               me "Уже держу."               
                me "{enen}I'm already holding it.{/enen}"
#               ss "Я не чувствую."
                ss "{enen}I can't feel it.{/enen}"
#           "Бедняжка не в себе":
            "Poor girl is not herself":
                $ sp_mizul = 1 
#               me "Хорошо, но ты отдыхай. Скоро пойдём."
                me "{enen}Sure, but have a rest. We'll leave soon.{/enen}"
    stop music fadeout 7  
    window hide 
    $ renpy.pause(1)
    scene bg dfoggyforest  with dissolve
    $ renpy.pause(0.5) 
    play sound sfx_bush_leaves  
#   "Кусты поблизости затрепетали, их пронзил луч света, наконец, они раздвинулись, и перед нами появилась..." 
    "Nearby bushes began to quiver, a beam of light blinded us, and at last..."
    window hide
    $ renpy.pause(0.3) 
    show un normal pioneer at right with flash
    window show 
#   "...Лена с фонариком в руке."
    "...Lena appeared, with a torch in her hand."
#   un "Вы чего? В трёх соснах заблудились?"  
    un "What's wrong? Have you gotten lost like a babe in the woods?"
###
#   me "Лена! Саманту змея укусила."
    me "Lena! Samantha was bitten by a snake."
    show un serious pioneer at right with dspr
#   un "Покажите."
    un "Let me see."
    window hide
    $ renpy.pause(0.3) 
    show ss sad casual with dspr
    $ renpy.pause(1.0)     
#   un "У, плохо дело. {w}Будет тошнить неделю."  
    play music music_list["lightness"] fadein 5
    un "Oh, it looks pretty bad. It's too late to help it... {w}Now she'll be sick for a week."
#   me "Значит, она поправится?"
    me "You mean... She'll be fine? She'll recover?"
#   un "Ну ты даёшь, натуралист юный. Напугал её до полусмерти, я смотрю."
    un "Of course she will, what a young naturalist you are? You scared her out of her senses, didn't you?"
#   me "Но ведь гадюка. И смотри как ногу раздуло."
    me "But it was a real viper. Her leg swelled up, can't you see?"
    show un normal pioneer at right
#   un "Яд у наших гадюк слишком слабый. Это же не двухметровая гюрза тебе. Отведём её в медпункт."
    un "Our vipers' venom is too weak. It was not some two-meter cobra, after all. Let's take her to the infirmary." #я использовала кобру вместо гюрзы, т.к. гюрза (Levantine viper) и гадюка (viper) в одной фразе плохо смотрятся.
###"It was not a saw-scaled (viper) one, one of the most poisonous snakes in the world, I guess." - so we keep talking about vipers
    stop ambience fadeout 2
    hide un
    hide ss 
    with dspr   
#   "Мы взяли Саманту под руки. Оказалось, что лагерь был совсем близко, нужно было пройти считанные метры, чтобы заметить огоньки."
    "We lifted Samantha by the arms. It turned out that the camp was close-by; we needed to go only a few meters further to see the camp lights."
    window hide
    $ renpy.pause(0.5)     
    play ambience ambience_camp_entrance_night  
    scene bg ext_camp_entrance_night 
    show un normal pioneer 
    show ss serious casual at left 
    with dissolve   
    window show
#   un "Надеюсь, ты не гонял её по лесу? Движение ускоряет действие яда."
    un "I hope you didn't make her race through the forest. Motion speeds the effects of the venom."
#   th "Откуда она столько знает про змей?"
    th "Where did she learn so much about snakes?"
#   me "Эээ... Я не мог оставить её одну в темноте."
    me "Erm... I couldn't leave her alone in the dark."
#   un "А можно узнать, зачем вы в лес полезли?"
    un "What were you looking for in the forest, if I may ask?"
#   me "За тобой. Выручать, вроде как. Лес полон опасностей, как видишь."
    me "Looking for you. We wanted to get you back... The forest is full of dangers, as you can see."
    window hide
    $ renpy.pause(0.5)    
    scene bg ext_aidpost_night  
    with dissolve
    $ renpy.pause(0.5)    
    show un normal pioneer 
    show ss serious casual at left    
    with dissolve    
    $ renpy.pause(0.5)  
    window show
#   un "А медпункт-то закрыт. Повезло, что у меня есть ключи."
    un "...And the infirmary is closed. Luckily, I have the key."
    window hide
    $ renpy.pause(0.5)
    hide ss with dspr   
    play sound sfx_open_door_1 
    $ renpy.pause(0.5)  
    window show     
#   un "Медсестра ушла со всеми, наверное. Я за ней схожу."
    un "The nurse must have gone with the others. I'll call for her."
#   me "Спасибо. Ты там постарайся... Чтоб без лишней паники."
    me "Thank you. Hey, when you'll get there... Olga Dmitrievna will be freaked out, I imagine..."
#   un "Поняла. А ты проследи, чтоб она лежала."
    un "I'll try not to cause a panic. And you here make sure that she stays lying down."
#   me "Может, оказать какую-то первую помощь?"
    me "Got it... What about some first-aid?"
#   un "Попить ей дай. И это всё. Ждите."
    un "Give her something to drink. That's all. Just wait for us."
    window hide
    $ renpy.pause(0.5)
    hide un with dissolve  
    $ renpy.pause(0.5)    
    play ambience ambience_medstation_inside_night  
    scene bg int_aidpost_night
    show ss normal casual 
    with dissolve
#   "Я вернулся к Саманте. Её лицо порозовело, на устах уже блуждала улыбка."
    "I returned to Samantha. Her face had regained some colour, and a smile hovered over her lips."
#   me "Хорошо выглядишь. Тебе лучше?"
    me "{enen}You look better. Do you feel better too?{/enen}"
    show ss smile casual  with dspr  
#   ss "Ну и дураки же мы с тобой! Я уж умирать собралась. И всё возле лагеря. Надеюсь, Лена никому не расскажет."
    ss "{enen}What fools we were! Gosh, I thought I'm really dying...  and so close to the camp. {w}I hope Lena doesn't tell anyone how stupid it was.{/enen}"
#   me "Твоя репутация не пострадает. Что с укушенной взять? А вот я оплошал, и много раз."
    me "{enen}You won't lose face, what can you expect of someone who's been bitten? I am the one to blame. Yes, I did a lot of mistakes...{/enen}"
    stop music fadeout 5
    show ss smile2 casual   
#   ss "Вовсе нет, ты был молодцом. У меня самый заботливый и самоотверженный переводчик."
    ss "{enen}Not at all, you were as solid as rock. My translator is the most caring and selfless man.{/enen}"
#   me "Просто у меня самая храбрая и терпеливая клиентка."
    me "{enen}It's just because my client is the most brave and patient girl.{/enen}"
    if sp_mizul >= 1:
        show ss shy casual  with dspr 
#       ss "Слушай, я там чего-то наговорила..."
        ss "{enen}Look, I said something out there...{/enen}"
#       me "Понимаю."
        me "{enen}I understand.{/enen}"
#       ss "Что ты понимаешь?"
        ss "{enen}What do you understand?{/enen}"
        menu:

#           "Сделаем вид, что этого не было":
            "Let's sweep it under the rug":
##pretend it hasn't happened
                show ss sad casual 
#               ss "Наверное, так будет лучше."
                ss "{enen}Uh... Perhaps that would be for the best.{/enen}"
                stop music fadeout 5                
                if sp_mizul == 2:
#                   ss "Но знай, то, что ты там сказал... это прекрасно. Я этого не забуду."
                    ss "{enen}But you must know, what you said out there... it was beautiful. I won't forget it.{/enen}"
#                   me "Кхм. Тебе бы отдохнуть."
                    me "{enen}Ahem. You should get some rest.{/enen}"
#                   ss "Прости. Да, наверное."
                    ss "{enen}Sorry. You're right.{/enen}"
                    jump getsumwater_eng

#           "Понимаю, о чём речь":
            "I understand what you are talking about":
#               ss "Хитрец."
                ss "{enen}Dodger.{/enen}"
#               me "Мы можем это и не обсуждать."
                me "{enen}We don't have to discuss it.{/enen}"
##or "may discuss not it"?
                play music music_list["a_promise_from_distant_days_v2"] fadein 5
#               ss "Но я хочу это сказать, на всякий случай. Вдруг меня сейчас увезут лечить, или ещё что. Пусть не будет недомолвок."
                ss "{enen}But I want to say it, just in case... {w}In case I have to go to a hospital for treatment, or something else. I don't want to leave that unsaid.{/enen}"
#               me "Я всё понимаю. Стресс, горячка."
                me "{enen}I know, I know... You were stressed and feverish.{/enen}"
                show ss shy2 casual 
#               ss "Нет, наоборот! Это всё правда, слышишь?!"
                ss "{enen}NO, the contrary! Everything is true, do you hear me?!{/enen}"
#               me "Хм. Ладно, всё правда. Но ты всё ещё нездорова. Отдохни лучше."
                me "{enen}Umph... Yes, everything is true. But you are still unwell. You need to rest.{/enen}"
                if sp_mizul == 2:
#                   ss "Но ты-то здоров. И там в лесу ты сказал... Это же правда?"
                    ss "But you're healty and fine to talk. What you've said out there in the forest... Was it true?"
                    menu:
#                       "Правда":
                        "True":
#                           me "Правда. Но что я сказал такого нового? Тебя многие любят, заботятся. Как и я."
                            me "{enen}It is true. But there was no news for you. Many people love you, care for you. I am among them.{/enen}"
                            show ss sad casual 
#                           ss "Не отпирайся, это было совсем не то! ...Или то? {w}Да, дура я. Вы просто добрые люди, у которых найдутся тёплые слова для всех. Даже для меня."
                            show ss surprise casual 
                            ss "{enen}Don't deny it, it wasn't anything like that!.. {w}Or was it?{/enen}"
                            show ss sad casual                             
                            ss "{enen}...I guess I'm a fool. You're one of those people who can always find warm words for everyone, including me.{/enen}"
#                           me "Ну не надо уж из крайности в крайность."
                            me "{enen}Well, there is no need to go that extreme...{/enen}"
#                           ss "А меня не любит никто. Давай, утешь меня, скажи, что меня любят родители."
                            ss "{enen}And nobody loves me. {w}Come on, comfort me, tell that my parents love me.{/enen}"
#                           me "Я. Я тебя люблю."
                            me "{enen}Me. I love you.{/enen}"
                            show ss shy2 casual
#                           ss "Как друга? Как сестру?"
                            ss "{enen}As a friend? As a sister?{/enen}"
##or "as"?
#                           me "Да..."
                            me "{enen}Yes...{/enen}"
                            show ss crysmile casual
#                           extend " И нет."
                            extend " {enen}And no.{/enen}"
#                           "Мне пришлось нагнуться к Саманте, чтобы дать ей себя приобнять. Отпускать не хотела, пришлось приложить некоторую силу, чтобы вернуть больную обратно в койку."
                            "I had to bend down to Samantha to let her hug me. She didn't want to let me go, so I had to apply some force to return the patient back into her bed."
                            show ss crysmile2 casual
#                           ss "Я уже почти благодарна этой змее. А ты такой грустный. Почему ты грустный?"
                            ss "{enen}I'm almost grateful to that snake. But you are so sad. Why are you sad?{/enen}"
#                           th "Потому что у меня нет жизни. Только какая-то пробная версия."
                            th "Because I don't have a life. Just some kind of trial version."
##trial? demo?
#                           me "Прости."
                            me "{enen}...Sorry.{/enen}"
#                           ss "Потому что нам нельзя быть вместе?"
                            ss "{enen}Is it because we can't be together?{/enen}"
#                           me "В том числе."
                            me "{enen}Among other things.{/enen}"
#                           ss "Да, всё против нас. Но давай побудем ещё немного? Всем назло? А там может и придумаем что-нибудь."
                            ss "{enen}Yes, everything is against us. But couldn't we just... hold it a bit more? In spite of everything?{/enen}"
#                           me "Давай. Но сейчас тебе нужно отдохнуть. И от разговоров тоже."
                            me "{enen}Yes, we will. We'll figure something out, but now you need some rest. From talking as well.{/enen}"
                            show ss unsure casual
                            stop music fadeout 5
#                           ss "Хорошо."
                            ss "{enen}Agreed...{/enen}"
                            $ sp_mizul = 3
                            $ sp_ss += 2

#                       "Не дави на меня":
                        "Don't press on me":
##don't press on me
                            stop music fadeout 6
#                           me "Ты сама знаешь, что к чему. Я не готов это обсуждать. Да и надо ли?"
                            me "{enen}You know what is what. I'm not ready to discuss it... Is it really necessary?{/enen}"
                            show ss sad casual                          
#                           ss "...Как скажешь."
                            ss "{enen}... As you wish.{/enen}"
                            $ sp_ss -= 1
                stop music fadeout 3                            
                jump getsumwater_eng
#           "Это ничего не значит" if (sp_mizul == 1):
            "That means nothing" if (sp_mizul == 1):
                show ss sad casual           
#               ss "Ну... Раз ты так считаешь."
                ss "{enen}Well... If you say so.{/enen}"
                $ sp_ss -= 2
#               me "Думаю, сейчас не время это обсуждать. Отдыхай."
                me "{enen}I think this is not the time to discuss it. Take a rest.{/enen}"
    else:
        show ss surprise casual 
#       ss "Храбрая? Нет, я так испугалась. Первый раз в жизни думала, что умру."
        ss "{enen}Brave? No, I was very scared. For the first time in my life I thought I could die.{/enen}"
        show ss normal casual       
#       me "Ты и меня очень напугала."
        me "{enen}You scared me enough as well.{/enen}"
        play music music_list["smooth_machine"] fadein 5    
#       ss "Ну, ты-то у нас готов к такому повороту."
        ss "{enen}But for you it must be kinda expected... Fate and so on?{/enen}"
#       me "Не шути так."
        me "{enen}It's the worst thing to joke about. Don't.{/enen}"
        window hide
        $ renpy.pause (0.7)
        show ss sad casual with dspr     
        $ renpy.pause (0.3)        
#       ss "Самое грустное во всём этом - это мысль о маме. Как она переживёт."
        ss "{enen}You know, the saddest part of the story was the sudden thought about my mother. How would she get through it?{/enen}"
#       me "Да, такие вещи забыть нельзя. Но ты же будешь в порядке."
        me "{enen}Yes, such things are unforgettable. But, fortunately, you'll be OK.{/enen}"
#       ss "А если не буду?"
        ss "{enen}And if not?{/enen}"
#       th "На этот вопрос у меня ответа нет."
        th "What if not?.. I don't have an answer to such a question."
#       "Саманта уловила моё желание сменить тему."
        "Samantha caught my wish to change the subject."
        show ss unsure casual    
#       ss "Как думаешь, почему Лена нас спасла?"
        ss "{enen}Why do you think Lena saved us?{/enen}"
#       me "Спасла - сильно сказано, наверное."
        me "{enen}Perhaps \"saved\" is a strong word for it...{/enen}"
#       ss "И всё-таки?"
        ss "{enen}And yet?{/enen}"
#       me "А разве не любой помог бы? Ну, говоря о тех, кто в лагере."
        me "{enen}Wouldn't everyone help us? I mean, speaking of out little camp society at least.{/enen}"
##everyone #wut
        show ss nosmile casual      
#       ss "Наверняка. А мы помогли бы любому, так?"
        ss "{enen}Definitely. And we would help any pioneer too, right?{/enen}"
##everyone #wut

#       me "Определённо."
        me "{enen}For sure.{/enen}"
#       ss "Тогда в чем же проблема? Почему везде не может быть так? Везде в мире."
        ss "{enen}I wonder what's the problem then... Why can't it be so everywhere? Everywhere in the world.{/enen}"
#       me "Потому что это место не такое, как везде. Второго такого нет."
        me "{enen}Because this place is not like everywhere. You won't find another place like this.{/enen}"
#       ss "Я так не думаю. То есть, место, конечно, особенное. Но чудеса видишь только ты."
        ss "{enen}I don't think so. I mean, this place is very special, of course. But it is you who is seeing miracles.{/enen}"
#       me "Здесь нет зла. Иногда кажется, что всё зло «Совёнка» - во мне."
        me "{enen}But I really think so. I'll tell you one special thing about it... There is no evil here. {w}Sometimes it seems that the only evil in «Sovyonok» is within me.{/enen}"
        show ss normal casual  
#       ss "Что за глупости? Ты - самый добрый!"
        ss "{enen}What nonsense! You're the kindest on Earth!{/enen}"
#       th "Бессовестная лесть и собаке приятна. Надо бы сделать что-нибудь доброе теперь."
        th "Bare-faced flattery goes a long way. At least I must do something kind now."
        stop music fadeout 5
###

    jump getsumwater_eng
label getsumwater_eng:
    #me "Тебе нужно питьё. Я принесу чего-нибудь."
    me "{enen}You need to drink. I'll go get you something.{/enen}"
    show ss smile2 casual 
    #ss "Не уходи. Я требую внимания!"
    ss "{enen}Hey, don't leave. I demand attention!{/enen}"
    #me "Но Лена велела тебя поить."
    me "{enen}But Lena told me to give you a drink.{/enen}"
    show ss normal casual   
    #ss "А тут ничего нет?"
    ss "{enen}There is no water here?{/enen}"
    #me "Только лекарства. Но давай посмотрим."
    me "{enen}I only see some medicine. But let's take a look...{/enen}"
    #me "...Спирт сгодится?"
    me "{enen}...Is alcohol okay?{/enen}"
    play music Hide_and_seek fadein 5
    show ss smile casual with dspr 
    #ss "Давай водку тогда уж. А то вернусь даже русской водки не попробовав - в школе засмеют."
    ss "{enen}Get me some Russian vodka then. Can't get back without trying it, they will make fun of me at school.{/enen}"
    #me "Где я тебе водку в медпункте возьму?"
    me "{enen}And why exactly would I find vodka in the infirmary?{/enen}"
    show ss laugh casual with dspr  
    #ss "Не знаю, может вы ей укусы медведей лечите."
    ss "{enen}I don't know, maybe you use it to treat bear bites.{/enen}"
    show ss grin casual  
    #extend " А если бы была?"
    extend " {enen}And if you find some?{/enen}"
    
    
    menu:
        #"Тогда грех не попробовать":
        "It will be a sin not to drink":
            #me "50 грамм не повредит. В порядке лечения."
            me "{enen}50 grams is okay. Just for a treat.{/enen}"
            show ss normal casual  with dspr    
            #ss "Но не больше?"
            ss "{enen}No more, no less?{/enen}"
            #me "Зачем? Тебе всё равно не понравится."
            me "{enen}Why more? I bet you won't like it.{/enen}"
            show ss unsure casual          
            # ss "Не больно-то и хочется."
            ss "{enen}Meh. I didn't really want it anyway.{/enen}"
            $ sp_ss -= 1
        #"Устроили бы вечеринку":
        "We'd have a party then":
            show ss smile2 casual  with dspr
            #me "Тайком пить чужое нельзя. Вот дождались бы медсестры, принесли бы закуску, музыку..."
            me "{enen}It's wrong to drink someone else's vodka. We could wait for the nurse, bring something to eat...{/enen}"
            #ss "Звучит неплохо."
            ss "{enen}Yes, and some music! Sounds nice.{/enen}"
        #"Забудь об этом":
        "Forget about it":
            #me "От меня ничего не получишь, алкоголичка маленькая."
            me "{enen}I won't let you drink, you little alchoholic.{/enen}"
            show ss smile2 casual  with dspr          
            #ss "Ну доктор. У меня такой стресс. Эти русские меня таскают по лесам, кусают змеями, пугают по-всякому."
            ss "{enen}But doctor. I had such a stress today. These Russians made me walk through the forest, bite me with snakes, scare me really bad...{/enen}."
            #me "Ремня тебе оздоровительного пропишу."
            # me "I will slap you with my belt as a treat instead."  # Как-то... Э... Не, это тут лишнее... - proofreader of proofreader of translator
            ##me "{enen}Oh, you want a treat? How about this?{/enen}"
            ##"I showed the fig sign to her."            
            me "{enen}Then you can wait and ask Viola.{/enen}"
            ss "{enen}You think she'll give me?..{/enen}"
            me "{enen}Oh, she'll give you what you need - a few good spanks.{/enen}"
            
            show ss unsure casual with dspr         
            #ss "Буу."
            ss "{enen}Woooh.{/enen}"
            $ sp_ss += 1    

    window hide     
    $ renpy.pause(1)           
    #me "...Ничего не нашёл попить. Ещё остались ящики. Продолжать обыск?"
    me "{enen}So... I didn't find anything to drink, but there are still some boxes left. Should I keep searching?{/enen}"
    show ss nosmile casual       
    #ss "Не надо. Но пить хочется, вообще-то. Сходи тогда, только возвращайся скорее."
    ss "{enen}Nah, don't touch them. We are terrible guests.{/enen}"
    me "{enen}I'll go get some water.{/enen}"
    ss "{enen}...Yes, actually I am thirsty. Go find some, but come back soon.{/enen}"
    window hide
    $ renpy.pause (0.5)
    scene bg ext_dining_hall_near_night  with dissolve
    play ambience ambience_camp_center_night
    #"Сперва я зачем-то пошёл к столовой. Ожидаемо, она оказалась закрыта. Тогда я решил проверить наши с вожатой запасы."
    "For some reason, at first I went to the canteen. It was expectedly locked. For the next try, I decided to check in my cabin."
    window hide
    $ renpy.pause (0.5)    
    scene bg int_house_of_mt_night  with dissolve
    play ambience ambience_int_cabin_night
    #"Снова неудача - осталось только немного заварки. Раз так, то остаётся только набрать воды. Я взял бутылку и пошёл к умывальникам."
    "There was no luck there either - the decanter was dry, and the half-empty teapot was no good for me. I grabbed some clean bottle, and went to the washing stands."
    window hide
    $ renpy.pause (0.5)    
    scene bg ext_aidpost_night  with dissolve
    play ambience ambience_camp_center_night    
    #"Уже возле медпункта меня осенило, что кран с водой там и так должен быть, хоть я его и не замечал. Но как же иначе?"
    "When I passed the infirmary, it dawned on me that there must be a sink somewhere inside, even though I never noticed it before."
    window hide
    $ renpy.pause (0.5)     
    scene bg int_house_of_mt_night  with dissolve
    play ambience ambience_int_cabin_night  
    #"Чтобы не показаться совсем идиотом, я вернулся в домик и отправил остатки заварки в бутылку. И побольше сахара. Может, и печенья взять? Ещё что-нибудь? Книжку?"
    "In order to not look like an idiot, I went back to the cabin and dropped every tea leaf into the bottle."
    th "Here it goes... And some sugar won't do any harm. {w}Maybe I should take cookies, too? Or something else for her?.. A book, maybe?"
    window hide
    $ renpy.pause (0.5)     
    stop music fadeout 5    
    scene bg ext_aidpost_night  with dissolve
    play ambience ambience_camp_center_night    
    $ renpy.pause (0.5)    
    #"Наконец, вернувшись к медпункту, я понял, что опоздал. Оттуда уже выходила Ольга Дмитриевна."
    "When I finally got back to the infirmary, I realized that I was too late. Olga Dmitrievna was leaving from there."
    window hide
    show mt angry panama pioneer at cright   with dspr
    window show
    #mt "А вот и ты."
    mt "Here you are."
    #me "Ругаться будете?"
    me "Are you going to scold me?"
    #mt "Бесполезно. У тебя же напрочь отсутствует какая-то ответственность. И откуда ты такой взялся на мою голову?"
    mt "Why, it's a pointless waste of time. You have no responsibility at all! And where have you been?"
    #th "По-моему, она всё-таки ругается."
    th "...It looks like scolding to me."
    #mt "И требовалось от тебя всего-ничего. Скажи, за каким лешим вы полезли в лес?"
    mt "You had one job... Why were you deep in the forest? How could you drag her out there?"
    play music music_list["two_glasses_of_melancholy"] fadein 5 
    #th "Не хочется обвинять Лену, и уж тем более - Саманту."
    th "It wouldn't be right to accuse Lena..."
    #me "Так вышло."
    me "Well, it just happened. I got lost. I'm sorry..."
    window hide
    $ renpy.pause(0.4) 
    show un shy pioneer at left   with dspr
    $ renpy.pause(0.3)     
    window show
    #un "Это они меня искали. А то я... заблудилась."
    un "They were looking for me. It was me who got... lost."
    #mt "И я должна в это поверить?"
    mt "You think I am going to believe that?"
    #un "Вы же сами всех послали в тёмный лес за хворостом. А в лесу темно и змеи."
    un "You sent us to find some brushwood. It's dark in the forest."
    me "Yeah, and there are snakes! You shouldn't send people there! Why didn't we bring the firewood with us?"
    #mt "Как дети малые."
    mt "Quit behaving like kids."
    show un normal pioneer at left   
    #me "А я теперь официально ненавижу этот лес. Отстраните меня от всех походов, сделайте одолжение."
    me "Since today, I officially hate the forest. Do me a favor, suspend me from all hikes."
    #mt "Теперь от тебя ничего и не требуется, можешь хоть до конца смены с кровати не вставать."
    mt "You don't have to do anything now! You can lie on your bed till the end of the camp."
    # un "Он же переводчик. И в пьесе играет."
    un "But he is a translator. And he has a role in a play."
    #mt "С этим покончено. Мы отправляем Саманту в больницу."
    mt "Not anymore. We are sending Samantha to a hospital."
    show un sad pioneer at left   
    #un "Но ведь укус не опасен."
    un "Why? The bite is not threatening her life."
    #mt "Может быть аллергия на лекарство. Рисковать нельзя."
    mt "She may be allergic to the medicine. We cannot risk it."
    #me "Ей будет плохо в больнице. А какой тот риск?"
    me "Is there a risk? She will not like the hospital, I can tell it for sure."
    #mt "От тебя подальше - точно здоровее будет."
    mt "She'll be fine as long as she'll be away from you."
    #me "Вы же не отошлёте Саманту только назло мне?"
    me "You won't send Samantha away just to spite me, will you?"
    #mt "Назло тебе? Какое самомнение. Исключительно по медицинским показаниям. Сейчас Виола посмотрит ещё раз, и решим."
    mt "Just to spite you? So much self-conceit! We'll act exclusively because of medical indications. Viola will make her decision."
    window hide
    $ renpy.pause (0.4)
    hide mt  
    hide un 
    with dspr
    $ renpy.pause(0.5)
    window show
    # "К Саманте меня, естественно, не пустили, но бутылку приняли. Затем вожатая пошла в домик, а мы с Леной остались ждать снаружи."
    "Of course they didn't let me visit Samantha, but they did take the bottle. Then the counselor went to her cabin, leaving Lena and me waiting outside."
    #th "Неужели и правда увезут Саманту? Да как же я тогда? А она как?"
    th "Are they really going to take her away? If so, how I am going to...? And what about her?"
    window hide
    $ renpy.pause(0.5)    
    show un normal pioneer at left   with dspr  
    $ renpy.pause(0.5)
    window show     
    #un "Не смогла Виолу одну привести."
    un "I couldn't bring Viola by herself."
    #me "Ничего. Спасибо за поддержку."
    me "That's okay. Thanks for the help... and support."
    #un "Не за что."
    un "You're welcome."
    #me "Почему помогаешь? Можно узнать?"
    me "Why are you helping? If you don't mind me asking."
    #un "Разве я не помогала раньше?"
    un "Didn't I help you before?"  
    #me "Ты поняла, о чём я. Разве не хотела прогнать Саманту из лагеря?"
    me "You know what I mean. You weren't happy about Samantha presence in the camp."
    show un angry2 pioneer at left     
    un "So what? Do you care whether I'm happy or not?"
    me "You tried to kick Samantha away from the camp. It was your own words."
    un "I did no harm to her. You think I can hurt her now? You think I want it? Well, think as you want."    
    me "It's not like that... But I can't understand you still."    
    #un "Я лучше пойду. Удачи."
    un "I'd better be going. Good luck."
    window hide
    $ renpy.pause(0.3)    
    hide un  with dspr
    $ renpy.pause(0.3)
    window show
    #th "Чёрт меня за язык тянул. Лена была бы ценной союзницей в переговорах."
    th "...Why did I even start asking? Lena would be a good ally in further negotiations, and now I'm alone."  
    window hide
    $ renpy.pause(1)    
    window show
    # "Через какое-то время Ольга Дмитриевна вернулась и зашла в медпункт, а затем вышла наружу в сопровождении нашей импозантной медсестры."
    "After a while, Olga Dmitrievna returned to the infirmary, then came out, accompanied by our imposing nurse."
    window hide
    show cs normal glasses far at cright   
    show mt normal panama pioneer far at cleft   
    with dissolve
    $ renpy.pause(0.3)
    window show 
    #"К этому моменту я придумал уже много доводов в защиту своей позиции, но разговор не начинался - дамы молча наслаждались вечером."
    "By this time, I'd come up with a few arguments in defense of my position, but I couldn't present them, as the ladies haven't started a conversation. They just silently enjoyed the evening."
    window hide
    $ renpy.pause(1) 
    #cs "...Тогда спокойной ночи."
    cs "...Good night then."
    #mt "Спокойной ночи "
    mt "Good night."
    show cs smile glasses far at cright 
    #cs "А ты чего ждёшь тут? Как муж у роддома."
    cs "And why are you still here? Waiting, like a husband before a maternity hospital."
    #me "Э..."
    me "I..."
    #cs "А чай у тебя вкусный."
    cs "Your tea was nice by the way."
    show mt smile panama pioneer at cleft   
    #mt "Пошли, Семён."
    mt "Let's go, Semyon."
    window hide
    $ renpy.pause(0.7)    
    scene bg int_house_of_mt_night
    with dissolve    
    show mt normal pioneer 
    with dissolve
    play ambience ambience_int_cabin_night  
    $ renpy.pause(1) 
    window show    
    #"Ольга Дмитриевна занялась уборкой стола, не обращая на меня никакого внимания. Наконец, я не выдержал:"
    "Olga Dmitrievna started to clean up the table, not paying any attention to me. Finally, I couldn't stand this anymore."
    #me "Что вы решили?"
    me "What have you decided?"
    #mt "Чай надо новый заварить..."
    mt "We need to brew some new tea..."
    #me "Что медсестра сказала?"
    me "What did the nurse say?"
    show mt smile pioneer  
    #mt "Смешной. Ладно, расслабься уже. Без больницы обойдёмся."
    mt "You are funny. Okay, relax already. Samantha will stay here."
    #me "Фуф."
    me "Phew."
    #mt "Да какая больница? Это же был бы скандал. Просто разозлил ты меня."
    mt "What hospital anyway? To cause such a big scandal - I'm not an enemy to myself! I was simply angry with you."
    # me "И всё равно буду наказан, так?"
    me "And I'm still going to get punished, right?"
    #mt "А за что тебя наказывать? Не ты кашу заварил, я узнала. А за слабоволие тебя жизнь наказывает. Не понравился наш лес-то, говоришь?"
    mt "For what? I found out that it wasn't you who started this mess. Or punish you for being too soft? Life punishes you for that!"
    th "Too soft, eh? She say so as if she'd ever contradict Samantha."
    #th "Пф, слабоволие. Будто она сама когда-то перечила Саманте."

    show mt normal pioneer  
    #mt "Так, отнесу ей кое-какие вещи. Сегодня там заночует, дальше посмотрим. А тебе вот наказание - учи свою роль, да хорошенько учи!"
    mt "I'm going to take a few things to her. She's going to sleep there, at least tonight. And here is your punishment - memorize your role."
    hide mt  with dspr
    #"Ольга Дмитриевна бросила мне пьесу и вышла."
    "Olga gave me the script and left."
    stop music fadeout 5
    #"С неохотой я взялся за чтение, но скоро меня прервал стук в дверь."
    "Reluctantly, I started reading it..."
    play sound sfx_knocking_door_outside    
    extend " but after a moment, someone knocked on the door."
    #me "Войдите."
    me "Come in."
    "..."
    #th "Не входят..."
    th "They are not coming in..."
    window hide
    $ renpy.pause (0.5)
    scene bg ext_house_of_mt_night  with dissolve
    play ambience ambience_camp_center_night
    #"Любопытство взяло верх над ленью, и я вышел из домика. Тут же из-за угла вынырнула Алиса."
    "Curiosity prevailed over my laziness, so I went outside. Alisa jumped on me from the corner immediately."
    play music music_list["gentle_predator"] fadein 3
    window hide
    show dv normal pioneer  with dspr
    $ renpy.pause(0.3)  
    window show
    #dv "Ты один?"
    dv "Are you alone?"
    #me "Ага."
    me "Yep."
    #dv "Что у вас случилось в лесу?"
    dv "What happened in the forest?"
    # "Я вкратце рассказал ей. Конечно, некоторые моменты должны остаться тайной."
    "I briefly described the situation. Of course I kept some moments in secret."
    show dv guilty pioneer     
    #dv "Нехорошо получилось. Эта Лена..."
    dv "That went pretty badly. Oh, Lena..."
    #me "Она нам помогла."
    me "She helped us."
    #dv "Ну да..."
    dv "Yeah, she did..."
    #me "Ты её хорошо знаешь. Расскажешь, что с ней такое?"
    me "It seems you know her well. Can you tell me about her? I mean, what's wrong with her?"
    show dv smile pioneer  
    #dv "Нет."
    dv "No, I can't."
    me "You don't know it too?"
    dv "I won't tell you."    
    #me "...Вот же хорошая подруга. А говорят, женской дружбы не бывает."
    me "Oh... It makes you really good friend, I guess. {w}And they say there is no female friendship!" 
    #dv "Много ты тут дружбы видишь? Так, негласная договорённость. Держим чужие секреты."
    dv "You see much of friendship there? I'd call it a tacit agreement. Yes, we're keeping each other's secrets."
    #me "Тоже чего-то стоит. Значит, даже не намекнёшь? Я видел Лену такой разной, какая из них настоящая?"
    me "That's still something. {w}So, not a hint for me? You know, I saw Lena so different, it's almost different personalities... But I wonder which one is the real one?"
    show dv normal pioneer  
    #dv "...Все настоящие."
    dv "...All of them."
    #me "Как же так?"
    me "For real? How does it works?"
    show dv guilty pioneer     
    #dv "Некоторым тяжело сдерживать эмоции. Ладно... Я пришла сказать, что зря вас за ней отпустила. Вот."
    dv "Some people try really hard to control their emotions. Sometimes it works... weird."
    dv "Anyway... I came here to tell you that I shouldn't have let you go after her. That's it."
    #me "Не переживай по этому поводу."
    me "Don't worry about that."
    show dv normal pioneer  
    #dv "Переживать? Вот ещё. Просто не хочу международного скандала на моей совести."
    dv "Worry? Pfft. I just don't want to have an international scandal on my conscience."
    #me "Извинения приняты."
    me "Apologies are accepted." 
    show dv angry pioneer  
    #dv "Извинения?! Да ты издеваешься! Это ты подслушивал за нами!"
    dv "Apologies?! You're kidding me! You eavesdropped on us!"
    #me "Я всё равно ничего не понял. Кроме того, что Цой - лапочка."
    $ renpy.notify("Viktor Tsoi - a famous Soviet rock singer.")
    me "I didn't understand a word anyway. Well, besides that Tsoi is the cutest."
    show dv normal pioneer 
    #dv "Ещё скажи, что тебе не было интересно."
    dv "Don't say it wasn't interesting for you!"
    #me "Ни капельки. Нет, погоди. Припоминаю что-то про белобрысую. Кто это?"
    me "Not a scrap. {w}But wait. I remember something about a blonde girl. Who is it?"
    #dv "Будто сам не знаешь."
    dv "As if you don't know."
    #me "Допустим. И что за подарок, которому она будет рада?"
    me "Let's assume I do. But what gift is she gonna be glad to see?"
    #dv "Забудь. Лена была не в себе даже по её меркам."
    dv "Forget it. Lena was haywire back there, even for her standards."
    #me "Всё равно интересно."
    me "It's still interesting."
    #dv "А что ты меня-то спрашиваешь, это не мои слова."
    dv "And why are you asking me? Those are not my words."
    #me "Но ты же знаешь. Скажи."
    me "But you do know. Tell me."
    show dv smile pioneer  
    #dv "...{i}А ну-ка, давай-ка, плясать выходи!{/i}"
    $ renpy.notify("These are lines from a song from a very famous Soviet cartoon called \"Nu pogodi!\"")
    dv "...{i}Come on, let's go, let's sing!{/i}"
    #me "Да ты не только играешь, но и поёшь."
    me "Wow, you're not only an actress, but a singer too."
    #dv "Подпевай."
    dv "Sing along."
    #me "Ты не ответила про подарок."
    me "You didn't answer about the gift."
    #dv "Так подпевай. {i}А ну-ка, давай-ка, плясать выходи!{/i}"
    dv "Sing along then. {i}Come on, let's go, let's sing!{/i}"
    #me "...Пила, мать?"
    me "Are you drunk?"
    show dv normal pioneer  
    #dv "Да ну тебя."
    dv "Oh, c'mon."
    # me "Ладно-ладно. Подпоём, не жалко."
    me "Okay-okay. I'll sing, I don't mind."
    # th "И повод есть - всё хорошо закончилось."
    th "And there is a reason - everything ended well today."
    #dv "Что, слова забыл?"
    dv "You didn't forget the lyrics, did you?"
    # me "{i}Нет, Дед Мороз, погоди!{/i}"
    $ renpy.notify("Ded Moroz (Can be translated as \"Granddad Frost\") is a Russian (and Soviet) analogue of Santa Claus")
    me "{i}No, Ded Moroz, wait for me!{/i}"
    show dv laugh pioneer  
    # dv "Подожду-подожду, вспоминай."
    dv "I will, I will, don't worry."
    dv "{i}Everyone's waiting for my gifts! And so do you!{/i}"
    # me "{i}Наконец, сбываются все мечты!{/i}"
    me "{i}Finally, all dreams come true!{/i}"
    dv "..."
    me "..."
    window hide
    show dv shy pioneer 
    $ renpy.pause(0.3)  
    show mt laugh pioneer close at left   with dspr
    window show
    play music music_list["that_s_our_madhouse"]    
    mt "{i}My best gift is you!{/i}"
    window hide
    stop music fadeout 6
    $ renpy.pause(1)
    scene black  with fade2
    $ renpy.pause(1)
    stop ambience
    #"Весь этот день я держал в уме... {w}Хорошо, ПОЧТИ весь этот день я держал в уме... {w}что в любой момент может появиться Пионер. И снова высмеять меня с моими потугами в этом мире."
    "The day was intense, but I didn't forget our strange morning chat with Pioneer. I kept in mind that he can appear in front of me again, laughing of my vain attempts in this world."
    "He didn't. But instead, he appeared in my dream."
    
    #"Но этого не произошло. Вместо этого он мне приснился."
    window hide
    $ renpy.pause(0.5)    
    show sspi smile at center with dissolve
    $ renpy.pause(0.8)
    window show 
    play music music_list["drown"] fadein 2
    #pi "Подарить «Совёнок»? Старик, это может быть моим билетом отсюда! И пусть какой-нибудь бот вместо меня кукует. Ха-ха."
    pi "Give «Sovyonok» to someone? Man, I like this idea! It could be my ticket from here! Let some cuckoo bot stay here instead..."
    #pi "Глупо, да. А вдруг сработает? Ничего не срабатывало, а это сработает. Интересно, чем всё закончится."
    pi "Sounds kinda foolish, eh? No problem, I tried lots of stupid things here. When you tried everything, you're glad for any new chance."
    pi "What if it will work? Nothing else has, but this will... I wonder how it ends."
    #pi "Так что ты закругляйся поскорее. Сделай одолжение, я же тебя не трогал. Да-да, вся ерунда, которая у тебя происходит - это не я. Это всё ты."
    pi "So, finish up faster, do me a favor. I was kind to you, I didn't hurt you, did I?"
    pi "If you saw any nonsense here - it wasn't me, really. It's you."
    #pi "Так вот, рекомендую утопление. А я в долгу не останусь. Что ещё? Ответы тебе нужны? Спрашивай."
    pi "Don't wait till the end, leave this circle. I guess you are curious too, so why not? {w}I would recommend you to drown."
    pi "Do so, I will be in your debt. What do you need? You need the answers - ask."
    window hide
    $ renpy.pause(1.5)
    #me "А шёл бы ты к чёрту!"
    me "Go to hell!"
    window hide
    $ renpy.pause(0.5)  
    show sspi normal at center with dspr   
    hide sspi  with dissolve2
    $ renpy.pause(1)  
    window show
    #th "И пошёл же. Выходит, я могу его прогнать? {w}А что ещё могу? Посмотрим..."
    th "It worked?.. Turns out, I can banish him? Whoa!"
    th "What else can I do?.. We'll see, I guess..."

    window hide
    stop music fadeout 5
    $ renpy.pause(2)
    window show
    "{color=#fff}A small note! Day 4 ends here. As you may know, there is no translated Day 5 at the moment. We skipped it for now because Day 5 in an independent story and not the part of the main route.{/color}"
    "{color=#fff}But it is still a fun part and we will try add it here (well, not exactly HERE - naturally it goes after the Island part of the Day 4) - sooner or later.{/color}"
    "{color=#fff}Thank you for your attention. And go for Day 6!{/color}"
    window hide   
    jump ss_d6p_eng
    
    
label ss_notready_eng:
    scene cg sam_menu with fade
    $ renpy.pause (1)
    "Day 4 is over. Notice that this day has two separate ways (it splits after the forest/island choice)."
    "Further days are under translation. Although the forest part is considered as the main one, the fifth day continues the island part. Day 5 will be our next thing to release." 
    "Big thanks to our translation team. And thank you for reading!"    
    return 

          

    