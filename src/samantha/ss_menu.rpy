﻿init 200 python:
    renpy.music.register_channel('silent', 'voice', True) 
    renpy.music.set_volume(volume=1.0, delay=0, channel='silent')
    renpy.music.register_channel('ss_amb', 'music', True) 
    renpy.music.set_volume(volume=0.3, delay=0, channel='ss_amb')
    renpy.music.register_channel('ss_amb2', 'music', False) 
    renpy.music.set_volume(volume=0.3, delay=0, channel='ss_amb2')    
    renpy.music.register_channel('ss_mus', 'music', True) 
    renpy.music.set_volume(volume=1.0, delay=0, channel='ss_mus')
    renpy.music.register_channel('ss_mus2', 'music', True) 
    renpy.music.set_volume(volume=1.0, delay=0, channel='ss_mus2')    
    renpy.music.register_channel('ss_sfx', 'music', False) 
    renpy.music.set_volume(volume=1.0, delay=0, channel='ss_sfx')
    renpy.music.register_channel('ss_sfx2', 'music', False) 
    renpy.music.set_volume(volume=1.0, delay=0, channel='ss_sfx2')      
    import time
    sam_time_irl = time.localtime()
    if persistent.ss_widget == "on":
        while editoverlay__samantha in config.overlay_functions:
            config.overlay_functions.remove(editoverlay__samantha)
        config.overlay_functions.append(editoverlay__samantha) 

init:


    $ solaricut = "samantha/sound/menu/solaricut.ogg"    
    $ solarilong = "samantha/sound/menu/solarilong.ogg"    
    $ solariclick = "samantha/sound/menu/solariclick.ogg"
    $ airport_ambience = "samantha/sound/menu/airport_ambience.ogg"
    $ airport_16 = "samantha/sound/menu/16s.ogg"  
    $ airport_notif1 = "samantha/sound/menu/airport_notif1.ogg"
    $ airport_notif2 = "samantha/sound/menu/airport_notif2.ogg"
    $ airport_notif3 = "samantha/sound/menu/airport_notif3.ogg" 
    $ airport_notif4  = "samantha/sound/menu/airport_notif4.ogg"
    $ airport_notif5  = "samantha/sound/menu/airport_notif5.ogg"
    $ bag_zipper = "samantha/sound/menu/zipper.ogg"     
    image no_sam = "samantha/image/menu/no_sam.png"
    image frames_fit = "samantha/image/menu/frames_fit.png"   
    image screen0sw = "samantha/image/menu/screen0sw.png"    
    image screen1sw = "samantha/image/menu/screen1sw.png"  
    image screen2sw = "samantha/image/menu/screen2sw.png"  
    image screen3sw = "samantha/image/menu/screen3sw.png"  
    image screen4sw = "samantha/image/menu/screen4sw.png"  
    image screen5sw = "samantha/image/menu/screen5sw.png"
    image screen5sw2 = "samantha/image/menu/screen5sw2.png" 
    image screen5sw3 = "samantha/image/menu/screen5sw3.png"     
    image screen6sw = "samantha/image/menu/screen6sw.png"  
    image screen7sw = "samantha/image/menu/screen7sw.png"  
    image screen8sw = "samantha/image/menu/screen8sw.png" 
    image screen5temp = "samantha/image/menu/screen5.png"
    image sam_menu_new = "samantha/image/menu/sam_menu_new.jpg"
    image sam_menu_start = "samantha/image/menu/sam_menu_start.png" 
    image sam_menu_start_eng = "samantha/image/menu/eng/sam_menu_start_eng.png"     
    image screen1switch = "samantha/image/menu/eng/screen1switch.png"
    image screen2switch = "samantha/image/menu/eng/screen2switch.png"
    image ss_ru_lang = "samantha/image/menu/options/ru_lang.png"
    image ss_eng_lang = "samantha/image/menu/options/eng_lang.png"
    image s_menu_bag = "samantha/image/menu/s_menu_bag.jpg"    
    image s_menu_bag_open = "samantha/image/menu/s_menu_bag_open.jpg"
    image s_depart = "samantha/image/menu/depart.png"    
    image ss_exp1 = "samantha/image/stuff/exp1.png"     
    image ss_exp2 = "samantha/image/stuff/exp2.png"    
    image ss_hlop a = "samantha/image/stuff/hlop1.png"  
    image ss_hlop b = "samantha/image/stuff/hlop2.png"
    image ss_hlop c = "samantha/image/stuff/hlop3.png" 
    image flyin_petr_t = "samantha/image/stuff/petr_t.png"    
    image flyin_petr3 = "samantha/image/stuff/petr3.png"
    image flyin_petr2 = "samantha/image/stuff/petr2.png"
    image flyin_petr9 = "samantha/image/stuff/petr9.png"
    image flyin_petr11 = "samantha/image/stuff/petr11.png"    
    image flyin_petr1 = "samantha/image/stuff/petr1.png"
    image flyin_petr8 = "samantha/image/stuff/petr8.png"    
    image flyin_petr20 = "samantha/image/stuff/petr2.png"
    image flyin_petr90 = "samantha/image/stuff/petr9.png"
    image flyin_petr110 = "samantha/image/stuff/petr11.png" 
    image flyin_petr10 = im.Flip( "samantha/image/stuff/petr1.png", vertical=False, horizontal=True)     
    image flyin_petr30 = "samantha/image/stuff/petr3.png"    
    image flyin_petr80 = im.Flip( "samantha/image/stuff/petr8.png", vertical=False, horizontal=True) 
    image ss_part_a1 = "samantha/image/stuff/a1.png" 
    image ss_part_a2 = "samantha/image/stuff/a2.png" 
    image ss_part_a3 = "samantha/image/stuff/a3.png" 
    image ss_part_a4 = "samantha/image/stuff/a4.png" 
    image ss_part_a5 = "samantha/image/stuff/a5.png" 
    image ss_part_a6 = "samantha/image/stuff/a6.png" 
    image ss_part_a7 = "samantha/image/stuff/a7.png" 
    image ss_part_a8 = "samantha/image/stuff/a8.png"     
    image ss_part_b1 = "samantha/image/stuff/b1.png" 
    image ss_part_b2 = "samantha/image/stuff/b2.png" 
    image ss_part_b3 = "samantha/image/stuff/b3.png" 
    image ss_part_b4 = "samantha/image/stuff/b4.png"
    image ss_part_c1 = "samantha/image/stuff/c1.png" 
    image ss_part_c2 = "samantha/image/stuff/c2.png" 
    image ss_part_c3 = "samantha/image/stuff/c3.png" 
    image ss_part_c4 = "samantha/image/stuff/c4.png"
    image ss_part_d1 = "samantha/image/stuff/d1.png" 
    image ss_part_d2 = "samantha/image/stuff/d2.png" 
    image ss_part_d3 = "samantha/image/stuff/d3.png" 
    image ss_part_d4 = "samantha/image/stuff/d4.png"    
    image depart_switch:
        "samantha/image/menu/depart_sw1.png"
        pause 0.25
        "samantha/image/menu/depart_sw2.png"
        pause 0.25  
        "samantha/image/menu/depart_sw3.png"
        pause 0.25  
        "samantha/image/menu/depart_sw4.png"
        pause 0.25  
        "samantha/image/menu/depart_sw5.png"
        pause 0.25  
        "samantha/image/menu/depart_sw6.png"
        pause 0.25  

    
    image nasty_letters_eng:
        "samantha/image/menu/eng/switch_a1.png"
        pause 0.14
        "samantha/image/menu/eng/switch_a2.png"
        pause 0.14  
        "samantha/image/menu/eng/switch_a3.png"
        pause 0.14  
        "samantha/image/menu/eng/switch_a4.png"
        pause 0.14  
        "samantha/image/menu/eng/switch_a5.png"
        pause 0.14  
        "samantha/image/menu/eng/switch_a6.png"
        pause 0.14  
        "samantha/image/menu/eng/switch_a7.png"
        pause 0.14  
        "samantha/image/menu/eng/switch_a8.png"
        pause 0.14  
        "samantha/image/menu/eng/switch_a9.png"
        pause 0.14  
        "samantha/image/menu/eng/switch_a10.png"
        pause 0.14   
        "samantha/image/menu/eng/switch_a11.png"
        pause 0.14 
        "samantha/image/menu/eng/switch_a12.png"
        pause 0.14 
        "samantha/image/menu/eng/switch_a13.png"
        pause 0.14         
        
    image nasty_letters_a:
        "samantha/image/menu/switcha1.png" 
        pause 0.07
        "samantha/image/menu/switcha3.png"
        pause 0.07
        "samantha/image/menu/switcha2.png"
        pause 0.07
        "samantha/image/menu/switcha1.png" 
        pause 0.09
        "samantha/image/menu/switcha2.png" 
        pause 0.09
        "samantha/image/menu/switcha3.png" 
        pause 0.09
        "samantha/image/menu/switcha4.png"
        pause 1.1        
        repeat  
    image nasty_letters_b: 
        pause 0.1    
        "samantha/image/menu/switchb1.png" 
        pause 0.1
        "samantha/image/menu/switchb2.png"
        pause 0.08
        "samantha/image/menu/switchb3.png"
        pause 0.08
        "samantha/image/menu/switchb2.png" 
        pause 0.08
        "samantha/image/menu/switchb3.png" 
        pause 0.08
        "samantha/image/menu/switchb1.png" 
        pause 0.1
        "samantha/image/menu/switchb2.png"
        pause 0.08
        "samantha/image/menu/switchb1.png" 
        pause 0.1        
        "samantha/image/menu/switchb4.png"
        pause 0.8       
        repeat 

    image nasty_letters_c:
        pause 0.3    
        "samantha/image/menu/switchc1.png" 
        pause 0.08
        "samantha/image/menu/switchc2.png"
        pause 0.08
        "samantha/image/menu/switchc1.png"
        pause 0.08
        "samantha/image/menu/switchc2.png" 
        pause 0.08
        "samantha/image/menu/switchc3.png" 
        pause 0.08
        "samantha/image/menu/switchc2.png" 
        pause 0.08
        "samantha/image/menu/switchc3.png" 
        pause 0.1        
        "samantha/image/menu/switchc4.png"
        pause 0.1        
        "samantha/image/menu/switchc5.png"
        pause 0.1        
        "samantha/image/menu/switchc6.png"
                            
        pause 0.75        
        repeat    

    image nasty_letters_d:
        pause 0.5    
        "samantha/image/menu/switchd2.png"
        pause 0.1
        "samantha/image/menu/switchd1.png"
        pause 0.1
        "samantha/image/menu/switchd2.png" 
        pause 0.1
        "samantha/image/menu/switchd3.png" 
        pause 0.1
        "samantha/image/menu/switchd1.png" 
        pause 0.1
        "samantha/image/menu/switchd3.png" 
        pause 0.1 
        "samantha/image/menu/switchd6.png" 
        pause 0.1        
        "samantha/image/menu/switchd4.png"
        pause 0.1        
        "samantha/image/menu/switchd5.png"
                            
        pause 0.65        
        repeat   
        
    image nasty_letters_e:
        pause 0.8    
        "samantha/image/menu/switche1.png"
        pause 0.08
        "samantha/image/menu/switche2.png"
        pause 0.08
        "samantha/image/menu/switche1.png" 
        pause 0.08
        "samantha/image/menu/switche2.png" 
        pause 0.08
        "samantha/image/menu/switche1.png" 
        pause 0.08
        "samantha/image/menu/switche3.png" 
        pause 0.1 
        "samantha/image/menu/switche4.png" 
        pause 0.1        
        "samantha/image/menu/switche5.png"
                            
        pause 0.65        
        repeat 
        
    image nasty_letters_n_prep:
        "samantha/image/menu/switcha2.png"
        pause 0.1        
        "samantha/image/menu/switchn1.png"
        pause 1.2    
    image nasty_letters_x_prep:
        "samantha/image/menu/eng/switch1.png"
        pause 0.1
        "samantha/image/menu/eng/switch2.png"
        pause 0.1        
        "samantha/image/menu/switchn1.png"
    
        pause 1.2     
        
    image nasty_letters_n:    
        "samantha/image/menu/switchn2.png"
        pause 0.20
        "samantha/image/menu/switchn3.png"
        pause 0.20
        "samantha/image/menu/switchn4.png"
        pause 0.20
        "samantha/image/menu/switchn5.png"
        pause 0.20
        "samantha/image/menu/switchn6.png"
        pause 0.20
        "samantha/image/menu/switchn66.png"
        pause 0.20        
        "samantha/image/menu/switchn7.png"
        pause 0.20
        "samantha/image/menu/switchn8.png"
        pause 0.20
        "samantha/image/menu/switchn9.png"
        pause 0.20
        "samantha/image/menu/switchn10.png"
        pause 0.20
        "samantha/image/menu/switchn11.png"
        pause 0.20
        "samantha/image/menu/switchn12.png"
        pause 0.20
        "samantha/image/menu/switchn22.png"
        pause 0.20        
        "samantha/image/menu/switchn13.png"        
        pause 0.9                         

    image nasty_letters_x:    
        "samantha/image/menu/eng/switch_b1.png"
        pause 0.28
        "samantha/image/menu/eng/switch_b2.png"
        pause 0.24
        "samantha/image/menu/eng/switch_b3.png"
        pause 0.24
        "samantha/image/menu/eng/switch_b4.png"
        pause 0.24
        "samantha/image/menu/eng/switch_b5.png"
        pause 0.24
        "samantha/image/menu/eng/switch_b6.png"
        pause 0.24        
        "samantha/image/menu/eng/switch_b7.png"
        pause 0.24
        "samantha/image/menu/eng/switch_b8.png"
        pause 0.24
        "samantha/image/menu/eng/switch_b9.png"
        pause 0.24
        "samantha/image/menu/eng/switch_b10.png"
        pause 0.24
        "samantha/image/menu/eng/switch_b11.png"
        pause 0.22
        "samantha/image/menu/eng/switch_b12.png"
        pause 0.22
        "samantha/image/menu/eng/switch_b13.png"       
        pause 0.9         
label sam_back:

    if _preferences.language == None:
        pass
    else:  
        jump ss_menu_eng 

    play sound solaricut    
    scene sam_menu_new     
    show nasty_letters_a 
    show nasty_letters_b
    show nasty_letters_c
    show nasty_letters_d
    show nasty_letters_e     
    $ renpy.pause (1.5)
    hide nasty_letters_a
    hide nasty_letters_b
    hide nasty_letters_c
    hide nasty_letters_d
    hide nasty_letters_e     
    call screen ss_main_menu
                
screen ss_main_menu:
    imagemap:
            ground "samantha/image/menu/sam_menu_start.png"  
            hover "samantha/image/menu/sam_menu_hover.png"
            alpha True

            hotspot (300, 300, 484, 38) action Jump("nasty_road_to_ss_d1") #Start() action Jump("goto_exit")            
            hotspot (295, 365, 484, 38) action Jump("ss_pick_a_day")
            hotspot (295, 430, 484, 38) action Jump("ss_options_new")
#            hotspot (295, 500, 484, 38) action Help()
            hotspot (295, 500, 484, 38) action Jump("temp_gal")
            hotspot (295, 565, 484, 38) action Quit(confirm=True)            

label temp_gal:
    show s_menu_bag with dissolve
    $ renpy.pause (0.2)     
    play sound bag_zipper
    $ renpy.pause (0.7)    
    show s_menu_bag_open with dissolve
    $ renpy.pause (0.2) 
    show frames_fit at Zoom((1920, 1080), (0, 0, 3840, 2160), (960, 540, 1920, 1080), 1.0)    
    $ renpy.pause (1.0)
    hide frames_fit
    call screen ssg_gallery 
         
#    menu:
#        "day500":            
#            jump ss_day500
#        "gal":
#            call screen ssg_gallery         


label nasty_road_to_ss_d1:
    show sam_menu_start
    play sound solariclick
    $ renpy.pause (0.2)    
    show nasty_letters_n_prep    
    $ renpy.pause (0.9)
    play sound solarilong
    $ renpy.pause (0.2)    
    show nasty_letters_n
    $ renpy.pause (4.0)    
    show no_sam with dissolve
    $ renpy.pause (0.5) 
    jump ss_d1_setup

label ss_pick_a_day:
    show sam_menu_start
    play sound solariclick
    $ renpy.pause (0.2)
    play sound solariclick
    hide sam_menu_start    
    show screen0sw
    $ renpy.pause (0.1)   
    hide screen0sw
    $ renpy.pause (0.2)
    play sound solariclick 
    $ renpy.pause (0.2)
    play sound solariclick    
    show screen1sw    
    $ renpy.pause (0.1)
    hide screen1sw
    call screen ss_pick_day 
    
screen ss_pick_day:    
    imagemap:
            ground "samantha/image/menu/screen1.png"  
            hover "samantha/image/menu/screen1_hover.png"
            alpha True

            hotspot (300, 300, 540, 40) action Jump("ss_d1_setup")      
            hotspot (300, 365, 540, 40) action Jump("ss_d2_setup")
            hotspot (300, 430, 540, 40) action Jump("ss_d3_options")
            hotspot (300, 500, 540, 40) action Jump("ss_d4_options")
            hotspot (300, 565, 540, 40) action Jump("ss_d5_options")
            hotspot (880, 300, 540, 40) action Jump("ss_d6_options")
            hotspot (880, 365, 540, 40) action Jump("ss_d7_options")
            hotspot (880, 430, 540, 40) action Jump("ss_d8_setup")
            hotspot (880, 500, 540, 40) action Jump("ss_d9_setup")
            hotspot (880, 565, 540, 40) action Jump("ss_d10_options")
            hotspot (880, 700, 540, 40) action Jump("ss_pick_a_route")
            hotspot (300, 700, 540, 40) action Jump("sam_back")            

label ss_pick_a_route: 
    play sound solariclick
    $ renpy.pause (0.2)
    play sound solariclick        
    show screen1sw
    $ renpy.pause (0.1)   
    hide screen1sw
    show screen2sw    
    $ renpy.pause (0.1)
    hide screen2sw
    call screen ss_pick_route
    
screen ss_pick_route:
    imagemap:
            ground "samantha/image/menu/screen2.png"  
            hover "samantha/image/menu/screen2_hover.png"
            alpha True    
            hotspot (300, 300, 540, 40) action Jump("ss_setup_dvroute")
            hotspot (300, 500, 540, 40) action Jump("ss_setup_ssroute")
            hotspot (300, 565, 540, 40) action Jump("ss_day500")
            hotspot (300, 630, 540, 40) action Jump("ss_year3")            
            hotspot (880, 435, 540, 40) action Jump("ssfears_setup")
            hotspot (880, 500, 540, 40) action Jump("ss_dlc_setup")
            hotspot (300, 700, 540, 40) action Jump("sam_back_to_pick")   

label sam_back_to_pick:
    play sound solariclick
    $ renpy.pause (0.2)
    show screen1sw    
    $ renpy.pause (0.1)
    hide screen1sw
    call screen ss_pick_day   
            
label ss_d3_options:
    play sound solariclick
    $ renpy.pause (0.2)
    play sound solariclick        
    show screen1sw
    $ renpy.pause (0.1)   
    hide screen1sw
    show screen3sw    
    $ renpy.pause (0.1)
    hide screen3sw
    call screen ss_d3_screen
    
screen ss_d3_screen:
    imagemap:
            ground "samantha/image/menu/screen3.png"  
            hover "samantha/image/menu/screen3_hover.png"
            alpha True    
            hotspot (300, 430, 540, 40) action Jump("ss_d3_setup_good")
            hotspot (300, 500, 540, 40) action Jump("ss_d3_setup_fail")
            hotspot (300, 565, 540, 40) action Jump("ss_d3_setup_dvfag")
            hotspot (300, 700, 540, 40) action Jump("sam_back_to_pick") 

label ss_d4_options:
    play sound solariclick
    $ renpy.pause (0.2)
    play sound solariclick        
    show screen1sw
    $ renpy.pause (0.1)   
    hide screen1sw
    show screen4sw    
    $ renpy.pause (0.1)
    hide screen4sw
    call screen ss_d4_screen
screen ss_d4_screen:
    imagemap:
            ground "samantha/image/menu/screen4.png"  
            hover "samantha/image/menu/screen4_hover.png"
            alpha True    
            hotspot (300, 430, 540, 40) action Jump("ss_d4_setup_civan")
            hotspot (300, 500, 540, 40) action Jump("ss_d4_setup_cam")
            hotspot (300, 565, 540, 40) action Jump("ss_d4_setup_god")
            hotspot (300, 700, 540, 40) action Jump("sam_back_to_pick") 
            
label ss_d5_options:
    play sound solariclick
    $ renpy.pause (0.2)
    play sound solariclick        
    show screen1sw
    $ renpy.pause (0.1)   
    hide screen1sw
    show screen5sw
    play sound solariclick     
    $ renpy.pause (0.1)
    hide screen5sw
    show screen5temp    
    show screen5sw3    
    $ renpy.pause (0.1)
    play sound solariclick 
    $ renpy.pause (0.1)    
    hide screen5sw3    
    show screen5sw2    
    $ renpy.pause (0.2)
    hide screen5sw2
    hide screen5temp
    call screen ss_d5_screen
screen ss_d5_screen:
    imagemap:
            ground "samantha/image/menu/screen5.png"  
            hover "samantha/image/menu/screen5_hover.png"
            alpha True    
            hotspot (300, 430, 540, 40) action Jump("ss_d5_setup_owl")
            hotspot (300, 500, 540, 40) action Jump("ss_d5_setup_ror")
            hotspot (300, 565, 540, 40) action Jump("ss_d5_setup_doc")
            hotspot (300, 700, 540, 40) action Jump("sam_back_to_pick") 

label ss_d6_options:
    play sound solariclick
    $ renpy.pause (0.2)
    play sound solariclick        
    show screen1sw
    $ renpy.pause (0.1)   
    hide screen1sw
    show screen6sw    
    $ renpy.pause (0.1)
    hide screen6sw
    call screen ss_d6_screen
screen ss_d6_screen:
    imagemap:
            ground "samantha/image/menu/screen6.png"  
            hover "samantha/image/menu/screen6_hover.png"
            alpha True    
            hotspot (300, 430, 540, 40) action Jump("ss_d6_setup_ro")
            hotspot (300, 500, 540, 40) action Jump("ss_d6_setup_mc")
            hotspot (300, 565, 540, 40) action Jump("ss_d6_setup_gc")
            hotspot (300, 700, 540, 40) action Jump("sam_back_to_pick") 
            hotspot (880, 430, 540, 40) action Jump("ss_d6_setup_ro2")
            hotspot (880, 500, 540, 40) action Jump("ss_d6_setup_mc2")
            hotspot (880, 565, 540, 40) action Jump("ss_d6_setup_gc2")            

label ss_d7_options:
    play sound solariclick
    $ renpy.pause (0.2)
    play sound solariclick        
    show screen1sw
    $ renpy.pause (0.1)   
    hide screen1sw
    show screen7sw    
    $ renpy.pause (0.1)
    hide screen7sw
    call screen ss_d7_screen
screen ss_d7_screen:
    imagemap:
            ground "samantha/image/menu/screen7.png"  
            hover "samantha/image/menu/screen7_hover.png"
            alpha True    
            hotspot (300, 430, 540, 40) action Jump("ss_d7_setup_yel")
            hotspot (300, 500, 540, 40) action Jump("ss_d7_setup_purp")
            hotspot (300, 565, 540, 40) action Jump("ss_d7_setup_by")
            hotspot (300, 630, 540, 40) action Jump("ss_d7_setup_bp")            
            hotspot (300, 700, 540, 40) action Jump("sam_back_to_pick") 

label ss_d10_options:
    play sound solariclick
    $ renpy.pause (0.2)
    play sound solariclick        
    show screen1sw
    $ renpy.pause (0.1)   
    hide screen1sw
    show screen8sw    
    $ renpy.pause (0.1)
    hide screen8sw
    call screen ss_d10_screen
screen ss_d10_screen:
    imagemap:
            ground "samantha/image/menu/screen8.png"  
            hover "samantha/image/menu/screen8_hover.png"
            alpha True    
            hotspot (300, 365, 540, 40) action Jump("ss_d10_setup_fox")
            hotspot (300, 500, 540, 110) action Jump("ss_d10_setup_ala")
          
            hotspot (300, 700, 540, 40) action Jump("sam_back_to_pick") 

label ss_d1_setup:
            stop ss_amb fadeout 1
            stop ss_amb2 fadeout 1
            scene black with fade
            jump ss_d1            
label ss_d2_setup:
            stop ss_amb fadeout 1
            stop ss_amb2 fadeout 1 
            $ sp_dv = (sp_dv) + (1)
            $ got_note = True
            $ got_kicked = True
            $ sp_sp = (sp_sp) - (1)
            $ bush_shirt = True
            jump ss_d2   

label ss_d3_setup_good:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ sp_dv = (sp_dv) + (2)
                    $ sp_sp = (sp_sp) - (1)
                    $ sp_ss = (sp_ss) + (3)
                    $ sp_sl = (sp_sl) + (1)
                    $ sp_un = (sp_un) + (2)
                    $ got_note = True
                    $ got_kicked = True
                    $ bush_shirt = True
                    $ got_candies = True
                    $ gave_guitar = True
                    $ dv_thi = True
                    $ rom_eo = True
                    jump ss_d3
label ss_d3_setup_fail:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ sp_dv = (sp_dv) + (1)
                    $ got_note = True
                    $ got_kicked = True
                    $ sp_sp = (sp_sp) - (2)
                    $ bush_shirt = True
                    $ sp_us = (sp_us) - (1)
                    $ sp_sl = (sp_sl) + (1)
                    $ sp_dv = (sp_dv) - (1)
                    $ mer_cu = True
                    $ d2_cyb = True
                    $ sp_sh = (sp_sh) - (1)
                    $ sp_ss = (sp_ss) - (1)
                    jump ss_d3
label ss_d3_setup_dvfag:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ sp_dv = (sp_dv) + (3)
                    $ sp_ss = (sp_ss) + (3)
                    $ sp_sp = (sp_sp) - (1)
                    $ got_note = True
                    $ got_kicked = True
                    $ kiss_dv = True
                    $ bush_shirt = True
                    $ got_candies = True
                    $ gave_guitar = True
                    $ dv_thi = True
                    $ rom_eo = True
                    jump ss_d3
label ss_d4_setup_civan:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    play sound CIvan
                    $ d3_usfail = True
                    $ sl_ft = True 
                    $ sp_un = 6
                    $ sp_ss = 5
                    $ sp_dv = 4
                    $ sp_sl = 6
                    $ un_inv = True 
                    $ mer_cu == True
                    $ sp_spts = 1
                    jump ss_d4start                    
label ss_d4_setup_cam:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    play sound DGirl
                    $ sp_un = 4
                    $ sp_ss = 7
                    $ sp_dv = 6
                    $ kiss_dv = True
                    $ sp_sl = 5
                    $ rom_eo = True
                    $ un_inv = True 
                    $ sp_spts = 3
                    jump ss_d4start
label ss_d4_setup_god:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    play sound Codebreaker
                    $ sp_un = 7
                    $ sp_ss = 7
                    $ sp_dv = 6
                    $ sp_sl = 4
                    $ un_inv = True 
                    $ rom_eo = True
                    $ sp_spts = 2
                    $ kiss_dv = True
                    $ ss_godmode = True
                    jump ss_d4start
                    
label ss_d5_setup_owl:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    play sound Owl
                    $ sp_un = 8
                    $ sp_ss = 5
                    $ sp_dv = 6
                    $ sp_sl = 6
                    jump ss_d5 
label ss_d5_setup_ror:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    play sound Rorschach                
                    $ sp_un = 5
                    $ sp_ss = 6
                    $ sp_dv = 8
                    $ sp_sl = 5
                    jump ss_d5                   
label ss_d5_setup_doc:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    play sound DM                   
                    $ sp_un = 13
                    $ sp_ss = 13
                    $ sp_dv = 13
                    $ sp_sl = 13             
                    jump ss_d5 
                    
label ss_d6_setup_ro:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ sp_dv = 6
                    $ sp_un = 5
                    $ sp_ss = 10
                    $ sp_sl = 5                 
                    $ dv_thi = True
                    $ rom_eo = True
                    $ mer_cu = False                    
                    jump ss_d6p          
label ss_d6_setup_mc:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ sp_dv = 5
                    $ sp_un = 6
                    $ sp_ss = 7
                    $ sp_sl = 6                     
                    $ rom_eo = False
                    $ mer_cu = True 
                    $ dv_thi = False                    
                    jump ss_d6p               
label ss_d6_setup_gc:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ sp_dv = 7
                    $ sp_un = 7
                    $ sp_ss = 13
                    $ sp_sl = 7                     
                    $ rom_eo = False
                    $ mer_cu = True 
                    $ dv_thi = False                    
                    jump ss_d6p
                    
label ss_d6_setup_ro_eng:
                    scene black with fade
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ sp_dv = 6
                    $ sp_un = 5
                    $ sp_ss = 10
                    $ sp_sl = 5                 
                    $ dv_thi = True
                    $ rom_eo = True
                    $ mer_cu = False                    
                    jump ss_d6p_eng           
label ss_d6_setup_mc_eng:
                    scene black with fade
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ sp_dv = 5
                    $ sp_un = 6
                    $ sp_ss = 7
                    $ sp_sl = 6                     
                    $ rom_eo = False
                    $ mer_cu = True 
                    $ dv_thi = False                    
                    jump ss_d6p_eng                
label ss_d6_setup_gc_eng:
                    scene black with fade
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ sp_dv = 7
                    $ sp_un = 7
                    $ sp_ss = 13
                    $ sp_sl = 7                     
                    $ rom_eo = False
                    $ mer_cu = True 
                    $ dv_thi = False                    
                    jump ss_d6p_eng                    
                    
                    
label ss_d6_setup_ro2:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1
                    $ sp_dv = 7
                    $ sp_un = 5
                    $ sp_ss = 11
                    $ sp_sl = 6
                    $ d6_dv_learned = 2 
                    $ d6_kissing = 1                     
                    $ d6_slchoice = 1  
                    menu:
                        "Авторские стихи":
                            jump ss_d6din
                        "Исправленные стихи":
                            jump ss_d6dinedit 
label ss_d6_setup_mc2:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1
                    $ sp_dv = 5
                    $ sp_un = 7
                    $ sp_ss = 7
                    $ sp_sl = 8 
                    $ d6_treasure = 2 
                    menu:                   
                        "Авторские стихи":
                            jump ss_d6din
                        "Исправленные стихи":
                            jump ss_d6dinedit                       
label ss_d6_setup_gc2:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ sp_dv = 8
                    $ sp_un = 8
                    $ sp_ss = 14
                    $ sp_sl = 10
                    $ sp_mizul = 3
                    $ d6_treasure = 1   
                    menu:                   
                        "Авторские стихи":
                            jump ss_d6din
                        "Исправленные стихи":
                            jump ss_d6dinedit  

label ss_d7_setup_yel:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ ss_friend = True
                    $ sp_un = 6
                    $ sp_dv = 10                    
                    $ d6_unchoice = 0
                    $ sp_sl = 9
                    jump ss_d6suddenmtmenu
label ss_d7_setup_purp:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ ss_friend = True    
                    $ d6_unchoice = 3    
                    $ sp_dv = 5                 
                    $ sp_un = 9
                    $ sp_sl = 6
                    jump ss_d6suddenmtmenu 
label ss_d7_setup_by:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ ss_friend = False               
                    $ d6_unchoice = 1
                    $ sp_un = 5
                    $ sp_dv = 9                     
                    $ sp_sl = 8
                    jump ss_d6suddenmtmenu        
label ss_d7_setup_bp:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ ss_friend = False
                    $ d6_unchoice = 2                   
                    $ sp_un = 8
                    $ sp_sl = 5
                    $ sp_dv = 6                     
                    jump ss_d6suddenmtmenu 
                    
label ss_d8_setup:
            stop ss_amb fadeout 1
            stop ss_amb2 fadeout 1 
            $ sam_dive = True   
            jump ss_d8start 
            
label ss_d9_setup:
            stop ss_amb fadeout 1
            stop ss_amb2 fadeout 1 
            jump ss_d9_start             

label ss_d10_setup_fox:
            stop ss_amb fadeout 1
            stop ss_amb2 fadeout 1 
            $ sp_dv = 12
            $ ss_friend = True
            $ sl_ft = True          
            jump ss_d10start

label ss_d10_setup_ala:
            stop ss_amb fadeout 1
            stop ss_amb2 fadeout 1          
            jump ss_d10start            
                    
label ss_setup_dvroute:
            stop ss_amb fadeout 1
            stop ss_amb2 fadeout 1                     
            $ sp_dv = 10    
            $ sl_ft = False 
            $ ss_friend = True             
            jump ss_dv11morning
            
label ss_setup_ssroute:
            stop ss_amb fadeout 1
            stop ss_amb2 fadeout 1            
            scene black with fade2
            jump ss_main_year2     


label ssfears_setup:
    stop ss_amb fadeout 1
    stop ss_amb2 fadeout 1 
    jump ssfears
label ss_dlc_setup:
    stop ss_amb fadeout 1
    stop ss_amb2 fadeout 1 
    jump ss_d6 
    
label airport_notif: 
    play ss_amb2 airport_16        
    $ rand = renpy.random.choice(["1","2","3"])
    if rand == "1":
        $ rand = renpy.random.choice(["1","2","3","4","5"])
        if rand == "1":  
            queue ss_amb2 airport_notif1
        if rand == "2":  
            queue ss_amb2 airport_notif2
        if rand == "3":  
            queue ss_amb2 airport_notif3
        if rand == "4":  
            queue ss_amb2 airport_notif4
        if rand == "5":
            queue ss_amb2 airport_notif5         
    else:
        pass 
    queue ss_amb2 airport_16  

    $ rand = renpy.random.choice(["1","2","3"])
    if rand == "1":
        pass            
    else:
        $ rand = renpy.random.choice(["1","2","3","4","5"])
        if rand == "1":  
            queue ss_amb2 airport_notif1
        if rand == "2":  
            queue ss_amb2 airport_notif2
        if rand == "3":  
            queue ss_amb2 airport_notif3
        if rand == "4":  
            queue ss_amb2 airport_notif4
        if rand == "5":
            queue ss_amb2 airport_notif5 
    queue ss_amb2 airport_16 
    
    $ count_20 = 20
    jump ss_count_20

label ss_count_20:
    $ count_20 = (count_20) - (1)
    if count_20 == 0:
        jump sam_back
        
    $ rand = renpy.random.choice(["1","2","3"])
    if rand == "1":
        $ rand = renpy.random.choice(["1","2","3","4","5"])
        if rand == "1":  
            queue ss_amb2 airport_notif1
        if rand == "2":  
            queue ss_amb2 airport_notif2
        if rand == "3":  
            queue ss_amb2 airport_notif3
        if rand == "4":  
            queue ss_amb2 airport_notif4
        if rand == "5":
            queue ss_amb2 airport_notif5         
    else:
        pass
    queue ss_amb2 airport_16 
    jump ss_count_20

    
label ss_menu_eng:    
    $ ss = Character(u'Samantha', color="#c8ffc8", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000001", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")

    $ stl = Character(u'Tolik', color="#83AF1C", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ alf = Character(u'Anchorman', color="#FF8F49", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ dim = Character(u'Dimitry', color="#60A5FF", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ ros = Character(u'Radoslav', color="#FF82BE", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ sg = Character(u'Special guest', color="#83AF1C", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ dev = Character(u'Little devil', color="#FF0000", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ ssunl = Character(u'Alyona', color="#FF7CFC", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")     
    $ drr = Character(u'Driver', color="#DAFF7F", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ grch = Character(u'Caretaker', color="#DAFF7F", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ sct = Character(u'Creepy girl', color="#60A5FF", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ evb = Character(u'Governess', color="#70D3BC", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ fnr = Character(u'Lantern', color="#60A5FF", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")                             
    $ svs = Character(u'Inner sexist', color="#60A5FF", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000") 
    $ brgu = Character(u'The Beard', color="#F75E00", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ brg = Character(u'Foreman', color="#F75E00", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ gust = Character(u'Gustav', color="#C7776B", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")            
    $ owlun = Character(u'Stranger', color="#E3E567", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")  
    $ sowl = Character(u'Sova', color="#E3E567", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ chert = Character(u'Devil', color="#9A3724", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ robocop = Character(u'Robocop', color="#8EABF0", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ ss_angel = Character(u'Angel', color="#FFFFFF", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ evw = Character(u'Inspector', color="#0FB1BC", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ evwun = Character(u'Woman', color="#0FB1BC", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ ss_voices = Character(u'Together', color="#F75E00", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ ssleo = Character(u'Leo', color="#FFB68C", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ sslyonka = Character(u'Leonya', color="#FFB68C", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ sspsy = Character(u'Sudden lunatic', color="#FF4235", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ ssdoc = Character(u'Doctor', color="#EAF2F7", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ sssan = Character(u'Paramedic', color="#EAF2F7", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")


    
    
    play sound solaricut    
    scene sam_menu_new     
    show nasty_letters_eng 
    
    $ renpy.pause (1.7)
    hide nasty_letters_eng    
    call screen ss_main_menu_eng
                
screen ss_main_menu_eng:
    imagemap:
            ground "samantha/image/menu/eng/sam_menu_start_eng.png"  
            hover "samantha/image/menu/eng/sam_menu_hover_eng.png"
            alpha True

            hotspot (300, 300, 484, 38) action Jump("nasty_road_to_ss_d1_eng") #Start() action Jump("goto_exit")           
            hotspot (295, 365, 484, 38) action Jump("ss_pick_a_day_eng")
            hotspot (295, 430, 484, 38) action Jump("ss_eng_options_sc1") 
            hotspot (295, 500, 484, 38) action Jump("temp_gal") 
            hotspot (295, 565, 484, 38) action Quit(confirm=True) 
    
label nasty_road_to_ss_d1_eng:
    play sound solariclick
    $ renpy.pause (0.1)   
    show nasty_letters_x_prep
    $ renpy.pause (0.2)    
    $ renpy.pause (0.7)
    play sound solarilong   
    show nasty_letters_x
    $ renpy.pause (4.0)    
    show no_sam with dissolve
    $ renpy.pause (0.5) 
    jump ss_d1_setup_eng

label ss_d1_setup_eng:
    stop ss_amb fadeout 1
    stop ss_amb2 fadeout 1 
    jump ss_d1_eng 

label ss_pick_a_day_eng:    
    show sam_menu_start_eng
    play sound solariclick
    $ renpy.pause (0.2)
    play sound solariclick
    hide sam_menu_start_eng    
    show screen1switch
    $ renpy.pause (0.1)   
    hide screen1switch
    $ renpy.pause (0.2)
    play sound solariclick 
    $ renpy.pause (0.2)
    play sound solariclick    
    show screen2switch  
    $ renpy.pause (0.1)
    hide screen2switch
    call screen ss_pick_day_eng 
    
screen ss_pick_day_eng:    
    imagemap:
            ground "samantha/image/menu/eng/screen1_eng.png"  
            hover "samantha/image/menu/eng/screen1_hover_eng.png"
            alpha True

            hotspot (300, 300, 540, 40) action Jump("ss_d1_eng_setup")      
            hotspot (300, 365, 540, 40) action Jump("ss_d2_eng_setup")
            hotspot (300, 500, 540, 40) action Jump("ss_d3_options_gg")
            hotspot (300, 565, 540, 40) action Jump("ss_d3_options_fail")
            hotspot (300, 630, 540, 40) action Jump("ss_d3_options_ali")
            hotspot (300, 700, 540, 40) action Jump("sam_back")
            hotspot (800, 365, 540, 40) action Jump("ss_d4_setup_civan_eng")
            hotspot (800, 430, 540, 40) action Jump("ss_d4_setup_cam_eng")
            hotspot (800, 500, 540, 40) action Jump("ss_d4_setup_god_eng")            
            hotspot (800, 700, 540, 40) action Jump("ss_d6_options_eng")            

label ss_d6_options_eng:
    play sound solariclick
    $ renpy.pause (0.2)
    play sound solariclick        
    show screen1sw
    $ renpy.pause (0.1)   
    hide screen1sw
    show screen6sw    
    $ renpy.pause (0.1)
    hide screen6sw
    call screen ss_pick_after_day6_eng
screen ss_pick_after_day6_eng:
    imagemap:
            ground "samantha/image/menu/eng/screen2_eng.png"  
            hover "samantha/image/menu/eng/screen2_hover_eng.png"
            alpha True   
            hotspot (300, 430, 540, 40) action Jump("ss_d6_setup_ro_eng")
            hotspot (300, 500, 540, 40) action Jump("ss_d6_setup_mc_eng")
            hotspot (300, 565, 540, 40) action Jump("ss_d6_setup_gc_eng")
            hotspot (300, 700, 540, 40) action Jump("sam_back_to_pick_eng") 
           
label sam_back_to_pick_eng:
    play sound solariclick
    $ renpy.pause (0.2)
    show screen1sw    
    $ renpy.pause (0.1)
    hide screen1sw
    call screen ss_pick_day_eng            
            
            
label ss_d1_eng_setup:
    stop ss_amb fadeout 1
    stop ss_amb2 fadeout 1 
    jump ss_d1_eng
    
label ss_d2_eng_setup:
            stop ss_amb fadeout 1
            stop ss_amb2 fadeout 1 
            $ sp_dv = (sp_dv) + (1)
            $ got_note = True
            $ got_kicked = True
            $ sp_sp = (sp_sp) - (1)
            $ bush_shirt = True
            jump ss_d2_eng 
            
label ss_d3_options_gg:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    $ sp_dv = (sp_dv) + (2)
                    $ sp_sp = (sp_sp) - (1)
                    $ sp_ss = (sp_ss) + (3)
                    $ sp_sl = (sp_sl) + (1)
                    $ sp_un = (sp_un) + (2)
                    $ got_note = True
                    $ got_kicked = True
                    $ bush_shirt = True
                    $ got_candies = True
                    $ gave_guitar = True
                    $ dv_thi = True
                    $ rom_eo = True
                    jump ss_d3_eng 
label ss_d3_options_fail:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1
                    $ sp_dv = (sp_dv) + (1)
                    $ got_note = True
                    $ got_kicked = True
                    $ sp_sp = (sp_sp) - (2)
                    $ bush_shirt = True
                    $ sp_us = (sp_us) - (1)
                    $ sp_sl = (sp_sl) + (1)
                    $ sp_dv = (sp_dv) - (1)
                    $ mer_cu = True
                    $ d2_cyb = True
                    $ sp_sh = (sp_sh) - (1)
                    $ sp_ss = (sp_ss) - (1)
                    jump ss_d3_eng 
label ss_d3_options_ali:  
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1        
                    $ sp_dv = (sp_dv) + (3)
                    $ sp_ss = (sp_ss) + (3)
                    $ sp_sp = (sp_sp) - (1)
                    $ got_note = True
                    $ got_kicked = True
                    $ kiss_dv = True
                    $ bush_shirt = True
                    $ got_candies = True
                    $ gave_guitar = True
                    $ dv_thi = True
                    $ rom_eo = True
                    jump ss_d3_eng 
                    
label ss_d4_setup_civan_eng:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    play sound CIvan
                    $ d3_usfail = True
                    $ sl_ft = True 
                    $ sp_un = 6
                    $ sp_ss = 5
                    $ sp_dv = 4
                    $ sp_sl = 6
                    $ un_inv = True 
                    $ mer_cu == True
                    $ sp_spts = 1
                    jump ss_d4start_eng         
label ss_d4_setup_cam_eng:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    play sound DGirl
                    $ sp_un = 4
                    $ sp_ss = 7
                    $ sp_dv = 6
                    $ kiss_dv = True
                    $ sp_sl = 5
                    $ rom_eo = True
                    $ un_inv = True 
                    $ sp_spts = 3
                    jump ss_d4start_eng
label ss_d4_setup_god_eng:
                    stop ss_amb fadeout 1
                    stop ss_amb2 fadeout 1 
                    play sound Codebreaker
                    $ sp_un = 7
                    $ sp_ss = 7
                    $ sp_dv = 6
                    $ sp_sl = 4
                    $ un_inv = True 
                    $ rom_eo = True
                    $ sp_spts = 2
                    $ kiss_dv = True
                    $ ss_godmode = True
                    jump ss_d4start_eng

 
                    
label ss_eng_options_sc1:
    show sam_menu_start_eng
    play sound solariclick
    $ renpy.pause (0.2)
    play sound solariclick 
    hide sam_menu_start_eng
    show screen1switch
    $ renpy.pause (0.1)   
    hide screen1switch
    $ renpy.pause (0.5)
    play sound leaf
    $ renpy.pause (0.4)       
    show cg ss_cliff with dspr
    call screen ss_options_eng
    
screen ss_options_eng:
    imagemap:
            ground "samantha/image/menu/options/1sc_eng.png"  
            hover "samantha/image/menu/options/1sc_hover_eng.png"
            alpha True

            hotspot (686, 284, 583, 95) action Jump("ss_change_color_eng")          
            hotspot (686, 440, 583, 95) action Jump("ss_widget_toggle_eng")
            hotspot (686, 592, 583, 95) action Jump("sam")    
                 
                    
label ss_options_new:
    show sam_menu_start
    play sound solariclick
    $ renpy.pause (0.2)
    play sound solariclick
    hide sam_menu_start    
    show screen0sw
    $ renpy.pause (0.1)   
    hide screen0sw
    $ renpy.pause (0.5)
    play sound leaf
    $ renpy.pause (0.4)    
label ss_options_new2:
    hide ss_eng_lang
    hide ss_ru_lang    
    show cg ss_cliff with dspr
    call screen ss_options

screen ss_options:
    imagemap:
            ground "samantha/image/menu/options/1sc.png"  
            hover "samantha/image/menu/options/1sc_hover.png"
            alpha True

            hotspot (710, 123, 1042, 209) action Jump("ss_change_dlang")          
            hotspot (710, 334, 1015, 167) action Jump("ss_menu_color")
            hotspot (710, 501, 1016, 282) action Jump("ss_widget_toggle")
            hotspot (710, 823, 541, 93) action Jump("sam")

label ss_widget_toggle:
    if persistent.ss_widget == "on":
        $ persistent.ss_widget = "off"                   
        python:
            while editoverlay__samantha in config.overlay_functions:
                config.overlay_functions.remove(editoverlay__samantha)
    else:            
        $ persistent.ss_widget = "on"               
        python:
            while editoverlay__samantha in config.overlay_functions:
                config.overlay_functions.remove(editoverlay__samantha)
            config.overlay_functions.append(editoverlay__samantha)  
   
    jump ss_options_new2
    
label ss_widget_toggle_eng:
    if persistent.ss_widget == "on":
        $ persistent.ss_widget = "off"                   
        python:
            while editoverlay__samantha in config.overlay_functions:
                config.overlay_functions.remove(editoverlay__samantha)
    else:            
        $ persistent.ss_widget = "on"               
        python:
            while editoverlay__samantha in config.overlay_functions:
                config.overlay_functions.remove(editoverlay__samantha)
            config.overlay_functions.append(editoverlay__samantha)
            
    call screen ss_options_eng

define ss_lang_colors = (
    "CED1C2", # Grey
    "ACDC13", # Green
    "B3A5FF", # Purple
    "8EC8FF", # Blue
    "FFBAD9", # Pink
    "FFF5C6", # White
    "FFDF89", # Default
    "FFFFFF", # TITANIUM WHITE
    "3968AC", # PRUSSIAN BLUE
    "246B43", # PHTALO GREEN
    "5B4224", # VAN DYKE BROWN
    "6C8822", # SAP GREEN
    "484850", # MIDNIGHT BLACK
    "DFB721", # INDIAN YELLOW
    "9A3724", # ALIZARIN CRIMSON
)

init python:
    def ss_show_color_menu(items, back):
        menus = [ ]
        cur_menu = [ ]
        index = 0
        platform = 0x2 if renpy.mobile else 0x1
        for mask, caption in items:
            if mask == 0x0:
                if index < len(ss_lang_colors):
                    caption = "{color=#%s}%s{/color}" % (ss_lang_colors[index], caption)
                cur_menu.append((caption, index))
                index += 1
            elif mask & platform:
                cur_menu.append((caption, -2 if mask & 0x4 else -1))
                cur_menu.append((back, -3))
                menus.append(cur_menu)
                cur_menu = [ ]

        cur_menu.append((back, -3))
        menus.append(cur_menu)

        result = None
        changed_music = False
        for m in menus:
            choice = renpy.display_menu(m)
            if choice >= 0:
                if choice < len(ss_lang_colors):
                    result = ss_lang_colors[choice]
                else:
                    result = renpy.random.choice(ss_lang_colors)
                break
            if choice == -3: # Back.
                break
            if choice == -2:
                changed_music = True
                renpy.music.stop("ss_amb", fadeout=1)
                renpy.music.stop("ss_amb2", fadeout=1)
                renpy.music.play(joy_of_painting)

        if changed_music:
            renpy.music.stop(fadeout=5)
            renpy.music.play(airport_ambience, channel="ss_amb", fadein=5)

        return result

label ss_menu_color:    
    if persistent.ss_text_lang == "en":
        call screen ss_change_to_tr

    window show
    "Диалоги, проходящие на английском, сейчас выделены {ru}этим{/ru} цветом."
    "Выберите желаемый цвет."
    window hide
    python hide:
        color = ss_show_color_menu((
            (0, "Grey"),
            (0, "Green"),
            (0, "Purple"),
            (2, "Другие цвета"), # For mobile.
            (0, "Blue"),
            (0, "Pink"),
            (0, "White"),
            (0, "Default"),
            (7, "Нужно больше цветов"), # For both desktop and mobile, with music.
            (0, "TITANIUM WHITE"),
            (0, "PRUSSIAN BLUE"),
            (0, "PHTALO GREEN"),
            (0, "VAN DYKE BROWN"),
            (2, "Ещё больше!"), # For mobile.
            (0, "SAP GREEN"),
            (0, "MIDNIGHT BLACK"),
            (0, "INDIAN YELLOW"),
            (0, "ALIZARIN CRIMSON"),
            (0, "Удивите меня"),
        ), "Назад")
        if color is not None:
            persistent.ss_text_style_ru = "color=#" + color

    jump ss_options_new2

label ss_change_color_eng:
    window show
    "{enen}This{/enen} is the current color for the discussions with Samantha (“real” English)."
    "Select the color you want."
    window hide
    python hide:
        color = ss_show_color_menu((
            (0, "Grey"),
            (0, "Green"),
            (0, "Purple"),
            (2, "Other colors"), # For mobile.
            (0, "Blue"),
            (0, "Pink"),
            (0, "White"),
            (0, "Default"),
            (7, "Need more colors"), # For both desktop and mobile, with music.
            (0, "TITANIUM WHITE"),
            (0, "PRUSSIAN BLUE"),
            (0, "PHTALO GREEN"),
            (0, "VAN DYKE BROWN"),
            (2, "More!"), # For mobile.
            (0, "SAP GREEN"),
            (0, "MIDNIGHT BLACK"),
            (0, "INDIAN YELLOW"),
            (0, "ALIZARIN CRIMSON"),
            (0, "Surprise me"),
        ), "Return")
        if color is not None:
            persistent.ss_text_style_enen = "color=#" + color

##    jump ss_options_eng
    call screen ss_options_eng

screen ss_change_to_tr:
    imagemap:
#            ground "patch/image/menu/options/2sc.png"  
#            hover "patch/image/menu/options/2sc_hover.png"
            ground "samantha/image/menu/options/2sc.png"  
            hover "samantha/image/menu/options/2sc_hover.png"
            alpha True
            
            hotspot (780, 465, 390, 130) action Jump("ss_ru_and_restart")
            hotspot (780, 624, 390, 130) action Jump("ss_options_new2")   
            
label ss_ru_and_restart:
    $ persistent.ss_text_lang = "ru" 
    jump ss_menu_color

label ss_change_dlang:
    if persistent.ss_text_lang == "ru":
        show ss_ru_lang
    else:
        show ss_eng_lang
        
    call screen ss_change_lang2
    
screen ss_change_lang2:
    imagemap:
#            ground "patch/image/menu/options/3sc.png"  
#            hover "patch/image/menu/options/3sc_hover.png"
            ground "samantha/image/menu/options/3sc.png"  
            hover "samantha/image/menu/options/3sc_hover.png"
            alpha True
            
            hotspot (780, 465, 390, 130) action Jump("ss_something_and_restart")
            hotspot (780, 624, 390, 130) action Jump("ss_options_new2")

label ss_something_and_restart:
    if persistent.ss_text_lang == "ru":
        $ persistent.ss_text_lang = "en" 
    else:
        $ persistent.ss_text_lang = "ru"    
    jump ss_options_new2
