label ss_d6edit:
    $ day_time()
    $ persistent.sprite_time = "day"
    play ambience ambience_int_cabin_day
    show unblink
    scene bg int_house_of_mt_day
    $ renpy.pause (0.5)
    show mt normal pioneer
    with dissolve
    $ renpy.pause (1.0)
    window show
    mt "Подъём уже! Возрадуйся, засоня! Пришёл сей мною долгожданный день."
    me "Это про пьесу снова вы поёте? Увольте, раз в который слушать лень."
    play music music_list["everyday_theme"] fadein 7
    mt "Про пьесу, обязательно про пьесу! Теперь, когда готово всё совсем..."
    me "И не готово ничего, меж тем."
    mt "Твоё нытьё уж ничего не весит."
    mt "На подготовку был потрачен месяц!"
    me "Да как же месяц? Только три недели."
    me "И те совсем внезапно пролетели."
    me "Ещё неделька нам пошла бы впрок."
    mt "Да разве три недели - малый срок?"
    me "Мал так, как шанс увидеть вас довольной,"
    me "Оставившей в покое остальных,"
    me "Чтоб насладились все житьём привольным."
    th "Тогда часов по десять бы я дрых."
    show mt surprise pioneer
    mt "Что главный лодырь ставит мне в укор?"
    mt "Воистину, нет праздности предела!"
    show mt normal pioneer
    mt "И молодости чужд тебе задор -"
    mt "Как старец. Избежишь любого дела."
    me "Но роль свою я выучил сполна!"
    mt "Тогда - проблемы? Назови одну."
    me "Костюмы не готовы - вот одна!"
    mt "Тащите ткань! Сошью и подгоню..."
    me "Портные есть - работать не желают,"
    me "А матерьял без дела пропадает."
    me "Я день и ночь учил треклятый текст,"
    me "Пока портные нежились на пляже."
    me "И где высокий ваш на то протест?"
    me "Бездельники не начинали даже."
    mt "Но часть костюмов есть и в эту пору?"
    me "Тряпьё! Я лучше замотаюсь в штору."
    mt "Так замотайся, прояви смекалку."
    show mt smile pioneer
    me "Пусть так. Хоть будет спрятать где шпаргалку."
    mt "Зачем? Недоучил? Соврал, юнец?"
    me "Начало подзабыл, зубря конец."
    $ renpy.pause(0.5)
    mt "Я ухожу. Не пропусти линейку."

    window hide
    hide mt with dspr
    $ renpy.pause(1.0)
    play sound sfx_open_door_1
    window show
    th "Взгляну-ка, как там наши лицедейки."
    window hide
    scene bg ext_square_day
    show dv angry pioneer at cright
    show sl angry pioneer at left
    with dissolve
    play ambience ambience_day_countryside_ambience fadein 3
    window show
    th "Что тут у нас? Алиса. Славя. Ссора."
    me "Любезные, в чем сущность разговора?"
    window hide
    if dv_thi == True:

        show dv guilty pioneer at cright with dspr
        window show
        sl "Алиса не участвовать решила."
        me "Да что ты, уговор же был. Забыла?"
        dv "Простите уж. К театру не готова."
        dv "Ошибка всё, и не взялась я б снова."
        sl "Все репетиции гуляла до одной."
        sl "Чего ждать от разбойницы такой?"
        sl "(Я б удивилась, честно говоря,"
        sl "Кабы она не маялась зазря,"
        sl "А первый раз своё сдержала слово.)"
        window hide
        show dv angry pioneer at cright with dspr
        window show
        dv "Несправедливо судишь - и сурово."
        me "Постой, Алиса, ты нам соврала?"
        me "Тогда весь лагерь этим подвела."
        me "Зачем же так? Тебе на всех плевать?"
        me "Могла хотя бы раньше ты сказать!"
        window hide
        show dv guilty pioneer at cright with dspr
        window show
        dv "День каждый роль я выучить хотела,"
        dv "Да, будучи в заботах, не успела:"
        dv "На той неделе занята весьма,"
        dv "На этой часто голова болела..."
        sl "Вот у того проблемы с головой,"
        sl "Кого в столовой ты вчера боднула."
        sl "Насколько девкой надо быть дурной,"
        sl "Чтоб драться из-за занятого стула!"
        window hide
        show dv normal pioneer at cright with dspr
        window show
        dv "Твои укоры - просто болтовня,"
        dv "Не нравлюсь - так играйте без меня."
        dv "Я ухожу, торчать тут смысла ноль."
        me "Нет уж, постой. А кто сыграет роль?"
        dv "Сказала же, не знаю ни бельмеса."
        me "Осталось лишь одно. Отменим пьесу."
        me "Дублёра нет, и слов никто не знает."
        sl "Всё проще: по листку пусть роль читает."
        window hide
        show sl serious pioneer at left
        show dv guilty pioneer at cright
        with dspr
        window show

    else:
        window hide
        show dv guilty pioneer at cright with dspr
        window show
        dv "Я тоже, кстати, не могу взять в толк,"
        dv "Какой с меня дриаде этой прок."
        sl "Дриаде? ...Тут, Семён, у нас проблема."
        sl "Тот парень, что играл Тибальта, Тима..."
        sl "Ему теперь не светит эта тема:"
        sl "Простыть он умудрился - и не в зиму."
        dv "И виновата, как всегда, Алиса."
        me "Спокойней же, поищем компромиссы..."
        sl "Ты ж облила колодезной водой?!"
        sl "Бедняга весь продрог, идя домой."
        window hide
        show dv normal pioneer at cright with dspr
        window show
        dv "Нелепо же. Такой лось лишь обрызган -"
        dv "И только что не инвалидом признан."
        sl "Алиса, не юли, ты виновата."
        sl "Вот почему не в пьесе ты сама-то?"
        window hide
        show sl serious pioneer at left with dspr
        window show
        sl "Твоя вина. Тибальтом будешь ты,"
        sl "Авось и совладаешь с ролью малой..."
        show dv grin pioneer at cright with dspr
        dv "Да, госпожа! То все твои мечты?"
        dv "Не хочешь ли массаж спины усталой?"
        me "Массаж? Тибальт - Алиса? Вот чудесно!"
        me "Понаблюдать мне будет интересно."
        me "Поддерживаю все инициативы."
        window hide
        show dv angry pioneer at cright with dspr
        window show
        dv "Ну нет, мой сладострастный театрал!"
        dv "Тебе массаж не светит особливо!"
        dv "К тому же и не больно ты устал."
        sl "Не просьба это. Выбор небогатый."
        dv "Заставишь силой? Пригрозишь вожатой?"
        #dv "Лошадкою любить себя изволь,"
        #dv "Театр мне этот сдался не настоль..."
        me "Участвовать желаешь не особо..."
        me "Но разве занимать тебе таланта?"
        me "Иль сцены у себя нашла боязнь ты?"
        me "Так намекала мне... одна особа."
        window hide
        show dv guilty pioneer at cright with dspr
        window show
        dv "Как низко. На \"слабо\" желаешь взять?"
        me "Попробуй предложение принять."
        dv "Мне не успеть освоить роль к премьере..."
        me "Шпаргалками снабдим, по крайней мере."
        sl "И даже больше сделаем поблажку:"
        sl "Весь текст пускай читает по бумажке."
    stop music fadeout 7
    dv "А прочие по памяти? То стыдно!"
    sl "Алисе стыд не чужд? Вот любопытно."
    dv "Слова свои внеси в мою анкету."
    me "Ничуть не стыдно! Вариантов нету."
    me "Давай, Алиса, надо! Выручай!"
    window hide
    show dv grin pioneer at cright with dspr
    window show
    dv "А вы мне что? А знаешь, вот давай..."
    play music music_list["always_ready"] fadein 3
    dv "Семён, залазь на Генду. Голышом."
    window hide
    show sl surprise pioneer at cleft with dspr
    window show
    me "Ты шутишь или тронулась умом?"
    dv "У Генды утвердись-ка над плечами!"
    dv "Не можешь? Ну тогда крутитесь сами."

    show sl angry pioneer at cleft

    sl "Бесстыжая девчонка. Стыд и срам!"
    sl "Сорвёшь всё! Где Алиса, там бедлам."
    dv "Трусы оставить можешь. Генда ждёт."
    dv "Народ к линейке скоро уж пойдёт."
    me "Кощунство - лезть на Генду в неглиже!"
    me "И альпинизм мне, кстати, тоже чужд."
    dv "Одетым лезь, Семён, но лезь уже!"
    dv "Хоть подвиг совершишь для общих нужд."
    th "А ну как с монумента я сорвусь?"
    th "Об травку, впрочем, вряд ли расшибусь."
    window hide
    scene bg ext_square_day_zoom
    show dv grin pioneer at fright
    with dissolve
    $ renpy.pause(1.0)
    window show
    dv "Полегче там, Семён! Порвёшь рукав!"
    "Попробовал залезть, всю грязь собрав,"
    "Но выше ног не лезлось ни в какую."
    th "Чего же ради шеей я рискую?"
    window hide
    show sl scared pioneer
    with dspr
    window show
    sl "То ж памятник, вандалы! Что творите?!"
    me "Я не могу, он... скользкий. Подсобите?"
    dv "Славяна, душка, подсади его!"
    sl "Позор какой! Святого ничего!"
    me "Ты против... Но мне одному никак..."
    me "Ругай меня поближе!"
    window hide
    show sl scared pioneer far
    with dspr
    window show
    me "...Да, вот так."
    "Вскарабкаюсь на Славю...{w} и наверх!"
    "Чуть подтянусь ещё - и вот успех!"
    window hide
    $ renpy.pause (0.5)
    scene bg ext_square_day_alpinist
    with dissolve

    $ persistent.ssg_109 = True

    $ renpy.pause (0.9)
    window show
    sl "Семён, ну что ты делаешь? Увидят!"
    dv "Тебя уже втихую ненавидят."
    sl "Ну так тебя - открыто. Есть за что."
    me "Эй! А видок отсюда на все сто!"
    dv "Легко парням лезть на тебя, однако."
    sl "Зато тебе такое не грозит."
    sl "В тюрьме для женщин парни - дефицит."
    dv "За что же мне в тюрьму? Полезешь в драку?"
    me "Полегче там! Не вздумайте сцепиться!"
    me "Готов упасть на вас я, аки птица."
    dv "Шанс на тюрьму свой оценила верно,"
    dv "Но нам решать не нужно дело кровью:"
    dv "Такой, как ты, балде высокомерной,"
    dv "Я всыплю вовсе без вреда здоровью."
    me "Полундра же, вожатая идёт!"
    th "За Гендой спрячусь - может, пронесёт."
    th "Хоть чувствую, недолго провишу."
    th "Гоните же её скорей, прошу!"
    window hide
    scene bg ext_square_day_alpinist2 with dissolve

    $ persistent.ssg_109 = True

    $ renpy.pause (1)
    window show
    mt "Девчата, что вы красные такие?"
    mt "Поссорились с утра? Вот вам укор,"
    mt "Сам Генда видит ваши лица злые!"
    sl "У нас... сейчас случился жаркий спор."
    mt "О чём же? Расскажите-ка мне, дамы."
    dv "О ступоре космической программы"
    dv "И прочих мне досадных неудачах."
    dv "Полёт на Марс - достойная задача."
    dv "Герой нам нужен смелый, как Гагарин,"
    dv "Коль первого сбил инопланетянин."
    dv "Да где такого взять, кто не боится"
    dv "Опасности ко всем чертям разбиться?"
    me "Ай!" with vpunch
    "Я больше уж не мог висеть за Гендой,"
    "И был отпущен мой могучий друг."
    window hide
    scene bg ext_square_day_zoom with dissolve
    window show
    "Лечу. Гремит Алисин смех победный."
    "И крик вожатой. (Ту постиг испуг.)"
    window hide
    play sound sfx_body_bump
    scene bg ext_square_day_zoom:
        xalign 1.0
        ypos 0.0
        ease 0.1ypos 0.025
        ease 0.1ypos -0.025
        ease 0.1ypos 0.0125
        ease 0.1ypos -0.0125
        ease 0.1ypos 0.00625
        ease 0.1ypos -0.00625
        ease 0.1ypos 0.003125
        ease 0.1ypos -0.003125
    show sl scared pioneer
    show dv laugh pioneer at fright
    show mt shocked pioneer  at fleft
    with dspr
    $ renpy.pause (0.7)
    window show
    stop music fadeout 5
    stop ambience fadeout 3
    dv "Возрадуйтесь! Герой явлен народу!"
    # Н.: Слова "явлён" в русском языке нет. Современное произношение — "я́влен".
    # Есть ещё "явле́н" — краткая форма церковнославянского причастия "явле́нный".
    # У меня имеются сомнения, уместно ли оно здесь, но ничего лучше не придумывается.
    window hide
    $ renpy.pause (1.5)
    scene bg ext_aidpost_day 
    with fade
    play music music_list["eternal_longing"] fadein 5
    window show

    "Хотя прошло падение удачно,"
    "Покорный ваш слуга идёт в медпункт."
    "Со мною вместе Славя, смотрит мрачно,"
    "Да уж, бедняжка вовсе не батут."
    "Хоть выполнить смогла его работу."
    "Теперь я должен проявить заботу."
    play ambience ambience_medstation_inside_day fadein 3    
    scene bg int_aidpost_day    
    show cs normal stethoscope at cright
    show sl angry pioneer at left
    with dissolve   

    "Виола поджидала в лазарете."
    cs "Какой же вид! Но что случилось, дети?"
    th "Опять похабный юмор пустит в дело."
    th "Я мог бы это упредить умело,"
    th "Сказать: у нас был садомазохизм,"
    th "В экстазе повреждён сей организм."
    show sl sad pioneer at left
    sl "Упала."
    cs "Вижу, мигом всё подлечим."
    cs "Тут шишка будет, это не навечно."
    cs "О, юноша, ты тоже пострадал?"
    me "...Моральную поддержку оказал."
    me "А сам - здоров как бык."
    cs "Тогда свободен."
    th "Так просто выпустит из цепких лап?"
    th "Ведь не из тех, кто сразу скажет: \"Годен!\""
    th "Да разве ж так поступит эскулап?"

    th "Пусть так. Не то чтоб я разочарован."
    sl "Его бы осмотреть, упал он тоже."
    th "...Разрушен план неосторожным словом."
    cs "Падений эпидемия, похоже!"
    cs "Откуда падаем - должна узнать тем паче."
    cs "Конечно же, с кроватей, не иначе?"
    window hide
    show sl shy pioneer at left
    show cs smile stethoscope at cright
    with dspr
    window show
    me "Всё было так: залез я на Славяну,"
    me "Алиса нас науськивала рьяно,"
    me "И тут подходит Ольга, говорит..."
    show sl surprise pioneer at left
    cs "Гусар, молчать! Скажи-ка, где болит?"
    me "Как же скажу? Приказ молчать гусару..."
    cs "Ох, выпросишь ты клизму! Или пару!"
    cs "Умолк? Теперь показывай раненья,"
    cs "Лишь в тишине пойдёт твоё леченье."
    stop ambience fadeout 2
    th "Вот то-то же, Виола. Так бы сразу."
    th "Не правда ль, хорошо унял заразу?"
    stop music fadeout 5
    window hide
    $ renpy.pause (1)
    play ambience ambience_camp_center_day fadein 3
    scene bg ext_square_day
    show sl normal pioneer
    with dissolve

    window show
    "От медсестры спешили на линейку."
    me "Я убеждён, Алиса сдержит слово."
    play music music_list["so_good_to_be_careless"] fadein 5
    sl "С чего же веришь этой ты злодейке?"
    me "А у тебя Тибальт найдётся новый?"
    if mer_cu == True:
        sl "Уговорил. Хотя есть риск отчасти."
        sl "...Ты молодец, избавил от напасти -"
        sl "Я про согласие играть Ромео."
        sl "Роль велика, освоить не успела б."
        th "Ромео? Разве я его играю?"
        th "Ах да. Всё временами забываю."
        sl "Я знаю, эта роль тебя смущает."
        me "Не спорю, там неловкостей хватает."
        me "Ну, ты же помнишь, как оно всё было,"
        me "Когда змея Саманту укусила."
        me "Виола ей режим ввела постельный,"
        me "На репетиции - запрет недельный."
        me "И раз уж всё равно мне с ней сидеть,"
        me "То что бы ролью и не овладеть?"
        me "Вдвоём мы репетировали долго..."

        me "Теперь - готов на роль любого толка!"
        sl "Но с ней сидеть тебя не заставляли."
        sl "Других вы репетировать не звали."
        me "Всё верно. Это ты сейчас к чему?"
        sl "Ты говорил - привычно одному."
        sl "Теперь вдвоём неплохо... Я пойму."
        th "Она ревнует?! Но кого к кому?"
        me "Тебе спасибо, Славя. Эта пьеса -"
        me "Твоя затея, просто на минутку."
        sl "Я отойду. Ты не сердись, повеса,"
        sl "Я это просто так сказала, в шутку."
        th "Та шутка не впервой уже звучала."


    else:
        sl "Нет. Но запомни: я не одобряла."

    window hide
    hide sl with dspr
    window show
    "В толпе искал Алису. Та сбежала."
    window hide
    show ss smile casual with dspr
    window show
    ss "{en}Good morning, Sam.{/en}{ru}Эй, с добрым утром, Сэм!{/ru}"
    show ss smile2 casual
    me "{en}Good morning to you too!{/en}{ru}Тебе того же.{/ru}"
    me "{en}I'm trying to catch Alice.{/en}{ru}Видала ль ты Алису нынче утром?{/ru}"
    ss "{en}She is here.{/en}{ru}Она стояла где-то там, как будто.{/ru}"
    show ss unsure casual
    ss "{en}Ulyana with her, such a menace.{/en}{ru}Ульяна подошла к ней чуть попозже.{/ru}"
    th "Неужто всё боится егозу?"
    me "{en}Many thanks. Then I'll be going.{/en}{ru}Благодарю. Визит им нанесу.{/ru}"
    window hide
    hide ss with dspr
    $ renpy.pause (1)
    show un normal pioneer far
    show mi smile pioneer far at right
    with dspr
    window show
    un "...Схожу, похоже, медленно с ума."
    mi "Ты? Медленно? Хм. Стало быть, давно!"
    window hide
    show un sad pioneer far
    with dspr
    window show
    mi "Шучу! А пусть и так - не всё ль равно?"
    mi "С безуминкой не будешь ты одна."
    window hide
    $ renpy.pause (1)
    hide mi
    hide un
    with dspr
    $ renpy.pause (0.5)
    show dv normal pioneer at cleft
    show us normal pioneer at cright
    with dspr
    window show
    me "Попалась же! Я сделал, что просила,"
    me "Так что изволь и роль теперь играть."
    dv "Что, если нет? Меня заставишь силой?"
    me "Дваче...вская! Хорош уже вилять!"
    dv "Расслабься ты, сказала же - сыграю."
    dv "Уж не впервой тебе я помогаю."
    me "Мне нужно то не больше твоего."
    me "Стараюсь... Сам не знаю для чего!"
    show us grin pioneer at cright
    us "Ради кого - вот так сказать верней."
    me "Ради кого же? Ради всех друзей?"
    us "Ты дурачка не строй нам, плут Семён!"
    show us laugh2 pioneer at cright
    us "Все знают, что ты кой в кого влюблён."
    me "...То не секрет: в тебе души не чаю."
    window hide
    show us surp3 pioneer at cright with dspr
    window show
    me "Так пьеса для тебя важна, родная?"
    window hide
    hide us with dspr
    window show
    me "Душа моя, куда же ты бежишь?"
    dv "Ну устыдись, зачем так говоришь?"
    me "Дурачусь. Что такого? Ей же можно..."
    dv "Шутить с девчонкой нужно осторожно."
    me "Ну неудачно вышло, но безвреден"
    me "Сей юмор... А скажи, её слова?"
    me "Иль чьи ещё?.."
    dv "Не собираю сплетен."
    dv "А для тебя, гляжу, важна молва?"
    me "Возможно. Но могла ль она соврать?"
    dv "Себя ты можешь этим утешать."
    mt "Отряд, постройся!"
    window hide
    scene cg d2_lineup
    with dissolve
    window show
    mt "Сегодня долго вас не задержу,"
    mt "Нас после завтрака ждёт много дел."
    mt "Крапива недокошена, гляжу,"
    mt "Чертополох вновь вылезти успел."
    mt "Мы это всё немедленно поправим."
    mt "А заодно щи из крапивы сварим."
    pi "Мы эту дрянь, мне помнится, едали."
    pi "Из нужника два дня не вылезали."
    mt "...Такая на сегодня установка."
    stop ambience fadeout 2
    stop music fadeout 5
    mt "А вечером - на сцене постановка."

    window hide
    $ renpy.pause(0.5)
    play ambience ambience_dining_hall_full fadein 5
    $ persistent.sprite_time = 'day'
    scene bg int_dining_hall_people_day
    with dissolve
    show ss smile2 casual at cright
    show mt normal pioneer at cleft

    with dissolve
    mt "Саманта, ты сегодня впрямь сияешь."
    mt "Жду не дождусь узреть, как ты играешь."
    show ss shy casual at cright with dspr
    ss "{en}Thank you. I am shaking hard myself.{/en}{ru}На спину дрожь готовится сойти.{/ru}"
    mt "Будь сильной. Знай: успех неотвратим."
    mt "А ты, Семён, не рад? Иль не готов?"
    me "Загвоздка есть. Для вас ответ не нов."
    show ss smile2 casual at cright with dspr
    mt "Что вновь не так? Молю, не про костюмы."
    mt "Чего молчишь? Чего глядишь угрюмо?"
    me "Стоят ребром вопросы режиссуры."
    th "Спасибо козням этой Жени-дуры."
    mt "Ну так уладь. В чём именно проблема?"
    me "Присутствует моральная дилемма."
    me "Герои наши с Сэм должны... сближаться."
    me "Нам эту б тему сгладить постараться."
    me "На то разнятся наши с Женей взгляды."
    me "Напомню, что на сцене ей не рады."
    window hide
    show ss serious casual at cright
    show mt smile pioneer at cleft

    with dspr
    window show
    mt "Ужели ты объятьями смущён?"
    mt "Нежданно у тебя проснулась совесть."
    me "Быть должен наперёд вопрос решён,"
    me "Чтоб защитить от срыва нашу повесть."
    mt "А ты, Саманта, что о том считаешь?"
    mt "Проблему если нашу понимаешь."
    window hide
    show ss shy casual at cright
    show mt normal pioneer at cleft

    with dspr
    window show
    ss "{en}It must be shameful for Semyon, my buddy,{/en}{ru}Смущать я не желаю никого.{/ru}"
    ss "{en}So let's not bother him or anybody...{/en}{ru}В особенности друга моего.{/ru}"
    "Мне Ольга подала секретный знак:"
    "\"Переводить не надо\". Не дурак."
    mt "Не знаю уж, чего вы там хотите,"
    mt "Саманте нет нужды в такой защите."
    show ss unsure casual at cright
    mt "По мне - так тем её не защищаешь."
    mt "А вероятно, только обижаешь."
    me "Не верю! Поцелуев вы хотите?"
    me "Как может быть такое? Объясните."
    mt "Не я. Ты должен их хотеть, смекаешь?"
    mt "А мы уж запретим, коль ты дерзаешь."
    me "Тогда пришёл черёд узнать у Сэмми,"
    me "Желает поцелуев ли на сцене?"
    mt "Нет..."
    me "Вы боитесь - может согласиться?"
    window hide
    show ss sad casual at cright
    show mt angry pioneer at cleft

    with dspr
    window show
    mt "Что говоришь? Ну ты подлец, Семён!"
    mt "Ни в чем мне на тебя не положиться,"
    mt "Раз ты изменчив, как хамелеон."
    show ss surprise casual at cright
    play music Nobodys_Secret fadein 5
    ss "{en}Don't push him, miss, I beg you.{/en}{ru}Вас если верно я смогла понять,{/ru}"
    ss "{en}We can discuss it later, just us two.{/en}{ru}Вдвоём нам это стоит обсуждать.{/ru}"
    show ss unsure casual at cright
    th "За совесть был я прозван подлецом."
    stop ambience fadeout 2
    th "Нет слов, лишь буквы. Вот и всё на том."
    window hide
    $ renpy.pause (0.5)
    $ persistent.sprite_time = 'day'
    scene bg ext_dining_hall_away_day
    with dissolve
    show ss shy casual
    with dissolve
    play ambience ambience_camp_entrance_day fadein 3
    $ renpy.pause (0.3)
    window show
    me "{en}It's awkward. Sorry if I was making a big deal.{/en}{ru}Неловко. Был то шум из ничего?{/ru}"
    me "{en}That's not important. Is this how you feel?{/en}{ru}Самой тебе не важно всё оно?{/ru}"
    ss "{en}I'm glad you care so much. In fact, I must confess,{/en}{ru}Я благодарна за твою заботу.{/ru}"
    ss "{en}I am concerned more than you. At least, not less.{/en}{ru}Вопрос меня волнует отчего-то.{/ru}"
    ss "{en}And hear my next confession... Oh, you might enjoy,{/en}{ru}И между нами, хочется признать:{/ru}"
    ss "{en}I've actually never kissed a boy. {/en}{ru}Парней мне не случалось целовать.{/ru}"
    me "{en}So haven’t I.{/en}{ru}О, их не целовал и я, поверь.{/ru}"
    show ss shy2 casual
    ss "{en}Don't laugh at me, not now.{/en}{ru}Не смейся надо мною, не теперь.{/ru}"
    show ss shy casual
    me "{en}It is decided then. No kisses in the play.{/en}{ru}Что ж, решено. Без поцелуев то бишь.{/ru}"
    show ss shy2 casual
    ss "{en}Why so? I'm ready if you may.{/en}{ru}Но почему? Готова, коль изволишь.{/ru}"
    show ss shy casual
    menu:
        "Тогда давай":
            $ sp_ss += 1
            $ d6_kissing = 1
            me "{en}Much honored with your permit,{/en}{ru}Согласье лестно моему уму.{/ru}"
            me "{en}And gladly I accept. So be it.{/en}{ru}Я с радостью. Решили - быть тому.{/ru}"
            show ss shy casual
            ss "{en}Good lord. I guess I need a partner for a practice...{/en}{ru}Мне нужен кто-нибудь для тренировки,{/ru}"
            ss "{en}Without experience it could be real disaster.{/en}{ru}Испорчу ж всё, играя без сноровки.{/ru}"
            me "{en}You're kidding now. You've got to be.{/en}{ru}Ты шутишь.{/ru}"
            window hide
            show ss smile casual with dspr
            window show
            ss "{en}Right... Pardon me. I'm nervous.{/en}{ru}От волненья то, не скрою.{/ru}"
##          me "{en}Don't be. I will be there with you.{/en}{ru}Для храбрости есть у Алисы зелье.{/ru}"
            show ss smile2 casual
            me "{en}Don't be. I will be there with you.{/en}{ru}Ты не одна в волненье, я с тобою.{/ru}"
            window hide

        "Не будем":
            me "{en}You are too young. I don't know what they're thinking{/en}{ru}Не нужно, Сэм. Ты слишком молода.{/ru}"
            me "{en}So it's decided. There will be no kissing.{/en}{ru}Додумались они как? Нет стыда!{/ru}"


            if sp_mizul == 3:
                $ d6_kissing = 1
                show ss sad casual with dspr
                ss "{en}You are so cold to me sometimes.{/en}{ru}Со мной так холоден ты временами.{/ru}"
                ss "{en}Youth is my sin, what else? Go, name my crimes.{/en}{ru}Чем провинилась? Юными годами?{/ru}"
                me "{en}You are a silly girl, who I am trying to protect.{/en}{ru}Пытаюсь защитить тебя, глупышка.{/ru}"
                ss "{en}But don't protect me from yourself.{/en}{ru}От самого себя? В том нет нужды.{/ru}"
##              ss "{en}Give me a kiss, don't be so cruel.{/en}{ru}Поцелуй ж меня, мой педомишка. {/ru}"
##              ну это уж совсем толсто, лол.
##              Присоединяюсь. Стыд то имейте. (бггг!11).
                show ss shy casual
                ss "{en}Give me a kiss, don't be so cruel.{/en}{ru}Целуй уже давай, не будь ледышкой.{/ru}"


                me "{en}Just for the practice, right? ...Uh, maybe later?{/en}{ru}Я... поцелую.{/ru}"
                show ss shy2 casual with dspr
                ss "{en}Now!{/en}{ru}Я хочу - сейчас!{/ru}"
                window hide
                $ renpy.pause (1)
                scene black with fade
                scene bg ext_dining_hall_away_day
                show ss shy casual
                with fade
            else:
                ss "{en}As you say. {/en}{ru}Как скажешь.{/ru}"
                ## вот такой вот первый раз

    $ renpy.pause (1)
    window show
    ss "{en}Shall we move now to the stage?{/en}{ru}Наверное, пора явиться к Жене?{/ru}"
    stop music fadeout 5
    me "{en}Let's go. Don't make them wait for ages.{/en}{ru}Тогда вперёд. Посмотрим, что на сцене.{/ru}"
    window hide
    $ renpy.pause (0.3)
    $ persistent.sprite_time = 'day'
    scene bg ss_ext_stage_big_day

    show sh normal pioneer at cleft
    show ss normal casual at cright
    with dissolve
    window show
    th "Так... Шурик? Он какими тут судьбами?"
    sh "Привет. Теперь и я играю с вами."
    me "Кого же, разреши-ка мне узнать?"

    sh "Представь, Бенволио, твой лучший друг."
    me "Роль стоило тебе другую взять."

    th "Парис, к примеру. Тот ещё... Индюк."
    sh "Других уже тогда не оставалось,"
    sh "Когда к театру тяга пробуждалась."
    sh "Ромео мог бы я играть иначе."
    sh "А ты и быть им не хотел, тем паче."

    me "Надеюсь, не сорвёшь спектакль, мой друг."

    show sh surprise pioneer at cleft

    sh "С чего бы? Непонятен твой испуг."
    window hide
    show sh normal pioneer at cleft
    show ss unsure casual at cright
    window show
    ss "{en}He's not a good friend, he is such a worm,{/en}{ru}Бенвольо - друг? Не назвала б его.{/ru}"
    ss "{en}He leaves Romeo at the tomb alone!{/en}{ru}Ромео в склепе бросил одного.{/ru}"
    me "Вот как. Тогда, пожалуй, роль твоя."
    sh "В том дозволенье не нуждался я."
    window hide
    $ renpy.pause (1)
    scene bg ext_stage_normal_day
    show mz angry glasses pioneer at cleft
    show dv angry pioneer at cright
    with dissolve

    window show
    play music music_list["gentle_predator"] fadein 3
    mz "...Тебя вообще сюда я не звала!"
    dv "А я и вовсе не к тебе пришла."
    me "Вновь ссоры? Ну и что опять такое?"
    mz "Себе поищет место пусть другое."
    me "Алиса с нами, хочешь или нет."
    mz "Но за неё тебе нести ответ!"
    window hide
    hide mz
    show dv smile pioneer at cright
    with dspr
    window show
    me "Алиса? Отчего она так злится?"
    dv "Как будто юбка всё на ней дымится..."
    me "С чего дымится? Это ты о чём?"
    dv "Хотела б если сжечь себя живьём,"
    dv "То получила от меня прививку."
    me "Злить тех, кто злее некуда, - ошибка."
    dv "Рассказывай, в чём суть моей тут роли."
    me "Ты до сих пор ещё не в курсе, что ли?"
    show dv smile pioneer at cright
    dv "Ну просвети меня! Иль сам не знаешь?"
    me "Меркуцио сначала убиваешь..."
    dv "А кто того Меркуцио играет?"
    me "Ну Славя же. Ромео не прощает..."
    dv "Я убиваю Славю? Во дела!"
    dv "Узнала б раньше - я бы здесь жила."
    me "В отместку же Тибальта убивает."
    me "Ромео. А Ромео буду я."
    dv "Что?! Заколоть меня? Ну ты свинья!"
    dv "Так не пойдёт, давай сюжет изменим."
    dv "Тибальт мой будет жить назло всем Женям."
    me "Не хочешь, чтобы Женя ликовала, -"
    me "Серьёзней будь, не допусти провала."
    show dv normal pioneer at cright
    dv "Провала? Без ножа меня пытаешь!"
    dv "С бумажкой не смогу играть, считаешь?"
    window hide
    $ renpy.pause (0.5)
    hide dv with dissolve
    $ renpy.pause (0.5)
    show sl normal pioneer at fleft
    show mi normal pioneer
    with dissolve
    window show
    sl "Все репетировать готовы? Начинай."
    mi "Входит хор."
    sl "Ты вместо хора. Это - пропускать."
    mi "Мой голос - многократно записать"
    mi "И наложить? Так сделать хор желаешь?"
    show sl serious pioneer at fleft
    sl "Без хора! Ты от автора читаешь."
    mi "Я поняла!"
    mi "{i}Две равноудалённые семьи...{/i}"
    sl "Не радуют познания твои."
    window hide
    show mi grin pioneer    with dspr
    window show
    stop music fadeout 5
    mi "Прости. Из геометрии вдруг всплыло."
    sl "Надеюсь, что ты всё же текст учила."
    mi "{i}Две равно одарённые семьи...{/i}"
    window hide
    show us laugh pioneer   at right with dspr
    window show
    play music music_list["eat_some_trouble"] fadein 0
    us "Не поделили потроха свиньи!"
    window hide
    show sl angry pioneer at fleft
    show mi normal pioneer
    with dspr
    window show
    sl "И эта тут! Ну сумасшедший дом!"
    sl "Могла б шалить и в месте ты другом!"
    window hide
    show us upset pioneer at right  with dspr
    window show
    us "Да пробовала, скучно. Можно с вами?"
    sl "Нет, поздно. Мы теперь уже и сами."

    window hide
    show sl serious pioneer at fleft
    show us normal pioneer     at right
    with dspr
    window show
    us "Ведь только утро... Почему же поздно?"
    "В ответ Славяна только смотрит грозно."
    me "Пусть герцога играет, слов там мало."
    sl "Нам и без герцога проблем хватало..."
    show us grin pioneer    at right

    us "А Сталина нельзя у вас сыграть?"
    me "В нём можешь вдохновение черпать."
    me "Усы тебе совсем не помешают,"
    me "Солидности для герцога добавят."
    show us normal pioneer    at right
    sl "Ну, раз Семён тут всё за нас решил,"
    sl "На роль Ульяну лично утвердил..."
    me "Пускай Ульяна тоже роль получит!"
    sl "Надеюсь, не сбежит, когда наскучит."
    show us normal pioneer  at right
    us "В свидетели мне Сталина усы:"
    us "Не подведу, торжественно клянусь!"
    show sl normal pioneer at fleft
    sl "Текст поищу для этой егозы."
    sl "Полезным делом хоть займётся пусть."

    window hide
    hide sl
    hide us
    hide mi
    with dissolve
    show ss nosmile casual at cleft
    show dv normal pioneer at cright
    with dissolve

    window show
    dv "И здесь она за мною увязалась."
    me "Ты ей как мама. Это ли не мило?"
    dv "Скажи, Семён. Тебя давно не били?"
    ss "{i}Didst thou not fall out with a tailor wearing{/i}"
    show ss smile2 casual at cleft with dspr
    ss "{en}I honestly have no idea what I'm saying.{/en}{ru}Я тоже ничего не поняла.{/ru}"
    me "Там репетиция идёт вовсю."
    window hide
    hide ss
    hide dv
    with dissolve
    show stl normal pioneer at cleft
    show el normal pioneer at cright
    with dissolve
    window show
    stl "{i}Пред шавками Монтекки я упрусь - не фдвинуть.{/i}"
    stl "{i}Фотру всех в порошок: и молодцев и девок.{/i}"
    el "{i}Ссора-то ведь господская и между мужской прислугой.{/i}"
    stl "{i}Всё равно. Слажу с мужской, примусь за женскую. Всем покажу фвою силу.{/i}"
    el "{i}И девочкам?{/i}"
    stl "А девочкам особо!"
    el "Ты сбился."
    stl "{i}Пока хватит мочи, и девочкам. Я, флава богу, кусок мяса не малый.{/i}"
    stl "Силу... девочкам... Надо показать..."
    window hide
    hide stl
    show el upset pioneer at cright
    with dspr
    window show
    el "Уходит? Да куда ж уходит он?"
    sl "Толь, подожди! Нам нужен твой Самсон!"
    me "Оставь, не пропадёт ваш паренёк,"
    me "Побудет в бункере своём чуток."
    window hide
    hide el
    show sl serious pioneer at center
    show mz normal glasses pioneer at right
    with dspr
    window show
    sl "Вернуть его... Кто тут не занят? Женя!"
    mz "Гонять за ним? Своё ценю я время."
    sl "Эх, ждите. Снова делать всё самой."
    me "А вдруг опасен? Не рискуй собой."
    show sl normal pioneer at center
    me "Про девочек коль скоро он твердит,"
    me "То Жене и опасность не грозит."
    window hide
    show mz angry glasses pioneer at  right
    with dspr
    window show
    stop music fadeout 4
    sl "Раз он из Капулетти - враг он мой."
    sl "Алиса, совладай-ка со слугой."
    window hide
    show dv normal pioneer  at fleft with dspr
    hide mz
    window show
    play music music_list["heather"] fadein 3
    dv "Сама лови, не твой ли брат-близнец?"
    dv "Вези домой - вдвоём пасти овец."
    window hide
    show dv grin pioneer    at fleft
    show sl angry pioneer at center
    with dspr
    window show
    sl "Их выпас тоже требует ума."
    sl "Боишься, что не справишься сама?"
    dv "Смотри, чтоб он тебя с овцой не спутал."
    dv "И лаской по ошибке не окутал."
    dv "А то ты, может быть, не возражаешь?"
    menu:
        "Заступиться за Славю":
            $ sp_sl += 1
            $ sp_dv -= 1
            me "Алиса, хватит. Ты перегибаешь."
            sl "Оставь. Из уст дурных людей хула"
            sl "Звучит, как от хороших - похвала."
            window hide
            show sl angry pioneer at fright

        "Не вмешиваться":
            sl "Довольно, замолчи! Стыда не знаешь."
            window hide
            hide sl with dspr
            $ renpy.pause(1)
            show dv guilty pioneer at fleft
            with dspr
            window show
            dv "Ну надо же, обиделась. Сбежала."
            dv "А я к разминке только приступала."
            window hide
            show mz rage glasses pioneer at  fright
            with dspr
            window show
            mz "Ведь говорила же, ей тут не место!"
            dv "А вот ещё одна пошла с насеста..."
            dv "Что квохчешь? Та сама ко мне пристала."
            dv "Порядком я от вас уже устала."
            show mz angry glasses pioneer at  fright


            mz "Устала ТЫ от нас? Смешно. И очень!"
            me "Я тут один, кто пьесой озабочен?"
            me "А вам друг другу только б глотки грызть."
            me "Могли бы ссоры на день позабыть?"

    show mt normal pioneer far at center
    with dspr
    window show
    mt "Опять все ссорятся? А ну-ка, перестали!"
    window hide
    hide dv
    hide mz
    hide sl
    with dspr
    $ renpy.pause (0.3)
    show mt normal pioneer at center with dspr
    window show
    stop music fadeout 5
    mt "Саманта, мы тебе костюм достали."
    mt "Два даже. Не заставьте Лену ждать!"
    show mt smile pioneer
    mt "Пойдёмте же скорее примерять!"
    mt "Смотри, Семён! И стоило сердиться?"
    mt "Идём, ты тоже можешь пригодиться."
    window hide
    $ renpy.pause (0.3)
    $ persistent.sprite_time = 'day'
    scene bg ext_library_day
    show ss nosmile casual at cright
    show mt normal pioneer at left
    with dissolve

    window show

    me "Средь книг - кружок раскройки и шитья?"
    me "Куда идём мы? Не смекаю я."
    play music music_list["went_fishing_caught_a_girl"] fadein 5
    mt "Я вас бросаю: срочные дела"
    mt "Мне вспомнились, пока сюда вела."
    window hide
    hide mt
    with dspr
    window show 
    me "{en}The library... Remember our last time here?{/en}{ru}Ты помнишь прошлый наш сюда визит?{/ru}" 
    stop ambience fadeout 3        
    ss "{en}I sure do. Come on, let's show no fear.{/en}{ru}Ещё б. Вперёд, ничто нам не грозит.{/ru}"         
    window hide
    $ renpy.pause (0.3)     
    scene bg int_library_day
    with dissolve    
    show un normal pioneer at cleft  
    with dissolve
    play ambience ambience_library_day fadein 3
    window show 
    un "А вот и вы. Работу завершила."
    me "И правда, платья. Ты сама их сшила?"
    window hide 
    show un shy pioneer at cleft
    show ss smile casual at cright
    with dspr   
    window show     
    ss "{en}Oh, great!{/en}{ru}Прелестно!{/ru}"
    un "Грешна. Люблю раскраивать, сшивать -"
    un "Одно из хобби, можно так сказать."
    show un normal pioneer at cleft
    show ss smile2 casual at cright
    me "Спасибо, это вышло очень кстати."
    un "Ну что, тогда давай примерим платья?"
    un "Ведь их должна ещё я подогнать,"
    un "Понадобятся ножницы острее..."
    show un evil_smile pioneer at cleft
    show ss surprise casual at cright
    with dspr
    un "И легче ими можно, и быстрее"
    un "Хотя б и с крокодила шкуру снять."
    un "Пусть отрезать совсем придётся малость -"
    un "На платье чтоб изъянов не осталось."
    show un smile pioneer at cleft with dspr
    window hide
    $ renpy.pause(0.5)
    me "{en}Have fun here... I shall wait outside.{/en}{ru}Я подожду снаружи, не робей.{/ru}"
    show ss scared casual at cright
    ss "{en}Don't leave me! She is so mad-eyed!{/en}{ru}Не оставляй меня одну тут с ней!{/ru}"
    show ss surprise casual at cright
    me "Мы можем выйти? И тотчас вернуться?"
    un "Иди. А я могу и отвернуться."
    show ss shy casual at cright
    me "{en}We cannot leave without dress nor I can stay.{/en}{ru}Я выйду, Сэм. Давай, примерь-ка их.{/ru}"
    window hide
    show ss shy2 casual at cright
    show un normal pioneer at cleft
    with dspr
    window show
    ss "{en}No way, stand here! You must obey.{/en}{ru}Останься! Возражений никаких!{/ru}"
    me "{en}Suit yourself then. I'm not looking.{/en}{ru}Я отвернусь. Могла бы постесняться.{/ru}"

    show ss normal casual at cright

    ss "{en}You're acting like I'm going to undress.{/en}{ru}Расслабься. Я не буду раздеваться.{/ru}"
    if persistent.ss_text_lang == "en":
        ss "{en}I'm not, Semyon. Chill out. Wrong guess.{/en}"
    window hide
    $ persistent.dress = 'purple'
    show un serious pioneer at cleft
    show ss shy dress  at cright
    show undds at cright
    with dissolve
    window show
    un "Чего на форму? Вдруг размер не тот?"
    un "И как понять? Да разве так пойдёт?"
    me "Всё здорово, старалась ты на славу."
    me "Тебе бы платья шить на всю ораву."
    window hide
    show un normal pioneer at cleft
    with dspr
    window show

    un "Серьёзно? Ладно, что насчёт второго?"
    window hide
    hide ss
    hide undds
    with dspr
    $ renpy.pause(0.5)
    show un serious pioneer at cleft
    $ persistent.dress = 'red'
    show ss nosmile dress  at cright
    show undds at cright
    with dissolve


    window show
    un "По крайней мере, оба ей под стать."
    me "Прелестны оба, надо выбирать."
    me "{en}They both look great. What do you think?{/en}{ru}Все платья хороши, возьмёшь какое?{/ru}"
    show ss unsure dress at cright
    show undds at cright
    ss "{en}It's so hard to decide which one to pick...{/en}{ru}Что скажешь? Тут решенье непростое.{/ru}"
    if persistent.ss_text_lang == "en":
        ss "{en}I need your judgment, Sam. Please. You choose it.{/en}"
    window hide
    $ renpy.pause(0.2)
    menu:
        "Первое лучше":
            me "{en}My vote goes for the first one, if it matters.{/en}{ru}Мой голос платью первому уходит.{/ru}"
            me "{en}It fits your eyes and makes them sparkle even better.{/en}{ru}Оно под цвет твоих очей подходит.{/ru}"
            $ sp_spts = 99
        "Второе лучше":
            me "{en}The second one's well-colored, looks so nice{/en}{ru}Второе даст подбор цветов богатый{/ru}"
            me "{en}Just like your homeland's flag with stars and stripes.{/en}{ru}Под флаг любимый звёздно-полосатый.{/ru}"
            $ persistent.dress = 'red'
    window hide
    ss "{en}I'd like to wear them both... But I agree, we'll take this one.{/en}{ru}Они непревзойдённы оба, право...{/ru}"
    if persistent.ss_text_lang == "ru":
        ss "{ru}Однако лишь одно могу носить.{/ru}"
    window hide
    $ renpy.pause(0.5)
    show ss normal casual at cright
    with dissolve
    window show
    stop music fadeout 5
    if sp_spts == 99:
        $ persistent.dress = 'purple'
    ss "{en}Thank you so much for what you've done.{/en}{ru}Надену то, Семёну что по нраву.{/ru}"
    if persistent.ss_text_lang == "ru":
        ss "{ru}Спасибо... Как тебя благодарить?{/ru}"
    window hide
    show un shy pioneer at cleft
    with dspr
    window show


    un "Внесла в спектакль свой вклад, хвалить не нужно."
    un "К судьбе его и я неравнодушна."
    me "И только это? Ты прости сомненья,"
    play music music_list["silhouette_in_sunset"] fadein 5
    me "Подумалось, ты ищешь примиренья."
    window hide
    show un serious pioneer at cleft
    show ss unsure casual at cright
    with dspr
    window show
    th "Вот ляпнул не подумавши, болтун!"
    th "Испортил всё, мне на язык типун..."
    un "Пожалуй, это тоже... Да, ты прав."
    un "Задабриваю - вот мой гнусный нрав."
    me "Ну что ты, ты хорошая подруга,"
    me "Нас выручала, если было туго."
    show ss normal casual at cright
    me "Дурное слово будет позабыто."
    stop ambience fadeout 5

    me "Ценней дела. Впредь тема та закрыта."

    play sound sfx_dinner_horn_processed
    $ renpy.pause(1.0)
    un "Спасибо... Нас всех горн зовет к обеду."
    un "Закрою тут - пойду за вами следом."
    window hide
    $ renpy.pause(0.5)

    $ persistent.sprite_time = 'day'
    scene bg int_dining_hall_people_day
    show notl:
        pos (1697,0)
    with dissolve
    show sl normal pioneer
    show ss nosmile casual at right
    with dissolve

    play ambience ambience_dining_hall_full fadein 3

    window show
    me "Есть платья! А на сцене как дела?"
    sl "Вот, Толика искала - не нашла."
    show sl serious pioneer

    sl "Алиса - наказание для нас."
    menu:
        "Прости её":
            me "Волнуется, играет в первый раз."
            $ sp_sl += 1
            sl "И потому хамит всем? Не пойдёт."
            me "Алиса неправа, она поймёт."
            me "Терпела раньше, вот терпи и впредь!"
            me "До вечера осталось потерпеть."

        "Ей тоже нелегко":
            me "Вы тоже бросили ей пару фраз."
            $ sp_sl -= 1
            sl "Мы виноваты? Вот как ты считаешь?"
            me "Ты - меньше, но характер Жени знаешь."
            sl "Она-то совершенно безобидна,"
            sl "А от твоей Алисы зло лишь видно."
            me "Так ведь... Алиса не моя, допустим."
            sl "Неважно, эту тему мы опустим."
            me "Чтоб справилась, не трогайте её."
            window hide
            show sl angry pioneer
            with dspr
            window show
            sl "Учту я пожелание твоё."


    window hide
    $ renpy.pause (1)
    show ss laugh casual at right
    show notl:
        pos (1697,0)
    with dspr
    window show
    ss "{en}Semyon can't wait to see you dressed as a boy,{/en}{ru}Семён ждёт в нетерпении ужасном{/ru}"
    if persistent.ss_text_lang == "ru":
        ss "{ru}Увидеть тебя мальчиком одетой.{/ru}"
    show ss grin_smile casual at right
    ss "{en}Haven't you seen those leggings? Oh, you will enjoy!{/en}{ru}Ты видела трико? Оно потрясно!{/ru}"
    if persistent.ss_text_lang == "ru":
        ss "{ru}Ты восхитишься, обещаю это.{/ru}"
    window hide
    show sl surprise pioneer
    with dspr
    window show
    sl "Не понимаю, что она сказала."
    me "Что лучше бы ты девушку играла."
    me "А так - придётся прятать всю красу."
    window hide
    show sl shy pioneer
    with dspr
    window show
    sl "Корсет тугим не... {w}Ой. Ты про косу..."
    window hide
    $ renpy.pause (1)

    show sl normal pioneer
    show ss normal casual at right
    show notl:
        pos (1697,0)
    show mt normal panama pioneer at fleft
    with dissolve
    window show
    stop music fadeout 7
    mt "Ну что, ребята? Столько важных дел."
    mt "А Толя где? Он что, уже поел?"
    sl "Сбежал куда-то, мы его искали."
    sl "Той прыти от него не ожидали."
    window hide
    show mt angry panama pioneer at fleft
    show sl scared pioneer
    show ss surprise casual at right
    show notl:
        pos (1697,0)
    with dspr
    window show
    play music Lou_Inspired
    mt "Проклятье! То была твоя забота -"
    mt "Стеречь того дурного обормота!"
    show ss sad casual at right
    me "Забота? Кто он, собственно, такой?"
    mt "Семён, не до тебя сейчас, постой."
    mt "Ну, Славя, что же ты ещё сидишь?"
    mt "Так сидя, ты его не возвратишь."
    window hide
    $ renpy.pause (0.5)
    hide sl with dissolve
    show mt normal panama pioneer at fleft
    hide notl
    with dspr
    $ renpy.pause (0.5)
    window show

    me "Зачем голодной Славю вы прогнали?"
    me "Я был бы зол, когда б поесть не дали."
    mt "Возможно, я слегка погорячилась,"
    mt "Но Толик кушать любит. Что случилось?!"
    show ss unsure casual at right
    me "Так погодите, Толик - вот же он."
    me "И жив-здоров, доволен аки слон."
    mt "Вернулся? Вот! Одной проблемой меньше."
    $ renpy.pause (0.5)
    mt "...Рассольник, кстати, с голоду вкуснейший!"
    me "Ну ладно. Как теперь со Славей быть?"
    mt "Сей промах, значит, можно ей простить."
    me "Её бы, по-хорошему, вернуть."
    mt "Найти её желаешь? В добрый путь."
    show ss nosmile casual at fright with dspr
    me "{en}Don't move! You - stay!{/en}{ru}Нет, ты - сиди!{/ru}"
    window hide
    show ss serious casual at fright with dspr
    with dspr
    window show
    stop ambience fadeout 3
    ss "{en}I'm staying. But I'm not a dog, okay?{/en}{ru}Я не собака, ладно? Ну иди.{/ru}"

    window hide

    $ persistent.sprite_time = 'day'
    scene bg ext_dining_hall_near_day
    play ambience ambience_camp_center_day fadein 3
    with dissolve

    th "Вот вызвался, а где её искать?"
    th "На помощь Сэмми стоило мне взять."
    th "Толян живет... где, кроме как в столовой?"
    th "А Славя где? Эх, сыщик я бедовый."
    window hide
    scene bg ext_houses_day with dissolve
    window show
    th "Вожатая могла их поселить"
    th "Присмотра ради и неподалёку."
    th "И где б тогда ещё Славяне жить?"
    th "Проверю-ка я этот домик сбоку."
    window hide
    scene bg ext_house_of_sl_day with dissolve
    $ renpy.pause (0.5)
    play sound sfx_knock_door2
    $ renpy.pause (1.5)
    stop ambience fadeout 2
    sl "Войдите."

    play ambience ambience_int_cabin_day fadein 3
    scene bg int_house_of_sl_day
    show sl normal pioneer
    with dissolve
    stop music fadeout 9
    me "Как ловко вычислил я домик твой!"
    sl "А ты не знал? От вас подать рукой."
    me "Наверное, ты редко здесь бываешь."
    me "Всегда в заботах где-то пропадаешь."
    me "Там, кстати, Толик ваш уже нашёлся."
    sl "Я видела, как мимо он прошёлся."
    me "Чего тогда назад ты не вернулась?"
    window hide
    show sl serious pioneer
    with dspr
    window show
    "Тут Славя только горько усмехнулась."
    me "Мне жаль. Она к тебе несправедлива."
    sl "Отчасти. Я могу быть нерадива."
    me "Вздор. Как насчёт пойти назад, к ребятам?"
    me "А то хватиться могут и меня там."
    me "Прервём же череду исчезновений."

    sl "Коль скоро мы дошли до откровений,"
    sl "Себя я лишней чувствую на сцене."
    th "Останемся так только я да Сэмми."
    play music music_list["two_glasses_of_melancholy"] fadein 7
    me "Не хочешь возвращаться? Как же так?"
    me "На ком вся пьеса? Это не пустяк."
    sl "Возможно, много на себя беру."
    sl "И без меня всё скажется к добру."
    me "Ох, Славя, Генда будет мне судья..."
    me "Затворник самый главный - это я."
    me "Всегда ленюсь и, кстати, на мгновенье:"
    me "На публике держусь в большом смущенье."
    me "Но даже я, собрав себя в кулак,"
    me "Помочь пытаюсь пьесе, как-никак."
    sl "Всё ясно, ты радеешь о других."
    sl "А мне зачем горбатиться для них?"
    sl "Ты видел отношение ко мне?"
    th "Не худшее, я думаю, в стране."
    me "Демаршем лишь одну себя накажешь,"
    me "Пошли, нас ждёт у сцены общий сбор."
    th "Вытаскиваю Славю. Вот расскажешь..."
    th "И кто поверит в этот разговор?"
    sl "Нужды в том нет, Семён. Я всё решила."
    sl "Довольно делу общему служила."
    sl "Прошу, не уговаривай, не жди."
    sl "Прими моё решение, иди."
    me "Силком тащить? Других путей не знаю."
    window hide
    show sl angry pioneer
    with dspr
    window show
    sl "Уйди, а то сама тебя ударю."
    th "Ударит ведь, поистине не шутит."
    th "И вряд ли уговорам уж уступит."
    menu:
        "Сдаться":
            me "Я ухожу. Что остальным сказать?"
            sl "Не вздумали чтоб никого прислать."
            jump ss_d5slfailedit
        "Идти до конца":
            $ sp_sl += 1
            me "Ну, вот что, Славя, я тебе скажу,"
            me "Давай-ка я с тобою посижу."
            sl "Ты говорил, что пьеса так важна."
            me "Но без тебя - и вовсе не нужна."
            sl "Решил подлизываться? Не наглей."
            me "Я так и про других скажу друзей."
            window hide
            show sl serious pioneer
            with dspr
            window show
            sl "Вы можете сыграть и без меня,"
            sl "Девчонок ведь хватает окромя,"
            sl "Кто по листку прочесть способен это;"
            sl "Ромео же - один, замены нету."
            sl "Неловко будет, ведь за мной придут."
            sl "Как объясню я посиделки тут?"
            me "Не надо будет объяснять никак."
            me "Скажу, что сам я не могу играть"
            me "От хвори и панических атак."
            me "И им придётся пьесу отменять."
            window hide
            show sl shy pioneer
            with dspr
            window show
            sl "Вот так и скажешь? Ты довольно мил."
            sl "Твоя взяла, хитрюга. Пристыдил."
            menu:
                "Позвать на сцену":
                    $ sp_sl -= 2
                    me "Раз так, то как насчёт пойти на сцену?"
                    me "Чтоб не было позора на всю смену."
                    window hide
                    show sl angry pioneer
                    with dspr
                    window show
                    sl "Ты только выманить меня желал?"
                    sl "Капризы, мол. Спасибо, \"поддержал\"."
                    me "Не хочешь?.. Ну, решили, остаюсь!"
                    sl "Второй раз я на это не куплюсь."
                    me "Ты погоди вот так сплеча рубить."
                    me "Ну, выслушай. Нам есть, что обсудить."
                    sl "Нет, уходи, расстанемся на том."
                    sl "Проверь дома: нет ли меня в каком?"
                    jump ss_d5slfailedit


                "Поговорить ещё":
                    jump ss_d5sl1edit

label ss_d5sl1edit:

    stop music fadeout 3
    me "Тогда скажи скорее, что случилось?"
    me "С чего ты резко так переменилась?"
    play music Warm_evening fadein 5
    window hide
    show sl sad pioneer
    with dspr
    window show
    sl "Ты вспомни лишь, что мне она сказала."
    me "Меня сто раз уж Ольга отчитала."
    sl "Я про Алису. Мол, все ненавидят."
    sl "И то спасибо ей за откровенность,"
    sl "В лицо другие не хотят обидеть."
    sl "А может, просто не находят смелость?"
    me "То сгоряча, она сама не рада."
    sl "Но сказанное ею - это правда."
    sl "Старалась всем помочь, и что с того?"
    sl "Была ли благодарность от кого?"

    sl "Взять хоть бы Олю - той я как должна,"
    sl "А остальным - и вовсе не нужна."
    me "Пускай не ценят все твою работу."
    me "Но лагерю нужна твоя забота."
    window hide
    show sl serious pioneer
    with dspr
    window show
    sl "Считают кем-то вроде полицая,"
    sl "Но за труды ничто не получаю."
    me "Помочь другим, ждя истово награды, -"

    me "Добро ли? Пусть другие будут рады."
    me "Поступок добрый сам вознаграждает,"
    me "Особо - коль никто о нём не знает."
    sl "Кто б мне о бескорыстии тут рёк,"
    sl "Но радоваться не приходит срок,"
    sl "А также быть довольною собою,"
    sl "Когда другие люди злы со мною."
    sl "Раз не на кого в жизни положиться,"
    sl "То надо, видно, приостановиться."

    me "Ужель так грустно всё? А как же я?"
    me "Иль, может быть, я - главная свинья?"
    window hide
    show sl normal pioneer
    with dspr
    window show
    sl "О нет, ты словно света луч во тьме."
    stop music fadeout 3
    sl "И так же тяжело тебе, как мне."
    me "Ты несколько утрируешь невзгоды."
    me "Со мной милы, не обделён свободой."
    me "Вожатой тоже сильно не замучен..."
    window hide
    show sl smile pioneer
    with dspr
    window show
    play music music_list["i_want_to_play"] fadein 3

    sl "Ты слишком мягок... А давай проучим?"
    me "Не ожидал затей я бунтовских..."
    sl "Я тоже не ждала, признаюсь, их."
    sl "У Генды утром был момент один,"
    sl "Когда взбодрил меня адреналин."
    sl "Похоже, в детстве я не нашалилась,"
    sl "Пелёнки лишь менять не приходилось,"
    sl "Пошла - и от хлопот не отходила,"
    sl "И больше не до шалостей уж было."
    sl "Теперь похулиганим. Что вы ждёте,"
    sl "Раз добрый нрав сегодня не в почёте."
    me "О чём-нибудь конкретном говоришь?"
    sl "Пока не знаю. План нам сочинишь?"
    me "Кого под \"ними\" подразумеваешь?"
    me "Вожатой иль Двачевской мстить желаешь?"
    window hide
    show sl surprise pioneer
    with dspr
    window show
    sl "Ещё я не решила, как предложишь."
    sl "Мне в этом начинании поможешь?"

    menu:
        "Согласиться":
            $ d6_slchoice = 1
            $ sp_sl += 1
            me "В любой я кипиш, кроме голодовки."
            me "Как говорил вчера Толян в столовке."
            window hide
            show sl smile pioneer
            with dspr
            window show
            me "Тот кипиш голодовке был полярен."
            me "Бунт без десертов - вот с чем солидарен..."
            show sl smile pioneer

            sl "Отлично! Знала, что не подведёшь."
            me "Надеюсь, что и ты меня поймёшь,"
            me "Но к воплощению любого плана"
            me "Придётся всё же выйти из тумана."
            window hide
            show sl sad pioneer
            with dspr
            window show
            sl "Так ведь у нас пока и плана нету."
            me "На месте будет проще разобраться,"
            me "Когда и как нам совершить вендетту."
            me "В разведку предлагаю выдвигаться."
            window hide
            show sl smile pioneer
            with dspr
            window show
            sl "Ты прав, теперь нам лучше быть со всеми."
            sl "Идём, идём! Устроим им проблемы!"
            jump ss_d5slwinedit
        "Отказаться":
            $ d6_slchoice = 0
            $ sp_sl -= 1
            me "Хм, Славя. Ты сегодня не в себе."

            show sl angry pioneer

            me "Тому виной ли прерванный обед"
            me "Или моё неловкое паденье -"
            me "Могло же вызвать мозга сотрясенье."
            me "Пожалуй, стоит отдохнуть денёк,"
            me "Моё присутствие - видать, не впрок."
            me "Поэтому сейчас вернусь ко всем,"
            me "Скажу, хандришь. О прочем буду нем."
            sl "Ошибся ты. Я полностью в порядке."
            sl "И более играть не стану в прятки."
            sl "Раз ты идешь, я тоже к ним вернусь."
            sl "Но планами с тобой не поделюсь."
            me "На сцене веселее будешь, верю."
            me "Не дело - подрывать людей доверье."

            jump ss_d5slwinedit

label ss_d5slfailedit:
    window hide
    stop ambience fadeout 3
    $ persistent.sprite_time = 'day'
    scene bg ext_houses_day
    with dissolve

    window show
    stop music fadeout 5
    "От Слави выйдя, я пошёл домой:"
    "Хоть на минутку обрести покой."
    window hide
    play ambience ambience_int_cabin_day fadein 3
    scene bg int_house_of_mt_day
    show mt normal pioneer at cleft
    show ss nosmile casual at cright
    with dissolve

    window show
    mt "Привет, сосед. Побалуемся чаем?"
    mt "Мы тут как раз варенье открываем."
    play music music_list["get_to_know_me_better"] fadein 5

    me "Я с радостью. А вы чего здесь, дома?"
    mt "На улице разгулье насекомых."
    ss "{en}Hey, did you find Slavyana?{/en}{ru}Семён, ну как ты, не нашёл Славяну?{/ru}"
    me "{en}Yes but there was some drama.{/en}{ru}Нашёл, но там не всё пошло по плану.{/ru}"

    mt "Что там со Славей вышло, говоришь?"
    menu:
        "Приболела":
            th "Винить вожатую - лишь усложнишь."
            me "Она немного... Вроде, приболела."

            show ss sad casual at cright

            me "Пусть отлежится, раз такое дело."
            mt "И прогуляет пьесу? Нет, никак!"
            mt "В медпункт пусть сходит, время есть пока."
            me "А толк с врачей? Там с ней уже бывал."

            show ss nosmile casual at cright
            mt "Патологоанатом мой отец."
            show mt sad pioneer at cleft
            mt "И с каждым был он вежлив, чтоб ты знал,"
            mt "С несчастным, повстречавшим свой конец."
            me "Славяне нынче надобен покой,"

            me "Чтоб не достаться вашему папуле."
            mt "За это не волнуйся, дорогой,"
            show mt smile pioneer at cleft
            mt "Такому не бывать, года минули."
            me "На все года ведь эскулап мертвецкий."
            show mt normal pioneer at cleft
            mt "Не папенька. Ведь врач он редкий... {w}Детский."





        "Обиделась":
            $ sp_sl -= 1
            me "Бастует. И никак не вразумишь -"
            me "Пытался. Лучше к ней не подходить."
            mt "А в чем проблема, можешь объяснить?"
            me "Вы были с ней резки, другие тоже."
            me "И на меня обижена, похоже."

    mt "Ну, загляну к помощнице своей."
    mt "Ты на хозяйстве. Чай пока разлей."
    me "Из дома выманить её - проблема."
    me "Пусть лучше отдохнёт, найдём замену."
    window hide
    show mt smile pioneer at cleft
    show ss nosmile casual at cright
    with dspr
    window show
    mt "Бьюсь об заклад, управлюсь в пять минут."
    mt "За десять всё варенье тут сметут."
    me "Пусть десять, только Славю не попустит."
    me "Скорее уж вас со ступеней спустит."
    mt "Пусть десять, но я выиграю спор -"

    mt "Три дня мне помогаешь, уговор?"
    mt "Без устали, на отдых без надежды"
    mt "В столовой убирать, стирать одежду..."
    me "А если же Славяна не придёт,"
    me "От всех трудов меня свобода ждёт."
    me "Линеек никаких, сплю до обеда."
    me "Вот что хочу я за свою победу."
    mt "Договорились. Ты продешевил."
    window hide
    hide mt
    with dspr
    window show
    th "Морально давит? Где-то я сглупил?"
    $ renpy.pause(0.5)
    ss "{en}How come you've spoken to Slavyana but failed to convince her?{/en}{ru}Как только Славю сам не убедил?{/ru}"
    ss "{en}I wonder if she showed you to the door.{/en}{ru}Иль прогнала тебя, ты утаил?{/ru}"
    me "{en}She's not so fond of me. Your accusations go in vain.{/en}{ru}Твои намёки мне отчасти лестны,{/ru}"
    me "{en}Looks like my persuasion power has failed me again.{/en}{ru}Не столь я убедителен, коль честно.{/ru}"
    $ renpy.pause(1)
    "А время шло. Вот пять минут, и шесть."
    "Я предвкушал уже благую весть..."
    play sound sfx_open_door_2
    window hide
    show ss normal casual at fright
    show sl normal pioneer at left
    show mt normal pioneer
    with dissolve
    window show
    mt "А вот и мы! Ну что, уже заждались?"
    mt "А Славя там совсем проголодалась."
    mt "Поэтому её в наш дом на чай"
    mt "Я пригласила как бы невзначай."
    "Вожатая смотрела на часы."
    mt "Давай, Семён, нарежь нам колбасы."
    window hide
    $ renpy.pause(1)
    hide mt
    hide ss
    show sl shy pioneer
    with dissolve
    "И вот конец колбасному раздолью."
    "Окончилось внезапное застолье,"
    "Вожатая с Самантой - мерить платья."
    "Теперь могу и Славе всё сказать я."
    "Она стыдливо на меня смотрела."
    sl "Да, признаю. Я слишком мягкотела."
    me "Какими же словами заманила?"
    me "Наверное, прощения просила?"
    sl "Открыла мне ваш с нею уговор."
    th "Пришла назло мне? Это перебор!"
    sl "...Что взялся ты во всём мне помогать."
    sl "И тут уж не могла я отказать."
    th "Вожатая меня перехитрила!"
    th "И Славю. Ведь обоих нас умыла."
    sl "Ради меня ты так самоотвержен."
    sl "Хотя и в разговорах вечно сдержан."
    th "Тут даже смысла нету говорить,"
    th "Что я помощником не вызывался."
    th "Раз проиграл, то лучше утаить,"
    th "Что в рабстве против воли оказался."
    sl "Я так была к тебе несправедлива..."
    th "...Сказать ей правду - просто некрасиво."
    window hide
    show sl sad pioneer
    with dspr
    sl "Ты весь в себе. Должно быть, утомила."
    me "Задумался. О чём ты говорила?"
    stop music fadeout 5
    sl "Неважно. Ты готов вернуться к сцене?"
    stop ambience fadeout 2

    me "Конечно. Будет день ещё для лени."

    $ sp_sl += 1
    jump ss_d5slwinedit
label ss_d5slwinedit:
    window hide
    $ renpy.pause (0.5)

    $ persistent.sprite_time = 'day'
    scene bg ext_stage_normal_day
    play ambience ambience_camp_center_day fadein 2

    show mz normal glasses pioneer at cleft
    with dissolve
    show sl normal pioneer  at cright
    with dissolve
    window show
    mz "Явились. Где носили чёрти их?"
    mz "Как репетировать без вас, блажных?"
    sl "Дела. Ну очень важные. Прости."
    stop music fadeout 4
    mz "Мы ждали час! Сил нет уж вас пасти."
    me "\"Мы\" - это кто? Ты тут одна, окстись!"
    mz "Теперь одна! Другие разошлись."
    play music music_list["my_daily_life"] fadein 7
    sl "И что нам делать? В срок всех не собрать,"
    sl "Уж скоро ужин не заставит ждать."
    me "Раз так, то репетиции не будет."
    me "Надеюсь, Женя текст свой не забудет."
    me "Две строчки... Я в других не сомневаюсь."
    window hide
    show mz angry glasses pioneer at cleft
    with dspr
    window show
    mz "Головотяпству просто поражаюсь."
    me "А есть ли предложения другие?"
    mz "Касательно тебя? Есть кой-какие."
    me "Тогда озвучь нам пару. Например?.."
    window hide
    show mz bukal glasses pioneer at cleft
    with dspr
    window show
    mz "Избавь от рыжей пьесу, кавалер."
    mz "Хлопочешь так. Надеешься, зачтётся"
    mz "И после пьесы бегать не придётся?"
    mz "Всех подведёт - начнёт на всех кидаться."
    mz "С тобой захочет первым поквитаться."
    me "А ты что скажешь, Славя? Как считаешь?"

    show sl serious pioneer     at cright

    sl "Мой взгляд не важен, ты же понимаешь."
    sl "Теперь уже и вариантов нет."
    mz "Смотрите: кто-то сляжет в лазарет."
    "Ругаться с Женей дальше не хотелось."
    "Зачем? Чтобы она тут разревелась?"
    me "На этом всё, хорошего вам дня."
    me "Дела есть и другие у меня."
    mz "Опять бежит! Какие чудеса!"
    window hide
    scene bg ext_house_of_mt_day with dissolve
    window show
    $ renpy.pause(0.5)
    th "До пьесы будет где-то три часа,"
    th "И, чтоб не провалиться с постановкой,"
    th "Важна минута даже подготовки."
    stop music fadeout 7
    th "Так быть или не быть усердным в этом?"
    th "Или пешком отправиться по свету?"
    window hide
    $ renpy.pause(0.5)

    $ disable_all_zones()
    $ set_zone("me_mt_house", "ss_d6homeedit")
    $ set_zone("library", "ss_d6uneedit")
    $ set_zone("beach", "ss_d6dveedit")
    $ show_map()


label ss_d6homeedit:
    play ambience ambience_int_cabin_day fadein 2
    scene bg int_house_of_mt_day
    show mystical_box:
        pos (1348,0)
    with dissolve
    "В усердии прошёл почти что час,"
    "Остановиться было б в самый раз."
    window hide


    menu:
        "Учить роль дальше":
            jump ss_d6urdedit


        "Читать чужие роли":
            jump ss_d6escedit

        "Сходить куда-нибудь ещё":
            $ disable_all_zones()
            $ set_zone("library", "ss_d6uneedit")
            $ set_zone("beach", "ss_d6dveedit")
            $ show_map()

label ss_d6urdedit:
            window show
            "Борясь упорно со своей зевотой,"
            "Не в силах совладать со смертной скукой,"
            "Я вскоре был во власти у дремоты"
            "В обнимку с лучшею своей подругой."
            "На все века - любимая игрушка,"
            "О, сколько счастья даришь ты, подушка."
            "Сигнал на ужин слышу я сквозь сон."
            "До вечера проспал я, как барон."
            "Да только вот еду не принесут."
            "Пойду-ка гляну, что там подают."
            window hide
            $ renpy.pause(1)
            jump ss_d6dinedit



label ss_d6escedit:
    $ d6_nolife = 1
    window show
    th "Чем роль одну по кругу повторять,"
    th "Могу другие также почитать."
    th "Вот партия Алисы. Пригодится,"
    th "Заминке коли суждено случиться."
    "..."
    th "Что, кстати, там со временем у нас?"
    th "На то ещё один потрачен час."

    th "Текст плыл пред взором, разбегались мысли."
    stop ambience fadeout 2

    th "Над текстами корпеть нет больше смысла."
    window hide
    scene bg ext_house_of_mt_day with dissolve
    $ renpy.pause(1)
    window show
    play sound sfx_dinner_horn_processed
    $ renpy.pause(1)
    th "Уже сигнал гудит? Что на часах?"
    th "Сегодня ужин раньше, стало быть."
    th "И хорошо: от скуки я б зачах."
    th "Теперь я рад к столовой поспешить."

    window hide
    $ renpy.pause(1)
    jump ss_d6dinedit




label ss_d6uneedit:
    $ d6_treasure = 1
    $ sp_un += 1

    play ambience ambience_int_cabin_day fadein 2
    scene bg ext_house_of_mt_day with dissolve
    th "Как там дела у Лены? Можно глянуть."
    th "Рискну в библиотеку к ней нагрянуть."
    scene bg int_library_day
    show un smile pioneer
    with dissolve
    $ renpy.pause(1.0)
    un "Один пришёл ты? Что-нибудь хотел?"
    me "Один... Нет, просто, без особых дел."
    un "Вам разве репетировать не нужно?"
    me "Актёры наши разбежались дружно."
    me "Я, может быть, не к месту заглянул?"

    show un shy pioneer

    un "Нет, что ты! Занимай скорее стул."
    show un smile pioneer
    un "Нельзя не к месту быть в библиотеке."
    me "У вас тут ценность в каждом человеке."



    un "Хотя мы в заведении публичном,"
    un "Нет публики, и дело то - обычно."
    me "Коль скоро так, воспользуйся моментом,"
    me "Готов побыть сейчас твоим клиентом."
    show un shy pioneer
    un "Решил взять что-то почитать? Сейчас?"
    un "Ведь пьеса на носу, никак, у нас."
    me "Свою я роль освоил на отлично."
    show un normal pioneer
    me "Пред смертью не надышишься обычно."
    me "Так почему бы и не отдохнуть?"
    me "Хотя б на иллюстрации взглянуть."
    play music music_list["your_bright_side"] fadein 7
    un "Пожалуйста. Бери любые книги,"
    un "Раз не содержит твой визит интриги."

    th "Содержит ли?.. Какую - сам не в курсе."
    th "Пройдусь по полкам - оценю ресурсы."
    window hide
    hide un with dspr
    $ renpy.pause(0.5)
    window show
    me "Брошюра - это Ольге и подругам:"
    me "Секреты воспитания линейкой."
    un "Ну что ты. Ольга только ставит в угол."
    me "Таких не помешало б ставить к стенке."
    me "...Поэзия, по слою пыли видно."
    un "Всю вытерла, выдумывать не стыдно?"
    me "А если я за полки загляну?"
    me "Я в паутине там не утону?"
    un "За чистотой следить не подряжали."
    me "Тут что-то между полками прижали..."
    me "Вот. Роберт Стивенсон. \"Сокровищ Остров\"."
    me "Вновь за стеллаж не улетит так просто?"
    me "Художественным книгам тут не рады,"
    me "Зато в почёте Брежнева доклады."
    "Едва свою находку я раскрыл,"
    "Спикировал на пол листок оттуда."
    window hide
    show un normal pioneer at fright with dspr
    window show
    # un "Тебе бы к книжкам поумерить пыл."
    # me "Эх, до чего же переплёт уныл."
    # th "Испортил, только в руки взяв, дебил!"
    # Н.: А, к чёрту. Пускай уж будет глагол.
    un "Гляди, Семён, ты что-то уронил."
    un "Давай я вклею, время есть покуда."
    un "А можно и подшить, несложен труд."
    me "Ба, да у нас сокровищ карта тут!"
    window hide
    show treasure_map with dissolve:
        pos (400,200)

    un "Лишь иллюстрация, к чему восторги?"
    un "Сейчас отреставрируем..."
    me "Постой-ка,"
    me "Попал нам в руки подлинник на вид,"
    me "Путь к сказочным сокровищам хранит."
    window hide
    hide treasure_map
    show un normal pioneer
    with dissolve
    window show
    un "Дай посмотреть. {w}Рисунок тут простецкий,"
    un "Что совершенно очевидно - детский."
    me "Допустим. Тут имеем мы расклад:"
    me "На Ближнем острове закопан клад."
    me "Успеть туда до ужина мы можем,"
    me "Задержимся - не отощаем тоже."
    window hide
    show un smile3 pioneer with dspr
    window show
    un "Всё бросить и копать по детской карте?"
    me "Неужто ты не чувствуешь азарта?"
    window hide
    show un serious pioneer with dspr
    window show
    un "Так ты не шутишь, что ли? Но зачем?"
    un "Какое-то ребячество совсем."
    me "Ты предпочтёшь средь книжек заточенье"
    me "Какому-никакому приключенью?"
    me "Отставить {w}жизнь без цели проживать!"
    me "Снаружи мимо нас проходит {w}лето!"
    un "Но разве можно просто так удрать?"
    un "А как вожатой объясню я это?"
    me "Не бойся, нас не хватится никто!"
    me "Сюда вообще заходят раз лет в сто."
    window hide
    show un smile pioneer with dspr
    window show
    un "Ну хорошо, тебе я уступаю."
    un "Откуда взялся твой энтузиазм?"
    me "Душа зовёт. Куда - и сам не знаю."
    un "Быть может, тянет в детство. Иль в маразм..."

    stop ambience fadeout 2
    window hide
    $ persistent.sprite_time = 'day'
    scene bg ext_boathouse_day
    show un normal pioneer at left
    with dissolve

    play ambience ambience_boat_station_day fadein 3

    window show
    me "Теперь, когда ты согласилась плыть,"
    me "Мне это кажется плохой идеей."
    un "Нет уж, плывём. Но сам ты будешь рыть."
    un "Авось и обернётся чем затея."
    window hide
    scene cg un_boat with dissolve2
    $ renpy.pause(1)
    window show
    un "Давай я помогу тебе грести?"
    me "Будь штурманом. Попробуй провести"
    me "Лодчонку мимо всех мелей и рифов."
    me "Пока я совершаю труд сизифов."
    un "Не жалуйся, ведь это лишь разминка,"
    un "Назад тяжёлый клад везти, смекаешь?"
    un "Твоя его по праву половинка."
    un "На что потратить ты её желаешь?"
    me "Смотря что будет там в ассортименте."
    me "Рассудим здраво, всё идёт к чему:"
    me "В кефирно-булочном эквиваленте"
    me "Положенную долю мне возьму."
    un "Тебя за скромность нужно похвалить."
    th "...Коль скоро спирт и женщин не купить."
    me "Ещё бы мне не помешала вилла"
    me "На острове, но, чур, не на Курилах."
    me "А там, где никогда не видят снега."
    me "А также яхта, вертолёт... {w}Из лего!"
    un "А что за лего? Это что такое?"
    me "Майнкрафт в коробке. Строить там любое..."
    un "Майнкрафт? Вроде цемента это что-то?"
    un "Хотя нет. Из цемента вертолёты..."
    me "Из грязи, если нужно точным быть."
    un "Из грязи хочешь остров прикупить?"
    me "Конструктор! Вспомнил слово наконец."
    un "Довёз ты нас до места, молодец."
    window hide

    $ persistent.sprite_time = 'day'
    scene bg ext_island_day
    show un laugh pioneer at cright
    with dissolve

    play ambience ambience_lake_shore_day fadein 3

    $ renpy.pause(0.5)
    window show
    un "Вот остров твой, и грязи тут хватает."
    un "Твои мечты без клада исполняет"
    un "Волшебник тутошний, а может, джинн,"
    un "Которому теперь ты властелин."
    me "Раз так, то загадаю что другое."
    window hide
    show un shy pioneer at cright
    with dspr
    window show

    un "И я хочу. Дозволено такое?"
    me "Конечно. Пробуй, отчего же нет?"
    show un smile pioneer

    un "Гадала уж - не видела ответ."
    me "На этот раз увидишь непременно."
    me "Смотри, какой вокруг вид обалденный."
    me "Жаль, любоваться времени и нет,"
    me "Пора лопатой несть пейзажу вред."
    me "Начну копать туда отсюда вот,"
    me "Пока клад сам нам в руки не придёт!"
    "..."
    play sound sfx_hatch_drop
    window hide
    show un surprise pioneer at cright
    with dspr
    window show
    me "Тут звякнуло! Похоже, наш сундук!"
    me "Сейчас к тебе я подкопаюсь, друг..."
    window hide
    show un normal pioneer at cright
    with dspr
    window show
    me "Ах, нет, смотри, простая банка это."
    me "Возьмём, раз больше ничего тут нету."
    me "За грязью содержимого не видно."

    un "Так это клад и есть твой? Вот обидно."
    me "Ну хоть нашли, а то копать устал."
    th "Ещё пред пьесой спину надорвал."
    un "Скорее открывай, чего томить."
    th "Не так-то просто крышку открутить."
    me "Ведь амфора сия не герметична,"
    me "Часть крышки кем-то съедена частично."
    "Вот крышка поддалась, и вуаля -"
    "А в нашей банке - доверху земля."
    me "Фигура шахмат - это ферзь понурый."
    me "Когда-то чёрный, а поныне бурый."
    me "Это к чему такая нам посылка?"
    show un smile pioneer at cright
    un "Наверное, к Кассилю тут отсылка."
    un "А кроме - что-нибудь ещё там есть?"
    me "Копнём поглубже... вижу, лупа здесь."
    stop music fadeout 5
    me "Монеток пара, зуб и шарик тоже."
    me "Не сильно на сокровища похоже."
    un "Постой, теперь письмо. Давай читать."
    un "Конверт прогнил, но можно разобрать..."
    play music music_list["always_ready"] fadein 5
    un "{i}Открыть в эпоху мира во всём мире.{/i}"

    me "Ещё чего. Карман держите шире."
    un "Смотрю... {i}Привет вам, будущего людям!{/i}"
    me "Не мне письмо? Пожалуй, нет. Забудем."
    un "{i}Мы пионеры лагеря «Совёнок».{/i}"
    un "{i}Никита, Олька, Стёпа-Фриц, Опёнок.{/i}"
    un "{i}Абрам Иваныч предал уговор:{/i}"
    un "{i}Бинокль зажал потомкам, вот позор!{/i}"
    un "{i}Поскольку он еврей, с него и спрос -{/i}"
    un "{i}За жадность получил от Стёпки в нос.{/i}"
    un "{i}Но нации здесь все равны вовек.{/i}"
    un "{i}И чтоб Абраше не было обидно,{/i}"
    un "{i}По носу получил и Турсунбек.{/i}"
    un "{i}Да так технично - ничего не видно.{/i}"
    me "А в будущем искоренён расизм."
    me "И смотрится весь лагерь, как один"
    me "Большой этнокультурный организм."
    me "Чему я рад, как не совсем блондин."
    me "И всё же мучает меня вопрос,"
    me "Куда все нацменьшинства подевались?"
    me "Или пропасть рискую, сунув нос"
    me "В проблемы, что мне, в сущности, не сдались..."
    window hide
    show un serious pioneer at cright
    with dspr
    window show
    un "Не замечаешь, может? Мне читать?"
    me "Читай, не буду я перебивать."

    un "{i}Вам - вещи, положите их в музей,{/i}"
    un "{i}Чтоб помнили из прошлого детей.{/i}"
    un "{i}(Могли бы к нам сквозь время прилететь -{/i}"
    un "{i}И сможете в бинокль вы посмотреть.){/i}"
    un "{i}А мы же здесь торжественно клянёмся{/i}"
    un "{i}Его скорей приблизить со всех сил,{/i}"
    un "{i}Иначе в гости вас мы не дождёмся,{/i}"
    stop music fadeout 5
    un "{i}И не получите вы наш посыл.{/i}"
    window hide
    show un smile pioneer at cright
    with dspr
    window show
    un "Там подпись, дата... Семьдесят второй."
    play music music_list["your_bright_side"] fadein 3
    me "Что скажешь, по душе ли клад такой?"
    un "Конечно же... Всё это очень мило."
    un "Когда-то я сама такой зарыла."
    me "Посмотрим, в банке что ещё осталось."
    me "С десяток слайдов... Порченые, жалость."
    me "Нет, погоди, один всё ж сохранился."
    window hide
    show youngod at center with dissolve
    $ renpy.pause(3)
    window show
    me "Смотри-ка, кто у нас тут объявился!"
    window hide
    hide  youngod with dissolve
    $ renpy.pause(0.5)
    show un laugh pioneer at cright
    with dspr
    window show
    un "Ну надо же! Привет нам от вожатой."
    me "Давнишний гость она тут. Завсегдатай!"
    window hide
    show un smile pioneer at cright
    with dspr
    window show
    un "Что, интересно мне, с другими стало?"
    un "У взрослых тяга к лагерю пропала?"
    me "Мы можем прямо у неё спросить."
    me "Настало время и обратно плыть."

    show un normal pioneer at cright

    me "Но вот смотри - ещё одна находка."
    un "Сырой земли комок? А, нет. Заколка..."
    me "Наверное, её. Возьмём с собой."
    window hide
    show un normal pioneer at cright
    with dspr
    window show
    me "А остальное закопать обратно."
    me "Ну, кроме слайдов и письма, понятно."
    me "Жаль, до сего дня не дожили вещи."
    me "Что ж, время не щадит. Ничто не вечно."
    stop music fadeout 5
    "Вот бережно уложены находки,"
    "Настал момент вернуться к нашей лодке."
    window hide
    scene cg un_boat with dissolve2
    $ renpy.pause(1)
    play music music_list["memories"] fadein 3
    window show
    un "Уже на тех детей мы не похожи."
    un "Вот ценности - от сердца оторвём?"
    un "И для потомков на потом положим?"
    un "Уже мы и не думаем о том."
    me "А у меня и ценностей-то нет..."
    me "Желаешь тоже написать завет?"
    un "Я не к тому. Те верили во что-то."
    me "Положено то детям желторотым."
    me "Да будто ты не верила сама?"
    un "Не помню. В голове как пелена."
    un "А вспомнить бы мне детские мечты,"
    un "Узнать бы, что я думала тогда,"
    un "Когда такой деревья высоты"
    un "И жуткой глубины такой вода."
    un "И было на душе всегда тепло,"
    un "И к будущему светлому вело..."
    un "Сейчас как будто вовсе нет его."
    un "И от него не нужно ничего."

    me "Поход-то на хандру тебя настроил."
    me "Надеюсь, что не зря его устроил."
    un "Нет-нет, всё хорошо, и даже слишком."
    un "Поэтому немного и раскисла."
    #un "Поэтому и тронулась умишком."
    "Тут Лена попыталась улыбнуться."
    stop ambience fadeout 2
    stop music fadeout 7
    un "Забудь. Приплыли мы, пора вернуться."
    window hide
    $ persistent.sprite_time = 'day'
    scene bg ext_library_day
    show un serious pioneer at cright
    with fade

    play ambience ambience_camp_center_day fadein 2

    $ renpy.pause(0.5)
    window show
    un "До ужина сижу в библиотеке."
    me "А Ольге отнести находку века?"
    un "А нет нужды: она и так идёт."
    un "Сейчас мне за отлучку попадёт."
    window hide
    show mt normal pioneer at left
    with dspr
    window show
    play music Father_of_Pearl fadein 3
    mt "Доверенный кружок сегодня что-то"
    mt "Спонтанно перешёл в режим работы"
    mt "Весьма свободный... Но оно понятно."
    mt "Сегодня день особый, так что ладно."
    mt "Не грех порой по делу отойти."
    me "...Посланье чтоб из прошлого найти!"
    window hide
    show mt surprise pioneer at left
    show un normal pioneer at cright
    with dspr
    window show
    me "Вот, посмотрите, что у нас для вас."
    mt "Что тут за грязь? Да ты и сам чумаз."
    me "Любимая заколка, не иначе."
    me "Ну что ж вы? По сюжету в этом месте"
    me "Вожатая растрогалась и плачет."
    me "И посвящает оды нашей чести."
    window hide
    show mt angry pioneer at left with dspr
    window show
    mt "То не моё. Верни туда, где взял."
    mt "Плыви и закопай, как откопал!"
    me "А вы, выходит, вещь свою узнали..."
    mt "Вот дети надоедливые стали!"
    window hide
    show mt normal pioneer at left with dspr
    $ renpy.pause(1)
    window show
    mt "Ты понял верно, эту вещь я знаю."
    mt "Однако видеть вовсе не желаю."
    un "Ещё письмо и карточка, взгляните..."
    mt "Что за обрывки это? Погодите..."
    window hide
    show mt smile pioneer at left
    show un smile pioneer at cright
    with dspr
    window show
    mt "Так это я! Малышка, ну и ну!"
    mt "Теперь припоминаю, что к чему."
    mt "Смотрю на фото и... могу вернуться."
    mt "Теперь я эти вещи сохраню."
    window hide
    show mt normal pioneer at left with dspr
    window show
    mt "Простите, что была резка, родные."
    mt "Печальные из памяти моменты..."
    mt "Мои воспоминания дурные."
    mt "Но всё равно мои вам комплименты."
    un "А что насчёт других ребят в письме?"
    show mt smile pioneer at left
    mt "Ну, как узнаешь - сообщи и мне."
    mt "Серьёзно, я не слышала про них."
    window hide
    show mt grin pioneer at left with dspr
    window show
    mt "Кроме Никиты. Тот ещё был псих."
    show un smile pioneer at cright
    mt "По плечи лохмы, позже - борода."
    th "Никита Литвинков был?.. Джигурда?"
    window hide
    show mt normal pioneer at left with dspr
    window show

    stop music fadeout 5
    mt "Он не любил тут быть, но приезжал,"
    mt "Как будто бы его тянули силой."
    mt "Активный парень, многих тем достал,"
    mt "В компании всегда был заводилой."
    mt "Однажды был вожатым, как узнала."
    mt "И вдруг удрал. В тот год не приезжала."
    me "Звучит, как будто всё он ради вас."
    show mt normal pioneer at left
    show un normal pioneer at cright
    play music Vesperia_Part_Three fadein 5
    mt "Ты, выдумщик, совсем не знаешь нас..."

    me "Но знаю, как одно с другим сложить!"
    me "Ваш кавалер открыться не решился"
    me "С того, что в ваших чувствах усомнился."
    me "Неправ он был, рискну предположить."

    mt "Вам, молодёжь, бы надо уяснить,"
    mt "Что парню с девушкою просто так"
    mt "И не зазорно просто рядом быть,"
    mt "Приличий не переходя никак."
    window hide
    show mt sad pioneer at left with dspr
    window show
    mt "...Что к большему в итоге не приводит."
    mt "Без чувств же ничего не происходит..."
    mt "Куда уж лучше этой новой моды:"
    mt "Чуть познакомились - уже сошлись."
    mt "Душа черствеет от такой свободы;"
    mt "Она одна. Не с каждым ей делись."
    un "Я с Ольгой Дмитриевной тут согласна:"
    un "Любя, не должно чувствовать сомнений."
    un "Превозносили бы любовь напрасно,"
    un "Родись она из каждых отношений."
    mt "Я заболталась. С вами хорошо,"
    mt "Но не оглянешься, как день прошёл."
    mt "За экскурс в прошлое благодарю,"
    mt "Жаль только, сохранилось маловато."
    mt "Пора бежать уже, как посмотрю."
    mt "Увидимся за ужином, ребята."
    window hide
    hide mt
    with dspr
    $ renpy.pause(1)
    window show
    me "Нашли больное место у вожатой."
    un "Наверное, теперь ей грустновато."
    un "Возможно, зря ту тему поднимал."
    me "Напротив - истины момент настал:"
    me "Его разыщет, может, наконец."
    me "И добрым словом вспомнится Семён -"
    me "Не как растяпа, а как Купидон,"
    me "Соединитель любящих сердец."
    me "Но мне заколка больше интересна."
    me "Есть в ней какой-то отблеск мрачной тайны."
    me "Других девчонок нет, как нам известно..."
    un "Нет, здесь наверняка исход печальный."


    me "Есть у меня забавный вариант."
    me "Заколка та Никиты. Экий франт!"
    un "Подарок Оли? Что же закопали?"
    me "Его девчонки мало занимали,"
    me "И в этом для печали повод ровно."
    window hide
    show un serious pioneer at cright
    with dspr
    window show
    un "Выдумывать такое? Как нескромно!"
    un "А если про тебя вдруг сплетни пустят?"
    un "Ты не из тех, кто шанса не упустит..."
    window hide
    show un shy pioneer at cright
    with dspr
    window show

    th "Ведь сам поднял вопросы декаданса."
    me "Какие это я теряю шансы?"
    me "Сама решила? Кто-то говорил?"
    $ renpy.pause(1)
    me "Не отвечай. Я просто пошутил."
    window hide
    show un sad pioneer at cright
    with dspr
    window show
    me "С тобой могу я это обсуждать,"
    me "Не станешь, верно, слухи распускать."

    un "Ты говоришь обычно деликатно,"
    un "Не вспомнил ты про мой поступок злобный."
    un "И хоть великодушие приятно,"
    un "От этой темы мне не быть свободной."
    un "Ты утром предлагал её закрыть,"
    un "Но разве можно просто так забыть?"
    me "Вернулись к драме той в библиотеке?"
    me "Что ж, не грешно спустить пар человеку."
    me "Конечно, способ твой - своеобразный,"
    me "Реакция людей быть может разной."
    un "Удивлена. Меня не ненавидишь."
    un "Такой меня ты больше не увидишь."
    me "Расскажешь, может, что в тот раз случилось?"
    $ renpy.pause(1)
    me "Нет?.. Ладно. Что-то правда изменилось."
    window hide
    show un normal pioneer at cright
    with dspr
    window show
    un "Да не особо, честно говоря."
    un "Но за меня ты не волнуйся зря."
    un "По правде, так я вовсе не опасна."
    un "И вред лишь словом нанесу напрасным."
    me "Ко мне всегда ты можешь обратиться,"
    me "Не чувствуя в душе своей дилеммы."
    me "Совсем не так твоя страшна проблема,"
    me "Когда ей с другом можешь поделиться."
    un "Нет, не рискну поведать о судьбе,"
    un "Безмолвствовать велит нелёгкий жребий."
    un "Вними же сердца искренней мольбе:"
    un "Не друг сейчас душе моей потребен."
    # Н.: Ну а что? Семён всё равно не понимает намёков.
    menu:
        "Оставить в покое":
            me "Наверное, в чужое лезу дело."
            stop music fadeout 5
            me "А коль захочешь, стало чтоб моим, -"
            me "Тогда ко мне ты обращайся смело!"
            me "Глядишь, твою проблему и решим."
            window hide
            show un smile pioneer at cright
            with dspr
            window show
            un "Спасибо. Коль не справлюсь - поделюсь."
            un "Пока в библиотеку я вернусь."
            th "Пожалуй, хватит мне за ней ходить."
            th "Чтобы совсем назойливым не быть."

            th "Направлюсь-ка, наверное, домой,"
            stop ambience fadeout 2
            th "Немножко хоть самим займусь собой."
            window hide
            scene bg int_house_of_mt_day with dissolve
            window show
            "Пока неспешно к домику добрёл,"
            "Слегка в порядок там себя привёл,"
            "Как всех уже зовёт гудок на ужин,"

            "Что после променада очень нужен."
            window hide
            $ renpy.pause(1)
            jump ss_d6dinedit

        "Попытаться разговорить":
            jump ss_d6unaedit


label ss_d6unaedit:
    $ d6_treasure = 2
    stop music fadeout 7
    me "Немало странных слов уже слыхал,"

    me "Но неприязнь к тебе не испытал."
    me "Коль хочешь - говори, не беспокойся."
    me "Я выслушать готов, спугнуть не бойся."
    play sound sfx_dinner_horn_processed
    window hide
    show un angry2 pioneer at cright
    with dspr
    window show
    th "Ну вот, гудок. Придётся задержаться."
    th "Я с Леною пытаюсь объясняться..."
    $ renpy.pause(1)
    play music music_list["confession_oboe"] fadein 5
    un "Моя загадка не даёт покоя?"
    un "Вот любопытство каково людское."
    me "Со мной несправедлива ты весьма,"
    me "Ты эту тему подняла сама."
    un "Зачем пришёл? Вот не было печали."
    un "Хотела лишь, чтоб от меня отстали."
    un "Являешься, на остров завлекаешь."
    window hide
    show un sad pioneer at cright
    with dspr
    window show
    un "Ведь мой это... наш остров, понимаешь?"
    un "И грёзы воплощались наяву,"
    un "Слетев с страницы книги, а затем..."
    un "...Очнулась. Прочь от острова плыву."
    un "Всё завершилось буднично. Ничем."

    show un cry pioneer at cright

    un "Так ты меня оставил без мечты"
    un "И, будто мало, что мой сон растоптан,"
    un "Живую душу, как под микроскопом,"
    un "Рассматривая, рвёшь на лоскуты."
    un "Всё забирай, я ль в силах отказать?"
    un "И нужно тебе было это знать?"

    me "На это вот что я тебе скажу..."
    stop music fadeout 7
    window hide
    show un sad pioneer at cright
    show mi normal pioneer at left
    with dspr
    window show
    mi "Секретничаем тут, я погляжу?"
    mi "Лен, ты сигнал на ужин пропустила,"
    mi "Вот за тобою я и заскочила."
    mi "Мне за столом одной неинтересно,"
    mi "Любой деликатес предстанет пресным."
    mi "Креветки были как-то раз, но толком..."
    window hide
    show mi dontlike pioneer at left
    with dspr
    window show
    mi "Да что вы на меня глядите волком?"
    mi "Моя ль вина - одну нам кашу варят."
    window hide
    show mi laugh pioneer at left
    with dspr
    window show
    mi "...Из старой недоеденной варганят."
    window hide
    show mi surprise pioneer at left
    with dspr
    window show
    mi "Ну ладно, так мы ужинать идём?"
    me "Да, мы минутой позже подойдём."

    show mi normal pioneer at left

    mi "Опаздывать нельзя, ведь стулья скоро"
    mi "На сцену из столовой понесут."
    mi "Иного запоздалого обжору"
    mi "Со стулом прямо и уволокут."
    mi "Нам так вожатая сейчас грозила."
    un "Тогда пошли уже, уговорила."
    window hide
    $ renpy.pause(1)
    hide mi
    hide un
    with dspr
    window show
    th "Как плохо вышло! С ней бы обсудить,"
    th "Да где б момент удачный улучить?"
    th "Пойду на ужин. Буду уповать,"
    th "Что сможет там себя в руках держать."
    window hide
    $ renpy.pause(1)
    jump ss_d6dinedit


label ss_d6dveedit:
    $ sp_dv += 1
    $ d6_dv_learned = 1

    scene bg int_house_of_mt_day with dissolve
    th "На пляже было весело всегда,"
    th "Чего бы не наведаться туда?"
    window hide

    $ persistent.sprite_time = 'day'
    scene bg ext_beach_day
    with dissolve

    play ambience ambience_lake_shore_day fadein 3

    window show

    th "Алиса и Ульянка тут как тут."
    th "Боюсь, что правда с пьесой подведут."
    window hide
    scene cg d4_us_cancer with dissolve
    $ renpy.pause(1)
    window show
    play music music_list["lightness"] fadein 7
    me "Баклуши бьёте? Как же ваши роли?"
    me "Про них-то вы уже забыли, что ли?"
    us "Да ну! Фотографическая память!"
    # Н.: Графическая память - это, знаете ли, немного другое) Гугл подскажет.
    us "Всё выучила, нечего горланить."
    window hide
    scene bg ext_beach_day
    show us normal swim at cright
    show dv smile swim at cleft
    with dissolve
    window show
    dv "Мне вовсе ничего учить не нужно."
    dv "Раз роль с листка - к чему зубрить натужно?"
    me "Да как к чему? Чтоб роль свою узнать."
    me "И не пришлось все реплики читать,"
    me "С бумаги глаз совсем не поднимая."
    me "Тебя саму устроит роль такая?"
    dv "Ну ладно, поглядим. Листки с собой,"
    dv "На камень положила здесь большой..."
    window hide
    show dv guilty swim at cleft
    with dspr
    window show
    me "На берегу листков нет, но в воде"
    me "Флотилия корабликов бумажных"
    me "Из гавани выходит строем важно"
    me "В кильватере у парня на плоте."
    window hide
    show dv angry swim at cleft
    show us angry swim at cright
    with dspr
    window show
    dv "Взгляд отведёшь - так кто-нибудь нашкодит!"
    us "Украл у нас и по воде уходит?"
    us "Раскается в нахальстве небывалом!"
    window hide
    show dv normal swim at cleft
    hide us
    with dspr
    window show
    "Ульянка - за бумажным адмиралом."
    dv "Потерян текст, нет смысла напрягаться."
    me "Есть вариант за копией смотаться."
    dv "На сцену? Нет уж, мне не рады там."
    me "Я мог бы принести тебе и сам,"
    me "Когда бы видел хоть чуток отдачи."
    #dv "Чтоб отдалась, мечтаешь, не иначе."
    me "Ведь тоже в реку канет, не иначе."
    dv "Нет, я берусь за дело, обещаю."
    me "Сидеть тут смысла я не ощущаю."
    me "Пойдём, свою я копию отдам."
    window hide
    show dv angry swim at cleft
    with dspr
    window show
    dv "Ульянка эта... Ох я ей задам!"
    dv "Ульяна! Ну-ка, марш сюда, дурёха!"
    window hide
    show us smile swim at cright
    with dspr
    window show
    us "Отделан этот кадр весьма неплохо."
    dv "Чем ты его отделала при этом?"
    window hide
    show us dontlike swim at cright
    with dspr
    window show
    us "Чем под руку попалось. Вот, пакетом..."

    dv "Пакет - с одеждой нашей, охламонка!"
    dv "Всё мокрое, как в лагерь мы пойдём?"

    show us grin swim at cright

    us "Пойдём, похоже, словно амазонки!"

    show us normal swim at cright
    show dv guilty swim at cleft

    us "Зазорно? Путь окольный мы найдём..."
    me "Алиса всё же своего добилась -"
    me "От дела скучного освободилась."
    me "Теперь придётся вам сушить одежду."
    us "Всё высушу, чем рот откроешь прежде!"
    dv "Пошли, отдашь мне текст, как обещал."
    me "В купальнике?.. Вот смелость же. Не ждал!"
    window hide
    show dv angry swim at cleft
    with dspr
    window show
    dv "А ну, оставь злорадные замашки!"
    stop ambience fadeout 2
    dv "У вас я только захвачу бумажки."
    window hide
    scene bg ext_house_of_mt_day
    show dv guilty swim
    with dissolve
    window show
    play ambience ambience_day_countryside_ambience fadein 2
    dv "Вот ерундой какой-то занялась."
    dv "Теперь шнырять по лагерю, таясь."
    me "В чём в лагере отличие от пляжа?"
    stop ambience fadeout 2
    dv "Отличий нет, но всё-таки... Не важно."
    me "Меня вот тоже это поражает,"
    me "На пляже люди будто стыд теряют."
    me "Но стоит от него лишь отойти,"
    me "Как можно стыд обратно обрести."
    window hide
    play ambience ambience_int_cabin_day fadein 2
    scene bg int_house_of_mt_day
    show dv normal swim
    show mystical_box:
        pos (1348,0)
    with dissolve
    $ renpy.pause(1)
    window show
    me "Вот копии."
    th "Откуда столько много?"
    me "Вторую можешь для Ульянки взять."
    me "А хочешь, можем вместе почитать?"
    me "Нелишнее - быть в курсе диалога."
    dv "Ты головой подумай, идиот."
    dv "Занятие - под стать актёров виду!"
    dv "А ну сюда вожатая зайдёт?"
    dv "Так лучше сразу хлопнуть цианиду."
    me "Ну, можешь завернуться в одеяло..."
    stop music fadeout 5
    dv "Чтоб сразу Ольге всё понятно стало."
    dv "Нет уж, спасибо, я пойду домой."
    play sound sfx_knock_door7_polite
    window hide
    show dv scared swim
    show mystical_box:
        pos (1348,0)
    with dspr
    window show
    "Раздался в двери чей-то стук глухой."
    "Алиса тут же перешла на шёпот:"
    dv "Да стой ты, сразу же услышат топот!"
    dv "Не открывай совсем. Нас нету дома."
    window hide
    hide dv
    with dspr
    window show
    "Сама ж - к двери, желанием ведома"
    "Через замок увидеть визитёра."
    window hide
    $ renpy.pause(1)
    show dv sad swim
    show mystical_box:
        pos (1348,0)
    with dspr
    window show
    dv "Саманта там. Она что, за вахтёра?"
    dv "Она сидит в шезлонге у крыльца."
    dv "Как я уйду?"
    me "Ты видишь тут проблему?"
    me "Не стоит выеденного яйца:"
    me "Саманта - это даже ведь не Лена."
    play music music_list["take_me_beautifully"] fadein 7
    dv "Давай ты уведёшь её подальше?"
    me "Неловко мне: на стук не отозвался."
    me "Врать не хочу, у ней чутьё на фальшь же."
    window hide
    show dv angry swim
    show mystical_box:
        pos (1348,0)
    with dspr
    window show
    dv "Болван! Невинной лжи он испугался."
    dv "Сидим до появления вожатой?"
    me "Да, в ситуации мы глуповатой..."
    me "И Ольга тоже редко днём заходит."
    window hide
    show dv normal swim
    show mystical_box:
        pos (1348,0)
    with dspr
    window show
    dv "И где она тогда обычно бродит?"
    dv "С делами, про которые твердит?"
    me "Кто знает? Может, просто где-то спит."
    dv "Тогда бы ей пока не просыпаться."
    dv "В истории нам светит оказаться."
    window hide
    show dv  grin swim
    show mystical_box:
        pos (1348,0)
    with dspr
    window show
    dv "Я знаю, как вопрос нам разрешить!"
    dv "Изволь твою мне форму одолжить."
    me "Но форма у меня одна лишь, вроде."
    me "Могу тебе другой одежды дать."
    dv "Другая по легенде не подходит:"
    dv "Скажу, что в этом собралась играть."
    dv "Раз выступать в мужской придётся роли,"
    dv "Мне для вживанья в роль потребна форма."
    dv "Старинной нет, хотя моя будь воля..."
    dv "Но - нет, ношу обычную покорно."
    me "Да, пьеса небогата на костюмы..."
    me "Тут будешь ты, похоже, на коне."
    me "Последняя проблема гложет ум мой:"
    me "Как объяснить сам твой приход ко мне?"
    window hide
    show dv guilty swim
    show mystical_box:
        pos (1348,0)
    with dspr
    window show
    dv "Ой, да зачем так глубоко копать?"
    dv "Саманте и не нужно это знать."
    dv "Не пожалел ты формы для искусства."
    dv "К вещам не так привязан пионер!"
    dv "А с ней у вас языковой барьер -"
    dv "Неловкости прикроет он искусно."

    me "А мне без формы ты ходить предложишь?"
    window hide
    show dv normal swim
    show mystical_box:
        pos (1348,0)
    with dspr
    window show
    dv "Зачем? Пока сидеть хоть голым можешь."
    dv "Вожатая вернётся и найдёт."
    dv "Не стой! Дождёмся, что она придёт."
    th "С утра раздеть пытается упорно,"
    stop music fadeout 3
    th "Предлог нашла вот - отнимает форму."
    me "Поспорила ты с кем-то или как?"
    window hide
    show dv angry swim
    show mystical_box:
        pos (1348,0)
    with dspr
    window show
    play music music_list["heather"] fadein 1
    dv "Ну ты, Семён... Какой же ты дурак!"
    window hide
    show dv angry swim  close
    with dspr
    window show
    me "Куда с меня ты галстук тянешь? {w}Ай!" with hpunch
    window hide
    show dv rage swim tie
    show mystical_box:
        pos (1348,0)
    with dspr
    window show
    dv "Так! Живо остальное всё снимай!"
    th "Её такая сцена не смущает?"
    window hide
    $ renpy.pause(1)
    show dv shy swim tie
    show mystical_box:
        pos (1348,0)
    with dspr
    window show
    th "Ну вот же. Наконец-то понимает."
    window hide
    show greysation with dissolve
    show lildevil with dissolve:
        pos (200,200)

    $ renpy.pause(1)
    window show
    dev "В делах любовных ты не чемпион,"
    dev "Добыча у тебя в руках, Семён!"
    dev "Она не будет больше так доступна,"
    dev "Такие шансы упускать преступно!"
    th "Она же не пришла сюда за этим?"
    dev "Довольно вам вести себя как дети."
    dev "Зачем пришла - она сама не знает,"
    dev "Чего-то подсознательно желает."
    th "А ангел где? Хочу его совет!"
    dev "Я тут один, второго парня нет."
    dev "Немножко ты и так у нас контужен,"
    dev "Так что второй советчик и не нужен."
    dev "И без него ты шансы все упустишь,"
    dev "Веселье в жизни главное пропустишь."
    th "Не вижу для веселья вариантов."
    th "Сейчас мы как бы в домике вожатой,"
    th "А за стеною юная Саманта..."
    th "Совсем не обстановка для разврата."
    dev "Не вижу тут этической дилеммы,"
    dev "Плохое место ж - не мои проблемы."
    dev "Надеюсь, не упустишь эту кису."
    window hide
    hide greysation
    hide lildevil
    with dissolve
    $ renpy.pause(1)
    window show
    th "Придётся мне послушаться Алису."
    th "Теперь приходит мой черёд смущаться,"
    th "Неловко перед нею раздеваться."
    me "Твоя взяла, сейчас получишь форму."
    stop music fadeout 5
    me "Не вздумай ржать, накроюсь одеялом."
    dv "Да накрывайся саваном хоть чёрным,"
    dv "Смотреть стриптиз я вовсе не желала."
    window hide
    scene black with fade
    scene bg int_house_of_mt_day
    show dv shy swim tie
    show mystical_box:
        pos (1348,0)
    with fade
    window show
    "Едва остался в майке и трусах,"
    play music music_list["pile"] fadein 3
    "Звук с улицы ввергает в жуткий страх."
    window hide
    show dv shocked swim tie
    show mystical_box:
        pos (1348,0)
    with dspr
    window show

    "То Ольги, несомненно, голос был."
    th "Как вовремя Алисе услужил!"
    "От ужаса и та оцепенела."
    th "Надеюсь, что одеться-то успела."
    "Хотя особой разницы не будет,"
    "Вожатая по-своему рассудит."
    window hide
    show dv shy swim tie
    show mystical_box:
        pos (1348,0)
    with dspr
    window show

    "В моём уме уж каждый миг просчитан,"
    "Что чрез секунды тут произойдёт:"
    "Меня завидит Ольга с сеньоритой..."
    "И шум сюда Саманту приведёт."
    "Отправимся тогда мы на Голгофу."
    "За что же мне такая катастрофа?"
    stop music fadeout 5
    ss "{en}Come with me please.{/en}{ru}Могли бы вы сейчас со мной пойти?{/ru}"
    mt "Да, что такое? Мне к тебе зайти?"
    window hide
    show dv surprise swim tie
    show mystical_box:
        pos (1348,0)
    with dspr

    window show
    dv "Удачно как, не верю. Правда, что ли?"
    window hide
    hide dv
    with dspr
    window show
    $ renpy.pause(1.2)
    stop ambience fadeout 2
    dv "Мы спасены! Теперь - скорей на волю!"
    window hide
    scene bg ext_house_of_mt_day
    play ambience ambience_day_countryside_ambience fadein 2
    show dv shy swim tie
    with dissolve
    window show
    me "Свободен путь. Сэм совершила чудо."

    dv "Тут больше ни секунды не пробуду."
    window hide
    hide dv
    with dspr
    window show
    "Алиса удирала без оглядки,"
    "Мою зачем-то форму прихватив."
    "А я скорей разыскивал манатки,"
    "Возврат вожатой этим упредив."
    play music music_list["lightness"] fadein 5
    window hide
    scene bg int_house_of_mt_day
    show mystical_box:
        pos (1348,0)

    with dissolve
    window show
    play ambience ambience_int_cabin_day fadeout 2
    "После экспресс-ревизии на полках"
    "Нашёл вполне приличную футболку -"
    "Картина не совсем уж безысходна."
    th "Штаны ли, джинсы, боже, что угодно,"
    th "Иначе буду в домике пленён."
    window hide
    play sound sfx_open_door_2
    show mt normal panama pioneer at cleft with dspr
    window show
    mt "Не видел щётку для волос, Семён?"
    mt "А ты зачем мою напялил блузку?"
    mt "И как она не оказалась узкой..."
    th "Я бы ответил как, да жить охота..."
    th "В такой не будет тесно бегемоту."    
    me "...Для образа ищу я варианты."
    me "А блузка оттенит мои таланты?"
    mt "Сойдут колготы и любой жакет."
    mt "И в довершение всего - берет."
    me "Я тот момент сейчас переживаю,"
    me "Которых в жизни барышень - не счесть."
    me "Есть в гардеробе всё, как по желанью,"
    me "Но СОВЕРШЕННО нечего надеть!"
    mt "Схожу я в рукоделия кружок,"
    mt "Авось найдём что для тебя, дружок."
    me "Ещё... Немножко форма замаралась"
    me "И неудачно в стирке оказалась."
    me "Нельзя ли сменную мне получить?"
    mt "Конечно, можно. Мог бы не просить."
    me "Стащил бы, да не знаю, где искать."
    mt "Залезть не догадался под кровать?"
    mt "Она там вся лежит в углу, в коробке."
    mt "Любой размер найдёшь ты в этой стопке."
    window hide
    hide mt
    with dspr
    window show
    "Ушла она, и я переоделся."
    "Да на кровать, расслабившись, уселся."
    play sound sfx_paper_bag
    "Тут подо мною хрустнула бумага."
    th "Алиса текст забыла, бедолага."
    stop music fadeout 5
    "Решил исправить эту я оплошность:"
    stop ambience fadeout 2
    th "Листки ей отнесу, явив дотошность."
    window hide
    scene bg ext_house_of_mt_day
    play ambience ambience_day_countryside_ambience fadein 2
    show ss nosmile casual
    with dissolve
    $ renpy.pause(1)
    window show
    play music Hide_and_seek fadein 3
    me "{en}You really like our lounger, don't you?{/en}{ru}Я вижу, полюбился наш лежак?{/ru}"
    ss "{en}I wish my house had one too...{/en}{ru}У моего крыльца нет, как-никак.{/ru}"

    show ss unsure casual
    ss "{en}My presence must be bothering for you? I'm sorry.{/en}{ru}Мешаю, здесь я сидя? Извини.{/ru}"
    me "{en}Of course it's not. No, not at all. Don't worry.{/en}{ru}Конечно, нет. Коль хочешь, тут живи.{/ru}"
    window hide
    show ss shy casual
    with dspr
    window show
    ss "{en}I actually saw you two. Not sure if I should say it...{/en}{ru}Вас видела с Алисой, вот дела...{/ru}"
    me "{en}So you distracted Olga to let poor Alice quit?{/en}{ru}Так Ольгу ты нарочно увела?{/ru}"
    ss "{en}Don't thank me, it was me who trapped you,{/en}{ru}Нет, не благодари меня о том.{/ru}"
    if persistent.ss_text_lang == "ru":
        ss "{ru}Из-за меня ловушкою стал дом.{/ru}"
    ss "{en}But in the end I meant no harm, and did what had to do.{/en}{ru}В конце концов, я не желала зла{/ru}"
    if persistent.ss_text_lang == "ru":
        ss "{ru}И сделала то, что была должна.{/ru}"
    show ss unsure casual
    th "Наверное, обидел я Саманту"
    th "И стал за то на время арестантом."
    me "{en}I hope you got it right that there was nothing embarrasing.{/en}{ru}Надеюсь, ты прекрасно поняла:{/ru}"
    if persistent.ss_text_lang == "ru":
        me "{ru}Там нечему смущать твоё вниманье.{/ru}"
    me "{en}But how it looked might have brought a misunderstanding.{/en}{ru}Хоть вызвать ситуация могла...{/ru}"
    if persistent.ss_text_lang == "ru":
        me "{ru}Твоё большое недопониманье.{/ru}"

    show ss normal casual

    ss "{en}Forget it. So where are you going now?{/en}{ru}Забудем. А теперь куда собрался?{/ru}"
    me "{en}I have to bring these to Alisa anyhow...{/en}{ru}Алисин текст так в доме и остался.{/ru}"
    me "{en}Care to join me on this wondrous adventure?{/en}{ru}Закончим приключенье вместе спешно?{/ru}"
    show ss smile2 casual   with dspr
    ss "{en}If I am not disturbing you... Then yes, with pleasure.{/en}{ru}Коль скоро не мешаю - то конечно.{/ru}"
    window hide
    $ persistent.sprite_time = 'day'
    scene bg ext_house_of_dv_day
    show ss nosmile casual at cright
    with dissolve
    play sound sfx_knock_door2
    $ renpy.pause(2)
    window show
    ss "{en}Nobody is at home or she's not opening again.{/en}{ru}Похоже, никого опять нет дома.{/ru}"
    if persistent.ss_text_lang == "ru":
        ss "{ru}Иль дверь решила мне не открывать.{/ru}"
    me "{en}It looks like our adventure goes in vain...{/en}{ru}Пожалуйста, давай не будем снова...{/ru}"
    if persistent.ss_text_lang == "ru":
        me "{ru}Кого-либо так с ходу обвинять.{/ru}"
    ss "{en}To make adventure worthy, you must say now something clever.{/en}{ru}Чтоб стоило чего-то приключенье,{/ru}"
    if persistent.ss_text_lang == "ru":
        ss "{ru}Блесни хотя бы умозаключеньем.{/ru}"
    me "{en}I will, I will... Well... I will say... {/en}{ru}Ну... как это. Погода хороша?{/ru}"
    if persistent.ss_text_lang == "en":
        extend "{en}Nice weather?{/en}"
    ## Колодец. И погода ничего. бгг
    th "Освоила сарказм, моя душа?"
    window hide
    stop music fadeout 5
    show dv guilty shorts at cleft
    show ss smile2 casual at cright
    with dspr
    window show
    dv "Ну ладно, признаюсь уже. Я дома."
    dv "Шептаться надо бы потише грома."
    dv "Явились отношенья выяснять?"
    me "Хм, вовсе нет. Вот, текст тебе отдать..."
    dv "Ах, текст... Вот это глупо получилось."
    window hide
    show dv normal shorts at cleft with dspr
    window show
    stop ambience fadeout 2
    dv "Ко мне тогда зайдёте, раз явились?"
    window hide
    scene bg int_house_of_dv_day
    play ambience ambience_int_cabin_day fadein 2
    show dv normal shorts  at cleft
    show ss normal casual at cright
    with dissolve
    $ renpy.pause(1)
    window show
    ss "{en}I like this room guys. But where is Ulyana?{/en}{ru}О, классный домик. Только где Ульяна?{/ru}"
    dv "Да где-нибудь опять буянит рьяно."
    ss "{en}I'm sorry, what's this?{/en}{ru}А это что?{/ru}"
    dv "Любимая открытка."
    window hide
    play music music_list["doomed_to_be_defeated"] fadein 0
    show redfeb with dissolve:
        pos (150,450)


    $ renpy.pause(1.5)
    window show
    me "Смешно, Алис. Хорошая попытка."
    window hide
    hide redfeb
    show ss serious casual at cright with dspr
    with dspr
    $ renpy.pause(0.5)
    window show
    dv "О чём ты?.."
    window hide
    show dv angry shorts at cleft with dspr
    window show
    dv "...Так ведь это не она!"
    dv "Ульянка, вот малая сатана!"
    show ss unsure casual at cright with dspr
    me "Вот честно, небольшая важность - чьё..."
    dv "Предупредил бы - спрятали б её."
    me "К тебе не набивались мы, поверь мне."
    stop music fadeout 5
    dv "Ну... без Саманты тёрся б ты у двери!"
    window hide
    show ss sad casual at cright
    show dv guilty shorts  at cleft
    with dspr
    window show
    ss "{en}All right, guys. I feel this is a wrong time for a visit,{/en}{ru}Не время для гостей, ребят, наверно...{/ru}"
    if persistent.ss_text_lang == "ru":
        ss "{ru}Да и дела имеются вокруг.{/ru}"
    ss "{en}I have to leave now, not to spoil the spirit.{/en}{ru}Не буду вам надоедать чрезмерно,{/ru}"
    if persistent.ss_text_lang == "ru":
        ss "{ru}Чтоб не травить товарищеский дух.{/ru}"
    show ss unsure casual at cright
    ss "{en}I know you need to practice so go on,{/en}{ru}Вы отточить спешите мастерство:{/ru}"
    ss "{en}You better hurry up before the time is gone.{/en}{ru}До пьесы времени чуть-чуть всего.{/ru}"
    window hide
    hide ss with dspr
    window show
    dv "Куда? Из-за открытки всё проклятой?"
    me "Что? Сэм - из-за открыток истерить?.."
    dv "Ушла же и смотрелась мрачновато."
    me "Ушла, чтоб дать нам роли поучить."
    dv "Оставила вдвоём нас, не пойму..."
    me "Ты подоплёки не ищи тому,"
    me "Не знаю, что тебя так в этом гложет:"
    me "Сама она с тобой читать не может."
    window hide
    show dv normal shorts at cleft with dspr
    window show
    dv "С чего ты взял, что я {i}с тобой{/i} хотела?"

    menu:
        "Остаться":
            jump ss_d6dv2edit
        "Уйти":
            th "Намёк простой. Алису оставляю,"
            th "Коль скоро та ко мне мне не подобрела."
            th "Незваным гостем быть я не желаю."
            me "Пожалуй, догоню-ка я Саманту."
            stop ambience fadeout 2
            dv "Иди давай, вы, как-никак, команда."
            window hide
            scene bg ext_house_of_mt_day with dissolve
            play ambience ambience_day_countryside_ambience fadein 2
            "Снаружи уж Саманты след простыл,"
            "До дома не догнал, хотя спешил."
            th "И дома тоже нет. Куда свернула?"
            stop ambience fadeout 2
            th "Выходит, из-под носа улизнула."
            th "Ну ладно, в лагере не пропадёт."
            th "Понадоблюсь - всегда меня найдёт."
            window hide
            scene bg int_house_of_mt_day
            show mystical_box:
                pos (1348,0)
            with dissolve
            play ambience ambience_int_cabin_day fadein 2

            $ renpy.pause(1)
            window show
            th "Коль скоро ничего не происходит,"
            th "Разумным будет снова глянуть роли."
            window hide
            menu:
                "Пройтись по своей роли":
                    jump ss_d6urdedit
                "Читать чужие":
                    jump ss_d6escedit





label ss_d6dv2edit:
    $ d6_dv_learned = 2
    dv "Ну ладно, раз пришёл, давай за дело."
    "На текст как будто в первый раз глядела,"
    "Назад-вперёд растерянно листала."
    me "Акт первый, сцена пятая сначала."
    dv "Да знаю я! Нашла уж, что хотела."
    dv "{i}Мне показалось, голосом - Монтекки.{/i}"
    dv "{i}Мальчишка, шпагу! Этот негодяй{/i}"
    dv "{i}Осмелился пробраться к нам под маской{/i}"
    dv "{i}В насмешку над семейным торжеством!{/i}"
    me "{i}Мой дорогой, зачем ты поднял крик?{/i}"
    dv "{i}У нас Монтекки! Как он к нам проник?{/i}"
    me "{i}Ты о Ромео?{/i}"
    dv "{i}О дрянном Ромео.{/i}"
    me "{i}Приди в себя. Что ты к нему пристал?{/i}"
    me "{i}В гостях надутость эта неуместна.{/i}"
    dv "{i}Нет, к месту, если лишние в гостях. Я не снесу...{/i}"
    play sound sfx_open_door_clubs
    window hide
    show us normal sport at cright with dspr
    window show
    play music music_list["eat_some_trouble"] fadein 2
    us "Какой-такой, какой-такой Ромео?"
    us "А, это в гости к нам Семён проник."
    us "Я форму нашу высушить сумела!"
    us "Только прожгла немного воротник..."
    window hide
    show dv angry shorts at cleft with dspr
    window show
    us "И кстати, на твоей рубашке тоже."
    us "Пришлось улики там же закопать,"
    us "Иначе получилось бы негоже,"
    us "Могло за порчу шмоток нам попасть."
    us "Ну ладно, с вами некогда трепаться."
    stop music fadeout 7
    us "Я удочку беру и удаляюсь!"
    us "Мне важным делом нужно заниматься."
    us "Освободиться к пьесе постараюсь."

    window hide
    hide us with dspr
    $ renpy.pause (0.6)
    show dv sad shorts at cleft
    with dspr
    window show
    dv "Зачем ей разрешила форму взять?"
    dv "Да... что теперь... Продолжу я читать."

    dv "{i}Ромео, сущность чувств моих к тебе{/i}"
    dv "{i}Вся выразима в слове: ты мерзавец.{/i}"
    play music music_list["take_me_beautifully"] fadein 7
    me "{i}Тибальт, природа чувств моих к тебе{/i}"
    me "{i}Велит простить твою слепую злобу.{/i}"
    me "{i}Я вовсе не мерзавец. Будь здоров.{/i}"
    me "{i}Я вижу, ты меня совсем не знаешь.{/i}"
    window hide
    show dv shy shorts at cleft with dspr
    window show
    dv "{i}Словами раздраженья не унять,{/i}"
    dv "{i}Которое всегда ты возбуждаешь.{/i}"
    #th "Ну, иногда ты возбуждаешь тоже!"
    window hide
    $ renpy.pause(1)
    show greysation
    show lildevil:
        pos (1100,200)
    with dissolve
    window show
    dev "Тогда пора его унять и делом!"
    dev "Пускай и преизрядно неуклюжим..."
    dev "Неужто роль учить не надоело?"
    dev "Я. Снова. По вопросу всё - тому же."
    dev "Теперь, раз обстановка изменилась,"
    dev "Вполне укромно вы уединились..."
    th "Вдруг выгонит? То было бы ужасно,"
    th "Поймёт превратно... Кстати, не напрасно."
    th "А ну как от поспешного решенья"
    th "Хорошее разрушу отношенье?"
    dev "Ты отношений захотел, глупец?"
    dev "Не скромный принц ей нужен, а наглец!"
    dev "Не рушит ничего в любви отвага:"
    dev "Дорогу не пройти, не сделав шагу,"
    dev "А камни на дороге неизбежны."
    dev "И что теперь, сидеть себе в надежде?"
    dev "Нет, трусость шансы те не повышает,"
    dev "Скорее, только напрочь их лишает."
    dev "Лишь то разрушишь, что себе надумал."
    dev "А в жизни выдумок и вовсе нет."
    dev "Жалеешь о несбывшемся угрюмо?"
    dev "Будь мужиком. Такой тебе совет."
    window hide
    hide greysation
    hide lildevil
    with dissolve
    $ renpy.pause(1)
    window show
    dv "...Ну что?"
    me "Кхм...{w} {i}Неправда, я тебя не обижал.{/i}"
    play sound sfx_dinner_horn_processed
    "Тут горна зов (в столовую) застал."
    window hide
    show greysation
    show lildevil
    with dspr
    window show
    dev "Да ладно! Снова ничего? Серьёзно?"
    dev "Со скуки с вами и подохнуть можно."
    window hide
    hide greysation
    hide lildevil
    with dspr
    $ renpy.pause(1)
    window show
    me "{i}Расстанемся друзьями, Капулетти!{/i}"
    me "{i}Едва ли знаешь ты, как дорог мне.{/i}"
    window hide
    show dv normal shorts  at cleft with dspr
    window show
    dv "Ну и понятно, подрались те двое,"
    dv "Друг друга убивали, всё такое."
    dv "И можем мы на этом, стало быть,"
    dv "На ужин с чистой совестью отбыть."

    window hide
    $ renpy.pause(1)
    jump ss_d6dinedit




label ss_d6dinedit:
    play music Del_mare fadein 8
    $ day_time()
    $ persistent.sprite_time = "day"
    scene bg ext_dining_hall_near_day
    show sl normal pioneer
    show dv guilty shorts at fleft
    with fade
    window show
    play ambience ambience_day_countryside_ambience fadein 2
    sl "Алиса в шортах? Это что за вид?"
    dv "Не порти, Славя, людям аппетит."
#play music Hide_and_seek fadein 3
    dv "Подходишь и ворчишь, идёшь и ноешь."
    dv "Зачем опять на связь со мной выходишь?"
    dv "Тебе полезней, говорю без фальши,"
    dv "Держаться от меня как можно дальше."
    show sl serious pioneer
    sl "Должна отстать я от тебя, подруга?"
    sl "Позволь спросить, с какого перепугу?"
    sl "С тобою мы пока ещё не квиты,"
    sl "Твои слова - и вовсе не забыты."
    show dv angry shorts at fleft
    dv "Зато твои не значат ничего,"
    dv "Ты угрожаешь снова, что с того?"
    dv "Чем подтвердить свои слова делами,"
    dv "Способна только скрежетать зубами."
    me "Пожалуйста, не надо провокаций."
    me "Закончить день бы без враждебных акций."
    show dv normal shorts  at fleft
    dv "Не бойся, я сама оплот порядка."
    dv "Ведь знает: воевать со мной несладко."
    dv "И поведёт себя вполне прилично,"
    dv "Поддержит снова имидж свой привычный."
    sl "Но ты на это не надейся лучше,"
    sl "Ведь час уже твой близок злополучный."
    show dv angry shorts at fleft
    dv "Скажи ей, чтоб ко мне она не лезла."
    me "Уже не так ты стала безмятежна?"
    me "Меня теперь в защиту призываешь?"
    dv "Никак, ты за неё? Смотри, как знаешь."
    window hide
    stop ambience fadeout 2
    $ renpy.pause(0.9)
    scene bg int_dining_hall_people_day
    show mt normal pioneer at fleft
    show ss nosmile casual at fright
    show sl normal pioneer
    play ambience ambience_dining_hall_full fadein 2
    with dissolve
    window show
    "В столовой необычная картина:"
    "Нам вместо вилок всем раздали ложки."
    "А ложки эти - к жареной свинине"
    "В объятиях зелёного горошка."
    "Саманта не особо удивилась,"
    "Ко всякому уже приноровилась."
    sl "Сойдут и ложки. Мы же не царицы."
    me "Всех с праздником! Тюремных день традиций!"
    mt "Сегодня день Уильяма Шекспира,"
    mt "И пьеса - чем не повод нам для пира?"
    me "А что Шекспир? Ведь даже ходят слухи,"
    me "Он бездарь был, успешный в показухе,"
    me "И тексты вышли под пером чужим."
    me "Тем гением был некий аноним."
    show ss surprise casual at fright
    ss "{en}Why do you think that he was fake?{/en}{ru}С чего ты взял, что сам он был фальшивкой?{/ru}"
    me "{en}He couldn't spell his name without mistake.{/en}{ru}Он делал в имени своём ошибки.{/ru}"
    show ss normal casual at fright
    me "{en}Such a man can't be that good with words,{/en}{ru}Неслыханно для мастера такого,{/ru}"
    me "{en}Leave that to noble kings and lords.{/en}{ru}Должно быть, нам подсунули другого.{/ru}"
    show ss smile2 casual at fright with dspr
    mt "Но ведь \"Шекспир\" - всего одно лишь слово,"
    mt "Что автора тех пьес обозначает."
    mt "Ну обзовёшь его ты кличкой новой,"
    mt "Что это для тебя вдруг поменяет?"
    mt "Сей парадокс несложно разрешим:"
    mt "Воспринимай его как псевдоним."
    show sl smile pioneer
    sl "Аль ошибался мастер смеха ради."
    sl "Ходили ведь и шутки среди знати?"
    me "Тогда та шутка плохо обернулась,"
    me "Кой у кого фантазия проснулась."
    stop music fadeout 4
    me "Кто знает, до чего она дойдёт,"
    me "Сто лет спустя кто правду разберёт?"
#    sl "А современный автор пусть на ус мотает,"
#    sl "Сегодня неуча, смеясь, изображает,"
#    sl "А для потомков может имя запятнать."
#    sl "А то и сможет кто его наследие украсть."

    if d6_treasure > 0:
        mt "О прошлом, кстати! Что же я сижу?"
        mt "Свой старый слайд сейчас вам покажу."
        "Я рассказал, как мы его добыли."
        "Жалели все, что с нами не ходили."

    mt "О, кстати, Славя и Семён, ребята!"
    mt "Костюмы обещала вам когда-то,"
    mt "Они вас ждут, сейчас я принесу."
    show sl angry pioneer
    "С досадой Славя дёрнула косу."
    sl "Сидите, я сама могу сходить."
    mt "Нет-нет, не нужно, Славя! Не вставай."
    mt "Скорей актёров нужно отпустить,"
    mt "А я могу потом допить свой чай."
    show sl sad pioneer
    show ss nosmile casual at fright
    mt "А вы быстрее ешьте, время ценно,"
    mt "Поторопиться надо, вас ждет сцена!"
    window hide
    hide mt with dspr
    $ renpy.pause(1)
    window show
    play music RS1 fadein 4
    me "Могла бы ты Алису пощадить?"
    me "С возмездием чуток повременить?"
    if d6_slchoice == 1:
        show sl surprise pioneer
        sl "Ты в этом помогать мне вызывался!"
        sl "Неужто от возмездья отказался?"
        me "Я думал, только как-нибудь подшутим:"
    else:
        sl "Я думала, ты мне протянешь руку."
        sl "Не помешало бы развеять скуку..."
        me "Готов, не злобно если мы пошутим:"

    me "Быть может, в доме лампочку открутим,"
    me "Бумажки будем клеить всем на спины,"
    me "Чернил нальём в стиральные машины..."
    if d6_slchoice == 1:
        me "Но поощрять открытую вражду"
        me "Меж вами не хотел, имей в виду."
    else:
        me "Но поощрять открытую вражду -"
        me "Такого вовсе не имел в виду."
    show sl serious pioneer
    sl "Раз так, тогда и вовсе ты свободен."
    sl "Такой помощник ни на что не годен."
    show ss unsure casual at fright
    sl "Дурачиться уже охоты нету,"
    sl "Пока не проучу мерзавку эту."
    window hide
    $ renpy.pause(1)
    show sl normal pioneer with dspr
    window show
    sl "А вид у вас с Алисой - обожаю!"
    sl "По беспокойству на лице читаю:"
    sl "\"Ах, что же эта Славя замышляет?\""
    sl "\"Или впустую только нас пугает?\""
    sl "За пьесу можешь быть спокоен, ладно."
    sl "Её сорвать в мои не входит планы."
    me "На ладан дышит всё, никак иначе."
    me "Сорвать она не хочет. Просто плачу!"
    show sl serious pioneer
    me "Мы брифинг тратим на твои проблемы,"
    me "Случись что - будет стыдно перед всеми."
    show ss surprise casual at fright
    ss "{en}What are you arguing about? Go on, translate!{/en}{ru}В чём спор? Не забывай переводить!{/ru}"
    ss "{en}I want to know what's going on, just say it straight.{/en}{ru}И говори как есть, хорош темнить.{/ru}"
    show ss serious casual at fright with dspr
    menu:
        "Выдать Славю":
            $ sp_ss += 1
            $ sp_sl -= 2
            me "{en}Our Slavya is just driven by ugly malice,{/en}{ru}Славяна замышляет злодеянье,{/ru}"
            me "{en}Whatever cost it takes, she wants to punish Alice.{/en}{ru}Ведь жаждет для Алисы наказанья.{/ru}"
            show ss surprise casual at fright
            ss "{en}I can't believe it! What the matter?{/en}{ru}Не верится. И как так получилось?{/ru}"
            ss "{en}You're such nice girls, I saw no better.{/en}{ru}Что между вами, девочки, случилось?{/ru}"
            show ss unsure casual at fright
            th "И пусть заставил я краснеть Славяну,"
            th "Пресечь скорее нужно эти планы."
            th "На пользу дела можно и соврать..."
            me "Саманта просит ей пообещать,"
            me "Что ты Алисе зла не причинишь,"
            me "Свои угрозы в жизнь не воплотишь."
            show sl angry pioneer
            sl "Раскрыл меня, развеял свой испуг?"
            sl "Предатель - вот ты кто. Паршивый друг."
            show ss sad casual at fright
            sl "Сама б Алиса ябедой не стала."
            sl "...Всерьёз вредить ей даже не желала."
            sl "Коль скоро так к бедняжке сердоболен,"
            sl "Теперь чертовски можешь быть доволен."
            me "С ней разделить могу любые мысли,"
            me "Держать её в неведенье нет смысла."
            me "Проблему эту можно ей доверить."
            me "Прости, но пыл твой нужно поумерить."
            sl "Еда доедена, питьё допито,"
            sl "Друзья, приятнейшего аппетита."
            window hide
            hide sl with dspr
            $ renpy.pause(1)
            window show
            th "Вот показуха. К сцене нам пора,"
            stop music fadeout 4
            th "Пока не набежала детвора."
            window hide
            $ renpy.pause(1.0)


        "Не выдавать":
            me "{en}There's nothing to be that concerned about, relax.{/en}{ru}Твержу, актёры к пьесе не готовы,{/ru}"
            if persistent.ss_text_lang == "ru":
                me "{ru}А Славя возражает каждый раз.{/ru}"
            me "{en}It's about things that play still lacks.{/en}{ru}История сия совсем не нова,{/ru}"
            if persistent.ss_text_lang == "ru":
                me "{ru}Не обращай внимания на нас.{/ru}"
            show ss unsure casual at fright
            me "Тебя, возможно, стоит выдать, Славя."
            me "Глядишь, она тебе мозги поправит?"
            show sl normal pioneer
            sl "Не впутывай Саманту в те дела,"
            sl "Смотри: она и так невесела."
            sl "Чем ссориться, узнай уж лучше пусть,"
            sl "Что причиняет ей такую грусть?"
            me "Ей счастья от вопросов тех не будет,"
            me "На сцене о невзгодах позабудет."
            sl "Пожалуй, что и верно рассуждаешь."
            sl "А в чём тогда проблема? Сам не знаешь?"
            me "Ты это хочешь обсудить при ней?"
            me "Раз я тебя не выдал, не наглей."
            sl "В дела соваться ваши не желала..."
            window hide
            $ renpy.pause(0.5)
            show sl sad pioneer
            window show
            stop music fadeout 4
            sl "...В её дела. Опять не так сказала."
            sl "Ну хорошо, давайте закругляться."
            sl "Пора уже и к сцене выдвигаться."
            window hide
            $ renpy.pause(1.0)


label ss_d6playedit:
    play ambience ambience_camp_center_day
    scene bg ext_dining_hall_away_day with dissolve
    $ renpy.pause(1)
    show mt normal pioneer far with dspr
    show mt normal pioneer with dspr
    mt "Простите, задержалась. Вот костюмы."
    mt "И обойдутся вам в смешную сумму..."
    window hide
    $ renpy.pause(0.5)
    show mt smile pioneer with dspr
    window show
    mt "Шучу. Но раздевалок нету там."
    mt "Бегите одеваться по домам."
    window hide
    $ renpy.pause(0.5)
    play ambience ambience_int_cabin_day
    scene bg int_house_of_mt_day with dissolve
    $ renpy.pause(1.5)
    show cg ro_m with dissolve
    th "А выглядит весьма недурно, нет?"
    th "Как здорово сидит на мне берет!"
    th "О нем владелец старый раз не тужит,"
    th "Пускай теперь искусству он послужит."
    window hide
    $ renpy.pause(0.5)
    play ambience ambience_day_countryside_ambience fadein 1
    scene bg ext_house_of_mt_day
    with dissolve
    $ renpy.pause(1)
    show ss smile2 dress
    with dissolve
    if persistent.dress == 'purple':
        $ persistent.ssg_94 = True
    else:
        $ persistent.ssg_95 = True
    $ renpy.pause(0.5)
    window show
    ss "{en}Wow, you in this... It looks so funny!{/en}{ru}В наряде этом ты... Смешной до колик!{/ru}"
    ss "{en}You look like some old Disney's bunny.{/en}{ru}Ни дать ни взять - совсем мультяшный кролик.{/ru}"
    ss "{en}Alice in Wonderland, a sweet cartoon.{/en}{ru}С \"Алисы в Зазеркалье\" от Диснея.{/ru}"
    me "{en}Then hop along. We're getting started soon.{/en}{ru}Идем. Нам к сцене надо побыстрее.{/ru}"
    window hide
    $ renpy.pause(0.5)
    hide ss with dspr
    $ renpy.pause(1)
    window show
    play music In_the_void fadein 5
    "Пошёл я, но Саманта оставалась,"
    "Глазами в землю молча упиралась."
    th "Чего-то застеснялась, может быть?"
    th "Её не помешает подбодрить."
    window hide
    show ss shy dress with dspr
    $ renpy.pause(0.5)
    menu:
        "Здорово выглядишь":
            $ sp_ss += 1
            me "{en}If I'm a bunny then you are a Disney's princess,{/en}{ru}Коль кролик я, то ты - принцесса точно,{/ru}"
            me "{en}I didn't mention that before - I ask for your forgiveness!{/en}{ru}Забыл сказать раз, извиняюсь срочно.{/ru}"
            me "{en}Be sure that you look great in this.{/en}{ru}Красою будь загадочной горда,{/ru}"
            me "{en}And don't you ever doubt it, miss.{/en}{ru}Не сомневайся в этом никогда.{/ru}"
            show ss shy2 dress with dspr
            ss "{en}Right now I don't feel comfy, to be honest,{/en}{ru}Неловко чуть, приходится признать мне,{/ru}"
            ss "{en}Because as a dress-wearer I'm a novice.{/en}{ru}Нет опыта совсем в ношенье платьев.{/ru}"
            show ss shy dress with dspr
            ss "{en}But thank you for your words. I'll try my best,{/en}{ru}Тебе спасибо. Потружусь на славу.{/ru}"
            ss "{en}At least I'm not alone who's fancy-dressed.{/en}{ru}Хотя бы не одна чудно одета.{/ru}"
            me "{en}Cheer up. It's not a test. And should be fun.{/en}{ru}Бодрей! Ждёт не экзамен, а забава.{/ru}"
            me "{en}What else is bothering you? Any problem? None?{/en}{ru}Ну, нет проблем? И пожеланий нету?{/ru}"

        "Бояться нечего":
            me "{en}There is no need to be afraid at all.{/en}{ru}Тебе бояться нечего совсем,{/ru}"
            me "{en}You may forget your line and not recall,{/en}{ru}И с фразами не может быть проблем.{/ru}"
            me "{en}Don't even bother, just move on,{/en}{ru}Забыла реплику - не вспоминай,{/ru}"
            me "{en}Or else the play will last till dawn...{/en}{ru}Читай другую или пропускай.{/ru}"
            show ss serious dress with dspr
            ss "{en}What makes you think that I'm not ready?{/en}{ru}С чего взял, что забуду я слова?{/ru}"
            ss "{en}I know the text as good as my own name,{/en}{ru}Я знаю роль, как собственное имя,{/ru}"
            show ss unsure dress with dspr
            ss "{en}It's just my heart's a bit unsteady...{/en}{ru}Хотя в руках держу себя едва,{/ru}"
            ss "{en}I'm trembling, and incertitude's to blame.{/en}{ru}Со страхами не совладать другими.{/ru}"
            show ss shy dress
            me "{en}For guessing incorrectly I'm notorious -{/en}{ru}Не представляю, что тебя тревожит...{/ru}"
            me "{en}I need the services of Captain Obvious.{/en}{ru}Намёк прозрачный мне понять поможет.{/ru}"

    ss "{en}There's nothing to discuss. No more details.{/en}{ru}Что говорить? Подробности опустим.{/ru}"
    window hide
    $ renpy.pause(1.0)
    show ss shy2 dress with dspr
    window show
    ss "{en}Just swear that you will kill me if I fail.{/en}{ru}Убить дай клятву, если ошибусь я!{/ru}"
    ss "{en}Yes, strangle me in there to end the shame.{/en}{ru}Да, задуши - избавишь от стыда.{/ru}"
    me "{en}Not funny, Sammy. I don't like your game.{/en}{ru}Не весело. Сэм, что за ерунда?{/ru}"
    show ss grin_smile dress with dspr
    ss "{en}Don't you believe in me? Just say, come on!{/en}{ru}В меня не веришь? Ну же, не молчи!{/ru}"
    ss "{en}Our destiny is long ago foregone.{/en}{ru}Судьба предрешена давно для нас.{/ru}"
    me "{en}We're late! There is no time to spare.{/en}{ru}Пойдём уж, пьесы наступает час.{/ru}"
    show ss grin dress
    stop music fadeout 3
    ss "{en}Relax, I'm going... Once you swear!{/en}{ru}Пойдём... как только клятва прозвучит!{/ru}"
    show ss scared dress with dspr
    me "{en}I SWEAR I'll KILL YOU IF YOU FAIL. {/en}{ru}КЛЯНУСЬ, УБЬЮ, ПРОВАЛИШЬ ПЬЕСУ КОЛЬ.{/ru}"
    if persistent.ss_text_lang == "en":
        extend "{en}Okay?{/en}"
    show ss surprise dress
    ss "{en}No need to yell like that. It's just a play.{/en}{ru}Играем же. В ушах от крика боль.{/ru}"
    me "{en}Don't play with death, that's what I ask of you.{/en}{ru}Со смертью не играй ты никогда.{/ru}"
    show ss smile2 dress with dspr
    ss "{en}To play with death is what we're gonna do,{/en}{ru}Но что собрались делать мы тогда?{/ru}"
    ss "{en}Romeo and Juliet, remember?{/en}{ru}Ромео и Джульетта, друг. Не помнишь?{/ru}"
    me "{en}Alright, just go! Now I surrender.{/en}{ru}Сдаюсь. Скорей идём уже. Бог в помощь.{/ru}"
    window hide
    $ renpy.pause(0.5)
    hide ss with dspr
    stop music fadeout 3
    $ renpy.pause(1.0)
    show greysation
    show sspi normal at center
    stop ambience fadeout 5
    with dissolve
    pi "А вас, товарищ, я прошу остаться."
    pi "Какой у нас, выходит, ты злодей!"
    pi "Привыкшего злокозненным считаться"
    pi "Весьма присягой удивил своей."
    me "О чём ты? Эта клятва несерьёзна."
    play music music_list["drown"] fadein 4
    show sspi smile at center
    pi "Серьёзней некуда, вот в чём беда."
    pi "И клятву эту брать обратно поздно."
    pi "Запомни это. Раз и навсегда."
    show sspi normal at center
    pi "За действия ответа не привык"
    pi "Держать ты за секретности заслоном."
    pi "Держи ж в узде свой собственный язык,"
    pi "Ведь здесь другие несколько законы."
    me "А кто тебя подслушивать просил?!"
    pi "При чём здесь я? Ты сам себе дал слово."
    pi "Раз клятву дать в себе набрался сил,"
    pi "Теперь будь и к последствиям готовым."
    window hide
    $ renpy.pause(1)
    show sspi smile at center
    pi "Готов ли ты Джульетту задушить?"
    pi "Пусть зритель рукоплещет этой сцене."
    pi "Сценарий нужно только изменить,"
    pi "Чтобы свой план осуществить пред всеми."
    pi "Побольше драматизма и надрыва -"
    pi "Пусть публика следит, открывши рот,"
    pi "Любуясь постановкою красивой,"
    pi "Пусть после аплодирует народ..."
    pi "А после застывает, ужаснувшись,"
    pi "Как будто от кошмара... не проснувшись!"
    # здесь должно скорчить рожу померзее.
    pi "Не нравится тебе мой план коварный?"
    pi "Ты на него взгляни со стороны:"
    pi "Как это будет адово угарно!"
    "..."
    pi "Другие варианты не даны."
    window hide    
    $ renpy.pause(0.5)
    window show

    me "Что. Если. Я. На клятву. Положу?"
    me "Сценарий изменить, сказал ты, нужно?"
    me "В другое время... Верно я сужу?"
    me "С чего так улыбаешься натужно?"
    show sspi normal at center
    me "Обет свой чтоб исполнить, человек"
    me "Не должен сразу делать ничего."
    me "День? Месяц? Год? Декада? Может, век?"
    me "Нет, мало. Лучше не сдержу его."
    me "Я волен сам сценарий выбирать:"
    me "Я выбираю тот, где Сэм живёт."
    pi "Но клятву невозможно не сдержать..."
    me "Действительно? А что произойдёт?"
    pi "Хоть длань твою обет и не направит,"
    pi "Здесь клятвою своею не владеем."
    me "Довольно! Метафизику отставить!"
    me "Что, кроме пустословия, имеем?"
    me "Короче, Склифосовский, вон пошёл!"
    stop music fadeout 1
    pi "Но... клятва... Как же так... Нехорошо..."
    me "Кому сказал? А ну, пшёл вон отсюда!"
    window hide
    hide sspi with dspr
    play ambience ambience_int_cabin_day fadein 2
    $ renpy.pause(1)
    hide greysation
    with dissolve
    th "Замедленное время трать ещё!"
    #эточозамэрисью?
    th "Бежать скорей, не опоздал покуда."
    $ renpy.pause(0.9)
    window hide

    $ day_time()
    $ persistent.sprite_time = "day"
    play music music_list["timid_girl"]
    scene bg verona_square
    show sl shy cos at fright
    show dv laugh cos at fleft
    show sh normal_smile pioneer far at cright
    show us smile cos  at right
    show mz laugh cos at center
    show un smile cos at left
    with dissolve
    $ renpy.pause(1.0)
    window show
    "Актёры остальные все на сцене,"
    "Кругом улыбки - пьеса на носу!"
    "Все кланяются (да, и даже Женя),"
    play sound Clapyell
    "А публика приветствует вовсю."
    "Но всё равно успели мы с запасом."
    "Пришлось ещё изрядно подождать,"
    "Пока собрались все внимать рассказу."
    "Вот, наконец-то, можно начинать."
    window hide
    $ renpy.pause(1.0)
    scene black with fade
    scene bg verona_square
    show mi normal pioneer
    with fade
    $ renpy.pause(0.5)
    window show
    mi "{i}Две равно уважаемых семьи{/i}"
    mi "{i}В Вероне, где встречают нас событья,{/i}"
    mi "{i}Ведут междоусобные бои{/i}"
    mi "{i}И не хотят унять кровопролитья.{/i}"
    mi "{i}Друг друга любят дети главарей,{/i}"
    mi "{i}Но им судьба подстраивает козни,{/i}"
    mi "{i}И гибель их у гробовых дверей{/i}"
    mi "{i}Кладёт конец непримиримой розни.{/i}"
    th "И сразу же заспойлерен сюжет -"
    th "На тизеры сии управы нет."
    show mi upset pioneer
    mi "{i}Помилостивей к слабостям пера -{/i}"
    mi "{i}Их сгладить постарается игра.{/i}"
    window hide
    hide mi with dspr
    $ renpy.pause(1.0)
    show stl normal pioneer at right
    show el smile pioneer
    with dspr
    $ renpy.pause(1.0)

    stl "{i}Грегорио. Перед ними не срамиться.{/i}"
    el "{i}Напротив: кого встречу, осрамлю.{/i}"
    stl "{i}Скорее же! Мы зададим им баню!{/i}"
    el "{i}Самим сухими б выйти из воды.{/i}"
    stl "..."
    show el serious pioneer
    el "{i}Я скорчу рожу, мимо проходя. {w}Посмотрим, что на то они предпримут.{/i}"
    stl "..."
    mt "Кончай грызть ногти, Толик."
    stl "{i}Я ноготь грызть намерен в адрес их. {w}Позор им будет, если промолчат.{/i}"
    $ renpy.pause(0.8)
    show sspi normal at fleft
    pi "{i}На чей вы счёт грызёте ноготь, сэр?{/i}"
    stl "{i}Нет, не грызу на ваш я ноготь, сэр. {w}Грызу на счёт, сказал я, ноготь, сэр.{/i}"
    show el upset pioneer
    el "{i}Вы набиваетесь на драку, сэр?{/i}"
    pi "{i}Я, сэр? Нет, сэр. Не очень набиваюсь.{/i}"
    el "{i}А если так - то я к услугам вашим. {w}Я проживаю у господ не хуже ваших.{/i}"
    pi "{i}Но явно не у лучших, чем мои.{/i}"
    show el angry pioneer
    el "{i}У лучших, сэр. Я в этом вас заверю.{/i}"
    pi "{i}Я оскорблён до глубины души.{/i}"
    pi "{i}Деритесь, если только вы мужчины!{/i}"
    play sound sworddraw
    window hide
    hide stl
    show el angry pioneer at right
    show sspi smile at fleft
    show sh rage pioneer
    with dissolve
    window show
    play sound swordclash
    sh "{i}Оружье прочь - и мигом по местам! {w}Не знаете, что делаете, дурни.{/i}"
    th "Сейчас Алисы выход. Вот она."
    window hide
    hide el
    hide sspi
    show sh normal pioneer at right
    show dv guilty cos at left
    with dissolve
    window show
    th "Но почему она стоит без текста?"
    th "Читать с листочка вроде бы должна?"
    dv "..."
    show sh surprise pioneer at right
    sh "{i}Прочь оружие!..{/i}"
    dv "{i}Вы огребаете... Злодеи.{/i}"
    sh "{i}Хочу их помирить. Вложи свой меч. {w}Или давай их сообща разнимем.{/i}"
    dv "{i}...Ладно? {w}...Нет? {w}Нет! Вы огребаете, злодеи!{/i}"
    show sh scared pioneer at right
    show dv angry cos at left
    play sound swordclash
    "Вот с Шуриком по плану стали драться."
    stop music fadeout 6
    th "Что же могло с её листками статься?"
    th "Провалим пьесу, если так пойдёт."
    mi "{i}Входит князь со свитой.{/i}"
    window hide
    show us upset cos
    show sh upset pioneer at right
    with dspr
    window show
    play music music_list["awakening_power"]
    us "{i}Изменники, убийцы тишины,{/i}"
    us "{i}Грязнящие железо братской кровью!{/i}"
    show us dontlike cos
    us "{i}Не люди, а подобия зверей,{/i}"
    us "{i}Гасящие пожар смертельной розни{/i}"
    us "{i}Струями красной жидкости из жил!{/i}"
    us "{i}Кому я говорю? Под страхом пыток{/i}"
    us "{i}Бросайте шпаги из бесславных рук!{/i}"
    play sound swordclash
    us "{i}Все бросили? {w}Приказываю всех казнить!{/i}"
    window hide
    $ renpy.pause(0.8)
    show us smile cos
    with dspr
    window show
    us "{i}...А может, нет? Могу и отпустить.{/i}"
    us "{i}Про праздник вспомнил, слыша медь литавр:{/i}"
    us "{i}Последний из Европы гадкий мавр{/i}"
    us "{i}Был изгнан ровно пять веков назад.{/i}"
    us "{i}Не казнь тогда устроим, а парад.{/i}"
    us "{i}Пусть и одно другому не мешает...{/i}"
    us "{i}Но милости вельможу украшают.{/i}"
    us "{i}Сим решено. Я забияк жалею.{/i}"
    us "{i}Свободны будьте! Слава юбилею!{/i}"
    stop music fadeout 5
    window hide
    $ renpy.pause(0.8)
    scene bg ss_bstage with dissolve
    $ renpy.pause(0.5)
    window show
    th "Где мог Алисин текст запропаститься?"
    th "Минутка есть, пора осведомиться."
    window hide
    show dv guilty cos
    show sl serious cos at right
    with dissolve
    window show
    dv "Мой текст пропал! И запасные тоже."
    me "Все копии? Такого быть не может!"
    sl "Нет, правда. Распечаток нет нигде."
    sl "Мы с Женей уж проверили везде."
    sl "Собрали перед сценой совещанье,"
    sl "Листки сложили с общими вещами."
    th "Меня терзают смутные сомненья"
    th "По поводу сего исчезновенья."
    show dv angry cos with dspr
    dv "Кому меня пристало унижать..."
    dv "Нет, я назло им буду продолжать!"
    dv "По памяти, иль выдумать смогу."
    dv "И, Славя. Не останусь я в долгу!"
    me "У Сэм есть текст. Попробую помочь,"
    show dv sad cos
    me "Пусть не смогу перевести точь-в-точь,"
    me "Суфлирую тебе, уйдя со сцены."
    me "...Смекалка, впрочем, тоже будет ценной."
    window hide
    $ renpy.pause(0.8)
    hide dv with dspr
    $ renpy.pause(0.7)
    menu:
        "Обвинить Славю":
            $ sp_sl -= 1
            show sl serious cos
            me "Душа моя, мне не хватает зла..."
            show sl angry cos
            sl "О чем ты? Я бумаги не брала."
            me "Кто бы ещё хотел её провала?"
            me "Вот кто ещё? Кто это мог бы быть?"
            sl "Не в курсе. И не смей меня винить!"
            sl "Уж дело портить общее не стала б."

        "Одобрить кражу":
            $ sp_dv -= 1
            show sl serious cos
            me "Не правда ли, сладка бывает месть?"
            me "Хитро, Славяна. В этом что-то есть."
            window hide
            show sl scared cos
            window show
            sl "Ты спятил?! Это не моих рук дело."
            sl "Не это вовсе я в виду имела."
            sl "Ей отомстить желала я иначе..."
            "Не лжёт она. Того гляди - заплачет."

        "Промолчать":
            sl "Ох, где найти зловредного повесу?"
            sl "И времени в обрез - играем пьесу."

    show mz normal cos at left
    mz "Чего стоите тут, глаза навыкат?"
    mz "Скорей на сцену, там уже ваш выход."
    window hide
    $ renpy.pause(0.7)
    scene bg verona_square
    show sh normal pioneer at right
    show sl normal cos at left
    with dissolve
    window show
    me "{i}Прочесть ли нам приветствие в стихах{/i}"
    me "{i}Или войти без лишних предисловий?{/i}"
    play music music_list["went_fishing_caught_a_girl"] fadein 5
    sh "{i}Под дудку их не будем мы плясать,{/i}"
    sh "{i}А спляшем под свою и удалимся.{/i}"
    me "{i}Тогда дай факел мне. Я огорчён{/i}"
    me "{i}И не плясун. Я факельщиком буду.{/i}"
    sh "{i}Ромео, нет, от танцев не уйдёшь.{/i}"
    sh "{i}Ты с нами спляшешь, порешим на том.{/i}"
    me "{i}Уволь меня... {w}Арнольд не пляшет. Он даже движется с трудом.{/i}"
    th "Ещё один такой нашёлся малый,"
    th "Как будто мне одной Алисы мало..."
    th "Он сам свой текст не выучил ни разу,"
    th "Теперь меня сбивает, вот зараза!"
    sh "{i}Стучитесь в дверь, и только мы войдём -{/i}"
    sh "{i}Все в пляс, и пошевеливай ногами.{/i}"
    me "{i}Дай факел мне. Пусть пляшут дураки.{/i}"
    me "{i}Половики не для меня стелили.{/i}"
    sl "{i}Ах, факельщик! Стучись в подъезд, чтоб не истлеть живьём.{/i}"
    sl "{i}Мы днем огонь, как говорится, жжём.{/i}"
    window hide
    $ renpy.pause(0.7)
label ss_d6playballedit:
    $ day_time()
    $ persistent.sprite_time = "day"
    scene bg verona_ball
    with fade
    $ renpy.pause(1.0)
    show el normal pioneer with dspr
    window show
    me "{i}Кто эта барышня, с которой в ряд{/i}"
    me "{i}Стал этот кавалер?{/i}"
    el "{i}Не знаю, сударь.{/i}"
    window hide
    hide el with dspr
    $ renpy.pause(0.5)
    show ss nosmile dress at right with dissolve
    window show
    me "{i}Её сиянье факелы затмило.{/i}"
    me "{i}Она, подобно яркому бериллу{/i}"
    me "{i}В ушах арапки, чересчур светла{/i}"
    me "{i}Для мира безобразия и зла.{/i}"
    th "Сейчас такое примут за расизм,"
    th "И не спасёт чудной метафоризм."
    me "{i}Я к ней пробьюсь и посмотрю в упор.{/i}"
    me "{i}Любил ли я хоть раз до этих пор?{/i}"
    window hide
    show godess with dissolve:
        pos (150,150)
    me "{i}О нет, то были ложные богини.{/i}"
    me "{i}Я истинной красы не знал доныне.{/i}"
    window hide
    hide godess with dissolve
    show dv angry cos
    hide ss
    with dspr
    window show
    if d6_dv_learned >= 1:
        dv "{i}Мне показалось... Тут Монтекки...{/i}"
    else:
        dv "{i}Как оказалось, в Трою вторглись греки!{/i}"
        un "Монтекки."
        dv "{i}Мне показалось, тут Монтекки!{/i}"
    th "Надеюсь, Лена дальше ей подскажет."
    th "Но пьесу всё ж игра такая смажет."
    dv "{i}Осмелился пробраться к нам под маской{/i}"
    dv "{i}В насмешку над семейным торжеством!{/i}"
    dv "{i}Мальчишка...{/i}"
    window hide
    show dv surprise cos with dspr
    $ renpy.pause(0.8)
    window show
    th "Забыла, чёрт! А Лены не слыхать,"
    th "Похоже, мне придётся подсказать."

    if d6_nolife == 1:
        th "Её я роль читал, припоминаю..."
        th "Конечно! Точно! Эту фразу знаю!"
        me "Шпагу!"
        show dv normal cos
        dv "{i}Мальчишка, шпагу!{/i}"
        $ d6_dvwellplayed += 1
    else:
        menu:
            "Вперёд ни шагу!":
                dv "{i}Вперёд... Ни шагу? {w}...И назад!{/i}"
            "Покажи отвагу!":
                show dv normal cos
                dv "{i}Покажи отвагу! {w}Да... И верность стягу!{/i}"
            "Шпагу!":
                show dv normal cos
                dv "{i}Мальчишка, шпагу!{/i}"
                $ d6_dvwellplayed += 1

    window hide
    $ renpy.pause(0.4)
    show unl normal at fright with dspr
    window show
    ssunl "{i}Мой дорогой, зачем ты поднял крик?{/i}"
    show dv angry cos
    dv "{i}У нас Монтекки!{/i}"
    menu:
        "Как он к нам проник?":
            if d6_dv_learned == 2:
                dv "{i}Как он к нам проник?{/i}"
                $ d6_dvwellplayed += 2
            else:
                dv "{i}...Откуда этот псих?{/i}"
                $ d6_dvwellplayed += 1
        "Убирайся, озорник!":
            dv "{i}...Умывался он у них? ...Убирался? {w}Убирайся! Или защищайся!{/i}"
    show unl upset at fright
    ssunl "{i}Оставь его, вот мой тебе приказ.{/i}"
    dv "{i}Уйти, с вареньем проглотивши кость?{/i}"
    dv "{i}...Смиреньем победивши злость?{/i}"
    dv "{i}Что ж, я уйду. Но ваш незваный гость,{/i}"
    dv "{i}Которого нельзя побеспокоить,{/i}"
    dv "{i}Ещё вам будет много крови стоить!{/i}"
    window hide
    $ renpy.pause(0.5)
    hide unl
    hide dv
    with dspr
    "Закончены Алисины мученья,"
    "Но за грядущий скоро эпизод"
    stop music fadeout 4
    "Не меньшие питаю опасенья;"
    "Меня же рандеву с Джульеттой ждёт."
    window hide
    $ renpy.pause(0.5)
    show ss shy dress with dspr
    $ renpy.pause(1.0)
    window show
    play music Romeo_And_Juliet fadein 3
    me "{i}Я ваших рук рукой коснулся грубой.{/i}"
    me "{i}Чтоб смыть кощунство, я даю обет:{/i}"
    me "{i}К угоднице спаломничают губы{/i}"
    me "{i}И зацелуют святотатства след.{/i}"
    "И тут прервать Ромео должен речи,"
    "Целуя руку молодой девице."
    "Не слишком ли он храбр для первой встречи?"
    "Покажут время и чужие лица."
    show ss smile2 dress with dspr
    ss "{i}{en}Good pilgrim, you do wrong your hand too much,{/en}{ru}Святой отец, пожатье рук законно.{/ru}{/i}"
    ss "{i}{en}Which mannerly devotion shows in this;{/en}{ru}Пожатье рук - естественный привет.{/ru}{/i}"
    ss "{i}{en}For saints have hands that pilgrims' hands do touch,{/en}{ru}Паломники святыням бьют поклоны.{/ru}{/i}"
    ss "{i}{en}And palm to palm is holy palmers' kiss.{/en}{ru}Прикладываться надобности нет.{/ru}{/i}"
    me "{i}Однако губы нам даны на что-то?{/i}"
    show ss shy dress with dspr
    ss "{i}{en}Ay, pilgrim, lips that they must use in prayer.{/en}{ru}Да, - для молитвы, добрый пилигрим.{/ru}{/i}"
    me "{i}Святая, так услышь тогда молитву:{/i}"
    me "{i}Устам позволь прильнуть к устам твоим.{/i}"
    show ss shy2 dress
    if persistent.dress == 'purple':
        $ persistent.ssg_94 = True
    else:
        $ persistent.ssg_95 = True
    ss "{i}{en}Saints do not move, though grant for prayers' sake.{/en}{ru}Не двигаясь святые внемлют нам.{/ru}{/i}"
    me "{i}Недвижно дай ответ моим мольбам.{/i}"
    th "Вот и настал тот трепетный момент."
    th "Не хочется устроить инцидент."
    th "А что могу я предложить взамен?"
    th "Как поступил бы истый джентльмен?"
    "Подался к замершей своей подруге,"
    "Раскрывшей очи широко в испуге."
    window hide
    $ renpy.pause (0.5)
    if persistent.dress == 'purple':
        show cg ss_playkiss1p with dissolve
    else:
        show cg ss_playkiss1r with dissolve

    $ persistent.ssg_110 = True

    $ renpy.pause (1)
    window show
    "И вот она. Решенье очевидно:"
    "Как мим, я чмокнул воздух безобидно."
    window hide
    $ renpy.pause (1)
    hide cg
    show ss shy dress
    with dissolve
    $ renpy.pause (0.5)
    window show
    me "{i}Вот с губ моих весь грех теперь и снят.{/i}"
    ss "{i}{en}Then have my lips the sin that they have took.{/en}{ru}Зато мои впервые им покрылись.{/ru}{/i}"
    me "{i}Тогда отдайте мне его назад.{/i}"
    "Теперь мне предстоит второй заход,"
    "И вновь к Саманте я тянусь вперёд."
    show ss shy2 dress
    "Однако у меня мысль пронеслась,"
    "Что в этот раз вперёд Сэм подалась."
    window hide
    if persistent.dress == 'purple':
        show cg ss_playkiss1p with dissolve
    else:
        show cg ss_playkiss1r with dissolve
    $ renpy.pause (0.5)
    window show
    "Невольно заглянул в её глаза,"
    "И ясно... что не ясно ничего."
    th "А если поцелую? Что с того?"
    th "Ох, нет, Семён! Дави на тормоза."
    th "Тот \"поцелуй\" удался нам отлично,"
    th "Теперь закончить следует прилично."
    menu:
        "Поцеловать понарошку":
            show ss shy dress
            "И вновь манёвр свой ловко повторил,"
            "Честной народ ни разу не смутил."
            window hide
            $ renpy.pause (0.5)
            hide cg with dissolve
            $ renpy.pause (0.5)
            if d6_kissing == 1:
                $ sp_ss -= 1
                th "Хоть обещал её поцеловать,"
                th "Опасности тому не занимать."
                th "Ну а чего ещё она хотела?"
                th "Вожатая б душила, как Отелло."
                window hide
            show un normal cos at fleft with dspr
            $ renpy.pause (0.5)

        "Поцеловать в губы":
            $ sp_ss += 2
            th "Ради того же всё и затевалось,"
            th "Иначе люди зря пришли б, казалось."
            if d6_kissing == 0:
                th "Пусть утром речь и о другом шла малость,"
                th "Но отчего б сейчас не сделать шалость?"
                $ sp_ss -= 1
            "И вновь к её приблизившись устам,"
            "Я не дышу и замираю сам,"
            "Тут словно равновесие теряю,"
            "К её губам своими припадаю."
            window hide
            if persistent.dress == 'purple':
                show cg ss_playkiss2p with dissolve
            else:
                show cg ss_playkiss2r with dissolve

            $ persistent.ssg_111 = True

            $ renpy.pause (1.5)
            window show
            "Тотчас же воцарилась тишина,"
            "Со стороны лишь нотка мне слышна:"
            "Похоже, это скрипнул стул вожатой,"
            "Но вмешиваться ей уж поздновато."
            "Когда злодей уже в принцессу впился,"
            "В театре - театрально оступился."
            window hide
            $ renpy.pause (0.7)
            hide cg
            show ss shy dress
            with dissolve2
            "И прочий зритель тут же оживает:"
            "Не каждый смотрит в небо да зевает."
            "Хихикали и живо обсуждали."
            "Заметно было оживленье в зале."
            "Я уж давно отпрянул. Что Саманта?"
            "Потупила взор, смотрит виновато."
            if d6_kissing == 0:
                th "Бедняжка. И зачем так поступил?"
                th "Сказал - \"не будем\". Кто меня просил?"
            show un normal cos at fleft with dspr
            th "Но хватит истуканом тут стоять,"
            th "Вернулась Лена, нужно продолжать."
            $ d6_kissing = 2

    stop music fadeout 5
    un "{i}Джульетта!{/i}"
    un "{i}Синьора, матушка вас просит.{/i}"
    window hide
    $ renpy.pause(0.5)
    hide ss with dspr
    $ renpy.pause(1.0)
    show un normal cos at fleft
    window show
    me "{i}Кто матушка её?{/i}"
    un "{i}Она глава семьи, хозяйка дома.{/i}"
    un "{i}А я вскормила дочь, вы с ней сейчас стояли. Помяните:{/i}"
    un "{i}Кто женится на ней, тот заберёт хороший куш.{/i}"
    window hide
    hide un with dspr
    $ renpy.pause(0.5)
    window show
    me "{i}Так это Капулетти?{/i}"
    me "{i}Я у врага в руках и пойман в сети!{/i}"
    window hide
    show sh normal pioneer with dspr
    $ renpy.pause(0.5)
    window show
    sh "{i}Прощайся. Вижу, шутка удалась.{/i}"
    me "{i}И даже чересчур на этот раз.{/i}"
    window hide
    $ renpy.pause(0.7)
    scene bg ss_bstage with fade
    "Минута есть за сценой отдохнуть,"
    "И снова в драму с головой нырнуть."
    window hide
    $ renpy.pause(0.5)
    if d6_kissing == 2:
        jump d6_whykissedit
    else:
        jump d6_findthiefedit

label d6_whykissedit:
    show dv smile cos with dspr
    window show
    play music music_list["gentle_predator"] fadein 2
    dv "Зачем с детьми целуешься, подлец?"
    dv "Ты что, охоч до молодых сердец?"
    menu:
        "Я не нарочно":
            me "Случайно вышло, я не виноват."
            me "Тебе меня не нужно укорять."
            me "Ведь сам себя сильнее во сто крат"
            me "За этот случай буду я терзать."
            show dv normal cos with dspr
            dv "Какой серьёзный. Ладно. Пошутила."
            dv "Сама я там не сильно навредила?"
            me "Ты справилась. Никто и не заметил."
            me "Мой выход затмевает всё на свете."
            jump d6_findthiefedit
        "Подумаешь":
            $ sp_dv += 1
            me "Хотели, чтоб любили мы на сцене,"
            me "Шутили надо мной, смущали Сэмми."
            me "И коли пьеса ничего не значит,"
            me "То с поцелуем - будет ли иначе?"
            me "Саманте плохо не было, похоже,"
            me "И заразить ничем не должен тоже."
            dv "Согласна. Но ведь ты сентиментален."
            dv "Такой поступок для тебя нормален?"
            me "Хотела подколоть меня опять?"
            dv "А ты пытаешься опять соврать."
            me "Нет, я не вру. Так обстоят дела."
            me "...Тех поцелуев тысяча была!"
            show dv angry cos with dspr
            dv "...Семён... Ты это говоришь серьёзно?"
            me "Ах, на меня ты не смотри так грозно!"
            me "И кто кого в итоге подловил?"
            dv "Я прислежу за вами. Уловил?"
            me "В своем уме ты? Глупость! Я шучу!"
            me "Подумал, что в отместку проучу..."
            show dv smile cos
            dv "И снова сам попался. Два - один."
            me "...А из тебя хороший паладин."
            jump d6_findthiefedit

label d6_findthiefedit:
    window hide
    $ renpy.pause(0.8)
    if d6_dvwellplayed <= 1:
        if d6_dvwellplayed == 0:
            $ sp_dv -= 1
        show dv guilty cos with dspr
        window show
        dv "Семён. Ты всё мне верно подсказал?"
        dv "Или тебя я плохо услыхала?"
        me "Никто ошибок этих не признал,"
        me "Пусть не точь-в-точь местами роль читала."
        dv "Ах, мне без текста лучше было сгинуть,"
        dv "А не стоять на сцене, рот разинув..."
        dv "Лишь потому я не сбежала прочь,"
        dv "Что согласился ты, брехун, помочь."
        me "Прости, твой текст мы очень плохо знали."
    else:
        $ sp_dv += 1
        if d6_dvwellplayed == 3:
            $ sp_dv += 1
        show dv smile cos with dspr
        window show
        dv "А ты неплохо мне там подсказал."
        dv "...Хоть я и так слова отлично знала."
    dv "Воздать воровке надобно теперь."
    show dv normal cos
    dv "О Славе слышал? Знаешь, что за зверь?"
    me "Не будем с осуждением спешить."
    dv "Да что ты? И кого тогда винить?"

    dv "Меня, быть может, обвиним мы спешно."
    dv "Раз есть и репутация, конечно:"
    show dv angry cos with dspr
    dv "Наверное, Алиса виновата?"
    dv "Что если не повинна в том, так в этом."
    dv "А Славечка чиста-бела как вата."
    dv "Ты тоже с ней теперь поёшь дуэтом?"
    menu:
        "Нет, ты права":
            $ sp_dv += 1
            me "Я на твоей, конечно, стороне."
            show dv normal cos with dspr
            dv "Да ладно? А вот это уже что-то."
            dv "Хотя по жизни Славя на коне,"
            dv "Разоблачить злодейку должен кто-то."
            dv "Какая ж лицемерка и подлиза..."
            dv "Найдём улики - будет экспертиза!"
        "Вдруг это кто-нибудь ещё?":
            me "А если тексты утащил другой?"
            me "Допустим, Женя. Вариант такой?"
            show dv guilty cos with dspr
            dv "У той найти мотив немудрено,"
            dv "Без разницы - подлюги заодно."
            me "Скольких против себя ты ополчила?"
            show dv normal cos with dspr
            dv "Неважно то. Тогда я лишь шутила."
            dv "Теперь же с ними я шутить не буду -"
            dv "Такую подлость долго не забуду!"


    me "Так. Мне теперь назад на сцену нужно."
    me "Давай хоть доиграем пьесу дружно?"
    me "Покамест мы ещё не провалились,"
    me "Не делай ничего, договорились?"
    dv "Расслабься, я не Славя - меру знаю."
    dv "Народу портить вечер не желаю."
    me "Не прозевать свой выход! Я пошёл."
    me "Поспрашивай, вдруг тексты кто нашёл."
    window hide
    stop music fadeout 5
    $ renpy.pause(0.8)
    scene bg verona_square
    show sl normal cos
    show sh upset pioneer at left
    with dissolve
    window show
    sh "{i}Ромео, стой!{/i}"
    sl "{i}Ромео не дурак. {w}Он дома и, наверное, в постели.{/i}"
    sh "{i}Он перелез чрез эту стену в сад. {w}Погромче позови его, Меркуцио.{/i}"
    sl "{i}Идем. Зачем искать того, кто найден быть не хочет?{/i}"
    window hide
    $ renpy.pause(0.8)
    scene bg verona_garden with fade
    window show
    play music Warm_evening fadein 5
    me "{i}И что за блеск я вижу на балконе?{/i}"
    me "{i}Там брезжит свет. Джульетта, ты как день!{/i}"
    me "{i}Стань у окна, убей луну соседством;{/i}"
    me "{i}Она и так от зависти больна,{/i}"
    me "{i}Что ты её затмила белизною.{/i}"
    me "{i}Стоит, сама не зная, кто она.{/i}"
    me "{i}Губами шевелит, но...{/i}"
    voice "Но ни черта, конечно же, не ясно!"
    th "И тут не обошлось без горлопана,"
    th "Вожатые, примите хулигана!"
    me "{i}Губами шевелит, но слов не слышно.{/i}"
    show ss unsure dress  with dspr
    ss "{i}{en}Ay me!{/en}{ru}О горе мне!{/ru}{/i}"
    ss "{i}{en}O Romeo, Romeo! Wherefore art thou Romeo?{/en}{ru}Ромео! Почему же ты Ромео?{/ru}{/i}"
    voice "Вопрос резонный, кстати говоря!"
    voice "И роль Ромео получил он зря!"
    show ss nosmile dress
    ss "{i}{en}Deny thy father and refuse thy name{/en}{ru}Отринь отца да имя измени,{/ru}{/i}"
    ss "{i}{en}Or, if thou wilt not, be but sworn my love,{/en}{ru}А если нет, меня женою сделай,{/ru}{/i}"
    ss "{i}{en}And I'll no longer be a Capulet.{/en}{ru}Чтоб Капулетти больше мне не быть.{/ru}{/i}"
    ss "{i}{en}Retain that dear perfection which he owes{/en}{ru}Зовись иначе как-нибудь, Ромео,{/ru}{/i}"
    ss "{i}{en}Without that title. Take all myself.{/en}{ru}И всю меня бери тогда взамен!{/ru}{/i}"
    th "{i}...Базара ноль?{/i}"
    th "Да как там было? Сбил меня проказник."
    me "{i}О, по рукам! Теперь я твой избранник!{/i}"
    show ss surprise dress
    me "{i}Я новое крещение приму,{/i}"
    me "{i}Чтоб только называться по-другому.{/i}"
    ss "{i}{en}What man art thou that thus bescreen'd in night{/en}{ru}Кто это проникает в темноте в мои мечты заветные?{/ru}{/i}"
    ss "{i}{en}Art thou not Romeo and a Montague?{/en}{ru}Ты не Ромео? Не Монтекки ты?{/ru}{/i}"
    me "{i}Ни тот, ни этот: имена запретны.{/i}"
    voice "Тогда проваливай-ай-ай!" # вот это пять.
    mt "Шутник наш обезврежен, продолжай."
    window hide
    $ renpy.pause(0.8)
    scene bg verona_church
    show mz normal cos
    with fade2
    window show
    mz "{i}Прошу благословения господня!{/i}"
    mz "{i}Кому б ко мне в такую рань сегодня?{/i}"
    me "{i}Святой Отец!{/i}"
    mz "{i}Ах, это ты? Вполне ли ты здоров,{/i}"
    mz "{i}Что пробудился раньше петухов?{/i}"
    mz "{i}Ты должен был по нездоровью встать,{/i}"
    mz "{i}А то и вовсе не ложился спать?{/i}"

    me "{i}Ты прав. Об этом не было помину.{/i}"
    show mz bukal cos
    mz "{i}Прости, господь! Ты был у Розалины?{/i}"
    me "{i}Нет, с Розалиной у меня конец.{/i}"
    me "{i}Я имя позабыл её, отец.{/i}"
    show mz normal cos
    mz "{i}Я одобряю. Что ж ты так сияешь?{/i}"
    me "{i}Сейчас, отец, ты главное узнаешь:{/i}"
    me "{i}Дочь Капулетти, знай, я полюбил,{/i}"
    me "{i}И ей такую же любовь внушил.{/i}"
    me "{i}Мы друг без друга часа не протянем,{/i}"
    me "{i}Все слажено, и дело за венчаньем.{/i}"
    mz "{i}Святой Франциск, какой переворот!{/i}"
    mz "{i}О Розалине уж и речь нейдёт.{/i}"
    mz "{i}Но если так мужское слово шатко,{/i}"
    mz "{i}Какого ждать от женщины порядка?{/i}"
    show mz smile cos
    stop music fadeout 4
    mz "{i}А впрочем... Я ведь плут, капиталист.{/i}"
    mz "{i}Уже вношу вас в свой венчаний лист.{/i}"
    window hide
    $ renpy.pause(0.8)
    scene bg verona_square
    show sl serious cos
    show sh normal pioneer at left
    with fade
    window show
    play music music_list["went_fishing_caught_a_girl"] fadein 5
    sl "{i}Где носят черти этого Ромео?{/i}"
    sl "{i}Он был сегодня ночью дома?{/i}"
    sh "{i}Нет. Я там справлялся.{/i}"
    sh "{i}Слыхал? Тибальт, племянник Капулетти,{/i}"
    sh "{i}Прислал ему письмо.{/i}"
    show sl surprise cos
    sl "{i}Вызов, конечно?{/i}"
    show sh serious pioneer at left
    sh "{i}Ромео наш ему ответит.{/i}"
    show sl sad cos
    sl "{i}Бедный Ромео! Уши у него прострелены серенадами, сердце - любовною стрелою.{/i}"
    sl "{i}И такому-то тягаться с Тибальтом!{/i}"
    show sh surprise pioneer at left
    sh "{i}А что Тибальт такое?{/i}"
    sl "{i}В вопросах чести - настоящий дьявол.{/i}"
    sl "{i}Фехтует, как по нотам: раз, два, а три уже сидит по рукоятку у тебя в брюхе.{/i}"

    sh "{i}Гляди-ка, никак, Ромео!{/i}"
    show sh normal pioneer at left
    show sl normal cos
    sl "{i}Бонжур, синьор Ромео!{/i}"
    sl "{i}Здорово вы нас вчера надули!{/i}"
    me "{i}Здравствуйте оба. Надул? Каким образом?{/i}"
    sl "{i}А как же: уговор был идти вместе, а вы улизнули.{/i}"
    me "{i}Прости, милый Меркуцио, я теперь так занят!{/i}"
    "Вот Лена, что кормилицу играет;"
    "Короткое ей выпало камео:"
    "Обговорить венчание желает -"
    "Затем хозяйкой послана к Ромео."
    show un normal cos at fright

    sl "{i}На горизонте парус!{/i}"
    show sh smile pioneer at left
    sh "{i}Целых два: то юбка и штаны.{/i}"
    un "{i}Ах, где мой веер?{/i}"
    sl "{i}Скорее дайте веер, чтоб прикрыться.{/i}"
    sl "{i}Он ей наверняка исправит внешность.{/i}"
    show un normal cos at right
    un "{i}С добрым утром, добрые государи!{/i}"
    show sl smile cos
    sl "{i}С добрым вечером, добрая государыня!{/i}"
    show un surprise cos at right
    un "{i}Разве уж вечер?{/i}"
    sl "{i}По-видимому. В вашей жизни - бесспорно.{/i}"
    show un angry2 cos at right
    un "{i}А ну вас, право! Что вы за человек?{/i}"
    show un serious cos at right
    un "{i}Пустое. Кто мне скажет, где найти мне юного Ромео в этом доме?{/i}"
    me "{i}Из людей с этим именем я самый младший.{/i}"
    un "{i}Если вы Ромео, мне надо вам сказать что-то доверительное.{/i}"
    me "{i}Тогда давайте отойдём.{/i}"
    sl "{i}Прощайте, старая барыня, прощайте!{/i}"
    un "{i}Прощайте, скатертью дорога.{/i}"
    window hide
    hide sl
    hide sh
    with dspr
    $ renpy.pause(1.0)
    window show
    un "{i}Боже правый, я до сих пор не могу прийти в себя,{/i}"
    un "{i}И всю меня так и трясёт! Подлый хвастун!..{/i}"
    un "{i}Ах, сэр, ведь я-то пришла совсем по другому делу.{/i}"
    un "{i}Моя барышня, как говорится, просила меня узнать.{/i}"
    un "{i}Что она просила, это, конечно, моя тайна,{/i}"
    un "{i}Но если вы, сударь, собираетесь её дурачить...{/i}"
    me "{i}Погоди, нянюшка. Во-первых, передай от меня барышне поклон. Уверяю тебя...{/i}"
    show un smile cos at right
    un "{i}Я передам ей это, добрая вы душа. То-то она обрадуется!{/i}"
    me "{i}Что передать ты хочешь?{/i}"
    show un smile2 cos at right
    un "{i}Передам, что вы уверяете. И этого довольно. До встречи.{/i}"
    stop music fadeout 5
    me "{i}Но погоди, нянюшка...{/i}"
    un "{i}Да-да. Тысяча поклонов барышне.{/i}"
    window hide
    $ renpy.pause(0.8)
    scene bg ss_bstage
    show sl normal cos at left
    show dv normal cos at right
    with dissolve
    $ renpy.pause(0.5)
    window show
    sl "Семён? {w}Пропавшие листки нашла..."
    dv "О да, как спёрла, так и отдала!"
    show sl serious cos at left
    sl "Оправдываться пред тобой не буду."
    sl "И не желаю больше пересудов."
    me "Спасибо, Славя. Без проблем продолжим."
    show sl normal cos at left
    show dv angry cos at right
    dv "Минуточку! За что спасибо всё же?"
    dv "Уж пьесу нам испортила она!"
    dv "Осталось только вынуть меч из ножен,"
    dv "И будет роль моя завершена."
    show sl serious cos at left
    play music music_list["faceless"]  fadein 5
    dv "Клинки из жести, кстати. Ими б надо"
    dv "Размахивать предельно осторожно,"
    dv "Легко и шишку получить в награду,"
    dv "Да и заразу занести несложно."
    dv "Щас ржавчиной поранишься немножко..."
    dv "Ты поняла? Дуэль не понарошку!"
    show sl scared cos at left
    me "Алиса, что я слышу? Что за дикость!"
    dv "Не большая, чем Славина двуликость."
    dv "Прощенья хочет - пусть же признаётся,"
    dv "Прощения не хочет - пусть дерётся."
    dv "А эти возвращённые бумажки"
    dv "Уже не купят никакой поблажки."
    show sl angry cos at left
    me "Затмение на вас, гляжу, нашло."
    me "Иду к вожатой с этим я, покуда"
    me "У вас не слишком далеко зашло."
    th "А после обе будут звать Иудой."
    sl "Не надо. Так мы не продолжим пьесу."
    me "Коль драке вашей нет противовесу..."
    sl "Не будет драки. Пусть разок ударит,"
    sl "Раз то ей наслаждение подарит."
    dv "Всего разок? Не так описан бой."
    me "Окстись, Алиса, что это с тобой?!"
    show dv normal cos at right
    dv "А с ней? Что за смирение, откуда?"
    dv "Сама ведь знает - поступила худо."
    dv "Желает наказания за то."
    show sl sad cos at left
    sl "Ты... ты сама жестокость - вот ты кто!"
    menu:
        "Я не позволю":
            $ sp_dv -= 1
            $ sp_sl += 1
            me "Себе железку эту забираю."
            show dv angry cos at right
            dv "Отдай сюда мой реквизит, дурак!"
            me "Хоть ветками деритесь - я не знаю."
            me "Но допустить увечий - нет, никак."
            dv "Конечно, ты не можешь не вмешаться."
            show dv guilty cos at right
            dv "Отдай мне шпагу. Я не буду драться."
            me "Дашь слово?"
            dv "Обещаю."
            me "Не отдам."
            show dv rage cos at right
            dv "Ах, так? Да ну и к чёрту этот хлам!"
            "Теперь Алиса вся побагровела."
            sl "Отдай, Семён. И отбирать - не дело."
            sl "Оружие найдёт, тут не поможешь,"
            sl "Поможет только персональный сторож."
            show dv angry cos at right
            me "Я отдаю. Но помни - обещала!"
            "Та шпагой завладела, убежала."
            hide dv
            sl "Там сцена уж закончена почти."
            sl "А значит, акт второй, считай, сыграли."
            show sl scared cos at left
            sl "Ты... Если... за меня уж отомсти."
            me "Ты про Алису или про сценарий?"
            me "Тибальту - как Ромео, отомщу."
            me "А так - тебе вреда не допущу."
        "Надо разбираться":
            me "Где ты нашла их, Славя? Расскажи."
            me "Узнала вора если - укажи."
            show sl angry cos at left
            "Славяна же, насупившись, молчала."
            th "Похоже, правда вора опознала."

            sl "Я человек не лживый от природы,"
            sl "Но также - хорошо храню секреты,"
            sl "А ябедою не была отроду."
            sl "И в этот раз не обвините в этом."
            sl "Не время для рассказов - и не место,"
            sl "И публика не очень-то уместна."

            me "Я понял! Это Шурик, козья рожа!"
            me "Он день за днём мне козни строит тоже!"
            dv "Четырёхглазый - тот меня боится,"
            dv "Но гадина одна могла решиться..."
            dv "Её обида не пройдёт вовеки -"
            dv "Речь об особе... из библиотеки!"
            me "Не может быть! Чтоб Лена - и твой враг?"
            show dv angry cos at right
            "Алиса показала мне кулак."
            dv "Ты туп как дуб. Про Женю говорю."
#  ###  #   dv "Я больше жизни Леночку люблю!"         ### ### #   ### ### ###
#  # #  #   dv "Мечтаю только в пледик завернуть,"      #  # # #    \   #  # #
## ###  ##  dv "Калачиком в ногах её уснуть."           #  ### ### ###  #  ### (И что что толсто, размер надо соблюдать).
            window hide
            $ renpy.pause(1.0)
            show dv normal cos at right
            dv "На Славину реакцию смотрю"
            dv "И убеждаюсь: так оно и было."
            show sl surprise cos at left
            dv "Но что ж ты нам её не заложила?"
            dv "Наверное, вы были заодно..."
            dv "А может, нет. Откроется оно."
            show sl angry cos at left
            sl "Тебя готова саблей отходить."
            show dv laugh cos at right
            dv "За сим - изволь на сцену поспешить!"

    window hide
    stop music fadeout 3
    $ renpy.pause(0.8)
    scene bg verona_square
    show sl serious cos
    show sh upset pioneer at left
    with dissolve
    window show
    sh "{i}Прошу тебя, Меркуцио, уйдём.{/i}"
    sh "{i}Сегодня жарко. Всюду Капулетти.{/i}"
    sh "{i}Нам неприятностей не избежать,{/i}"
    sh "{i}И в жилах закипает кровь от зноя.{/i}"
    sh "{i}Нет, всё. Теперь уж поздно.{/i}"
    window hide
    play music music_list["heather"] fadein 2
    show dv normal cos at fright with dspr
    window show
    sh "{i}Ручаюсь головой, вот Капулетти.{/i}"
    sl "{i}Ручаюсь пяткой, мне и дела нет.{/i}"
    dv "{i}За мной, друзья! Я потолкую с ними -{/i}"
    dv "{i}Словечко-два, не больше, господа!{/i}"
    sl "{i}Словечко-два? Хотел удар-другой.{/i}"
    dv "{i}Всегда к услугам вашим, дайте повод.{/i}"
    sl "{i}Его ещё и принято давать?{/i}"
    dv "{i}Меркуцио? В компании с Ромео?{/i}"
    sl "{i}В компании? Да что за выражение!{/i}"
    sl "{i}В артели мы бродячих музыкантов?{/i}"
    play sound sworddraw
    sl "{i}Вот мой смычок, заставлю вас попрыгать.{/i}"
    dv "{i}Отстаньте! Вот мне нужный человек.{/i}"
    dv "{i}Ромео, сущность чувств моих к тебе{/i}"
    dv "{i}Вся выразима в слове: ты мерзавец.{/i}"
    me "{i}Я вовсе не мерзавец. Будь здоров.{/i}"
    me "{i}Я вижу, ты меня совсем не знаешь.{/i}"
    me "{i}А скоро до тебя дойдёт известье,{/i}"
    me "{i}Которое нас близко породнит.{/i}"
    me "{i}Расстанемся друзьями, Капулетти!{/i}"
    show sl angry cos
    sl "{i}Трусливая, презренная покорность!{/i}"
    sl "{i}Я кровью должен смыть её позор!{/i}"
    sl "{i}Как, крысолов Тибальт, ты прочь уходишь?{/i}"
    show dv angry cos at fright
    dv "{i}Что, собственно, ты хочешь от меня?{/i}"
    sl "{i}Одну из твоих жизней девяти, о, царь кошачий,{/i}"
    sl "{i}А после восемь остальных я выколочу следом.{/i}"
    sl "{i}Тащи за уши шпагу ты, покуда{w} я не тащу за уши целого тебя!{/i}"
    dv "{i}С готовностью! -{/i}"
    play sound sworddraw
    dv "{i}Такие вот зелёные береты{/i}"
    dv "{i}Я ем на завтрак, право, по утрам!{/i}"
    hide sh
    show dv angry cos at right
    "Вот встали в позу наши дуэлянты."
    "И кошки на душе моей скребут:"
    th "Не примирил их. Были варианты?"
    th "Надеюсь, что друг дружку не прибьют."
    play sound swordclash
    me "{i}Меркуцио, оставь!{/i}"
    play sound swordclash
    "Мечи два раза в воздухе сошлись -"
    "И с большей силой, чем необходимо."
    show dv normal cos at right
    "Алиса тут возьми да усмехнись;"
    play sound swordswipe
    "Ещё один удар проходит мимо."
    me "{i}Вынь меч, Бенвольо! Выбивай из рук{/i}"
    me "{i}У них оружье. Господа, стыдитесь!{/i}"
    me "{i}Тибальт! Меркуцио! Князь запретил{/i}"
    me "{i}Побоища на улицах Вероны!{/i}"
    "Промеж врагов Ромео должен встать"
    "В губительной попытке их разнять."
    play sound swordswipe
    show sl scared cos
    sl "{i}Ах!{/i}"
    "Из-под его руки Тибальт разит"
    "И, кровь увидев, тут же прочь бежит."
    stop music fadeout 4
    hide dv
    sl "{i}Чума возьми семейства... ваши... оба.{/i}"
    window hide
    $ renpy.pause(0.8)
    window show
    play music music_list["orchid"] fadein 3
    th "Но что же Славя вдруг молчит, осела?"
    th "Когда ей дальше нужно говорить..."
    th "Я тоже вижу кровь! И побелела!"
    th "Не верю, нет, нет, нет! Не может быть!"
    window hide
    show dv surprise cos at fright
    hide sl
    with dissolve
    window show
    dv "Эй, ты чего? Я что... тебя неча..."
    "..."
    "Мы в ступоре: не надо ль звать врача."
    "И словно чуда ждём, чтоб Славя встала"
    "И роль свою по плану доиграла."
    show dv scared cos at fright
    "А зрители считают всё спектаклем"
    "И крови не удивлены ни капли."
    stop music fadeout 2
    window hide
    show dv surprise cos at fright
    show sl serious cos
    with dissolve
    window show
    sl "Простите... Закружилась голова."
    play music music_list["you_lost_me"]
    sl "Свой текст не позабыла я едва..."
    show dv guilty cos
    th "Жива! Молитвам внемлет провиденье!"
    th "Но что случилось, что за наважденье?"
    window hide
    show sh upset pioneer at left with dspr
    window show
    sl "{i}Веди, Бенвольо. Чувств сейчас лишусь.{/i}"
    sl "{i}Чума возьми семейства ваши оба!{/i}"
    sl "{i}Я из-за вас стал кормом для червей.{/i}"
    sl "{i}Всё прахом!{/i}"

    window hide
    hide sh
    hide sl
    with dissolve
    window show
    "Увёл Славяну Шурик за кулисы,"
    "В прострации оставив нас с Алисой."
    "В смятенье - будет правильней сказать."
    th "Бежать за ней - иль пьесу продолжать?"
    "Не слушаются ноги. Руки тоже."
    window hide
    show sh upset pioneer at left
    hide dv
    with dspr
    window show
    "Вот Шурик. Вид спокойный. Мы продолжим?"
    sh "{i}Ромео, наш Меркуцио угас.{/i}"
    sh "{i}Его бесстрашный дух вознёсся к небу,{/i}"
    sh "{i}С презреньем отвернувшись от земли.{/i}"
    me "{i}Но... В целом-то она... Точней... Он как?{/i}"
    show sh serious pioneer at left
    sh "{i}...Мёртв совершенно.{/i}"
    me "{i}Недобрый день. Одно убийство это -{/i}"
    me "{i}Грядущего недобрая примета.{/i}"
    me "{i}...Не говоря о том, что плохо по себе.{/i}"
    me "{i}Приметы забывать немаловажно.{/i}"
    show sh rage pioneer at left
    sh "Что ты лопочешь, не в своём уме?"
    "- Шепнул тогда сердитый Шурик мне."
    show sh upset pioneer at left
    show dv sad cos at right
    sh "{i}Ты видишь, вот опять Тибальт кровавый!{/i}"
    me "{i}Как, невредим и на вершине славы?{/i}"
    me "{i}Меркуцио убит, но дух его{/i}"
    me "{i}Ещё не отлетел так далеко,{/i}"
    me "{i}Чтобы тебя в попутчики не жаждать.{/i}"
    me "{i}Ты или я разделим этот путь.{/i}"
    play sound sworddraw
    show dv angry cos at right
    dv "{i}Нет, только ты. Ты в жизни с ним якшался,{/i}"
    dv "{i}Ты и ступай!{/i}"
    play sound sworddraw
    me "{i}Ещё посмотрим, кто!{/i}"
    "Теперь и нам с Алисой смертный бой"
    "На сцене надлежит изобразить,"
    play sound swordclash
    "Вот только в этот раз её герой"
    "Уже не должен в схватке победить."
    show dv shocked cos at right
    play sound swordswipe
    "Я сделал выпад, и Тибальт повержен."
    hide dv
#   "Хоть дроп не выпал, killing spree прерван."
    "Зал зрительный теперь слегка рассержен."
    "Сочувствуют Тибальту? Нет, едва ли."
    "Скорее, выходок Алисы ждали."
    show sh surprise pioneer at left
    sh "{i}Беги, Ромео! Живо! Горожане{/i}"
    sh "{i}В движенье. Ты Тибальта заколол.{/i}"
    sh "{i}Тебя осудят на смерть за убийство.{/i}"
    sh "{i}Что ты стоишь? Немедленно беги!{/i}"
    "Два раза и не надобно просить."
    stop music fadeout 5
    "Быстрей со Славей жажду говорить."
    window hide
    scene bg ss_bstage
    show sl sad cos at right
    with dissolve
    window show
    me "Что там случилось, Славя? Ты цела?"
    "Алиса тоже к Славе подошла."
    show dv sad cos at left
    dv "Поранила? Специально бы не стала!"
    dv "Казалось, тебя шпагой не достала..."
    show dv guilty cos at left
    play music Warm_evening fadein 5
    sl "На сцене глупость сделала, ребята."
    sl "Не сорвала чуть пьесу. Виновата."
    sl "Не кровь то. Это был свекольный сок,"
    show dv surprise cos at left
    sl "Хотела всем преподнести урок."
    sl "Реакцию народа посмотреть -"
    sl "Вдруг плакать будет кто? А вдруг жалеть?"
    show dv normal cos at left
    sl "Придумала, но делать не хотела,"
    sl "Пред боем разозлилась, осмелела."
    menu:
        "C ума сошла?":
            $ sp_sl += 1
            me "Ты спятила?! Как можно так шутить?"
            me "Вожатой папы чуть не стал клиентом!"
            me "Собрался к медсестре тебя тащить,"
            me "Понёс бы, не хватило лишь момента!"
            #th "Ещё чуть-чуть, казалось, - и вот-вот"
            #th "Кирпичный заработает завод."
            show dv guilty cos at left
            dv "Но Славя вовремя остановилась."
            show sl surprise cos  at right
            th "С чего Алиса за неё вступилась?"
##          dv "Не думал ты, что я в неё влюбилась?.."     ## Почему бы и нет, гуляем на все
            sl "На сцене поняла, как будет худо."
            show sl normal cos at right
            sl "Прости меня. Я больше так не буду."


        "Главное, что ты в порядке":
            $ sp_dv -= 1
            me "Наверное, я должен был бы злиться,"
            me "Что ты могла на шаг такой решиться."
            me "Но рад лишь, что беда не приключилась,"
            show sl normal cos at right
            me "К тому же ты от плана отступилась,"
            me "И нервы потрепала только мне."
            dv "Мои же - пострадали там вдвойне."
            dv "Вы удивитесь, но я не убийца."
            dv "И не желаю ею становиться."


    me "И как бы людям после объяснила?"
    me "Что смеха ради пьесу провалила?"
    sl "Сказала бы - то часть игры, конечно,"
    sl "Чтоб было публике смотреть потешно."
    sl "И мне бы с рук оно легко сошло."
    show dv normal cos at left
    sl "Ведь перегнуть немного - не грешно?"
    dv "Признаюсь, я была несправедлива,"
    show sl surprise cos at right
    dv "Ты не стукачка, вовсе не труслива."
    dv "Листки взяла не ты, но их вернула"
    dv "И фокус с соком ловко провернула."
    dv "Не стала нам комедию ломать."
    dv "Девчонка ты что надо. Не отнять."
    show sl smile cos at right
    dv "Не то чтоб мы могли друзьями стать."
    dv "Кто правильным пытается предстать,"
    show dv guilty cos at left
    dv "А сам... Хотя на этом мы прервёмся."
    dv "Не лезь ко мне - и миром разойдёмся."
    show dv normal cos at left
    dv "Обиды забываем. Ты согласна?"
    sl "Конечно. Это было бы прекрасно."
    sl "Прощенья от тебя не ожидала..."
    dv "Раз шалостей я много совершала,"
    dv "То, опыт я имея и закалку,"
    stop music fadeout 5
    dv "Ценю в других и дерзость, и смекалку."
    window hide
    $ renpy.pause(1.3)
    scene bg verona_church
    show mz normal cos
    with dissolve
    $ renpy.pause(0.6)
    window show
    me "{i}Отец, какие вести?{/i}"
    me "{i}Какие новые напасти? Что приговор гласит?{/i}"
    play music music_list["your_bright_side"] fadein 5
    mz "{i}Ты прав, с тобою в дружбе беды все.{/i}"
    mz "{i}Я весть принёс о княжеском решенье.{/i}"
    me "{i}Он дело переносит в страшный суд?{/i}"
    mz "{i}О нет, зачем? Его решенье мягче:{/i}"
    mz "{i}Ты к ссылке, а не к смерти присуждён.{/i}"
    me "{i}О, лучше сжалься и скажи, что к смерти!{/i}"
    me "{i}Вне стен Вероны жизни нет нигде.{/i}"
    mz "{i}Неблагодарный! Ты ведь по закону{/i}"
    mz "{i}Достоин смерти, а остался жить.{/i}"
    me "{i}Какая это милость! Это месть.{/i}"
    me "{i}Небесный свод есть только над Джульеттой.{/i}"
    me "{i}Собака, мышь, любая мелюзга{/i}"
    me "{i}Живут под ним и вправе с ней видаться,{/i}"
    me "{i}Но не Ромео.{/i}"
    play sound sfx_knock_door2
    mz "{i}Стучат. Вставай. Скорей, Ромео! Прячься!{/i}"
    window hide
    $ renpy.pause(1.0)
    show un normal cos at right with dissolve
    window show
    un "{i}Святой отец, скажите, где Ромео?{/i}"
    mz "{i}Он на полу. Мертвецки пьян от слёз.{/i}"
    show un sad cos at right
    un "{i}Какое совпаденье! Точь-в-точь она.{/i}"
    un "{i}Вот так лежит и плачет.{/i}"
    me "{i}Что с ней? Я, верно, ей кажусь злодеем?{/i}"
    un "{i}Что говорит? Ревёт, ревмя ревёт.{/i}"
    un "{i}То на постель повалится, то вскочит,{/i}"
    un "{i}То закричит \"Ромео\", то \"Тибальт\".{/i}"
    me "{i}Ромео! Ах, это имя - гибель для неё.{/i}"
    show un normal cos at right
    un "{i}Вы встаньте, сударь, встаньте!{/i}"
    un "{i}Мужчина Вы, Вам слёзы не к лицу.{/i}"
    show mz bukal cos
    mz "{i}Встань, человек! Жива твоя Джульетта. Это счастье.{/i}"
    mz "{i}Пойди к Джульетте ночью на свиданье,{/i}"
    mz "{i}Как решено, и успокой её,{/i}"
    mz "{i}Но возвращайся до обхода стражи,{/i}"
    mz "{i}А то не сможешь в Брюгге ты попасть.{/i}"
    mz "{i}Заляг на дно, пока найдётся повод{/i}"
    mz "{i}Открыть ваш брак и примирить дома.{/i}"
    mz "{i}Потом упросим, чтоб тебя вернули,{/i}"
    mz "{i}И радость будет в двести раз сильней,{/i}"
    mz "{i}Чем горе нынешнего расставанья.{/i}"
    me "{i}Звучит как план!{/i}"
    un "{i}Скажу, что вы придёте, доложу.{/i}"
    me "{i}Как ожил я от этого всего!{/i}"
    mz "{i}Спокойной ночи. А теперь ступай.{/i}"
    $ renpy.pause(0.7)
    th "Спокойной? Лучше доброй пожелай."
    window hide
    $ renpy.pause(0.8)
    scene bg ss_bstage
    with dissolve
    window show

    "Ромео убежал. Сижу за сценой."
    "Неподалёку замечаю Лену."
    stop music fadeout 5
    window hide
    show un normal cos at right with dissolve
    window show
    if d6_treasure == 0:
        me "Кормилица. Ну, как у нас дела?"
        un "Нормально... Если я не подвела."
        me "Не показался странным поединок?"
        un "Я отошла, чтоб текст свой повторить."
        un "Боюсь, что не сыграю без запинок..."
        me "Не отвлекаю. Продолжай учить."
        jump d6_zhavoronokedit

    "Мы в первый раз наедине остались"
    "С тех пор, как перед ужином расстались."
    if d6_treasure == 2:
        jump d6_untalkbsedit
    if d6_treasure == 1:
        th "У ней на лбу написано \"всё сложно\"."
        th "Проведать, что случилось, осторожно?"
        menu:
            "Спросить":
                jump d6_untalkbsedit
            "Не спрашивать":
                th "Всё остров, чем-то он её расстроил."
                th "С ней деликатней надо, я усвоил."
                th "И совесть у молчащего чиста..."
                th "Само всё встанет на свои места."
                jump d6_zhavoronokedit

label d6_untalkbsedit:
    window hide
    show un normal cos
    with dissolve
    window hide

    me "Лен, слушай. У тебя всё хорошо?"
    un "Наверно, ты мой жалким вид нашёл?"
    me "Вид замечателен. Но дай ответ,"
    me "Всё у тебя нормально? Или нет?"
    window hide
    $ renpy.pause(1.0)
    show un sad cos with dspr
    window show
    th "Ох, чувствую, что лезу к Лене зря."
    play music music_list["meet_me_there"] fadein 5
    $ renpy.pause(0.7)
    un "В порядке ли? Нет. Я... {w}Люблю тебя."
    "..."
    un "Уходит лето, вдаль летя стремглав,"
    un "И если наблюдать продолжу молча,"
    un "То ты исчезнешь, так и не узнав"
    un "О чувствах, сладить с коими нет мочи, -"
    un "О тех, что сердце мне щемят порой,"
    un "О тех, которыми душа согрета."
    show un cry_smile cos   with dspr
    un "В «Совёнке» я утратила покой,"
    un "И вижу я себя твоей Джульеттой."
    window hide
    show un sad cos with dspr
    $ renpy.pause(0.5)
    window show
    un "Молю, затишья словом не нарушь"
    un "И не терзай меня ты взглядом острым!"
    un "Наступит для волнений час досуж,"
    un "А ныне роль сыграть должна я сносно."
    un "Но после всех финальных слов, когда"
    un "Дозволено нам будет скинуть маски,"
    un "Найдёшь меня, где лодки и вода, -"
    un "Такую оставляю я подсказку."
    show un cry cos
    un "Наверное, сочтёшь меня дурной..."
    un "И поделом: на то имеешь повод,"
    un "Минувшие грехи тому виной."
    un "Но не ищи в моих речах уловок:"
    un "Не думай, не придя на рандеву,"
    un "Отринув безрассудную девицу,"
    un "Что навлечёшь какую тем беду,"
    un "Что сумасшедшая пойдёт топиться,"
    un "От горя не в себе, дрожа от слёз,"
    un "Ромео не дождавшись на причале..."
    un "Нет, не таю подобных я угроз,"
    un "Такого не случится, обещаю!"
    menu:
        "Я приду":
            $ sp_un += 1
            me "Как ты просила, я готов молчать,"
            me "Но кое-что ответить я обязан,"
            show un cry_smile cos
            me "Сомнения твои чтоб развенчать."
            me "Ты выдумки отбросить можешь сразу:"
            me "На пристань обязательно приду."
            stop music fadeout 3
            window hide
            show un smile cos  with dspr
            window show
            me "Надеюсь, что тебя я там найду."
            $ renpy.pause(0.5)
            $ d6_unchoice = 3
            jump d6_zhavoronokedit

        "Промолчать":
            $ sp_un -= 1
            stop music fadeout 4
            th "Спасибо, что молчать меня просила."
            th "Вот новость, что сказать? Неловко б было."
            $ d6_unchoice = 2
            jump d6_zhavoronokedit

        "Не приду":
            $ sp_un = 2
            $ d6_unchoice = 1
            me "Выбрасывай меня из головы:"
            show un surprise cos
            me "Не пристань не приду к тебе, увы."

            me "Твои безмерно чувства уважая,"
            me "К несчастию, я их не разделяю."
            me "И дружбой откупаться не намерен."
            me "Так будет только хуже, я уверен."
            show un cry cos
            me "Но просьба есть о милости благой,"
            me "В которой отказал минутой прежде."
            me "Позволь мне быть любимым для другой..."
            me "Могу об этом я питать надежду?"

            window hide
            $ renpy.pause(1.0)
            show un angry2 cos  with dspr
            $ renpy.pause(0.5)
            window show
            un "Не ожидала я подобных слов,"
            un "Спокоен будь: не затаю обиду."
            un "Мной более любим не будешь вновь,"
            un "Отныне будешь мною ненавидим."
            un "И хоть жестокий дал ты мне ответ,"
            un "Мне лучше. Больше муки в сердце нет."

            stop music fadeout 4
            th "Надеюсь, Лена правду говорит,"
            th "Пусть неприязнь ей сердце исцелит."
            $ renpy.pause(0.5)
            jump d6_zhavoronokedit

#   un "Тебе пора на сцену возвращаться."
#   un "А мне немного с мыслями собраться..."

label d6_zhavoronokedit:
    window hide
    $ renpy.pause(0.9)
    scene bg verona_bedroom
    show ss surprise dress
    with dissolve
    if persistent.dress == 'purple':
        $ persistent.ssg_94 = True
    else:
        $ persistent.ssg_95 = True
    $ renpy.pause(0.7)
    window show
#    play music Romeo_And_Juliet    fadein 3
    play music music_list["trapped_in_dreams"] fadein 3
    ss "{i}{en}Wilt thou be gone? It is not yet near day:{/en}{ru}Уходишь ты? Ещё не рассвело.{/ru}{/i}"
    ss "{i}{en}It was the nightingale, and not the lark,{/en}{ru}Нас оглушил не жаворонка голос,{/ru}{/i}"
    ss "{i}{en}That pierced the fearful hollow of thine ear;{/en}{ru}А пенье соловья. Он по ночам{/ru}{/i}"
    ss "{i}{en}Nightly she sings on yon pomegranate-tree:{/en}{ru}Поёт вон там, на дереве граната.{/ru}{/i}"
    show ss smile2 dress   with dspr
    ss "{i}{en}Believe me, love, it was the nightingale.{/en}{ru}Поверь, мой милый, это соловей.{/ru}{/i}"
    me "{i}Нет, это были жаворонка клики,{/i}"
    me "{i}Глашатая зари. Её лучи{/i}"
    me "{i}Румянят облака. Светильник ночи{/i}"
    me "{i}Сгорел дотла. В горах родился день.{/i}"
    me "{i}Мне надо удаляться, чтобы жить,{/i}"
    me "{i}Или остаться и проститься с жизнью.{/i}"
    show ss unsure dress   with dspr
    ss "{i}{en}Yon light is not day-light, I know it,{/en}{ru}Та полоса совсем не свет зари,{/ru}{/i}"
    ss "{i}{en}Therefore stay yet; thou need'st not to be gone.{/en}{ru}Побудь ещё. Куда тебе спешить?{/ru}{/i}"
    me "{i}Пусть схватят и казнят. Раз ты согласна,{/i}"
    show ss sad dress
    me "{i}Я и подавно остаюсь с тобой.{/i}"
    me "{i}Мы пили чай всю ночь, и будем дальше,{/i}"
    me "{i}Как должно делать людям молодым.{/i}"
    show ss surprise dress
    ss "{i}{en}It is, it is: hie hence, be gone, away!{/en}{ru}Нельзя, нельзя! Скорей беги: светает,{/ru}{/i}"
    ss "{i}{en}It is the lark that sings so out of tune{/en}{ru}Светает! Это жаворонка песнь!{/ru}{/i}"
    ss "{i}{en}O, now be gone; more light and light it grows.{/en}{ru}Теперь беги: блеск утра всё румяней.{/ru}{/i}"
    show ss sad dress
    me "{i}Обнимемся. Прощай! Я спрыгну в сад.{/i}"
    me "{i}Я буду посылать с чужбины весть{/i}"
    me "{i}Со всяким, кто её возьмётся свезть.{/i}"
    show ss surprise dress
    ss "{i}{en}O think'st thou we shall ever meet again?{/en}{ru}Увидимся ль когда-нибудь мы снова?{/ru}{/i}"
    me "{i}Наверное. А муки эти все{/i}"
    me "{i}Послужат нам потом воспоминаньем.{/i}"
    me "{i}Прощай, прощай!{/i}"
    window hide
    $ renpy.pause(1.0)
    show unl normal at fright
    show ss nosmile dress
    ssunl "{i}Ты встала, дочь?{/i}"
    ssunl "{i}Отец твой полон о тебе заботы.{/i}"
    ssunl "{i}Чтобы тебя развлечь, я выбрал день{/i}"
    ssunl "{i}Для праздника. Нам и во сне не снилось{/i}"
    ssunl "{i}Нежданное такое торжество.{/i}"
    show ss surprise dress
    ss "{i}{en}In happy time, what day is that?{/en}{ru}Что ж, в добрый час. Когда назначен праздник?{/ru}{/i}"
    ssunl "{i}В четверг, моя хорошая. В четверг{/i}"
    ssunl "{i}Прекрасный граф Парис, твой наречённый,{/i}"
    show ss angry dress with dspr
    ssunl "{i}С утра нас приглашает в храм Петра,{/i}"
    ssunl "{i}Чтобы с тобою сочетаться браком.{/i}"
    show ss vangry dress
    ss "{i}{en}Now, by Saint Peter's Church and Peter too,{/en}{ru}Клянусь Петровым храмом и Петром,{/ru}{/i}"
    ss "{i}{en}He shall not make me there a joyful bride.{/en}{ru}Ничем с Парисом я не сочетаюсь!{/ru}{/i}"
    show unl upset at fright
    show ss surprise dress
    ss "{i}{en}I wonder at this haste; that I must wed{/en}{ru}Какая спешка! Гонят под венец,{/ru}{/i}"
    ss "{i}{en}Ere he, that should be husband, comes to woo.{/en}{ru}Когда жених и глаз ещё не кажет.{/ru}{/i}"
    ss "{i}{en}I pray you, listen me! I will not marry yet.{/en}{ru}Прошу, услышьте! Замуж рано мне!{/ru}{/i}"
    show ss serious dress with dspr
    ssunl "{i}Ты что, не поняла всей чести?{/i}"
    ssunl "{i}Гордись! Не поняла, во сколько раз жених знатнее нас?{/i}"
    show ss unsure dress with dspr
    ss "{i}{en}Not proud, you have; but thankful, that you have:{/en}{ru}Должна благодарить, но не горжусь.{/ru}{/i}"
    ss "{i}{en}Proud can I never be of what I hate;{/en}{ru}Какая гордость в том, что ненавистно?{/ru}{/i}"
    ss "{i}{en}But thankful even for hate, that is meant love.{/en}{ru}Но и напрасный труд ваш дорог мне.{/ru}{/i}"
    ssunl "{i}Брось эти штуки, всё уж решено!{/i}"
    show ss surprise dress
    ssunl "{i}Что гордость мне твоя и благодарность?{/i}"
    ssunl "{i}А вот в четверг, пожалуйста, изволь{/i}"
    ssunl "{i}Пойти венчаться в храм с Парисом, или{/i}"
    ssunl "{i}Тебя я на верёвке притащу.{/i}"
    show ss cry dress
    ss "{i}{en}Good father, I beseech you on my knees,{/en}{ru}Отец, прошу вас слезно на коленях,{/ru}{/i}"
    ss "{i}{en}Hear me with patience but to speak a word.{/en}{ru}Позвольте только слово мне сказать!{/ru}{/i}"
    ssunl "{i}Ни звука! Все заранее известно.{/i}"
    ssunl "{i}В четверг будь в церкви или на глаза{/i}"
    ssunl "{i}Мне больше никогда не попадайся!{/i}"
    show ss cry2 dress
    ssunl "{i}На размышленье у тебя два дня,{/i}"
    ssunl "{i}И если ты мне дочь, то выйдешь замуж,{/i}"
    stop music fadeout 5
    ssunl "{i}А если нет, скитайся, голодай,{/i}"
    ssunl "{i}Тебе тогда я больше не отец.{/i}"
    window hide
    $ renpy.pause(1.2)
    scene bg ss_bstage
    with dissolve
    window show
    dv "Эй, эй! Ромео! Подойди."
    play music music_list["gentle_predator"] fadein 3
    window hide
    show dv normal cos with dissolve
    window show

    dv "К концу подходит пьеса. Смена тоже."
    dv "Чтоб грусти не бывать - отметим, может?"
    dv "Согласен если, после постановки"
    dv "В наш домик набирается массовка."
    me "Заманчиво. Ещё кто приглашён?"
    dv "Кого ещё позвать, детишек кроме?"
    dv "Ульянки посему не будет в доме."
    dv "Славяна будет точно, кто ещё..."
    me "Ого! Вот уж кого не ожидал."
    show dv guilty cos
    dv "...И звать-то некого мне, если честно."
    dv "А сам бы ты кого ещё позвал?"
    dv "В том смысле, с кем бы ты пришёл совместно?"
    show dv normal cos
    menu:
        "Не приду":
            $ d6_dvchoice = 1
            $ sp_dv -= 1
            me "Увы, я не смогу к вам заглянуть."
            me "Другие планы. Уж не обессудь..."
            dv "Мне стоило тебя спросить сперва."
            dv "О {i}планах{/i} этих донесёт молва..."
            me "Вам будет хорошо и без меня."
            show dv guilty cos
            dv "Сдается мне, мы посидим бездарно."
            dv "Теперь Ульянку можно не гонять,"
            dv "Малая будет очень благодарна."
            show dv normal cos
            dv "Не знаю, на кого нас променял,"
            dv "Но лучше нас не будет, чтоб ты знал."


        "Ещё не решил":
            $ d6_dvchoice = 2
            me "Спасибо, я польщён. Пока не знаю,"
            me "Пусть больше никого не обещаю."
            dv "Ты лучше приходи. Вдвоём уныло."
            dv "Не зря ж Ульянку крова я лишила?"
            me "Не факт, но постараюсь я. Тем боле"
            me "Хочу из дома вырваться на волю."

        "Лену":
            $ sp_un += 1
            $ sp_dv -= 1
            $ d6_dvchoice = 2
            dv "Не стоит, не пойдёт она тем паче,"
            dv "Сама бы подошла я к ней иначе."
            dv "Такое уж бывало, и не раз."
            dv "И получали каждый раз отказ."
            dv "Совет: нас соберёшься навестить -"
            dv "Не вздумай ты её оповестить."

        "Саманту":
            $ sp_ss += 1
            show dv angry cos
            dv "Да ты, я погляжу, совсем дурак?"
            dv "Двоим вам разделиться, хоть на вечер,"
            dv "Отныне - получается, никак?"
            dv "Нет, видеть не хотим её на встрече."
            me "Не кипятись, а почему бы нет?"
            me "Она куда своих серьёзней лет."
            me "Хоть взрослые напитки отпадают."
            me "Но вечеринки и без них бывают."
            show dv guilty cos
            dv "Так не годится. С ней неловко всем."
            dv "Достаточно, что Славя будет там,"
            dv "А с ней из разных звёздных мы систем."
            show dv normal cos
            dv "Вожатой только не хватает нам!"
            menu:
                "Приду один":
                    $ d6_dvchoice = 3
                    me "Тогда приду один, уговорила."
                    dv "А нужен ли такой нам фантазёр?"
                    show dv laugh cos
                    dv "Ну ладно, приходи. Я пошутила."
                    dv "Разбавите девичник наш, синьор."

                "Тогда я пас":
                    $ d6_dvchoice = 1
                    $ sp_dv -= 2
                    $ sp_ss += 1
                    me "Я пас тогда. Гуляйте до рассвета,"
                    me "А мне тогда не светит пьянка эта."
                    show dv angry cos
                    dv "Иди играй с любимицей в бирюльки!"
                    dv "И молока погрей ещё в кастрюльке,"
                    dv "Чтоб гостья завтра не слегла с простудой..."
                    dv "Поверить не могу, какой зануда!"
                    th "Спасибо, что не кинула вдогонку"
                    th "Навета про пристрастие к ребёнку..."

        "Больше никого не нужно":
            $ d6_dvchoice = 3
            $ sp_dv += 1
            me "Тогда и так компания - что надо!"
            me "Веселье я предвижу до упаду,"
            me "Как штык я буду, то само собой."
#           dv "Тот штык не пригодится, вот усвой."   # есть с собой ### ненуачо?
            show dv laugh cos
            dv "Штыки и сабли все сдаём на входе,"
            dv "Из драк возможен только мордобой,"
            dv "А от ножей сегодня я на взводе."


    window hide
    stop music fadeout 3
    $ renpy.pause(1.0)
    scene bg verona_church
    show ss unsure dress
    show mz normal cos at right
    with dissolve
    $ renpy.pause(0.5)
    window show
    mz "{i}Джульетта, мне твоя печаль известна.{/i}"
    mz "{i}Как быть тебе, ума не приложу.{/i}"
    play music music_list["just_think"] fadein 5
    mz "{i}В четверг, слыхал, твоё венчанье с графом,{/i}"
    mz "{i}И будто отложить его нельзя.{/i}"
    ss "{i}{en}Tell me not, friar, that thou hear'st of this,{/en}{ru}Не говори, раз выхода не видишь.{/ru}{/i}"
    ss "{i}{en}Unless thou tell me how I may prevent it:{/en}{ru}И если ты не можешь мне помочь,{/ru}{/i}"
    ss "{i}{en}If, in thy wisdom, thou canst give no help,{/en}{ru}То оправдай меня по крайней мере,{/ru}{/i}"
    ss "{i}{en}Then with this knife I'll end my life.{/en}{ru}И мне в беде поможет этот нож.{/ru}{/i}"
    show ss sad dress
    show mz bukal cos at right
    mz "{i}Ты говоришь, что вместо свадьбы с графом{/i}"
    mz "{i}В себе нашла бы силу умереть?{/i}"
    mz "{i}Что ж, если так, есть средство вроде смерти.{/i}"
    show ss surprise dress
    mz "{i}Я дам его, но тут нужна решимость.{/i}"
    show mz smile cos at right
    mz "{i}Продам, вернее. Вещь цены немалой.{/i}"
    mz "{i}Хоть не пытаюсь я набить карманы...{/i}"
    mz "{i}Но больно дороги те компоненты -{/i}"
    mz "{i}Мышиные хвосты и экскременты.{/i}"
    show mz bukal cos at right
    mz "{i}К нам с жалобой назад не возвращались.{/i}"
    $ renpy.pause(0.5)
    mz "{i}Возможно, впрочем, что не просыпались...{/i}"
    show ss serious dress   with dspr
    ss "{i}{en}I will do anything without fear or doubt{/en}{ru}Без колебаний это совершу,{/ru}{/i}"
    ss "{i}{en}To live an unstain'd wife to my sweet love{/en}{ru}Чтоб не нарушить верности Ромео.{/ru}{/i}"
    show mz normal cos at right
    mz "{i}Тогда ступай уверенно домой,{/i}"
    mz "{i}Ляг и пред сном откупорь эту склянку.{/i}"
    mz "{i}Когда ты выпьешь весь раствор до дна,{/i}"
    mz "{i}Тебя скуёт внезапный холод. В жилах{/i}"
    mz "{i}Должна остановиться будет кровь.{/i}"
    mz "{i}Ты обомрёшь. Но не умрёшь совсем.{/i}"
    mz "{i}В тебе не выдаст жизни ни слабый вздох, ни след тепла.{/i}"
    mz "{i}Когда тебя придёт будить Парис,{/i}"
    mz "{i}Ты будешь мёртвой. Как у нас обычай,{/i}"
    mz "{i}Тебя сей час в ваш склеп снесут фамильный.{/i}"
    mz "{i}Я вызову Ромео. До того{/i}"
    mz "{i}Как ты проснёшься, мы с ним будем в склепе.{/i}"
    mz "{i}Вы сможете уехать в ту же ночь.{/i}"
    show ss surprise dress   with dspr
    ss "{i}{en}Give me, give me! O, tell not me of fear!{/en}{ru}Дай склянку мне! Не говори о страхе.{/ru}{/i}"
    show ss nosmile dress
    mz "{i}Возьми её. Я напишу письмо{/i}"
    mz "{i}К Ромео отправляю с ним монаха.{/i}"
    mz "{i}Его услуги должно оплатить...{/i}"
    mz "{i}Мужайся и решимость прояви!{/i}"
    stop music fadeout 3
    ss "{i}{en}Love give me strength! And strength shall help afford.{/en}{ru}Решимость эту я найду в любви.{/ru}{/i}"
    window hide
    $ renpy.pause(1.0)
    scene bg verona_bedroom
    show un normal cos
    with fade
    window show
    play music music_list["you_lost_me"] fadein 4
    un "{i}Сударыня! Сударыня! Вставай!{/i}"
    un "{i}Пора вставать! Ай-ай, какая соня!{/i}"
    un "{i}Джульетта! Разве будет хорошо,{/i}"
    un "{i}Когда тебя застанет граф в постели?{/i}"
    un "{i}Сударыня! Сударыня! А ну-ка! -{/i}"
    show un surprise cos
    $ renpy.pause(0.5)
    un "{i}Не может быть... Сюда! Она мертва!{/i}"
    un "{i}О господи! О господи! На помощь!{/i}"
    show unl upset at fright
    show un sad cos
    ssunl "{i}Что ты шумишь?{/i}"
    un "{i}Ужасное несчастье! Посмотрите!{/i}"
    show unl sad at fright
    ssunl "{i}Ах, жизнь моя, дитя моё родное!{/i}"
    ssunl "{i}Окоченела. Холодна, как лед.{/i}"
    ssunl "{i}О господи, она давно без жизни!{/i}"
    ssunl "{i}Смерть, взявшая её без сожаленья,{/i}"
    ssunl "{i}Сжимает горло мне, лишает слов!{/i}"
    show mz smile cos at left
    mz "{i}Ну как? Готова в храм идти невеста?{/i}"
    ssunl "{i}Идти - пойдёт, но не придёт назад{/i}"
    ssunl "{i}Её в супруги взял подземный царь.{/i}"
    show mz normal cos
    un "{i}О боже, не глядели бы глаза!{/i}"
    un "{i}Какой проклятый день! Какой проклятый!{/i}"
    ssunl "{i}Моей душой, а не моим ребёнком{/i}"
    ssunl "{i}Была она, и вот она мертва.{/i}"
    show mz bukal cos
    mz "{i}Знать, мало вы любили дочь свою,{/i}"
    mz "{i}Когда не рады, что она в раю.{/i}"
    show unl upset at fright
    ssunl "{i}Да что же вы такое говорите?!{/i}"
    show mz shy cos
    mz "{i}Пожалуй, лишнего сказал, простите.{/i}"
    mz "{i}Я человек для вас чужой, а умничать люблю...{/i}"
    mz "{i}Давайте выполню я роль свою.{/i}"
    show mz normal cos
    show unl sad at fright
    mz "{i}Осыплем это тело розмарином{/i}"
    mz "{i}И вынесем в венчальном платье в храм.{/i}"
    mz "{i}Отправимся. Сударыня, и вы.{/i}"
    mz "{i}Вы тоже, граф. Подите приготовьтесь.{/i}"
    stop music fadeout 3
    mz "{i}Её пора на кладбище нести.{/i}"
    window hide
    $ renpy.pause(0.5)
    window show
    "Свой выход ожидая, рад безделью."
    "Но Славя прервала моё веселье."
    window hide
    scene bg ext_stage_normal_day
    show sl normal cos
    with dissolve
    window show
    "Сырой кафтан - пыталась отстирать."
    "Да разве можно чем-то свёклу взять?"
    "Я молча вид её улыбкой встретил."
    sl "У Жени очень странный текст. Приметил?"
    me "Там слышен политический окрас,"
    me "Должно ли это впрямь заботить нас?"
    me "Скажи, к Алисе собралась ты вправду?"
    sl "Тебе в том что-то показалось странным?"

    sl "К таким вещам я холодна обычно,"
    sl "А хоть и друг позвал бы закадычный..."
    sl "Но тут теперь у нас расклад другой:"
    sl "Алиса - вроде главный недруг мой,"
    sl "Но мира хочет и смогла позвать."
    sl "Стремленье это стоит поддержать."
    me "Возможно, суну в ваше дело нос..."
    me "Намеренья не ставишь под вопрос?"
    show sl surprise cos
    sl "Должна ли? Почему ты так спросил?"
    me "Я много перепалок пережил"
    me "Для веры в то, что всё у вас в порядке"
    me "И кончились взаимные нападки."
    show sl smile cos
    sl "Возможно, я тебя разочарую -"
    sl "В игру мы не играем ни в какую."
    if d6_dvchoice == 1:
        sl "Сказала - отказался ты прийти."
        sl "Неужто времени не смог найти?"
        me "Смогу едва ли. Впрочем, может сдаться,"
        me "Что и удрать из домика удастся..."
        sl "С вожатой вечер в планах? Ох и дивно."
        me "С чего бы? Любит делом нагружать."
        sl "На вечер? Не настолько я наивна."
        me "С ней не живёшь, удобно рассуждать."

    me "Ты в курсе, что там будет алкоголь?"
    show sl serious cos
    me "Не точно, но сей шанс не исключён."
    window hide
    $ renpy.pause(1.0)
    me "Чего молчишь? Ответить уж изволь."
    sl "Наш вечер им не будет омрачён."
    sl "Ну, мой так точно. Я сама не пью."
    sl "Кто пьёт - тот пусть. Чайку себе налью."
    sl "Конечно, пьяных я терпеть не буду."
    sl "Уйду, туда дорогу позабуду."
    window hide
    $ renpy.pause(1.0)
    show sl normal cos
    me "Что за заминка? Там уж пятый акт?"
    sl "Кого-то ждут, пока у нас антракт."
    play music music_list["eternal_longing"] fadein 4
    "На стуле потянулся я, зевнул."
    th "Эх, с радостью сейчас бы прикорнул..."
    th "...И выход свой проспал бы. Вот позор!"
    "А Славя продолжала разговор:"
    show sl smile cos
    sl "Надеюсь, что и ты почтишь визитом."
    sl "Нам будешь попечителем сердитым."
    me "Не знаю, что и ждать от вас, девицы."
    sl "Не жди, чего дурного - не случится."
    me "Хорошего, быть может, ожидаешь?"
    window hide
    show sl surprise cos with dissolve
    window show
    sl "На что конкретно ты мне намекаешь?"
    me "Я? Намекаю? Боже упаси!"
    window hide
    show sl smile cos with dissolve
    window show
    sl "Один разок, и больше не проси."
    me "...Прости меня... О чём мы говорим?"
    sl "...Закусок из столовой одолжим."
    sl "Небось, для этого и зазывала."
    sl "Как Буратино я, вся ценность - ключ,"
    sl "Им никогда не злоупотребляла,"
    sl "Один разок - и баста. Не канючь."
    window hide
    show sl surprise cos with dspr
    $ renpy.pause(0.7)
    window show
    sl "Не так сказала что-то? Покраснел."
    window hide
    $ renpy.pause(1.0)
    show sl scared cos with dspr
    window show
    sl "Семён, кошмар! О чём подумать смел?.."
    sl "К такому повороту не готова!"
    sl "Учти: до свадьбы я не буду... {w}Снова."
    sl "Ещё ведь и Алиса будет там..."
    window hide
    show sl angry cos with dspr
    window show
    sl "Ты так себе представил это? Срам!"
    sl "В присутствии Алисы ты намерен?.."
    sl "Ну нет уж! {w}...Не при ней, по крайней мере."
    window hide
    show sl tender cos with dspr
    window show
    sl "Ох. Что со мной... Нельзя так волноваться..."
    me "О чём ты там? Не будешь... целоваться?"
    window hide
    show sl happy cos with dspr
    window show
    sl "...Конечно же. Неужто не понятно?"
    sl "Улавливаешь верно, что приятно."
    th "Я просто в шоке. Кто мне объяснит,"
    stop music fadeout 3
    th "С чего она такое говорит?"
    window hide
    show sl serious cos with dspr
    $ renpy.pause(1.0)
    window hide
    play music music_list["into_the_unknown"]
    sl "Ну, ладно, Сём. Тебе открою тайну."
    sl "В игре той давней твой удел - печальный."
    sl "Однажды двум известным нам актрисам -"
    sl "Конечно, я о Лене и Алисе -"
    sl "Когда-то «Спортлото» билет достался."
    sl "Он выигрышным, кстати, оказался:"
    sl "Им выпал главный приз - автомобиль."
    sl "Да кто б для них ту «Волгу» поделил?"
    sl "Решили две коварные девицы"
    sl "Устроить меж собой соревнованье:"
    sl "Кому Семёна сердце покорится,"
    sl "Та побеждает в этом состязанье."
    show sl shy cos with dspr
    sl "Победа - затащить тебя в постель,"
    sl "Но не в любой момент, а после пьесы."
    sl "Не так: кто подошёл - тот и успел."
    sl "Девчонки наши - те ещё повесы."
    sl "Кто раньше преуспеет - проиграет."
    show sl serious cos with dspr
    sl "Сейчас тебя не тронут - выжидают."
    sl "Одной Алисе звать тебя неловко -"
    sl "Вот позвала меня. Лишь согласилась,"
    sl "Чтоб помешать в задуманном чертовке."
    sl "Там... Лена на свиданку не просилась?"
    window hide
    $ renpy.pause(0.5)
    show sl surprise cos with dspr
    window show
    sl "Ну-ну, ты что? Спокойнее, Семён."
    sl "Конечно, ты сейчас опустошён."
    sl "Но разве не казался тебе странным"
    sl "Повальный вдруг у девушек успех?"
    sl "Каким ты объяснил самообманом?"
    show sl smile2 cos with dspr
    sl "Растяпа ты, Семён, ну правда. Смех."
    me "...Была ко мне ты дружелюбна тоже."
    me "С растяпою любезной быть негоже?"
    window hide
    $ renpy.pause(1.0)
    show sl sad cos with dspr
    window show
    sl "...Откроюсь. Спор я тоже принимала."
    sl "Секрет я выдав, «Волгу» проиграла..."
    me "Спор на троих? Четвёртая найдётся?"
    show sl normal cos with dspr
    sl "На них глядя, любая увлечётся."
    sl "Теперь ты самый модный охламон."
    sl "Спор - на троих. На этом всё, Семён."
    menu:
        "Не верю!":
            show sl surprise cos
            me "Неправда! Признавайся - это фарс?"
            me "Что, чёрт дери, творится тут у вас?"
            sl "Был спор у нас..."
            me "Хочу назад вернуться!"
            me "Как страшный сон... Хочу домой. Проснуться!"
            window hide
            $ renpy.pause(0.5)
            scene black with fade
            $ renpy.pause(1)
            stop music fadeout 3
            show unblink
            scene bg ext_stage_normal_day
            show sl normal cos
            with fade
            window show
            jump ssd6_wakeupedit

        "Ты ещё можешь выиграть":
            me "Твои весьма коварны оппоненты,"
            me "Я в шоке. Но есть светлые моменты."
            me "Сознаюсь: ты мне нравилась всегда."
            me "Пусть даже не взаимно - не беда."
            me "Давай составим тайный уговор:"
            me "Ты победишь - и кончен разговор."
            window hide
            show sl angry cos with dspr
            $ renpy.pause(1.0)
            window show
            sl "Мне навязали мерзкое пари."
            sl "То был шантаж. Была я спора против."
            sl "Я не в игре. А ты - слюну утри."
            sl "За «Волгой» не гонялась я - напротив,"
            sl "Смирилась, что её мне не видать."
            sl "Ты что же, предлагаешь мне отдать"
            sl "Ночь за машину? Я не так наивна."
            sl "Не кончится на этом, очевидно:"
            sl "Получишь власть - и в ход пойдёт шантаж,"
            if sp_sl >= 8:
                stop music fadeout 5
            sl "В любой момент ты выдашь сговор наш."
            me "Я так не поступлю, ведь ты же знаешь..."
            sl "А я {i}так{/i} - поступлю, ты полагаешь?"
            window hide
            show sl sad cos with dspr
            $ renpy.pause(1.0)
            if sp_sl >= 8:
                jump d6_gooddreamedit

            else:
                window show
                me "Забудем, ладно. Как же быть тогда?"
                sl "Скажу, что победила. Подтвердишь."
                sl "И больше ты, конечно, никогда"
                sl "Ни с кем об этом не заговоришь."
                sl "Что до машины - выкину билет."
                sl "Коль скоро он несёт от спора след,"
                sl "Само его наличие порочит"
                sl "Владелицу. Меня позорит точно."
                me "Напомни мне причину, по которой"
                me "Красоткам откажу в любви я скоро?"
                me "Одной, верней. А может, повезёт"
                me "Успеть к обеим и сорвать джекпот!"
                # Н.: "Обеим", не "обоим". Да и оригинальный вариант здесь очень даже неплох.
                show sl smile cos
                sl "Такую глупость ты творить не вправе,"
                sl "Когда есть шанс служить богине - Славе."
                #sl "Ты даже не имеешь представленья,"
                #sl "Как хорошо... моё благоволенье."
                window hide
                stop music fadeout 4
                $ renpy.pause(1)
                scene black with fade
                $ renpy.pause(1)
                show unblink
                scene bg ext_stage_normal_day
                show sl normal cos
                window show
                jump ssd6_wakeupedit

        "Ты проиграла":
            me "Сюжет припоминаю мелодрамы,"
            me "В которой, охмуряя на спор даму,"
            me "Влюбляется герой неосторожно,"
            me "А той его простить за это сложно."
            me "Хотя обоих после счастье ждёт."
            me "Конечно, не на твой я это счёт."
            me "Я вижу, ты совсем другая. Знаешь..."
            window hide
            show sl angry cos with dspr
            $ renpy.pause(0.5)
            window show
            sl "Ах, вот как? И кого из них прощаешь?"
            sl "Неважно. Хоть обеих. Твоё дело."
            sl "Очистить совесть только я хотела."
            stop music fadeout 3
            window hide
            $ renpy.pause(1)
            scene black with fade
            $ renpy.pause(1)
            show unblink
            scene bg ext_stage_normal_day
            show sl normal cos
            window show
            jump ssd6_wakeupedit

label d6_gooddreamedit:
            window show
            play music music_list["confession_oboe"] fadein 5
            me "Прости мне неучтивые слова."
            me "Язык свой контролировать едва"
            me "Я мог: в уме картины нас с тобой"
            show sl surprise cos
            me "Затмили чрезвычайно разум мой."
            me "Твой образ мне сияет, как звезда."
            me "Так не любил я, Славя, никогда..."
            me "И я ни на секунду не представил,"
            me "Чтоб спор тебя столь низко пасть заставил..."
            show sl normal cos
            me "Не важно: будь ты ангел, будь ты чёрт!"
            me "Коль ведьма - пусть народ меня сожжёт,"
            me "Коль Дьявол - пусть меня низвергнут в ад,"
            me "Но ты... добро. Нет! Лучше во сто крат!"
            me "Добро звать должно Славей. Эталон"
            me "Ниспослан миру. Чтить его - закон!"
            me "Закону буду счастлив я служить."
            show sl surprise cos
            sl "Я разрываюсь. Как мне поступить?"
            sl "За лесть тебе бы надо наподдать."
            sl "Пинками, да подальше бы прогнать!"
            window hide
            $ renpy.pause(0.5)
            show sl shy cos with dspr
            window show
            sl "Однако... Это было мне так мило."
            sl "А я была с тобой несправедлива."
            sl "Теперь же таю, как в духовке масло."
            sl "В момент, когда надежда уж угасла,"
            sl "Ты разорвал клуб злобы и интриг -"
            sl "Весь тихий ужас, что меня постиг."

            window hide
            $ renpy.pause(0.5)
            show sl tender cos with dspr
            window show
            sl "Я уступаю. Я с тобой. Тут ясно."
            #sl "Отныне я твоя навеки, ясно?" #сон же, чего стесняться?
            me "Ах, Славя, дорогая! Как прекрасно!"
#           "Я ласке и какой-то теплоте отдался."
#           sl "Проснись, Семён! Ты обоспался!
            window hide
            $ renpy.pause(1.0)
            scene black with fade
            $ renpy.pause(1)
            show unblink
            scene bg ext_stage_normal_day
            show sl normal cos
            window show
            th "Что за несчастье? То был только сон?"
            th "Вернуться бы сейчас в него обратно,"
            th "Пока мой разум не покинул он,"
            th "Хочу туда, где было так приятно."
            th "Хочу ту Славю, эту хуже знаю."
            th "Хотя и эта неплоха, признаю."
            th "Но с этой мы едва-едва дружили,"
            th "А с той как будто век в любви прожили."
            th "Что за слова я ей сказал тогда?"
            th "...Уже забыл. Не вспомнить никогда."
            th "Без слов, что я не в силах повторить,"
            th "Я Славю не смогу в себя влюбить."
            th "Но всё же, когда будем уезжать,"
            th "Рискну шепнуть, а может, и сказать:"
            me "Однажды жарко мы с тобой любили."
            me "Но почему-то это позабыли."
            show sl surprise cos with dspr
            th "Я только что... Вслух это произнёс?"
            stop music fadeout 4
            th "Как оправдаться? Был я в царстве грёз?"
            sl "Не помню строчек тех. Но будут в тему."
            show sl normal cos with dspr
            sl "Но поздно повторять! Бегом на сцену!"
            window hide
            $ renpy.pause(1)
            jump ssd6_bruedit





label ssd6_wakeupedit:
    sl "Проснись и пой! Хотя бы декламируй."
    sl "Давай уже на сцену дефилируй!"
    th "Мне всё это приснилось? Вот так раз."
    me "...А побеждала ль ты в лото у нас?"
    me "Речь шла об этом или мне приснилось?"
    "Мотает головой. Всё прояснилось."
    window hide
    $ renpy.pause(1.0)

label ssd6_bruedit:
    scene bg verona_square
    show sh upset pioneer
    with dissolve
    $ renpy.pause(0.5)
    play music music_list["goodbye_home_shores"] fadein 4
    me "{i}Бенволио? С вестями из Вероны?{/i}"
    me "{i}От моего монаха писем нет?{/i}"
    me "{i}Ну, как жена? Что дома? Как Джульетта?{/i}"
    me "{i}Скажи скорее. Дело только в ней.{/i}"
    me "{i}И всё в порядке, если ей не плохо.{/i}"
    show sh serious pioneer
    sh "{i}Джульетте плохо никогда не будет.{/i}"
    sh "{i}Её останки в склепе Капулетти.{/i}"
    sh "{i}Её душа средь ангелов небес.{/i}"
    sh "{i}Я видел погребение Джульетты{/i}"
    sh "{i}И выехал вас тотчас известить.{/i}"
    sh "{i}Помилуйте меня за эту новость.{/i}"
    me "{i}Что ты сказал? Я шлю вам вызов, звёзды!..{/i}"
    me "{i}Беги в мой дом. Бумаги и чернил!{/i}"
    me "{i}Достанем лошадей и выезжаем.{/i}"
    window hide
    hide sh with dspr
    $ renpy.pause(0.5)
    window show
    me "{i}Джульетта, мы сегодня будем вместе.{/i}"
    me "{i}Обдумаю, как это совершить.{/i}"
    me "{i}Я видел старика - он знает яды,{/i}"
    me "{i}Что людям запрещают продавать.{/i}"
    me "{i}За склянку я его озолочу,{/i}"
    me "{i}От жизни этой отдых получу.{/i}"
    me "{i}Сие душеспасительное зелье{/i}"
    me "{i}Я захвачу к Джульетте в подземелье.{/i}"
    window hide
    $ renpy.pause(0.5)
    scene bg verona_church
    show mz normal cos
    with fade
    $ renpy.pause(0.5)
    window show
    mz "{i}Ужасные я вести получил.{/i}"
    mz "{i}Письмо моё к Ромео - возвратилось.{/i}"
    mz "{i}Посыльный наш монах на полпути{/i}"
    mz "{i}Принял ислам. Уж он в Небесном Царстве.{/i}"
    mz "{i}Сражаться будет там за Гроб Господень{/i}"
    mz "{i}В их этом тухлом Иерусалиме,{/i}"
    mz "{i}Губить людей хороших, вроде нас.{/i}"
    mz "{i}Предателю не быть на побегушках.{/i}"
    mz "{i}Подлец рекомендаций не дождётся от меня.{/i}"
    mz "{i}Паршивая работа. Не того я ждал,{/i}"
    mz "{i}Когда в трактире том его спасал{/i}"
    mz "{i}В день нашей первой встречи роковой -{/i}"
    mz "{i}Когда не сдох он чуть, давясь мацой.{/i}"
    window hide
    show mz bukal cos with dspr
    $ renpy.pause(0.5)
    window show
    mz "{i}О чём я сам с собой тут говорил? Ах, да.{/i}"
    mz "{i}Задержка этих строк грозит несчастьем.{/i}"
    mz "{i}Придётся самому сходить к гробнице:{/i}"
    mz "{i}Джульетта может встать с минуты на минуту{/i}"
    mz "{i}Да не простит, что я ещё не известил Ромео.{/i}"
    mz "{i}И всё же я не буду торопиться...{/i}"
    window hide
    $ renpy.pause(0.8)
    scene bg verona_garden
    show sh normal pioneer
    with fade
    window show
    $ renpy.pause(0.5)
    me "{i}Дай мне кирку и лом. Возьми письмо.{/i}"
    me "{i}Оно к отцу. Теперь дай факел мне.{/i}"
    stop music fadeout 5
    me "{i}Стань в стороне и, что бы ни случилось,{/i}"
    me "{i}Не вмешивайся и держись вдали.{/i}"
    sh "{i}Немедленно уйду, чтоб не мешать.{/i}"
    window hide
    $ renpy.pause(0.5)

label ss_playtombedit:
    scene bg verona_tomb
    with fade
    window show
    play music Romeo_And_Juliet fadein 3
    me "{i}Любовь моя! Жена моя! Тёмный жнец{/i}"
    me "{i}Хоть высосал, как мед, твоё дыханье,{/i}"
    me "{i}Не справился с твоею красотой.{/i}"
    me "{i}Прости меня! Джульетта, для чего{/i}"
    me "{i}Ты так прекрасна? Я могу подумать,{/i}"
    me "{i}Что ангел смерти взял тебя живьём{/i}"
    me "{i}И взаперти любовницею держит.{/i}"
    me "{i}И губы, вы, преддверия души,{/i}"
    me "{i}Запечатлейте долгим поцелуем{/i}"
    me "{i}Со смертью мой бессрочный договор.{/i}"
    me "{i}Пью за тебя, любовь!{/i}"
    me "..."
    me "{i}Ты не солгал, Аптекарь! С поцелуем умираю.{/i}"
    window hide
    $ renpy.pause(1.0)
    show ss unsure dress with fade
    if persistent.dress == 'purple':
        $ persistent.ssg_94 = True
    else:
        $ persistent.ssg_95 = True
    $ renpy.pause(1.0)
    window show
    ss "{i}{en}I do remember well where I should be,{/en}{ru}Я сознаю, где быть сейчас должна.{/ru}{/i}"
    ss "{i}{en}And there I am. Where is my Romeo?{/en}{ru}Я там и нахожусь. Где ж мой Ромео?{/ru}{/i}"
    show ss  scared dress
    ss "{i}{en}My husband in my bosom there lies dead.{/en}{ru}Мой муж лежит со мною, упокоен!{/ru}{/i}"
    show ss cry dress
    ss "{i}{en}What's here? a cup, closed in my true love's hand?{/en}{ru}Что он в руке сжимает? Это склянка.{/ru}{/i}"
    ss "{i}{en}Poison, I see, hath been his timeless end:{/en}{ru}Он, значит, отравился? Ах, злодей,{/ru}{/i}"
    ss "{i}{en}O churl! drunk all, and left no friendly drop to help me after{/en}{ru}Все выпил сам, а мне и не оставил!{/ru}{/i}"
    ss "{i}{en}I will kiss thy lips;{/en}{ru}Но, верно, яд есть на его губах.{/ru}{/i}"
    ss "{i}{en}Haply some poison yet doth hang on them.{/en}{ru}Быть может, с поцелуем смерть найду.{/ru}{/i}"
    show ss shy dress
    "Лежу. Саманта, преклонив колени,"
    "Меня, помедлив, накрывает тенью."
    "Теперь вожатой гнев нас не минует:"
    "Джульетта беззащитный труп целует."
    # иначе: "Она потенциальный труп целует."
    window hide
    show white with dissolve2
    if persistent.dress == 'purple':
        show cg ss_playkiss3p with dissolve2
    else:
        show cg ss_playkiss3r with dissolve2

    $ persistent.ssg_112 = True

    $ renpy.pause (1)
    window show
    "Не в шутку тронули её уста,"
    "И каждый понял: это неспроста."
    if d6_kissing == 2:
        th "Несчастье! Как она пошла на это?"
        th "Теперь обоим оправданья нету,"
        th "А может, поцелуй наш повторила,"
        th "Чтобы меня не одного винили?"
        "Иных причин и не вообразить,"
        "С чего б Сэм так решила поступить."
    else:
        th "Зачем, Саманта? Как же ты решилась?"
        th "Или случайно это приключилось?"
        th "Бедняжка оступилась - не иначе."
        th "Спасибо, не плюётся и не плачет."
        "Иных причин и не вообразить,"
        "С чего б Сэм так решила поступить."
        "А что она об этом говорила,"
        "И на словах как будто разрешила -"
        "Лишь чтоб Ромео не было обидно,"
        "Не испытать ей совести укор."
        "Слова те не всерьёз уж, очевидно,"
        "Зачем на публике терпеть позор?"

    window hide
    $ renpy.pause(0.9)
    hide cg
    hide white
    show ss shy dress
    with dissolve2
    $ renpy.pause(1.3)
    show ss crysmile dress with dspr
    window show
    ss "{i}{en}Yea, noise? then I'll be brief. O happy dagger!{/en}{ru}Я слышу голоса. Пора кончать.{/ru}{/i}"
    ss "{i}{en}This is thy sheath: there rust, and let me die{/en}{ru}Вот ножен содержимое - поможет.{/ru}{/i}"
    window hide
    $ renpy.pause(1.0)
    hide ss with dspr
    $ renpy.pause(1.8)
    show mz normal cos with dspr
    window show
    mz "{i}А вот и я, друзья! Но что же...{/i}"
    show mz bukal cos
    stop music fadeout 5
    mz "{i}Вот лежит Ромео. Вот Джульетта,{/i}"
    mz "{i}Опять тепла и вновь умерщвлена.{/i}"
    mz "{i}Печально зрелище, и мне сулит проблемы.{/i}"
    mz "{i}В своих решениях подростки так поспешны,{/i}"
    mz "{i}За это их Господь и ненавидит.{/i}"
    mz "{i}Вот взять хоть музыку их бесовскую -{/i}"
    mz "{i}Уж за неё одну грехом не будет{/i}"
    mz "{i}Немного по карманам их пошарить,{/i}"
    mz "{i}Свою лишь плату только честно взять.{/i}"
    window hide
    $ renpy.pause(1.0)
    scene bg verona_garden
    show us angry cos
    with fade
    $ renpy.pause(0.5)
    play music music_list["farewell_to_the_past_full"] fadein 3
    us "{i}Что тут случилось? Ищите, кто виновник изуверства.{/i}"
    voice "{i}Вот тут слуга Ромео и монах.{/i}"
    voice "{i}При них орудье взлома. Ими вскрыта{/i}"
    voice "{i}Могила эта.{/i}"
    show unl sad at fright
    ssunl "{i}Глянь, жена, как наша дочка истекает кровью!{/i}"
    show us upset cos
    us "{i}Сдержите горестные восклицанья,{/i}"
    us "{i}Пока не разъяснили этих тайн.{/i}"
    us "{i}Когда я буду знать их смысл и корень,{/i}"
    us "{i}Не буду вас удерживать от мести.{/i}"
    us "{i}Пока пусть пострадавшие молчат.{/i}"
    us "{i}Где эти подозрительные лица?{/i}"
    show mz normal cos at left
    mz "{i}Хоть без вины, как будто главный я.{/i}"
    mz "{i}Так говорят, на первый взгляд, улики.{/i}"
    us "{i}Рассказывай, что ты об этом знаешь.{/i}"
    mz "{i}Я буду краток, коротко и так{/i}"
    mz "{i}Для длинной повести моё дыханье.{/i}"
    $ renpy.pause(0.5)
    window hide
    show us calml cos
    with fade2
    window show
    us "{i}В письме подтверждены слова монаха.{/i}"
    us "{i}Но в случае любом,{/i}"
    us "{i}Сей Божий человек наказан быть не может.{/i}"
    show mz smile cos at left
    mz "{i}Но может быть слегка вознаграждён.{/i}"
    show us dontlike cos
    us "{i}Где вы, непримиримые враги,{/i}"
    show mz bukal cos at left
    us "{i}И спор ваш, Капулетти и Монтекки?{/i}"
    show us upset cos
    us "{i}Какой для ненавистников урок,{/i}"
    us "{i}Что небо убивает вас любовью!{/i}"
    ssunl "{i}Мы больше не враги уж, а собратья по несчастью.{/i}"
    ssunl "{i}Монтекки, руку дай тебе пожму.{/i}"
    show us sad cos
    us "{i}Сближенье ваше сумраком объято.{/i}"
    us "{i}Сквозь толщу туч не кажет солнце глаз.{/i}"
    us "{i}Пойдём, обсудим сообща утраты{/i}"
    us "{i}И обвиним иль оправдаем вас.{/i}"
    us "{i}Но повесть о Ромео и Джульетте{/i}"
    stop music fadeout 3
    us "{i}Останется печальнейшей на свете...{/i}"
    window hide
    $ renpy.pause(1.0)
    show us smile cos
    "Закончено! Осталось поклониться."
    window hide
    $ renpy.pause(1.0)
    hide us
    hide unl
#    show unl normal at cleft
    show sl smile2 cos at center
    show ss normal dress at fleft
    show mz normal cos at cleft
    show dv laugh cos at fright
    show us smile cos at cright
    with fade2
    $ renpy.pause(1.0)
    window show
    "Ах, чудные, но редкие моменты:"
    "Всё сделано, работа завершилась,"
    play sound Clapyell
    "Осталось принимать аплодисменты."
    window hide
    $ renpy.pause(1.5)

    $ persistent.sprite_time = 'sunset'
    $ sunset_time()
    scene bg ss_ext_stage_big_day
    with dissolve
    window hide
    play music music_list["smooth_machine"] fadein 5
    "Актёры, да и зрители, устали."
    "Последние нас спешно покидали."
    "У лицедеев было много дел,"
    "Хоть кто-то и из них - сбежать успел."
    window hide
    $ renpy.pause(1.0)
    if d6_dvchoice >= 2:
        show dv laugh cos with dspr
        dv "А что, неплохо сыграно, Семён!"
        dv "Тебя после заката в гости ждём."
        hide dv with dspr
    $ renpy.pause(0.5)
    window show

    "Задумался, у сцены всё сижу,"
    "И с планом действий не определиться."
    th "Нескучно ремесло, как погляжу."
    th "Махнуть, что ль, в театральное, в столицу?"
    th "Не зря же мне лет двадцать форы дали -"
    th "Ещё сыграть успею в «Аватаре»."
    th "Мне ближе, впрочем, мысли о насущном."
    th "Я чую, вечер выдастся не скучным."
    th "Последние деньки остались смены -"
    th "Когда, как не сейчас, решать проблемы?"

    "А Сэм стоит в раздумиях одна."
    "С ней на мгновенье встретились мы взглядом,"
    "И тотчас же ко мне идёт она,"
    "Но не успела, хоть стоял я рядом:"
    show mt angry panama pioneer
    "Вожатая меж нами возникает,"
    "С собой бесцеремонно увлекает."
    mt "Домой, Семён! Там кое-что обсудим,"
    mt "Округу сценами смущать не будем."
    window hide
    $ renpy.pause(0.5)
    scene bg ext_house_of_mt_sunset
    with dissolve
    show mt normal panama pioneer
    with dissolve
    window show
    me "Какие сцены? Даже это слово"
    me "Оскомину изрядную набило."
    me "Не надо сцен. Мне и без них хреново."
    me "Участвовать ещё в одной - не в силах."
    mt "Годится. Я сама от вас устала."
    mt "В покое вечер провести желала."
    me "Мы поняли друг друга? Очень рад,"
    me "Отбуду по делам, сменив наряд..."
    stop music fadeout 2
    window hide
    scene bg int_house_of_mt_sunset
    with dissolve
    play ambience ambience_int_cabin_evening
    $ renpy.pause(0.3)
    show mt normal panama pioneer
    with dissolve
    window show
    play music music_list["torture"] fadein 5
    mt "Нет, не отбудешь. Всё теперь, пострел,"
    mt "Терпение моё вконец извёл."
    mt "Ты не продолжишь этих гнусных дел,"
    mt "Свои деньки на воле ты провёл."
    mt "Отныне под домашним ты арестом -"
    mt "Продлится он до самого отъезда."
    window hide
    $ renpy.pause(0.5)
    th "Опять попал в ухватистые лапы"
    th "Назойливого местного гестапо."
    th "Спокойно! Главное - её не злить,"
    th "Пусть даже очень хочется грубить."
    me "Я ваше беспокойство понимаю"
    me "И наказанье - тоже принимаю,"
    me "Хоть и имею мнение своё,"
    me "Но вот на вечер планов громадьё."
    me "Когда друзьям я это объясню,"
    me "Вернусь повинность отбывать свою."
    show mt smile panama pioneer
    mt "Ну да, Семён. Хорошая попытка."
    th "Не по себе мне от её улыбки."
    show mt normal panama pioneer
    mt "Надеюсь, повторю, и ты усвоишь:"
    mt "Из домика ты больше не выходишь."
    mt "Коль надо - караул к тебе приставлю."
    mt "Но в домике сидеть тебя заставлю!"
    if (d6_dvchoice) +  (d6_unchoice) >= 3:
        me "Поймите же: явиться я обязан!"
        th "Возможно, не в единственное место..."
        me "Я обещал, почти что клятвой связан!"
        me "Смените чуть условия ареста!"
    else:
        me "Поговорить, поймите, с ней обязан!"
        me "Что было - было, большего не будет."
        me "Уходит лето, день отъезда назван,"
        me "Разъедемся, и каждый всё забудет."
        me "Поймите же, вам нечего бояться,"
        me "И дайте по-хорошему расстаться."

    mt "Семён, твои попытки бесполезны."
    mt "Они меня лишь больше убеждают:"
    mt "Чреда твоих поступков неуместных"
    mt "Большую нам опасность представляет."
    menu:
        "Мне нужно к Саманте":
            me "Не знаю сам, что на неё находит,"
            me "Меж нами ничего не происходит."
            me "Разумны будьте - нечему и быть."
            me "Пустите ей мне это объяснить."
            mt "Не надо строить из себя невинность."
            mt "Отсутствует в словах необходимость:"
            mt "Весь лагерь «ничего» видал отлично,"
            mt "Молчать тут будет более приличным."

        "Мне вовсе не к Саманте":
            me "Решили, что к Саманте я собрался?"
            me "Но я в другом бы месте оказался,"
            me "Иные я могу иметь дела."
            mt "Подробностей не надо - поняла."
            mt "Пока у нас Саманта, ты при ней,"
            mt "И думать о свиданиях не смей!"
            mt "И так сполна уж на тебе вины,"
            mt "Засим твои дела отменены."
            mt "Мне одного тут с вами не хватало -"
            mt "С другою чтоб она тебя застала!"
            me "То не свиданье - лишь поговорить!"
            me "Нас не найдут, позвольте мне сходить."
            mt "Нет, я за вами бегать не согласна -"
            mt "Сиди-ка дома, будет безопасно."

        "Мне нужно к Лене" if (d6_unchoice >= 2):
            me "Вообще-то, я спешу на встречу с Леной."
            mt "Считаешь, это что-то переменит?"
            me "Пожалуй, может много изменить..."
            mt "С чего ты мне изволил сообщить?"
            mt "Спешить хоть к Лене, хоть к оленю можешь."
            mt "Есть разница, к кому ты НЕ уходишь?"
            menu:
                "Лена в опасности":
                    stop music fadeout 3
                    me "У Лены очень тонкая натура,"
                    me "Опасно обещать и не прийти."
                    me "Пока мы будем тут сидеть понуро,"
                    play music music_list["afterword"] fadein 3
                    me "Несчастье может с ней произойти."
                    show mt sad panama pioneer
                    mt "Что за беда? О чём ты говоришь?"
                    me "На пристани со мною встречи ждёт."
                    me "Вот не дождётся - и, того глядишь..."
                    me "Привяжет камень да на дно пойдёт."
                    show mt angry panama pioneer
                    mt "Такое говорить тебе не стыдно?"
                    mt "Врёшь не краснея! Ничего не будет."
                    me "Я думал, это вам должно быть видно."

                    me "Её проведать должно. Не убудет."
                    show mt sad panama pioneer
                    mt "Я - не пущу, и вместе - тоже нет."
                    me "Так не сидите. Совесть же заест!"
                    mt "Других я тоже не могу позвать -"
                    mt "Придётся слишком много объяснять."
                    me "Идите. Обещаю: не сбегу."
                    me "Коль не грозит беда ей - ждать могу."
                    mt "Обманешь и сбежишь - не пощажу!"
                    mt "К кровати цепью ржавой привяжу."
                    me "Могу я Лене пару строк черкнуть?"
                    me "Вы справитесь с соблазном заглянуть?"
                    window hide
                    $ renpy.pause(1.0)
                    window show
                    mt "Давай сюда. А сам сиди тут тихо!"
                    mt "А коль за дверь полезешь - будет лихо!"
                    window hide
                    hide mt with dspr
                    $ renpy.pause(1.0)
                    window show
                    "Ушла. И чем же мне теперь заняться:"
                    "Бежать отсюда? Или оставаться?"
                    "Глядишь, останусь, и она смягчится,"
                    "Моя свобода вскоре возвратится..."
                    window hide
                    menu:
                        "Уходить":
                            "Минуту выждав, всё же ухожу."
                            $ persistent.sprite_time = 'night'
                            $ night_time()
                            window hide
                            play ambience ambience_forest_night
                            scene bg ext_house_of_mt_night with dissolve
                            th "Уж темень за порогом, я гляжу."
                            window hide
                            stop music fadeout 1
                            $ renpy.pause(1.0)
                            play sound sfx_scary_sting
                            show mt angry panama pioneer
                            play music music_list["torture"] fadein 1
                            mt "Вот ты мне и попался, негодяй!"
                            mt "А ну, давай обратно полезай!"
                            th "Обман! Она стояла за углом."
                            mt "Тебе не стыдно быть таким вруном?"
                            me "...А если не полезу? Что тогда?"
                            me "Физически я вас куда сильнее,"
                            me "И не догнать меня вам никогда."
                            me "Так, кстати, будет только веселее."
                            me "Я добровольно дал себя держать,"
                            me "Нельзя людей так просто запирать."
                            mt "Однако, если выйдешь за порог -"
                            mt "Я сдам тебя в милицию, щенок!"
                            mt "Ты в лагере устроил саботаж,"
                            mt "Когда живёт в нём гость почётный наш!"
                            mt "А гостя мы тотчас домой отправим."
                            mt "Так хочешь? Нет? А ну, марш в дом теперь!"
                            mt "Рад будь, что всё по-тихому уладим."
                            mt "В твоих же интересах всё, поверь."
                            jump ssd6_zakedit

                        "Ждать":

                            $ persistent.sprite_time = 'night'
                            $ night_time()
                            show bg int_house_of_mt_night with fade
                            "Та стрелка, что в ответе за минуты,"
                            "На полный циферблат уж провернулась,"
                            "Когда, довольно злая почему-то,"
                            "Вожатая в наш домик возвернулась."
                            show mt normal panama pioneer
                            mt "На лодке твоя Лена уплыла,"
                            mt "Кому расскажешь... Еле догнала!"
                            mt "Устроили какую-то регату..."
                            mt "Из-за твоей фантазии богатой."
                            mt "...От ней и для тебя теперь записка."
                            mt "Хоть кончена на этом переписка?"
                            window hide
                            show backnote with dissolve:
                                pos (700,400)
                            th "Без подписи и родовой эмблемы,"
                            th "А всё ж понятно, что от нашей Лены..."
                            hide backnote
                            me "Спасибо и за эту вам заботу,"
                            me "Но лучше бы вернули мне свободу."
                            jump ssd6_zakedit


                "Она тоже личность":
                    me "Я вижу, наказать меня готовы,"
                    me "А равно и друзей моих абстрактных."
                    me "Но имя я назвал, прошу я снова"
                    me "Вас отойти от методов бестактных."
                    me "Жизнь травите вы людям беспричинно,"
                    me "Их взаперти в домах держа злочинно."

                    mt "Пойми, на это я пойти готова."
                    mt "История совсем твоя не нова,"
                    mt "Но есть нюанс, открою, если хочешь:"
                    mt "На сей раз репутацию подмочишь"
                    mt "Не лагерю, а, может, всей стране -"
                    mt "Об этом только нужно думать мне."
                    mt "Всегда работу делала исправно,"
                    mt "И в этот раз так будет и подавно."
                    jump ssd6_zakedit

label ssd6_zakedit:
    window hide
    stop music fadeout 2
    $ renpy.pause(0.8)
    $ persistent.sprite_time = 'night'
    $ night_time()
    play ambience ambience_int_cabin_night
    scene bg int_house_of_mt_night with dissolve
    $ renpy.pause(0.8)
    window show
    play music music_list["faceless"]  fadein 5
    th "По-всякому пытался уболтать;"
    th "По-доброму сегодня не удрать."
    th "Уйти? Она, конечно, не отстанет,"
    th "Ещё со мной, наверно, драться станет."
    th "Никто не разберёт, что я был прав,"
    th "Лишь всё усугублю, вот так удрав."
    th "...Конечно, я судьбе не повинуюсь,"
    th "Но как-нибудь потише ретируюсь."
    th "Или погромче? Так её достать,"
    th "Что ей меня захочется прогнать?"
    me "Вот почему вы к нам такая злая?"
    me "Вас, может, комплекс к этому склоняет?"
    me "Вам запрещали, может, так дружить?"
    me "Откройте эту тайну. Что таить?"
    show mt smile pioneer
    mt "Я добрая ещё. Ты обожди."
    mt "Как сможешь хорошенько разозлить -"
    mt "На складе разместишься под замком."
    mt "Ну что, Семён, желаешь новый дом?"
    window hide
    $ renpy.pause(0.6)
    hide mt with dspr
    $ renpy.pause(1.0)
    window show

    "Так час прошёл. Моё уходит время."
    th "Как глупо! Даже не могу поверить."
    th "Провёл я много вечеров пустых,"
    th "Сегодняшний - совсем удар под дых."
    th "Сам виноват - на дерзость не решился."
    stop music fadeout 3
    th "Потерян вечер. С этим я смирился."
    th "Как у Алисы там дела со Славей?"
    th "Вдруг без меня они получше сладят?"
#    play music Girls_Just_Want_To_Have_Fun fadein 2
#    window hide
#    $ renpy.pause(1.0)
#    show cg dv_sl with dissolve2
#    $ renpy.pause(5.0)
#    hide cg with dissolve2
#    stop music fadeout 3
    mt "Что без толку на потолок глядеть?"
    mt "Из клетки сей, орёл, не улететь."
    mt "Не кисни так. В другой приедешь раз."
    mt "Глядишь, и Ольга будет не указ."
    if d6_treasure >= 1:
        me "Не раскисать? Так сами развлеките."
        me "Историю с заколкой расскажите."
        me "А то она мне не даёт покоя."
        window hide
        show mt normal pioneer with dspr
        window show
        mt "Я лучше б рассказала что другое..."
    else:
        window hide
        show mt normal pioneer with dspr
        window show
        mt "Я слышала, у вас листки пропали?"
        mt "Как же без них вы пьесу доиграли?"
        me "Спасибо Славе - вовремя вернула."
        show mt smile pioneer with dspr
        mt "Она сама их, что ли, умыкнула?"
        me "Не может Славя ничего украсть."
        mt "Уверен так? Сама тебе сказала?"
        me "Она не хуже вас, чтоб воровать."
        me "Уж вы-то сами ничего не крали?"
        window hide
        $ renpy.pause(0.5)
        show mt scared  pioneer with dspr
        $ renpy.pause(1.0)

        me "...Так тут у нас воровка! Не молчите,"
        me "Подробности скорее расскажите."
        mt "То юных лет ошибка... ерунда."
        me "Вам ерунду поведать - нет труда!"

    show mt normal  pioneer
    mt "О многом не придётся рассказать..."
    me "Мне интересно, я готов внимать."
    play music music_list["waltz_of_doubts"] fadein 7
    mt "Внимай тогда. Жила-была заколка."
    mt "Жила она на магазинной полке,"
    mt "В галантерейной лавке за углом."
    show mt smile  pioneer
    mt "Мы были часто в магазине том."
    mt "Заколки те девчонки полюбили,"
    mt "Иные много штук себе купили."
    mt "Но мне тогда мать денег не дала:"
    mt "За что-то я наказана была."
    mt "Зараза ж не давала мне покоя."
    show mt normal  pioneer
    mt "Из-за неё решилась на лихое,"
    mt "Уж слишком дрянь мозолила глаза:"
    mt "Цвета, узоры... яхонт, бирюза..."
    mt "Пошла без денег в этот магазин -"
    mt "Неслыханный то был адреналин!"
    mt "Вот так заколку эту я стащила..."
    me "Вас не заметили там? Не побили?"
    mt "Тогда никто об этом не узнал."
    mt "Но дома мой энтузиазм пропал."
    show mt sad  pioneer
    mt "К заколке я не прикасалась даже,"
    mt "А на душе день ото дня всё гаже..."
    mt "А скольких это стоило мне нервов,"
    mt "Чуть что, пугалась милиционеров."
    mt "И почтальонов. Да любого в форме."
    mt "Мерещился тогда такой позор мне."
    mt "Отличница, пример для всех была..."
    mt "Узнали б - со стыда бы померла!"
    window hide
    $ renpy.pause(0.4)
    show mt normal  pioneer with dspr
    $ renpy.pause(0.4)
    window show
    mt "Чем кончилось, ты спросишь? Да ничем."
    mt "Зарыла я её - и нет проблем."
#    mt "Отличный способ, я должна заметить:"
#    mt "Создаст проблему что-нибудь на свете"
#    mt "Иль кто-нибудь - всегда ответ один."
#    mt "Ни разу он меня не подводил."    
    mt "...С тех пор чужого не брала, конечно."
    mt "Раз красть мне неприятно - буду честной!"
    me "А если бы заколку ту не взяли,"
    me "Сейчас бы постоянно воровали?"
    mt "Ха-ха. Возможно. Не исключено."
    mt "Но у детей украсть - совсем грешно..."
    window hide
    $ renpy.pause(1.0)
    window show
    mt "Ну что ж, Семён. День длинный был... Устал?"
    me "Спасибо вам - весь вечер отдыхал."
    mt "Понятно всё. Ложусь я потихоньку."
    mt "Спим вместе, арестован ты поскольку."
    mt "Ложись-ка ты, чтоб не сбежал, у стенки."
    mt "А я уж с краю... Что таращишь зенки?"
    me "Не вместимся..."
    show mt smile  pioneer
    mt "А ежели прижаться?"
    window hide
    $ renpy.pause(1.5)
    show mt laugh  pioneer with dspr
    window show
    mt "Ну, видел бы лицо своё! Расслабься."
    show mt grin  pioneer
    mt "Шучу я. Удаляюсь восвояси."
    mt "Ведь ты всерьёз не думаешь бежать?"
    show mt smile pioneer
    mt "К своим \"друзьям\" не станешь пролезать?"
    me "Средь ночи не особо и охота."
    me "Ещё сломаю что, пока темно..."
    stop music fadeout 6
    mt "И славно. На меня нашла дремота."
    mt "Спокойной ночи и приятных снов."
    window hide
    hide mt with dissolve
    $ renpy.pause(0.5)
    scene bg int_house_of_mt_night2 with dissolve
    $ renpy.pause(1.0)
    window show
    "Но сон не шёл ко мне порой полночной."
    "Ворочался, перебирал слова,"
    "Затем от всякой мысли стало тошно."
    th "Так. Мне нужна пустая голова."
    "Не спится. Может... погулять сейчас?"
    "И кстати, день закончен. Первый час."
    jump ss_d6_afmidnight
