﻿init:

    $ sp_sl = 0
    $ sp_ss = 0
    $ sp_dv = 0
    $ sp_un = 0
    $ sp_sp = 0


init 137 python:
    w__sd = {}
    
    if _preferences.language == "english":
        w__sd['sp_sl'] = "Progress with Slavya"
        w__sd['sp_un'] = "Progress with Lena"
        w__sd['sp_dv'] = "Progress with Alisa"
        w__sd['sp_ss'] = "Progress with Samantha"
        w__sd['sp_sp'] = "Samantha's safety points"
    else:
        w__sd['sp_sl'] = "Прогресс со Славей"
        w__sd['sp_un'] = "Прогресс с Леной"
        w__sd['sp_dv'] = "Прогресс с Алисой"
        w__sd['sp_ss'] = "Прогресс с Самантой"
        w__sd['sp_sp'] = "Очки комфорта Саманты"

    #order
    w__so = ['sp_sp', 'sp_ss', 'sp_dv', 'sp_un', 'sp_sl']


#    def widget__samantha():

#        def editoverlay__samantha():
    def editoverlay__samantha():
            #ui.vbox(xpos=1.0, xanchor=1.0, ypos=0.80, yanchor=1.0)
            ui.hbox(ypos=2)

            w__sv = [sp_sp, sp_ss, sp_dv, sp_un, sp_sl]
            for widg,val in zip(w__so,w__sv):
                #ui.hbox(xpos=1.0, xanchor=1.0)
                ui.hbox()
                #ui.button(clicked=None, xpadding=6, xminimum=250)
                ui.button(clicked=None, xpadding=6)
                ui.text("%s: %d" % (w__sd[widg], val), style="button_text", size=14)
                ui.close()

            ui.close()


#        config.overlay_functions.append(editoverlay__samantha)

