﻿
label ss_d6p_eng:

    $ stl = Character(u'Tolik', color="#83AF1C", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ dev = Character(u'Little devil', color="#FF0000", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000")
    $ ssunl = Character(u'Alyona', color="#FF7CFC", what_color="FFDF89", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000", what_drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ],
what_drop_shadow_color = "#000") 

    $ new_chapter(6, u"Samantha: Day 6")

    $ day_time()

    $ persistent.sprite_time = "day"

    play ambience ambience_int_cabin_day
    $ renpy.pause (2)

    scene bg int_house_of_mt_day
    show unblink
    $ renpy.pause (1)

    show mt normal pioneer

    with dissolve

    $ renpy.pause (1.0)    

    window show

    mt "The day we've all been waiting for has come! Awaken, you!"

    me "May I... suggest... to sleep some more... to sleep... just... for a few..."

    play music music_list["everyday_theme"] fadein 7    

    mt "Oh, lazybird, you should ascend and fly and sing your song!"

    th "Am I a bird?"
    me "Of course, I will, but... please... don't get me wrong..."

    mt "I'll get you wrong if you won't wake. So don't you make such a mistake."

    me "For Heaven's sake, allow for me to keep eyes shut."

    mt "Of course I will not!"

    th "... My eyes are open. Am I blind?"

    mt "Enjoy this sunny day - and run to the line!"

    me "Await!"

    mt "Waste my time no more. I see what this is about."

    mt "You've spent the month, so you're prepared! Time for you to act out."

    me "The month? No way! A week... or two... or three."

    me "Time passes faster than it used to be."

    me "We need more practice, do you see there crime?"

    mt "But twenty days is not a little time!"

    show mt normal pioneer

    me "I'll take another ten. Or maybe five... Or two..."

    mt "Stop dreamin', boy. And learn the simple fact:"

    mt "Whine more, or less - nobody cares! In play today we act!"

    me "I mean some facts as well by saying: we're not ready!"

    mt "What are they then? What reasons can you name?"

    me "Our tailors we should blame! Costumes are laying cloth!"

    me "Your girls are lazy, useless kind, yet calling me a sloth..."


    me "Not only that. I must inform: our play has an ill fame." 

    me "Expect mishaps of every kind because our actors are yet lame."

    mt "I saw rehearsal by myself. Your words are unconvincing..."


    mt "And still, there are few hours left to learn the parts they're missing."

    me "Excuse me then for my opinion, and for my will to reach perfection."

    me "Why, after all, am I still concerned? I wasn't put for stage direction."

    me "Most certainly, not taking blame for anything that happens after."

    me "I am remorseful, ma'am. My bad. I shouldn't indicate disaster."

    show mt smile pioneer    

    mt "Excusing failed sabotage, right now I'm leaving for the line."

    mt "That I would like to see you there - should I again remind?"

    window hide

    $ renpy.pause(0.5)  

    mt "I'm leaving."   

    

    window hide

    hide mt with dspr

    $ renpy.pause(1.0)

    play sound sfx_open_door_1      

    window show 

    th "Wait, what? It seems our wise mistress is not familiar with sarcasm."
    th "And my precise, ironic words just went into an empty chasm."
    th "How pity... {w}Fine. I do not care."
    th "I think I'll head now for the square."

    window hide
    $ renpy.pause(0.5)
    scene bg ext_square_day
    with dissolve
    show dv angry pioneer at cright

    show sl angry pioneer at left

    with dissolve 

    play ambience ambience_day_countryside_ambience fadein 3
    $ renpy.pause(0.5)
    window show    

    th "What is it there? A quarrel at my sight?"

    me "Hey, stop, you two! And tell me why you fight!"

    window hide

    if dv_thi == True:

        

        show dv guilty pioneer at cright with dspr  

        window show     

        sl "Alisa is refusing to participate in play, although..."

        me "Although she promised."

        dv "I promised nothing 'cause that wasn't an oath." 

        dv "For the past few weeks you didn't care, you both!"

        dv "So don't you make big eyes. And anyway..."

        dv "Surprise or not, I'm still not ready for the play."

        sl "True that. I'd be much more surprised,"

        sl "To see you hold your word, act civilized,"

        sl "To see you doing something good,"
        sl "Instead of ruining the neighborhood."


        window hide     

        show dv angry pioneer at cright with dspr   

        window show         

        dv "You have no right to judge me."

        me "You let us down, Alisa. How can we ignore?" 

        me "Whole camp is waiting for that damned play."

        me "You could have told us that you quit before."

        me "It's way too late to give it up today!"

        window hide         

        show dv guilty pioneer at cright with dspr  

        window show         

        dv "I didn't suppose that. Tried to catch up,"

        dv "But had no time. Been busy with startup..."

        dv "And for the last week I've had a headache."

        sl "To speak, how did you hurt your head?"

        sl "I heard the story, it's widespread,"

        sl "You've hit a guy! You've hurt him very much."

        dv "It was deserved. He mustn't hold a grudge."

        window hide

        show dv normal pioneer at cright with dspr  

        window show         

        dv "I think it's time to end this chatter,"

        dv "Don't wanna hear your blames till' supper."

        dv "I'm leaving."

        me "Wait. What about your part? The play?"

        dv "I don't know nothing. As I told you, by the way."
        dv "You should have asked me before. Last week or even yesterday,"
        dv "But not like this, definitely not today!"

        me "I see your point. But I don't see solution,"
        me "This play is in a deep downswing."
        me "What mess it'll be! A hell of a confusion..."
        me "Maybe let's cancel it? Shut down the whole thing?"

        sl "I have another option. There'll be no greater..."

        sl "It's simple: let her read it by the paper."

        window hide

        show sl serious pioneer at left

        show dv guilty pioneer at cright

        with dspr   

        window show



    else:

        window hide

        show dv guilty pioneer at cright with dspr  

        window show     

        dv "So here I stand and struggling with my mind:"

        dv "What does she wants of me? A favor? Of which kind?"

        sl "No favor, but a compensation for your harm."

        me "What harm is that? Please, tell me what she's done."

        sl "You know our Tybalt, Timofei? Good actor, even better drummer..."

        sl "He caught a cold. Yes, in the middle of a summer."
        sl "Out of the play now. And whose fault you think is that?"

        dv "Of course it's mine! You said that. I'm the brat."

        me "Stay calm, please, girls..."

        sl "You doused him with plenty of well water!"

        sl "So that poor guy became all chilled while going home."

        window hide

        show dv normal pioneer at cright with dspr    

        window show 

        dv "Big guy got wet a bit. What a disaster!"

        dv "I don't believe he caught a cold. More likely the Down syndrome."

        sl "Don't wriggle now, you are to blame! You left our play without its pivot."


        sl "Go on now, expiate your fault. No matter how, but give us Tybalt."
    

        window hide

        show sl serious pioneer at left with dspr  

        window show 

        sl "Wait, I know how. You take the role."

        show dv grin pioneer at cright            

        dv "Of course, my lady! Name your wishes! Tell them all!"
        dv "What next to do? What else does your heart desire?"

        sl "There's only one thing I require..."

        dv "There must be more! Back massage maybe? Comb your hair?"

        dv "Our Semyon's gonna love to stare,"

        dv "His mouth is open and saliva's dripping..."

        dv "I bet he thinks that massage involves stripping."

        me "No, I think nothing. But I like some of your ideas..."

        me "I mean the Tybalt role. Alisa, you should help us."

        window hide

        show dv angry pioneer at cright with dspr   

        window show         

        dv "Shut up, you, lusty playgoer! Little sinner!"
        dv "Don't ask again! Don't make me use my middle finger."

        sl "Alisa, I'm not asking, I am telling."

        dv "You have no right to tell me what to do."
        dv "Just skip this part and go complain to Olga."

        me "Samantha thinks that you have enough talent."

        me "But rumour has it that you have stage fright..."

        window hide

        show dv guilty pioneer at cright with dspr  

        window show     

        dv "Lies, ugly lies! Everyone knows it - I fear nothing!"

        me "So show it then. Show everyone what you can do."

        dv "No, no... It's way too late. I have no time to learn the words."

        me "Learn what you can. And for the rest we'll give you cribs."

        sl "We'll give her everything. No need to learn at all."


        sl "Go there and read every single line. Sounds fine?"

    stop music fadeout 7    

    dv "While others will recite by heart? Sounds not fine! It's a shame!"

    sl "Alisa is afraid of shame, huh? That's a switch."

    dv "You can record it in your logbook."

    me "It's not that shameful... There's no shame at all!"

    me "It's our camp, our play, we do it for ourselves," 
    me "We'll make it better with you, yes! Together we excel!" 

    window hide

    show dv grin pioneer at cright with dspr    

    window show     

    dv "Wow, Simon, so excited! What if I say \"Yes\"? What's in it for me?"
    dv "Wait, wait... I think... I know."

    play music music_list["always_ready"] fadein 3  

    dv "Climb on Genda for me. Ride him. Naked."

    window hide

    show sl surprise pioneer at cleft with dspr 

    window show     

    me "You're kidding... No?.. You must be crazy!"

    dv "Not crazy and not kidding. I'm sincere..."

    dv "Be brave now, give me an example. Show no fear!"



    show sl angry pioneer at cleft   



    sl "You truly are a shameless girl! No decency at all!"

    dv "It's our camp, right? We're no strangers and so on..."

    dv "But Semyon has to hurry up. Go on!"

    dv "Know what? I will rejoice you: don't remove your pants."

    me "That's a relief. But can I stay all clothed and on the ground?"

    dv "No, you cannot! Just climb it!.. Do it dressed up!"

    th "It's really happening? I have to do this thing indeed?"

    th "Alright, I got some tricks to show. Let's call it Pioneer's Creed!"

    window hide 

    scene bg ext_square_day_zoom

    show dv grin pioneer at fright

    with dissolve

    $ renpy.pause(1.0)  

    window show 

    "I looked around and headed to Genda's head."

    "But had no luck at all with the unyielding monument."
#"Haven't succeeded though. What a slippery monument!"

    "I tried again..."

    window hide 

    show sl scared pioneer 

    with dspr

    window show

    sl "Stop it, Semyon! You hooligans! What are you doing?!"

    me "I can't get... on this thing... The stone... too slippery... "

    dv "Hey, Slavya. Semyon needs your helping hand!"
    dv "Or any other body part which you can lend."

    sl "You are disgusting! And dis... graceful! Disrespectful!"

    me "You are against it, right... But could you be against it... a little closer?"

    me "I need one thing to do this - someone's shoulder."

    window hide 

    show sl scared pioneer far

    with dspr

    window show 

    me "Yes, very nice..."

    "With Slavya's help I've had a swift and huge success,"

    "At least I've pulled myself up, more or less."

    window hide


    scene bg ext_square_day_alpinist

    with dissolve

    

    $ persistent.ssg_109 = True 

         

    $ renpy.pause (0.9)

    window show         

    sl "Oh, please, come down! They'll catch us! They will see!"

    dv "No need to fuss, my dear. Things won't get worse for you."
    dv "You are already hated in this camp - but secretly."

    sl "Me, secretly? Well, you are hated in your face! As you deserve!"

    me "Guys, look how high I am! So much here to observe!"

    dv "But I'm amazed... How easy for a guy to get on Slavya. "

    sl "Take a good look. You'll never see another guy in years,"

    sl "Because there are no guys in women's prison!"

    dv "Why prison, sister? Want a fight?"

    me "Hey, easy now! Don't grapple there, alright?"
    me "I mean it, ladies! Listen! Mark my words!" 

#    me "You fight and I'll dive at you! You'll face a crazy bird!"
    me "No fighting or I'll dive at you! You'll face a crazy bird!"

    dv "There'll be no prison,  Slavya, dear, because I'll manage"

    dv "To kick your supercilious butt without any noticeable damage."


    me "Ahoy on deck! The counselor is coming!"  

    th "Can't get down fast - that's a downside of being above all..."

    th "Oh, Genda, hide me, please! And don't you let me fall!"

    th "Please, girls, just drive away this Olga!"

    window hide

    scene bg ext_square_day_alpinist2 with dissolve

    

    $ persistent.ssg_109 = True 

         

    $ renpy.pause (1)

    window show

    mt "Hi there! Why are the both of you so strained, so red?"

    mt "Having a fight so early in the morning? Go ahead!"

    mt "But first: I'm curious what's this about,"
    mt "Enlighten me! Or shall we wait the line to hear you out?"

    sl "We... only had a heated argument."

    mt "Oh, a debate? About?.."

    dv "...About our scientific failures!"

    dv "Our space program seems to be stagnating,"

    dv "The flight to Mars is still delaying, it's frustrating,"

    dv "So long-awaited. Never happens. Makes me sad."

    dv "I think the county needs a hero, needs one bad,"
    dv "There must be one. I can't believe our soil is barren,"
    dv "Where are you, the successor of Gagarin?"



    dv "His plane was downed, who will take vengeance on the aliens?"

    dv "Who will fly high, no matter how grim it is?"

    dv "And who is not afraid to fatally fall down?"

    me "Oh!" with vpunch

    "I could no longer hang on to Genda,"

    "My weakened arms slipped off his neck,"
    "Leaving no options not to hit the deck."

    window hide

    scene bg ext_square_day_zoom with dissolve

    window show     

    "So I fell down, hearing Alisa's crazy laugh,"

    "And Olga's scream of fear on my behalf."

    window hide    

    play sound sfx_body_bump

    scene bg ext_square_day_zoom:

        xalign 1.0          

        ypos 0.0     

        ease 0.1ypos 0.025

        ease 0.1ypos -0.025

        ease 0.1ypos 0.0125

        ease 0.1ypos -0.0125

        ease 0.1ypos 0.00625

        ease 0.1ypos -0.00625

        ease 0.1ypos 0.003125

        ease 0.1ypos -0.003125     

    show sl scared pioneer 

    show dv laugh pioneer at fright

    show mt shocked pioneer  at fleft

    with dspr  

    $ renpy.pause (0.7)

    window show

    stop music fadeout 5

    stop ambience fadeout 3

    dv "Here comes the Hero!"

    window hide

    $ renpy.pause (1.5)    

    scene bg int_aidpost_day 

    show cs normal stethoscope at cright

    show sl angry pioneer at left   

    with fade2

    play ambience ambience_medstation_inside_day fadein 3

    play music music_list["eternal_longing"] fadein 5

    window show 

 

    "Despite the fact that I was pretty fine,"
    "Almost no hurt, no reason to distract the doctor,"
    "I've had to do it for the sake of Slavya."
    "Poor girl, she took a hit because of me (and actually by me),"
    "She isn't a trampoline, but did its job for free."
    "As gentleman, I've had to show my care and compassion."


    

    cs "What happened to you, my kittens?"


    th "It is impossible to dodge her nasty jokes,"

    th "I have to counter them before she starts."
    th "What should I say? That we've established a new club,"
    th "A good addition to the cybernetic one and others:"



#    th "Our brand new thing is a BDSM-club, but..."
    th "A sadomasochistic one... Alas,"

    th "We have it overdone by inexperience?"
    cs "So, Slavya?"

    show sl sad pioneer at left    

    sl "I have fallen..."

    cs "Huh, really?.. Then let's start examination."

    cs "There are some bruises, I see nothing strange..."

    cs "But what I do see is a strange young man!"
    cs "What is it, pioneer? Have something to complain?"

    me "Nothing at all..."

    cs "Then you are free to go."

#    th "Am I going to get rid of her that easy?"


    

    sl "No, please... Examine him as well. He's fallen too."

    cs "What, fallen too? We're having a fall epidemic!" 

    cs "And where are you falling from, I wonder?"

    cs "Seeing your face I guess the bed's somehow involved!"

    window hide

    show sl shy pioneer at left

    show cs smile stethoscope at cright

    with dspr

    window show     

    me "That's how it was: Alisa has provoked us, teased us bad,"
    me "She had insisted that I mount Slavyana,"
    me "I'd never do so, but Alisa had her reasons..."
    me "At least I did it in my clothes, without undressing,"
    me "It was OK at first while there was two of them."
    me "Then Olga Dmitrievna appeared. I tried to hold on,"
    me "But had no strength. So she left angry and dissatisfied."


    show sl surprise pioneer at left    

    cs "I think we here heard enough,"
    cs "You may shut up your dirty mouth,"
    cs "Before I wash it with a soap,"
    cs "Or even get my proctoscope!"
    me "..."
    cs "I see you've finally found your brakes?"
    cs "Now you will tell me where it aches!"

#    me "How can I shut up and tell in the same time?"

#    cs "Oh, you gonna get clyster! Or two!"

#    cs "Quiet now? Show me your wounds."

#    cs "We will heal you in silence."


    stop ambience fadeout 2  

    th "I'd better shut up and take my treatment..."

    th "To save myself from an enema."

    stop music fadeout 5

    window hide

    $ renpy.pause (1)

    play ambience ambience_camp_center_day fadein 3 

    scene bg ext_square_day 

    show sl normal pioneer 

    with dissolve



    window show

    "Right from the nurse's house, tired of sitting,"
    "We rushed to the square for morning meeting." 

    me "...I trust her word. Alisa won't deceive us."

    play music music_list["so_good_to_be_careless"] fadein 5

    sl "Why are you so confident in this miscreant?"

    me "We have no choice. I think she has no choice as well."

    if mer_cu == True:

        sl "She acts now like she doesn't care about being expelled..."
        sl "Or anything. Hey, listen, by the way."

        sl "So great that you've agreed to play Romeo."
        sl "You saved me there. The role is big, I had no time to learn it."

#

#

        th "Romeo? Is it me who acts him?.."

        th "Oh, yes, it must be me. I totally forgot."

        sl "It must be hard for you or even embarrassing..."

        me "Awkward a bit, yes. But on the other hand,"
        me "Me and Samantha had much time for repetitions,"
        me "While she was resting after the snakebite."



        me "I had to be with her because somebody must..."
        me "I had to read the play, to swallow my disgust,"

        me "It wasn't my intention or a goal"
        me "To be Romeo. Yet I'm ready for the role."

#

#

#

#

#

#

        sl "It seems you are exaggerating,"
        sl "It couldn't have been so frustrating,"
        sl "To read the text and to recite,"
        sl "Nobody made you do this, right?"
        sl "Nobody forced you stay together,"
        sl "As if there was some magic tether."


        me "What do you mean, I'm sorry?"

        sl "You mentioned once or twice that you are used to being alone,"

        sl "But now it seems that you are getting used to being together."
        sl "I understand it."

        th "Why would she say that, is she jealous? And of whom?.."

#        me "...You thank me, I thank you. The play is your idea anyway."

        sl "I have to go. Please, don't be angry, I'm just kidding."

        

        

        

    else:

        sl "She acts now like she doesn't care about being expelled..."
        sl "Or anything. Can't agitate her or affect,"
        sl "And be that sure of effect."
        sl "I'm very doubtful. Please, remember."

    

    window hide

    hide sl with dspr
    $ renpy.pause (0.5)
    window show

    "I tried to find Alisa in the crowd,"
    "Without making fool of me by screaming name out loud."

    window hide

    show ss smile casual with dspr

    window show

    ss "{enen}Good morning, Sam.{/enen}"

    show ss smile2 casual   

    me "{enen}And same to you. I'm trying to catch Alice.{/enen}"

    ss "{enen}She is here, but I don't know where exactly...{/enen}"

    show ss unsure casual   

    ss "{enen}Ulyana is with her though. Try following her voice.{/enen}"


#    th "Is she still afraid of this fussy monkey?"

    me "{enen}I think I heard it somewhere... Many thanks.{/enen}"

    window hide

    hide ss with dspr

    $ renpy.pause (1)

    show un normal pioneer far 

    show mi smile pioneer far at right

    with dspr   

    window show 

    un "...Sometimes it feels like I am slowly going nuts."

    mi "You? Slowly?..  It must have been started pretty long ago?"

    window hide

    show un sad pioneer far 

    with dspr   

    window show 

    mi "What's wrong, girl? Don't frown, I'm kidding!"
    mi "And even if I'm not - who cares?"

    mi "A bit of craziness will work for you just fine,"
    mi "It lures people and intrigues them, makes us girls more attractive."

    window hide

    $ renpy.pause (1)   

    hide mi

    hide un

    with dspr

    $ renpy.pause (0.5)

    show dv normal pioneer at cleft

    show us normal pioneer at cright

    with dspr

    window show 

    me "Got you!.. So, your request has been fulfilled,"

    me "And now you owe us what you've promised."

    dv "Or else? Will you drag me there by force?"

    me "Don't start again! Enough hanging back!"

    dv "Relax, Semyon. I'll keep my word. I'll join the play."
    dv "You will appreciate my acting skills today."

    dv "And mark my kindness too, I hope. It's a big favor."

    me "I need this play no more than others do."

    me "I'm striving for the sake of... I don't know what for."     

    show us grin pioneer at cright    

    us "To be precise - WHO are you striving for."

    me "What is your answer then? For all my friends?"

    us "Trying to play the fool? Cunning Semyon!" 

    show us laugh2 pioneer at cright    

    us "Everyone knows that you're in love with someone."

    me "...That's not a secret - I adore Ulyana."

    window hide     

    show us surp3 pioneer at cright with dspr

    window show     

    me "But do you care about the play, sweetheart?"

    window hide     

    hide us with dspr

    window show

    me "Ulyana, soul of mine, why are you running?"

    dv "Oh, my. What need to joke like that?"

    me "I only poked her back. Why not?"

    dv "It wasn't delicate at all!.. She's a susceptible little girl."

    me "Let's say things turned out awkwardly a bit..."
    me "But what about the other thing she said?"
    me "Me and Samantha... Has the rumor spread?"

    dv "I am indifferent to gossip. Do you care?"
    
#    I don't collect gossip. Does it bother you?"

    me "Of course, if it's there. Could she invent it though?"

    dv "I have no words of comfort for you..."

    mt "Line up, pioneers!"

    window hide

    scene cg d2_lineup 

    with dissolve

    window show    

    mt "Today I won't be holding you for long,"
    mt "Because less time for chat leaves more for work!"


    mt "We have so many things to do..."

    mt "We need to cut this grass again,"
    mt "The thistle has grown very high this year. "

    mt "And don't forget about the spiky nettle!"

    mt "Once you cut it, bring the leaves to the canteen,"

    mt "We'll have a nice green soup for dinner."

    pi "Oh, not again! Last time we ate this rubbish,"
    pi "There was a never-ending line before the toilets!"


    mt "Shush there!{w} That's our programme for a day."

    stop ambience fadeout 2

    stop music fadeout 5

    mt "And for the evening, as you know, \"Romeo and Juliet\" and others..."
    mt "You finally will see the play in all its colors."



    window hide

    $ renpy.pause (0.3)

    play ambience ambience_dining_hall_full fadein 5    

    $ persistent.sprite_time = 'day'

    scene bg int_dining_hall_people_day 
    with dissolve 
    show ss smile2 casual at cright 

    show mt normal pioneer at cleft 

    

    with dissolve   

    mt "Well, dear Samantha, I must say you are just shining."

    mt "Can't wait to see you on the stage. It's so exciting!"

    show ss shy casual at cright with dspr  

    ss "{enen}Thank you. I'm pretty nervous.{/enen}"

    mt "It's sure to be a huge success, don't worry."
    mt "Don't you agree, Semyon? Why the sour face?"

    me "We are not ready still. But that's not news for you."

    show ss smile2 casual at cright with dspr   

    mt "What's troubling you? Don't mention the costumes, please."


    me "I'm troubled with the question of direction."

    th "Meaning that I blame Zhenia grumpy pants..."

    mt "What question did you find this time?"

    me "Let's say there is a problem of a moral nature."

    me "Romeo talks to Juliet, getting closer..."
    me "As close as they can get, you know?"
    me "There is a certain level of intimacy,"
    me "I very doubt it's legitimacy."

#    me "We need to smooth this topic."

    me "Howerer Zhenya never recognises the problem."

#


    window hide

    show ss serious casual at cright 

    show mt smile pioneer at cleft 

    

    with dspr

    window show

    mt "Define \"intimacy\" for me. You mean a kiss?"

    mt "Is it your problem? Are you shy about this?"

    me "I'm just a proper pioneer who is concerned... And doesn't understand you."

    me "Not trying to protect myself, it's clearly not about me."

    mt "And you, Samatha, what's you point?"

    mt "You see a problem, do you?"

    window hide

    show ss shy casual at cright 

    show mt normal pioneer at cleft 

    

    with dspr

    window show 

    ss "{enen}It must be shameful for Semyon, my buddy,{/enen}"

    ss "{enen}So let's not bother him or anybody...{/enen}" 

#     "Olga gave my a sign: "I understood her, no need to translate."

#

    mt "Do not translate my words, Semyon. I'll tell you what:"

    mt "It matters how you act, not what you want,"

    show ss unsure casual at cright 

    mt "This silly boyishness does not suit you well."

    mt "You're not protecting, you are just worrying. Well? Understood?"

    me "I just can't see your reason here... Or don't you know her age?"

    me "You want to see me kissing Juliet on the stage?"

    mt "No, it is you who is supposed to want those kisses,"

    mt "And then it is my duty to forbid them."

    me "So you expect me to find out from Samantha,"

    me "If she is into kissing in the public's eyes?"

    mt "No...."

    me "Because she could accidentally agree."

    window hide

    show ss sad casual at cright 

    show mt angry pioneer at cleft 

    

    with dspr

    window show 

    mt "She can?! Oh, what a scoundrel you!"

    mt "I trusted you, am I so terribly mistaken?"

    mt "You would change colors like a chameleon!"

    show ss surprise casual at cright   

    play music Nobodys_Secret fadein 5

    ss "{enen}Don't push him, miss, I beg you.{/enen}"

    ss "{enen}We can discuss it just us two.{/enen}"

    show ss unsure casual at cright     

    th "So when I stood up for decency and honesty,"
    th "She called me a reptile and also scoundrel..."

    stop ambience fadeout 2 

    th "I think it's time to end this conversation."

    window hide

    $ renpy.pause (0.5)

    $ persistent.sprite_time = 'day'

    scene bg ext_dining_hall_away_day 

    with dissolve
    
    show ss shy casual

    with dissolve    

    play ambience ambience_camp_entrance_day fadein 3

    $ renpy.pause (0.3)    

    window show

    me "{enen}It's awkward. Sorry if I was making a big deal.{/enen}"

    me "{enen}That's not important. Is this how you feel?{/enen}"

    ss "{enen}I'm glad you care so much. Infact, I must confess,{/enen}"

    ss "{enen}I am concerned more than you. At least, not less.{/enen}"

    ss "{enen}And hear my next confession... Oh, you might enjoy, {/enen}"

    ss "{enen}I've actually never kissed a boy. {/enen}"
    
    me "{enen}So haven’t I.{/enen}"
#    me "{enen}Neither have I...  Kissed a girl, I mean.{/enen}"

    show ss shy2 casual     

    ss "{enen}Don't laugh at me, not now.{/enen}"

    show ss shy casual  

    me "{enen}It is decided then. No kisses in the play.{/enen}"

    show ss shy2 casual        

    ss "{enen}Why so? I'm ready if you may.{/enen}"

    show ss shy casual 

    menu:

        "So it is":

            $ sp_ss += 1

            $ d6_kissing = 1

            me "{enen}Much honored with your permit,{/enen}"

            me "{enen}And gladly I accept. So be it.{/enen}"

            show ss shy casual                 
            
            ss "{enen}Good lord. I guess I need a partner for a practice...{/enen}"             
            
            ss "{enen}Without experience it could be real disaster.{/enen}"

            me "{enen}You're kidding now. You've got to be.{/enen}"

            window hide

            show ss smile casual with dspr

            window show         

            ss "{enen}Right... Pardon me. I'm nervious.{/enen}"


            show ss smile2 casual         

            me "{enen}Don't be. I will be there with you.{/enen}"

            window hide

            

        "We shouldn't":

            me "{enen}You are too young. I don't know what they're thinking{/enen}"

            me "{enen}So it's decided. There will be no kissing.{/enen}"

           

            

            if sp_mizul == 3:

                $ d6_kissing = 1

                show ss sad casual with dspr           

                ss "{enen}You are so cold to me sometimes.{/enen}"

                ss "{enen}Youth is my sin, what else? Go, name my crimes.{/enen}"

                me "{enen}You are a silly girl, who I am trying to protect.{/enen}"

                ss "{enen}But don't protect me from yourself.{/enen}"

                show ss shy casual                

                ss "{enen}Give me a kiss, don't be so cruel.{/enen}"







                me "{enen}Just for the practice, right? ..Uh, maybe later?{/enen}"

                show ss shy2 casual with dspr              

                ss "{enen}Now!{/enen}"

                window hide

                $ renpy.pause (1)

                scene black with fade

                scene bg ext_dining_hall_away_day 

                show ss shy casual 

                with fade                

            else:

                ss "{enen}As you say.{/enen}"
                window hide


    $ renpy.pause (0.7)

    window show 

    ss "{enen}Shall we move now to the stage?{/enen}"

    stop music fadeout 5    

    me "{enen}Let's go. Don't make them wait for ages.{/enen}"

    window hide

    $ renpy.pause (0.4)

    $ persistent.sprite_time = 'day'

    scene bg ss_ext_stage_big_day 

    show sh normal pioneer at cleft

    show ss normal casual at cright

    with dissolve
    $ renpy.pause (0.5)
    window show

    th "What's Shurik doing?  He's not supposed to be here..."

    sh "Hello, my friends! I've got myself a role."

    me "What role is that, if I may ask?"
    th "Except the usual role of a moron..."



    sh "It is Benvolio, the best friend of Romeo."
    sh "Can you believe it?"

    me "You should have chosen something else. "





    sh "All better roles had been already taken,"

    sh "I guess my passion for the theater must awaken"

    sh "A bit too late to get the part that I desire."

    sh "I will replace you though, if you wish to retire."



    me "I had this wish for many weeks, but suddenly I lost it."
    me "Right now. The very moment you appeared here."
    sh "Because you don't want to destroy our fresh tandem?"
    me "Precisely."



    show sh surprise pioneer at cleft

    

    sh "Oh, so sarcastic... How do you find Benvolio, Samantha?"

    window hide

    show sh normal pioneer at cleft

    show ss unsure casual at cright

    window show     

    ss "{enen}He's not a good friend, he is such a worm,{/enen}"

    ss "{enen}He leaves Romeo at the tomb alone!{/enen}"

    me "Hey, know what? Shurik fits the role. I changed my opinion."

    sh "For your approval, thanks a lot! It means the world to me... Or not."

    window hide 

    $ renpy.pause (1) 

    scene bg ext_stage_normal_day 

    show mz angry glasses pioneer at cleft  

    show dv angry pioneer at cright

    with dissolve



    window show

    play music music_list["gentle_predator"] fadein 3

    mz "...Did I invite you here? No, I didn't!"

    dv "It doesn't matter if I came here not to you."

    me "Another quarrel, eh? What now?"

    mz "The stage is not a circus or playground,"
    mz "She better find another place to hang around." 

    me "Her place is here. Alisa has a role so she will play."
    me "It is decided, whether you want it or not really."

    mz "...You will be held responsible for consequences!"

    window hide

    hide mz
    with dspr
    $ renpy.pause (0.3)
    show dv smile pioneer at cright

    with dspr

    window show     

    me "I wonder what is she so mad about?"

    dv "Me too. Like her skirt is still on fire..."

    me "I haven't heard of such occasion. Tell me more."

    dv "Oh, I did nothing but a favor to her..."
    
    dv "In case of a big fire Zhenya wouldn't burn alive,"

    dv "Thanks to the vaccination I provided."

    me "No way... Why do you need to anger someone,"
    me "Who is so hateful anyway?"

    dv "Boy, leave it. Let's talk business."
    
    dv "Tell me, my role - what's it all about?"

    me "You still don't know, do you?"

    show dv smile pioneer at cright

    dv "Come on, enlighten me. Or you don't know it too?"

    me "Well... You are Tybalt. You must kill Mercurio..."

    dv "And who's Mercurio?"

    me "It's Slavya. But after that Romeo..."

    dv "Wait, am I killing Slavya? Let me see the page..."

    dv "If I had known, I would have lived on the stage!"

    me "Romeo takes the revenge after that. He slays Tybalt."

    dv "You mean you kill me? Oh, what a swine you are."

    dv "No, I don't like this part. Let's fix the plot a bit."

    dv "My Tybalt gotta stay alive. To spite all Zhenyas, he'll outlive them!"

    me "Can you be serious? It is the best way to spite Zhenya,"
    me "Because she's only person who awaits your failure."

    me "Remember: if you let us down, she will prevail."

    show dv normal pioneer at cright    

    dv "To let you down? I'm not retarded, nor a baby in a diaper."

    dv "How can I fail my reading from a piece of paper?"

    window hide

    $ renpy.pause (0.5)

    hide dv with dissolve

    $ renpy.pause (0.5) 

    show sl normal pioneer at fleft

    show mi normal pioneer

    with dissolve 

    window show

    sl "Are you guys ready? Let's start the rehearsal!"

    mi "Enter Chorus!"

    sl "You are the chorus. Skip this line."

    mi "I am alone. How can I be the chorus?"
    mi "Shall I repeat my words multiple times?"


    mi "I doubt I'll manage to repeat them fast enough..."

    show sl serious pioneer at fleft

    sl "Forget the chorus! Read it, please, as the narrator."

    mi "I got it. {w}{i}Two households both are like infinity...{/i}"

    sl "So far I'm not impressed by your knowledge."

    window hide 

    show mi grin pioneer    with dspr

    window show

    stop music fadeout 5    

    mi "No, it was nothing!.. Just a flashback from astronomy... "
    mi "I'm sorry." 

    sl "I hope you learned the text. Continue, please."

    mi "{i}Two households, both alike in dignity...{/i}"

    window hide 

    show us laugh pioneer   at right with dspr

    window show

    play music music_list["eat_some_trouble"] fadein 0  

    us "Have shown the record level of stupidity!"

    window hide

    show sl angry pioneer at fleft

    show mi normal pioneer

    with dspr

    window show     

    sl "Why you are here?! Gosh, what a madhouse!"

    sl "Please, be a nuisance somewhere else!"

    window hide 

    show us upset pioneer at right  with dspr

    window show     

    us "I tried, but it's boring. May I join you?"

    sl "You should have come to us way earlier,"
    sl "But not today. Now it's too late."

    window hide 

    show sl serious pioneer at fleft     

    show us normal pioneer     at right  

    with dspr

    window show     

    us "It is still morning now, why late?"



    me "Let her play the Prince, it is an easy role."

    sl "We have enough problems even without this Prince..."

    show us grin pioneer    at right 

  

    us "The Prince, huh? It's not bad! Are there other options?"
    us "I wish for something bigger! Maybe Stalin? Can I play him?"

    me "No, you can't... But you can have him as your muse,"

    me "And add a mustache for extra inspiration,"

    me "That will improve the Prince's respectability."

    show us normal pioneer    at right  

    sl "Hold on, Semyon. You're acting like it is decided..."


    sl "You single-handedly appointed her for this role?"

    me "Not single-handedly, but with your kind approval,"
    me "There is no reason not to give it, right?.."

    sl "Oh, you... {w}Listen to me, Ulyana."
    sl "You'd better not run off as soon as you feel bored."

    show us normal pioneer  at right

    us "I'll stay with you, I swear by Stalin's moustache!"

    us "I won't run off, I swear it by Lenin's beard!"

    show sl normal pioneer at fleft 

    sl "Come here now, I'll show your lines."

 

    window hide 

    hide sl

    hide us

    hide mi

    with dissolve   

    show ss nosmile casual at cleft 

    show dv normal pioneer at cright

    with dissolve



    window show

    dv "And even here this tomboy Ulyana follows me."

    me "You're like a mother to her. Isn't that cute?"

    dv "And you're to me... is just a talking bum,"
    dv "One who is desperately asking for a kicking."

    ss "{i}Didst thou not fall out with a tailor wearing...{/i}"

    show ss smile2 casual at cleft with dspr

    ss "{enen}I honestly have no idea what I'm saying.{/enen}"

    "Meanwhile the rehearsal goes on."

    window hide     

    hide ss

    hide dv

    with dissolve

    show stl normal pioneer at cleft

    show el normal pioneer at cright

    with dissolve

    window show 

    stl "{i}A dog of that house shall move me to stand.{/i}"

    stl "{i}I will take the wall of any man or maid of Montague’s.{/i}"

    el "{i}The quarrel is between our masters and us their men.{/i}"

    stl "{i}'Tis all one, I will show myself a tyrant: when I have fought with the men I will be cruel with the maids, I will cut off their heads.{/i}"

    el "{i}The heads of the maids?!{/i}"

    stl "Especially maids! I will be cruel to them!"

    el "Wrong text."

    stl "{i}Me they shall feel while I am able to stand: and 'tis known I am a pretty piece of flash!{/i}"

    stl "Me... maids... shall feel!.."

    window hide

    hide stl

    show el upset pioneer at cright 

    with dspr

    window show

    el "Hey, is he leaving? Why? And where?"

    sl "Tolik, come back! We need your Sampson here!"

    me "I guess he will be in his bunk. Just leave him for a while,"

    me "After a brief rest he'll be fine."

    window hide

    hide el

    show sl serious pioneer at center

    show mz normal glasses pioneer at right

    with dspr

    window show

    sl "No, bring him back! Who's not busy here?.."

    mz "Don't look at me! My time is precious, I won't chase this lubber."

    sl "As always... Wait then, I will go myself."

    me "But if he's dangerous right now? Don't risk it!"

    show sl normal pioneer at center 

    me "Let Zhenya go, I'm pretty sure she can approach him safely."

    me "Since his excitement applies only to females."

    window hide

    show mz angry glasses pioneer at  right

    with dspr

    window show 

    stop music fadeout 4    

    sl "Leave her alone. I know who should be going." 
    sl "Not me, for I am a Montague, and he is Capulet - my enemy." 

    sl "Where is Alisa? Let her deal with nasty servant."
    sl "It is their home business, after all."     

    window hide

    show dv normal pioneer  at fleft with dspr

    hide mz

    window show

    play music music_list["heather"] fadein 3   

    dv "Catch him yourself! He looks like your twin brother,"

    dv "So take him home - to graze the sheeps together."

    window hide

    show dv grin pioneer    at fleft

    show sl angry pioneer at center

    with dspr

    window show 

    sl "Even for grazing some intelligence is needed."

    sl "I wouldn't trust a single sheep to you."

    dv "Take care of herds and flocks yourself, but be alert around your twin."

    dv "He could mistake you for a sheep... That's how you get into a sin!"

    dv "You are a simple folk though. Do you mind?"

    menu:

         "Intercede Slavya":

            $ sp_sl += 1

            $ sp_dv -= 1            

            me "Enough, Alisa. This is way too much."

            sl "What else would you expect from her? I'm used to it."
            sl "Sometimes such hatred and reproval from bad people"

            sl "Sounds sweeter then a compliment from good ones."

            window hide

            show sl angry pioneer at fright

            

         "Don't intervene":

            sl "Enough, shut up! There is no piece of soul in you."

            window hide

            hide sl with dspr

            $ renpy.pause(1)

            show dv guilty pioneer at fleft

            with dspr

            window show

            dv "She's so offended, even ran away..."

            dv "But why?.. I hardly said a thing."

            window hide         

            show mz rage glasses pioneer at  fright

            with dspr

            window show

            mz "She shouldn't be here! I'm tired of repeating!"

            dv "Would you be quiet? Damn, I have no luck,"

            dv "I'm tired too of your repeating like: \"cluck cluck\"!"
            dv "First chicken gone - a second one steps forward..."


            dv "It's true: I'm tired of you all."

            show mz angry glasses pioneer at  fright           





            mz "SHE tired of us?! That's just ludicrous!"

            me "Am I alone here who is thinking 'bout the play?"

            me "Why can't you pause your quarrels for a day?"

            me "Go bite each other's throats tomorrow."



    show mt normal pioneer far at center

    with dspr

    window show 

    mt "Fighting again? This must be stopped right now!"
    me "I've already stopped them, miss! So don't you steal my glory."

    window hide

    hide dv

    hide mz

    hide sl

    with dspr

    $ renpy.pause (0.3)

    show mt normal pioneer at center with dspr  

    window show

    stop music fadeout 5

    mt "Good news: Samantha's costume is complete!"

    mt "Or maybe even two! Go try them on, Lena is waiting."

    show mt smile pioneer   


    mt "You see, Semyon? The job is done. Your worries were for nought."

    mt "Please, come with us. You will translate."

    window hide

    $ renpy.pause (0.3) 

    $ persistent.sprite_time = 'day'

    scene bg ext_library_day 

    show ss nosmile casual at cright

    show mt normal pioneer at left

    with dissolve



    window show



    me "It's strange. Why we are going to the library?"

    me "Is there a sewing club between the bookshelves?"

    play music music_list["went_fishing_caught_a_girl"] fadein 5    

    mt "Something like that... But wait! I'm leaving you now for a while!"

    mt "I have to do a high amount of urgent things! Just realised that!"

    window hide

    hide mt

    with dspr
    $ renpy.pause (0.5)
    window show 

    me "{enen}The library... Remember our last time here?{/enen}"

    stop ambience fadeout 3        

    ss "{enen}I sure do. Come on, let's show no fear.{/enen}"

    window hide

    $ renpy.pause (0.3)     

    scene bg int_library_day

    with dissolve    

    show un normal pioneer at cleft  

    with dissolve

    play ambience ambience_library_day fadein 3

    window show 

    un "Oh, here you are. Please, look at my creations."

    me "Wow, these are really dresses. Did you make them by yourself?"

    window hide 

    show un shy pioneer at cleft

    show ss smile casual at cright

    with dspr   

    window show     

    ss "{enen}Oh, great!{/enen}"

    un "This is my hobby and my weakness. I am obsessed with tailoring."

    un "Just having scissors and the needles in my hands could make me happy."

    show un normal pioneer at cleft 

    show ss smile2 casual at cright 

    me "Thank you. That's very handy."

    un "Well, now you have to try them..."

    un "And then, Samantha, I will fit them well for you."

    un "It is important to prepare good scissors..." 

    show un evil_smile pioneer at cleft

    show ss surprise casual at cright   

    with dspr   

    un "I like them big and shiny. And, of course, as sharp as possible!"
    un "These huge ones seems very promising," 

    un "Сould skin a crocodile alive with them!"

    un "We'll cut off all the extra pieces to the basket,"

    un "Fill it with cloth and everything, because we're serious!"

    show un smile pioneer at cleft with dspr  

    window hide

    $ renpy.pause(0.5)      

    me "{enen}Have fun here... I shall wait outside.{/enen}"

    show ss scared casual at cright

    ss "{enen}Don't leave me! She is so mad-eyed!{/enen}"

    show ss surprise casual at cright     

    me "Hey, Lena, should we go outside, give her a moment?"

    un "You go, of course. For me it is enough to turn away."

    show ss shy casual at cright    

    me "{enen}We can not leave without dress nor I can stay.{/enen}"

    window hide 

    show ss shy2 casual at cright

    show un normal pioneer at cleft

    with dspr

    window show     

    ss "{enen}No way, stand here! You must obey.{/enen}"

    me "{enen}Suit yourself then. I'm not looking.{/enen}"



    show ss normal casual at cright

    

    ss "{enen}You acting like I'm going to undress.{/enen}"
    ss "{enen}I'm not, Semyon. Chill out. Wrong guess.{/enen}"

    window hide

    $ persistent.dress = 'purple'   

    show un serious pioneer at cleft

    show ss shy dress  at cright 

    show undds at cright    

    with dissolve

    window show     

    un "You put it up over the uniform?"

    un "What if the size is wrong? This just won't do!"

    me "It's not wrong, everything is great. Well done."

    me "Too bad there is no time to dress all actors"
    me "In your brand clothes. You are a great tailor indeed."

    window hide 

    show un normal pioneer at cleft 

    with dspr

    window show     



    un "You think so?... What about the second dress?"

    window hide 

    hide ss

    hide undds

    with dspr

    $ renpy.pause(0.5)  

    show un serious pioneer at cleft

    $ persistent.dress = 'red'   

    show ss nosmile dress  at cright 

    show undds at cright  

    with dissolve   



    

    window show

    un "At least it suits her."

    me "Yes, as the first one. But she needs only one."

    me "{enen}They both look great. What do you think?{/enen}"

    show ss unsure dress at cright 

    show undds at cright    

    ss "{enen}It's so hard to decide which one to pick...{/enen}"
    ss "{enen}I need your judgment, Sam. Please. You choose it.{/enen}"

    

    menu:

        "The first one":

            me "{enen}My vote goes for the first one, if it matters.{/enen}"

            me "{enen}It fits your eyes and makes them sparkle even better.{/enen}"

            $ sp_spts = 99                     

        "The second one":

            me "{enen}The second one's well-colored, looks so nice{/enen}"

            me "{enen}Just like your homeland's flag with stars and stripes.{/enen}"

            $ persistent.dress = 'red'

    window hide     

    ss "{enen}I'd like to wear them both... I am agree, we'll take this one.{/enen}"

    window hide

    $ renpy.pause(0.5)  

    show ss normal casual at cright

    with dissolve

    window show 

    stop music fadeout 5    

    if sp_spts == 99:

        $ persistent.dress = 'purple'    

    ss "{enen}Thank you so much for what you've done.{/enen}"

    window hide 

    show un shy pioneer at cleft    

    with dspr

    window show

   



    un "No need for gratitude. Just count it as my modest contribution"

    un "To our common work. I care about the play right as you are."

    me "It was about the play and nothing else?"

    play music music_list["silhouette_in_sunset"] fadein 5  

    me "Maybe some sort of a conciliatory gesture..."

    window hide 

    show un serious pioneer at cleft

    show ss unsure casual at cright   

    with dspr

    window show

    th "Talking too much, killing the mood again,"

    th "Why can't I keep my tongue behind my teeth?"

    un "May be this too... A gesture of good will?"

    un "Or just a trick by sneaky Lena? Is she trying to appease you?" 

    me "Nobody thinks that. You're a good friend."

    me "It's hard to overestimate your help."

    show ss normal casual at cright     

    me "The words were said, unkindly words. But words are just the air's vibrating."

    stop ambience fadeout 5



    me "Your doings matter more than that. Let's close the topic. It's frustrating."

    window hide 
    $ renpy.pause(0.5)
    play sound sfx_dinner_horn_processed

    $ renpy.pause(1.0)    
    window show
    un "Thank you... There was a horn, go have a dinner."

    un "I'll close the door and follow. "

    window hide

    $ renpy.pause(0.5)  



    $ persistent.sprite_time = 'day'

    scene bg int_dining_hall_people_day 

    show notl:

        pos (1697,0)   

    with fade 
    $ renpy.pause(0.5)    

    show sl normal pioneer

    show ss nosmile casual at right

    with dissolve    

    play ambience ambience_dining_hall_full fadein 3

    $ renpy.pause(0.3)

    window show

    me "It's done: Samantha got a dress now. How is it going on a stage?"

    sl "Not good at all. Still can't find our Tolik."

    show sl serious pioneer



    sl "And this Alisa here... She is a scourge of god."

    menu:

        "Forgive her":

            me "Try to forgive her, she is clearly nervous"
            me "...About the play. It is her first performance. "

            $ sp_sl += 1

            sl "That's not excuse for being rude. No way."

            me "I'm sure she's capable to understand how wrong she is."

            me "Give her some time. You always were so patient, keep it up."

            me "Or just endure her presence till' this evening."

            

        "It's not so easy for her too":

            me "Sometimes she has to be defensive against Zhenya's quips."
            me "You also threw at her a couple of remarks..."

            $ sp_sl -= 1        

            sl "Are you implying that her bearing is our fault?"

            me "I mostly speaking about Zhenya... You know her character."

            sl "I know that she is absolutely harmless."

            sl "Can't speak the same about your troublemaking redhead."

            me "At first... Please, notice: she's not mine."

            sl "It doesn't matter, skip this issue."

            me "Alisa surely will handle with her role."
            me "Just don't confront her for a day. Leave her alone."

            window hide

            show sl angry pioneer

            with dspr

            window show 

            sl "I will respect your wishes."





    window hide

    $ renpy.pause (1)   

    show ss laugh casual at right

    show notl:

        pos (1697,0)    

    with dspr

    window show 

    ss "{enen}Semyon can't wait to see you dressed as a boy,{/enen}"

    show ss grin_smile casual at right     

    ss "{enen}Haven't you seen those leggings? Oh, you will enjoy!{/enen}"

    window hide

    show sl surprise pioneer

    with dspr

    window show 

    sl "What is she saying? I don't understand. "

    me "She says you better play a lady, not a boy. "

    me "Because you have to hide your beauty now."


    sl "The corset isn't tight..."


    show sl shy pioneer
    
    extend " Oh, you mean braid!.."

    window hide 

    $ renpy.pause (0.7)

    show mt normal panama pioneer at fleft   

    with dissolve

    show sl normal pioneer

    show ss normal casual at right

    show notl:

        pos (1697,0)     

    with dspr

    window show

    stop music fadeout 7    

    mt "How is it going guys? So many things to do..."

    mt "Hey, where is Tolik? Has he eaten something?"

    sl "He ran away somewhere. We tried to pursuit..."

    sl "No chance. No one expected such a speed from him."

    window hide

    show mt angry panama pioneer at fleft    

    show sl scared pioneer

    show ss surprise casual at right

    show notl:

        pos (1697,0)      

    with dspr

    window show     

    play music Lou_Inspired

    mt "Dammit, Slavyana!"

    mt "You had but one important task - to watch this dummy Tolik."

    show ss sad casual at right    

    me "What's the problem? Who he actually is?"

    mt "Semyon, no time for explanations."

    mt "What are you waiting for, Slavyana? Get up now!"

    mt "You'll never find him in your plate. Go look outside."

    window hide


    $ renpy.pause (0.5)     

    hide sl with dissolve

    show mt normal panama pioneer at fleft   

    hide notl   

    with dspr

    $ renpy.pause (0.5)

    window show 

    me "What need of sending Slavya off like this?"

    me "I'd be unhappy if you break my lunch."

    mt "It was too harsh? Maybe I got overexcited for a second..."

    mt "But lunch is precious for Tolik! It worries me that he is missing."

    show ss unsure casual at right 

    me "Not missing anymore! I see him: there he is!"

    me "He looks alright and happy as a hyppo."

    mt "Oh, he is back. What a relief."

    $ renpy.pause (0.5)   

    mt "Now I can deal with this rassolnik... A blessing for an empty stomach."

    me "Can I reminder about Slavya?"

    mt "I think her mistake is forgivable."

    me "I'm not about that. I will bring her back."

    mt "Please, do. Good luck."

    show ss nosmile casual at fright with dspr

    me "{enen}Don't move! You - stay!{/enen}"

    window hide

    show ss serious casual at fright with dspr 

    with dspr

    window show

    stop ambience fadeout 3

    ss "{enen}I'm staying. But I'm not a dog, okay?{/enen}"

    

    window hide

    $ renpy.pause (0.5) 

    $ persistent.sprite_time = 'day'

    scene bg ext_dining_hall_near_day

    play ambience ambience_camp_center_day fadein 3 

    with dissolve



    th "So now I look for Slavya. Could be not that easy."
    th "To say \"I'll bring her\" was a bit too optimistic..." 

    th "At least I'll try... And where should I start?"

    th "This fatty Tolik - where does he live? Except of the canteen?"

    th "...And where Slavya does? {w}I'm definitely not a Sherlok."

    window hide

    scene bg ext_houses_day with dissolve

    window show

    th "Campleader could have placed Slavya somewhere near to her..."

    th "Let's try that house."

    window hide

    scene bg ext_house_of_sl_day with dissolve

    $ renpy.pause (0.5) 

    play sound sfx_knock_door2

    $ renpy.pause (1.5)

    stop ambience fadeout 2  

    sl "Come in."

    
    window hide
    $ renpy.pause (0.4)     
    play ambience ambience_int_cabin_day fadein 3

    scene bg int_house_of_sl_day 

    show sl normal pioneer

    with dissolve

    stop music fadeout 9    

    me "A piece of cake! That is how easy was to find your house."

    sl "How didn't you know already where I live?"

    me "How would I know?.. You're not a home-bird, I suppose."

    me "You must be spending time anywhere else from dawn to dusk."
    me "Doing numerous things across the camp."

    me "Oh, by the way: Tolik is back. We found him right in the canteen."

    sl "I've seen him passing near of me."

    me "Why didn't you come back then?"

    window hide 

    show sl serious pioneer 

    with dspr

    window show   

    "Slavya has nodded, chuckled bitterly."

    me "Olga was wrong at dinner. She mistreated you. I'm sorry."

    sl "She may be wrong or not. I can be negligent sometimes."

    me "That's nonsense. How about coming back to guys?"

    me "Actors are missing one by one...  Let's stop this line of disappearances?"

#    me "Let's stop this numerous disappearences."





    sl "You should come back alone... I don't belong there."

    play music music_list["two_glasses_of_melancholy"] fadein 7 

    me "You don't want to return? How so?"

    me "You're needed there, it's a big deal. Everyone is waiting."

    sl "Maybe I'm taking too much on myself."

    sl "Perhaps they can do better without me."

    me "I'll tell you what... Let Genda be my judge,"

    me "You're facing now the most dedicated recluse. There is no second one arond like me."

    me "It's me who's lazy and afraid of many things,"

    me "I hate to stay in public, in the crowds. I have to force myself to do that."

    me "But even so, I'm trying to support the play, not giving up,"

    me "As much as I would like to quit I want to win this challenge."

    sl "Sure thing. You do this for the others."

    sl "But for what sake should I be buckling down?"

    sl "You've heard their words. You've seen how do they treat me."

    sl "Is it a curse? Am I attracting human impudence?"

    me "You got a point, but hiding won't do any good..."

    me "By staying here you will only punish me and you."

    th "Amazing: who is motivating who now. Nobody would believe."

#

    sl "I don't intend to go, Semyon. It's my decision."

    sl "I've served quite enough for them. And now I'm resting..."

    sl "So don't you wait for me, don't try to persuade."

    sl "It's not about the persuation. I have no more to give."

    me "I can't believe you're that capricious suddenly. What, do I have to drag you there?"

    window hide 

    show sl angry pioneer   

    with dspr

    window show 

    sl "Just go away! Or I myself will hit you!"

    th "It looks like she is up to fight, no jokes."

    th "And I don't see how could I persuade her."

    menu:

        "To give up":

            me "You won, I'm leaving."

            sl "Don't send anyone else to me."

            jump ss_d5slfail_eng

        "Go all the way":

            $ sp_sl += 1

            me "Well, you are stubborn... Fine. I'll stay here too."

            me "I hope you are alright with that."

            sl "No need to fool around. You must return, you care about them."

            me "No, I don't need the play without you."

            sl "You're being awkward now with this plain fllatery..."

            me "It was the truth. And I would say the same about my other friends."

            window hide 

            show sl serious pioneer 

            with dspr

            window show             

            sl "Compared to yours my role is not important."


            sl "I am replaceable. Some girl would read my text..."

            sl "They'll manage without me just fine."

            sl "Romeo is another case, there is no way to replace you."

            sl "It means that we expect a lot of guests next hour,"
            sl "Knocking this door, calling you out..."

            sl "So tell me, how should I explain your presence?"

            me "Leave it to me, I'm taking care of it."

            me "I will explain that I can't play because of sickness:"

            me "Panic attacks and fever and so on..."

            me "And they will have to cancel everything, I guess."

            window hide 

            show sl shy pioneer 

            with dspr

            window show             

            sl "You're going to do that, really? Just to cover up my leave?"
            sl "It's very noble of you, what to say..."

            sl "You're Mephistopheles, the devil of negotiations!"
            sl "For I am feeling guilty now. Congratulations."

            menu:

                "Call her on the stage":

                    $ sp_sl -= 2                        

                    me "I see the common sence is coming back to you."

                    me "Let us return together now. We will avoid all the forthcoming shame and trouble."

                    window hide 

                    show sl angry pioneer   

                    with dspr

                    window show                     

                    sl "So you just trying to coax me out of here?"

                    sl "It was your plan? You never wanted to protect me?"

                    me "No, no, I truly was sincere! It is decided, I'll stay here..."

                    sl "I'm not a fool. Not buying it the second time. "

                    me "Try to be calm. Why shooting from the hip?"

                    me "We still have things to talk about. "

                    sl "No. Leave me now."

                    sl "Try other houses. Look for me somewhere else."
                    "..."
                    window hide
                    $ renpy.pause (0.5)
                    jump ss_d5slfail_eng

                    

                    

                "Talk more":

                    jump ss_d5sl1_eng

                    

label ss_d5sl1_eng:



    stop music fadeout 3    

    me "Explain me then, what happened to you."

    me "Why is this change in you so suddenly?"

    play music Warm_evening fadein 5    

    window hide 

    show sl sad pioneer 

    with dspr

    window show     

    sl "I can't forget her words."

    me "Olga is always high demanding, never thankful..."

    sl "It is Alisa who I'm speaking of. She said that everybody hates me."

    sl "I should appreciate her frankness on that matter."

#    sl "Others just don't want to hurt me."

#    sl "Or don't have enough courage?"

    me "Believe me, she is not happy herself"
    me "About the awful things she said to you."

    sl "But she was right. I am the black sheep, it is obvious."

    sl "I tried to help everyone. What is the outcome do I see?"

    sl "They are annoyed by me. They run of gratitude long time ago." 



    sl "You saw it - Olga takes my help for granted, like if I owe her."

    sl "As for the other guys, I am a bothering upstart for them."
    sl "Or maybe even worse... A toady or a snitch!"


    me "You are exaggerating very much. Some people love you,"
    me "Some people not. Some don't appreciate your work, but others do."

    me "No matter what, they need you, all of them. I hope it mean something to you."

    window hide 

    show sl serious pioneer 

    with dspr

    window show     

    sl "They count me as some kind of overseer,"

    sl "But I get nothing for my efforts."                                            

    me "What actual reward do you expect? And would it be a true good deed,"



    me "If you expecting something back for it?"

    me "Your kindness should be an award itself."

    me "Even when no one knows about your charity."

    sl "I do appreciate your lesson of true altruism,"

    sl "Although it isn't in my nature to be self-righteous."

    sl "You see, I can't be satisfied by myself,"

    sl "When others aren't. And I have nobody to rely on."

#

    sl "What else could I do? Learn to be egoistic?"        



    me "Is it so bad for real? I can't believe that..."

    me "What about me? Am I among the douchebags?"

    window hide 

    show sl normal pioneer  

    with dspr

    window show     

    sl "No, you are good... In this dark cloud you are a silver lining."

    stop music fadeout 3    

    sl "Too bad that you're mistreated too. And have no will or power to withstand."



    me "Withstand to what? They're mostly nice with me,"

    me "And, most importantly, I'm not deprived of freedom."

    window hide 

    show sl smile pioneer   

    with dspr

    window show

    play music music_list["i_want_to_play"] fadein 3    

    sl "We are too soft... Let's show our teeth. We need to give them a good lesson."

    me "You frighten me a bit with such rebellious ideas..."

    sl "I know. There was a moment in the morning,"

    sl "You climbed Genda... there I felt adrenaline."

    sl "My life is dull, you know. I never fool around."

    sl "It lasting from my early childhood..."

    sl "There was no time to prank and play, I've always been a help,"

    sl "I'm tired of it. I eager to catch up. Or just to have some fun!"

    sl "So it is time to do something naughty,"

    sl "Once prudence is considered here for cowardice."

    me "What is this naughty thing exactly?"

    sl "I do not know yet. If you're with me, we need to make a plan."

    me "Another question: who do you mean by \"them\"?"

    me "You're planning a revenge to who exactly? Olga? Or Alisa?"

    window hide 

    show sl surprise pioneer    

    with dspr

    window show     

    sl "They both deserve it! It's just a question how to start..."

    sl "So will you help me?"

    

    menu:

        "I will":

            $ d6_slchoice = 1       

            $ sp_sl += 1

            me "Ready to try. Olga insisted that I should join more activities." 

            me "A proper pioneer can't miss an opportunity."

            window hide 

            show sl smile pioneer   

            with dspr

            window show             

#            me "When the cook dared to leave the pioneers without desserts."

#

 

#

#

            sl "What a good friend you are! I knew you will support me."

            me "I hope that you will understand me, too..."

            me "But to the implement of any plan, we'll have to leave the house anyway."

#            me "You'll still have to get out of the fog and to explain."

            window hide 

            show sl sad pioneer 

            with dspr

            window show             

            sl "It's true, but we don't have a plan yet."

            me "So let's go out and make some reconnaissance."

            me "It will be easier to improvise once we are there."

#            me "Let's do some recon."

            window hide 

            show sl smile pioneer   

            with dspr

            window show             

            sl "I got your point and I agree: we'll start with mingling in the crowd." 

            sl "We're going then. Lead on, Semyon! Let's make some trouble!"

            jump ss_d5slwin_eng

        "Refuse":

            $ d6_slchoice = 0

            $ sp_sl -= 1

            me "You're having a hard day... You're not yourself."



            show sl angry pioneer   

            

            me "Maybe just tired. You've had an interrupted dinner..."

            me "Or maybe it is me to blame. My awkward fall from Genda,"

            me "Could also cause even a brain concussion."

            me "You should have a good break for a day."

            me "I also think my presence here is not helping."

            me "So now I will go back to others,"

            me "I'm gonna tell them that you're ill."

            sl "You're going to lie then. I am fine as ever."

            sl "Know, what? I am tired of this hide-and-seek,"

            sl "If you are leaving, then I'm leaving too."
            me "What are you going to do next?"

            sl "Don't ask me, I'm not telling you."
            sl "Keeping my plans in secret from this moment."

            me "I hope you will be in a better mood before the evening,"

            me "And that you won't mess up with people's trust." 



            jump ss_d5slwin_eng

            

label ss_d5slfail_eng:

    window hide

    stop ambience fadeout 3

    $ persistent.sprite_time = 'day'

    scene bg ext_houses_day 

    with dissolve



    window show

    stop music fadeout 5

    "Leaving Slavyana, I decided to go home,"

    "To find some peace just for a moment."

    window hide 

    play ambience ambience_int_cabin_day fadein 3       

    scene bg int_house_of_mt_day

    show mt normal pioneer at cleft

    show ss nosmile casual at cright   

    with dissolve



    window show    

    mt "Hello, dear neighbor. How about some tea?"

    mt "We've opened the jam for this occasion."

    play music music_list["get_to_know_me_better"] fadein 5 



    me "I'll join you gladly. But what's the actual occasion?"

    mt "Uhm, nothing special. The swarm of insects drove us here, to be fair."
    mt "The heat too. It is unbearable to be outside right now."

    ss "{enen}Hey, did you find Slavyana?{/enen}"

    me "{enen}Yes... She is in her house.{/enen}"

    

    mt "What are you saying? What's the deal with Slavya?"

    menu:

        "She is ill":

            th "I'll only make it worse by blaming leader..."

            me "She is not feeling well today. Maybe ill."




            show ss sad casual at cright

        

            me "I persuaded her to have some rest."

            mt "To stay at home and skip the play? No way!"

            mt "She'd better visit our nurse, if it is serious."

            me "Doctors are useless, we've already been there..."

            show ss nosmile casual at cright            

            mt "Hey, don't you touch the doctors! My father is pathologist."

            show mt sad pioneer at cleft            

            mt "And he is great one. Respectful as he can with every soul,"


            mt "Or with a body, mostly... With every poor fellow on his table!"

            me "I recommend to leave Slavya alone for a good rest,"



            me "To cut the chances of her meeting with your father."

            mt "It is impossible, my dear. She is too old now." 

            show mt smile pioneer at cleft          

            me "What do you mean?.. Nobody is too old to die." 

            me "I guess you're saying that your father is retired..."
            th "Or maybe that her father is a woman, my oh my!"

            show mt normal pioneer at cleft             

            mt "No, he is active. She is too old for him because,"
            mt "He's very rare pathologist, one of a kind... {w}Pediatric."



        



        

            

        "She is offended":

            $ sp_sl -= 1        

            me "She is on strike. It will be hard to reason with her."

            me "I did my best, but all was useless."

            mt "Can you explain what is the problem?"

            me "You were too harsh with her, and others too."

            me "It seems now she's displeased almost of everyone."

            

    mt "Well, then I have to visit my assistant."

    mt "I'm leaving you in charge here. Pour the tea."

    me "To lure her out of the house is a problem."

    me "Please, let her be. We'll find a bearable replacement."

    window hide

    show mt smile pioneer at cleft

    show ss nosmile casual at cright   

    with dspr

    window show     

    mt "Want a parlay? I'll bring her in five minutes."

    mt "I'll take the risk, because in ten we will be out of the jam. "

    me "Whatever - five or ten - Slavyana won't give in."

    me "Be careful, she can kick you down the stairs."

    mt "Count ten minutes then. She will be here in time."



    mt "I'll win the bet - three days you help me. Deal?"

    mt "I have a lot of work for you, my friend!"

#    mt "To clean the square, to darn clothes..."

    me "Alright. But if I win... If Slavya does not come,"

    me "I wish the freedom for all duties."

    me "I will be sleeping half a day... So no more lineups - that's for sure!"

    me "And one more thing: you bring me breakfast into bed."

    mt "We have a deal. Don't smile, boy, you went cheap!"

    window hide

    hide mt 

    with dissolve

    $ renpy.pause(0.3)    
    window show

    th "Went cheap - no way! She must be bluffing..."

    $ renpy.pause(0.5)  

    ss "{enen}How come you spoken to Slavyana but not convinced her?{/enen}"

    ss "{enen}I wonder if she showed you to the door.{/enen}"

    me "{enen}She's not so fond of me. Your accusations go in vain.{/enen}"

    me "{enen}My persuasion power just has failed me again.{/enen}"
    window hide
    $ renpy.pause(1)        
    window show
    "As we were talking, time went by. Five minutes passed, and six."

    "I already believed that Olga can not make it..."

    play sound sfx_open_door_2

    window hide

    show ss normal casual at fright

    show sl normal pioneer at left

    show mt normal pioneer 

    with dissolve

    window show

    mt "Well, here we are, guys! I see, you're waiting?"

    mt "Sit down, Slavya, you are hungry."

    mt "So, I invited her to have some tea, you see?"

#    mt "As if casually"

    "The leader looked at her watch."

    mt "Come on, Semyon, cut for a guest a sausage."

    window hide 

    $ renpy.pause(1)

    hide mt

    hide ss

    show sl shy pioneer 

    with dissolve    

    "We finished all the tea, it was the end of sudden party."

    "Samantha called Olga to her house,"

    "To take a good look at the dresses,"

    "So me and Slavya were left alone."

    "The girl was looking very bashfully."

    sl "I know, I know... I am too soft."

    me "How did she lured you here? By what words?"

#    me "I guess she asked you for forgiveness..."

    sl "She told me everything about the bet."

    th "So Slavya came to spite me? Make me lose?.."

    sl "...That you agreed to help me for three days,"

    sl "In everything. There was no way I could refuse."

    th "What a deceit! The counselor outsmarted me!"

    th "She made a fool of Slavya too... Of both of us!"

    sl "You are so selfless for my sake..."

    sl "Although you always seem restrained in our conversations."

    th "She thinks I volunteer to help her,"

    th "Is there a point to tell the truth?"

#    th "The bet is lost, I am their slave now anyway..."

#    th "So let her think it's not against my will."

    sl "Today I was unfair to you..."

    th "The bet is lost, I am their slave now anyway..."
    th "So let her think it's not against my will."

    window hide

    show sl sad pioneer 

    with dspr   

    sl "No, I should stop the chatter. You are deep in thought,"
    sl "You must've been tired of me."

    me "Excuse me. Have you been saying something?"

    stop music fadeout 5

    sl "Please, nevermind. Let's go back to the stage?"

    stop ambience fadeout 2



    me "Of course. It is too early to relax."

    

    $ sp_sl += 1

    jump ss_d5slwin_eng

label ss_d5slwin_eng:

    window hide

    $ renpy.pause (0.5) 

    $ persistent.sprite_time = 'day'

    scene bg ext_stage_normal_day 

    play ambience ambience_camp_center_day fadein 2

    with fade
    $ renpy.pause (0.3)    

    show mz normal glasses pioneer at cleft

    with dissolve    

    show sl normal pioneer  at cright

    with dissolve

    window show

    mz "Semyon and Slavya! Where have you been?!"
    mz "Let me correct the question: where THE HELL have you been?"


    mz "And how THE HELL we could rehearse without you?"

    sl "We had... very important things to do. Forgive me."

    stop music fadeout 4

    mz "Just for the record: we've been waiting for an hour."

    me "You are alone here! Who are \"we\"?"

    mz "I am alone, you dunce, because everyone else is gone!"

    play music music_list["my_daily_life"] fadein 7 

    sl "What shall we do?.. There is no time to get them back."

    sl "The dinner is around the corner..."

    me "We have to skip the grand rehearsal. It is clear."

    me "We'll manage fine without it... If Zhenya won't forget her lines."

    me "Both of them lines... I have no doubt in other guys."

    window hide 

    show mz angry glasses pioneer at cleft  

    with dspr

    window show 

    mz "So irresponsible... I am amazed."

    me "Feel free to tell us your suggestions. Have you any?"

    mz "Have some suggestions about you, yes!"

    me "Well, shoot then."

    window hide     

    show mz bukal glasses pioneer at cleft

    with dspr

    window show     

    mz "Get rid of the redhead, you, mr. ladies' man."

    mz "What are you hoping for, exactly?"

    mz "She as an actress? Imagine all the booing she will get!"

    mz "She will be devastated, and you're the first man for revenge,"

    mz "After the play you have to run from her again!"

    me "What say you, Slavya? Do you think the same?"



    show sl serious pioneer     at cright

   

    sl "My own opinion's not important. You know better."

    sl "It was too hard to drag Alisa in the play... So let it be."


    mz "I will be laughing when she send Semyon to the infirmary."



    th "I shouldn't swear with Zhenya any more now,"

    th "There is no need to make her cry in front of Slavya..."

    me "Since we have nothing to do here anymore... Have a nice day!"


    mz "A-a-and he is running off again! What a surprise..."

    window hide
    $ renpy.pause(0.3) 
    scene bg ext_house_of_mt_day with dissolve



    $ renpy.pause(0.5)  
    window show    

    th "There are still three hours left before the play,"

    th "And I must spend this time with use. {w}Or not."

#    th "Every minute of preparation can mean a lot."

    stop music fadeout 7

    th "But really, should I pass those hours cramming,"

    th "Or go out for a walk somewhere?"

    window hide

    $ renpy.pause(0.5)



    $ disable_all_zones()

    $ set_zone("me_mt_house", "ss_d6home_eng")

    $ set_zone("library", "ss_d6une_eng")

    $ set_zone("beach", "ss_d6dve_eng")

    $ show_map()





label ss_d6home_eng:

    play ambience ambience_int_cabin_day fadein 2

    scene bg int_house_of_mt_day

    show mystical_box:

        pos (1348,0)    

    with dissolve    

    "Reading, reciting and repeating were my doings for an hour."

    "My knowledge has improved and so is my self-confidence."

    window hide



    

    menu:

        "Keep on learning my role":

            jump ss_d6urd_eng

        

               

        "Read other roles":

            jump ss_d6esc_eng

        

        "Go somewhere else":

            $ disable_all_zones()

            $ set_zone("library", "ss_d6une_eng")

            $ set_zone("beach", "ss_d6dve_eng")

            $ show_map()        



label ss_d6urd_eng:
            window hide
            with fade2
            $ renpy.pause (0.3)            
            play sound sfx_dinner_horn_processed            
            $ renpy.pause (1.0)             
            window show 
            
            "Persisting, but without special need,"

            "I suddenly felt tired, fell asleep,"

#

#

#

#

            "But now I hear a signal for a dinner."


            "That means I slept like prince until the evening."

            "Alas, my servants won't bring food to me."

            "So I will check myself what's in canteen."

            window hide         

            $ renpy.pause(1)            

            jump ss_d6din_eng





            

label ss_d6esc_eng:

    $ d6_nolife = 1

    window show     

    th "Enough of me with this Romeo, I'll take a look at other roles..."

#    th "I better entertain myself by reading other roles."

    th "Here I see Alisa's part - to know it better might be useful."

#    th "Just in case."

    window hide
    with fade
    $ renpy.pause (0.3)             
    window show 

    th "What time is it?.."

    th "One hour less, alright."



    th "All letters blend into a porridge, I'm sick of sitting here!"

    stop ambience fadeout 2  



    th "So no more reading, gonna take a walk..."

    window hide

    scene bg ext_house_of_mt_day with dissolve

    $ renpy.pause(0.3)    



    play sound sfx_dinner_horn_processed

    $ renpy.pause(1)    
    window show 
    th "Is it a horn? Already?"

    th "The dinner must be earlier today..."

    th "It's fine by me. And more than fine by my starveling stomach."

#    th "But now I'm happe to hurry up to the canteen."



    window hide         

    $ renpy.pause(1)            

    jump ss_d6din_eng









label ss_d6une_eng: 

    $ d6_treasure = 1 

    $ sp_un += 1    



    play ambience ambience_int_cabin_day fadein 2

    scene bg ext_house_of_mt_day with dissolve

    th "I wonder how is Lena's business going..."

    th "I'll check it out. Let's drop down to the library."
    window hide
    $ renpy.pause (0.5)
    scene bg int_library_day
    with dissolve
    show un smile pioneer 

    with dissolve

    $ renpy.pause(1.0)  
    window show
    un "You come alone? What for? I mean... How can I help?"

    me "I came alone... Without special matters, to be honest."

    un "I thought there must be grand rehearsal now... "


    me "That's what was planned, but all the actors disappeared."

    me "So I am free for now. I'm sorry if I bothered you..."



    show un shy pioneer



    un "Nothing like that! Sit down... Take a chair."

    show un smile pioneer  

    un "No one is out of place in our library."

    me "Is it so hopelessly deserted that welcomes anyone?"

    un "I mean that it's a public place, that's it..."

    un "But you are right, there is no public most of time."

    me "Then catch the moment, here I am. The visitor."

    me "And I am honored to be your only client."

    show un shy pioneer 

    un "So I suppose you want to take a book..."
    me "I do."

    un "Why would you need it just before the play?.."

    me "I know my part. That words won't leave my head..."
    me "For next ten years, I'm afraid. So let's put there something else!"

    show un normal pioneer      

#    me "Otherwise you can't make up for lost time."

    me "...Alright, I'm kidding. I'm actually sick of reading."
    me "Just looking here for a brief distraction,"
    me "It would be nice to clear a head, you know?"

    me "Maybe you'll give me something with the pictures?"

    play music music_list["your_bright_side"] fadein 7  

    un "There are too many. Look around, take any books,"

    un "If you came here without second thought."



    th "Was there a second thought? I don't know either."

    th "...Let's check how rich these bookshelves are."

    window hide 

    hide un with dspr

    $ renpy.pause(0.5)  

    window show

    me "There is a booklet: the reminder for the teachers."
    me "Contains the secrets of upbringing with a ruler."

    me "A desk book of O.D. and her colleagues."

    un "You mean our Olga? No, she is very soft..." 
    un "She can't do worse than put you in a corner."

    me "True that, she's not a fighting type,"
    me "But still, the way she's treating us sometimes..."
    me "It's like she wants you standing up against the wall,"
    me "With your arms raised... Not just in the corner!"

    me "...Oh, here is poetry. Easy to tell it by the dust."

    un "I wiped it all out yesterday, do not invent."

    me "You're cleaning here? What if I'll look behind the cupboards?"

    me "I see a spiderweb that could be old enough,"
    me "To hold some extinct species there."

    un "I'm not a janitor, just doing what I can."

    me "Here is something pressed between the racks..."

    me "A book, of course. It's Stevenson, the Treasure Island."

    me "Such a good novel thrown away... Shame on this library."

#

#

    "The sheet flew out once I have opened the book."

    window hide

    show un normal pioneer at fright with dspr

    window show 

    un "Give me that page, I'd paste it back in there."


    me "Wait, wait... We're lucky! It's a treasure map!"

    window hide

    show treasure_map with dissolve:

        pos (400,200)
    $ renpy.pause(0.5)  

    window show
        
    th "But we're unlucky with this weird language..." 

    un "You're so delighted about simple illustration?.."

    un "Give it to me now, I will make a restoration."

    me "It's not an illustration, it's a real handmaded map,"

    me "Hiding a treasure behind it... Or a trap!"

    window hide

    hide treasure_map

    show un normal pioneer 

    with dissolve

    window show

    un "Oh, now I see it... It's a simple drawing, funny,"

    un "It's obviously made by kids."

    me "The kids that left for us some fabulous riches."

    me "Their treasure hidden on the nearest island,"

    me "It is awaits for us. We can get there and be back before the dinner."

    me "...What say you?"
#    me "But if we suddenly will be late - we won't die of starvation."

    window hide

    show un smile3 pioneer with dspr

    window show     

    un "Quit everything and go to dig the treasures with the children's map?"

    me "Precisely. The only thing we need is your decition."

    window hide

    show un serious pioneer with dspr

    window show     

    un "So you're not kidding, are you? What is going on?"

    un "Sounds like a children's game..."

    me "For me it also sounds like an adventure."

    me "Will you prefer to stay, extending your imprisonment in here and nothing more?"

    me "Outside this library there is a summer passing by!"
   

    un "It's tempting, but... How can I leave my post?"

    un "And how do I explain this to the leader?"

    me "We will be back in no time, do not worry."

    me "No one will ask about it and no one will even notice!"

    window hide

    show un smile pioneer with dspr

    window show

    un "You're so enthusiastic about this... It's contagious."

    me "A strange mood, yes. It happens not so often."

#    me "I just follow where my soul leads, Lena."     

    un "...I'm giving in, let's go!"



    stop ambience fadeout 2

    window hide
    $ renpy.pause (0.5)

    $ persistent.sprite_time = 'day'

    scene bg ext_boathouse_day
    with dissolve
    
    show un normal pioneer at left  

    with dissolve



    play ambience ambience_boat_station_day fadein 3


    $ renpy.pause (0.5)
    window show    

    me "And now, when we are really going..."

    me "I'm not so fond of this idea anymore. Ha-ha."

    un "Yet we are going. I am happy to participate,"

    un "On one condition: you will do the digging."

    window hide

    $ renpy.pause (0.5)    
    scene cg un_boat with fade

    $ renpy.pause(1)

    window show 

    un "You need my help with paddles maybe?.."

    me "No, I don't. You are a navigator here, so sit still,"

    me "Lead us past all the reefs and shallows... And let me do the rowing."
    


#

    un "You gotta save the strength for the way back,"

    un "There will be heavy threasure with us, don't forget."

    un "You'll have your rightful half of those findings,"


    un "How would you like to spend the fortune?"

    me "Well, that depends on what they're capable to sell me."


    me "Not much, as I assume... And in this camp especially."

    me "Can't buy a thing, even a bucket of potato peelings,"

    me "I'm gonna need an access to black market to get some buns and chocolates."

    un "A very humble wish."

    th "...As long as alcohol and drugs are not to buy."


    un "...You drove us to the place, well done."
    me "Yeah, let's get out. Please, watch your step."


    window hide
    $ renpy.pause(0.3)


    $ persistent.sprite_time = 'day'

    scene bg ext_island_day
    with dissolve
    show un laugh pioneer at cright  

    with dissolve



    play ambience ambience_lake_shore_day fadein 3



    $ renpy.pause(0.5)

    window show     

    un "Here is your treasure island."

    un "I hope it's capable to make your wish come true..."
 

    me "You didn't tell about your own wish. Do you have one?"

    window hide

    show un shy pioneer at cright

    with dspr

    window show 



    un "What if I have a wish, but not about the treasure?"


    me "You need a special treasure then - look for a magic lamp."

    show un smile pioneer 



    un "You mean, I need a genie? It will be hard to find him..."


    me "Who knows. The island looks so magical from here,"

    me "Alas, we can't appreciate this beauty for too long... {w}In fact, we came here to ruin it!"

    me "It's time to get my shovel dirty and to damage the landscape,"

    me "Digging from here... and till' the evening! Do you know the joke?"
    me "Whatever. I will go along the coast,"

    me "And will not rest until our treasure's found!"

    "..."

    play sound sfx_hatch_drop   

    window hide

    show un surprise pioneer at cright

    with dspr

    window show     

    me "Hey, something clinked! It might be our chest!"

    me "Come here, friend, just let me dig you out..."

    window hide

    show un normal pioneer at cright

    with dspr

    window show     

    me "Ugh, no... That's just a glass can. Really dirty."
    un "Let's pull it out anyway?"

    me "Sure... Since there is nothing better here."

    me "It's hard to see what is inside, the mud is everythere."



    un "Is that our treasure? How unfortunate."

    me "At least we found it, right? And just about time,"

    me "I'm already kinda bored of digging."

    un "Open the can then, get it over with."

    th "Easy to say. I'm trying to unscrew the lid,"

    th "And in the same time trying to disguise,"

    th "That I am trying really hard already!"

    "At last, the lid succumbed, and voila:"

    "Another nuisance - our can is full of earth."
    "But, fortutately, there is something else too."

    me "Our first find will be... a chess queen. Here you are."

    me "Once it was black, now more like brown."


    show un smile pioneer at cright 

#    un "Could be a reference to Lev Kassil. Is there something else?"
    un "Could be a hint to something. Is there more?"
#

    me "Let's dig in deeper... Here is a magnifying glass."

    stop music fadeout 5

    me "A glass ball too... Along with someone's tooth and five kopecks."

    me "That already looks a little bit like treasure."

    un "Hold on, here is a letter. Let me take it, careful..."

    un "The envelope is rotten, but it's readable. I'll try."

    play music music_list["always_ready"] fadein 5  

    un "{i}A message to the people of next era -{/i}" 
    un "{i}Of peace and freedom throughout the world!{/i}"
    un "{i}(If it's not yet, don't open then!){/i}"



    me "I think we're fit. It is quite peaceful here."

    un "I open it... {i}Hello, the people of bright future!{/i}"

    th "Oh, how ironic. Bright or not, I'm actually from there."

    un "{i}As proud pioneers of the Sovyonok, we write to you.{/i}"

    un "{i}Nikita, Olga, Styopa-Fritz, his brother Puffin...{/i}"

    un "{i}And Abraham, who is refused to send his gift,{/i}"

    un "{i}So you won't get his spyglass. We are sorry.{/i}"

    un "{i}Even a jewish pioneer can not be greedy,{/i}"

    un "{i}So Styopa has hit him in the nose for that.{/i}"

    un "{i}Don't get us wrong: in Soviet Union we all are equal, even jews.{/i}"

    un "{i}So, not to hurt Abraham's feelings,{/i}"

    un "{i}Fritz also hit another fella, Tursunbek.{/i}"

    me "And almost had no pleasure, right. Hey, by the way..."

    me "There's certainly no racism in Sovyonok nowadays,"

    me "As not a blond man, I'm pleased and happy with this fact,"

    me "There is a question still that bothers me:"

    me "I never see ethnic minorities. Where are they?"

#

    me "Or maybe it's a question that I shouldn't ask?"

    me "I promise, I will ease my curiosity,"

    me "If there is risk to disappear like they did!"

    window hide

    show un serious pioneer at cright

    with dspr

    window show     

    un "It's just you didn't notice them... Should I continue?"

    me "Yes, please. Not interrupting you again."

    

    un "{i}We send you our precious things, please put them to museum,{/i}"

    un "{i}So that you will remember us, the children of the past,{/i}"

    un "{i}And you will come to us one day, using the time machine.{/i}"

    un "{i}(You will be capable to look through the spyglass then).{/i}"

    un "{i}And here we solemnly swear to make every effort,{/i}"

    un "{i}To bring the promised bright future closer!{/i}"

    un "{i}Or else you will never get our message. And you will never visit us.{/i}"

    stop music fadeout 5

    un "{i}We're hoping for the best. Remember, we are waiting!{/i}"

    window hide

    show un smile pioneer at cright

    with dspr

    window show 

    un "Here is their signatures and the date: 1972"#Nineteen seventy-two."

    play music music_list["your_bright_side"] fadein 3  

    me "That's it... How do you like this treasure?"

    un "It's very cute. I mean, the whole this treasure thing..."

    un "I've hid a hoard too once as a chield. But I forgot the place."

    me "Let's check the can again. There must be something else."

    me "Yes, there are photo slides... But they are spoiled, what a pity."

    me "Wait, one is good enough..."

    window hide

    show youngod at center with dissolve

    $ renpy.pause(3)

    window show     

    me "Amazing! Look who's here!"

    window hide 

    hide  youngod with dissolve

    $ renpy.pause(0.5)

    show un laugh pioneer at cright

    with dspr

    window show    

    un "Wow, big surprise! So it is our leader's treasure."

    me "She must be an old-timer here, a long-time guest..."

    window hide

    show un smile pioneer at cright

    with dspr

    window show     

    un "I wonder what became of her old friends."

    me "They, probably, grew up of pioneer shoes and shirts... Of all of this."

    un "I'd like to know a little more."


    me "Yeah, we will ask her in the camp... We have to be there soon."



    show un normal pioneer at cright

   

    me "But, firstly, take this final find."

    un "What is it? It's a hairpin..."

    me "It could be hers. I guess she will be happy that we found it."

    window hide

    show un normal pioneer at cright

    with dspr

    window show     

    me "I'm taking it for now. Along with the slide photo and the letter."

    me "They won't last long here anyway."

    me "Let's put the other things back in their can. I'll bury it."

    stop music fadeout 5    
    window hide
    with fade2
    window show
    "When it is done and all the useful finds are carefully put in the pockets,"

    "The time has come - we are returning to the boat."

    window hide

    scene cg un_boat with dissolve2

    $ renpy.pause(1)

    play music music_list["memories"] fadein 3 

    window show     

    un "Know what I'm thinking of?.. We're not alike those children,"

    un "We wouldn't give away a valuable thing,"
    un "How can we tear it from the heart and put it in the jar?"

    me "I can't because I don't have any valuables."

    me "What, do you also wish to write a word or two for our descendants?"

    un "I don't. I'm saying that those kids believed in something."

    me "Of course. That's what the little suckers do."

    me "What about you? I'm sure you were believing too."

    un "I don't remember this. It's like a veil in my head,"

    un "That separates the careless memories."
    un "I wish to be that chield again. To know again, what I've been thinking of."

    un "To prove myself that I was optimistic once." 

    un "Sometimes it feels like there's no past, there is no future,"

    un "And even if there is - I do not need a single thing from it."

    me "Our trip has made you feel so melanholic,"

    me "Perhaps it was a bad idea, after all..."

    un "No, everything was good, and more than that,"

    un "I'm sad because it's over... But I'll get myself together."

#

    stop ambience fadeout 2

    stop music fadeout 7    

    un "Don't mind me, please... {w}We're already here. Let's go back." 

    window hide
    $ renpy.pause (0.5)

    $ persistent.sprite_time = 'day'

    scene bg ext_library_day
    with fade
    $ renpy.pause(0.3)   
    show un serious pioneer at cright

    with dissolve 


    play ambience ambience_camp_center_day fadein 2



    $ renpy.pause(0.5)  

    window show     

    un "I will be in the library until the dinner."

    me "I thought at first we'll bring our find to Olga..."

    un "There is no need: look, here she goes to us."

    un "She's gonna punish me for leaving."

    window hide 
    $ renpy.pause(0.4)
    show mt normal pioneer at left  

    with dspr   
    $ renpy.pause(0.4)
    window show

    play music Father_of_Pearl fadein 3

    mt "The temple of The Book was left without priestess..."

    mt "Is that acceptable on this important day? How do you think?"

    "..."

    mt "...I think it's bearable. Not a big deal. But actually I hope"

    mt "That deal that made you leave your post was big enough."

    me "It can't be bigger, miss! We found a letter from the past!"

    window hide 

    show mt surprise pioneer at left 

    show un normal pioneer at cright 

    with dspr   

    window show     

    me "Look what we've got! A big surprise for you!"

    mt "Is it a piece of dirt? Hey, why are you so dirty? Your poor clothes..."

    me "Forget the dirt, just take a look! And reunite with your hairpin!"

    me "Come on, it must a heartwarming moment!"

    me "The precious thing is back to you! Embrace the memories!"

    me "Get sentimental! Cry and praise us! That's how it should be..."

    window hide 

    show mt angry pioneer at left with dspr

    window show 

    mt "You have mistaken. It's not mine. Now get it back."

    mt "Bury this thing under the ground right where you found it."

    me "How so?.. You seem familiar with this thing. You know it?"

    mt "I gotta say you are the one bothersome quirky weasel!"

    window hide 

    show mt normal pioneer at left with dspr

    $ renpy.pause(1)

    window show     

    mt "...You undrestood it right, I know this hairpin."

    mt "But I don't like it. I don't want to see it."

    un "We also have a couple of another things..."

    mt "What is it?.. Papers? Photo? Wait, hold on."

    window hide 

    show mt smile pioneer at left

    show un smile pioneer at cright 

    with dspr

    window show         

    mt "Just look at this! How little I am here!"

    mt "Yes, now I see what is this thing about..."

    mt "My memories about those days are coming back,"
    mt "When I am looking at this photo. Sweet..."

    mt "Well done, guys. Thank you. I will keep this."

    window hide 

    show mt normal pioneer at left with dspr

    window show     

    mt "And, please, excuse me. I was rude with you."

    mt "That hairpin has reminded me of some unpleasant moments."

#

#

    un "If I may ask... We were also wondering,"
    un "What happened to your friends?.. I mean the other kids."

    show mt smile pioneer at left   

    mt "Who knows? I never saw them since that year,"

#

    window hide 

    show mt grin pioneer at left with dspr

    window show 

    mt "Nikita was the one exception. Oh, what a reckless guy..."

    show un smile pioneer at cright     

    mt "He had long hair... And lately there was a beard."


    window hide

    show mt normal pioneer at left with dspr

    window show 

    stop music fadeout 5

    mt "He spend a lot of summers here. And he has always been in the spotlight,"

    mt "A bit annoyng, very active person, a ringleader,"

    mt "Encouraged kids to all kinds of stuff. Good and bad."

    mt "He tried himself as a camp leader also, as I heard..."

    mt "He didn't like the job though, ran away. I don't know why."

    mt "I wasn't here in that summer. It was the only year I skipped."
    mt "As for Nikita, it was his last visit..."

    me "It sounds like he was doing it for you."

    show mt normal pioneer at left

    show un normal pioneer at cright 

    play music Vesperia_Part_Three fadein 5        

    mt "Oh, you're fantasist, Semyon. What could you know?"



    me "Not much. But I can put the two together."

    mt "We were just friends and nothing more."

    me "Perhaps he had a feeling for you, but hesitated to reveal himself,"

    me "Being unsure of your feelings."

    

    mt "Listen, young people. It could hard for you to understand,"

    mt "That boy and girl may spend a lot of time together,"

    mt "Without crossing any lines. But, please, believe me - it is normal."

    mt "It is good. And this is how it should be... Usually."

    window hide 

    show mt sad pioneer at left with dspr

    window show         

    mt "If there is no big feelings - nothing happens."

    mt "It is way better than the new trends of your generation,"

    mt "Next thing they're gonna call themselves a couple,"

    mt "Without asking of each other's names!"

    mt "A soul grow stale of such a freedom, as you call it..."

    mt "It is the only soul we have. Mind good, with whom you share it."

    un "Yes, here I agree. The true love must be clear and undoubtful,"

    un "And if there is a doubt - it should be something else..."


    un "The love would vainly be exalted if it would born from every acquaintance."

#

    mt "Alright, my friends... This topic is eternal, and our time is not," 

    mt "So I must leave you now.  Thank you for what you did," 

    mt "It was a pleasant walk through a timeline. I'll see you at the dinner. "

#
     
    window hide
    $ renpy.pause (0.3)
    hide mt 

    with dissolve

    $ renpy.pause (0.7)  

    window show     

    me "Whoa. We've found our leader's vulnerable spot."

    un "Yes, she is sad now... I hope it's just nostalgia,"

    un "But I'm afraid you've brought up something that you shouldn't."

    me "Why shouldn't I? If she cares much about the guy,"

    me "It's on the contrary then. She heard exactly what she must,"

    me "Now she can find him and so on, be happy..."

    me "And, after all, she will praise me, her real personal Cupid."

#

#    me "And by the way. This hairpin... Interesting, there were no other girls in this letter. It must be a grim secret here..."

#


#    un "Shouldn't we mind our own business? "

    

    

#    me "I see one funny possibility. This hairpin belong to that Nikita."

#

#    un "Could it be Olga's gift? Then... why digging it?"

#    me "He didn't like girls at all, don't you see?  It's a good reason for her to be mad."
    me "There could be the another possibilites, of course..."
    me "What if Nikita's not fond of girls at all?" 

    me "She mentioned the long hair... That fancy hairpin could be one of his!"

    window hide

    show un serious pioneer at cright 

    with dspr

    window show     

    un "Please, hold it. Don't invent such things"
    un "About the person you don't know at all."

    un "You'll hate it if you'll hear the same about yourself."
    me "Why should I? Am I giving any reasons?"
 

    un "Are you a guy who never missing chances with the girls?"
#    un "Excuse me, but you're not that person either."

    window hide

    show un shy pioneer at cright 

    with dspr

    window show     

    me "Of course I am! Because I never have them in the first place..."
    window hide
    $ renpy.pause(1)    

#    me "Don't answer. I'm just kidding. But you are right about gossiping. "

    window hide

    show un sad pioneer at cright 

    with dspr

    window show     

#    un "Kidding again, right..."
    me "Perhaps I went too far with jokes. I'm sorry."
    me "Yes, you are right about gossiping and so on..."
    me "But I believe you're not a spreading rumors type,"


    me "So I feel confident discussing things with you. "

    

    un "I'm sorry too. I know that you are delicate and measured..."

    un "You haven't mentioned my evil act, not even once,"

    un "For all the hour or more that we have spent together."

    un "Although, it haunts me still somehow. And even after your kind offer"

    un "To wrap things up once and for all."

    un "I guess it's not a thing to easily forget..."

    me "So we are back again to our little drama?"

    me "Alright, the library... What can I say, it was a strange way to blow off steam."

#    me "Strange, but forgivable. And you're forgiven."

    me "I'd like to know what actually happened then. If you can tell me."

#

    "..."

    me "But it seems you're not... {w}At least has something changed since then?"

    window hide

    show un normal pioneer at cright 

    with dspr

    window show     

    un "No... To be honest, not that much."

    un "But don't you worry about me. I am not dangerous to others." 

    un "At least I think so... I only harm myself with vain words."

    me "Something is bothering you... I'm pretty sure it can be helped,"

    me "You need to share your problem with some trusted person,"

    me "Then you will be already on a half way of solving it."

    un "I truly trust you, but how can I trust myself?"

    un "I make too much mistakes. Could be the worst idea ever if I share it..."

    un "There are some things that shouldn't be discussed at all,"

    un "There are some words that must remain unsaid."

    menu:

        "Leave her alone":

            me "I should be minding my own business. Got it."

            stop music fadeout 5

            me "But you can count on me, if you will change your mind,"

            me "Don't hesitate to drop on me the things weighting too heavy."

#

            window hide

            show un smile pioneer at cright 

            with dspr

            window show             

            "And Lena nodded thankfully."

            un "I do complain too much. It must be bothersome... Excuse me."

            un "And thank you for your care, Semyon. It much appreciated."

            un "Well, duty calls... I will return now to the library. "

#            th "Now it's time to stop following her. To avoid being persistent."

            me "...Right. See you later."



            th "I guess it's time to return home."

            stop ambience fadeout 2


            window hide
            $ renpy.pause (0.3)
            scene bg int_house_of_mt_day with fade
            $ renpy.pause (0.3)
            window show

            "After a brief rest and some last preparations,"

            "I heard a sound invinting me to have a dinner."

#

#



            window hide         

            $ renpy.pause(1)            

            jump ss_d6din_eng

            

        "Try to get her to talk":

            jump ss_d6una_eng





label ss_d6una_eng:

    $ d6_treasure = 2

    stop music fadeout 7    

    me "I've heard and seen a lot of bizarre stuff,"

    me "Meaning aswell your words and actions. No offence."

    me "Yet I am here with you. Still not scared off..."

    me "So if you want to tell me something - tell it. I will understand."

#

    play sound sfx_dinner_horn_processed

    window hide

    show un angry2 pioneer at cright 

    with dspr

    window show

    th "You, stupid horn! It's not the time now, don't you see?"

    th "Leave us alone, we will be late for dinner..."
    window hide
    $ renpy.pause(1)    

    play music music_list["confession_oboe"] fadein 5   
    window show
    un "Is it the reason why you came to me today?"

    un "A curiosity? You want to know my secret?"

    me "It is unfair and very wrong assumption. I just want to help."


    un "What was your reason then?"

    un "...You know, why I am helping at the library?"
    un "Because it's always empty there. I want to be alone... {w}Or I pretend so?"

    un "Today you came to me, you took me to the island..."

    window hide

    show un sad pioneer at cright 

    with dspr

    window show     

    un "It was my island... Our island. Don't you understand?"

    un "The special place. It was like in the dream and everything was perfect," 
    un "Exactly as it meant to be, but then..."
    un "I just woke up - we're in the boat, it's over."

    un "And it was nothing, it was dull... It led nowhere."




    show un cry pioneer at cright 

   

    un "The dream is gone. You left me without it."

    un "It's not enough for you. You also want to know it all,"

    un "It's fun for you to learn my soul, to see it underneath a microscope."

    un "But you don't leave me even ounce of pride..." 

    un "Fine, take it all! I can't refuse to you. It's not an option."

    un "Now tell me... Do you really wanted to know this?"

    stop music fadeout 6    

    me "Wait. Listen..."###This is what I'll tell you now..."



    window hide

    show un sad pioneer at cright

    show mi normal pioneer at left  

    with dspr

    window show     

    mi "What are you whispering about? Must be an awfully big secret?"


    mi "Hey, Lena, have you heard the dinner horn?"

    mi "Don't make me wait or eat alone - it's boring,"

    mi "There is no reason to consume a food without a good company,"

    mi "It only makes you fat and nothing more."

#

    window hide

    show mi dontlike pioneer at left    

    with dspr

    window show     

    mi "Hey, don't you look at me like this! It's not my fault,"

    mi "That both of you are fatter than I am..."

    window hide

    show mi laugh pioneer at left   

    with dspr

    window show     

    mi "...It's not the case, right? Well, never mind then!"

    window hide

    show mi surprise pioneer at left    

    with dspr

    window show     

    mi "Okay, guys! Follow me to the canteen!"

    me "You go alone, please. Give us five minutes."



    show mi normal pioneer at left  



    mi "I won't, we have to hurry up! Come on!"

    mi "They're going to take all chairs from the canteen"

    mi "To put them by the stage. The play starts very soon, no waiting!"

    mi "This time if you won't eat fast enough,"

    mi "They'll take you to the stage right with your chair!"

    un "I see... Let's go then, Miku."

    window hide 

    $ renpy.pause(0.3)    

    hide mi

    hide un

    with dissolve
    $ renpy.pause(0.3)
    window show

    th "How bad. I have to talk to her again, as soon as possible,"

    th "I hope we'll get a chance for it this evening." 

#    th "Let's go to the canteen then. I only have to hope that Lena can control herself."

#

    window hide         

    $ renpy.pause(1)            

    jump ss_d6din_eng

    

    

    

label ss_d6dve_eng:

    $ sp_dv += 1

    $ d6_dv_learned = 1



    scene bg int_house_of_mt_day with dissolve 

    th "Visiting beach is always lots of fun."

    th "Why not to go and try the local one?"

    window hide



    $ persistent.sprite_time = 'day'

    scene bg ext_beach_day 

    with dissolve



    play ambience ambience_lake_shore_day fadein 3



    window show

    

    th "As I expected, both of redheads here - very joyful, not concerned at all..."

    th "Did they forget about the play? How could they be so reckless?"

    window hide

    scene cg d4_us_cancer with dissolve

    $ renpy.pause(1)

    window show

    play music music_list["lightness"] fadein 7

    me "Hey, ladies, do you care about your roles?"

    me "Why don't you learn them? Why are you here, doing nothing?"

    us "I know my role! I have a photographic memory!"
    us "A piece of cake for me - such a small part!"
    us "And have already learned it all by heart."


    dv "Speaking of me - why should I memorise the text at all?"

    dv "I'll have the paper by my eyes, there is no need to learn this scrawl."

    window hide

    scene bg ext_beach_day

    show us normal swim at cright

    show dv smile swim at cleft

    with dissolve

    window show    

    me "You can't just read it there with your eyes down,"

    me "And call it a good acting! It is shameful."

    me "At very least you need to know where is your lines and what are they about,"


    me "So please, stop wasting time, do some preparing!"

    dv "Okay, let's take a look. The text is with me."

    dv "Or almost with me. I put it somewhere here, by the rock..."

    window hide  

    show dv guilty swim at cleft   

    with dspr

    window show 

    me "I don't see any papers on the beach, but there's a lot of papers on the water." 

    me "There is a fat kid on the raft, who's leading by the rope,"

    me "A huge fleet of familiar-looking paperships."

    window hide  

    show dv angry swim at cleft 

    show us angry swim at cright    

    with dspr

    window show     

    dv "Can't to relax just for a second in this camp! Ulyana, we got robbed!"

    us "He stole from us?.. He'll never get away with it!"

    window hide  

    show dv normal swim at cleft 

    hide us     

    with dspr

    window show     

    "Ulyana rushed without hesitation to catch the paper admiral."

    dv "Well, once the text has finally been lost,"
    dv "I'm in my right to take it easy and relax..."

    me "You had enough of relaxation. Now go get another copy."

    dv "Go where? Go to Zhenya?.. I am not welcomed anywhere near her."

    me "It's not a big deal, I could bring it here myself,"

    me "If only I could see a tiny bit of effort from you."

    me "It's clear now that the second copy will be blown away,"
    me "Or end up in the water too."

    dv "It won't, Semyon! At least not in the water this time... Have my promise!"

    me "Well... Anyway, I see no need in sitting on the beach."

    me "Let's go with me, I'll give you my own copy."

    window hide  

    show dv angry swim at cleft     

    with dspr

    window show     

    dv "You are a piece of idiot, Ulyana! Come here right away!"

#

    window hide  

    show us smile swim at cright    

    with dspr

    window show     

    us "I wrecked this guy so good! Next time he will think twice before stealing!"

    dv "What have you wrecked him with, you dummy?"

    window hide  

    show us dontlike swim at cright     

    with dspr

    window show     

    us "Oh, with this bag, of course!"



    dv "This is the bag with our uniform!"

    dv "Look, it's all wet now! How will we go back to the camp?"

 

    show us grin swim at cright     

   

    us "Like this, in swimsuits, I guess! Half-naked, amazonian style!"

 

    show us normal swim at cright 

    show dv guilty swim at cleft    

   

    us "Is there something wrong with it? Well, we can find a detour."

    me "Alright, Alisa, here you're having what you wanted,"

    me "A good excuse for laze instead of reading."

    me "Now you will stay and dry your clothes, I guess."

    us "Wait, guys, I'll dry it up before you blink!"

    dv "It's fine, let's go. I'll take your copy."

    me "What, really?..  I see you have a courage."

    window hide  

    show dv angry swim at cleft     

    with dspr

    window show     

    dv "Don't even think of mocking me! Remember: I am doing you a favor."

    stop ambience fadeout 2

    dv "Do me a favor too: shut up and watch your six... Not mine."

    window hide
    $ renpy.pause (0.5)
    scene bg ext_house_of_mt_day
    with dissolve
    show dv guilty swim 

    with dissolve
    $ renpy.pause (0.5)
    window show

    play ambience ambience_day_countryside_ambience fadein 2

    dv "What an absurdity I am engaged in..."

    dv "Sneaking around the camp in swimsuit."

    me "Why not? What makes the beach so special?"
    me "What is the difference between here and there?"

    stop ambience fadeout 2 

    dv "Sort of no difference, but still... No, nevermind."

    me "I find it quite remarkable, you know," 

    me "You, people, feel so comfortable on the beach,"

    me "Yet awkward in the same clothes while ten meters away."
    dv "So what? Are you implying that you're different?"
    me "Sure. My own shame level's always stable."

    me "For I feel awkward everywhere, in any clothes..."

    window hide
    $ renpy.pause (0.5)
    play ambience ambience_int_cabin_day fadein 2

    scene bg int_house_of_mt_day
    show mystical_box:
        pos (1348,0)      
    with dissolve    
    show dv normal swim

  

    with dissolve

    $ renpy.pause(1)

    window show    

    me "The copies, here they are. Hey, take another for Ulyana."

    me "Or we could read them now together, if you want..."

    dv "Just look at me. Does the rehearsal fits me now?"

    dv "I will commit a suicide right here if Olga comes."

    me "Well, take a blanket, wrap yourself..."

    stop music fadeout 5

    dv "To make the situation even more weird?"
    dv "And in the same time more obvious to Olga?"

    dv "Not gonna happen. I am going home..."

    play sound sfx_knock_door7_polite

    window hide  
    $ renpy.pause (0.3)
    show dv scared swim

    show mystical_box:

        pos (1348,0)    

    with dspr

    window show     

    "Somebody knocked the door politely."

    "Alisa lowered her voice immediately:"

    dv "Stand still, be quiet!"

    dv "Don't open. Nobody's at home."

    window hide  

    hide dv     

    with dspr

    window show     

    "She deftly sneaked to the door,"

    "Trying to see our visitor through the keyhole."

    window hide 

    $ renpy.pause(1)  

    show dv sad swim

    show mystical_box:

        pos (1348,0)    

    with dspr

    window show     

    dv "It is Samantha. She is sitting in the lounge right next to us."

    dv "How would I leave now? It's impossible to sneak past her."

    me "Maybe just go... Say hi to her. Samantha's not a problem."


    play music music_list["take_me_beautifully"] fadein 7   

    dv "You go, Semyon! Take her away, distract her for some time."

    me "Too late for that, it's awkward now... I didn't open when she knocked."

    me "And also I don't want to lie, she's very sensitive to false."

    window hide  

    show dv angry swim

    show mystical_box:

        pos (1348,0)    

    with dspr

    window show     

    dv "What a blockhead! It's fine to get away with such a little lie!.."

    dv "Is there a better plan? To sit and wait till' Olga comes?"

    me "Yes, we can wait. She comes here not that often before evening."


    window hide  

    show dv normal swim

    show mystical_box:

        pos (1348,0)    

    with dspr

    window show     

    dv "And where is she? What are those numerous things"

    dv "She always has to do, as she says?"

    me "Who knows. Perhaps she is just vegging out somewhere,"
    me "Having a nap in a quiet place. That's what I'd do."

    dv "Well, if it so, she better keep on sleeping. I smell the trouble otherwise."

#    dv "Otherwise we are in trouble."

    window hide  

    show dv  grin swim

    show mystical_box:

        pos (1348,0)    

    with dspr

    window show     

    dv "Hold on! I have a wonderful idea!"

    dv "You have to spare for me your uniform!"

    me "I am not greedy, but I don't have the second one..."

    dv "Your one is fine, I won't be picky this time."

    dv "Follow my thought: I need a boy's clothes for the play,"

    dv "Yet I don't have a single piece. So I am borrowing yours."

    me "There is some sense in it since they don't have a costume for you..."

    me "But still... Why me? Why coming here?"

    me "It's neither a storehouse, nor a tailor's shop,"

    me "I see no explanation why to take my only clothes."

    window hide  

    show dv guilty swim

    show mystical_box:

        pos (1348,0)    

    with dspr

    window show     

    dv "No need to dig that deep, don't be a downer."

    dv "Your job is to explain one simple thing to her:"


    dv "Your had to spare your uniform, it's for the play. That's it!"

    dv "If she don't get it - blame the language barrier."

    me "What about me? How should I walk around?.. In underwear?"

    window hide  

    show dv normal swim

    show mystical_box:

        pos (1348,0)    

    with dspr

    window show     

    dv "Don't walk at all. Stay here until Olga comes," 

    dv "I'm pretty sure she'll find something for you."

    dv "Come on, undress now! Hurry up!"

    me "What's going on? You've asked me for the same this morning,"
    me "You're trying to undress me for the second time today!"

    stop music fadeout 3    

    me "This weird fixation... where does it comes from?"

    me "Or maybe it's a bet? I can imagine..."

    window hide  

    show dv angry swim

    show mystical_box:

        pos (1348,0)    

    with dspr

    window show 

    play music music_list["heather"] fadein 1    

    dv "Cut off this nonsense... What a fool you are!"

    window hide  

    show dv angry swim  close   

    with dspr

    window show     

    me "Don't pull my tie! {w}Ouch!" with hpunch

    window hide 

    show dv rage swim tie

    show mystical_box:

        pos (1348,0)    

    with dspr

    window show     

    dv "Take off the rest right now!"

    th "How comes she isn't weirded out with what we're doing?.."

    window hide     

    $ renpy.pause(1)

    show dv shy swim tie

    show mystical_box:

        pos (1348,0)    

    with dspr

    window show 

    th "At last... I see she understands now."

    window hide

    show greysation with dissolve

    show lildevil with dissolve:

        pos (200,200)

    

    $ renpy.pause(1) 

    window show 

    dev "You're not a champion in love affairs, Semyon,"

    dev "But still somehow you pulled it off this time!"

    dev "The prey is going to your hands, so use them now,"

    dev "You can't afford to miss this chance. She'll never be so close again!"

    th "But did she come here for this?.."

    dev "Who cares! She doesn't know it for sure either,"

    dev "But now she wants it, yes! Subconsciously!"

    dev "Enough of acting children, time to be a man!"

    th "...Can I have angel's advice now? For a change?"

    dev "There is no angel, dude. I am alone."

    th "What do you mean? There is no other guy at all?"



    dev "Just not for you. You are that sort of \"special\" person,"

    dev "That capable of spoiling all the fun just by himself,"

    dev "And miss all chances without any divine help."

    th "What fun? What chances? That's ridiculous!"

    th "It's Olga's house! She could come at any moment,"
    th "Not speaking of Samantha right behind this wall!"

    dev "It's not my business... I'm here only to discuss the moral side,"

    dev "Certainly not to solve your housing problems..."

    dev "Good luck, comrade! Nature will tell you what to do!"

    window hide

    hide greysation

    hide lildevil

    with dissolve

    $ renpy.pause(1) 

    window show

    th "...Fine, fine. I'll do precisely what Alisa's asking for."

    th "Which means undressing... It's my turn now to feel awkward."

#

    me "Okay then, you will get my uniform."

    stop music fadeout 5        

    me "Make fun of it or not, but I will hide under the blanket!" 

    dv "Use what you wish: a blanket, curtain, or a tablecloth... I do not care."

    dv "No matter what you think, I'm here not to watch you stripping."

    window hide
    $ renpy.pause (0.3)
    scene bg int_house_of_mt_day

    show dv shy swim tie

    show mystical_box:

        pos (1348,0)    

    with fade
    $ renpy.pause (0.3)
    window show 

    "As soon as I removed all of my clothes except the underwear,"

    play music music_list["pile"] fadein 3  

    "I heard the sounds from the street that made me tremble:" 

    window hide 

    show dv shocked swim tie

    show mystical_box:

        pos (1348,0)    

    with dspr

    window show 



    "It was our camp leader's voice without a doubt."

#

    "Alisa stood still too. She had a chance to take my clothes on,"

    "But didn't do it, paralysed in the same terror."

    "I reasoned that it couldn't save us anyway -"

    "No matter what, the situation will be clear for Olga."

    window hide 

    show dv shy swim tie

    show mystical_box:

        pos (1348,0)    

    with dspr

    window show     



    "I have created a good picture in my mind,"

    "What's going to happen in the next few minutes."

    "Olga will gasp or scream, just seeing us with the senorita,"

    "The noise will lead Samantha here too,"

    "And then it will be something! A scandal to remember,"

    "Biggest calamity since the fire in the old camp!"

    stop music fadeout 5    

    ss "{enen}Come with me please.{/enen}"

    mt "Yes? Can I help you?"

    window hide

    show dv surprise swim tie

    show mystical_box:

        pos (1348,0)

    with dspr

    

    window show    

    dv "Is this what I'm thinking of?.. Move, let me check..."

    window hide 

    hide dv

    with dspr    

    $ renpy.pause(1.2)

    stop ambience fadeout 2     
    window show 
    dv "Yes, we are saved! Now I am out of here!"

    window hide

    scene bg ext_house_of_mt_day

    play ambience ambience_day_countryside_ambience fadein 2    

    show dv shy swim tie 

    with dissolve

    window show 

    me "The road is clear. What a miracle."

    

    dv "I'm gonna use this miracle right now! Goodbye!"

    window hide 

    hide dv

    with dspr

    window show     

    "Alisa ran way, still holding all my clothes in hands."

    "I had no choice but to return back to the house,"
    "And try to find something to wear instead of blanket."

    play music music_list["lightness"] fadein 5 

    window hide

    scene bg int_house_of_mt_day 

    show mystical_box:

        pos (1348,0)    

    

    with dissolve

    window show

    play ambience ambience_int_cabin_day fadeout 2

    "A fine t-shirt was found in the wardrobe pretty soon,"

    "But my new outfit was still incomplete without pants,"

    "Because I've had no luck to find them."

    window hide

    play sound sfx_open_door_2
    $ renpy.pause (0.4)
    show mt normal panama pioneer at cleft with dspr
    $ renpy.pause (0.3)
    window show 

    mt "Hey, neighbour, have you seen my hairbrush?.."

    mt "Hold on. Why are you wearing my old blouse?"

    mt "It fits you pretty well... And not too tight under the arms?"

    me "I try to find a costume for Romeo's role."

#

    mt "You need the tights and jacket. Get a beret also."

    me "Damn... Where would I get this kind of stuff?"

    me "I understand you, ladies, now..."

    mt "What do you mean?"



    me "I stand before the wardrobe full of clothes,"

    me "Yet there is not a single piece that I could wear!"


    mt "Maybe our handicraft club will help us. Let me ask them."

    me "Yes, please... And one more problem here:" 

    me "My uniform has end up in the laundry before time..." 

    me "Please, could you give me spare one?"

    mt "Why all this courtesy?.. You could have take it without asking."

    me "You bet I would, but I do not know where..."

    mt "Haven't you tried to watch under the bed?"

    mt "It's in the box there in the corner. Any size."

#

    window hide 
    $ renpy.pause (0.3)
    hide mt

    with dspr
    $ renpy.pause (0.3)
    window show     

    "Once Olga left, I found a shirt and shorts for me,"

    "And sit back on the bed."

    play sound sfx_paper_bag

    extend " I heard a cracking paper under me -"

    "It was the text that I have given to Alisa. She forgot it."

    stop music fadeout 5

    "So I decided to complete the good deed that I have started,"

    stop ambience fadeout 2

    "To bring these damned papers to Alisa yet again."

    window hide

    scene bg ext_house_of_mt_day

    play ambience ambience_day_countryside_ambience fadein 2    

    show ss nosmile casual

    with dissolve

    $ renpy.pause(1)    

    window show

    play music Hide_and_seek fadein 3   

    me "{enen}You really like our lounger, don't you?{/enen}"

    ss "{enen}I wish my house had one too...{/enen}"



    show ss unsure casual   

    ss "{enen}My presence must be bothering for you? I'm sorry.{/enen}"

    me "{enen}Of course it's not. No, not at all. Don't worry.{/enen}"

    window hide 

    show ss shy casual 

    with dspr

    window show     

    ss "{enen}I actually saw you two. Not sure if I should say it...{/enen}"
    ss "{enen}But I already did. Haha.{/enen}"

    me "{enen}You mean you have distracted her on purpose?{/enen}"
    me "{enen}You took Olga away to let Alisa quit?{/enen}"

    ss "{enen}Don't thank me, it was me who trapped you,{/enen}"

    ss "{enen}But in the end I meant no harm, and did what ought to do.{/enen}"

    show ss unsure casual   

    th "We didn't let her in. It was unpleasant, I imagine..."

    th "Not mentioning what she might think of us."

    me "{enen}I hope you got it right that there was nothing embarrasing.{/enen}"

    me "{enen}But how it looks may bring a big misunderstanding.{/enen}"



    show ss normal casual

     

    ss "{enen}Forget it. So where are you going now?{/enen}"

    me "{enen}I have to bring these to Alisa anyhow....{/enen}"

    me "{enen}Care to join me on this wondrous adventure?{/enen}"

    show ss smile2 casual   with dspr

    ss "{enen}If I am not disturbing you... Then yes, with pleasure.{/enen}"

    window hide
    $ renpy.pause (0.3)
    $ persistent.sprite_time = 'day'

    scene bg ext_house_of_dv_day
    with dissolve
    show ss nosmile casual at cright    

    with dissolve
    $ renpy.pause (0.3)
    play sound sfx_knock_door2

    $ renpy.pause(2)

    window show    

    ss "{enen}Nobody is at home or she's not opening again.{/enen}"

    me "{enen}It looks like our adventure goes in vain...{/enen}"

    ss "{enen}To make adventure worthy, you must say now something clever.{/enen}"

    me "{enen}I will, I will... Well... I will say... {w}Nice weather?{/enen}"

    window hide

    stop music fadeout 5

    show dv guilty shorts at cleft

    show ss smile2 casual at cright  

    with dspr   

    window show     

    dv "Okay, I'm here. You may start to scold me."

    me "Scold you for what?.. We've brought you something."

    dv "Oh, right, the papers... Stupid me."

    window hide

    show dv normal shorts at cleft with dspr

    window show 

    stop ambience fadeout 2 

    dv "...Will you get in, as soon as you are here anyway?"

    window hide
    $ renpy.pause (0.3)
    scene bg int_house_of_dv_day
    with dissolve
    play ambience ambience_int_cabin_day fadein 2

    show dv normal shorts  at cleft

    show ss normal casual at cright    

    with dissolve

    $ renpy.pause(1) 

    window show     

    ss "{enen}I like this room guys. But where is Ulyana?{/enen}"

    dv "Ulyana? She's outside. Making a mess somewhere..."

    ss "{enen}I'm sorry, what's this?{/enen}"

    dv "It's just a postcard. But I like this one."

    window hide

    play music music_list["doomed_to_be_defeated"] fadein 0 

    show redfeb with dissolve:

        pos (150,450)

    



    $ renpy.pause(1.5)

    window show 
    th "\"Happy Defender of the Fatherland Day\"..."
    me "I've never thought that you are such an overzealous communist."

    window hide

    hide redfeb
    show ss serious casual at cright 

    with dspr

    $ renpy.pause(0.5)  

    window show 

    dv "What do you mean?.. Show it to me!"

    window hide

    show dv angry shorts at cleft with dspr

    window show 

    dv "...Hey, it's another one! Not what I meant!"
    stop music fadeout 9
    dv "It's not mine, it's Ulyana's! What an imp!.."

    show ss unsure casual at cright with dspr   

    me "It doesn't matter now if it is her or yours..."

    dv "You screwed me bad by coming here without warning,"
    dv "I could had clean it up here if I knew you come!"

    me "Nobody forced you to invite us."


    dv "I was polite! I wouldn't do so if you come alone!"

    window hide

    show ss sad casual at cright

    show dv guilty shorts  at cleft 

    with dspr

    window show 

    ss "{enen}All right, guys. I feel this is a wrong time for a visit,{/enen}"

    ss "{enen}I have to leave now, not to spoil the spirit.{/enen}"

    show ss unsure casual at cright 

    ss "{enen}I know you need to practice so go on,{/enen}"

    ss "{enen}You better hurry up before the time is gone.{/enen}"

    window hide
    $ renpy.pause (0.3)
    hide ss with dspr
    $ renpy.pause (0.3)
    window show

    dv "Where is she going? Is it because of damned postcard?"

    me "Don't panic. She's not mad at all."

    dv "But still she's left us. And she wasn't very happy!"

    me "She left to let us read the lines... Because you need it."

    me "She can not help us since she doesn't read in Russian,"

    me "Without it her presence would be only a distraction."

#

    dv "So many reasons just for one unreasonable thing!"

    window hide 

    show dv normal shorts at cleft with dspr

    window show 

    dv "What makes you think that I would read with you?"



    menu:
        "It's time to..."
        "Stay":

            jump ss_d6dv2_eng

        "Leave":

            th "The hint is clear. Now I must leave,"

            th "Since I can see no sympathy for me here."

            me "Fine, I don't want to be an uninvited guest."

            me "Excuse me. I will try to catch Samantha."

            stop ambience fadeout 2         

            dv "You're leaving?.. Sure, go get her... You're a team."

            window hide

            scene bg ext_house_of_mt_day with dissolve

            play ambience ambience_day_countryside_ambience fadein 2
            window show
            "Samantha's trail went gone,"

            "I walked all the way back to her house,"

            "And knocked in. She wasn't there too."

            stop ambience fadeout 2

            th "She must have turned somewhere... Whatever."

            th "It is impossible to get lost here in the camp."

            th "And if she needs me she can find me any moment."

            window hide         

            scene bg int_house_of_mt_day

            show mystical_box:

                pos (1348,0)            

            with dissolve

            play ambience ambience_int_cabin_day fadein 2           



            $ renpy.pause(1)

            window show            

            th "While I have nothing else to do, it will be wise,"

            th "To read Shakespeare once more, renew the memory."
            window hide
            menu:

                "Read my lines":

                    jump ss_d6urd_eng    

                "Read other's lines":

                    jump ss_d6esc_eng

        

            

            





label ss_d6dv2_eng: 

    $ d6_dv_learned = 2

    dv "Well, if you're here anyway,  let's get it done."

    "Alisa looked at her lines like if she saw them for the first time,"

    "She flipped through the papers in confusion."

    me "Look for the scene five in the first act."

    dv "I know! I've already found it."

    dv "{i}This, by his voice, should be a Montague.{/i}"

    dv "{i}Fetch me my rapier, boy:  what dares the slave{/i}"

    dv "{i}Come hither, cover'd with an antic face,{/i}"

    dv "{i}To fleer and scorn at our solemnity?{/i}"

    me "{i}Why, how now, kinsman! Wherefore storm you so?{/i}"

    dv "{i}Uncle, this a Montague, our foe!"

    me "{i}Young Romeo, is it?{/i}"

    dv "{i}'Tis he, that villain, Romeo.{/i}"

    me "{i}Content thee, gentle coz, let him alone, he bares him like a porty gentleman.{/i}"

    me "{i} Show a fair presence and put off these frowns, an ill-beseeming semblance for a feast. {/i}"

    dv "{i}It fits, when such a villain is a guest: I'll not endure him...{/i}"

    play sound sfx_open_door_clubs

    window hide

    show us normal sport at cright with dspr

    window show

    play music music_list["eat_some_trouble"] fadein 2  

    us "Who is that guest?.. Oh, hi, Semyon!"

    us "Listen, Alisa, I have dried your uniform,"
    us "But burned through the collar, just a little."

    window hide

    show dv angry shorts at cleft with dspr

    window show

    us "Hey, don't get angry, please... It's not my fault,"
    us "That both our shirts are slightly burnt,"
    us "But it is not so bad as you imagine."
    dv "Where is my uniform? Show it to me!"

    us "It's gone, of course. I've got to bury it behind the boiler..."

    us "You know the drill. Can't leave the clues to Olga,"

    us "Or else she would punish us for carelessness."

    us "Okay then. We could chit-chat forever..."

    stop music fadeout 7    

    us "Give me a fishing rod. I have some things to do."

    window hide

    show us grin sport at cright with dspr

    window show   

    us "I bet you think I'm going fishing now... Wrong guess!"

    us "I'm gonna catch boy's underwear! Right from their..."



    window hide

    hide us with dspr

    $ renpy.pause (0.6)

    show dv sad shorts at cleft 

    with dspr

    window show 

    dv "It was a terrible idea to leave my clothes with her..."
    me "What's done is done."

    dv "...Yes. Never mind. Let's keep on reading."

    

    dv "{i}Romeo, the love I bear thee can afford {/i}"

    dv "{i}No better term than this, thou art a villain.{/i}"

    play music music_list["take_me_beautifully"] fadein 7   

    me "{i}Tybalt, the reason that I have to love thee{/i}"

    me "{i}Doth much excuse the appertaining rage{/i}"

    me "{i}To such a greeting. Villain am I none;{/i}"

    me "{i}Therefore farewell; I see thou know'st me not.{/i}"

    window hide

    show dv shy shorts at cleft with dspr

    window show 

    dv "{i}Boy, this shall not excuse the injuries {/i}"

    dv "{i}That thou hast done me; {/i}"

    window hide

    $ renpy.pause(1)    

    show greysation

    show lildevil:

        pos (1100,200)

    with dissolve

    window show    

    dev "Therefore let's heal them with some action!"

    dev "It's me again. With the same topic, yes."

    dev "Last time you mentioned, that it was too dangerous?"

    dev "No such a problem now: you're in a perfect shelter!"

    th "I still can't risk it... She may kick me out,"

    th "It will be terrible. And even if she wouldn't..."

    th "She will think bad of me right from this moment,"

    th "And it will ruin our good relations."

    dev "What do you want then, fool? A true love, full of romance?" 

    dev "You think she needs a modest prince, not an upstart?"

    dev "Remember, you can't harm the love with any courage!"

    dev "There is a chance to be rejected, yes,"

    dev "But cowardness will never give you better chances,"

    dev "More likely, it will spoil them all."

    dev "Be brave! You are afraid to destroy something,"
    dev "That you have build yourself in your imagination,"

    dev "But in reality that thing doesn't exist at all."

    dev "People regret much more about missed opportunities,"
    dev "And less about the things they tried. You know that."

    dev "So, be a man, big guy... {w}That's my advice. "

    window hide 
    $ renpy.pause (0.5)
    hide greysation

    hide lildevil

    with dissolve

    $ renpy.pause(1)    

    window show       

    dv "...So what?"

    me "Uhm...{w} {i}I do protest I never injur'd thee.{/i}"
    window hide
    play sound sfx_dinner_horn_processed    
    $ renpy.pause(1)    

    window show 
    "The horn has sounded in that moment, calling us to the canteen."

    window hide 

    show greysation

    show lildevil

    with dspr

    window show     

    dev "Nothing again?.. Come on! You're serious?.."

    dev "I'm gonna die of boredom here with you."

    window hide 

    hide greysation

    hide lildevil

    with dspr

    $ renpy.pause(1)    

    window show 

    me "{i}And so Capulet, which name I tender{/i}"

    me "{i}As dearly as mine own, - be satisfied.{/i}"

    window hide

    show dv normal shorts  at cleft with dspr

    window show 

    dv "And then we fight, we kill each other and so on..."
    stop music fadeout 5
    dv "I get it. Well, enough of this. Let's go to eat."

    me "Right... We did good and we deserve a good dinner."

#



    window hide         

    $ renpy.pause(1)            

    jump ss_d6din_eng

  





  

label ss_d6din_eng:

    play music Del_mare fadein 8

    $ day_time()

    $ persistent.sprite_time = "day"

    scene bg ext_dining_hall_near_day

    show sl normal pioneer 

    show dv guilty shorts at fleft

    with fade 
    $ renpy.pause (0.5)
    window show 

    play ambience ambience_day_countryside_ambience fadein 2

    sl "Alisa wearing shorts. Time for another goofy prank?.."

    dv "Time for you keep your tails away. Don't spoil my dinner."

#play music Hide_and_seek fadein 3   

    dv "Why would you come and whine, and grumble..."

    dv "Every time you see me? What's your problem?"

    dv "I'm telling you: just keep away of me,"

    dv "And be a good girl somewhere else, friend."

    show sl serious pioneer

    sl "You're not a friend to me, and I'm not trying to be good,"

    sl "At least not here, not for you. What for?.."

    sl "Your words are not forgotten, we're not even,"
    sl "Nothing you did so far to be forgiven,"


    sl "So you don't like me? Well, it's just a start."

    show dv angry shorts at fleft    

    dv "You threaten me by what? Your words mean nothing." 

    dv "And nothing you will do, except of barking," 

    dv "I'm not afraid of you, you only grind your teeth."

#

    me "Please, take it easy, girls! Do not provoke each other,"

    me "Let's try to end this evening peacefully."

    show dv normal shorts  at fleft 

    dv "Don't worry about me. I am myself a bastion of order."

    dv "And Slavya knows that war with me is not an evening stroll."

    dv "No matter what she says - she will behave,"

    dv "Right as she always do."

    sl "Don't you rely on that! I'll have my satisfaction!"

#

    show dv angry shorts at fleft 

    dv "Wow, she is truly mad! You gotta watch her!"

    me "Now you don't look so calm. What is it?"

    me "Am I mistaken or... you're asking for protection?"

    dv "You're on her side, huh? Fine, suit yourself."

    window hide 

    stop ambience fadeout 2

    $ renpy.pause(0.5)  

    scene bg int_dining_hall_people_day
    with dissolve      
 
    show mt normal pioneer at fleft

    show ss nosmile casual at fright

    show sl normal pioneer

    play ambience ambience_dining_hall_full fadein 2

    with dissolve   
    $ renpy.pause(0.5) 
    window show 

    "There is unusual situation in the dinner:" 

    "The pioneers eat the fried pork with spoons,"

    "And I can't see a single fork around."

    "Must be a robbery or other trouble at the kitchen."

    "Not a big deal. Even Samantha doesn't seem surprised,"

    "She is already used to such cases."

    sl "Yes, spoons are fine! We're not a royalty."

    me "What do we celebrate today? The prison's day?"
    me "No forks is their good tradition, clearly."
    me "Congratulations then. Long live the thiefs!"

    mt "Stop it, you're fantasist! It's just a Shakespeare's day."

    mt "The theatre is only reason for this turmoil."

    me "Speaking of theatre... I've heard some speculation,"

    me "That your beloved Shakespeare was a fake,"

    me "Not genius at all, but a successful showcase,"

    me "And all his plays was written by anonymous."


    show ss surprise casual at fright    

    ss "{enen}Why do you think that he was fake?{/enen}"

    me "{enen}He couldn't spell his name without mistake.{/enen}"

    show ss normal casual at fright     

    me "{enen}Such man can't be that good with words,{/enen}"

    me "{enen}Leave that to noble kings and lords.{/enen}"

    show ss smile2 casual at fright with dspr

    mt "I see no problem here. \"Shakespeare\" is just a word for us,"

    mt "Which means the author of those plays. And nothing more."

    mt "Think about it as about alias... If you're so sure about this story."

    mt "I personally doubt it."



    show sl smile pioneer   

    sl "If there are mistakes really... Could he make them just for fun?"

    sl "A sort of inside jokes among his circle?.."

    me "If it is so, then this prank went out of control."

    me "It was a fuel to so many funny theories..."

    stop music fadeout 4

    me "So much conspiracy just based on that,"

    me "Who knows where it will lead us in the end?"
    me "It's hard to prove a thing about such distant events."



    if d6_treasure > 0: 

        mt "Hey, talking about distant events..."

        mt "I want to show you my old photo."
        mt "Semyon, why don't you tell us how you've found it?"

        me "Lena and I have found it in a treasure jar..."

#        me "Which you have buried on the closest island..."


    

    mt "Excuse me now! I have another thing to do,"

    mt "As you remember, Slavya and Semyon,"
    mt "I promised that I'll find you costumes..."

    mt "It's done, they're waiting. I will bring them."

    show sl angry pioneer       

    "Showing a real annoyance on her face,"
    "Slavya have twisted herself by the tail."

    sl "Don't bother... I can do it."

    mt "No, please, Slavyana, don't get up,"

    mt "I don't want to distract the actors,"

    mt "Your job now is to eat fast and get ready for the play!"

    show sl sad pioneer

    show ss nosmile casual at fright 

    mt "Bon appetit, my friends."

#

    window hide

    hide mt with dspr

    $ renpy.pause(1)    

    window show

    play music RS1 fadein 4    

    me "We have to talk. What are you up to, Slavya?"
    me "Could you have mercy on Alisa? She's no threat to you."

    me "Could you at least delay your vengeance?"

    if d6_slchoice == 1:

        show sl surprise pioneer    

        sl "What is it?.. You were ready to assist me!"

        sl "Why don't you want to keep your promise?"

        me "I thought we're going to make a joke, not start a total war. "

    else:

        sl "There was a hope that you would help me."

        sl "Think of it as a way to beat the boredom."

        me "I'm ready to assist if you are talking of some harmless joke,"

    

    me "To bring a mice in the canteen or put some paint into the washers..."

    me "We could just put a sticker on a someone's back, you know?"

    me "The same old \"hit me\" thing..."

    me "But I don't want an open fight at all."

    if d6_slchoice == 1:    

        me "I didn't promised you to help in such a mess."

    else:

        me "I can't approve such an ambition."

    show sl serious pioneer 

    sl "Then you are free to go. What's point of having such a helper?"###I don't need such a helper. "

#

    show ss unsure casual at fright     

    sl "I don't have time for useless jokes, even the good ones,"

    sl "No, not until I teach that wrench a lesson."

    window hide

    $ renpy.pause(1)    

    show sl normal pioneer with dspr

    window show 

    sl "You have this funny look, same as Alisa before that,"

    sl "Oh, I can read it on your worried faces:"

    sl "\"What does this Slavya want to do?\""

    sl "\"Is it a bluff or she is serious?\""

    sl "Don't be afraid about the play, Semyon."

    sl "I don't have any plans to spoil it."

    me "Wow, thank you very much! You have no plans to spoil it, really?"

    me "We have so many troubles even without you! Have some shame!"
    me "Nothing to spoil there as this play is barely alive, as always."

    me "So if you wish to spoil there something,"

    me "You gotta help us to create this thing at first."

    show sl serious pioneer     

#    me "We has had no rehearsal and our last briefing is spent on your pique."

#    me "Calm down. Otherwise we will be ashamed. "

    show ss surprise casual at fright  

    ss "{enen}What are you arguing about? Go on, translate!{/enen}"

    ss "{enen}I want to know what's going on, just say it straight.{/enen}"

    show ss serious casual at fright with dspr

    menu:

        "To give up Slavya":

            $ sp_ss += 1 

            $ sp_sl -= 2 

            me "{enen}Our Slavya is just driven by ugly malice,{/enen}"

            me "{enen}Whatever cost it takes, she wants to punish Alice.{/enen}"

            show ss surprise casual at fright            

            ss "{enen}I can't believe it! What the matter?{/enen}"

            ss "{enen}You're so nice girls, I saw no better.{/enen}"

            show ss unsure casual at fright

            th "I've let Slavyana burn of shame here,"

            th "But it was for  the common good. I've got to break her sick intentions."

#

            me "Samantha wants you to back down,"

            me "You need to promise her not to do harm to Alisa."

#

            show sl angry pioneer           

            sl "You gave me up here just to ease your fears?"

            sl "You are a traitor. And a lousy friend."

            show ss sad casual at fright            

            sl "Even Alisa wouldn't snitch on me..."

            sl "You think I am some sort of maniac, or what?"

            sl "Of course I didn't want to really harm her,"

            sl "So you defended your red fox from nothing,"
            sl "Oh, what a hero you! Be satisfied with it!"


            me "What need to be so angry about this?"

            me "It is Samantha you are talking of. She is our friend,"

            me "I'm sure that we can trust her such a problem."

            me "I'm sorry, but you need to settle down."

            sl "Enjoy your dinner, mine is finished,"

            sl "I'm leaving you... my friends."

            window hide

            hide sl with dspr

            $ renpy.pause(1)    

            window show             

            "What a sideshow act... We also have to go now."

            stop music fadeout 4            

            "Until it's not too crowded there by junior pioneers."

            window hide         

            $ renpy.pause(1.0)          



        

        "To cover Slavya up":

            me "{enen}There's nothing to concern about, relax.{/enen}"

            me "{enen}It's about things that play still lacks.{/enen}"

            show ss unsure casual at fright         

            me "Maybe I had to tell Samantha on you. Let her fix your head."

            me "Since I can't manage it myself."


            show sl normal pioneer              

            sl "Don't worry her with such a nonsense,"

            sl "Besides, she looks troubled enough already."

            sl "You better speak to her about herself."

            sl "And try to understand what saddens her."

            me "Yes, something bothers her. But I don't want to ask her now." 

            me "I think she needs to be distacted. It's a better tactic,"
            me "Considering the play that is ahead of us."

            sl "You can be right... But still, what worries her?"

            sl "Is it the play or something else? Don't you know?"

            me "You really want us to discuss it, right in front of her?"

            me "I didn't give you up, as you remember,"
            me "You can expect the same behavoir here."


            sl "I didn't want to interfere in your, guys... "

            window hide

            $ renpy.pause(0.5)

            show sl sad pioneer

            window show 

            stop music fadeout 4            

            sl "...in her business. Sorry."

            sl "I'm full of food already... And your plates are empty too,"

            sl "Let's round it off, I guess? It's time to go."

            window hide             

            $ renpy.pause(1.0)

       
jump ss_d6play_eng
            


#-------------------------------------------------------------------------------
#http://translatedby.com/you/samanta-mod-den-6-otryvok-1/into-en/trans/
#Оригинал (русский): Саманта-мод. День 6, отрывок 1. (http://samanthamod.ru/)
#Перевод: © Modmaker9000, Kompas, Exiled, KriDan, MajorGopnik, Bust, stalker-ru, miruemu.







label ss_d6play_eng:

    play ambience ambience_camp_center_day    

    scene bg ext_dining_hall_away_day with dissolve

    $ renpy.pause(1)

    show mt normal pioneer far with dspr

    show mt normal pioneer with dspr

    mt "Hey, sorry for the wait. Here are your costumes."

    mt "And they will cost you almost nothing..."

    window hide

    $ renpy.pause(0.5)

    show mt smile pioneer with dspr

    window show     

    mt "Just kidding! Go on, take them for free,"

    mt "And hurry to your cabins. There's no locker room around."

    window hide

    $ renpy.pause(0.5)

    play ambience ambience_int_cabin_day

    scene bg int_house_of_mt_day with dissolve
    $ renpy.pause(0.5)
    window show    
    th "Let's take a look..."
    window hide
    $ renpy.pause(0.5)
    show cg ro_m with dissolve
    $ renpy.pause(0.5)
    window show
    th "Whoa! This outfit isn't bad at all, is it?"

    th "The beret suits me pretty well!"

    th "It was left here by some labor guy, I guess,"

    th "But now it has a better purpose. Let it serve the scene."
    window hide
    $ renpy.pause(0.5) 
    play ambience ambience_day_countryside_ambience fadein 1    
    hide cg            
    scene bg ext_house_of_mt_day
    with dissolve
    $ renpy.pause (1)    
    show ss smile2 dress
    with dissolve
    if persistent.dress == 'purple':
        $ persistent.ssg_94 = True 
    else:   
        $ persistent.ssg_95 = True      
    $ renpy.pause(0.5)        
    window show
    ss "{enen}Wow, you in this... It looks so funny!{/enen}"

    ss "{enen}You look like some old Disney's bunny.{/enen}"

    ss "{enen}Alice in Wonderland, a sweet cartoon.{/enen}"

    me "{enen}Then hop along. We're getting started soon.{/enen}"

    window hide

    $ renpy.pause(0.5)  

    hide ss with dspr

    $ renpy.pause(1) 

    window show

    play music In_the_void fadein 5 

    "I made a few steps, but my Juliet didn't follow."

    "She was just standing there, on the same spot."

    th "Does she feel shy?.. Is she afraid of something?"

    th "I should encourage her somehow."

    window hide

    show ss shy dress with dspr

    $ renpy.pause(0.9)  

    menu:

        "You look great!":

            $ sp_ss += 1 

            me "{enen}If I'm a bunny then you are a Disney's princess,{/enen}"

            me "{enen}I didn't mention that before - I ask for your forgiveness!{/enen}"

            me "{enen}Be sure that you look great in this,{/enen}"

            me "{enen}And don't you ever doubt it, miss.{/enen}"

            show ss shy2 dress with dspr            

            ss "{enen}Right now I don't feel comfy, to be honest,{/enen}"

            ss "{enen}Because as a dress-wearer I'm a novice.{/enen}"

            show ss shy dress with dspr            

            ss "{enen}But thank you for your words. I'll try my best,{/enen}"

            ss "{enen}At least I'm not alone who's fancy-dressed.{/enen}"

            me "{enen}Cheer up. It's not a test. And should be fun.{/enen}"

            me "{enen}What else is bothering you? Any problem? None?{/enen}"

            

        "There's nothing to be afraid of":

            me "{enen}There is no need to be afraid at all.{/enen}"

            me "{enen}You may forget your line and not recall,{/enen}"

            me "{enen}Don't even bother, just move on,{/enen}"

            th "{enen}Or else the play will last till dawn...{/enen}"

            show ss serious dress with dspr          

            ss "{enen}What makes you think that I'm not ready?{/enen}"

            ss "{enen}I know the text as good as my own name,{/enen}"

            show ss unsure dress with dspr          

            ss "{enen}It's just my heart's a bit unsteady...{/enen}"

            ss "{enen}I'm trembling, and incertitude's to blame.{/enen}"

            show ss shy dress          

            me "{enen}For guessing incorrectly I'm notorious -{/enen}"

            me "{enen}I need the services of Captain Obvious.{/enen}"

            

    ss "{enen}There's nothing to discuss. No more details.{/enen}"

    window hide

    $ renpy.pause(1.0)

    show ss shy2 dress with dspr

    window show

    ss "{enen}Just swear that you will kill me if I fail.{/enen}"

    ss "{enen}Yes, strangle me in there to end the shame!{/enen}"

    me "{enen}Not funny, Sammy. I don't like your game.{/enen}"

    show ss grin_smile dress with dspr    

    ss "{enen}Don't you believe in me? Just say, come on!{/enen}"

    ss "{enen}Our destiny is long ago foregone.{/enen}"

    me "{enen}We're late! There is no time to spare.{/enen}"

    show ss grin dress     

    stop music fadeout 3    

    ss "{enen}Relax, I'm going... Once you swear!{/enen}"

    show ss scared dress with dspr  

    me "{enen}I SWEAR I'll KILL YOU IF YOU FAIL. {w}Okay?{/enen}" with vpunch

    show ss surprise dress  

    ss "{enen}No need to yell like that. It's just a play.{/enen}"

    me "{enen}Don't play with death, that's what I ask of you.{/enen}"

    show ss smile2 dress with dspr   

    ss "{enen}To play with death is what we're gonna do,{/enen}"

    ss "{enen}Romeo and Juliet, remember?{/enen}"

    me "{enen}Alright, just go! Now I surrender.{/enen}"

    window hide

    $ renpy.pause(0.5)  

    hide ss with dspr

    stop music fadeout 3    

    $ renpy.pause(1.0)  

    show greysation

    show sspi normal at center 

    stop ambience fadeout 5 

    with dissolve   

    pi "Hold on, comrade. Don't go so fast."

    pi "You have just proved yourself a villain!.."

    pi "You know me as a very evil guy,"
    pi "It's fair enough, I'm far from being kind and gentle,"

    pi "Still, you surprised me with your oath! Yes, even me!"

    me "It was a joke. Go get some sense of humor."

    play music music_list["drown"] fadein 4     

    show sspi smile at center    

    pi "No, it's an oath. It can't be canceled or reviewed,"

    pi "You'd better be more careful with your words." 
    pi "It will be a good lesson for you."

#

    show sspi normal at center    

    pi "I know, you used to wear a mask, to feel protected,"

    pi "And say whatever comes to your sick mind,"

    pi "Such an approach shall lead to trouble in some places."

    pi "Like here... We have some rules, if you don't know."

    me "Hold on! Who gave you any right to eavesdrop?"

    pi "It's not about me. I didn't make you say those words."

    pi "So if you're looking for someone to blame,"

    pi "I'll tell you how to find him: get a mirror!"

    window hide

    $ renpy.pause(1)

    show sspi smile at center 

    pi "Well, are you ready to end Juliet's life?"

    pi "The audience will only make it more exciting,"

    pi "But they will not appreciate your impulse..."

    pi "Let's think how you should strangle her in public."

    pi "I know: you'd better wait for the right moment,"

    pi "Pretend it's just an act, part of the play,"

    pi "So they won't realize until it's finished,"

    pi "And the most stupid kids will even clap!"

    pi "I see you do not like my artful plan?"

    pi "I understand. But look at it this way,"

    pi "It will be fun as hell, I guarantee!"

    "..."

    pi "There are no better options anyway."

    $ renpy.pause(0.5)  

    window show

    me "What if. I. Will not. Keep my vow?"

    me "You're telling me to wait. So I decide myself"

    me "When to fulfill my oath. Well, am I right?"

    me "My hands won't do it by themselves, will they?" 

    pi "That's right, you have your own free will..."

    show sspi normal at center    

    pi "But you cannot ignore your oath. You get it?"

#    pi "Although your will is free, just like before,"

#    pi "Alas, you're bound by the oath you gave!"

    me "What if I break it? No more metaphysics!"

    pi "You can't just break it! Do you have no shame?"
    pi "It is an Oath!.. You can't... You won't... It's wrong..."

    stop music fadeout 1

#    pi "You'll be in trouble... You'll become a liar!"
#    pi "Yes, sir. A liar. I'll think worse of you."

    me "...Get the hell out of here!"

    window hide

    hide sspi with dspr

    play ambience ambience_int_cabin_day fadein 2   

    $ renpy.pause(1)    

    hide greysation

    with dissolve

    th "This guy... I wasted precious slow-mo time on him!"

    th "I'd better hurry up before the play is canceled."

    $ renpy.pause(0.9)  

    window hide

    

    $ day_time()

    $ persistent.sprite_time = "day" 

    play music music_list["timid_girl"]     

    scene bg verona_square 

    show sl shy cos at fright

    show dv laugh cos at fleft

    show sh normal_smile pioneer far at cright

    show us smile cos  at right

    show mz laugh cos at center

    show un smile cos at left

    with dissolve

    $ renpy.pause(1.0) 

    window show 

    "All other actors are on stage already,"

    "All smiling, 'cause the play is starting soon."

    "They're bowing to the crowd (and even Zhenya),"

    play sound Clapyell 

    "The audience welcomes them with might and main."
    "Me and Samantha get our portion of applause,"

    "As we are going up the stage. We've made it!"

#     "In fact, we even had to wait a good while."

#     "While everybody was preparing for the play,"
    "Now both the actors and the audience are ready,"

    "So finally we start."

    window hide

    $ renpy.pause(1.0)  

    scene black with fade

    scene bg verona_square

    show mi normal pioneer

    with fade
    $ renpy.pause(0.5)
    window show    

    mi "{i}Two households, both alike in dignity,{/i}"

    mi "{i}In fair Verona, where we lay our scene,{/i}"

    mi "{i}From ancient grudge break to new mutiny,{/i}"

    mi "{i}Where civil blood makes civil hands unclean.{/i}"

    mi "{i}From forth the fatal loins of these two foes {/i}"

    mi "{i}A pair of star-cross'd lovers take their life;{/i}"

    mi "{i}Whose misadventur'd piteous overthrows.{/i}"

    mi "{i}Doth with their death bury their parents' strife.{/i}"

    th "So here are spoilers, right at the beginning,"

    th "These teasers are the scourge of entertainment..."

    show mi upset pioneer   

    mi "{i}What here shall miss-{/i}"

    mi "{i}our toil shall strive to mend.{/i}"

    window hide

    hide mi with dspr

    $ renpy.pause(1.0)

    show stl normal pioneer at right

    show el smile pioneer 

    with dspr

    $ renpy.pause(1.0)  

    stl "{i}Gregory, o' my word, we'll not carry coals.{/ i}"

    el "{i}No, for then we should be colliers.{/i}"

    stl "{i}I mean, an we be in choler we'll draw!{/i}"

    el "{i}Ay, while you live, draw your neck out o' the collar.{/i}"


    stl "..."

    show el serious pioneer    

    el "{i}I will frown as I pass by; and let them take it as they list.{/i}"

    stl "..."

    mt "Tolik, don't bite your nails!"

    stl "{i}I will bite my thumb at them; which is disgrace to them if they bear it.{/i}"

    $ renpy.pause(0.8)

    show sspi normal at fleft    

    pi "{i}Do you bite your thumb at us, sir?{/i}"

    stl "{i}No, sir, I do not bite my thumb at you, sir; but I bite my thumb, sir.{/i}"

    show el upset pioneer   
     
    
    el "{i}Do you quarrel, sir?{/i}"

    pi "{i}Quarrel, sir! No, sir.{/i}"

    el "{i}But if you do, sir, am for you: I serve as good a man as you.{/i}"

    pi "{i}No better.{/i}"

    show el angry pioneer   

    el "{i}Yes, better, sir.{/i}"

    pi "{i}That's it. I'm totally offended.{/i}"

    pi "{i}Draw, if you be men.{/i}"

    play sound sworddraw    

    window hide

    hide stl

    show el angry pioneer at right

    show sspi smile at fleft

    show sh rage pioneer

    with dissolve

    window show  

    play sound swordclash   

    sh "{i}Part, fools! Put up your swords; you know not what you do.{/i}"

    th "All right, Alisa, it's your turn. Oh, here she goes."

    window hide

    hide el

    hide sspi 

    show sh normal pioneer at right

    show dv guilty cos at left  

    with dissolve

    window show     

    th "But empty-handed? Where's the text for her to read?"

    dv "..."

    show sh surprise pioneer at right   

    sh "{i}Turn Benvolio...{/i}"

    dv "{i}I will kill you... villains.{/i}"

    sh "{i}Put up thy sword, or manage it to part these men with me!{/i}"

    dv "{i}...Okay? {w}...No way? {w}No way! I'll kick your lower parts!{/i}"

    show sh scared pioneer at right

    show dv angry cos at left

    play sound swordclash   

    "Alisa started to fight Shurik."

    stop music fadeout 6    

    th "Good thing she doesn't need the paper for this part,"
    th "But still, what happened? Where is her text?"

    mi "{i}Enter Prince, with Attendants.{/i}"

    window hide

    show us upset cos 

    show sh upset pioneer at right  

    with dspr

    window show

    play music music_list["awakening_power"]    

    us "{i}Rebellious subjects, enemies to peace, {/i}"

    us "{i}Profaners of this neighbour-stained steel, -{/i}"

    show us dontlike cos    

    us "{i}Will they not hear? What, ho! you men, You beasts, {/i}"

    us "{i}That quench the fire of your pernicious rage {/i}"

    us "{i}With purple fountains issuing from your veins!{/i}"

    us "{i}On pain of torture, from those bloody hands {/i}"

    us "{i}Throw your mistemper'd weapons to the ground! {/i}"

    play sound swordclash

    us "{i}All thrown?.. Good! {w}Now, when your hands are empty...{/i}"
    us "{i}I'm gonna execute every last one of you!{/i}"
    us "{i}Yes, that's my order! You will lose your heads!{/i}"

    window hide 

    $ renpy.pause(0.8)  

    show us smile cos

    with dspr

    window show     

    us "{i}...Or not. {w}Maybe I will set you free.{/i}"

    us "{i}I have recalled the day of glory{/i}"
    us "{i}When heard the songs of timpani -{/i}"

    us "{i}The bloody Moors were banished from Europe{/i}"

    us "{i}This very day, five hundred years ago,{/i}"
    us "{i}So let us honor the ancestors! We must hold{/i}"

    us "{i}A great parade instead of execution!{/i}"

    us "{i}Well, one thing doesn't spoil the other usually...{/i}"

    us "{i}But mercy makes a nobleman more popular.{/i}"    

    us "{i}So it's decided. I shall pardon them, the bullies.{/i}"

    us "{i}You're free, poor bastards. Hail the jubilee!{/i}"

    stop music fadeout 5    
    window hide
    $ renpy.pause(0.8)  
    scene bg ss_bstage with dissolve
    $ renpy.pause(0.5)
    window show    
    th "Where could Alisa's papers disappear?"
    th "We have a minute, I can ask the girl."
    window hide
    show dv guilty cos
    show sl serious cos at right
    with dissolve 
    window show    


    dv "My text is missing! And the copy! All of them!"

    me "All papers? Is this even possible?"
    
    sl "It is. We had collected them before the play,"

    sl "Behind the stage, we put them all away...."
    
    sl "After that cleanup they nowhere to be found."

    sl "Zhenya and I have looked for them everywhere."

    th "I can't help feeling doubtful and suspicious"

    th "About this mystifying disappearance."

    show dv angry cos with dspr   

    dv "You see, Semyon, they want to ruin my performance,"

    dv "But I will not surrender just to spite them!"

    dv "I will recall the words or improvise my part."

    dv "But, Slavya, you'll regret what you have done!"

    me "Samantha’s text can help, it’s not too late,"

    show dv sad cos 

    me "Though I might not translate it all that great."

    me "But I will prompt you when and if I can."

    me "I hope your wits can help us pull through this." 

    window hide 

    $ renpy.pause(0.8)  

    hide dv with dspr

    $ renpy.pause(0.7)  

    menu:

        "Accuse Slavya":

            $ sp_sl -= 1

            show sl serious cos          

            me "What are you doing, Slavya? Do return the papers!"

            show sl angry cos        

            sl "Don't rush with accusations. I don't steal."

            me "Who else would want it? Don't you lie to me."

            me "It is plain silly to suspect somebody else."

            sl "And yet, you should. I said I didn't do it."

            sl "I wouldn't ruin this event for selfish purposes."

            

        "Approve of the stealing":

            $ sp_dv -= 1

            show sl serious cos               

            me "You found a cool way to get back at her."

            me "It took some cunning, not to mention courage."

            window hide

            show sl scared cos

            window show         

            sl "It wasn't me, Semyon! I didn't do it."

            sl "I said what I didn't mean back then..."

            sl "And now I'm terrified myself!"

            th "Seems to be true. She's almost crying."

            

        "Say nothing":

            sl "I will keep looking. But I don't know where..."

            sl "Besides, it's soon my turn to go onstage."

    

    show mz normal cos at left 

    mz "Why are you standing here with silly faces?"

    mz "It's time to act, so hurry up, you slowpokes!"
    window hide

    $ renpy.pause(0.7)  

    scene bg verona_square

    show sh normal pioneer at right

    show sl normal cos at left

    with dissolve      
    window show
    me "{i}What, shall this speech be spoke for our excuse?{/i}"

    me "{i}Or shall we on without a apology?{/i}"

    play music music_list["went_fishing_caught_a_girl"] fadein 5    

    sh "{i}But let them measure us by what they will;{/i}"

    sh "{i}We'll measure them a measure, and be gone.{/i}"

    me "{i}Give me a torch: I am not for this ambling;{/i}"

    me "{i}Being but heavy, I will bear the light.{/i}"

    sh "{i}Nay, gentle Romeo, we must have you dance.{/i}"

    sh "{i}Thou dance with us.{/i}"

    me "To dance with thou? Not a chance! {w}I'll better lie down on the grass. "

#    th "Hell what?! That idiot! His blurts confuse me."

#    th "As if Alisa's problem isn't enough."

    sh "{i}Come, knock and enter; and no sooner in,{/i}"

    sh "{i}But every man betake him to his legs.{/i}"

    me "{i}A torch for me: let wantons light of heart{/i}"

    me "{i}Tickle the senseless rushes with their heels.{/i}"

    sl "{i}Of this sir-reverence love, wherein thou stick'st.{/i}"

    sl "{i}Up to the ears. Come, we burn daylight, ho!{/i}"

    window hide 

    $ renpy.pause(0.7)

    

label ss_d6playball_eng: 

    $ day_time()

    $ persistent.sprite_time = "day"  

    scene bg verona_ball

    with fade

    $ renpy.pause(1.0)

    show el normal pioneer with dspr

    window show 

    me "{i}What lady is that, which doth enrich the hand{/i}"

    me "{i}Of yonder knight?{/i}"

    el "{i}I know not, sir.{/i}"

    window hide

    hide el with dspr

    $ renpy.pause(0.5)

    show ss nosmile dress at right with dissolve

    window show     

    me "{i}O, she doth teach the torches to burn bright!{/i}"

    me "{i}It seems she hangs upon the cheek of night{/i}"

    me "{i}Like a rich jewel in an Ethiope's ear;{/i}"

    me "{i}Beauty too rich for use, for earth too dear!{/i}"

#    th "Nowadays they’d say it's racism,"

#    th "Albeit it has a metaphoric prism."

    me "{i}The measure done, I'll watch her place of stand,{/i}"

    me "{i}And touching hers make blessed my rude hand.{/i}"
    me "{i}Did my heart love till now?.. {/i}"

    window hide 

    show godess with dissolve:

        pos (150,150)

    extend "{i}Forswear it, sight! {/i}"

    me "{i}For I ne'er saw true beauty till this night.{/i}"

    window hide 

    hide godess with dissolve

    show dv angry cos

    hide ss

    with dspr

    window show

    if d6_dv_learned >= 1:  

        dv "{i}This, should be a Montague, by his voice...{/i}"

    else:       

        dv "{i}It seems like... {w} This should be a... {w}Mountain Dew?{/i}"

        un "Montague."

        dv "{i}This, by his voice, should be a Montague!{/i}"    

    th "I guess we can rely on Lena’s hints."

    th "But bloopers, still, might ruin the impression."

    dv "{i}Come hither, cover'd with an antic face,{/i}"

    dv "{i}To fleer and scorn at our solemnity?{/i}"    

    dv "{i}Boy...{/i}"    

    window hide 

    show dv surprise cos with dspr

    $ renpy.pause(0.8)  

    window show

    th "Alisa's stuck and Lena is away,"

    th "I guess it's now my turn to save the day."

    

    if d6_nolife == 1:

        th "I’ve read her text, but now my mind's in haze..."

        th "But I seem to remember the next phrase!"

        me "Rapier!"

        show dv normal cos      

        dv "{i}Fetch me my rapier, boy!{/i}"

        $ d6_dvwellplayed += 1

    else:

        menu:

            "Take no step forward!":

                dv "{i}Don't you move forward...{w} Nor move back!{/i}"

            "Show your courage!":

                show dv normal cos          

                dv "{i}Show me your courage!{w} Yes... And eat your porridge!{/i}"        

            "Rapier!":

                show dv normal cos          

                dv "{i}Fetch me my rapier, boy!{/i}"

                $ d6_dvwellplayed += 1          

    

    window hide

    $ renpy.pause(0.4)  

    show unl normal at fright with dspr

    window show

    ssunl "{i}Why, how now, kinsman! Wherefore storm you so?{/i}"

    show dv angry cos   

    dv "{i}Uncle, this is a Montague!{/i}"

    menu:

        "Our foe":

            if d6_dv_learned == 2:

                dv "{i}Our foe!{/i}"

                $ d6_dvwellplayed += 2              

            else:

                dv "{i}Our hoe... He took it!{/i}"

                $ d6_dvwellplayed += 1 

        "Go away, hellion!":

            dv "{i}Go away, hellion! {w}Join the rebellion!{/i}"        

    show unl upset at fright         

    ssunl "{i}Therefore be patient, take no note of him.{/i}"

    dv "{i}Nations for horse with wistful overheating?{/i}"

    dv "{i}...Patience perforce with wilful choler meeting{/i}"

    dv "{i}Makes my flesh tremble in their different greeting.{/i}"

    dv "{i}I will withdraw: but this intrusion shall{/i}"

    dv "{i}Now seeming sweet convert to bitter gall.{/i}"

    window hide 

    $ renpy.pause(0.5)  

    hide unl

    hide dv

    with dspr

    "And with this, ends Alisa's torment,"

    "But other things do keep me worried."

    stop music fadeout 4

    "I see our special guest. It's time to let"

    "My Romeo meet beloved Juliet."

    window hide

    $ renpy.pause(0.5)  

    show ss shy dress with dspr

    $ renpy.pause(1.0) 

    window show

    play music Romeo_And_Juliet fadein 3    

    me "If I profane with my unworthiest hand"

    me "This holy shrine, the gentle fine is this, -"

    me "My lips, two blushing pilgrims, ready stand"

    me "To smooth that rough touch with a tender kiss."

    "The silence falls onto Romeo's lips,"

    "For they are busy with the promised kiss."

    show ss smile2 dress with dspr

    ss "{enen}Good pilgrim, you do wrong your hand too much,{/enen}"

    ss "{enen}Which mannerly devotion shows in this;{/enen}"

    ss "{enen}For saints have hands that pilgrims' hands do touch,{/enen}"

    ss "{enen}And palm to palm is holy palmers' kiss.{/enen}"

    me "{i}Have not saints lips, and holy palmers too?{/i}"

    show ss shy dress with dspr

    ss "{i}Ay, pilgrim, lips that they must use in prayer.{/i}"

    me "{i}O, then, dear saint, let lips do what hands do{/i}"

    me "{i}They pray, grant thou, lest faith turn to despair.{/i}"

    show ss shy2 dress 

    ss "{i}Saints do not move, though grant for prayers' sake.{/i}"

    me "{i}Then move not while my prayer's effect I take.{/i}"

    "The cruсial moment. Duty versus senses."

    th "...And what about unwanted consequences?"

    th "How can I workaround in current scene?"

    th "I know the trick! I'll handle it, I mean."

    "I took a step towards my silent mate,"

    "Awaiting in a petrified state."

    window hide

    $ renpy.pause (0.5)

    if persistent.dress == 'purple':

        show cg ss_playkiss1p with dissolve 

    else:   

        show cg ss_playkiss1r with dissolve

    

    $ persistent.ssg_110 = True 

     

    $ renpy.pause (1)

    window show

    "I hid my smile in Juliet's wavy hair,"

    "Like in a mime, I softly kissed the air."

    window hide

    $ renpy.pause (1)

    hide cg

    show ss shy dress

    with dissolve

    $ renpy.pause (0.5)

    window show

    me "{i}Thus from my lips, by thine my sin is purg'd.{/i}"

    ss "{i}Then have my lips the sin that they have took.{/i}"

    me "{i}Sin from my lips? Give me my sin again.{/i}"

    "It's time to have a second go at \"kissing\" -" 

    "I'm reaching for Samantha for this reason."

    show ss shy2 dress 

    "And maybe it was my imagination,"

    "But Sammy also moved in my direction."

    window hide

    if persistent.dress == 'purple':

        show cg ss_playkiss1p with dissolve 

    else:   

        show cg ss_playkiss1r with dissolve

    $ renpy.pause (0.5)     

    window show

    "And suddenly I'm drowning in her eyes:"

    "It's clear... there's nothing clear at all."

    th "What if I kiss her? No, it won't be wise!"

    th "Hold on, Semyon! It's surely a bad call."

    th "The previous \"kiss\" was quite innocuous,"

    th "So I should keep it up. It's obvious."

    menu:

        "Pretend to kiss":

            show ss shy dress       

            "My lips just kissed the air another time,"

            "There truly wasn't any trail of crime."

            window hide

            $ renpy.pause (0.5)

            hide cg with dissolve

            $ renpy.pause (0.5)

            if d6_kissing == 1:

                $ sp_ss -= 1 

                th "I'd promised her, and almost had it done,"

                th "Though previously I had kissed no one."

                th "I might be deemed an untrustworthy fellow,"

                th "But Olga will not kill me like Othello..."

                window hide 

            show un normal cos at fleft with dspr

            $ renpy.pause (0.5)         

            

        "Kiss on the lips":

            $ sp_ss += 2

            th "We've planned it. So it shouldn't be undone,"

            th "I'll do it for the audience... and fun!"

            if d6_kissing == 0:

                th "I said it would be better not to kiss,"

                th "But was there any reason to insist?"

                $ sp_ss -= 1                               

            "I'm reaching for Samantha's lips again,"

            "We're now too close for things like shaking hands,"

            "I'm stumbling to pretend it's all amiss,"

            "And thus we have an \"accidental\" kiss."

            window hide

            if persistent.dress == 'purple':

                show cg ss_playkiss2p with dissolve 

            else:   

                show cg ss_playkiss2r with dissolve

    

            $ persistent.ssg_111 = True 

                     

            $ renpy.pause (1.5) 

            window show         

            "The silence fell onto the audience hall,"

            "The play stood still, there was no sound at all"

            "Except the squeak of our camp leader's chair,"

            "She wouldn't stop the play - it'd be unfair."

            "There's nothing left to Olga, but pretend"

            "Ignoring that dramatic incident."

            window hide

            $ renpy.pause (0.7)

            hide cg

            show ss shy dress 

            with dissolve2           

            "I hear the sighs of interest in the crowd,"

            "The audience is slowly filled with sounds,"

            "The giggles and the chuckles of the folks,"

            "The moaning of excitement and the talks."

            "I sprang back and stood, staring at the girl,"

            "Who seemed to be the shiest in the world."

            if d6_kissing == 0:           

                th "Poor little soul. Semyon, what's wrong with you?"

                th "It's something that she shouldn't be going through..."

            show un normal cos at fleft with dspr             

            th "But I don't have the time to moan and groan,"

            th "It's Lena's time. The show - it must go on."

            $ d6_kissing = 2

    

    stop music fadeout 5            

    un "{i}Madam!{/i}"

    un "{i}Your mother craves a word with you.{/i}"

    window hide 

    $ renpy.pause(0.5)  

    hide ss with dspr

    $ renpy.pause(1.0)  

    show un normal cos at fleft   

    window show 

    me "{i}What is her mother?{/i}"

    un "{i}Her mother is the lady of the house.{/i}"

    un "{i}I nurs'd her daughter that you talk'd withal;{/i}"

    un "{i}I tell you, he that can lay hold of her... {w}Shall have the chinks.{/i}"

    window hide

    hide un with dspr

    $ renpy.pause(0.5)      

    window show

    me "{i}Is she a Capulet?{/i}"

    me "{i}O dear account! My life is my foe's debt.{/i}"

    window hide 

    show sh normal pioneer with dspr

    $ renpy.pause(0.5)      

    window show 

    sh "{i}Away, be gone; the sport is at the best.{/i}"

    me "{i}Ay, so I fear; the more is my unrest.{/i}"

    window hide     

    $ renpy.pause(0.7)   

    scene bg ss_bstage with fade 

    "I have a moment just to catch my breath,"

    "And then I'll play that setting to the death."

    window hide

    $ renpy.pause(0.5)

    if d6_kissing == 2:

        jump d6_whykiss_eng

    else:

        jump d6_findthief_eng

    

label d6_whykiss_eng:   

    show dv smile cos with dspr    

    window show

    play music music_list["gentle_predator"] fadein 2   

    dv "You scoundrel! Why'd you kiss her in that scene?"

    dv "Are you a hunter for young hearts? She is thirteen!"

    menu:

        "It was an accident":

            me "I didn't mean to. Don't blame me,"

#

            me "Because I'm going to blame myself much more"

            me "For what I've done... although it wasn't on purpose."

            show dv normal cos with dspr            

            dv "You are so serious, while I am only joking!"
            dv "I'm now more concerned about myself..."

            dv "How was it? Did I screw up real bad?"

            me "It was alright, I guess... They didn't notice anything."

            me "After the thing that I just did? Nobody will."

            jump d6_findthief_eng

        "I don't care":

            $ sp_dv += 1

            me "They made us play this part, it's too much stress."

            me "You know, {i}ars longa, vita brevis est{/i}"

            me "So if the setting is no more than just a play"

            me "Then nothing's wrong about a fake kiss, I should say!"

            me "I'm sure it's bearable for Sam, what I have done."

            me "It's not like it was hurting anyone."

            dv "Perhaps. But still... You're acting weird, Semyon."

            dv "It wasn't typical of you, what you have shown."
            dv "You're falling for her. Don't you even try to hide"
            dv "The feelings that you have. It is an awful lie."

#            me "You're teasing me, Alisa. Listen, why?"

#            dv "And you are making up another lie."

            me "There is no lie. I'll tell you even more:"

            me "We've done it many, many times before!"

            show dv angry cos with dspr             

            dv "What?.. Is that true?!"

            me "...Nope! Gotcha!"

            me "Amazing! I've just made you act the fool."

            dv "You are a predator! I'm gonna keep an eye on you."

            me "Come on, Alisa. I was joshing you, okay?.."

            me "I wanted to get back at you for that, the other day..."

            show dv smile cos            

            dv "And now the joke's on you, you see? I win!"

            me "Oh... You were joking too? Okay, don't grin..."

            jump d6_findthief_eng           

    

label d6_findthief_eng:

    window hide

    $ renpy.pause(0.8) 

    if d6_dvwellplayed <= 1:

        if d6_dvwellplayed == 0:

            $ sp_dv -= 1

        show dv guilty cos with dspr   

        window show     

        dv "Semyon. Have you been prompting me correctly?"

        dv "It sounded wrong... Or maybe I misheard?"

        me "But no one noticed, so it doesn't matter"

        me "That it was not exactly word for word."

        dv "It was embarrassing and totally uncool!"

        dv "I mumbled awkward nonsense, like a fool,"

        dv "I've only promised of making all efforts"

        dv "For you have promised of lending me support."

        me "I'm sorry. No one really knew your lines."

    else:

        $ sp_dv += 1

        if d6_dvwellplayed == 3:

            $ sp_dv += 1        

        show dv smile cos with dspr

        window show     

        dv "So, it was fine. And you did prompted well,"

        dv "Though I have learned my lines enough to tell..."

        dv "The thief still must be punished anyway."

    show dv normal cos 

    dv "I hope you've got it 'bout Slavyana's kind."

    me "Don't judge so fast. The thief is still not found."

    dv "If it's not her then who's the one to blame?"
    dv "Do you suspect somebody? Give a name!"

    

    dv "Or let's blame me, no matter reason why..."

    dv "Except the fact that I have got black eye."

    show dv angry cos with dspr 

    dv "I am the guiltiest one in whole the camp,"

    dv "The vile crow who is guilty by default!"

    dv "While Slavya still as innocent as lamb,"

    dv "No matter what she did - let's sing her dithyramb!"

    menu:

        "You're right":

            $ sp_dv += 1

            me "Relax, Alisa. I am on your side."

            show dv normal cos with dspr            

            dv "Huh, really?.. Fine. That's something for a start..."

            dv "You know, Slavyana used to be on right"

            dv "That larceny we must investigate"

            dv "To find the clues before it's not too late."

            dv "We must expose the crimes of sneaky hypocrite!"

        "But if it was someone else?":

            me "If someone else has stolen all the things?"

            me "Like Zhenya, for example. How'd you think?"

            show dv guilty cos with dspr            

            dv "Hell maybe! She has motives to as well."

            dv "They're neighbours, after all... They dwell"
            dv "In the same house. Plotting against me"
            dv "Right there. They must be both at it."

            me "How many had you turned against yourself?"

            show dv normal cos with dspr            

            dv "Who knows? My jokebooks wouldn't fit the shelf!"

            dv "But jokes' behind. I'll punish thieving birds,"
#            dv "But jokes' behind. It's time to measure swords,"

            dv "They will regret their vileness, mark my words!"

            

        

    me "I must be on the stage. I'm got to go."

    me "Let's show them our best by plain' as one."

    me "We played pretty well... So far so good. So,"

    me "Don't you take any action till we're done."

    dv "Relax, Semyon. I, honestly, not disinclined"

    dv "To spoil this evening for all folks. I'm not that Slavya kind."

    me "Go ask if someone could have seen your text,"

    me "But don't you miss your part! My part is next."

    window hide

    stop music fadeout 5

    $ renpy.pause(0.8)    

    scene bg verona_square

    show sl normal cos

    show sh upset pioneer at left

    with dissolve

    window show

    sh "{i}Romeo! my cousin Romeo! {/i}"

    sl "{i}He is wise;{w} And, on my life, hath stol'n him home to bed.{/i}"

    sh "{i}He ran this way, and leap'd this orchard wall.{w} Call, good Mercutio.{/i}"

    sl "{i}Nay, I'll conjure too.{/i}"

    window hide

    $ renpy.pause(0.8)  

    scene bg verona_garden with fade

    window show

    play music Warm_evening fadein 5    

    me "{i}But, soft! What light through yonder window breaks?{/i}"

    me "{i}It is the east, and Juliet is the sun.{/i}"

    me "{i}Arise, fair sun, and kill the envious moon,{/i}"

    me "{i}Who is already sick and pale with grief,{/i}"

    me "{i}That thou her maid art far more fair than she.{/i}"

    me "{i}O, that she knew she were!{/i}"

    me "{i}She speaks yet...{/i}"

    voice "None can get it!"

    th "The devil with that pioneering tweeter,"

    th "He must be aimed now by our dear campleader."

    me "{i}She speaks yet she says nothing: what of that?{/i}"

    show ss unsure dress  with dspr 

    ss "{enen}Ay me!{/enen}"

    ss "{i}{enen}O Romeo, Romeo! Wherefore art thou Romeo?{/enen} {/i}"

    voice "Yes! Yes! Wherefore - and that's the question!"

    voice "Stop blah-blah-blah and show the real passion!"

    show ss nosmile dress   

    ss "{i}{enen}Deny thy father and refuse thy name{/enen}{/i}"

    ss "{i}{enen}Or, if thou wilt not, be but sworn my love,{/enen}{/i}"

    ss "{i}{enen}And I'll no longer be a Capulet.{/enen}{/i}"

    ss "{i}{enen}Retain that dear perfection which he owes{/enen}{/i}"

    ss "{i}{enen}Without that title. Take all myself.{/enen}{/i}"

    th "{i}...We got a deal?{/i} Damned speaker from the hell!"

    me "{i}We got a deal. I am that one for you!{/i}"

    show ss surprise dress  

    me "{i}Call me but love, and I'll be new baptized;{/i}"

    me "{i}Henceforth I never will be Romeo.{/i}"

    ss "{i}{enen}What man art thou that thus bescreen'd in night{/enen}{/i}"

    ss "{i}{enen}Art thou not Romeo and a Montague?{/enen}{/i}"

    me "{i}Neither, fair saint, if either thee dislike.{/i}"

    voice "Thus get away-ay-ay-ay-ay!"

    mt "I've shut that joker, now go on the play!"

    window hide

    $ renpy.pause(0.8)      

    scene bg verona_church

    show mz normal cos

    with fade2

    window show    

    mz "{i}Benedicite!{/i}"

    mz "{i}What early tongue so sweet saluteth me?{/i}"

    me "{i}Good morrow, father.{/i}"

    mz "{i}Young son, it argues a distemper'd head{/i}"

    mz  "{i}So soon to bid good morrow to thy bed:{/i}"

    mz "{i}Thou art up-roused by some distemperature;{/i}"

    mz "{i}Or... hath not been Romeo in bed to-night?{/i}"

    

    me "{i}That last is true; the sweeter rest was mine.{/i}"

    show mz bukal cos 

    mz "{i}God pardon sin! Wast thou with Rosaline?{/i}"

    me "{i}With Rosaline, my ghostly father? No;{/i}"

    me "{i}I have forgot that name, and that name's woe.{/i}"

    show mz normal cos  

    mz " {i}That's my good son: but where hast thou been, then?{/i}"

    me "{i}Then plainly know my heart's dear love is set{/i}"

    me "{i}On the fair daughter of rich Capulet:{/i}"

    me "{i}As mine on hers, so hers is set on mine;{/i}"

    me "{i}And all combined, save what thou must combine{/i}"

    me "{i}By holy marriage: when and where and how.{/i}"

    mz "{i}Holy Saint Francis, what a change is here!{/i}"

    mz "{i}Is Rosaline, whom thou didst love so dear,{/i}"

    mz "{i}And art thou changed? Pronounce this sentence then,{/i}"

    mz "{i}Women may fall, when there's no strength in men.{/i}"

    show mz smile cos 

    stop music fadeout 4    

    mz "{i}But since you know the haven where you heading{/i}"

    mz "{i}Let's set the date of that... that epic wedding.{/i}"

    window hide 

    $ renpy.pause(0.8)  

    scene bg verona_square

    show sl serious cos

    show sh normal pioneer at left

    with fade

    window show     

    play music music_list["went_fishing_caught_a_girl"] fadein 5    

    sl "{i}Where the devil should this Romeo be?{/i}"

    sl "{i}Came he not home to-night?{/i}"

    sh "{i}Not to his father's; I spoke with his man.{/i}"

    sh "{i}Tybalt, the kinsman of old Capulet,{/i}"

    sh "{i}Hath sent a letter to his father's house.{/i}"

    show sl surprise cos 

    sl "{i}A challenge, on my life.{/i}"

    show sh serious pioneer at left 

    sh "{i}Romeo will answer it.{/i}"

    show sl sad cos

    sl "{i}Alas poor Romeo! He is already dead; stabbed with a white wench's black eye; shot through the ear with a love-song;{/i}"

    sl "{i}The very pin of his heart cleft with the blind bow-boy's butt-shaft: and is he a man to encounter Tybalt?!{/i}"

    show sh surprise pioneer at left    

    sh "{i}Why, what is Tybalt?{/i}"    

    sl "{i}More than prince of cats, I can tell you.{/i}"    

    sl "{i}He fights as you sing prick-song, keeps time, distance, and proportion; rests me his minim rest, one, two, and the third in your bosom.{/i}"    

    

    sh "{i}Here comes Romeo, here comes Romeo.{i}"

    show sh normal pioneer at left

    show sl normal cos

    sl "{i}Signior Romeo, bon jour! There's a French salutation to your French slop{/i}"

    sl "{i}You gave us the counterfeit fairly last night.{/i}"

    me "{i}Good morrow to you both. What counterfeit did I give you?{/i}"

    sl "{i}The slip, sir, the slip; can you not conceive?{/i}"

    me "{i}Pardon, good Mercutio, my business was great; and in such a case as mine a man may strain courtesy.{/i}"

    "Now Lena has appeared on the scene:"

    "In minute role of Juliette's nurse."

    "Her mistress sends a message for Romeo"

    "And now she's patiently stamps indoors."

    

    sl "{i}A sail, a sail!{/i}"

    show sh smile pioneer at left   

    sh "{i}Two, two; a shirt and a smock.{/i}"

    un "{i}My fan! Where is my fan?{/i}"

    sl "{i}Give her that fan, to hide her face;{/i}"

    sl "{i}For her fan's the fairer face.{/i}"

    show un normal cos at right 

    un "{i}God ye good morrow, gentlemen.{/i}"

    show sl smile cos

    sl "{i}God ye good den, fair gentlewoman.{/i}"

    show un surprise cos at right   

    un "{i}Is it good den?{/i}"

    sl "{i}'Tis no less, I tell you, for the bawdy hand of the dial is now upon the prick of noon.{/i}"

    show un angry2 cos at right 

    un "{i}Out upon you! What a man are you!{/i}"

    show un serious cos at right

    un "{i}Gentlemen, can any of you tell me where I may find the young Romeo?{/i}"

    me "{i}I am the youngest of that name.{/i}"

    un "{i}If you be he, sir, I desire some confidence with you.{/i}"

    me "{i}I will follow you.{/i}"

    sl "{i}Farewell, ancient lady; farewell,{/i}"

    un "{i}Marry, farewell!{/i}"

    window hide

    hide sl

    hide sh

    with dspr

    $ renpy.pause(1.0)      

    window show

    un "{i}Now, afore God, I am so vexed,{/i}"

    un "{i}That every part about me quivers. Scurvy knave!{/i}"

    un "{i}Ah! Sir. I've come for other matter.{/i}"

    un "{i}My young lady bade me inquire you out;{/i}"

    un "{i}What she bade me say, I will keep to myself{/i}"

    un "{i}But first let me tell ye, if ye should lead her into a fool's paradise...{/i}"

    me "{i}Nurse, commend me to thy lady and mistress. I protest unto thee...{/i}"

    show un smile cos at right  

    un "{i}Good heart, and, i' faith, I will tell her as much:"
    un "{i}Lord, Lord, she will be a joyful woman.{/i}"

    me "{i}What wilt thou tell her, nurse? Thou dost not mark me.{/i}"

    show un smile2 cos at right     

    un "{i}I will tell her, sir, that you do protest; which, as I take it, is a gentlemanlike offer."
    un "{i}Good bye!{/i}"

    stop music fadeout 5

    me "{i}But wait dear nurse...{/i}"

    un "{i}Yes-yes. Regards to young lady.{/i}"

    window hide

    $ renpy.pause(0.8)  

    scene bg ss_bstage

    show sl normal cos at left
    show dv normal cos at right

    with dissolve

    $ renpy.pause(0.5)

    window show

    sl "Semyon! I've found what have been lost."

    dv "Oh, my! Returned my stuff - and boasts"

    dv "Just when that papers not so matter"

    dv "You're just turned back. Could you do better?"

    sl "No reasons why. Think in your fav'rite way,"

    sl "I've got your papers. Nothing more to say."

    me "Thanks, Slavya. Now the show will go on."


    show dv angry cos at right  

    dv "My play was spoiled! For that \"thanks\"'s not my word!"

    dv "Not much to read left; it's the time to draw the sword"

    dv "And smash with it the blonde princess"
    show sl serious cos at left    
    dv "With fury, yes! Precicely as the paper says!"

    dv "It is the only way my role can be well played,"

    dv "That nasty theif's depts must be paid."

    play music music_list["faceless"]  fadein 5 

    dv "Tin rapiers. Sort of things to care about -"

    dv "They can make harm just as the real one."

    dv "That rusty blade can also infect you,"

    dv "A great disaster from some point of view,"

    dv "But certainly will teach you how to steal..."

    dv "Have got it, you?! Our duel is for real!"

    
    

    show sl scared cos at left  

    me "What do you say, Alisa?! Why so evil?"

    dv "Because I'm not impressed of this retrieval."

    dv "I hate hypocrisy. She must confess."
#    dv "What do I say: that steal she must confess."

    dv "With that I'll maybe reconcider nontheless,"
#    dv "Thought kicking of some booties fits not less."

    dv "So think. Without this basic righteous step"

    dv "I see not reason to discount the dept."

    show sl angry cos at left   

    me "It must be obscuration in your head,"

    me "I call camplead before it's not too late!"

    sl "Don't do. We must continue the play"
    sl "Without any furthermore delay."

    me "What is your plan then to prevent the fight?"

    sl "I'll let her hit me once. It is alright,"

    

    sl "Then probably she will be satisfied."

    dv "One strike isn't enough! Look at the script."
    dv "It says you must be badly whipped."

    me "Stop it already. What is wrong with you?"

    show dv normal cos at right 

    dv "What's wrong with her? Why don't you ask?"
    dv "Suspicious kind of lowliness, you see;"
    dv "Why this obedience all of the sudden?"

    dv "I'll tell you why: she know that she deserves it -"

    dv "A punishment for the rat thing she did!"

    show sl sad cos at left 

    sl "You don't need a good reason to be cruel!"
    sl "You are the cruelness itself!"

    menu:

        "I shall not let":

            $ sp_dv -= 1

            $ sp_sl += 1

            me "I take that iron thing away, Alisa."

            show dv angry cos at right          

            dv "Hey, give it back! I need my sword!"
#            dv "Must we have our rapiers from a... birch?"

            me "It is too dangerous for you. Go break a branch,"
            me "Fight with a wooden stick, I do not care."

            me "There'll be no stabbing in my watch."

            dv "You truly can't help keeping out, I see."

            show dv guilty cos at right

            dv "Just give me rapier back. I shall not fight."

            me "You promise that?.."

            dv "Sure thing."

            me "I don't believe you."

            show dv rage cos at right           

            dv "You driveller! To hell that piece of tin!"

            "Alisa turned red. At least, not steamed."

            sl "Thank you, Semyon... But let her have it."

            sl "Taking the props away is not enough"
            sl "To stop her. She always gets the stuff."

            sl "If she is up to it - she'll do it. One way or another."

            show dv angry cos at right          

            me "...Take it, Alisa. But you had promised me."

            hide dv with dspr

            "She took the rapier and had run away."

            sl "I hear the scene is almost reaches it's end:"

            sl "The second act is seems to be played out."

            show sl scared cos at left

            sl "If something happens... well... Avenge me."

            me "You mean Alisa or her character?"

            me "As a Romeo I will punish Tibalt,"

            me "As a Semyon - I will protect my friend."

        "We have to investigate first":

            me "Where have you found them, Slavya?"

            me "And by the way... Any idea who's the thief?"

            show sl angry cos at left           

            "The girl kept silent, frowning."

            th "It looks like Slavya knows the answer."

            sl "I have no mood for any sort of a confession,"

            sl "It's not a time and place for that. But also"

            sl "I'm tought to tell the truth. And so I say"

            sl "...That I do not like whimpering at all."

            sl "I hasn't ever given out a single secret"

            sl "Of other people. Should keep it that way."

            me "I know, I know! It must be Shurik! Him again!"

            me "That villain has been plotting every day!"

            dv "The four-eyed engineer?.. It's not him. He's afraid of me."

            dv "But there's another vengeful snake around,"
            dv "Near library she could be found..."


#            dv "Her grievance will last forever -"

#

            me "What?! Lena - is your enemy?"

            show dv angry cos at right          

            dv "How stupid can you be? I talk 'bout Zhenya."

            dv "Her forked tongue spreads only venom."

            window hide

            $ renpy.pause(1.0)

            show dv normal cos at right 

            dv "Hey, look how Slavya listens. Check her face,"

            dv "You see? It figures that we found the truth!"

            show sl surprise cos at left            

            dv "The question left is did them act together?"

            dv "I guess you could... Why else you didn't turn her in?"

            dv "But I'm not sure. It certainly will open later."

            show sl angry cos at left               

            sl "Another word and I will slap you by this saber."

            show dv laugh cos at right          

            dv "Sure, it's a fight time! Do it on the stage!"         



    window hide

    stop music fadeout 3

    $ renpy.pause(0.8)  

    scene bg verona_square

    show sl serious cos

    show sh upset pioneer at left

    with dissolve

    window show         

    sh "{i}I pray thee, good Mercutio, let's retire:{/i}"

    sh "{i}The day is hot, the Capulets abroad,{/i}"

    sh "{i}And, if we meet, we shall not scape a brawl;{/i}"

    sh "{i}For now, these hot days, is the mad blood stirring.{/i}"

    sh "{i}No, that is all. It seems too late to leave.{/i}"

    window hide

    play music music_list["heather"] fadein 2   

    show dv normal cos at fright with dspr

    window show 

    sh "{i}By my head, here come the Capulets.{/i}"

    sl "{i}By my heel, I care not.{/i}"

    dv "{i}Follow me close, for I will speak to them.{/i}"

    dv "{i}Gentlemen, good den: a word with one of you.{/i}"

    sl "{i}And but one word with one of us? Couple it with something; make it a word and a blow.{/i}"

    dv "{i}You shall find me apt enough to that, sir, an you will give me occasion.{/i}"

    sl "{i}Could you not take some occasion without giving?{/i}"

    dv "{i}Mercutio, thou consort'st with Romeo,{/i}"    

    sl "{i}Consort! what, dost thou make us minstrels?{/i}"

    sl "{i}An thou make minstrels of us, look to hear nothing but discords?{/i}"

    play sound sworddraw    

    sl "{i}Here's my fiddlestick; {w}here's that shall make you dance.{/i}"

    dv "{i}Well, peace be with you, sir: here comes my man.{/i}"

    dv "{i}Romeo, the hate I bear thee can afford{/i}"

    dv "{i}No better term than this, - thou art a villain.{/i}"

    me "{i}I'm not a villain, bless you.{/i}"

    me "{i}Your words are wrong, I see thou know'st me not.{/i}"

    me "{i}But thou shall know good news soon{/i}"

    me "{i}There' something that unites us. {/i}"

    me "{i}Let's part friends with thou, Capulet.{/i}"

    show sl angry cos

    sl "{i}O calm, dishonourable, vile submission!{/i}"

    sl "{i}Alla stoccata carries it away!{/i}"

    sl "{i}Tybalt, you rat-catcher, will you walk?{/i}"

    show dv angry cos at fright  

    dv "{i}What wouldst thou have with me?{/i}"

    sl "{i}Good king of cats, nothing but one of your nine lives,{/i}"

    sl "{i}That I mean to make bold withal, and as you shall use me hereafter, drybeat the rest of the eight.{/i}"

    sl "{i}Will you pluck your sword out of his pitcher by the ears?{/i}"

    dv "{i}I am for you.{/i}"

    play sound sworddraw    

    dv "{i}Such a Green Berets{/i}"

    dv "{i}I eat for breakfast!{/i}"

    hide sh 

    show dv angry cos at right  

    "The enemies, our duelists stood in the pose,"

    "So have I, rooted in the place by fear,"
    "For what is going to happen next."

    "Since I had failed to make peace between the two,"

    "Now I can only hope for their own sense and reason."

    play sound swordclash

    me "{i}Gentle Mercutio, put thy rapier up!{/i}"

    play sound swordclash   

    "The swords clashed twice in the air,"

    "Far harder then it's necessary."

    show dv normal cos at right 

    "Alisa grinned: she's having a good time,"

    play sound swordswipe

    "Another thrust hit nothing, passed the target."

    me "{i}Draw, Benvolio; beat down their weapons!{/i}"

    me "{i}Gentlemen, for shame! Forbear this outrage!{/i}"

    me "{i}Tybalt, Mercutio, the prince expressly hath!{/i}"

    me "{i}Forbid this bandying in Verona streets!{/i}"

    "Romeo must now try to stop them"

    "By stepping in between of two. That's what I do."
    "But Tybalt places sword under my hand. Mercutio is hit."

    play sound swordswipe   

    show sl scared cos

    sl "{i}Ah!{/i}"

    

    "Seeing the blood, Tybalt immediately runs off."

    stop music fadeout 4

    hide dv

    sl "{i}I am hurt. A plague o' both your houses!{/i}"


    window hide

    $ renpy.pause(0.8)

    window show

    play music music_list["orchid"] fadein 3    

    th "But why is Slavya suddenly so silent? Is she falling?"

    th "It's not the end yet, she must say her final words..."

    th "I see the blood too! She is turning pale!"

    th "What's going on?! No, it can not be!"

    window hide

    show dv surprise cos at fright

    hide sl

    with dissolve

    window show

    dv "Hey, what are you? I did not touch you, did I?"

    "We both stood still, looking at Slavya,"

    "Waiting for her to act again or say a word at least."

    "Like waiting for a miracle - instead of calling for a doctor."

    show dv scared cos at fright 

    "Everyone else consider this a part of our performance,"

    "Even the drops of blood didn't surprise them."

    stop music fadeout 2

    window hide

    show dv surprise cos at fright

    show sl serious cos

    with dissolve

    window show 

    sl "I'm sorry... I felt dizzy here."

    play music music_list["you_lost_me"]    

    sl "My tirade was not over."

    show dv guilty cos  

    th "She's back to us! Oh, thank you, Providence!"

    th "But then what happened? What a nightmare?"

    window hide

    show sh upset pioneer at left with dspr 

    window show

    sl "{i}Help me into some house, Benvolio{/i}"

    sl "{i}Or I shall faint. A plague o' both your houses!{/i}"

    sl "{i}They have made worms' meat of me: {/i}"

    sl "{i}I have it, and soundly too. Your houses!{/i}"

    window hide

    hide sh

    hide sl

    with dissolve

    window show

    "Shurik took pale Slavyana from the stage,"

    "And left me and Alisa in bewilderment."

#    "No. We were hell of a stunned!"

    th "What should I do now, follow them? Or stay?"

    th "How to continue the play? I could not!"
    "So we stood on the stage, me and Alisa"
    "Like the two statues of lost and shocked people."
#    "Fruitlessly trying to put it together."

    window hide

    show sh upset pioneer at left

    hide dv

    with dspr

    window show

    "Finally Shurik is returning from behind the scenes,"
    "He's looking calm and eager to go on."

    sh "{i}O Romeo, Romeo, brave Mercutio's dead! {/i}"

    sh "{i}That gallant spirit hath aspir'd the clouds, {/i}"

    sh "{i}Which too untimely here did scorn the earth.{/i}"

    me "{i}But... Is she... I mean is he fine?{/i}"

    show sh serious pioneer at left 

    sh "{i}...Completely dead.{/i}"

    me "{i}This day's black fate on more days doth depend; {/i}"

#    me "{i}This but begins the woe others must end.{/i}"

#    me "{i}Not to mention the fact that it's bad in itself,{/i}"
    me "{i}It's a bad sign... I mean, the death is not good in itself,{/i}"

    me "{i}But still should not forget about the signs.{/i}"

    show sh rage pioneer at left        

    sh "What are you babbling? Pull yourself together!"

    

    show sh upset pioneer at left

    show dv sad cos at right    

    sh "{i}Here comes the furious Tybalt back again.{/i}"

    me "{i}Alive in triumph! And Mercutio slain! {/i}"

    me "{i}Away to heaven respective lenity,{/i}"

    me "{i}And fire-ey'd fury be my conduct now!{/i}"

    me "{i}Now, Tybalt, take the \"villain\" back again{/i}"
    me "{i}That late thou gavest me; for Mercutio's soul{/i}"
    me "{i}Is but a little way above our heads, {/i}"

    me "{i}Staying for thine to keep him company.{/i}"
    me "{i}Either thou or I, or both, must go with him.{/i}"

    play sound sworddraw    

    show dv angry cos at right    

    dv "{i}Thou, wretched boy, that didst consort him here,{/i}"

    dv "{i}Shalt with him hence.{/i}"

    play sound sworddraw    

    me "{i}This shall determine that.{/i}"

    "And now another fight we should play,"

    play sound swordclash

    "This time Alisa mustn't be victorious."

    "The fight is short: just a few moves;"

    show dv shocked cos at right 

    play sound swordswipe   

    "Romeo lunged with sword and Tybalt is defeated."

    hide dv

    "The public seems to be displeased by it,"

    "Perhaps they were expecting more from Alisa."

#     "Rather, they were waiting for Alisa's new tricks."

    show sh surprise pioneer at left    

    sh "{i}Romeo, away, be gone! {/i}"

    sh "{i}The citizens are up, and Tybalt slain.{/i}"

    sh "{i}Stand not amaz'd. The prince will doom thee death {/i}"

    sh "{i}If thou art taken. Hence, be gone, away!{/i}"

    "Well, he don't have to ask me twice,"

    stop music fadeout 5

    "To get behind the scene is all I want."

    window hide

    scene bg ss_bstage

    show sl sad cos at right

    with dissolve

    window show

    me "What happened there, Slavya? Are you fine?"

    "I hear the steps: Alisa is approaching too."

    show dv sad cos at left

    dv "Have I hurt you? No, I shouldn't have!"

    dv "I never touched you with my sword, I swear!"
    dv "Or... Am I wrong?"

    show dv guilty cos at left

    play music Warm_evening fadein 5    

    sl "I did a very stupid thing there, guys."

    sl "I'm am a fool. I almost ruined everything..."


    sl "It's not a blood at all, but a beet juice,"

    show dv surprise cos at left   

    sl "I wanted to teach everyone a lesson,"

    sl "To see some tears about me or at least some pity,"

    sl "If someone's able still to care about me..."

    show dv normal cos at left  

    sl "It was a fantasy of mine, I had no plan to really do it,"

    sl "But then the quarrel right before the battle"
    sl "Where angriness took over me. It made me bold." 

    menu: 

        "Have you lost your mind?":

            $ sp_sl += 1

            me "Excuse me, are you crazy? What a jokes?!"

            me "Don't speak now like it wasn't a big deal,"

            me "Can you imagine how much you scared us?"

            me "There was a second before everyone would see!"

            show dv guilty cos at left           

            dv "They didn't though.  It's good to stop at the right time."

            show sl surprise cos  at right         

            th "What? Does Alisa stand up for Slavyana?"    

            sl "I realized how wrong I am just the next thing after I did it."

            show sl normal cos at right        

            sl "What else can I say? I'm very sorry."
            sl "I'll never do something like it again."

            

            

        "The main thing is that you're alright":

            $ sp_dv -= 1

            me"I probably should have been angry,"

            me "I'm shocked that you could take such a decision..."

            me "But I am glad that trouble has passed us."

            show sl normal cos at right       

            me "Well, and the fact that you have stopped"

            me "At the right time. And teased my nerves only."

            dv "Speaking of nerves, I suffered twice there!"

            dv "You'll be surprised, but I'm not a killer,"

            dv "And I have no intention to become one."

            

            

    me "What was your further plan?"
    me "Did you prepare an explanation for the others"

    me "That you have failed the play just for the sake of laugh?"     

    sl "I'd say that I did juice trick for the play,"

    sl "To entertain the audience, nothing more."

    sl "And I would get away with it. No problem."

    show dv normal cos at left

    sl "They would say: \"Slavya overdid a little.\""

    dv "I must admit: I was unfair with you,"

    show sl surprise cos at right    

    dv "You're not sneak, and you're not a coward."

    dv "You didn't steal, but took the blame for it with dignity."

    dv "I also like your trick with juice, it's smart enough,"

    dv "Almost a pity that you left it incompleted."

    dv "I see a character in you. I think you are okay."

    show sl smile cos at right 

    dv "It doesn't mean we're friends or something..."

    dv "I disapprove samaritans like you,"
    dv "Because I disapprove pretending... But whatever."

    show dv guilty cos at left 

    dv "I mean, don't cross my way, don't mess with me,"

    dv "Don't bother with your stuff... And we are even."

    show dv normal cos at left

    dv "And no hard feelings then, all right?"

    sl "Of course. It's very generous of you," 

    sl "It's unexpected that you're able to forgive me."
#    sl "I have not expected you to forgive me."

    dv "I did enough of things like this myself, "

    dv "How could I blame you for this course of action?"

    stop music fadeout 5

    dv "You showed the qualities that I respect."

    window hide 

    $ renpy.pause(1.3)   

    scene bg verona_church

    show mz normal cos

    with dissolve

    $ renpy.pause(0.6)  

    window show 

    me "{i}Father, what news?{/i}"

    me "{i}What is the prince's doom what sorrow craves acquaintance at my hand that I yet know not?{/i}"

    play music music_list["your_bright_side"] fadein 5  

    mz "{i}Too familiar. Is my dear son with such sour company:{/i}"

    mz "{i}I bring thee tidings of the prince's doom.{/i}"

    me "{i}What less than doomsday is the prince's doom?{/i}"

    mz "{i}A gentler judgment vanish'd from his leaps,{/i}"

    mz "{i}Not body's death but body's banishment{/i}"

    me "{i}Ha, banishment? Be merciful, say death.{/i}"

    me "{i}There is no world without Verona walls.{/i}"

    mz "{i}O deadly sin! O rude unthankfulness! {/i}"

    mz "{i}Thy fault our law calls death; but the kind prince turn'd that black word death to banishment. {/i}"

    me "{i}'Tis torture, and not mercy: heaven is here,{/i}"

    me "{i}Where Juliet lives;{/i}"

    me "{i}And every cat, and dog, and little mouse, every unworthy thing{/i}"

    me "{i}Live here in heaven, and may look on her;{/i}"

    me "{i}But Romeo may not.{/i}"

    play sound sfx_knock_door2

    me "{i}Arise; one knocks. Good Romeo, hide thyrself. {/i}"
    window hide

    $ renpy.pause(1.0)
    show un normal cos at right with dissolve
    window show
    un "{i}Where is my lady's lord, where is Romeo?{/i}"

    mz "{i}There on the ground, with his own tears made drunk.{/i}"

    show un sad cos at right    

    un "{i}O, he is even in my mistress's case,{/i}"

    un "{i}Even so lies she. Blubbering and weeping, weeping and blubbering.{/i}"

    me "{i}Spakest thou of Juliet? How is it with her? Doth not she think me an old murderer?{/i}"

    un "{i}O she says nothing, sir, but weeps and weeps.{/i}"

    un "{i}And now falls on the bed, and then starts up.{/i}"

    un "{i}And \"Tybalt \" calls, and then \"Romeo\" cries.{/i}"

    me "{i}As if that name, shot from the deadly level of a gun, did murder her.{/i}"

    show un normal cos at right 

    un "{i}Stand up, stand up.{/i}"

    un "{i}Stand, an you be a man.{/i}"

    show mz bukal cos   

    mz "{i}What, rouse thee, man! Thy Juliet is alive. There art thou happy.{/i}"

    mz "{i}Go, get thee to thy love, as was decreed, ascend her chamber{/i}"

    mz "{i}Hence and comfort her{/i}"

    mz "{i}But, look, thou stay not till the watch be set,{/i}"

    mz "{i}For then thou canst not past to Mantua.{/i}"

    mz "{i}Where thou shalt live till we can find a time{/i}"

    mz "{i}To blaze your marriage, reconsile your friends{/i}"

    mz "{i}Beg pardon of the prince, and call thee back{/i}"

    mz "{i}With twenty hundred thousand times more joy{/i}"

    mz "{i}Than thou went'st fourth in lamentation.{/i}"

    un "{i}My lord, I'll tell my lady you will come.{/i}"

    me "{i}Do so!{/i}"

    me "{i}How well my comfort is reviv'd by this!{/i}"

    mz "{i}Go hence; good night!{/i}"

#

#

    window hide 

    $ renpy.pause(0.8)  

    scene bg ss_bstage

    with dissolve

    window show 

    

    "Romeo ran away. And so I can take a breather"

    "Behind the scene. And here I noticed Lena."

    stop music fadeout 5    

    window hide

    show un normal cos at right with dissolve

    window show

    if d6_treasure >= 1:

        "We are alone for the first time"

        "Since our talk before the dinner."

    if d6_treasure == 0:

        me "Hey... How you doing, nurse?"

        un "I'm fine, fine. More or less..."

        me "What would you say about the duel?"
        me "I mean the first one. Don't you find it strange?"

        un "I didn't see it... Trying to reread my lines"


        un "Every given moment. I can't afford to forget something."

        me "Then I don't want to interrupt... Continue, please."

        jump d6_zhavoronok_eng

    if d6_treasure == 2:

        jump d6_untalkbs_eng

    if d6_treasure == 1:        

        th "Something is wrong. I feel the tension."

        th "Should I ask her?.. I must be very careful."

        menu:

            "Ask":

                jump d6_untalkbs_eng

            "Don't ask":

                th "What bothers her?.. It must be something" 

                th "About that island. Somehow it made her sad."

                th "I should be delicate right now. Inaction is the key"
                th "For staying out of trouble..."
                th "Keeping clear conscience and the purest karma."

#                th "Sooner or later everything will fall into place."

                jump d6_zhavoronok_eng


label d6_untalkbs_eng:

    window hide

    show un normal cos

    with dissolve

    window hide 

    me "Excuse me, Lena... What's the matter?"
    me "I mean... Are you all right?"

    un "I must have a pathetic view, don't I?"

    me "I'd rather call it sympathetic... I like the view, but still,"


    me "Please, answer. Are you fine? Or not?"

    window hide

    $ renpy.pause(1.0)

    show un sad cos with dspr

    window show

    th "Oh, I have bad feelings about being so forward."

    play music music_list["meet_me_there"] fadein 5 

    $ renpy.pause(0.7)  

    un "I'm not fine. {w}I love you."

    "..."

    un "You have to know it... Sorry."
    un "Summer is ending, like our time."

    un "Can't keep my secret anymore. Can't lose you tacitly."

    show un cry_smile cos   with dspr   

    un "I dream that I am Juliet in this play,"

    un "I see myself right on her lawful place."

    window hide 

    show un sad cos with dspr

    $ renpy.pause(0.5)

    window show 

    un "Semyon, I beg you, don't you say a word."

    un "Don't even look at me! Not now."

    un "Please, let me calmly play my part until the end."

    un "And only then I will be free to weep and suffer."

    un "I have a place for it. You'll find me at the pier,"

    un "But only if you want to... meet me there."

    show un cry cos     

    un "I gave you reasons to think bad of me,"

    un "And I can't change it. But I'm not that bad."

    un "I promise you that I will cause no trouble"

    un "If you won't come. Don't worry."

    un "You think this lunatic infront of you may drown herself?"  #Or something worse?"

    un "No, it won't happen. Take my word. "

    un "Come or do not... Do only as your heart desire."

    menu:

        "I will come":

            $ sp_un += 1

            me "As you asked I am ready to be silent."

            me "But still I have one thing to say:"

            show un cry_smile cos           

            me "Please, have no doubt for I will come," 

            me "The peer it is. Wait for me there."

#

            stop music fadeout 3

            window hide

            show un smile cos  with dspr   

            window show         

            me "I hope to find you with a smile like this."
            window hide
            $ renpy.pause(0.5)

            $ d6_unchoice = 3

            jump d6_zhavoronok_eng          

            

        "Say nothing":

            $ sp_un -= 1

            stop music fadeout 4

            th "She asked me to be silent - I am silent,"

            th "There is enough of awkwardness already..."

            $ d6_unchoice = 2

            jump d6_zhavoronok_eng          

            

        "I won't come":

            $ sp_un = 2

            $ d6_unchoice = 1

            me "Don't feed in vain a false hope,  Lena."

            show un surprise cos            

            me "Don't wait me on the pier, I won't come."


            me "Respecting you, respecting all your feelings, I have to answer now:"

            me "There is no love for you in me. Your feelings aren't mutual."

            me "And I'm not offering my friendship in exchange,"

            me "I'm sure it will not help, won't satisfy you."

            show un cry cos             

            me "But I'm a friend still... As a friend I'm asking you:" 

            me "What do you feel - am I allowed to feel the same... but with another?"

            me "Or do you think I still belong to you?"

            me "Please, let me go. Let your beloved one go..."

            me "Let me be free in your mind too. If you are ready."

            window hide

            $ renpy.pause(1.0)

            show un angry2 cos  with dspr       

            $ renpy.pause(0.5)          

            window show

            un "...I understand. However this is... unexpected."

            un "Suit yourself then. I won't bother you."

            un "You have your right, but don't you call yourself beloved"

            un "Cause you are not. Since now, you're hated one!"

            un "Was it your plan?.. Oh, anyway, I thank you for that cruelty!"

            un "You are invisible for me! Do what you want, be free of me,"
            un "Because I am already free! No love!"

            stop music fadeout 4

            th "I hope it's true. Let it be. The cold disgust of the reality"

            th "Will heal her soul and calm her heart... I hope."
            window hide
            $ renpy.pause(0.5)  

            jump d6_zhavoronok_eng          

       


    

label d6_zhavoronok_eng:

    window hide

    $ renpy.pause(0.9)

    scene bg verona_bedroom

    show ss surprise dress 

    with dissolve

    if persistent.dress == 'purple':

        $ persistent.ssg_94 = True 

    else:   

        $ persistent.ssg_95 = True      

    $ renpy.pause(0.7)

    window show 

#    play music Romeo_And_Juliet    fadein 3

    play music music_list["trapped_in_dreams"] fadein 3

    ss "{i}{enen}Wilt thou be gone? It is not yet near day:{/enen}{/i}"


    ss "{i}{enen}It was the nightingale, and not the lark,{/enen}{/i}"

    ss "{i}{enen}That pierced the fearful hollow of thine ear;{/enen}{/i}"

    ss "{i}{enen}Nightly she sings on yon pomegranate-tree:{/enen}{/i}"

    show ss smile2 dress   with dspr   

    ss "{i}{enen}Believe me, love, it was the nightingale.{/enen}"

    me "{i}It was the lark, the herald of the morn,{/i}"

    me "{i}No nightingale. Look, love, what envious streaks,{/i}"

    me "{i}Do lace the severing clouds in yonder east.{/i}"

    me "{i}Night’s candles are burnt out, and jocund day{/i}"

    me "{i}Stands tiptoe on the misty mountain tops.{/i}"

    me "{i}I must be gone and live, or stay and die.{/i}"

    show ss unsure dress   with dspr    

    ss "{i}{enen}Yon light is not day-light, I know it,{/enen} {/i}"

    ss "{i}{enen}Therefore stay yet; thou need'st not to be gone.{/enen}{/i}"

    me "{i}Let me be ta'en. Let me be put to death.{/i}"

    show ss sad dress    

    me "{i}I am content, so thou wilt have it so.{/i}"

    me "{i}We have been drinking tea, talking and eating cookies all night long{/i}"
    me "{i}As decent young people in love are supposed to. So let's continue!{/i}"

    me "{i}Come, death, and welcome! Juliet wills it so.{/i}"
    me "{i}How is't, my soul. Let's talk, - it's not day.{/i}"

    show ss surprise dress    

    ss "{i}{enen}It is, it is: hie hence, be gone, away!{/enen}{/i}"

    ss "{i}{enen}It is the lark that sings so out of tune{/enen}{/i}"

    show ss sad dress   

    me "{i}Farewell, farewell. One kiss, and I’ll descend.{/i}"

    me "{i}Farewell! I will omit no opportunity{/i}"

    me "{i}That may convey my greetings, love, to thee.{/i}"

    show ss surprise dress  

    ss "{i}{enen}O think'st thou we shall ever meet again?{/enen}{/i}"

    me "{i}I doubt it not, and all these woes shall serve.{/i}"

    me "{i}For sweet discourses in our time to come.{/i}"

    me "{i}Adieu, adieu!{/i}"    

    window hide

    $ renpy.pause(1.0)

    show unl normal at fright

    show ss nosmile dress   

    ssunl "{i}Ho, daughter, are you up?{/i}"

    ssunl "{i}Well, well, thou hast a careful father, child.{/i}"

    ssunl "{i}One who, to put thee from thy heaviness,{/i}"

    ssunl "{i}Hath sorted out a sudden day of joy {/i}"

    ssunl "{i}That thou expect’st not, nor I looked not for.{/i}"

    show ss surprise dress    

    ss "{i}{enen}In happy time, what day is that?{/enen}{/i}"    

    ssunl "{i}Marry, my child, early next Thursday morn,{/i}"

    ssunl "{i}The gallant, young, and noble gentleman,{/i}"

    show ss angry dress with dspr

    ssunl "{i}The County Paris, at Saint Peter’s Church,{/i}"

    ssunl "{i}Shall happily make thee there a joyful bride.{/i}"

    show ss vangry dress    

    ss "{i}{enen}Now, by Saint Peter's Church and Peter too,{/enen}{/i}"

    ss "{i}{enen}He shall not make me there a joyful bride.{/enen}{/i}"

    show unl upset at fright 

    show ss surprise dress  

    ss "{i}{enen}I wonder at this haste; that I must wed{/enen}{/i}"

    ss "{i}{enen}Ere he, that should be husband, comes to woo.{/enen}{/i}"

    ss "{i}{enen}I pray you, listen me! I will not marry yet.{/enen}{/i}"

    show ss serious dress with dspr     

    ssunl "{i}How, will you none? Dost thou not give us thanks? {/i}"

    ssunl "{i}Is she not proud? Dost thou not count your blessed, unworthy as you are{/i}"
    ssunl "{i}That we have wrought so worthy a gentleman to be her bride?{/i}"

    show ss unsure dress with dspr     

    ss "{i}{enen}Not proud, you have; but thankful, that you have:{/enen}{/i}"

    ss "{i}{enen}Proud can I never be of what I hate;{/enen}{/i}"

    ss "{i}{enen}But thankful even for hate, that is meant love.{/enen}{/i}"        

    ssunl "{i}How, how, how, how? Chopped logic! What is this?{/i}"

    show ss surprise dress      

    ssunl "{i}\"Proud\", and \"I thank you\", and \"I thank you not\","
    ssunl "{i}And yet \"not proud\"? Mistress minion you,"
    ssunl "{i}Thank me no thankings, nor proud me no prouds,{/i}"

    ssunl "{i}But fettle your fine joints 'gainst Thursday next{/i}"

    ssunl "{i}To go with Paris to Saint Peter’s Church,{/i}"

    ssunl "{i}Or I will drag thee on a hurdle thither.{/i}"

    show ss cry dress   

    ss "{i}{enen}Good father, I beseech you on my knees,{/enen}{/i}"

    ss "{i}{enen}Hear me with patience but to speak a word.{/enen}{/i}"

    ssunl "{i}Speak not. Reply not. Do not answer me!{/i}"    

    ssunl "{i}I tell thee what: get thee to church o' Thursday,{/i}"

    ssunl "{i}Or never after look me in the face.{/i}"

    show ss cry2 dress  

    ssunl "{i}Thursday is near. Lay hand on heart, advise.{/i}"

    ssunl "{i}An you be mine, I’ll give you to my friend.{/i}"

    stop music fadeout 5    

    ssunl "{i}An you be not, hang, beg, starve, die in the streets,{/i}"

    ssunl "{i}For, by my soul, I’ll ne'er acknowledge thee, nor what is mine shall never do thee good.{/i}"

    window hide

    $ renpy.pause(1.2)  

    scene bg ss_bstage

    with dissolve

    window show     

    dv "Hey, hey! You, Romeo! Come here."

    play music music_list["gentle_predator"] fadein 3   

    window hide

    show dv normal cos with dissolve

    window show

    

    dv "The play is almost done, so is this summertime"
    dv "And many other things... Let's celebrate it?"

    dv "We'll throw a party in my house. You're invited."

    dv "Come when the play is over. Will you?"

    me "Sounds good. Who's coming else?"

    dv "Who else? No kids, of course..."

    dv "That means Ulyana's out. She's going to her friends,"

    dv "Slavya will come though."

    me "Wow, really? A very unexpected guest."
    dv "We aren't enemies, you know... Not anymore."

    show dv guilty cos  

    dv "I'm sure of Slavya but not sure about the others..."

    dv "Who else can we invite? I am confused."

    dv "Do you have any propositions? If you are joining us at all."

    show dv normal cos  

    menu:

        "I won't come":

            $ d6_dvchoice = 1

            $ sp_dv -= 1

            me "I'm sorry, I can't join you. It's a pity."

            me "I have another plans this evening. No offense."

            dv "I see, I see... Damn you, Semyon!"
            dv "And damn me too! I had to ask you first."

            dv "Fine, busy boy. Be lucky with your {i}plans{/i},"
            dv "The gossips will bring news about your doings."

            me "Don't hold a grudge. I wish you a good party."
            me "I think you, girls, will have a lot of fun."

            show dv guilty cos          

            dv "Do you believe that, really? Well, whatever..."

            dv "At least there is no need to kick Ulyana out, not anymore."

            dv "This mad squirrel must be so thankful to you."

            show dv normal cos

            dv "You will regret that you chose something over us,"

            dv "There is no party to compete with ours, that's for sure."

            

            

        "I haven't decided yet":

            $ d6_dvchoice = 2

            me "Thank you for calling me, but I'm not sure if I can make it..."

            me "Also I don't know who else to invite."

            dv "You better come, Semyon. It will be boring without you."

            dv "Infact it may be boring with you too, but without you - awfully boring!"

            me "Maybe I'll come. But I cannot promise."

            me "I'm busy almost every evening, thanks to Olga."

            

        "Lena":

            $ sp_un += 1

            $ sp_dv -= 1

            $ d6_dvchoice = 2

            dv "Don't waste you time - she won't come anyway."

#            dv "I would invite her myself otherwise. "

            dv "I called her a few times before,"

            dv "She's totally not in the mood this summer."

            dv "Hey, if you want a free advice:"

            dv "If you are going to the party,"
            dv "Don't even think about telling Lena."

            
        "Samantha":

            $ sp_ss += 1

            show dv angry cos       

            dv "You are an idiot, you know it?"

            dv "Can't you leave her just for one evening?"

            dv "I can't believe it! What a wuss!"

            me "Calm dawn. What do you have against Samantha?"

            me "She's not a chield, she's mature for her age..." 

            me "Of course strong beverages are out of question,"

            me "But I believe we'll manage without them."

            show dv guilty cos  

            dv "Come on, we're having Slavya there already."
            dv "Me, you and her are different enough to make it awkward,"


            dv "And everyone feels awkward near Samantha..."
            dv "We can't invite a person from another planet."



            dv "What do you want? To throw the lamest party ever?"

            show dv normal cos          

            dv "We might as well call Olga Dmitrievna then."

            menu:

                "I will come alone":

                    $ d6_dvchoice = 3

                    me "Fine, deal... Then I will come alone."

                    dv "But do we actually need such a daydreamer by the way?"

                    show dv laugh cos

                    dv "Relax, just kidding. We will wait for you, señor."

                    dv "We need at least one boy for the diversity."

                    

                "I won't come":

                    $ d6_dvchoice = 1

                    $ sp_dv -= 2

                    $ sp_ss += 1

                    me "Then I intend to skip your bender."

                    me "I won't lose anything if I will do so."

                    show dv angry cos   

                    dv "It's you the one who's talking about bender!"
                    dv "Alright, alright! Go, keep on drinking milk" 

                    dv "And get yourself a diaper, mommy's boy!"

                    th "Huh, mommy's boy? No problem. Thank you."

                    th "Since I chose Sammy over her and Slavya,"
                    th "I'm glad she didn't call me something worse."

                    

        "I need nobody else":

            $ d6_dvchoice = 3

            $ sp_dv += 1

            me "Can't ask for more. The company is already perfect," 

            me "And I am dying of excitement!"

            me "I swear that I will come. I swear it by my sword!"


            show dv laugh cos

            dv "Enough of swords! They make me nervious today,"

            dv "So you may throw your stick away,"

#            dv "Bring no sharp items to my house,"
            dv "From now - no fencing! Only fair fistfights!"



    window hide

    stop music fadeout 3

    $ renpy.pause(1.0)  

    scene bg verona_church

    show ss unsure dress

    show mz normal cos at right

    with dissolve

    $ renpy.pause(0.5)  

    window show                 

    mz "{i}O Juliet, I already know thy grief.{/i}"

    mz "{i}It strains me past the compass of my wits.{/i}"

    play music music_list["just_think"] fadein 5    

    mz "{i}I hear thou must, and nothing may prorogue it,{/i}"

    mz "{i}On Thursday next be married to this county.{/i}"

    ss "{i}{enen}Tell me not, friar, that thou hear'st of this,{/enen}{/i}"

    ss "{i}{enen}Unless thou tell me how I may prevent it:{/enen}{/i}"    

    ss "{i}{enen}If, in thy wisdom, thou canst give no help,{/enen}{/i}"

    ss "{i}{enen}Then with this knife I'll end my life.{/enen}{/i}"

    show ss sad dress   

    show mz bukal cos at right  

    mz "{i}If, rather than to marry County Paris,{/i}"

    mz "{i}Thou hast the strength of will to slay thyself,{/i}"

    mz "{i}Then is it likely thou wilt undertake{/i}"
    mz "{i}A thing like death to chide away this shame,{/i}"

    show ss surprise dress      

    mz "{i}That copest with death himself to ’scape from it.{/i}"
    mz "{i}An if thou darest, I’ll give thee remedy.{/i}"

    show mz smile cos at right  

    mz "{i}Sell, to be quite honest and correct. For a fair coin.{/i}"

    mz "{i}Don't think that I am trying to fill my pockets,{/i}"

    mz "{i}I don't have pockets, as you see... But I need money.{/i}"

    mz "{i}Bat's excrement and mouse tails... All components are quite expensive.{/i}"

    show mz bukal cos at right  

    mz "{i}High price means quality, you know. So far there were no complaints.{/i}"

    $ renpy.pause(0.5)  

    mz "{i}I'm not quite sure though... Perhaps those fools never woke up.{/i}"

    show ss serious dress   with dspr

    ss "{i}{enen}I will do anything without fear or doubt{/enen}{/i}"

    ss "{i}{enen}To live an unstain'd wife to my sweet love{/enen}{/i}"

    show mz normal cos at right 

    mz "{i}Hold, then. Go home, be merry. {/i}"

    mz "{i}Take thou this vial, being then in bed,{/i}"

    mz "{i}And this distillèd liquor drink thou off,{/i}"

    mz "{i}When presently through all thy veins shall run{/i}"

    mz "{i}A cold and drowsy humor, for no pulse{/i}"

    mz "{i}Shall keep his native progress, but surcease,{/i}"

    mz "{i}No warmth, no breath shall testify thou livest.{/i}"

    mz "{i}Now, when the bridegroom in the morning comes{/i}"

    mz "{i}To rouse thee from thy bed, there art thou dead.{/i}"

    mz "{i}Thou shalt be borne to that same ancient vault where all the kindred of the Capulets lie.{/i}"

    mz "{i}In the meantime, against thou shalt awake,{/i}"
    mz "{i}Shall Romeo by my letters know our drift,{/i}"

    mz "{i}And hither shall he come, and he and I{/i}"
    mz "{i}Will watch thy waking, and that very night{/i}"

    mz "{i}Shall Romeo bear thee hence to Mantua.{/i}"

    show ss surprise dress   with dspr   

    ss "{i}{enen}Give me, give me! O, tell not me of fear!{/enen}{/i}"

    show ss nosmile dress    

    mz "{i}Hold. Get you gone. Be strong and prosperous{/i}"

    mz "{i}In this resolve. I’ll send a friar with speed{/i}"

    mz "{i}To Mantua with my letters to thy lord.{/i}"

    mz "{i}...This guy, a friar, has to be paid too, by the way.{/i}"

    stop music fadeout 3   

    ss "{i}{enen}Love give me strength! And strength shall help afford.{/enen}{/i}"

    window hide 

    $ renpy.pause(1.0)      

    scene bg verona_bedroom

    show un normal cos 

    with fade

    window show

    play music music_list["you_lost_me"] fadein 4   

    un "{i}Mistress! What, mistress! Juliet!{/i}"

    un "{i}Why, lamb! Why, lady! Fie, you slug-a-bed.{/i}"

    un "{i}Ay, let the county take you in your bed.{/i}"

    un "{i}He’ll fright you up, i' faith. Will it not be?{/i}"

    un "{i}I must needs wake you. Lady, lady, lady!{/i}"

    show un surprise cos    

    $ renpy.pause(0.5)      

    un "{i}Alas, alas! Help, help! My lady’s dead!{/i}"

    un "{i}Some aqua vitae, ho! My lord! My lady!{/i}"

    show unl upset at fright 

    show un sad cos 

    ssunl "{i}What noise is here?{/i}"

    un "{i}O lamentable day!{/i}"

    show unl sad at fright   


    ssunl "{i}O me, O me! My child, my only life,{/i}"

    ssunl "{i}She’s cold. Her blood is settled, and her joints are stiff.{/i}"


    ssunl "{i}Life and these lips have long been separated.{/i}"


    ssunl "{i}Death, that hath ta'en her hence to make me wail,{/i}"


    ssunl "{i}Ties up my tongue and will not let me speak.{/i}"

    show mz smile cos at left   

    mz "{i}Come, is the bride ready to go to church?{/i}"


    ssunl "{i}Ready to go, but never to return.{/i}"

    ssunl "{i}Death is my son-in-law. Death is my heir. My daughter he hath wedded. {/i}"

    show mz normal cos  

    un "{i}Most lamentable day, most woeful day{/i}"
    un "{i}That ever, ever, I did yet behold!{/i}"

    un "{i}O woeful day, O woeful day!{/i}"

    ssunl "{i}O child, O child! My soul, and not my child!{/i}"


    ssunl "{i}Dead art thou! Alack, my child is dead, and with my child my joys are buried!{/i}"

    show mz bukal cos   

    mz "{i}Peace, ho, for shame! Confusion’s cure lives not{/i}"
    mz "{i}In these confusions. Heaven and yourself{/i}"

    mz "{i}Had part in this fair maid; now heaven hath all,{/i}"
    mz "{i}And all the better is it for the maid.{/i}"
    mz "{i}Oh, in this love, you love your child so ill{/i}"
    mz "{i}That you run mad, seeing that she is well.{/i}"
    mz "{i}She’s not well married that lives married long,{/i}"
    mz "{i}But she’s best married that dies married young.{/i}"

    show unl upset at fright     

    ssunl "{i}Shut up now! What an ugly thing to say!{/i}"

    show mz shy cos 

    mz "{i}Excuse me... It was odd enough, I guess,{/i}" 

    mz "{i}It is my duty to pretend all-knowing{/i}"
    mz "{i}And find a meaning for you in this matter,{/i}"

    mz "{i}But if it's sensitive for you... Forget the meanings.{/i}"
    mz "{i}Let us just do what must be done,{/i}"

    show mz normal cos 

    show unl sad at fright   

    mz "{i}Dry up your tears and stick your rosemary{/i}"
    mz "{i}On this fair corse, and, as the custom is,{/i}"

    mz "{i}And in her best array, bear her to church.{/i}"

    mz "{i}Sir, go you in, and, madam, go with him;{/i}"

    mz "{i}And go, Sir Paris. Every one prepare{/i}"

    stop music fadeout 3    

    mz "{i}To follow this fair corse unto her grave.{/i}"

    window hide

    $ renpy.pause(0.5)  

    window show

    "I have been watching others play and waiting for my turn,"

    "But was distracted by Slavyana - she approached me."  

    window hide

    scene bg ext_stage_normal_day

    show sl normal cos

    with dissolve 

    window show

    "I noticed that her costume became wet -"

    "She obviously tried to wash the juice off,"

    "But the attept was unsuccessful. So I smiled about it."

    sl "Did you hear Zhenya? There is something wrong"

    sl "About her text. She's adding something from herself."

    me "I hardly listen her... I have another question for you:"

    me "You are awaited at Alisa's party, aren't you?"
    me "I see you know about this... What's the deal?"

    sl "Yeah, it seems awkward... You must be thinking of some weird caprice,"
#You must see some sort of awkwardness in such a caprice, don't you?"

    

    sl "I know. I'm always sceptic for late parties too,"

    sl "Never attend them... And perhaps I woudn't even for a friend,"

    sl "But I must do it for a foe, you see?"

    sl "I can't refuse an invitation from Alisa," 

    sl "If she wants peace - how can turn my back on her?"

    sl "There's no illusions: she's a walking trouble still,"
    sl "But I don't want her to be worse because of me,"
    sl "And if there's something good in her - I won't deny it,"
    sl "Our duty is to answer to it... Let it grow."

    me "Very admirable... You're right, and I don't really want to interfere,"

    me "But do you trust her? Don't you see a risk in it?"

    show sl surprise cos  

    sl "No, I have no suspicions. Why?"

    me "The two of you had too much fighting that I've witnessed,"

    me "So now I'm struggling to believe in this reconciliation..."

    me "I'm sorry to be doubting Thomas here who spoils the mood."

    show sl smile cos

    sl "Nothing is spoiled. We won't fight anymore, I'm telling you."

    sl "No games, no tricks. We're both already sick of them."
    sl "I know it for myself... And, yes, I trust Alisa on this matter too. "

    if d6_dvchoice == 1:

        sl "Alisa said that you won't come. Why so?"

        sl "Don't you have time for us this evening?"

        me "While Olga's sitting on my ears? No, Slavya, I'm afraid I don't." 

        me "A hope that she will let me go is very vague."

        sl "She's forcing you that much to stay with her?"

        me "No, not with her exactly... Her main idea is to keep me busy"
        me "Or keep me locked. Or both. {w}She, actually, likes to be outside"
        me "As long as I am on a leash."
#Of course not. But I'm sure she is going to make me overloaded with amazing activities."

#        sl "In the evening? I'm not that naive. "

#        sl "Sure, easy for you to say - you don't live with her."

    

    me "Hey, by the way... Late party of this kind..."
#What do you think about strong beverages?" 


    show sl serious cos

    me "It might involve some booze. What to you think?"

    window hide

    $ renpy.pause(1.0)  

    me "There is a chance, right? Are you expecting some strong beverages?"

    sl "My evening won't be spoiled by alcohol, don't worry,"

    sl "I don't drink nothing stronger than a tea."

    sl "And if somebody wants to... Let them be, I guess,"

    sl "Yet I won't stand a drunk, not for a single second." 

    sl "If something happens - I will leave that place forever." 

    window hide

    $ renpy.pause(1.0)
  
    window show
    me "Why the delay? When the act V begins?"
    show sl normal cos 
    sl "There's nothing wrong. It's some expected intermission."

    play music music_list["eternal_longing"] fadein 4   

    me "And we must wait here on these chairs while nothing happens."
    me "They must continue soon... before somebody dies of boredom."
    "I stretched my arms and yawned pretty loud."

    th "If only I could have a small shut-eye now..."


    th "But it's impossible.  Can't skip my turn to act."

    "Meanwhile Slavyana kept on talking near of me."

    show sl smile cos

    sl "I hope you will find time to visit us,"

    sl "Have some respect for our invitation!"

    me "I do, I do... But I have not a faint idea"
    me "What to expect from you, girls, there."


    sl "Don't you expect much, that will do,"
    sl "I see no reason to expect bad things there..."

    me "And what about the good things?"

    window hide

    show sl surprise cos with dissolve

    window show 

    sl "I'm sorry, what are you implying here?"

    me "Huh? Me - implying?  God forbid."

    window hide

    show sl smile cos with dissolve

    window show 

    sl "Okay, I'll do it... maybe. But don't ask again!"

    me "What are we talking about this time?.."

    sl "I'm talking about snacks from the canteen."

    sl "I have the key so I can take them freely,"  

    sl "And I suppose it's a main reason to invite me,"



    sl "Alisa surely is craving for that key..."

    sl "Whatever, huh? I'm a bad girl today. Let's do it!"
    sl "I'm into all sorts of bad things today!"

    window hide

    show sl surprise cos with dspr

    $ renpy.pause(0.7)  

    window show 

    sl "...Why are you blushing so? What's wrong?"
    me "Some of your words are funny... No, it's nothing."

    window hide

    $ renpy.pause(1.0)  

    show sl scared cos with dspr

    window show     

    sl "No way, Semyon... What are you thinking... Shame on you!"


    sl "Your thoughts are shocking me! How could you even..." 




    sl "{b}That{/b} kind of bad things, really?.."

    sl "I'll never do it, no... Alisa will be there!"

    window hide

    show sl angry cos with dspr

    window show     

    sl "I won't do anything before the wedding!"

    sl "I won't do anything in front of her!"

    sl "...I won't do anything behind her either!"

    window hide

    show sl tender cos with dspr

    window show     

    sl "What's wrong with me... I'm not myself today,"
    sl "To be hysterical is not a pioneer's way."

    me "I am confused... What did you say about?" 

    me "You won't do... what? You won't... make out?"

    show sl happy cos with dspr

    window show     

    sl "That's right, ha-ha! It's clear as skies above us!"

    sl "You are so pleasantly perceptive..."

    th "What's going on?.. Can someone tell me?"

    stop music fadeout 3    

    th "She shouldn't say such things... She never does."

    window hide 

    show sl serious cos with dspr

    $ renpy.pause(1.0)  

    window hide 

    play music music_list["into_the_unknown"]   

    sl "Alright, Semyon. I'll share with you a secret,"
    sl "Prepare to hear now something horrible."

    sl "It is the game that started a long time ago."
    sl "You have your own sad part in it,"
    sl "It's all about you... or at least around you."

    sl "It's a conspiracy from Lena and Alisa."
    sl "They've found an empty ticket on a road."
    sl "I'm speaking about \"Sportloto\". The lottery."

    sl "They've filled the ticket and then send it there,"

    sl "The deal was to split any prize between them..."

    sl "What do you think? They won a car! Brand new \"Volga\"!"

    sl "The only problem now is how to split the prize."
    sl "Their first idea was to sell the car,"
    sl "But it is troublesome for them - they doesn't trust each other," 

    sl "So they decided to tempt fate once more,"
    sl "To make a bet about you. Who will win?"
    sl "Whoever take your heart and spend a night with you,"


    sl "Can also take the \"Volga\". That's the deal."

    sl "There's one condition: it must happen not at any moment,"

    sl "But at the time after the play. Today."

    sl "It is a good condition I must say,"

    show sl shy cos with dspr   

    sl "It would be easy otherwise... Poor sportsmanship."

    sl "Whoever comes to you first - here's a winner!"


    sl "No offence, friend, but they are good in it,"
    sl "To lure you into bed - a piece of cake for them,"

    sl "Yet they can't make such a bold move before the play,"

    sl "It will be counted as a loss, of course."

    show sl serious cos with dspr   

    sl "All traps are set, all pawns are sacrificed."

    sl "The plotters did their parts. They're waiting for your move."

    sl "Alisa needs me at her party. I'm her cover."
    sl "It would be too straightforward otherwise -"
    sl "To call you at her place alone."
    sl "So she invited me as well. And I agreed..."
    sl "But not to help her! Quite the opposite."


    sl "My duty is to break this unfair game."

    sl "Hey, by the way: has Lena called you for a date?"

    window hide

    $ renpy.pause(0.5)  

    show sl surprise cos with dspr  

    window show

    sl "Well-well, calm down, friend. Take it easy."

    sl "This news for you are overwhelming, I imagine,"

    sl "But you should have expected this deep in your heart..."
    sl "Or something like this... Haven't you?"
    sl "You suddenly became so popular in here,"

    sl "So many girls around you...  With what wishful thinking"

    sl "Could you explain this fact? Tell me, Semyon."

    show sl smile2 cos with dspr        

    sl "What womanizer are you?.. That's a laugh!"
    sl "You are much closer to a wally, face the truth."

    me "So many words... And too much for the truth!"
    me "What about you? Why were you friendly with a wally?"
    me "You also were around me all the time! Not less than Lena or Alisa."

    me "How can it be if I am so unworthy?"

    window hide

    $ renpy.pause(1.0)  

    show sl sad cos with dspr

    window show     

    sl "You want to know it? Sure, there is an explanation..."
    sl "Hold on, I'll tell you. It will be a big confession"


    sl "With a few words: {w}I also joined the bet."
    sl "I am a part of it... and I'm not proud of it."

    me "I am amazed. What is the final count then?"
    me "Is it the game for three or is there someone else?"

    show sl normal cos with dspr    

    sl "There is nobody else. But I can see what do you mean,"
    sl "The fight for your attention made you famous,"


    sl "So, now you are the most desired guy around."

    sl "Still, there are only three of us who bet."

    menu:

        "I don't believe it!":

            show sl surprise cos

            me "All what you say is not true! Can you stop?"
            me "It's just a stupid joke - admit it!"

            me "They love me in this camp! It is for real!"
            me "I am well-liked! And I am well-respected!"

            me "But... Something's wrong. It feels like a wrong place,"
            me "You are wrong Slavya, not the real one. Put me back!"


            me "I want to leave the show... I must wake up!"        

            window hide 

            $ renpy.pause(0.5)          

            scene black with fade

            $ renpy.pause(1)

            stop music fadeout 3


            scene bg ext_stage_normal_day 

            show sl normal cos
            show unblink
            $ renpy.pause (1)
            window show                

            jump ssd6_wakeup_eng

            

        "You can win":

            me "You're not as greedy as your opponents, I see."
            me "Thank you for that. Thanks for the honesty..."

            me "The truth is ugly. It is hard for me"
            me "To find a bright side in this situation..."

            me "But here are you. Good as you are."
            me "The fairest girl, I always liked you,"

            me "Perhaps not mutually... but it's not the point."

            me "I want to offer you the secret deal:"

            me "You win. They lose. It's over."

            window hide

            show sl angry cos with dspr 

            $ renpy.pause(1.0)  

            window show             

            sl "I didn't want this nasty bet at the first place!"

            sl "They made me join it! By the blackmail and so on..."

            sl "I didn't try to win, you know it,"

            sl "You better leave your lusty hope,"

            sl "I'm not competing for the car and never did."

            sl "One night for \"Volga\"? Is that what you're saying?"
            sl "I have a pride and I'm not that naive!"

            sl "If I will satisfy you, it won't end there;"


            sl "In any time you can denounce our secret,"



            sl "I will be vulnerable to blackmail again."


            if sp_sl >= 8:

                stop music fadeout 5            

            sl "You will not have that power over me!"

            me "Wait... Me blackmailing you?.. I won't do that!"

            sl "Will I do {i}that{/i}? How do you think?"

            window hide

            show sl sad cos with dspr           

            $ renpy.pause(1.0)

            if sp_sl >= 8:

                jump d6_gooddream_eng

                        

            else:

                window show

                me "Please, never mind. What can we do then?"

                sl "I'll tell them that I won. You will approve that."

                sl "But you will never ever speak about that anymore."

                sl "You'll never mention that with anyone, including me." 

                sl "As for the car, I'll throw away the ticket."

                sl "This shameful bet... I will destroy all proofs of it." 

                sl "I hope my conscience will be silent afterwards."

#

                me "What for should I deny the other girls?" 

                me "I still have a strong chance with one of them."

                me "Or I can visit even both in sequence if I'm lucky,"

                me "I'll hit a jackpot then! Let's hope I will be fast enough..."

                show sl smile cos

                sl "You have no right to think about such a nonsense,"

                sl "While you have chance to serve your only true goddess!"

                window hide                  

                stop music fadeout 4

                $ renpy.pause(1)                   

                scene black with fade

                $ renpy.pause(1)



                scene bg ext_stage_normal_day 

                show sl normal cos
                show unblink
                $ renpy.pause (1)
                window show                

                jump ssd6_wakeup_eng                

    

        "You lost the bet":

            me "That's right like it happens in comedy movies,"

            me "A ladies' man seduces a shy girl - "
            me "Just out of fun or, more usually, on a bet..."

            me "But then he really falls in love with her,"

            me "And she forgives him. Happy end. I think it's possible sometimes..."

            me "But not this time. And not with you."
#I'm afraid you are the side character not only in the play but in this movie too."

            window hide

            show sl angry cos with dspr 

            $ renpy.pause(0.5)  

            window show             

            sl "Fine! Who of them will you forgive?"

            sl "Wait, I don't care! Good luck with your forgiven ones!"

            sl "This whole chat was about easing my own conscience."

            stop music fadeout 3

            window hide 

            $ renpy.pause(1)            

            scene black with fade

            $ renpy.pause(1)



            scene bg ext_stage_normal_day 

            show sl normal cos
            show unblink
            $ renpy.pause (1)
            window show                

            jump ssd6_wakeup_eng   

                

label d6_gooddream_eng:             

            window show

            play music music_list["confession_oboe"] fadein 5           

            me "I mustn't offer you such thing, I'm very sorry."

            me "Dear Slavya, I have never... loved so strongly,"

            show sl surprise cos       

            me "The possibility to stay with you in private"


            me "Has clouded my poor mind and tricked it..."

            me "Forgive me this indecent offer, if you can,"


            me "You are nothing like that. You're different."

            show sl normal cos         

            me "You're also not the one I used to know,"
            me "But I don't care anymore for what you are,"
            me "Be any what you wish: an angel or an imp!"

            me "If you're a witch, people must burn me for worshiping you!"

            me "If you are Devil - I will gladly go to Hell!"

            me "But you're not that, you're good... No! You're much better!"



            me "The \"good\" word is not good enough for you,"

            me "Your name should be the good-defining word instead,"
            me "Yes,  \"Slavya\" is more suitable for me,"

            me "Because it means the ideal! Perfection!"
            me "How can I serve to this perfection? Tell me..."

            show sl surprise cos    

            sl "Oh, you are sneaky creepy sycophant... I'm so confused."

            sl "Your words are so improper and annoying..."

#

            window hide

            $ renpy.pause(0.5)          

            show sl shy cos with dspr   

            window show         

            sl "However... they are touching me somehow."
            sl "They warmth my heart, as weird and cringy as they are."
            sl "Sometimes even a little bit or warmth is good,"
            sl "After your words I'm melting like a butter in an oven..."
            sl "It's strange but cute... I don't know what to do."


            sl "It's so relieving to hear something truthful..." 
#            sl "Forgive me, I was cruel to you. It's unfair." 

            sl "I'm tired of living in this circle of intrigue,"


            sl "And with your help there is a chance of breaking free."



            sl "I want to cut stop the lies, to cut these ferres,"

            sl "And I... succumb to your gallant charm."

            window hide

            $ renpy.pause(0.5)          

            show sl tender cos with dspr

            window show         

            sl "I want to have you by my side, yes! I'm with you!"

            me "Oh, Slavya, oh, my dearest! I'm so happy!"

###        "The wave of warmth and caress took me away."

###        sl "Wake up, Semyon! You sleep your pants!"

            window hide

            $ renpy.pause(1.0)          



            scene black with fade

            $ renpy.pause(1)



            scene bg ext_stage_normal_day 

            show sl normal cos
            show unblink
            $ renpy.pause (2)
            window show             

            th "Oh, what an ugly hoax! It was a dream..."

            th "Could I get back to this warm stream?"
            th "The image in my mind is faint, dissolving fast..."

            th "My memory of better place won't last."

            th "I will forget even the best part - my own Slavya,"

            th "Who I knew better than the real one."

            th "Admittedly, this one's not bad at all,"

            th "But we've been only friends, as I recall."

            th "Hard to compare with Slavya from the dream,"
            th "It was like we had shared a century of love."

            th "What words did I tell her back then?"

            th "Oh fool, I won't remember them again."

            th "Without the words, which I'm unable to repeat,"

            th "Our hearts won't ever have a chance to meet."


            th "But anyway, when we are leaving camp,"

            th "I'll dare to whisper this, if not to say out loud:"

            me "We were in love, my dearest soul,"

            me "But both somehow forgot it all."

            show sl surprise cos with dspr          

            th "Wait, did I... utter it just now?"

            stop music fadeout 4

            th "Will slumber be a good excuse?"

            sl "I don't recall these lines... But add them to the play."

            show sl normal cos with dspr            

            sl "Rehearsal time is up. Please go without delay!"

            window hide

            $ renpy.pause(1)            

            jump ssd6_bru_eng

            

            

            

         

    

label ssd6_wakeup_eng:          

    sl "Hey, rise and shine! You are the star, Semyon."

    sl "They're waiting on the stage. Come on!"

    me "Was it a dream I saw, and nothing more?"

    me "That lottery and craziness galore?"

    me "We didn't have that conversation here?"

    "She shook her head. Oh, now it's loud and clear."

    window hide 

    $ renpy.pause(1.0)   

    

label ssd6_bru_eng:

    scene bg verona_square

    show sh upset pioneer 

    with dissolve

    $ renpy.pause(0.5)

    play music music_list["goodbye_home_shores"] fadein 4   

    me "{i}News from Verona! How now, Benvolio?{/i}"

    me "{i}Dost thou not bring me letters from the friar?{/i}"

    me "{i}How doth my lady? Is my father well?{/i}"

    me "{i}How fares my Juliet? That I ask again,{/i}"

    me "{i}For nothing can be ill if she be well.{/i}"

    show sh serious pioneer     

    sh "{i}Then she is well, and nothing can be ill.{/i}"

    sh "{i}Her body sleeps in Capels' monument,{/i}"

    sh "{i}And her immortal part with angels lives.{/i}"

    sh "{i}I saw her laid low in her kindred’s vault.{/i}"

    sh "{i}And presently took post to tell it you.{/i}"

    sh "{i}O, pardon me for bringing these ill news.{/i}"

    me "{i}Is it e'en so? Then I defy you, stars!..{/i}"

    me "{i}Thou know’st my lodging. Get me ink and paper,{/i}"

    me "{i}And hire post horses. I will hence tonight.{/i}"

    window hide

    hide sh with dspr

    $ renpy.pause(0.5)  

    window show

    me "{i}Well, Juliet, I will lie with thee tonight.{/i}"

    me "{i}Let’s see for means.{/i}"

    me "{i}I do remember an apothecary{/i}"

    me "{i}Such mortal drugs he has, but Mantua’s law{/i}"
    me "{i}Is death to any he that utters them.{/i}"

    me "{i}I will pay him gold. The gold, worse poison to men’s souls,{/i}"
    me "{i}Doing more murder in this loathsome world,{/i}"
    me "{i}Than these poor compounds that he mayst not sell.{/i}"

    me "{i}I sell him poison. He sell me none.{/i}"
    me "{i}Come, cordial and not poison, go with me{/i}"

    me "{i}To Juliet’s grave, for there must I use thee.{/i}"

#

    window hide

    $ renpy.pause(0.5)      

    scene bg verona_church

    show mz normal cos

    with fade

    $ renpy.pause(0.5) 

    window show     

    mz "{i}I have got some news. My letter to Romeo is returned{/i}"

    mz "{i}The monk I'd sent, while on his way,{/i}"

    mz "{i}Converted to Islam in want of Heaven.{/i}"

    mz "{i}He's fighting for the Holy Sepulcher{/i}"

    mz "{i}In old Jerusalem, that rotten city,{/i}"

    mz "{i}And killing the good folk, the likes of us.{/i}"

    mz "{i}In short, that traitor is no longer running errands.{/i}"

    mz "{i}The scoundrel! I won't give recommendations{/i}"

    mz "{i}For such a lousy job. What wasted effort -{/i}"

    mz "{i}I saved him in that dirty little tavern{/i}"

    mz "{i}Where I first met him. 'Twas a fateful day:{/i}"

    mz "{i}His matzo almost made him choke to death.{/i}"

    window hide 

    show mz bukal cos with dspr

    $ renpy.pause(0.5)  

    window show     

#    mz "{i}О чём я сам с собой тут говорил? Ах, да.{/i}"

    mz "{i}And the neglecting it may do much danger.{/i}"

    mz "{i}Now must I to the monument alone.{/i}"

    mz "{i}Within this three hours will fair Juliet wake.{/i}"

    mz "{i}She will beshrew me much that Romeo{/i}"
    mz "{i}Hath had no notice of these accidents.{/i}"

    mz "{i}But I will write again to Mantua,{/i}"
    mz "{i}And keep her at my cell till Romeo come.{/i}"

    window hide 

    $ renpy.pause(0.8)  

    scene bg verona_garden

    show sh normal pioneer

    with fade

    $ renpy.pause(0.5)  
    window show 
    me "{i}Give me that mattock and the wrenching iron.{/i}"
    me "{i}Hold, take this letter. Early in the morning{/i}"

    me "{i}See thou deliver it to my lord and father.{/i}"

    stop music fadeout 5    

    me "{i}Whate'er thou hear’st or seest, stand all aloof,{/i}"

    me "{i}And do not interrupt me in my course.{/i}"

    sh "{i}I will be gone and not trouble you.{/i}"

    window hide 

    $ renpy.pause(0.5) 

    

label ss_playtomb_eng:  

    scene bg verona_tomb

    with fade

    window show

    play music Romeo_And_Juliet fadein 3

    me "{i}O my love, my wife!{/i}"


    me "{i}Death, that hath sucked the honey of thy breath,{/i}"


    me "{i}Hath had no power yet upon thy beauty.{/i}"


    me "{i}Ah, dear Juliet,{/i}"


    me "{i}Why art thou yet so fair? Shall I believe{/i}"

    me "{i}That unsubstantial death is amorous,{/i}"

    me "{i}And that the lean abhorrèd monster keeps{/i}"
    me "{i}Thee here in dark to be his paramour?{/i}"


    me "{i}And, lips, O you{/i}"


    me "{i}The doors of breath, seal with a righteous kiss{/i}"


    me "{i}A dateless bargain to engrossing death.{/i}"


    me "{i}Here’s to my love!{/i}"

    me "..."

    me "{i}O true apothecary,{/i}"
    me "{i}Thy drugs are quick. Thus with a kiss I die.{/i}"

    window hide

    $ renpy.pause(1.0)  

    show ss unsure dress with fade

    if persistent.dress == 'purple':

        $ persistent.ssg_94 = True 

    else:   

        $ persistent.ssg_95 = True      

    $ renpy.pause(1.0)  

    window show 

    ss "{i}{enen}I do remember well where I should be,{/enen}{/i}"

    ss "{i}{enen}And there I am. Where is my Romeo?{/enen}{/i}"

    show ss  scared dress   

    ss "{i}{enen}My husband in my bosom there lies dead.{/enen}{/i}"

    show ss cry dress   

    ss "{i}{enen}What's here? A cup, closed in my true love's hand?{/enen}{/i}"    

    ss "{i}{enen}Poison, I see, hath been his timeless end:{/enen}{/i}"

    ss "{i}{enen}O churl! Drunk all, and left no friendly drop to help me after{/enen}{/i}"

    ss "{i}{enen}I will kiss thy lips;{/enen}{/i}"

    ss "{i}{enen}Haply some poison yet doth hang on them.{/enen}{/i}"

    show ss shy dress     

    "I'm on the floor. Samantha's bending over,"

    "Reluctant for a moment, then she waits no longer."

    "To put it mildly, Olga was against this:"

    "My Juliet kisses me while I'm defenseless."

    window hide 

    show white with dissolve2   

    if persistent.dress == 'purple':

        show cg ss_playkiss3p with dissolve2 

    else:   

        show cg ss_playkiss3r with dissolve2

    

    $ persistent.ssg_112 = True 

             

    $ renpy.pause (1) 

    window show

    "Her lips touched mine, and I could clearly feel"        

    "What everyone could see: the kiss was real."        

    if d6_kissing == 2: 

        th "Disaster! Now we're both in trouble."

#

        th "Or did she purposefully make it double"

        th "To prove I shouldn't be the only one to blame?"

        th "That must be it, no doubt. But what a shame!"
      

    else:

        th "Why would you do that? Why, Samantha, why?"

        th "Or was it just an accident? Oh my,"

        th "She tripped and fell upon me, now it's clear."

        th "I'm glad she's not disgusted, not in tears."

        "I can't imagine any other explanation"

        "For this ridiculous and awkward situation."

        "She did say yes to kissing, but she wasn't serious."

        "I have no doubt about this, I'm not delirious."

        "She only didn't want to hurt my feelings,"

        "Concerning that she must have had her own misgivings."

        "She didn't mean it. Thinking otherwise is lame."

        "Why would she want to stand the public shame?"

    window hide

    $ renpy.pause(0.9)  

    hide cg

    hide white

    show ss shy dress   

    with dissolve2   

    $ renpy.pause(1.3)

    show ss crysmile dress with dspr

    window show

    ss "{i}{enen}Yea, noise? Then I'll be brief. O happy dagger!{/enen}{/i}"

    ss "{i}{enen}This is thy sheath: there rust, and let me die{/enen}{/i}"

    window hide

    $ renpy.pause(1.0)  

    hide ss with dspr

    $ renpy.pause(1.8)

    show mz normal cos with dspr

    window show     

    mz "{i}And here I come, my friends! But what the hell...{/i}"

    show mz bukal cos 

    stop music fadeout 5    

    mz "{i}Here lies Romeo, Juliet lies near him.{/i}"

    mz "{i}She's warm and newly killed. Oh what a pity{/i}"

    mz "{i}To see them lying dead. And I smell trouble.{/i}"

    mz "{i}These crazy teenagers are hasty with decisions,{/i}"

    mz "{i}And that's the very reason why God hates them.{/i}"

    mz "{i}Or think about their music, Devil's music!{/i}"

    mz "{i}And therefore, I will not be deemed a sinner{/i}"

    mz "{i}If I allow myself to check their pockets{/i}"

    mz "{i}Just to collect the payment that I earned.{/i}"

    window hide 

    $ renpy.pause(1.0)  

    scene bg verona_garden

    show us angry cos

    with fade

    $ renpy.pause(0.5)  

    play music music_list["farewell_to_the_past_full"] fadein 3 

    us "{i}What fear is this which startles in our ears? Search, seek, and know how this foul murder comes.{/i}"

    voice "{i}Here is a friar, and Romeo's man. With instruments upon them fit to open.{/i}"


#

    show unl sad at fright

    ssunl "{i}O heavens! O wife, look how our daughter bleeds!{/i}"

    show us upset cos   

    us "{i}Seal up the mouth of outrage for a while,{/i}"

    us "{i}Till we can clear these ambiguities{/i}"

    us "{i}And know their spring, their head, their true descent,{/i}"

    us "{i}And then will I be general of your woes.{/i}"

    us "{i}Meantime forbear, and let mischance be slave to patience.{/i}"

    us "{i}Bring forth the parties of suspicion.{/i}"

    show mz normal cos at left  

    mz "{i}I am the greatest, able to do least.{/i}"

    mz "{i}And here I stand, both to impeach and purge, myself condemnèd and myself excused.{/i}"

    us "{i}Then say at once what thou dost know in this.{/i}"

    mz "{i}I will be brief, for my short date of breath{/i}"

    mz "{i}Is not so long as is a tedious tale.{/i}"

    window hide

    $ renpy.pause(0.5) 

    show us calml cos

    with fade2

    window show

    us "{i}This letter doth make good the friar’s words.{/i}"

    us "{i}And as the matter stands,{/i}"

    us "{i}This humble man of God cannot be punished.{/i}"    

    show mz smile cos at left       

    mz "{i}But maybe he can be rewarded then?{/i}"

    show us dontlike cos    

    us "{i}Where be these enemies? Capulet! Montague!{/i}"

    show mz bukal cos at left   

#

    show us upset cos   

    us "{i}See what a scourge is laid upon your hate,{/i}"

    us "{i}That heaven finds means to kill your joys with love!{/i}"

    ssunl "{i}O brother Montague, give me thy hand.{/i}"

    ssunl "{i}This is my daughter’s jointure, for no more can I demand.{/i}"

    show us sad cos 

    us "{i}A glooming peace this morning with it brings.{/i}"

    us "{i}The sun, for sorrow, will not show his head.{/i}"

    us "{i}Go hence, to have more talk of these sad things.{/i}"

    us "{i}Some shall be pardoned, and some punishèd.{/i}"

    us "{i}For never was a story of more woe.{/i}"

    stop music fadeout 3

    us "{i}Than this of Juliet and her Romeo.{/i}"

    window hide

    $ renpy.pause(1.0)

    show us smile cos

    "We did it! Now we'll only take our bows."

    window hide 

    $ renpy.pause(1.0)

    hide us

    hide unl

#    show unl normal at cleft

    show sl smile2 cos at center    

    show ss normal dress at fleft

    show mz normal cos at cleft

    show dv laugh cos at fright

    show us smile cos at cright 

    with fade2  

    $ renpy.pause(1.0) 

    window show 

    "What rare and lovely moments, namely those"

    "When work is finished, everything is done,"

    play sound Clapyell

    "And all that's left is taking the applause."

    window hide

    $ renpy.pause(1.5)

    

    $ persistent.sprite_time = 'sunset'

    $ sunset_time() 

    scene bg ss_ext_stage_big_day

    with dissolve 

    window hide

    play music music_list["smooth_machine"] fadein 5    

    "The actors and the audience are tired."

    "The latter disappeared like doused fire."


    "The troupe remained at first for a discussion,"

    "But then began to leave, without rushing."

    window hide

    $ renpy.pause(1.0)  

    if d6_dvchoice >= 2:

        show dv laugh cos with dspr

        dv "Well done, Semyon! Oh my, you're quite an actor."

        dv "Come over after sunset. And you'd better!"

        hide dv with dspr

    $ renpy.pause(0.5)      

    window show

    

    "I'm sitting near the stage. A thoughtful mood –"

    "I'm wondering about my next pursuit."

    th "What if I look beyond the camp perimeter?"


    th "What if I go and join a Moscow theater?"

    th "Indeed, I have a twenty-year head start –"

    th "I might become the star of Avatar!"

    th "Oh well. I have too little time for dreaming."

    th "It seems I'm gonna have a busy evening."

    th "Our days in summer camp will soon be over –"

    th "It's time I acted like a problem solver."

    

    "And here's Samantha, pensive and alone."

    "It wasn't long before she saw me staring."


    "She brightened and (as if to say hello)"

    "She headed towards me, but wasn't wary"

    show mt angry panama pioneer

    "Of Olga, who appeared out of the blue"

    "And dragged away Samantha. Why? No clue."

    mt "Semyon, return to base, we need to talk."

    mt "Don't make a scene, let us not scare the folk."

    window hide 

    $ renpy.pause(0.5)  

    scene bg ext_house_of_mt_sunset
    with dissolve
    show mt normal panama pioneer

    with dissolve
    $ renpy.pause(0.3)
    window show

    me "A scene... I'm really tired of this word."

    me "At least I am no longer on the stage."

    me "One play a day is all I can afford,"

    me "So please, no more theatrics for a change."

    mt "Agreed. It's me who's tired of you all."

    mt "I wanted rest tonight, as I recall."

    me "Our mutual agreement has its merits."

    me "I'll change my clothes, then leave to run some errands..."

    stop music fadeout 2

    window hide

    scene bg int_house_of_mt_sunset
    with dissolve
    play ambience ambience_int_cabin_evening 
    $ renpy.pause(0.3)
    show mt normal panama pioneer

    with dissolve

    window show

    play music music_list["torture"] fadein 5

    mt "Oh no you won't. That's it, Semyon."

    mt "My patience has been wearing thin."

    mt "Your little {i}errands{/i} can't go on,"


    mt "The time has come to lock you in."


    mt "From now on you are under house arrest,"

    mt "And it will last until you leave our place."

    window hide

    $ renpy.pause(0.5)

#    th "Oh no. There is no easy way to stop it."

#    th "It's like I'm in the clutches of Gestapo."
    window show
    th "Calm down! I shouldn't make the matters worse,"

    th "Although I really want to swear and curse."

    me "I think I understand why you're concerned,"

    me "And I accept the punishment I earned."

    me "It is severe, but I agree, believe me."

    me "Yet, I had plans for this specific evening."

    me "And since I'm going to be justly grounded,"

    me "I need to first inform my friends about it."

    show mt smile panama pioneer

    mt "Yeah, right, Semyon. I've heard enough. Nice try."

    th "It is disturbing that her smile is wry."

    show mt normal panama pioneer  

    mt "I'll say it twice. I hope it's very clear:"

    mt "No leaving is allowed. You're staying here!"

    mt "If needed, I will have you guarded,"

    mt "But I'll make sure that you are grounded."

    if (d6_dvchoice) +  (d6_unchoice) >= 3:

        me "Please understand, I really have to go!"

        th "And, possibly, to more than one location."

        me "I promised, it was almost like an oath!"

        me "Can we postpone it, my incarceration?"

    else:

        me "Please understand, I really have to go"

        me "And tell her properly that it's all over."

        me "The summer's ending, we are going home."

        me "We'll both move on and think of it no longer."

        me "How would you feel if you were in my shoes?" 

        me "Just let me go and tell her what I should."

    

    mt "Semyon, your efforts are in vain,"

    mt "They make me see it's in your nature:"

    mt "Your actions, which you can't explain,"

    mt "Can put us all in real danger."

    menu:

        "I have to talk to Samantha":

            me "I have no clue what's on Samantha's mind."

            me "Our relationship with her is very... undefined."
            
            me "What do you think of us - could never happen."

            me "I will explain it to her if you let me in her cabin."

            mt "No way. And don't pretend you're such an angel."

            mt "I am no longer open to your tricks of persuasion."

            mt "I seen your {i}undefined{/i} with my own eyes,"

            mt "So even bringing up this topic is unwise."

            

        "But it's not Samantha I need to see":

            me "You think I'm going to Samantha's place?"

            me "But I assure you, this is not the case."

            me "I have enough – without her – on my plate."

            mt "Okay, I get it. Don't elaborate."
            
            mt "I wouldn't want her, should it catch her eye,"

            mt "To see you with another girl and cry!"            

            mt "Samantha isn't blind. Don't try to break her heart"

            mt "By dating other girls – it may have a horrendous impact!"
            
            mt "You've done enough of mischief for the day."

            mt "And hence no errands for tonight. You stay."            

            me "It's not a date, we simply need to talk!"

            me "We won't stand out, don't make it a deadlock!"

            mt "I do not want to follow you around."

            mt "You stay. It's safe, no trouble to be found."

            

        "I need to talk to Lena" if (d6_unchoice >=2):    

            me "I need to talk to Lena. I must hurry!"

            mt "You think this changes something? Don't you worry..."

            me "But surely this can change a lot for us!"

            mt "It's not the point. Indeed, why make a fuss?"

            mt "It may be Lena or it may be Lenin,"

            mt "This doesn't change the fact that you're STAYING."

            menu:

                "She is a person too":

                    me "I see you want to punish me and blame,"

                    me "And all my friends as long as they are nameless..."

                    me "But they are real as you and me. Your job is to support them."

                    me "What about Lena? It seems she is important for you."

                    me "You're like an older sister to her... maybe second mother."


                    me "You hurt her badly by forbidding to us see each other."

                    mt "Oh, what a drama. Such torments are acceptable to me."

                    mt "There's nothing new, it happens pretty often in this camp..."

                    mt "No, actually there is one new detail,"


                    mt "We're talking not about the reputation of the camp,"

                    mt "It means a world for me, of course, but there is a higher stake today:"

                    mt "The reputation of the state! {w}Beware of messing with it."


                    mt "I'll do my job as I am supposed to do... and always do. "

                    mt "I will appreciate your understanding."

                    jump ssd6_zak_eng   
                    
                "Lena is in danger":

                    stop music fadeout 3

                    me "Our Lena is so sensitive by nature,"

                    me "Not going there is something I'll regret."

                    me "By sitting here we're leaving her in danger,"

                    play music music_list["afterword"] fadein 3

                    me "Who knows what she can do if she's upset."

                    show mt sad panama pioneer

                    mt "What can she do? What is it I've just heard?"

                    me "She's waiting for me on the pier alone."

                    me "She'll think I stood her up. She will be hurt,"

                    me "So she might drown herself, for all I know."

                    show mt angry panama pioneer

                    mt "Oh, have some shame, you! Voice of doom!"

                    mt "It's just another lie from you. Nothing is happening."

                    me "You have an eye for this kind of things,"

                    me "Are you that sure it's nothing? Oh, I doubt that." 

                    me "Let's make a move, let's check it. Just in case."

                    show mt sad panama pioneer

                    mt "Who's moving where? I am forbidding you to go." 
                    mt "And we're not going there together either."

                    me "Will you just sit here doing nothing then?"
                    me "If something happens - you will not forgive yourself."

                    mt "You're grounded. Do you know the definition?"

                    me "So go alone. I promise, I'll stay here."
                    me "If she is safe then I am yours to judge."

                    mt "I'll think about it... Damn, you're giving me a riddle!"
                    mt "Expect no mercy if you lied to me."

                    mt "I'll tie you to the bed with rusty chains!"

                    me "Yes, sure... Wait, let me write a note to Lena,"

                    me "Couple of words, no more. I hope you won't read them..."

                    window hide

                    $ renpy.pause(1.0)

                    window show

                    mt "Feel free to keep them for yourself if you don't trust me."
                    me "I didn't mean... I'm sorry. Here's the note."

                    mt "Stay put. Don't even think about opening the door."

                    window hide

                    hide mt with dspr

                    $ renpy.pause(1.0)

                    window show

                    "My plan is working: Olga is away,"

                    "Now it's my turn to choose to leave or stay. "

                    "If I stay, it will probably help me to regain her trust,"

                    "And with the trust comes back my precious freedom."

                    window hide

                    menu:

                        "Leave":

                            "I wait a bit more and leave the house."

                            $ persistent.sprite_time = 'night'

                            $ night_time()                          

                            window hide

                            play ambience ambience_forest_night

                            scene bg ext_house_of_mt_night with dissolve                            
                            window show
                            th "Not good... It's almost night already."

                            window hide

                            stop music fadeout 1

                            $ renpy.pause(1.0)

                            play sound sfx_scary_sting 

                            show mt angry panama pioneer

                            play music music_list["torture"] fadein 1                           

                            mt "I knew it! Gotcha, scoundrel!"


                            th "She ambushed me! Stood here, behind the corner!"

                            mt "Get back there, you are lying... liar!"
                            mt "Son of a lying liar also... You!"

                            me "...What if I don't get back?"

                            me "I'm physically stronger. Also faster."

                            me "You have no right to hold me. And you can't!"

                            me "I was obedient because of my good will,"

                            me "But you abused it too much. So no more orders!"

                            me "There's no such law to hold me like in jail."

                            mt "How dare you even speak, you saboteur?!"


                            mt "You will be held! By me or by the law!"


                            mt "Run to the house or I'm calling the police!"

                            mt "But first of all I'll send Samantha home!"

                            mt "We had enough of shame from you in front of honored guest!"

                            mt "Get back now and be happy we'll handle this"

                            mt "Calmly and quietly. It is best for you, believe me."


                            jump ssd6_zak_eng

                            

                        "Wait":

                            

                            $ persistent.sprite_time = 'night'

                            $ night_time()

                            show bg int_house_of_mt_night with fade

                            "I spent some time looking at the clock."

                            "Its minute hand did a whole circle"

                            "Before the leader came back to the house."

                            "Olga was angry for some reason."

                            window hide
                            show mt normal panama pioneer with dissolve
                            window show

                            mt "I had to chase your Lena by boat and it was hard to catch her. "

                            mt "We had a bloody regatta there, and all because of your rich imagination!"



                            mt "Here is a letter for you. Please, don't reply to it."

                            mt "It was the last one for me as a postman."

                            window hide

                            show backnote with dissolve:

                                pos (700,400)

                            th "She answered to \"See you soon\" with \"Kill her\". How laconic."

                            th "There is no doubt that it's from Lena."

                            hide backnote with dissolve

                            me "Thank you for that. And thank you for your care."

                            me "No, I need nothing more from you..."
                            me "Maybe except my freedom."

                            jump ssd6_zak_eng

    
                            
                  

    

label ssd6_zak_eng: 

    window hide

    stop music fadeout 2

    $ renpy.pause(0.8)  

    $ persistent.sprite_time = 'night'

    $ night_time()

    play ambience ambience_int_cabin_night

    scene bg int_house_of_mt_night with dissolve

    $ renpy.pause(0.8)  

    window show

    play music music_list["faceless"]  fadein 5

     

    "It's dark outside - my cruel jailer doesn't care."
    "Same for the cruel clockhand. It's just moving further."

    "I tried my best to persuade O.D. into letting me go,"

    "That was a time I spent in vain - she's adamant."

    th "Should I revolt and simply walk away?"

    th "Then she will try to keep me here by force..."

    th "No, I don't want to make things worse."

    th "I'll sit and wait - and with the first chance I'll sneak out."

    th "But until then I will be quiet... {w}Or loud?"
    th "Since I am stuck here, should I try to be annoying?"

    th "Maybe she'll throw me out of here."

    me "Hey, Olga, tell me, why are you so cruel?"

    me "What makes you do so? Any troubles in the past?"

    me "It happened when you were a pioneer yourself?"
    me "Did somebody forbid your friendship?"

#    me "Open your secret."

    show mt smile pioneer

    mt "Me? Cruel? I am nice for now. You'll see,"

    mt "As soon as you annoy me a bit more,"

    mt "I'll lock you down behind the biggest door - at the storehouse!"

    mt "I will be free of guarding you myself. Give me a reason."

    window hide

    $ renpy.pause(0.6)  

    hide mt with dspr

    $ renpy.pause(1.0)

    window show

    

    th "An hour passed. My time is running out."

    th "How did I get into this stupid situation?"

    th "I had so many dull and empty evenings in my life,"
    th "And I will probably have many more, but why tonight?"


    th "It has to be the one exception... Or am I asking for too much?"

    th "Perhaps it's all my own fault. The punishment for being indecisive."

    stop music fadeout 3

    th "This evening's lost, I see now." 

#    th "It's interesting, how Alisa and Slavya are doing?"

#    th "They can pal up. Maybe it's for the best that I'm not going."

#    play music Girls_Just_Want_To_Have_Fun fadein 2

#    window hide

#    $ renpy.pause(1.0)  

#    show cg dv_sl with dissolve2

#    $ renpy.pause(5.0)  

#    hide cg with dissolve2

#    stop music fadeout 3

    mt "Here, take a book. Stop looking at the wall."

    mt "You cannot leave this cage at all."
   

    mt "And you don't need to. Have some reason."

    mt "Perhaps you'll have more luck next season."
        

    if d6_treasure >= 1:

        me "What, tired of my grumpy face? Then, entertain me."

        me "You rarely speak about yourself. Tell me a story?"
        mt "A story about me? Why do you need it suddenly?"
        me "I'd like to hear about that hairpin that we found,"
        me "I feel that there is something big behind it."

        me "It's kinda bothering me..."

        window hide

        show mt normal pioneer with dspr

        window show

        mt "I would prefer to tell you something else..."
        me "That is exactly why I want to hear about the hairpin."        
        show mt normal  pioneer    
        mt "I'll tell you, fine. You're asked for it."
        mt "But don't you dare judge me or my story."        
        me "Got it. I'm listening."        
        

    else:

        window hide

        show mt normal pioneer with dspr

        window show 

        mt "I heard you lost the papers with the script,"

        mt "A big deal, right? How did you manage to succeed?"

        me "Yes, Slavya brought them back. Big thanks to her,"
        mt "She brought them back from where, if I may ask?"
        "I shrugged indifferently."

        show mt smile pioneer with dspr 

        mt "Hey... Was it her who took the pack? By any chance?"

        me "Slavyana can't steal anything."

        mt "Are you so sure? She was so tense today..."

        me "She is an honest person. Surely not worse than you."

        me "Maybe you look tense too, so?.. Did you ever steal?"


        window hide

        $ renpy.pause(0.5)      

        show mt scared  pioneer with dspr

        $ renpy.pause(1.0)

        window show

        me "...I see you did, wow! Found a thief here!"

        me "Please, tell me more, don't keep silent!"

        mt "Mistakes of a stormy youth, a trifle..."

        me "Tell me about a trifle then. This could be fun."

    

        show mt normal  pioneer      

        mt "No, there is hardly any fun... But alright."
        
        show mt normal  pioneer    

        me "I'm listening."

    play music music_list["waltz_of_doubts"] fadein 7   

    mt "Well... Once upon a time there was a fancy hairpin."

    mt "She lived right on the shelf in a haberdashery,"

    mt "In the big store next to the local school."

    show mt smile  pioneer      

    mt "We often visited that store after the lessons,"
#    mt "We would visit it again and again,"

    mt "The shelf with hairpins magnetized us, girls, especially..."

    mt "Oh, shiny things! Some of my classmates bought a few,"
    mt "And all my friends had at least one. Everyone did."

    mt "Except me. No matter how I asked, mom wouldn't give me money."

    mt "We weren't rich and also I was punished for something..."

    mt "I had no chance to buy the thing I so desired."
    mt "Glued to the store, I watched these colors, yacht and turquoise..."


    show mt normal  pioneer   

    mt "And every day there were less hairpins on the shelf."

    mt "I had no rest. I couldn't let it slip away from me."

    mt "Slowly I made up my mind to commit a crime."

    mt "I went there, waited for the moment... and I took it."

    mt "I had so much adrenaline!  My heart almost exploded there."

    mt "But, as you see, it wasn't a big heist at all..."
    mt "The damned thing ended in my pocket and I left."
    mt "That's how I stole the hairpin."

    me "Did somebody notice? Were you caught?"

    mt "No, I will disappoint you here,"
    mt "Nobody ever knew about this..."

    mt "But I was punished in some way, of course."
    mt "It started just when I returned home..."

    show mt sad  pioneer        

    mt "I hid the hairpin, too afraid to even look at it,"

    mt "And I felt worse and worse every hour." 

    mt "I recollect how scared I was. I was afraid of every policeman on a street," 

    mt "Or any man in uniform, including the postman."

    mt "Imagine all the shame on me, a good girl and the best pupil,"

    mt "If anyone found out about my deed!"


    mt "Foretasting that nightmare, I shaked like a leaf,"

    mt "I thought I wouldn't live through such a humiliation..."

    window hide

    $ renpy.pause(0.4)      

    show mt normal  pioneer with dspr

    $ renpy.pause(0.4)  

    window show

    mt "What happend then, you ask? Not much. I buried the hairpin."

    mt "Along with some of my related fears..."

    mt "I never took what didn't belong to me since then."

    mt "If stealing is so hurting me, I guess it's not my thing at all,"
    mt "And there's no other way to live but to live honestly."

    me "You had quite an experience. What do you think..."
    me "What if you didn't take that hairpin then?"

    me "Would you be capable of stealing now?"
    me "You're having here in the camp quite an advantageous position..."

    mt "Ha-ha. Who knows. But speaking about stealing from the camp..."

    mt "It's not for me in any circumstances. To steal from kids is just too much."

    window hide

    $ renpy.pause(1.0)  

    window show 

    mt "So, roomie... Crazy day, huh? Are you tired?"

    me "I have been vegging out for hours... Thanks to you. "

    mt "Tired or not, but now it's time to rest."

    mt "And since you are under arrest... {w}Today we sleep together."


    mt "Need to be sure that you won't elope -"
    mt "Your place is here by the wall."

    mt "What? Don't look at me like that."

    me "But we won't fit in here..."

    show mt smile  pioneer      

    mt "Nonsense. We can snuggle up."

    window hide 

    $ renpy.pause(1.5)

    show mt laugh  pioneer with dspr    

    window show     

    mt "Boy, your expression's priceless!"

    show mt grin  pioneer   

    me "A joke?.. Whatever. I don't care."

    mt "I'm going to my bed. Don't run away, please."

    show mt smile pioneer       

    mt "Or is not too late to see your \"friends\"?"

    me "A bit too dark for me. I'll see them in the morning."

    stop music fadeout 6    

    mt "Then I don't need to worry. You won't ever get up before I do."

    window hide
    hide mt with dissolve
    $ renpy.pause(0.5)     
    scene bg int_house_of_mt_night2 with dissolve

    $ renpy.pause(1.0)  

    window show

    "I couldn't sleep. There were too many heavy thoughts"
    "That kept my mind busy and restless."

    "I tossed and turned until I felt sick of any thought at all,"

    "To clear my mind - that's all I wanted."

    "To do so, I had to leave the bed. So, moving quietly, I did."

    "I looked at the clock and noticed that it was already past midnight."

    "The day was over..."

    

label ss_d6_afmidnight_eng:

    th "Well, who cares if the day is over? I'm not Cinderella, my money doesn't turn into roubles at midnight. Nothing changes... Right?"

    th "It's too late to run away, but at least I can go breathe some fresh air."

    th "Just because I want to. And to spite Olga. {w}However, it's best if she doesn't find out."

    window hide

    stop ambience fadeout 1
    
    $ renpy.pause(0.5)    

    scene bg ext_house_of_mt_night_without_light with dissolve

    play music spiritual_spiritual fadein 2

    $ renpy.pause(1.0)

    window show 

    th "Ahh, excellent."

    "What a night. A deep, rich night that makes the shadows inky black. A very nighty night, a model one."

    "It was not hard to see the figure of the night, her shape. The forest looked right like a gigantic hoodie of the night, and long shadows falling from the trees were the sleeves."

    "And if I could, I would grab her by the sleeve and say something in her face:"

    me "Hey, Night! Empty your pockets, and then I will disappear into the... you!"

    th "Is it silly?.. {w}Well, what kind of conversation with Night wouldn't be?"

    "I got a feeling that somebody had to appear right now. Just like they always do. Because if  not, then my moment of unity with nature will remain unspoiled."

    "..."

    th "Well?"

    "..."

    th "Not this time, it seems. Maybe I should drop in on somebody myself?"

    th "...Or not. It's really not my style."
    th "Decent people don't visit anyone. They stay at home alone, enjoying their decency."

    window hide

    $ renpy.pause(0.3)    

    show cg moonstars at Zoom((1920, 1080), (0, 0, 1920, 1080), (0, 0, 1920, 1080), 0.0)  

    with dissolve

    

    $ persistent.ssg_113 = True 

            

    window show

    "I lied on the grass, trying not to scare off the romantic mood. For example, by thinking about the possibility of finding myself on on anthill. "

    th "...Damned ants, with their stupid perfect organization. {w}Yeah, like there is an ant king somewhere, who commands them:"

    th "{i}This giant is destroying our home! Run all over him! Send some reinforcements to the left flank! Add more chaos!{/i}"

    ss "{enen}Hey. What are you doing here?{/enen}"

    window hide

    show ss nosmile pv

    hide cg 

    with dissolve 

    

    $ persistent.ssg_96 = True 

        

    $ renpy.pause(0.5)

    window show    

    me "{enen}I’m lying in the night.{/enen}"

    ss "{enen}...Cool.{/enen}"

    me "{enen}How did you find me here?{/enen}"

    ss "{enen}I don't know. Maybe we share a special connection?{/enen}"    

    me "{enen}How many fingers do I have behind my back?{/enen}"

    window hide

    show ss grin_smile pv with dspr

    $ renpy.pause(0.3)    

    window show

    ss "{enen}Two!{/enen}"

    window hide

    $ renpy.pause(0.5)     

    menu:

        "True":

            $ sp_ss += 1

            me "{enen}Right! How did you know?{/enen}"

            show ss grin pv 

            ss "{enen}Nobody shows one or five fingers. And it's pretty hard to raise three or four. Logic!{/enen}"            

        "False":

            show ss nosmile pv with dspr

            ss "{enen}No special connection then. Can we live without it?{/enen}"        


            me "{enen}Let me think... {w}Nope. I don't like you anymore.{/enen}"        

            show ss sad pv        

            window hide
            $ renpy.pause(1.0)
            window show

            me "{enen}It was a joke...{/enen}"

            show ss unsure pv           

            ss "{enen}I know... But it kinda hurts anyway. Can't help it.{/enen}"    

            me "{enen}I can't help it either. I can't go ahead and say \"I like you anyway\", can I?{/enen}"

            ss "{enen}That would be nice, actually.{/enen}"            

    

    window hide

    $ renpy.pause(1.0)

    show ss nosmile pv with dspr   

    window show 

    ss "{enen}So... Lying on the grass... next to a lounger. That's really something.{/enen}"

    me "{enen}Something stupid?{/enen}"

    show ss surprise pv    

    ss "{enen}No, I like it, really. It's like you are free and impulsive...{/enen}"

    show ss unsure pv   

    me "{enen}But it was really stupid. I'd like to get up, but I'm already green and dirty, so whatever.{/enen}"

    me "{enen}Okay, maybe freedom is involved, too, just a little...{/enen}"    

    show ss smile pv with dspr

    ss "{enen}I knew it!{/enen}"

    window hide

    $ renpy.pause(1.0)  

    show ss nosmile pv with dspr    

    window show 

    ss "{enen}So? Is there any room for me?{/enen}"

    me "{enen}Be my guest.{/enen}"

    window hide

    $ renpy.pause(0.6)  

    hide ss with dspr

    window show     

    "Samantha lied next to me. That meant that not only my shirt to be washed the following day."


    window hide

    $ renpy.pause(0.3)    

    show cg moonstars at Zoom((1920, 1080), (0, 1000, 1920, 1080), (0, 1000, 1920, 1080), 0.0) 

    with dissolve2

    $ renpy.pause (1)

    window show

    ss "{enen}Speaking of freedom...{/enen}"    

    extend " {enen}It seems you are grounded.{/enen}"

    me "{enen}I am grounded. Literally.{/enen}"    

    ss "{enen}Yeah, right. But you're also stuck in the house. All because of me.{/enen}"

    me "{enen}I'll figure something out.{/enen}"

    ss "{enen}Stupid me... Why did I do that?{/enen}"

    

    if d6_kissing == 2:

        if sp_ss >= 14:

            me "{enen}I started it. And I regret nothing.{/enen}"

            jump ss_d6nlv_eng

        else:   

            me "{enen}A bad idea. But it was me who started it.{/enen}"

            jump ss_d6nfr_eng

    else:

        if sp_ss >= 16:

            me "{enen}I don't regret the kiss... well, maybe the fake one.{/enen}"

            jump ss_d6nlv_eng           

        if sp_ss >= 14:

            me "{enen}I think it was a beautiful way to finish the play. Even if Olga didn't appreciate it.{/enen}"

            jump ss_d6nlv_eng

        else:

            me "{enen}You made a mistake.{/enen}"

            jump ss_d6nfr_eng

    

label ss_d6nfr_eng:

    $ persistent.sprite_time = 'night'

    $ night_time()  

    $ ss_friend = True

    ss "{enen}It was so selfish of me. I'm really sorry.{/enen}"

    me "{enen}Don't apologize - I certainly wasn't offended. But you know what people might think. It might hurt you.{/enen}"

    ss "{enen}But I am free and you are grounded. The way you care about me is sweet, but I know that you have better things to do. I screwed you over.{/enen}"

    me "{enen}No, you didn't... But... {w}Frankly, I still wonder why you did that.{/enen}"

    "..."

    me "{enen}Nevermind, forget it.{/enen}"

    ss "{enen}I feel like we are meant to be friends no matter what.{/enen}"

    ss "{enen}So it was a simple thought... why not?{/enen}"

    ss "{enen}I found myself in the middle of something important... At least I felt like that... and had to do something...{/enen}"

    ss "{enen}Uh, this is so dumb! Why am I even explaining?..{/enen}"

    me "{enen}I understand. Somehow, this feeling never leaves me in this camp.{/enen}"

    me "{enen}It's like... I'm not myself here. I'm acting like a different person all the time.{/enen}"

    me "{enen}I mean... In my world, how could I even talk to a girl your age? It's embarrassing!{/enen}"

    ss "{enen}I know. Why is it embarrassing though? It seems we get along well.{/enen}"

    me "{enen}The rules of modern society, I guess. Yes, such a friendship often leads to... what they think it leads to. Maybe it happens mostly because they think about that?{/enen}"

    ss "{enen}Maybe it's not about the rules? Maybe we are the exception?{/enen}"

    me "{enen}So there must be something wrong with me - not to... Am I just an idiot?{/enen}"

    ss "{enen}Well, you might be!{/enen}"
    
    "She giggled."
    
    me "{enen}Where is the fine line between an idiot and a сhild moles...{/enen}"

    me "{enen}Hey, what are we talking about?! You are a kid!{/enen}"

    ss "{enen}Are you not? Alright, grandpa.{/enen}"

    window hide

    stop music fadeout 4     

    $ renpy.pause (1)   

    show cg moonstars at Zoom((1920, 1080), (0, 1000, 1920, 1080), (0, 0, 1920, 1080), 5.0)

    $ renpy.pause(5.0)     

    play music Somewhere_in_the_Midnight_of_Summer fadein 2

    $ renpy.pause (1)

    window show    

    me "{enen}I don't think it is good for you. To hang out with me.{/enen}"    

    ss "{enen}Who cares.{/enen}"

    extend " {enen}I mean, why isn't it good? There is nothing bad at all.{/enen}"

    me "{enen}If I were your father, I wouldn't approve of such a friendship. I would never allow it.{/enen}"

    ss "{enen}So you'd never allow me to be in the play? Or even visit the camp?{/enen}"

    me "{enen}That's right.{/enen}"

    ss "{enen}How cruel. Didn't you like our play?{/enen}"

    me "{enen}I liked it, of course. You were amazing.{/enen}"

    ss "{enen}But I made one mistake...{/enen}"

    ss "{enen}I guess you'll have to kill me now.{/enen}"

    th "Sometimes she's unbearable.  Why are we talking about this again? She must be teasing me."

    me "{enen}Only if by \"kill\" you mean \"kick\".{/enen}"

    ss "{enen}Or \"kiss\".{/enen}"

    "Samantha giggled - of course, nobody is going to put these ideas into practice."

    show ss_comet at Move((-0.5, -0.5), (1.0, 1.0), 4.0)     

    ss "{enen}Oh, look! There is a comet!{/enen}"    

    me "{enen}A comet? It was a real falling star! You should make a wish.{/enen}"

    ss "{enen}I will. But I rest my case, it was a comet.{/enen}"

    me "{enen}I'm pretty sure that you must call it a star to make a wish.{/enen}"

    ss "{enen}Or what? My wish will never come true?{/enen}"    

    me "{enen}Of course not.{/enen}"    

    ss "{enen}Oh, that's too bad. I wished something for you. {w}I wished for you to learn some basic astronomy!{/enen}"

    "She was laughing again."

    me "{enen}...Can you enjoy the stars like normal people do?{/enen}"

    ss "{enen}Like how?{/enen}"

    me "{enen}What people say about the stars... {w}Can you imagine living species out there? What if they're looking at us right now?{/enen}"

    ss "{enen}Those beardy guys from the space station?{/enen}"    

    me "{enen}Come on! Don't you like the stars?{/enen}"

    ss "{enen}I adore those luminous spheres of plasma held together by their own gravity.{/enen}"

    me "{enen}I see...{/enen}"    

    ss "{enen}What's wrong? Or should I say that they are pretty?{/enen}"

    me "{enen}...You can't argue with that.{/enen}"

    show ss_comet at Move((-0.5, -0.5), (1.0, 1.0), 2.5)     

    ss "{enen}Why should I? I do like them. I even got a telescope at home. And a big book about them.{/enen}"            

    me "{enen}Okay, you know a lot about stars. But...{/enen}"

    show cg moonstars at Zoom((1920, 1080), (0, 1000, 1920, 1080), (0, 1000, 1920, 1080), 0.0)    

    with dissolve

    me "{enen}You know what's the saddest thing about them?{/enen}"

    ss "{enen}Enlighten me.{/enen}"

    me "{enen}Some of these stars are already dead. Some have been dead for thousands of years! It's only the light...{/enen}"

    ss "{enen}That's not true.{/enen}"

    me "{enen}Why not?{/enen}"

    stop music fadeout 5    

    ss "{enen}Every single star you can see now is within just a few light years from us. They're simply not that far away.{/enen}"        

    window hide

    $ renpy.pause (0.5)

    play music the_tunnels     

    hide cg

    show ss nosmile pv  

    with dissolve  

    window show 

    me "{enen}Go home!{/enen}"

    ss "{enen}Why?!{/enen}"    

    me "{enen}You are a terrible star talker!{/enen}"

    show ss surprise pv     

    ss "{enen}Oh, come on! You can't send me away because of the truth.{/enen}"

    me "{enen}No. But seriously. It's getting cold.{/enen}"

    show ss unsure pv 

    $ renpy.music.set_volume(volume=0.5, delay=4, channel='music')    

    ss "{enen}I guess... {w}Are we fine?{/enen}"

    me "{enen}Of course we are.{/enen}"

    show ss nosmile pv    

    ss "{enen}...Alright then. See you tomorrow. I hope.{/enen}"

    me "{enen}Good night.{/enen}"

    window hide

    $ renpy.pause(1.0)  

    hide ss

    with dspr   

    $ renpy.pause(0.5) 

    window show     

    me "{enen}Hey, wait!{/enen}"    

    window hide 

    $ renpy.pause(0.5) 

    show ss surprise pv 

    window show 

    ss "{enen}What?{/enen}"

    $ renpy.music.set_volume(volume=1.0, delay=1, channel='music')    

    me "{enen}...We are more than fine!{/enen}"    

    window hide

    $ renpy.pause(0.5)  

    show ss smile pv with dspr 

    $ renpy.pause(2.0)

    jump ss_d6suddenmt_eng

    

label ss_d6nlv_eng: 

    $ persistent.sprite_time = 'night'

    $ night_time()

    $ ss_friend = False     

    ss "{enen}No, I shouldn't have. Now you're in trouble.{/enen}"    

    ss "{enen}We're both kinda grounded now... I can't do much by myself. Even if I wanted to.{/enen}"

    ss "{enen}Sorry, it's so selfish. You have your own life... {w}Well, you used to have it. Sorry again.{/enen}"

    me "{enen}Don't blame yourself - I'm happy with the play and perfectly fine right now. And we are together.{/enen}"

    ss "{enen}Yes, we are. But I almost ruined it there. Maybe I did.{/enen}"

    ss "{enen}You know... The tomb scene... I was about to do a fake kiss, but suddenly...{/enen}"

    ss "{enen}I realized that I pretend way too much. All my life has become a fake. While I am missing something true.{/enen}"

    ss "{enen}Why should I fake the real thing, the beautiful thing? The thing it's all about?{/enen}"

    ss "{enen}I can never do what I want. I feel like I will never be able to. So I did it today. Just this once.{/enen}"    

    ss "{enen}It probably makes no sense to you. Just a stupid little girl mumbling her stupid little problems...{/enen}"

    me "{enen}Don't say that. I'm concerned about any matter which concerns you.{/enen}"

    ss "{enen}Maybe we are all overreacting a bit... Let's just look at the sky.{/enen}"

    window hide    

    $ renpy.pause (0.7)     

    show cg moonstars at Zoom((1920, 1080), (0, 1000, 1920, 1080), (0, 0, 1920, 1080), 4.0)

    $ renpy.pause(5.0)     

    me "{enen}You said that you can't do what you want.{/enen}"

    ss "{enen}Did I? Right...{/enen}"

    me "{enen}So what do you want, exactly?{/enen}"

    ss "{enen}Nothing crazy at all. But I'd like to have the opportunity. Sometimes.{/enen}"

    ss "{enen}You must be thinking that I'm just whining about nothing. {w}Well, I am. I have a good life, obviously.{/enen}"

    stop music fadeout 4  

    ss "{enen}But a part of me doesn't want this life. {w}No one have met this part before. You are the first.{/enen}"

    me "{enen}I am honored.{/enen}"

    play music Nobodys_Secret fadein 3  

    ss "{enen}Yes, for me, you are special. {w}For me... {w}Are you for me?{/enen}"

    me "{enen}What do you mean?{/enen}"

    ss "{enen}If I am for you, are you for me?{/enen}"

    window hide

    $ renpy.pause(1.0)

    window show 

    if sp_mizul >= 2:

        me "{enen}Nothing new since the snakebite. I am for you.{/enen}"

        ss "{enen}I just wanted to hear it again.{/enen}"

        me "{enen}We can't speak much about it.{/enen}"

        ss "{enen}I know. But...{/enen}"

        ss "{enen}I don't think it's working. We should either be together or be apart. Not like this - together and apart.{/enen}"

    

    else:

        me "{enen}I am.{/enen}"

        ss "{enen}Say it.{/enen}"

        me "{enen}I am for you.{/enen}"

        window hide

        $ renpy.pause(1.0)

        window show         

        ss "{enen}...What are we gonna do?{/enen}"    

        me "{enen}...Nothing. We can do nothing.{/enen}"        

        ss "{enen}Is it really true?..{/enen}"            

    

    

    me "{enen}It will end soon.{/enen}"

    ss "{enen}How cruel.{/enen}"

    me "{enen}That's how it works in the cruel world. We can only hope that this one is different.{/enen}"

    window hide

    $ renpy.pause(1.0)

    window show

    ss "{enen}Maybe my death is for the best.{/enen}"

    me "{enen}What are you saying?{/enen}"

    ss "{enen}Eternal love for me. Eternally good little girl for the others.{/enen}"

    ss "{enen}And I will be eternally yours if you want.{/enen}"    

    ss "{enen}But if I live... There will be pain and shame.{/enen}"

    ss "{enen}How can I be what people want me to be? Even if they leave me alone... I can't do it without you. No, I can't play my role anymore.{/enen}"    

    ss "{enen}How can I smile if I am so miserable? How can I call for friendship and unity, being the most lonely person?{/enen}"

    th "She took that play to heart. Definitely."

    me "{enen}You are a strong girl. You've made it so far.{/enen}"    

    me "{enen}We've been through quite a lot together. If you value it as much as I do - then hold on to it. Treasure what you have.{/enen}"

    ss "{enen}But I want more.{/enen}"

    me "{enen}There will be more.{/enen}"

    ss "{enen}How is that?{/enen}"

    me "{enen}I don't know yet. But there will be, I promise.{/enen}"

    ss "{enen}And what if I die? Even then?{/enen}"

    me "{enen}I'm not God. I don't know, honestly. But you'd better stick with the living.{/enen}"

    me "{enen}Actually, what did he say to Jesus? \"I am almighty, but those nails are really tough. I'll give you a week\"?{/enen}"

    show ss_comet at Move((-0.5, -0.5), (1.0, 1.0), 4.0)

    ss "{enen}Don't joke now.{/enen}"

    ss "{enen}Look at the star.{/enen}"

    ss "{enen}What are you gonna do if I... fall like this star? {w}If I die?{/enen}"

    me "{enen}You want me to go after you? Romeo style?{/enen}"

    ss "{enen}No!{/enen}"

    me "{enen}It would be the easy way. So you want me to live and suffer instead?{/enen}"    

    ss "{enen}I want you to live.{/enen}"

    me "{enen}Then I will live.{/enen}"

    ss "{enen}And don't suffer.{/enen}"

    me "{enen}Everyone will. And for a very long time. So you'd better not die. Live your life as it is.{/enen}"

    ss "{enen}I know you will remember me. But not the others. Not everyone.{/enen}"

    me "{enen}Do you want to be remembered?{/enen}"

    ss "{enen}I guess so... Well, who doesn't want it?{/enen}"

    me "{enen}I don't want it.{/enen}"

    ss "{enen}You can't be that shy.{/enen}"

    me "{enen}No, I don't want YOU to be remembered.{/enen}"

    ss "{enen}Oh.{/enen}"

    me "{enen}That's bullshit! I want to know you, not to remember!{/enen}"

    window hide     

    show cg moonstars at Zoom((1920, 1080), (0, 1000, 1920, 1080), (0, 1000, 1920, 1080), 0.0)

    with dissolve 

    window show

    me "{enen}So you're not dying. Got it?{/enen}"

    ss "{enen}...Okay. Not dying.{/enen}"

    me "{enen}You will live your messed up life. And I will be around, messing it up even more.{/enen}"    

    ss "{enen}Yes! I will.{/enen}"

    window hide

    $ renpy.pause(1.5)  

    hide cg

    with dissolve

    window show 

    stop music fadeout 7     

    me "{enen}Get up now. It's getting cold.{/enen}"

    window hide

    show ss shy pv         

    with dspr

    $ renpy.pause(1.5)  

    window show 

    "We stood silent. So many words had been told without seeing each other - like in a dark confessional. Now it felt awkward."

    me "{enen}Good night?{/enen}"

    ss "{enen}Good night.{/enen}"

    ss "{enen}I know you don't take me very seriously. And you're right, probably...{/enen}"    

    me "{enen}You need some time.{/enen}"

    show ss smile2 pv with dspr

    window show     

    ss "{enen}I just want to say that you're the best person.{/enen}"

    window hide   

    $ renpy.pause(1.5)  

    jump ss_d6suddenmt_eng

 

label ss_d6suddenmtmenu_eng:

    $ night_time()

    $ persistent.sprite_time = "night"

    stop music fadeout 3    

    scene bg ext_house_of_mt_night_without_light

    show ss smile2 pv  

    with fade 

    $ renpy.pause(1.0)

    jump ss_d6suddenmt_eng  

    

label ss_d6suddenmt_eng:

    $ night_time()

    $ persistent.sprite_time = "night"

    stop music fadeout 3    

    ss "{enen}Good night.{/enen}"

    window hide    

    $ renpy.pause(1.0)  

    hide ss 

    with dspr

    window show 

    play music music_list["sparkles"]   

    "Samantha almost knocked me off my feet in a sudden rush to hug. I didn’t even have time to get embarrassed." with vpunch

    stop music fadeout 6

    th "After all, it's so great when you're dear to another being. Moreover, to such a sweetheart. {w} What could spoil a nice moment like this?"

    window hide

    $ renpy.pause(0.5)

    play music music_list["doomed_to_be_defeated"] fadein 0     

    show mt angry pioneer with dspr

    window show

    th "Well, yeah."

    "A silent scene. In such darkness, it wasn't hard to take our hugging for making out. Olga Dmitrievna just saw what she wanted to see."

    "Judging by her look, by her faintly moving lips it seemed that she was looking for the most horrible curse of all she knew."

    mt "{enen}It is time to go home, Samantha.{/enen}"

    "- Olga uttered through gritted teeth."

    ss "{enen}Sure! Good night.{/enen}"

    "She hurried to hide in her house, but was stopped by the leader's cold voice."

    show mt rage pioneer    

    mt "{enen} No! Go home! To America! {/enen}"

    show mt angry pioneer   

    window hide

    $ renpy.pause(0.5)      

    show ss scared pv at left 

    with dspr   

    $ renpy.pause(0.5)      

    window show

    me "Hold on! We just had a nice evening and..."

    show ss surprise pv at left     

    mt "I can see that!"

    me "...and just hugged goodbye - that's all."
    stop music fadeout 3
    window hide
    $ renpy.pause(0.5)     
    show mt normal pioneer with dissolve
    $ renpy.pause(0.5)  
    window show
    mt "Oh. I'm sorry. I thought you were..."
    show mt smile pioneer    
    show ss smile2 pv    
    mt "It's nothing then."
    me "...Really?"
    mt "Sure. Go to sleep, guys. Sleep well, Samantha!"    
    ss "{enen}Good night!{/enen}"
    window hide
    $ renpy.pause(0.5)
    hide ss with dspr
    $ renpy.pause(0.5)
    window show
    show mt normal pioneer     
    mt "Behave!"
    window hide
    $ renpy.pause(0.5)    
    hide mt with dissolve
    $ renpy.pause(2)
    $ day_time()
    $ persistent.sprite_time = "day"    
    play music free_air fadein 4    
    show cg ss_cliff with fade2
    $ renpy.pause(1)    
    window show

    "{color=#fff}Dear player! Here ends the English Day 6.{/color}"
    "{color=#fff}As you might have guessed, we changed last conversation a bit - to make it less of a cliffhanger and simply because we have to stop somewhere... for now.{/color}"
    "{color=#fff}Day 6 was without a doubt the hardest part of the translation. It took a few years and a lot of effort from a lot of people to finish it. But together we did it. Hooray!{/color}"
    "{color=#fff}It's still far from being perfect. It wasn't made from something perfect either. But we hope you liked it.{/color}"
    "{color=#fff}Thanks to the site translatedby.com and to the translators: Kompas, A_ya_fonar, MajorGopnik, stalker-ru, Exiled, KriDan, Bust, miruemu and to those, who helped the translation before (we will try to recollect all the names).{/color}"
    "{color=#fff}Thanks to those, who waited for so long. And thanks to you, the reader! It is mostly because of your enthusiasm to see more - we are able to show more! And, hopefully, it will continue so. There is much more to show still.{/color}"
    "{color=#fff}To be continued...{/color}"                       
    window hide 
    stop music fadeout 5
    $ renpy.pause (1)
    scene black with fade
    return

    show mt angry pioneer   

    mt "{enen}Get ready. We go tonight. {/enen}"

    show ss sad pv at left

    me "Don't overreact. You will not send her away like this, will you?"

    mt "This is my duty and, unlike you, I am doing it."

    show ss surprise pv at left     

    ss "{enen}Everything is fine here! No need to worry!{/enen}"

    "Olga Dmitrievna flashed an angry glance at Samantha. Now the counselor, of course, would remind her of our way of playing, if she could."

    show ss sad pv at left

    ss "{enen}Please...{/enen}"

    mt "{enen}Pack bags! {/enen}"

    window hide

    $ renpy.pause(0.3)  

    show ss cry pv at left

    with dspr

    $ renpy.pause(0.5)  

    window show 

    me "You scare her! {w} You scare me too!"

    ss "{enen}Don't fight... I'll go.{/enen}"

    window hide

    $ renpy.pause(0.5)      

    hide ss 

    with dspr

    window show 

    me "What are you doing?!"

    "A shadow of doubt flashed on the leader's face, but a second later the determination returned."

    stop music fadeout 3    

    mt "You promised me, but fled! At night! What kind of a person are you? {w} Why do I spend time with you?"

    window hide

    $ renpy.pause(0.5)      

    hide mt with dspr

    window show     

    mt "I have to start packing..."

    "It came from the house already. The leader left, as if forgetting that such a dangerous criminal remained at large. Is it my last chance to talk with Samantha?"

    window hide

    $ renpy.pause(0.5) 

    stop ambience

    play music Nobodys_Secret fadein 5  

    scene bg night_sam with dissolve

    

    $ persistent.ssg_2 = True 

    

    window show

    "Samantha was really stuffing the suitcase with things, occasionally sobbing."

    me "{enen}Are you really leaving?{/enen}"

    window hide

    $ renpy.pause(0.5) 

    show ss cry pv  

    with dspr   

    $ renpy.pause(0.5)  

    window show 

    ss "{enen}It seems so.{/enen}"    

    me "{enen}You can't leave like this.{/enen}"

    ss "{enen}I can't... fit all the stuff! This stupid bag!{/enen}"

    show ss cry2 pv   

    "Samantha pounded her fists on the stubborn lid, sat down helplessly on the bed and still burst into tears."

    window hide

    hide ss 

    with dspr

    $ renpy.pause(0.5)      

    window show     

    th "Maybe I should let her go? Isn't it the same if she leaves today or three days later. What will change?"

    "..."

    th "No, no! I must save her! Do everything for this. What can I actually do right now? "

    "Samantha did not calm down at all, so she was not able to discuss anything, and I would not have had words."

    "I stroked her back, said a couple of soothing banalities, and went to the leader."

    window hide

    $ renpy.pause(0.5)

    play ambience ambience_int_cabin_night

    scene bg int_house_of_mt_night 

    show mt sad pioneer

    with dissolve

    th "That's who is deciding our fate now, but how to get through?"

    mt "Is she ready?"

    me "She is crying."

    mt "I give her ten minutes."

    me "You have no idea what's going on here..."

    show mt normal pioneer  

    mt "Let me guess: it is a matter of life or death?"

    me "Yes, we can not let her go! It's the only place she is safe!"

    mt "Really? If you could tell me the details..."

    me "I will!"

    mt "...Then I could take it for paranoia, if I did not know you, as the most shameless liar."

    me "I'm not lying, I would like to be mistaken, but... I believe in it, and that's it."

    mt "In what exactly?"

    me "That Samantha is in danger."

    mt "Without you?"

    me "Yes! {w} I mean, \"no\"... And \"yes\"!"

    mt "I understand."

    me "I was filled with her, because I was sorry, I really tried to help... Why are you so harsh?"

    show mt sad pioneer 

    mt "And I've tried to help you, but all my efforts have been useless. Now I need to help her - to get rid of you."

    me "Do you really consider me this evil?"

    show mt smile pioneer       

    mt "Know what? Not evil? Do something good - bring Lena here."

    stop music fadeout 6

    me "Lena? Why?"

    mt "I have to warn someone that I won't be here, give instructions. To avoid panic."

    me "Why not me?"

    mt "You? Well, I would prefer somebody..."

    me "Sane?"

    show mt normal pioneer      

    mt "Oh, nevermind!"

    th "Maybe Lena will talk some sense into her? She can work something out."

    play music music_list["into_the_unknown"] fadein 5  

    me "Ok, I will bring her. And you, please, think twice..."

    mt "Hold on. Do you know where she lives?"

    me "Yes."

    mt "Hmm... No, it will be better to bring Viola. Do you know where her house is?"

    me "I don't."

    mt "Then listen..."

    "Having received the necessary instructions, I went in search of the nurse."

    window hide

    play ambience ambience_camp_center_night

    scene bg ext_aidpost_night  with dissolve

    window show 

    "After wandering around the far side of the camp for a long time, I failed to find the house. It simply was not there."

    window hide 

    $ renpy.pause(0.5)  

    scene bg ss_ext_library_night  with dissolve

    window show 

    th "Should I try to wake everyone up and ask? Well, my crippled body will sooner or later lure the nurse..."

    window hide 

    $ renpy.pause(0.5)      

    scene bg ss_ext_house_of_el_night with dissolve

    window show 

    th "And there's Lena's house, I can ask where Viola lives... But why such complicated schemes? I'll just bring Lena."

    window hide

    play sound sfx_knock_door7_polite   

    $ renpy.pause(1.0)  

    mi "Who's there?"

    me "Semyon."

    mi "In the middle of the night? Have you come to woo?"

    me "I need Lena..."

    mi "I have no doubt! Heh."

    me "The leader calls her, it's urgent."

    mi "Wait, we're not dressed!"

    "Miku laughed again."

    un "I'm getting out."

    window hide

    $ renpy.pause(0.5)

    show un normal pioneer with dspr

    window show 

    un "What happened?"

    me "She is taking Samantha from the camp."

    un "Right now?"

    me "Yes. Come on!"

    un "Let's go."

    window hide 

    $ renpy.pause(0.5)  

    scene bg ext_square_night with dissolve

    

    

    if d6_unchoice >= 2:

        window show

        me "I'm sorry I could not break out to you. She would have thrown a terrible scandal."

        th "And that's exactly what happened."

        un "I understand."

        window hide 

    $ renpy.pause(0.5)

    window show 

    un "But why does she need... me?"

    me "She wants to give you instructions."

    window hide 

    $ renpy.pause(0.5)

    scene bg ext_house_of_mt_night_without_light with dissolve

    me "Hmm, the light is off."

    $ renpy.pause(0.5)  

    window hide

    scene bg int_house_of_mt_night2 with dissolve

    window show

    me "Nobody here."

    $ renpy.pause(0.5)

    scene bg ext_house_of_mt_night_without_light

    show un normal pioneer

    with dissolve   

    window show

    "It did not take long to make sure that both houses are empty."

    me "Where are they? Have left without us, or what?"

    un "Seems so."

    me "Let's go to the bus stop."

    window hide

    scene bg ext_square_night with dissolve

    window show 

    me "You know, I'll run. Maybe they did not wait at all?"

    play sound run_forest fadein 1  

    window hide

    hide un with dspr 

    $ renpy.pause(0.5)

    window show 

    th "Just to catch up!" with vpunch

    stop sound fadeout 1

    window hide

    scene bg ext_no_bus_night with dissolve

    window show

    "And here I am on an empty stop. The car that had been standing here all these days - the evacuator of Samantha for such a case - disappeared. " with vpunch

    th "The leader! She did it for a reason! Lena, Viola... How could I not have seen it before? She just got rid of me!"

    th "How cruel it is - not even to let us say goodbye! {w} Why is it like this? Did she thought that we could not be separated otherwise?"

    th "What now... Is this all?"

    stop music fadeout 3    

    window hide 

    $ renpy.pause(0.5)

    show un sad pioneer with dspr

    $ renpy.pause(0.5)  

    window show 

    un "Are we too late?"

    "Lena understood everything perfectly. Except my strong reaction, maybe"

    un "What a pity."

    me "I did not do something. I hope it does not matter. Otherwise..."

    show un serious pioneer 

    me "No, it's not that. If you thought about something. "

    un "I did not think anything."

    "..."

    play music music_list["silhouette_in_sunset"] fadein 4  

    me "Okay, why stand here..."

    if d6_unchoice == 1:

        jump ss_d7unbye_eng     

    show un normal pioneer  

    un "Do you want me to leave you alone?"

    th "And what if not? It seemed that Lena does not want to leave me, but I should not call her with me? Or should?"

    if d6_unchoice <= 2:

        if sp_un <= 7:

            th "No, why would I call her? It's time to say goodbye."

            jump ss_d7unbye_eng     

    menu:

        "Call her with me":

            me "Samantha could leave us something. Come, let's take a look."

            un "Good idea!"

            un "I mean... I don't need her things, but..."

            me "Let's go. "

            jump ss_d7unhi_eng          

            

        "Say goodbye":

            jump ss_d7unbye_eng

            

    




    
    
    
      